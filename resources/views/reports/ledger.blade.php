@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
#to_date_root .picker__holder {
    right: 30px;
}
.badge{
	font-size: 100%;
}
.sell{
	font-size: 35px;
	font-weight:bolder;
	color:#003473;  
	padding:30px 50px 25px 50px;
	text-align:center;
}
.buy{
	font-size: 35px;
	font-weight:bolder;
	color:#4caf50;   
	padding:30px 50px 25px 50px;
	text-align:center;

}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>General Ledger</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
					    <div class="col-lg-12 col-sm-12 mb-1">
					    	<div class="row">
							    <div class="col-lg-6 mb-4">
				                <form class="needs-validation" novalidate id="fromcustomer" action="#">
							        <div class="card">
	   								 <label class="col-sm-12  col-form-label">CR</label>
							            <div class="card-body">
							            	<div class="row mb-1">
												<label class="col-sm-4  col-form-label">
													<a onclick="GetledgerDetails('ledgercustomer','from');" data-section="From" data-toggle="modal" data-target="#get_modal_details" href="">From Account</a></label>
												<div class="col-sm-6 text-left">
													<input type="text" required class="form-control" name="fromcode" placeholder="From Account">
													<div class="invalid-feedback code_error">
	                                                    Please enter Code
	                                                </div>
												</div>
												<label class="col-sm-2  col-form-label">(CR)</label>
											</div>
											<div class="row mb-1">
												<label class="col-sm-4  col-form-label">Name</label>
												<div class="col-sm-8 text-left">
													<label class="col-sm-8  col-form-label" name="fromname" id="fromname"></label>
												</div>
												<div class="invalid-feedback code_error">
                                                    Please enter Valid Code
                                                </div>
											</div>
											<div class="row mb-1">
												<label class="col-sm-4  col-form-label">Current Balance (S$)</label>
												<div class="col-sm-8 text-left">
													<label class="col-sm-8  col-form-label" name="fromcurbal" id="fromcurbal"></label>
												</div>
												<div class="invalid-feedback code_error">
                                                    Please enter Valid Code
                                                </div>
											</div>
											<div class="row mb-1">
												<label class="col-sm-4  col-form-label">Remarks From</label>
												<div class="col-sm-8 input-group mb-3">
											        <input type="text" name="fromledgerRemark" class="form-control" id="pp_nric" placeholder="Remarks From" required >
											        <div onclick="GetRemarkDetails('ledger','from');$remarkname='From'" class="input-group-append" id="pp_nric" data-toggle="modal" data-target="#get_remark_details" style="cursor: pointer;">
											            <span class="input-group-text">
											                <i class="i-Business-Mens"></i>
											            </span>
											        </div>
													<div class="invalid-feedback code_error">
	                                                    Please enter Remarks
	                                                </div>
												</div>
											</div>
							            </div>
							        </div> 
								</form>

							    </div>
							    <div class="col-lg-6 mb-4">
				                <form class="needs-validation" novalidate id="tocustomer" action="#">
							        <div class="card">
	   								 <label class="col-sm-12  col-form-label">DR</label>
							            <div class="card-body">
							            	<div class="row mb-1">
												<label class="col-sm-4  col-form-label"><a onclick="GetledgerDetails('ledgercustomer','to'); name='To'" data-section="To" data-toggle="modal" data-target="#get_modal_details" href="">To Account</a></label>
												<div class="col-sm-6 text-left">
													<input type="text" required class="form-control" name="tocode" placeholder="To Account">
													<div class="invalid-feedback code_error">
	                                                    Please enter Code
	                                                </div>
												</div>
												<label class="col-sm-2  col-form-label">(DR)</label>
											</div>
											<div class="row mb-1">
												<label class="col-sm-4  col-form-label">Name</label>
												<div class="col-sm-8 text-left">
													<label required class="col-sm-8  col-form-label" name="toname" id="toname"></label>
													<div class="invalid-feedback code_error">
	                                                    Please enter Valid Code
	                                                </div>
												</div>
											</div>
											<div class="row mb-1">
												<label class="col-sm-4  col-form-label">Current Balance (S$)</label>
												<div class="col-sm-8 text-left">
													<label class="col-sm-8  col-form-label" name="tocurbal" id="tocurbal"></label>
												</div>
											</div>
											<div class="row mb-1">
												<label class="col-sm-4  col-form-label">Remarks To</label>
												<div class="col-sm-8 input-group mb-3">
											        <input  required type="text" name="toledgerRemark" class="form-control" id="pp_nric" placeholder="Remarks From">
											        <div onclick="GetRemarkDetails('ledger','to'); $remarkname='To'" class="input-group-append" id="pp_nric" data-toggle="modal" data-target="#get_remark_details" style="cursor: pointer;">
											            <span class="input-group-text">
											                <i class="i-Business-Mens"></i>
											            </span>
											        </div>
													<div class="invalid-feedback">
	                                                    Please enter Remark
	                                                </div>
												</div>
											</div>
							            </div>
							        </div> 
								</form>
							    </div>
					    	</div>
					    </div>

                		<form class="needs-validation" novalidate id="tocustomer" action="#">
						<div class="col-lg-12 col-md-12 mb-4">
						    <div class="card">
						        <div class="card-body">
						           <div class="row">
						               <div class="col-lg-2 col-sm-12">
						                  <div class="row mb-1">
												<label class="col-sm-4  col-form-label">
													<a onclick="GetCurrencyModal('ledgercurrency');" data-toggle="modal" data-target="#get_currency_modal_details" href>
														Curr
													</a>
												</label>
												<div class="col-sm-8 text-left">
													<input type="text" class="form-control" placeholder="Currency" name="ledgercurCode" required>
													<div class="invalid-feedback">
	                                                    Please enter Currency
	                                                </div>
												</div>
											</div> 
						               </div>
						               <div class="col-lg-3 col-sm-12">
						                  <div class="row mb-1">
												<label class="col-sm-4  col-form-label">Amount</label>
												<div class="col-sm-8 text-left">
													<input type="text" class="form-control" name="ledgerAmount" id="ledgerAmount" placeholder="Amount" required>
													<div class="invalid-feedback">
	                                                    Please enter Amount
	                                                </div>
												</div>
											</div> 
						               </div>
						               <div class="col-lg-2 col-sm-12">
						                  <div class="row mb-1">
												<label class="col-sm-4  col-form-label">Rate</label>
												<div class="col-sm-8 text-left">
													<input type="text" class="form-control" name="exchangerate" placeholder="Ex-Rate" required>
													<div class="invalid-feedback">
	                                                    Please enter Rate
	                                                </div>
												</div>
											</div> 
						               </div>
						               <div class="col-lg-3 col-sm-12">
						                  <div class="row mb-1">
												<label for="" class="col-sm-4  col-form-label">Total (S$)</label>
												<div class="col-sm-8 text-left">
													<input type="text" class="form-control" name="totalamount" id="totalamount" placeholder="Total" required>
													<div class="invalid-feedback">
	                                                    Please enter Total
	                                                </div>
												</div>
											</div> 
						               </div>
						               <div class="col-lg-2 col-sm-12 mb-1">
						                  <div class="row mb-1">
												<div class="col-lg-6 text-left">
													<button type="button" class="btn btn-primary" id="saveLedger">Save</button>
												</div>
												<div class="col-lg-6 text-left">
													<button type="button" class="btn btn-primary">Cancel</button>
												</div>
											</div> 
						               </div>
						           </div> 
						        </div>
						    </div>        
						</div>
						</form>

						<div class="col-lg-12 col-md-12 mb-4">
						    <div class="card">
						        <div class="card-body">

						        	<div class="row mb-2">
						        		 <div class="col-lg-4 col-sm-12">
						        		 	<div class="row mb-1">
						        		 		<label class="col-sm-2  col-form-label">Date</label>
						        		 		<div class="col-sm-5 text-left">
						        		 			<input id="datepicker_from" placeholder="dd/mm/yyyy" class="form-control" name="from_date">
						        		 		</div>
						        		 		<div class="col-sm-5 text-left">
						        		 			<input id="datepicker_to" placeholder="dd/mm/yyyy" class="form-control" name="to_date">
						        		 		</div>
						        		 	</div>
						        		 </div>
						        		 <div class="col-lg-3 col-sm-12">
						        		 	<div class="row mb-1">
					        		 			<label class="col-sm-4  col-form-label">Account</label>
					        		 			<div class="col-sm-8 text-left">
					        		 				<input type="text" class="form-control" name="" placeholder="Account">
					        		 			</div>
						        		 	</div>
						        		 </div>
						        		 <div class="col-lg-3 col-sm-12">
						        		 	<div class="row mb-1">
					        		 			<label class="col-sm-4  col-form-label">GL No</label>
					        		 			<div class="col-sm-8 text-left">
					        		 				<input type="text" class="form-control" name="" placeholder="GL No">
					        		 			</div>
						        		 	</div>
						        		 </div>
						        		 <div class="col-lg-2 col-sm-12">
						        		 	<div class="row mb-1">
					        		 			<label class="col-sm-4  col-form-label">User</label>
					        		 			<div class="col-sm-8 text-left">
					        		 				<input type="text" class="form-control" name="" placeholder="User">
					        		 			</div>
						        		 	</div>
						        		 </div>
						        	</div>

						        	<div class="row">
						        		 <div class="col-lg-4 col-sm-12">
						        		 	<div class="row mb-1">
						        		 		<label class="col-sm-4  col-form-label">Remarks</label>
						        		 		<div class="col-sm-8 text-left">
						        		 			<input id="datepicker_from" placeholder="dd/mm/yyyy" class="form-control" name="from_date">
						        		 		</div>
						        		 	</div>
						        		 </div>
						        		 <div class="col-lg-4 col-sm-12">
						        		 	<div class="row mb-1">
					        		 			<div class="col-sm-6 text-left">
													<label class="checkbox checkbox-primary">
														<input class="form-control" type="checkbox" formcontrolname="checkbox">
														<span>Deleted Only</span>
														<span class="checkmark"></span>
													</label>
					        		 			</div>
						        		 	</div>
						        		 </div>
						        		 <div class="col-lg-3 col-sm-12">
						        		 	<div class="row mb-1">
												<div class="col-sm-4">
													<button class="btn btn-primary">Search</button>
												</div>
												<div class="col-sm-4">
													<button class="btn btn-primary">Clear</button>
												</div>
												<div class="col-sm-4">
													<button class="btn btn-primary">Report</button>
												</div>
						        		 	</div>
						        		 </div>
						        	</div>

						        </div>
						    </div>    							
						</div>

					    <!-- Table -->
					    <div class="table-responsive">
					    	 <table id="table_generalledger" class="display nowrap  table-striped table-bordered table" style="width:100%">
                            </table>	
					    </div>
					</div>	
				</div>
			</div>
		</div>	
	@include('modal.customer')	
	@include('modal.ledger')	
	@include('modal.currency')
	@include('modal.dealedit')
@endsection
@section('page-js')
    <script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
    <script src="{{asset('assets/js/ladda.script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="{{asset('assets/js/modal.details.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>

@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){
		var selectedCode;
		generalledgertable();
		$("#datepicker_to , #datepicker_from").inputmask("dd/mm/yyyy", {
	        separator: "/",
	        alias: "dd/mm/yyyy",
	        placeholder: "dd/mm/yyyy"
		});
	});

	const serializeToJSON = str => 
	str.split('&')
	.map(x => x.split('='))
	.reduce((acc, [key, value]) => ({
	  ...acc,
	  [key]: isNaN(value) ? value : ''
	}), {});

	$("input[name='fromcode']").blur(function(){
		getfromcustcode($(this).val())
	});

	$("input[name='tocode']").blur(function(){
		gettocustcode($(this).val())
	});

	$(document).on("click", "#ledger_customer_from",function() {
	    $('#get_modal_details').modal('toggle');
	    $('input[name="fromcode"]').val($(this).val());
	    $('label[name="fromname"]').html(this.getAttribute('data-ref'));
	    $('label[name="fromcurbal"]').html(this.getAttribute('data-curbal'));
	    $('input[name="fromcode"]').trigger( "blur" );
	    selectedCode = $('#ledger_customer_from').val();
	});

	$(document).on("click", "#ledger_customer_to",function() {
	    $('#get_modal_details').modal('toggle');
	    $('input[name="tocode"]').val($(this).val());
	    $('label[name="toname"]').html(this.getAttribute('data-ref'));
	    $('label[name="tocurbal"]').html(this.getAttribute('data-curbal'));
	    $('input[name="tocode"]').trigger( "blur" );
	});
	$(document).on("click", "#ledger_remark_from",function() {
	    $('#get_remark_details').modal('toggle');
	    $('input[name="fromledgerRemark"]').val($(this).val());
	});
	$(document).on("click", "#ledger_remark_to",function() {
	    $('#get_remark_details').modal('toggle');
	    $('input[name="toledgerRemark"]').val($(this).val());
	});
	$(document).on("click", "#currency_id_detail", function(){
	    $('#get_currency_modal_details').modal('toggle');
	    var val = $(this).val();
	    var valarray = val.split('|');
	    $('input[name="ledgercurCode"]').val(valarray[0]);
	    $('input[name="exchangerate"]').val(valarray[2]);
	});
	$(document).on('keyup keypress',"#ledgerAmount", function(){
		var rate = $('input[name="exchangerate"]').val();
	    if(rate){        
	        var totalamount = parseFloat($('input[name="exchangerate"]').val())*parseFloat($('input[name="ledgerAmount"]').val());
	        $('input[name="totalamount"]').val(totalamount.toFixed(2));
	    }
	});
	$(document).on("click","#saveLedger", function(){
		getgeneralledgerDetails();
	})
    $("input[name='ledgercurCode']").blur(function() {
        getCurrency();
    });
    
	function getCurrency() {
	    $.ajax({
	        url : "{{ route('ledger.getCurrency') }}",
	        type: "POST",
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        },
	        data : {
	            id : $('input[name="ledgercurCode"]').val(),
	        },
	        success: function(response){
	            if(response.status == 200){
				    $('input[name="exchangerate"]').val(response.data.AvgCost);
	            } else {
	                $('input[name="exchangerate"]').val('');
	            }
	        },      
	    });
	}

function generalledgertable(){
	var location = 	$('#location').val();
	var from_date = 	$('#from_date').val();
	var to_date = 	$('#to_date').val();

    $('#table_generalledger').DataTable({
    paging:   false,
    destroy: true,
    searching : false,
    autoWidth : false,
    info:     false,
    ordering: false,
    ajax: {
        url:"{{ route('reports.ledger') }}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    },
     columns: [
            /*{data: 'GlN0', name: 'GlNo',title:'GLNo'},*/                    
            {data: 'date', name: 'date',title:'Date'},
            {data: 'from_customer_code', name: 'from_customer_code',title:'From Account (CR)'},
            {data: 'to_customer_code', name: 'to_customer_code',title:'To Account (DR)'},
            {data: 'currency', name: 'currency',title:'Currency'},
            {data: 'amount', name: 'amount',title:'Amount',className: "text-right"},
            {data: 'remark_from', name: 'remark_from',title:'Remarks From'},
            {data: 'remark_to', name: 'remark_to',title:'Remarks To'},
            {data: 'Status', name: 'Status',title:'Delete'},
            {data: 'Select', name: 'Select',title:'Recipt'},
        ],
	});
}
function getgeneralledgerDetails() {

	var fromCustomerCode = $('input[name="fromcode"]').val();
	var fromCustName = $('label[name="fromname"]').text();	
	var toCustName = $('label[name="toname"]').text();
	var toCustomerCode = $('input[name="tocode"]').val();
	var ledgerCurrencyVal = $('input[name="ledgercurCode"]').val();
	var exchangerate = $('input[name="exchangerate"]').val();
	var ledgerAmountVal = $('input[name="totalamount"]').val();
	var remarkFrom = $('input[name="fromledgerRemark"]').val();
	var remarkTo = $('input[name="toledgerRemark"]').val();
	var fromBal = $('#fromcurbal').text();
	var toBal = $('#tocurbal').text();	
	$('#fromcustomer, #tocustomer').removeClass('was-validated');	
	if(fromCustomerCode == '' ||  toCustomerCode == '' || ledgerCurrencyVal == '' || ledgerAmountVal == '' || ledgerAmountVal == 'NaN' || ledgerAmountVal == 0 || remarkFrom == '' || remarkTo == '' || fromCustName == '' || toCustName == '' || fromBal == '' || toBal == '' || exchangerate == '' || fromCustName == 'Please Enter Valid Code' || toCustName == 'Please Enter Valid Code'){

		$('#fromcustomer, #tocustomer').addClass('was-validated');

		return false;
	}
	if(fromCustomerCode == toCustomerCode) {
		$.alert('From Customer and To Customer should not be same');
	}
	else {
	    $.ajax({
	        url: "{{route('reports.ledgercustomerdetails')}}",
	        type: "POST",
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        },
	        data : {
				from_customer_code : fromCustomerCode,
				to_customer_code : toCustomerCode,
				currency : ledgerCurrencyVal,
				amount : ledgerAmountVal,
				remark_from : remarkFrom,
				remark_to : remarkTo,
				fromBal : fromBal,
				toBal   : toBal,
				exchangerate   : exchangerate
	        },
	        success: function(response){
	            if(response.status == 200){
	            	success_toastr(response.content);
	            	generalledgertable();
				    $('input[name="fromcode"]').val('');
				    $('label[name="fromname"]').html('');
				    $('label[name="fromcurbal"]').html('');
				    $('input[name="tocode"]').val('');
				    $('label[name="toname"]').html('');
				    $('label[name="tocurbal"]').html('');
	    			$('input[name="fromledgerRemark"]').val('');
	    			$('input[name="toledgerRemark"]').val('');
				    $('input[name="ledgercurCode"]').val('');
				    $('input[name="exchangerate"]').val('');
	        		$('input[name="ledgerAmount"]').val('');
	        		$('input[name="totalamount"]').val('');
	            } else {
	            	danger_toastr(response.content);
	            }
	        },      
	    });
	}
}

function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}

function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}


function getfromcustcode(code) 
{
	$.ajax({
        url: "{{route('reports.customerdetails')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
			code : code,
        },
        success: function(response){
            if(response.status == 200){
				$('label[name="fromname"]').html(response.data.CustName); 
				$('#fromname').css('color', 'black');
				$('label[name="fromcurbal"]').html(response.data.Balance);
				$('#fromcurbal').css('color', 'black');
            } else {
				$('label[name="fromname"]').text('Please Enter Valid Code'); 
				$('#fromname').css('color', 'red');
				$('label[name="fromcurbal"]').text('Please Enter Valid Code'); 
				$('#fromcurbal').css('color', 'red');
            }
        },      
    });	
}

function gettocustcode(code) 
{
	$.ajax({
        url: "{{route('reports.customerdetails')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
			code : code,
        },
        success: function(response){
            if(response.status == 200){
			    $('label[name="toname"]').html(response.data.CustName);
				$('#toname').css('color', 'black');
			    $('label[name="tocurbal"]').html(response.data.Balance);
				$('#tocurbal').css('color', 'black');
            } else {
				$('label[name="toname"]').text('Please Enter Valid Code'); 
				$('#toname').css('color', 'red');
				$('label[name="tocurbal"]').text('Please Enter Valid Code'); 
				$('#tocurbal').css('color', 'red');
            }
        },      
    });	
}

$( function() {
	$( "#datepicker_to , #datepicker_from").datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection