@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
.filerCheckbox {
	margin-top:10px;
	display: flex;
	align-items: center;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Customer Balance Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

						<div class="col-lg-12 mb-12">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('reports.customerbalancereportexport')}}" method="POST" id="">
					            		@csrf
									<div class="row">
										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="voucher_no" class="col-sm-5  col-form-label">Location</label>
												<div class="col-sm-7 text-left">
													<select class="form-control" name="location" id="location">
														@if(count($alllocations) > 0)
														@foreach($alllocations as $location)
														<option value="{{ $location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){echo 'selected'; } ?> >{{ $location->LocationCode}}</option>
														@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="voucher_no" class="col-sm-5  col-form-label">From Date</label>
												<div class="col-sm-7 text-left">
										            <input type="text" id="from_date" class="form-control" placeholder="From Date" name="from_date">
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="voucher_no" class="col-sm-5  col-form-label">To Date</label>
												<div class="col-sm-7 text-left">
										            <input type="text" id="to_date" class="form-control" placeholder="To Date" name="to_date">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-2 col-sm-12">
											<div class="row filerCheckbox">
												<div class="col-sm-1 text-left">
	                                                <input type="checkbox" id="zeroBalance" name="zeroBalance" value="No">
	                                                <span class="checkmark"></span>
												</div>
												<label for="zeroBalance" class="col-sm-7  col-form-label">Zero Balance</label>
											</div>
										</div>
										<div class="col-lg-3 col-sm-12">
											<div class="row filerCheckbox">
												<div class="col-sm-1 text-left">
	                                                <input type="checkbox" id="zeroDealBalance" name="zeroDealBalance" value="No">
	                                                <span class="checkmark"></span>
												</div>
												<label for="zeroDealBalance" class="col-sm-7  col-form-label">Zero Deal Balance</label>
											</div>
										</div>
										<div class="col-lg-7 col-sm-12 text-right">
											<button class="btn btn-primary m-1" type="button" id="check_details" style="font-size: larger;">Submit</button>
											<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF" style="font-size: larger;">
											<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV" style="font-size: larger;">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>

                        <div class="table-responsive">
                            <table id="table_reportcustomer" class="display nowrap table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="width:80px;">S.No.</th>
                                        <th>CustCode</th>
                                        <th>Name</th>
                                        <th style="text-align:right;">Balance</th>
                                        <th style="text-align:right;">Deal Balance</th>
                                        <th style="text-align:right;">Total</th>
                                    </tr>
                                </thead>
                            </table>    
                        </div>

					</div>	
				</div>
			</div>
		</div>	
    @endsection

@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
	$(document).ready(function(){
		getTableData();

	    $("#from_date , #to_date").inputmask("dd-mm-yyyy", {
			separator: "-",
			alias: "dd-mm-yyyy",
			placeholder: "dd-mm-yyyy",
		});
		$(document).on('change', '#zeroBalance', function() {
	    	if ($(this).is(':checked')) {
	    		$(this).attr('value', 'Yes');
	    	}else {
	    		$(this).attr('value', 'No');
	    	}
		});
		$(document).on('change', '#zeroDealBalance', function() {
	    	if ($(this).is(':checked')) {
	    		$(this).attr('value', 'Yes');
	    	}else {
	    		$(this).attr('value', 'No');
	    	}
		});
	});
	
	$(document).on("click", "#check_details",function() {
       getTableData();
        
    });

	function getTableData(){
		var location = $('#location').val();
		var from_date = $('#from_date').val();
		var to_date = $('#to_date').val();
		/*var zeroBalance = $('input[name="zeroBalance"]:checked') ? 'Yes' : 'No';
		var zeroDealBalance = $('input[name="zeroDealBalance"]:checked') ? 'Yes' : 'No';*/
		var zeroBalance = $('#zeroBalance').val();
		var zeroDealBalance = $('#zeroDealBalance').val();
		$('#table_reportcustomer').DataTable({
			paging: true,
			destroy: true,
			serverSide: true,
			autoWidth : false,
			info:     false,
			searching:true,
			ajax: {
				url:"{{ route('reports.customerbalancereport') }}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: function (d) {
					d.locationcode=location;
					d.from_date=from_date;
					d.to_date=to_date;
					d.zeroBalance=zeroBalance;
					d.zeroDealBalance=zeroDealBalance;
	            },
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'Custcode', name: 'Custcode'},
				{data: 'CustName', name: 'CustName'},
				{data: 'Balance', name: 'Balance',class:'text-right'},
				{data: 'DealBalance', name: 'DealBalance',class:'text-right'},
				{data: 'tot', name: 'tot',class:'text-right'},
			],
		});
	}
	$( function() {
		$( "#from_date, #to_date").datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>
@endsection