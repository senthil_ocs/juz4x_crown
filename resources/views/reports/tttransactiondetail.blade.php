@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>TT Transaction Detail</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-body">
									<form action="{{route('reports.tttransactiondetail')}}" method="POST" id="">
					            		@csrf
									<div class="row">
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
												<label for="from_date" class="col-sm-4  col-form-label">From Date</label>
												<div class="col-sm-7 text-left">
													<input type="text" name="from_date" id="from_date" class="form-control" value="{{$from}}">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
												<label for="to_date" class="col-sm-4  col-form-label">To Date</label>
												<div class="col-sm-7 text-left">
													<input type="text" name="to_date" id="to_date" class="form-control" value="{{$to}}">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4">
 											<button class="btn btn-primary m-1" type="submit" id="check_details" >Submit</button>
											<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>

						<div class="col-lg-12" style="margin-top: 50px;">
						@if(count($final) > 0)
							@foreach($final as $key => $val)
								<?php
								$dates = $key;
								$cumulative_cash = 0;
								$cumulative_nets = 0;
								$cumulative_lamount = 0;
								$cumulative_totamount = 0;
								$cumulative_agentamount = 0;
								$cumulative_profit = 0;
								?>
								@foreach($val as $key1 => $val1)
			                        <div class="table-responsive">
			                            <table id="table_reporttransdetail" class="table table-striped" style="width:100%">
			                                <thead>
			                                    <tr>
			                                        <th scope="col">Trans No</th>
			                                        <th>Agent Name</th>
			                                        <th>Type</th>
			                                        <th>Currency Code</th>
			                                        <th>Rate</th>
			                                        <th>Local Amt</th>
			                                        <th>Comm</th>
			                                        <th>Total Amt</th>
			                                        <th>Agent Rate</th>
			                                        <th>Agent Comm</th>
			                                        <th>Agent Amt</th>
			                                        <th>Profit</th>
			                                        <th>Comm Profit</th>
			                                    </tr>
			                                </thead>
			                                <?php
			                                	$cash = 0;
			                                	$nets = 0;
			                                	$lamount = 0;
			                                	$totamount = 0;
			                                	$agentamount = 0;
			                                	$commprofit = 0;
			                                	$a_profit = 0;
			                                ?>
											@foreach($val1 as $key => $val2)
											@php 
											$profit = $val2['TotalAmount']-$val2['AgentTotal'];
											@endphp
			                                <tbody>
			                                	<tr>
			                                		<td>{{$val2['TranNo']}}</td>
			                                		<td>{{$val2['AgentName']}}</td>
			                                		<td>{{$val2['DocType']}}</td>
			                                		<td>{{$val2['CurrencyCode']}}</td>
			                                		<td class="text-right">{{$val2['ExchRate']}}</td>
			                                		<td class="text-right">{{number_format($val2['LAmount'],2)}}</td>
			                                		<td class="text-right">{{number_format($val2['Comm'],2)}}</td>
			                                		<td class="text-right">{{number_format($val2['TotalAmount'],2)}}</td>
			                                		<td class="text-right">{{number_format($val2['AgentRate'],8)}}</td>
			                                		<td class="text-right">{{number_format($val2['AgentCommission'],2)}}</td>
			                                		<td class="text-right">{{number_format($val2['AgentTotal'],2)}}</td>
			                                		<td class="text-right">{{number_format($profit,2)}}</td>
			                                		@php 
			                                		$commprofit +=$profit;
			                                		@endphp
			                                		<td class="text-right">{{number_format($commprofit,2)}}</td>
			                                		<?php
			                                			if(strtolower($val2['PayMode']) == 'cash'){
			                                				$cash+=$val2['LAmount']; 
			                                			} else {
			                                				$nets+=$val2['LAmount'];
			                                			} 
			                                			$lamount+=$val2['LAmount'];
			                                			$totamount+=$val2['TotalAmount'];
			                                			$agentamount+=$val2['AgentTotal'];
			                                			$a_profit+=$profit;
			                                		?>
			                                	</tr>
			                                </tbody>
											@endforeach
											<tfoot style="border-bottom: solid 1px #dee2e6;background: #d6d4d4;">
												<tr>
													<?php
														$cumulative_cash+=$cash;
														$cumulative_nets+=$nets;
														$cumulative_lamount+=$lamount;
			                                			$cumulative_totamount+=$totamount;
			                                			$cumulative_agentamount+=$agentamount;
			                                			$cumulative_profit+=$a_profit;
													?>
													<td colspan='5'><b>{{$key1}}</b></td>
													<td class="text-right"><b>{{number_format($lamount,2)}}</b></td>
													<td colspan='2' class="text-right"><b>{{number_format($totamount,2)}}</b></td>
													<td colspan='3' class="text-right"><b>{{number_format($agentamount,2)}}</b></td>
													<td class="text-right"><b>{{number_format($a_profit,2)}}</b></td>
													<td></td>
												</tr>
											</tfoot>
			                            </table>    
			                        </div>
								@endforeach
								<div class="table-responsive" style="position: relative;bottom: 12px;">
									<table id="table_reporttransdetail" class="table table-striped" style="width:100%;">

										<tbody style="border-bottom: solid 1px #dee2e6;background: #d6d4d4;">
											<tr>
												<td style="width: 34%;"><b>{{date("d-m-Y", strtotime($dates))}}</b></td>
												<td class="text-right" style="width: 11%;"><b>{{number_format($cumulative_lamount,2)}}</b></td>
												<td colspan='2' class="text-right" style="width: 13%;"><b>{{number_format($cumulative_totamount,2)}}</b></td>
												<td colspan='3' class="text-right" style="width: 24%;"><b>{{number_format($cumulative_agentamount,2)}}</b></td>
												<td  style="width: 10%;"><b>{{number_format($cumulative_profit,2)}}</b></td>
												<td></td>
											</tr>
											<tr>
												<td style="width: 34%;"></td>
												<td colspan='2' style="width: 11%;"><b>CASH : {{number_format($cumulative_cash,2)}}</b></td>
												<td colspan='3' style="width: 24%;"><b>NETS : {{number_format($cumulative_nets,2)}}</b></td>
												<td colspan="3"></td>
											</tr>
										</tbody>
									</table>
								</div>
	                        @endforeach
                        @else 
                        	<p>Not Found Data!</p>
                        @endif 
                    	</div>
					</div>	
				</div>
			</div>
		</div>	
    @endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
    <script src="{{asset('assets/js/ladda.script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="{{asset('assets/js/modal.details.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')
@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
$(document).ready(function(){

	$("#from_date , #to_date").inputmask("dd-mm-yyyy", {
		separator: "-",
		alias: "dd-mm-yyyy",
		placeholder: "dd-mm-yyyy",
	});
});

$( function() {
	$( "#from_date, #to_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection