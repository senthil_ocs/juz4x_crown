@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Buy sell Transaction Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

						<form action="{{ route('buyselltransactionreport.export')}}" method="POST" id="buyselltransactionreport">
							@csrf
							<div class="row">
								<div class="col-lg-12 col-sm-12">

									<div class="row">
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
												<label for="location" class="col-sm-4  col-form-label">Location</label>
												<div class="col-sm-8 text-left">
													<select class="form-control" name="location" id="location">
													@if(count($alllocations) > 0)
														@foreach($alllocations as $location)
														<option value="{{$location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){echo 'selected'; } ?> >{{ $location->LocationCode}}</option>
														@endforeach
													@endif
													</select>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
											 	<label for="location" class="col-sm-4  col-form-label text-right">From data</label>
											 	<div class="col-sm-8">
											 		<input type="text" id="from_date" class="form-control" placeholder="From Data" name="from_date" value="{{$fromDate}}">
											 	</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
											 	<label for="location" class="col-sm-4  col-form-label text-right">To data</label>
											 	<div class="col-sm-8">
											 		<input type="text" id="to_date" class="form-control" placeholder="To Data" name="to_date" value="{{$toDate}}">
											 	</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row mt-3">
								<div class="col-lg-7 col-sm-12">
								</div>
								<div class="col-lg-5 col-sm-12 text-right">
										<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
									<input  class="btn btn-primary m-1" type="submit" name="export"  value="PDF">

									<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV">
								</div>
							</div>
						</form>

                        <div class="table-responsive mt-5">
                            <table id="table_buyselltransactionlist" class="display nowrap table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
										<th>TransNo</th>
										<th>TransDate</th>
										<th>CustCode</th>
										<th>Currency</th>
										<th>B/S</th>
										<th class="text-right">FAmount</th>
										<th class="text-right">Ex.Rate</th>
										<th class="text-right">LAmount</th>
										<!-- <th>Remarks</th> -->
                                    </tr>
                                </thead>
                            </table>    
                        </div>

					</div>	
				</div>
			</div>
		</div>	
    @endsection

@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
$(document).ready(function(){

    $("#from_date, #to_date").inputmask("dd-mm-yyyy", {
        separator: "-",
        alias: "dd-mm-yyyy",
        placeholder: "dd-mm-yyyy"
    });

    getdatatable(); 
});

function submitForm(type) {
	getdatatable()
}

function getdatatable() {
	var location = 	$('#location').val();
	var from_date = $('#from_date').val();
	var to_date = 	$('#to_date').val();

	$('#table_buyselltransactionlist').DataTable({
        paging:   false,
        destroy: true,
        searching : false,
        autoWidth : false,
        info:     false,
        serverSide: true,
		ajax: {
			url:"{{ route('buyselltransactionreport') }}",
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			 data: function (d) {
			 	d.location=location;
			 	d.from_date=from_date;
			 	d.to_date=to_date;
			 },
		},
		columns: [
			{data: 'DT_RowIndex', name: 'DT_RowIndex'},
			{data: 'TranNo', name: 'TranNo'},
			{data: 'TranDate', name: 'TranDate'},
			{data: 'CustCode', name: 'CustCode'},
			{data: 'CurrencyCode', name: 'CurrencyCode'},
			{data: 'bs', name: 'bs'},
			{data: 'FAmount', name: 'FAmount', class:'text-right'},
			{data: 'Rate', name: 'Rate', class:'text-right'},
			{data: 'LAmount', name: 'LAmount', class:'text-right'},
		],
	});
}
$( function() {
	$( "#from_date, #to_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection