@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Cash Customer Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

						<div class="col-lg-12 mb-12">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('reports.cashcustomerexport')}}" method="POST" id="">
					            		@csrf
									<div class="row">
										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="voucher_no" class="col-sm-5  col-form-label">Location</label>
												<div class="col-sm-7 text-left">
													<select class="form-control" name="location" id="location">
														<option value="">Select location</option>
														@if(count($alllocations) > 0)
														@foreach($alllocations as $location)
														<option value="{{ $location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){echo 'selected'; } ?> >{{ $location->LocationCode}}</option>
														@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>

										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="voucher_no" class="col-sm-5  col-form-label">Name</label>
												<div class="col-sm-7 text-left">
													<input type="textbox" class="form-control" name="cust_name" id="cust_name" placeholder="Enter Name">
												</div>
											</div>
										</div>

										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="voucher_no" class="col-sm-5  col-form-label">Phone No</label>
												<div class="col-sm-7 text-left">
													<input type="textbox" class="form-control" name="cust_phone" id="cust_phone" placeholder="Enter Phone No">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-7 col-sm-12"></div>
										<div class="col-lg-5 col-sm-12 text-right">
											<button type="button" class="btn btn-primary m-1" id="check_details" onclick="submitForm('submit');">Submit</button>
											<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF">
											<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>

                        <div class="table-responsive">
                            <table id="table_reportcustomer" class="display nowrap table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>CustCode</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>DOB</th>
                                        <th>NRICNo</th>
                                    </tr>
                                </thead>
                            </table>    
                        </div>

					</div>	
				</div>
			</div>
		</div>	
    @endsection

@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
	$(document).ready(function(){
		getdatas();
	});
	function submitForm(type) {	
		getdatas();
	}
	function getdatas() {
		$('#table_reportcustomer').DataTable({
			paging:   true,
			destroy: true,
			serverSide: true,
			autoWidth : false,
			info:     false,
			searching:false,
			ajax: {
				url: "./cashcustomer",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: function (d) {
					d.cust_name = $('input[name="cust_name"]').val();
					d.cust_phone = $('input[name="cust_phone"]').val();
				},
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'CustCode', name: 'CustCode'},
				{data: 'Name', name: 'Name'},
				{data: 'PhoneNo', name: 'PhoneNo'},
				{data: 'DOB', name: 'DOB'},
				{data: 'PPNo', name: 'PPNo'},
			],
		});
	}
</script>
@endsection