@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
#to_date_root .picker__holder {
    right: 30px;
}
.badge{
	font-size: 100%;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Locationwise Stock Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

					    <div class="col-lg-12 mb-12">
					        <!-- <div class="card"> -->
					            <!-- <div class="card-body"> -->
					            	<form action="{{ route('reports.locationStockReportExport')}}" method="POST" id="ledgercurrencyfilterform">
					            		@csrf
										<div class="row">
											<div class="col-lg-4 col-sm-4 mb-1">
												<div class="row">
													<label for="location" class="col-sm-5  col-form-label">Location Name</label>
													<div class="col-sm-7 text-left">
														<select class="form-control" name="location" id="location">
															<option value="">Select location</option>
														@if(count($alllocations) > 0)
															@foreach($alllocations as $location)
															<option value="{{$location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){echo 'selected'; } ?> >{{ $location->LocationCode}}</option>
															@endforeach
														@endif
														</select>
													</div>
												</div>
											</div>	

											<div class="col-lg-3 col-sm-4 mb-1">
												<label class="radio radio-primary">
														<input class="form-control" type="radio" name="businessnature" value="current stock" formcontrolname="radio" checked="checked">
													<span>Current Stock</span>
													<span class="checkmark"></span>
												</label>
											</div>

											<div class="col-lg-3 col-sm-4 mb-1">
												<label class="radio radio-primary">
														<input class="form-control" type="radio" name="businessnature" value="deal stock" formcontrolname="radio">
													<span>Deal Stock</span>
													<span class="checkmark"></span>
												</label>
											</div>
										</div>

										<div class="row mt-3">
											<div class="col-lg-7 col-sm-12">
											</div>
											<div class="col-lg-5 col-sm-12 text-right">
	 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
												<input  class="btn btn-primary m-1" type="submit" name="export"  value="PDF">

												<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV">
											</div>
										</div>
					            	</form>
					            <!-- </div> -->
					        <!-- </div> -->
					    </div>
					    <div class="table-responsive">
					       <table id="table_locationstockreport" class="display nowrap  table-striped table-bordered table" style="width:100%">
                           
                           </table>	
					    </div>
					</div>	
				</div>
			</div>
		</div>	
	@include('modal.customer')	
	@include('modal.currency')
@endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/modal.details.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){
 		triggertabel();
	});

	function submitForm(type) {
		var location = 	$('#location').val();
		var businessnature = $('input[name="businessnature"]:checked').val();
		if(location=='') {
			$('#location').css('border','1px solid #ff7d7d');
			return false;
		} else {
			triggertabel();
		}
	}

	function triggertabel() {
		var location = 	$('#location').val();
		var date = new Date();
 		var businessnature = $('input[name="businessnature"]:checked').val();
		$('#table_locationstockreport').DataTable({
		    paging:   false,
		    destroy: true,
		    searching : false,
		    autoWidth : false,
		    info:     false,
		    ordering: false,
		    ajax: {
		            url:"{{ route('reports.locationStockReport') }}",
		            type: "POST",
		            headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		            data: function (d) {
						d.location     = location;
						d.businessnature = businessnature;
		            },
		        },
		    columns: [
		            {data: 'CurrencyCode', name: 'CurrencyCode',title: "Currency"},
		            {data: 'Stock', name: 'Stock',title: "Stock", class: "text-right"},
		            {data: 'AvgCost', name: 'AvgCost',title: "Average Cost", class: "text-right"},
		            {data: 'LValue', name: 'LValue',title: "Local Value", class: "text-right"},
		            {data: 'Date', name: 'Date',title: "Last P.Date"},
		        ],
		    
		});
	}
	
	$( "#ledgercurrencyfilterform" ).submit(function( event ) {
		var location = 	$('#location').val();
	 	if(location=='') {
			$('#location').css('border','1px solid #ff7d7d');
			return false;
		}else{
			return true;
		}
	  event.preventDefault();
	});

</script>
@endsection