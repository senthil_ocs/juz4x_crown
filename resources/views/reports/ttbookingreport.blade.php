@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">

</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>TT Booking Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

						<div class="col-lg-12 mb-12">
							<div class="card">
								<div class="card-body">
									<form action="{{route('reports.ttbookingreport')}}" method="POST" id="">
					            		@csrf
									<div class="row">
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
												<label for="custName" class="col-sm-4  col-form-label"><a onclick="GetModalDetails('customer');" data-toggle="modal" data-target="#get_modal_details" href="">Customer/Agent</a> </label>
												<div class="col-sm-7 text-left">
													<input type="text" name="custName" id="custName" class="form-control" value="{{$custName}}">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
												<label for="currency" class="col-sm-4  col-form-label"><a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">Currency</a></label>
												<div class="col-sm-7 text-left">
													<input type="text" name="currency" id="currency" class="form-control" value="{{$currency}}">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4 mb-1">
										</div>
									</div>
									<div class="row">
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
												<label for="from_date" class="col-sm-4  col-form-label">From Date</label>
												<div class="col-sm-7 text-left">
													<input type="text" name="from_date" id="from_date" class="form-control" value="{{$fromDate}}">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
												<label for="to_date" class="col-sm-4  col-form-label">To Date</label>
												<div class="col-sm-7 text-left">
													<input type="text" name="to_date" id="to_date" class="form-control" value="{{$toDate}}">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4">
 											<button class="btn btn-primary m-1" type="submit" id="check_details" >Submit</button>
											<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>

                        <div class="col-lg-12" style="margin-top: 50px;">
	                        <div class="table-responsive">
	                            <table id="table_bookingreport" class="table table-striped" style="width:100%">
		                            <thead>
	                                	<tr>
	                                		<th colspan="9">{{$compName}}</th>
	                                		<th colspan="2">Date : {{date('d/m/Y H:s')}}</th>
	                                	</tr>
	                                	<tr><th colspan="11">Booking Transaction Report</th></tr>
	                                    <tr>
	                                        <th>RefNo</th>
	                                        <th>Type</th>
	                                        <th>Customer</th>
	                                        <th>Booking Date</th>
	                                        <th>Value Date</th>
	                                        <th>Valid Till</th>
	                                        <th>Currency</th>
	                                        <th>FAmount</th>
	                                        <th>Rate</th>
	                                        <th>Balance</th>
	                                        <th>View</th>
	                                    </tr>
	                                </thead>
								@if(count($final) > 0)
                               	@foreach($final as $key => $val)
		                            <thead>
	                                	<tr><th colspan="11">{{$key}}</th></tr>
	                                </thead>
	                                	<?php $balanceAmount = 0; ?>
										@if(count($val) > 0)
		                               	@foreach($val as $key => $val)
		                               		<?php if($val->TranType == 'Buy'){
		                               			$balanceAmount += $val->FAmount;
		                               		} else {
		                               			$balanceAmount -= $val->FAmount;
		                               		}
		                               		?>
			                                <tbody>
			                                	<tr>
			                                		<td>{{$val->DealNo}}</td>
			                                		<td>{{$val->TranType}}</td>
			                                		<td>{{$val->CustomerName}}</td>
			                                		<td>{{$val->DealDate != ""? date("d-m-Y H:i:s",strtotime($val->DealDate)):""}}</td>
			                                		<td>{{$val->DealDate != ""? date("d-m-Y",strtotime($val->DealDate)):""}}</td>
			                                		<td>{{$val->ValidTill != ""? date("d-m-Y",strtotime($val->ValidTill)):""}}</td>
			                                		<td>{{$val->CurrencyName}}</td>
			                                		<td class="text-right">{{number_format($val->FAmount,2)}}</td>
			                                		<td class="text-right">{{$val->Rate}}</td>
			                                		<td class="text-right"><?php echo $val->TranType == 'Sell' ? '-' : '' ?>{{number_format($val->BalanceAmount,2)}}</td>
			                                		<td><a id="editForm" href="" data-toggle="modal" data-target="#editDealModal" data-ref = <?php echo($val->TranNo) ?> >Show Tran</a></td>
			                                	</tr>
			                                </tbody>
										@endforeach	
				                        @endif 
	                                <tbody>
	                                	<tr><th colspan="9"></th><th class="text-right">{{number_format($balanceAmount,2)}}</th><th></th></tr>
	                                </tbody>
									@endforeach	
			                        @else 
			                        	<tbody><tr><td colspan="11">Not Found Data!</td></tr></tbody>
			                        @endif 
	                            </table>    
	                        </div>
	                    </div>
					</div>	
				</div>
			</div>
		</div>	
		<div id="editDealModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <h4 class="modal-title">Remittance Booking</h4>
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				    </div>

				    <div class="col-md-12 mb-12 master-main-sec" style="margin-top: 10px;">
			            <div class="card">
			                <h5 class="m-2">Deal Details</h5>
			                <div class="card-body">
			                    <div class="row">
			                       <div class="col-lg-12 col-sm-12">
						            	<form  class="needs-validation" method="post" novalidate action="#" id="editDeal" name="editDeal">
						            		{{csrf_field()}}
											<div class="form-group row">
                                                <label for="custagentName" class="col-sm-5 col-form-label">Customer/Agent<span class="mandatory">*</span>
                                                </label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="custagentName" autocomplete="off" id="custagentName" readonly>
                                                </div>
                                           	</div>
											<div class="form-group row">
                                               <label for="agentname" class="col-sm-5 col-form-label text-rights">Type</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="dealType" autocomplete="off" id="dealType" readonly>
                                                </div>
											</div>
                                          	 <div class="form-group row">
                                               <label for="currname" class="col-sm-5 col-form-label text-rights">Currency</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="currname" autocomplete="off" id="currname" readonly>
                                                </div>
                                          	</div>
                                           	<div class="form-group row">
                                               <label for="amount" class="col-sm-5 col-form-label text-rights">Amount</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="amount" autocomplete="off" id="amount" readonly>
                                                </div>
                                           	</div>
                                           	<div class="form-group row">
                                               <label for="rate" class="col-sm-5 col-form-label text-rights">Rate</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="rate" autocomplete="off" id="rate" readonly>
                                                </div>
                                           	</div>
                                           	<div class="form-group row">
                                               <label for="sgdAmount" class="col-sm-5 col-form-label text-rights">SGD Amount</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="sgdAmount" autocomplete="off" id="sgdAmount" readonly>
                                                </div>
                                           	</div>
                                           	<div class="form-group row">
                                               <label for="validTill" class="col-sm-5 col-form-label text-rights">Valid Till</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="validTill" id="validTill" readonly>
                                                </div>
                                           	</div>
                                           	<div class="form-group row">
                                               <label for="amount" class="col-sm-5 col-form-label text-rights">Value Date</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="valueDate" id="valueDate" readonly>
                                                </div>
                                           	</div>
                        					<button type="button" class="btn btn-primary m-1" style="float:right; close" data-dismiss="modal">Close</button>
										</form>
									</div>
								</div>
							</div>
						</div>
				    </div>
			    </div>
		  	</div>
		</div>

	@include('modal.customer')
	@include('modal.currency')
    @endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
    <script src="{{asset('assets/js/ladda.script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="{{asset('assets/js/modal.details.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')
@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
$(document).ready(function(){

	$(" #from_date, #to_date, #validTill, #valueDate").inputmask("dd-mm-yyyy", {
		separator: "-",
		alias: "dd-mm-yyyy",
		placeholder: "dd-mm-yyyy"
	});

    $(document).on("click", "input[name='currency_id_detail']",function() {
        $('#get_currency_modal_details').modal('toggle');
        var val = $(this).val();
        var valarray = val.split('|');
        $('input[name="currency"]').val(valarray[1]);
    });

    $(document).on("click", "input[name='customer_id_detail']",function() {
        $('#get_modal_details').modal('toggle');
        $('input[name="custName"]').val($(this).attr('data-ref'));
    });

	$(document).on("click", "#editForm",function() {
		var TranNo = $(this).attr('data-ref');
	    	if(TranNo != ''){
    		 $.ajax({
	           type:'GET',
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        },
	           data:{
	           		TranNo : TranNo
	           },
	           url:"{{ route('reports.getBookingReport') }}",
	           success:function(result){
		           	if(result.status == 200){
		           		data = result.data[0];
		           		var date = new Date(data.ValidTill);
						var ValidTill = date.toString('dd-MM-yy');
		           		var date = new Date(data.DealDate);
						var DealDate = date.toString('dd-MM-yy');
			           	$('input[name="custagentName"]').val(data.CustomerName);
			           	$('#dealType').val(data.TranType);
			           	$('input[name="currname"]').val(data.CurrencyName);
			           	$('input[name="amount"]').val(data.FAmount);
			           	$('input[name="rate"]').val(data.Rate);
			           	$('input[name="sgdAmount"]').val(data.LAmount);
			           	$('#validTill').val(ValidTill);
			           	$('#valueDate').val(DealDate);    	
		           	}
	           }
	        });
	    }
	});

});
$( function() {
	$( "#from_date, #to_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection