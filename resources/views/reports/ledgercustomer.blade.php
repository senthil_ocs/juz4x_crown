@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
#to_date_root .picker__holder {
    right: 30px;
}
.badge{
	font-size: 100%;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Ledger Customer Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

					    <div class="col-lg-12 mb-12">
					        <!-- <div class="card"> -->
					            <!-- <div class="card-body"> -->
					            	<form action="{{ route('reports.ledgercustomerexport')}}" method="POST" id="">
					            		@csrf
										<div class="row">
											<div class="col-lg-3 col-sm-6 mb-1">
												<div class="row">
													<label for="location" class="col-sm-5  col-form-label">Location</label>
													<div class="col-sm-7 text-left">
														<select class="form-control" name="location" id="location">
															<option value="">Select location</option>
														@if(count($alllocations) > 0)
															@foreach($alllocations as $location)
															<option value="{{$location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){echo 'selected'; } ?> >{{ $location->LocationCode}}</option>
															@endforeach
														@endif
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-3 col-sm-6 mb-1">
												<div class="row">
												 	<label for="location" class="col-sm-4 col-form-label text-right">
												 	<a onclick="GetModalDetails('customer');" data-toggle="modal" data-target="#get_modal_details" href="">
												 		Code
												 	</a>
												 	</label>
												 	<div class="col-sm-8">
												 		<input id="cust_code" class="form-control" placeholder="Customer Code" name="cust_code" onkeyup="this.value = this.value.toUpperCase();">
												 	</div>
												</div>
											</div>
											<div class="col-lg-3 col-sm-6 mb-1">
												<div class="row">
												 	<label for="location" class="col-sm-6  col-form-label text-right">From data</label>
												 	<div class="col-sm-6">
												 		<input type="text" id="from_date" class="form-control" placeholder="From Data" name="from_date" value="{{$fromDate}}">
												 	</div>
												</div>
											</div>
											<div class="col-lg-3 col-sm-6 mb-1">
												<div class="row">
												 	<label for="location" class="col-sm-6  col-form-label text-right">To data</label>
												 	<div class="col-sm-6">
												 		<input type="text" id="to_date" class="form-control" placeholder="To Data" name="to_date"  value="{{$toDate}}">
												 	</div>
												</div>
											</div>
										</div>
										<div class="row mt-3">
											<div class="col-lg-7 col-sm-12">
											</div>
											<div class="col-lg-5 col-sm-12 text-right">
	 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
												<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF">

												<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV">
											</div>
										</div>
					            	</form>
					            <!-- </div> -->
					        <!-- </div> -->
					    </div>
					    <div class="table-responsive">
					    	<div id="table_ledgercustomer_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
						    	<table id="" class="display nowrap  table-striped table-bordered table" style="width:100%">
						    		<tr>
						    			<td class="text-right">Opening Balance</td>
						    			<td id="bbuy" style="width: 8%;" class="text-right"></td>
						    			<td id="bsell" style="width: 8%;" class="text-right"></td>
						    			<td style="width: 8%;"></td>
						    		</tr>
						    	</table>
					    	</div>
					    	 <table id="table_ledgercustomer" class="display nowrap  table-striped table-bordered table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Trans No</th>
                                        <th>Trans Date</th>
                                        <th>Curr Description</th>
                                        <th>Type B/S</th>
                                        <th>Rate</th>
                                        <th>F.Currency</th>
                                        <th>Sell</th>
                                        <th>Buy</th>
                                        <th>Balance</th>
                                    </tr>
                                </thead>					    	 
                            </table>	
					    </div>
					</div>	
				</div>
			</div>
		</div>	
	@include('modal.customer')	
	@include('modal.cashcustomer')
@endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/modal.details.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
	<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){

	    $('#from_date, #to_date').inputmask("dd-mm-yyyy", {
	        separator: "-",
	        alias: "dd-mm-yyyy",
	        placeholder: "dd-mm-yyyy"
	    });
		$('#bbuy').html(0);
		$('#bsell').html(0);
	    $(document).on("click", "input[name='customer_id_detail']",function() {
	        $('#get_modal_details').modal('toggle');
	        $('input[name="cust_code"]').val($(this).val());
	    });
	    $('#table_ledgercustomer').DataTable({
            paging:   false,
            destroy: true,
            searching : false,
            autoWidth : false,
            info:     false,
            ordering: false,
	    });
	});

	function submitForm(type) {
		var location = 	$('#location').val();
		var code = 	$('input[name="cust_code"]').val();
		var fdate = $('input[name="from_date"]').val();
		var tdate = $('input[name="to_date"]').val();
		if(code == ''){
			$('input[name="cust_code"]').css('border','1px solid #ff7d7d');
			return false;
		} else {
			$('input[name="cust_code"]').css('border','1px solid #ced4db');
            $('#table_ledgercustomer').DataTable({
                paging:   false,
                destroy: true,
                searching : false,
                autoWidth : false,
                info:     false,
                serverSide: true,
                ordering: false,
                ajax: {
                    url:"{{ route('reports.ledgercustomer') }}",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function (d) {
						d.location=location;
						d.code=code;
						d.fdate=fdate;
						d.tdate=tdate;
                    },
                },
                columns: [
                    {data: 'TranNo', name: 'TranNo'},
                    {data: 'TranDate', name: 'TranDate'},
                    {data: 'CurrencyCode', name: 'CurrencyCode'},
                    {data: 'TranType', name: 'TranType'},
                    {data: 'Rate', name: 'Rate',className: "text-right"},
                    {data: 'FAmount', name: 'FAmount',className: "text-right"},
                    {data: 'Tsell', name: 'Tsell',className: "text-right"},
                    {data: 'Tbuy', name: 'Tbuy',className: "text-right"},
                    {data: 'bal', name: 'bal',className: "text-right"},
                ],
            });

			$.ajax({
	    		url: "./customerbalance",
	    		type: "POST",
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					location : location,
					code : code,
					fdate : fdate,
					tdate : tdate,
				},
				beforeSend: function() {},
				success: function(response){
					if(response.status == 200){
						var finder = response.bal.OpeningBalance;
						if(finder >= 0){
							$('#bbuy').html(response.bal);
						} else {
							$('#bsell').html(response.bal);
						}
					} else {
						$('#bbuy').html(0);
						$('#bsell').html(0);
					}	
				},
				error: function(){}
	    	});
		}
	}	
$( function() {
	$( "#from_date, #to_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection