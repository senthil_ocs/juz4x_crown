@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Customer Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

						<div class="col-lg-12 mb-12">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('reports.customerexport')}}" method="POST" id="">
					            		@csrf
									<div class="row">
										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="voucher_no" class="col-sm-5  col-form-label">Location</label>
												<div class="col-sm-7 text-left">
													<select class="form-control" name="location" id="location">
														<option value="" >Select location</option>
														@if(count($alllocations) > 0)
														@foreach($alllocations as $location)
														<option value="{{ $location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){ echo "selected"; } ?>>{{ $location->LocationCode}}</option>
														@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-6 mb-1 mt-2">
											<div class="row">
												<div class="col-lg-12">
													<label class="checkbox checkbox-primary">
														<input class="form-check-input" type="checkbox" name="codeactive" id="codeactive" value="active" checked>
														<span class="ml-3">Active</span>
														<span class="checkmark"></span>
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-7 col-sm-12"></div>
										<div class="col-lg-5 col-sm-12 text-right">
 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
											<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF">
											<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>

                        <div class="table-responsive">
                            <table id="table_reportcustomer" class="display nowrap table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Custcode</th>
                                        <th>CustName</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>DOB</th>
                                        <th>CompanyRegNo</th>
                                        <th>MoneyChangerLicense</th>
                                        <th>NRICNo</th>
                                    </tr>
                                </thead>
                            </table>    
                        </div>

					</div>	
				</div>
			</div>
		</div>	
    @endsection

@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')
@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
$(document).ready(function(){
	getdatatable();
	$('input[name="codeactive"]').click(function(){
	    if($(this).prop("checked") == true){
	       $('input[name="codeactive"]').val('active');
	    }
	    else if($(this).prop("checked") == false){
	        $('input[name="codeactive"]').val('inactive');
	    }
	});
});

function submitForm(type) {
	getdatatable()
}

function getdatatable() {
	var location = 	$('#location').val();
	var active =  $('input[name="codeactive"]').val();
	console.log(active);
	$('#table_reportcustomer').DataTable({
		paging:   false,
		destroy: true,
		serverSide: true,
		autoWidth : false,
		info:     false,
		searching:false,
		ajax: {
			url: "./customer",
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			 data: function (d) {
			 	d.location=location;
			 	d.active=active;
			 },
		},
		columns: [
			{data: 'DT_RowIndex', name: 'DT_RowIndex'},
			{data: 'Custcode', name: 'Custcode'},
			{data: 'CustName', name: 'CustName'},
			{data: 'customer.Phone', name: 'customer.Phone'},
			{data: 'customer.Email', name: 'customer.Email'},
			{data: 'customer.DOB', name: 'customer.DOB'},
			{data: 'customer.CompanyRegNo', name: 'customer.CompanyRegNo'},
			{data: 'customer.MoneyChangerLicense', name: 'customer.MoneyChangerLicense'},
			{data: 'customer.NRICNo', name: 'customer.NRICNo'},
		],
	});
}
</script>
@endsection