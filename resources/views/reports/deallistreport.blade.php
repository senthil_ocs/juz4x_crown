@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
#to_date_root .picker__holder {
    right: 30px;
}
.badge{
	font-size: 100%;
}
.sell{
	font-size: 35px;
	font-weight:bolder;
	color:#003473;  
	padding:30px 50px 25px 50px;
	text-align:center;
}
.buy{
	font-size: 35px;
	font-weight:bolder;
	color:#4caf50;   
	padding:30px 50px 25px 50px;
	text-align:center;

}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Deal List</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			    <li><a href="{{ route('dealview') }}">Create</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

					    <div class="col-lg-12 mb-12">
					        <div class="card">
					            <div class="card-body">
					            	<form action="{{ route('deallistexport')}}" method="POST" id="deallistform">
					            		@csrf
										<div class="row">
											<div class="col-lg-6 col-sm-12 mb-1">
												<div class="row">
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
															<label for="location" class="col-sm-4  col-form-label">Location</label>
															<div class="col-sm-8 text-left">
																<select class="form-control" name="location" id="location">
																	<option value="">Select location</option>
																@if(count($alllocations) > 0)
																	@foreach($alllocations as $location)
																	<option value="{{$location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){echo 'selected'; } ?> >{{ $location->LocationCode}}</option>
																	@endforeach
																@endif
																</select>
															</div>
														</div>
													</div>
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
															<label for="trantype" class="col-sm-4  col-form-label">Trans Type</label>
															<div class="col-sm-8 text-left">
																<select class="form-control" name="transtype" id="trantype">
																	<option value="">Select Type</option>
																	<option value="buy">Buy</option>
																	<option value="sell">Sell</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
														 	<label for="location" class="col-sm-4 col-form-label">
														 	<a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">
														 		Curr Code
														 	</a>
														 	</label>
														 	<div class="col-sm-8">
														 		<input id="currency_code" class="form-control" placeholder="Currency Code" name="currency_code" onkeyup="this.value = this.value.toUpperCase();">
														 	</div>
														</div>
													</div>	
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
														 	<label for="location" class="col-sm-4 col-form-label">
														 	<a onclick="GetModalDetails('customer');" data-toggle="modal" data-target="#get_modal_details" href="">
														 		Cust Code
														 	</a>
														 	</label>
														 	<div class="col-sm-8">
														 		<input id="cust_code" class="form-control" placeholder="Customer Code" name="cust_code" onkeyup="this.value = this.value.toUpperCase();">
														 	</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-lg-6 col-sm-12 mb-1">
												<div class="row">
													<div class="col-lg-12 col-sm-12 mb-1">
														<div class="row">
															<div class="col-lg-2 col-sm-4 mb-1">
																<label class="checkbox checkbox-primary">
																		<input class="form-control" type="checkbox" name="dealstatus[]" value="Open" checked="checked">
																	<span>Open</span>
																	<span class="checkmark"></span>
																</label>
															</div>
															<div class="col-lg-3 col-sm-4 mb-1">
																<label class="checkbox checkbox-primary">
																		<input class="form-control" type="checkbox" name="dealstatus[]" value="Completed">
																	<span>Completed</span>
																	<span class="checkmark"></span>
																</label>
															</div>
															<div class="col-lg-2 col-sm-4 mb-1">
																<label class="checkbox checkbox-primary">
																		<input class="form-control" type="checkbox" name="dealstatus[]" value="Partial" formcontrolname="radio" checked="checked">
																	<span>Partial</span>
																	<span class="checkmark"></span>
																</label>
															</div>
															<div class="col-lg-3 col-sm-4 mb-1">
																<label class="checkbox checkbox-primary">
																		<input class="form-control" type="checkbox" name="dealstatus[]" value="Cancelled" formcontrolname="radio" >
																	<span>Cancelled</span>
																	<span class="checkmark"></span>
																</label>
															</div>
															<div class="col-lg-2 col-sm-4 mb-1">
																<label class="checkbox checkbox-primary">
																		<input class="form-control" type="checkbox" name="dealstatus[]" value="Closed" formcontrolname="checkbox" >
																	<span>Closed</span>
																	<span class="checkmark"></span>
																</label>
															</div>
														</div>
													</div>
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
														 	<label for="from_date" class="col-sm-6  col-form-label">From date</label>
														 	<div class="col-sm-6">
														 		<input type="text" id="from_date" class="form-control" placeholder="From Data" name="from_date">
														 	</div>
														</div>
													</div>
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
														 	<label for="to_date" class="col-sm-6  col-form-label">To date</label>
														 	<div class="col-sm-6">
														 		<input type="text"  id="to_date" class="form-control" placeholder="To Data" name="to_date">
														 	</div>
														</div>
													</div>
												</div>
												</div>
											</div>
											
										<div class="row mt-3">
											<div class="col-lg-7 col-sm-12">
											</div>
											<div class="col-lg-5 col-sm-12 text-right">
												<a href="{{route('reports.ttbookingreport')}}" target="_blank">
													<button class="btn btn-primary m-1" type="button" id="clear">Booking Report
													</button>
												</a>
	 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
												<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF">
												<a href="{{route('deallist')}}">
													<button class="btn btn-primary m-1" type="button" id="clear">Clear
													</button>
												</a>
											</div>
										</div>
					            	</form>
					            </div>
					        </div>
					    </div>
					    <div class="table-responsive">
					    	 <table id="table_dealtransactionlist" class="display nowrap  table-striped table-bordered table" style="width:100%">
                            </table>	
					    </div>

						<div class="row mt-3">
							<div class="col-lg-7 col-sm-12"></div>
							<div class="col-lg-5 col-sm-12 text-right" style="margin-left: -13px;">
								<form action="{{ route('deallistexport')}}" method="post" id="select_options" onsubmit="return selectoption()">
									{{ csrf_field() }}
									<input type="hidden" name="selectid" id="selectid">
									<input type="hidden" name="from_date" id="select_from_date">
									<input type="hidden" name="to_date" id="select_to_date">
									<button type="submit" class="btn btn-primary" id="select_print">Print</button> 
									<button class="btn btn-primary m-1" type="button" id="select_close" >Close</button>
									<button class="btn btn-primary m-1" type="button" id="select_cancel" >Cancel</button>
								</form>
								</a>
							</div>
						</div>

					</div>	
				</div>
			</div>
		</div>	
	@include('modal.customer')	
	@include('modal.currency')
	@include('modal.dealedit')
@endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/modal.details.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
	<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){
    	$('#edit_famount').mask("000,000,000.00", {reverse: true});

		$('#bbuy').html(0);
		$('#bsell').html(0);
	    $(document).on("click", "#select_close",function() {
	    	closeandcancel('close');
	    });
	    $(document).on("click", "#select_cancel",function() {
	    	closeandcancel('cancel');
	    });

	    /*var date = new Date();
	    var mnt = date.getMonth()+1;
	    var cdate = date.getDate()+'-'+mnt+'-'+date.getFullYear();
	    $('#from_date, #to_date').val(cdate);*/

	    $("#from_date, #to_date").inputmask("dd-mm-yyyy", {
	        separator: "-",
	        alias: "dd-mm-yyyy",
	        placeholder: "dd-mm-yyyy"
	    });

	    function closeandcancel(type){
		    var favorite = [];
		    $.each($("#deallistcheckbox:checked"), function(){            
		        favorite.push($(this).val());
		    });
		    if (favorite.length === 0) {		    
		    } else{
		        $.ajax({
		            url:"{{ route('dealliststatusupdate') }}",
		            type: "POST",
		            headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		            data : {
		                type : type,
		                id   : favorite,
		            },
		            success: function(response){
		                if(response.status == 200){
		                    // window.location.reload(true);
		                    submitForm('submit');
		                } else {
		                	submitForm('submit');
		                }
		            },      
		        });
		    }
	    }
	    


	    $(document).on("click", "input[name='customer_id_detail']",function() {
	        $('#get_modal_details').modal('toggle');
	        $('input[name="cust_code"]').val($(this).val());
	    });
	    $(document).on("click", "input[name='currency_id_detail']",function() {
	        $('#get_currency_modal_details').modal('toggle');
	        var val = $(this).val();
	        var valarray = val.split('|');
	        $('input[name="currency_code"]').val(valarray[0]);
	        $( "#currency_code" ).trigger( "blur" );
    	});
    	$(document).on("click", "#edit_deallistform",function() {
    		dealupdate();
    	});

	    $(document).on("click", ".edit_deal",function() {
	    	var edit_id = $(this).attr('data-ref');
	    	if(edit_id != ''){
	    		 $.ajax({
		           type:'GET',
		           url:'./deallist/edit/'+edit_id,
		           success:function(result){
			           	if(result.status == 200){
			           		data = result.data;
				           	$('input[name="edit_id"]').val(data.TranNo);
				           	$('input[name="dealno"]').val(data.DealNo);
				           	$('input[name="validtilldate"]').val(data.ValidTill);
				           	$('input[name="customercode"]').val(data.CustomerCode);
				           	$('input[name="customername"]').val(data.CustomerName);
				           	$('input[name="contact"]').val(data.contact);
				           	$('input[name="rate"]').val(data.Rate);
				           	$('input[name="amount"]').val(data.FAmount);
				           	$('input[name="currencycode"]').val(data.CurrencyCode);
				           	$('input[name="currencyname"]').val(data.CurrencyName);
				           	$('#edit_trantype').html(data.TranType);
				           	$('input[name="edit_remark"]').val(data.Remarks);
				           	$('input[name="edit_rate"]').val(data.Rate);
				           	$('input[name="edit_famount"]').val(data.FAmount); 
				           	$('input[name="edit_validtilldate"]').val(data.ValidTill);
				           	if(data.TranType == 'Buy'){
				           		$("#edit_trantype").addClass("buy");
				           		$("#edit_trantype").removeClass("sell");
				           	}else{
				           		$("#edit_trantype").addClass("sell");
				           		$("#edit_trantype").removeClass("buy");
				           	}
			           	}
	           		$('#get_deal_list').modal('toggle');
		           }
		        });

	    	}
	    });

		var location = 	$('#location').val();
		var from_date = 	$('#from_date').val();
		var to_date = 	$('#to_date').val();
		var formdata = $('#deallistform').serialize();
		var formData =serializeToJSON(formdata);
		var dealstatus = $("input[name='dealstatus[]']:checked").map(function () {
				    return this.value; // $(this).val()
				}).get();
	    $('#table_dealtransactionlist').DataTable({
            paging:   false,
            destroy: true,
            searching : false,
            autoWidth : false,
            info:     false,
            ordering: false,
	        ajax: {
                url:"{{ route('deallist') }}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {'formdata':formData,'dealstatus': dealstatus },

            },
             columns: [
                    {data: 'DealNo', name: 'DealNo',title:'Deal No'},
                    {data: 'DealDate', name: 'DealDate',title:'Deal Date'},
                    {data: 'TranType', name: 'TranType',title:'Tran Type'},
                    {data: 'CustomerCode', name: 'CustomerCode',title:'Customer'},
                    {data: 'CurrencyCode', name: 'CurrencyCode',title:'Curr. Code'},
                    {data: 'FAmount', name: 'FAmount',title:'FAmount',class: 'text-right'},
                    {data: 'Rate', name: 'Rate',title:'Rate',class: 'text-right'},
                    {data: 'ValidTill', name: 'ValidTill',title:'Valid Till'},
                    {data: 'Status', name: 'Status',title:'Status'},
                    {data: 'Select', name: 'Select',title:'Select'},
                   
                ],
	    });

	});

	const serializeToJSON = str => 
	str.split('&')
	.map(x => x.split('='))
	.reduce((acc, [key, value]) => ({
	  ...acc,
	  [key]: isNaN(value) ? value : ''
	}), {});

	function submitForm(type) {

		
		var formdata = $('#deallistform').serialize();
		var formData =serializeToJSON(formdata);
		var location = 	formData.location;
		var dealstatus ={};
		var dealstatus = $("input[name='dealstatus[]']:checked").map(function () {
				    return this.value; // $(this).val()
				}).get();
		if(location == ''){
			$('#location').css('border','1px solid #ff7d7d');
			return false;
		} else {
			 $('#table_dealtransactionlist').DataTable({
	            paging:   false,
	            destroy: true,
	            searching : false,
	            autoWidth : false,
	            info:     false,
	            ordering: false,
		        ajax: {
	                url:"{{ route('deallist') }}",
	                type: "POST",
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },
	                data: {'formdata':formData,'dealstatus': dealstatus },
	            },
	             columns: [
	                    {data: 'DealNo', name: 'DealNo',title:'Deal No'},
	                    {data: 'DealDate', name: 'DealDate',title:'Deal Date'},
	                    {data: 'TranType', name: 'TranType',title:'Tran Type'},
	                    {data: 'CustomerCode', name: 'CustomerCode',title:'Customer'},
	                    {data: 'CurrencyCode', name: 'CurrencyCode',title:'Curr. Code'},
                    	{data: 'FAmount', name: 'FAmount',title:'FAmount',class: 'text-right'},
	                    {data: 'Rate', name: 'Rate',title:'Rate'},
	                    {data: 'ValidTill', name: 'ValidTill',title:'Valid Till'},
	                    {data: 'Status', name: 'Status',title:'Status'},
	                    {data: 'Select', name: 'Select',title:'Select'},
	                   
	                ],
		    });
		}
	}	

	function selectoption(){
		var favorite = [];
		$.each($("#deallistcheckbox:checked"), function(){            
		    favorite.push($(this).val());
		});
		$('#selectid').val(favorite);
		$('#select_from_date').val($('#from_date').val());
		$('#select_to_date').val($('#to_date').val());
		if(favorite.length === 0){
			return false;
		}
	}


	function dealupdate() {
        $.ajax({
            url:"{{ route('deallistupdate')}}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : $('#deallisteditform').serialize(),
            success: function(response){
                if(response.status == 200){
                	$("#check_details").trigger("click");
                    success_toastr(response.data);
                	$('#get_deal_list').modal('toggle');
                } else {
                	danger_toastr(response.data);
                }
            },      
        });
	}

function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}

function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
$( function() {
	$( "#from_date, #to_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection