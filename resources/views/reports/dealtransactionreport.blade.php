@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
.badge{
	font-size: 100%;
}
#to_date_root .picker__holder {
   right: 30px;
}
#valid_to_root .picker__holder {
   right: 30px;
}

</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Deal Transaction Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

					    <div class="col-lg-12 mb-12">
					        <div class="card">
					            <div class="card-body">
					            	<form action="{{route('dealtransactionreportexport')}}" method="POST" id="dealtransactionreportform">
					            		@csrf
										<div class="row">
											<div class="col-lg-6 col-sm-12 mb-1">
												<div class="row  form-group">
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class=" row">
														 	<label for="location" class="col-sm-4 col-form-label">
														 	<a onclick="GetModalDetails('customer');" data-toggle="modal" data-target="#get_modal_details" href="">
														 		Cust Code
														 	</a>
														 	</label>
														 	<div class="col-sm-8">
														 		<input id="cust_code" class="form-control" placeholder="Customer Code" name="cust_code" onkeyup="this.value = this.value.toUpperCase();">
														 	</div>
														</div>
													</div>
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
														 	<label for="cust_name" class="col-sm-4 col-form-label">
														 		Cust Name
														 	</label>
														 	<div class="col-sm-8">
														 		<input id="cust_name" class="form-control" placeholder="Customer Name" name="cust_name" readonly="">
														 	</div>
														</div>
													</div>
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
														 	<label for="currency_code" class="col-sm-4 col-form-label">
														 	<a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">
														 		Curr Code
														 	</a>
														 	</label>
														 	<div class="col-sm-8">
														 		<input id="currency_code" class="form-control" placeholder="Currency Code" name="currency_code" onkeyup="this.value = this.value.toUpperCase();">
														 	</div>
														</div>
													</div>	
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
														 	<label for="currency_name" class="col-sm-4 col-form-label">
														 		Curr Name
														 	</label>
														 	<div class="col-sm-8">
														 		<input id="currency_name" class="form-control" placeholder="Currency Name" name="currency_name" readonly>
														 	</div>
														</div>
													</div>

													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
														 	<label for="location" class="col-sm-4 col-form-label">
														 		User
														 	</label>
														 	<div class="col-sm-8">
														 		<input id="user" class="form-control" placeholder="User" name="user" >
														 	</div>
														</div>
													</div>	
													<div class="col-lg-6 col-sm-12 mb-1">
														<div class="row">
														 	<label for="user_name" class="col-sm-4 col-form-label">
														 		User Name
														 	</label>
														 	<div class="col-sm-8">
														 		<input id="user_name" class="form-control" placeholder="User Name" name="user_name" readonly>
														 	</div>
														</div>
													</div>

												</div>
											</div>
											
											<div class="col-lg-6 col-sm-12 mb-1">
												<div class="row">
													<div class="col-lg-12 col-sm-12 mb-1">
														<div class="row form-group">

															<div class="col-lg-6 col-sm-12 mb-1">
																<div class="row">
																 	<label for="location" class="col-sm-6  col-form-label">From Date</label>
																 	<div class="col-sm-6">
																 		<input type="text" id="from_date" class="form-control" placeholder="From Date" name="from_date" value="{{$fromDate}}">
																 	</div>
																</div>
															</div>

															<div class="col-lg-6 col-sm-12 mb-1">
																<div class="row">
																 	<label for="location" class="col-sm-6  col-form-label">To Date</label>
																 	<div class="col-sm-6">
																 		<input type="text" id="to_date" class="form-control" placeholder="To Date" name="to_date" value="{{$toDate}}">
																 	</div>
																</div>
															</div>

															<div class="col-lg-6 col-sm-12 mb-1">
																<div class="row">
																 	<label for="location" class="col-sm-6  col-form-label">Status</label>
																 	<div class="col-sm-6">
																 		<select id="status" class="form-control" name="status">
																 			<option value="">-Select-</option>
																 			<option value="Open">Open</option>
																 			<option value="Completed">Completed</option>
																 			<option value="Partial">Partial</option>
																 			<option value="Closed">Closed</option>
																 			<option value="Canceled">Canceled</option>
																 		</select>
																 	</div>
																</div>
															</div>

															<div class="col-lg-6 col-sm-12 mb-1">
																<div class="row">
																 	<label for="location" class="col-sm-6  col-form-label">Tran Type</label>
																 	<div class="col-sm-6">
																 		<select id="trantype" class="form-control" name="trantype">
																 			<option value="">Select Type</option>
																 			<option value="Buy">Buy</option>
																 			<option value="Sell">Sell</option>
																 		</select>
																 	</div>
																</div>
															</div>

															<div class="col-lg-6 col-sm-12 mb-1">
																<div class="row">
																 	<label for="valid_from" class="col-sm-6  col-form-label">Valid From</label>
																 	<div class="col-sm-6">
																 		<input type="text" id="valid_from" class="form-control" placeholder="Valid From" name="valid_from">
																 	</div>
																</div>
															</div>

															<div class="col-lg-6 col-sm-12 mb-1">
																<div class="row">
																 	<label for="valid_to" class="col-sm-6  col-form-label">Valid To</label>
																 	<div class="col-sm-6">
																 		<input type="text" id="valid_to" class="form-control" placeholder="Valid To" name="valid_to">
																 	</div>
																</div>
															</div>															
														</div>
													</div>													
												</div>
												</div>
											</div>
											
										<div class="row mt-3">
											<div class="col-lg-7 col-sm-12">
											</div>
											<div class="col-lg-5 col-sm-12 text-right">
	 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
												<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF">
												<a href="{{route('dealtransactionreport')}}"><button class="btn btn-primary m-1" type="button">Clear</button></a> 
											</div>
										</div>
					            	</form>
					            </div>
					        </div>
					    </div>
					    <div class="table-responsive">
					    	 <table id="table_dealtransactionlist" class="display nowrap  table-striped table-bordered table" style="width:100%">
                            </table>	
					    </div>
					</div>	
				</div>
			</div>
		</div>	
	@include('modal.customer')	
	@include('modal.currency')

@endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/modal.details.js')}}"></script>
	<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){
		submitForm('submit');
	    $('#from_date, #to_date, #valid_from, #valid_to').inputmask("dd-mm-yyyy", {
	        separator: "-",
	        alias: "dd-mm-yyyy",
	        placeholder: "dd-mm-yyyy"
	    });

		$('#bbuy').html(0);
		$('#bsell').html(0);
	    $(document).on("click", "input[name='customer_id_detail']",function() {
	        $('#get_modal_details').modal('toggle');
	        $('input[name="cust_code"]').val($(this).val()); 
	        $('input[name="cust_name"]').val($(this).attr('data-ref')); 
	    });
	    $(document).on("click", "input[name='currency_id_detail']",function() {
	        $('#get_currency_modal_details').modal('toggle');
	        var val = $(this).val();
	        var valarray = val.split('|');
	        $('input[name="currency_code"]').val(valarray[0]);
	        $('input[name="currency_name"]').val($(this).attr('data-ref'));
	        $( "#currency_code" ).trigger( "blur" );
    	});

    	$(document).on("blur", "input[name='cust_code']",function() {
	        var cval = $(this).val();
	        $.ajax({
	        		headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                  url:"{{ route('getcustomerdetails') }}",
                  method:"POST",
                  data:{code:cval},
                  success:function(cdata){
                  	if(cdata.CustName != undefined){
	        			$('input[name="cust_name"]').val(cdata.CustName);
                  	}else{
                  		$.alert('Customer detail not found');
                  	}
                  }
            });
    	});
    	$(document).on("blur", "input[name='currency_code']",function() {
	        var cval = $(this).val();
	        $.ajax({
	        	 headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },
	              url:"{{ route('getcurrencydetails') }}",
	              method:"POST",
	              data:{currencycode:cval},
	              success:function(cdata){
	              	if(cdata.CurrencyCode != undefined){
	        			$('input[name="currency_name"]').val(cdata.CurrencyName);
	              	}else{
                  		$.alert('Currency Code not founded');
                  	}
	              }
	        });
    	});
	    $('#table_dealtransactionlist').DataTable({
            paging:   false,
            destroy: true,
            searching : false,
            autoWidth : false,
            info:     false,
            ordering: false,
             columns: [
                    {data: 'DealNo', name: 'DealNo',title:'Tran No'},
                    {data: 'TranType', name: 'TranType',title:'Tran Type'},
                    {data: 'Customer', name: 'Customer',title:'Customer'},
                    {data: 'Currency', name: 'Currency',title:'Currency'},
                    {data: 'FAmount', name: 'FAmount',title:'FAmount'},
                    {data: 'Rate', name: 'Rate',title:'Rate'},
                    {data: 'LAmount', name: 'LAmount',title:'LAmount'},
                    {data: 'ValidTill', name: 'ValidTill',title:'Valid Till'},
                    {data: 'Remarks', name: 'Remarks',title:'Remarks'},
                    {data: 'Status', name: 'Status',title:'Status'},
                    {data: 'RealisedAmount', name: 'RealisedAmount',title:'Realised Amount'},
                    {data: 'Balance', name: 'Balance',title:'Balance'},
                    {data: 'Date', name: 'Date',title:'Date'},
                    {data: 'User', name: 'User',title:'User'},
                   
                ],
	    });
	});

	

	/*$( "#dealtransactionreportform" ).submit(function( event ) {
		var formData =serializeToJSON($("#dealtransactionreportform").serialize());
		var cust_code = formData.cust_code;
		var currency_code = formData.currency_code;
		var from_date = formData.from_date;
		var to_date = formData.to_date;
	 // 	if(cust_code=='') {
		// 	$('input[name="cust_code"]').css('border','1px solid #d52323');
		// 	return false;
		// }else{
		// 	$('input[name="cust_code"]').css('border','');
		// 	return true;
		// }
	  event.preventDefault();
	});*/


	function submitForm(type) {

		var formData =serializeToJSON($("#dealtransactionreportform").serialize());
		var location 	= formData.location;
		var code 		= formData.cust_code;
		var currcode 	= formData.currency_code;
		var fdate 		= formData.from_date;
		var tdate 		= formData.to_date;
		var trantype 	= formData.trantype;
		// if(code == ''){
		// 	// $('input[name="cust_code"]').css('border','1px solid #ff7d7d');
		// 	return false;
		// } else 
		// if(formData){
			$('input[name="cust_code"]').css('border','1px solid #ced4db');
			$('input[name="currency_code"]').css('border','1px solid #ced4db');
            $('#table_dealtransactionlist').DataTable({
                paging:   false,
                destroy: true,
                searching : false,
                autoWidth : false,
                info:     false,
                ordering: false,
                ajax: {
                    url:"{{ route('dealtransactionreport') }}",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data:formData,
                },
                columns: [       
                    {data: 'DealNo', name: 'DealNo',title:'Tran No'},
                    {data: 'TranType', name: 'TranType',title:'Tran Type'},
                    {data: 'Customer', name: 'Customer',title:'Customer'},
                    {data: 'Currency', name: 'Currency',title:'Currency'},
                    {data: 'FAmount', name: 'FAmount',title:'FAmount', className: "text-right"},
                    {data: 'Rate', name: 'Rate',title:'Rate', className: "text-right"},
                    {data: 'LAmount', name: 'LAmount',title:'LAmount', className: "text-right"},
                    {data: 'ValidTill', name: 'ValidTill',title:'Valid Till'},
                    {data: 'Remarks', name: 'Remarks',title:'Remarks'},
                    {data: 'Status', name: 'Status',title:'Status'},
                    {data: 'RealisedAmount', name: 'RealisedAmount',title:'Realised Amount', className: "text-right"},
                    {data: 'Balance', name: 'Balance',title:'Balance', className: "text-right"},
                    {data: 'Date', name: 'Date',title:'Date'},
                    {data: 'User', name: 'User',title:'User'},

                ],
            });			
		}
	// }


const serializeToJSON = str => 
str.split('&')
.map(x => x.split('='))
.reduce((acc, [key, value]) => ({
  ...acc,
  [key]: isNaN(value) ? value : ''
}), {});
$( function() {
	$( "#from_date, #to_date, #valid_from, #valid_to").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection