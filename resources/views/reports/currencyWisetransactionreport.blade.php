@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
#to_date_root .picker__holder {
    right: 30px;
}
.badge{
	font-size: 100%;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Currency wise Transaction Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

					    <div class="col-lg-12 mb-12">
					        <!-- <div class="card"> -->
					            <!-- <div class="card-body"> -->
					            	<form action="{{ route('reports.currencyWiseTransactionexport')}}" method="POST" id="ledgercurrencyfilterform">
					            		@csrf
										<div class="row">

											{{-- <div class="col-lg-3 col-sm-6 mb-1">
												<div class="row">
												 	<label for="location" class="col-sm-4 col-form-label text-left">
												 	<a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">
												 		Curr Code
												 	</a>
												 	</label>
												 	<div class="col-sm-8">
												 		<input id="currency_code" class="form-control" placeholder="Currency Code" name="currency_code" onkeyup="this.value = this.value.toUpperCase();">
												 	</div>
												</div>
											</div>	 --}}									
											
											<div class="col-lg-6 col-sm-6 mb-1">
												<div class="row">
												 	<label for="location" class="col-sm-4 col-form-label text-left">
												 	<a onclick="GetModalDetails('customer');" data-toggle="modal" data-target="#get_modal_details" href="">
												 		Customer Code
												 	</a>
												 	</label>
												 	<div class="col-sm-8">
												 		<input id="cust_code" class="form-control" placeholder="Customer Code" name="cust_code" onkeyup="this.value = this.value.toUpperCase();">
												 	</div>
												</div>
											</div>
											<div class="col-lg-3 col-sm-6 mb-1">
												<div class="row">
												 	<label for="location" class="col-sm-6  col-form-label text-right">From date</label>
												 	<div class="col-sm-6">
												 		<input type="text" id="from_date" class="form-control" placeholder="From Data" name="from_date">
												 	</div>
												</div>
											</div>
											<div class="col-lg-3 col-sm-6 mb-1">
												<div class="row">
												 	<label for="location" class="col-sm-6  col-form-label text-right">To data</label>
												 	<div class="col-sm-6">
												 		<input type="text" id="to_date" class="form-control" placeholder="To Data" name="to_date">
												 	</div>
												</div>
											</div>
										</div>

										<div class="row mt-3">
											<div class="col-lg-7 col-sm-12">
											</div>
											<div class="col-lg-5 col-sm-12 text-right">
	 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
												<input  class="btn btn-primary m-1" type="submit" name="export"  value="PDF">

												<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV">
											</div>
										</div>
					            	</form>
					            <!-- </div> -->
					        <!-- </div> -->
					    </div>
					    
				    <div class="table-responsive">
				    	 <table id="table_ledgercustomer" class="display nowrap  table-striped table-bordered table" style="width:100%">

                         </table>	
				    </div>
					</div>	
				</div>
			</div>
		</div>	
	@include('modal.customer')	
	@include('modal.currency')
@endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/modal.details.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){
		
	    var date = new Date();
	    var mnt = date.getMonth()+1;
	    var cdate = date.getDate()+'-'+mnt+'-'+date.getFullYear();
	    $('#from_date, #to_date').val(cdate);

		$("#from_date , #to_date").inputmask("dd-mm-yyyy", {
			separator: "-",
			alias: "dd-mm-yyyy",
			placeholder: "dd-mm-yyyy"
		});
	    $(document).on("click", "input[name='customer_id_detail']",function() {
	        $('#get_modal_details').modal('toggle');
	        $('input[name="cust_code"]').val($(this).val());
	    });
	    $(document).on("click", "input[name='currency_id_detail']",function() {
	        $('#get_currency_modal_details').modal('toggle');
	        var val = $(this).val();
	        var valarray = val.split('|');
	        $('input[name="currency_code"]').val(valarray[0]);
	        $( "#currency_code" ).trigger( "blur" );
    	});
    	var fdate = $('input[name="from_date"]').val();
		var tdate = $('input[name="to_date"]').val();
	    $('#table_ledgercustomer').DataTable({
            paging:   false,
            destroy: true,
            searching : false,
            autoWidth : false,
            info:     false,
            ordering: false,
            columns: [
                    {data: 'CurrencyCode', name: 'CurrencyCode',title: "Currency Code"},
                    {data: 'CurrName', name: 'CurrName',title: "Currency"},
                    {data: 'CustCode', name: 'CustCode',title: "CustCode"},
                    {data: 'CustName', name: 'CustName',title: "Cust Name"},
                    {data: 'Tbuy', name: 'Tbuy',className: "text-right",title: "Buy"},
                    {data: 'Tsell', name: 'Tsell',className: "text-right",title: "Sell"},
                ],
            
	    });
	});

	function submitForm(type) {
		var cust_code = 	$('input[name="cust_code"]').val();
		var fdate = $('input[name="from_date"]').val();
		var tdate = $('input[name="to_date"]').val();
		
			if(cust_code == ''){
				$('input[name="cust_code"]').css('border','1px solid #d52323');
				return false;
			}else{
				$('input[name="cust_code"]').css('border','');
				var oTable =$('#table_ledgercustomer').DataTable({
	                paging:   false,
	                destroy: true,
	                searching : false,
	                autoWidth : false,
	                info:     false,
	                serverSide: true,
	                ordering: false,
	                ajax: {
	                    url:"{{ route('reports.currencyWiseTransactionReport') }}",
	                    type: "POST",
	                    headers: {
	                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                    },
	                    data: function (d) {
							d.cust_code=cust_code;
							d.fdate=fdate;
							d.tdate=tdate;
	                    },
	                },
	                columns: [
	                    {data: 'CurrencyCode', name: 'CurrencyCode',title: "Currency Code"},
	                    {data: 'CurrName', name: 'CurrName',title: "Currency"},
	                    {data: 'CustCode', name: 'CustCode',title: "CustCode"},
	                    {data: 'CustName', name: 'CustName',title: "Cust Name"},
	                    {data: 'Tbuy', name: 'Tbuy',className: "text-right",title: "Buy"},
	                    {data: 'Tsell', name: 'Tsell',className: "text-right",title: "Sell"},
	                ]                
	            });
			}
            

		}
	
	$( "#ledgercurrencyfilterform" ).submit(function( event ) {
		var cust_code = 	$('input[name="cust_code"]').val();
	 	if(cust_code=='') {
			$('input[name="cust_code"]').css('border','1px solid #d52323');
			return false;
		}else{
			$('input[name="cust_code"]').css('border','');

			return true;
		}
	  event.preventDefault();
	});

$( function() {
	$( "#from_date, #to_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection