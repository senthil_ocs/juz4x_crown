@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
.borderSummary {
	border-right: 1px solid #646769;
}
.borderDate {
	border-left: 1px solid #646769;
}
.textHeaderCenter th{
	text-align: center;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>TT Transaction Summary</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="col-lg-12 mb-12">
							<div class="card">
								<div class="card-body">
									<form action="{{route('reports.tttransactionsummary')}}" method="POST" id="">
					            		@csrf
									<div class="row">
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
												<label for="from_date" class="col-sm-4  col-form-label">From Date</label>
												<div class="col-sm-7 text-left">
													<input type="text" name="from_date" id="from_date" class="form-control" value="{{$fromDate}}">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4 mb-1">
											<div class="row">
												<label for="to_date" class="col-sm-4  col-form-label">To Date</label>
												<div class="col-sm-7 text-left">
													<input type="text" name="to_date" id="to_date" class="form-control" value="{{$toDate}}">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-4">
 											<button class="btn btn-primary m-1" type="submit" id="check_details" >Submit</button>
											<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-lg-12" style="margin-top: 50px;">
	                        <div class="table-responsive">
	                            <table id="table_reporttranssummary" class="table table-striped" style="width:100%">
	                                <thead>
	                                    <tr class="textHeaderCenter">
	                                    	<th rowspan="2" class="borderSummary " colspan="1">Date</th>
	                                        <th colspan="3" class="borderSummary">Customer</th>
	                                        <th colspan="3" class="borderSummary">Agent</th>
	                                        <th colspan="3" >Profit</th>
	                                    </tr>
	                                    <tr class="textHeaderCenter"> 
	                                    	<th>Amount</th>
	                                    	<th>Commission</th>
	                                    	<th class="borderSummary">Total</th>
	                                    	<th>Amount</th>
	                                    	<th>Commission</th>
	                                    	<th class="borderSummary">Total</th>
	                                    	<th>Profit Amount</th>
	                                    	<th>Profit Commission</th>
	                                    	<th>Profit Total</th>
	                                    </tr>
	                                </thead>
									@if(count($datasSummary) > 0)
		                               	@foreach($datasSummary as $key => $val)
											@php 
												$totalAmount = $val->customerAmount - $val->agentAmount;
												$commission = $val->customerComm - $val->agentComm;
												$totalProfit = $val->customerTotalAmount - $val->agentTotal;
											@endphp
			                                <tbody>
			                                	<tr>
			                                		<td class="borderSummary">{{date("d-m-Y", strtotime($val->TranDate))}}</td>
			                                		<td class="text-right">{{number_format($val->customerAmount,2)}}</td>
			                                		<td class="text-right">{{number_format($val->customerComm,2)}}</td>
			                                		<td class="text-right borderSummary">{{number_format($val->customerTotalAmount,2)}}</td>
			                                		<td class="text-right">{{number_format($val->agentAmount,2)}}</td>
			                                		<td class="text-right">{{number_format($val->agentComm,2)}}</td>
			                                		<td class="text-right borderSummary">{{number_format($val->agentTotal,2)}}</td>
			                                		<td class="text-right">{{number_format($totalAmount,2)}}</td>
			                                		<td class="text-right">{{number_format($commission,2)}}</td>
			                                		<td class="text-right ">{{number_format($totalProfit,2)}}</td>
			                                	</tr>
			                                </tbody>
										@endforeach	
			                        @else 
			                        	<tbody><tr><td colspan="10">Not Found Data!</td></tr></tbody>
			                        @endif 
	                            </table>    
	                        </div>
	                    </div>
					</div>	
				</div>
			</div>
		</div>	
    @endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
    <script src="{{asset('assets/js/ladda.script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="{{asset('assets/js/modal.details.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')
@if(session()->has('message.level') && session('message.level')=="success")                           
	<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
	</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
	<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){

		$("#from_date , #to_date").inputmask("dd-mm-yyyy", {
			separator: "-",
			alias: "dd-mm-yyyy",
			placeholder: "dd-mm-yyyy"
		});
	});
	
$( function() {
	$( "#from_date, #to_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>

@endsection