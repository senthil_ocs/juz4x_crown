@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>TT Funds Accepted</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

						<div class="col-lg-12 mb-12">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('reports.customerexport')}}" method="POST" id="">
					            		@csrf
									<div class="row mb-4">
										<div class="col-lg-4 col-sm-6 mb-1">
											<div class="row">
												<label for="from_date" class="col-sm-4  col-form-label">From Date</label>
												<div class="col-sm-8 text-left">
													<input type="text" name="from_date" id="from_date" class="form-control">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-6 mb-1">
											<div class="row">
												<label for="to_date" class="col-sm-4 col-form-label">To Date</label>
												<div class="col-sm-8 text-left">
													<input type="text" name="to_date" id="to_date" class="form-control" >
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-sm-12">
											<div class="row">
												<label for="tttype" class="col-sm-4 col-form-label">TT Type</label>
												<div class="col-sm-8 text-left">
													<select class="form-control">
														<option>Select Option</option>
													</select>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-2 col-sm-12">
											<div class="row">
												<label class="radio radio-primary mx-3">
													<input class="form-control" type="radio" name="summary" formcontrolname="radio" checked="checked">
													<span>Summary</span>
													<span class="checkmark"></span>
												</label>
											</div>
										</div>
										<div class="col-lg-2 col-sm-12">
											<div class="row">
												<label class="radio radio-primary">
													<input class="form-control" type="radio" name="summary" formcontrolname="radio">
													<span>Detail</span>
													<span class="checkmark"></span>
												</label>
											</div>
										</div>
										<div class="col-lg-2 col-sm-12">
											<div class="row">
												<label class="radio radio-primary mx-3">
													<input class="form-control" type="radio" name="singapore" formcontrolname="radio" checked="checked">
													<span>In Singapore</span>
													<span class="checkmark"></span>
												</label>
											</div>
										</div>
										<div class="col-lg-2 col-sm-12">
											<div class="row">
												<label class="radio radio-primary">
													<input class="form-control" type="radio" name="singapore" formcontrolname="radio">
													<span>Out Singapore</span>
													<span class="checkmark"></span>
												</label>
											</div>
										</div>

										<div class="col-lg-4 col-sm-12 text-right">
 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
											<input  class="btn btn-primary m-1" type="submit" name="export" value="PDF">
											<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV">
										</div>

									</div>
									</form>
								</div>
							</div>
						</div>

                        <div class="table-responsive">
                            <table id="table_reportcustomer" class="display nowrap table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Trans No</th>
                                        <th>Trans Date</th>
                                        <th>Currency Code</th>
                                        <th>SGD Amount</th>
                                        <th>Comm</th>
                                        <th>T.Amount</th>
                                    </tr>
                                </thead>
                            </table>    
                        </div>

					</div>	
				</div>
			</div>
		</div>	
    @endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
    <script src="{{asset('assets/js/ladda.script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="{{asset('assets/js/modal.details.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')
@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
$(document).ready(function(){
	getdatatable();
	$('input[name="codeactive"]').click(function(){
	    if($(this).prop("checked") == true){
	       $('input[name="codeactive"]').val('active');
	    }
	    else if($(this).prop("checked") == false){
	        $('input[name="codeactive"]').val('inactive');
	    }
	});

    var date = new Date();
    var cdate = date.getDate()+'-'+date.getMonth()+'-'+date.getFullYear();
    $('#from_date, #to_date').val(cdate);

	$("#from_date , #to_date").inputmask("dd-mm-yyyy", {
		separator: "-",
		alias: "dd-mm-yyyy",
		placeholder: "dd-mm-yyyy"
	});
});

function submitForm(type) {
	getdatatable()
}

function getdatatable() {
	var location = 	$('#location').val();
	var active =  $('input[name="codeactive"]').val();
	console.log(active);
	$('#table_reportcustomer').DataTable({
		paging:   false,
		destroy: true,
		serverSide: true,
		autoWidth : false,
		info:     false,
		searching:false,
		ajax: {
			url: "./customer",
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			 data: function (d) {
			 	d.location=location;
			 	d.active=active;
			 },
		},
		columns: [
			{data: 'DT_RowIndex', name: 'DT_RowIndex'},
			{data: 'Custcode', name: 'Custcode'},
			{data: 'CustName', name: 'CustName'},
			{data: 'customer.Phone', name: 'customer.Phone'},
			{data: 'customer.Email', name: 'customer.Email'},
			{data: 'customer.DOB', name: 'customer.DOB'},
			{data: 'customer.DOB', name: 'customer.DOB'},
		],
	});
}
$( function() {
	$( "#from_date, #to_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection