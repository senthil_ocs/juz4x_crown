@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
.badge{
	font-size: 100%;
}
#to_date_root .picker__holder {
   right: 30px;
}
#from_date_root .picker__holder {
   right: 30px;
}

</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>DD/TT Unpost Search</h1>
			<ul>
			    <li>Listing</li>
			    <li><a href="{{route('ddtransaction.unpost')}}">Create</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
					    <div class="col-lg-12 mb-12">
					        <div class="card">
					            <div class="card-body">
					            	<form action="{{ route('dealtransactionreportexport')}}" method="POST" id="ddtransactionform">
					            		@csrf
										<div class="form-group row">
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Receipt No</label>
										            <div class="col-sm-6">
										            <input type="text" name="beneficiary_id" id="beneficiary_id" placeholder="Beneficiary Id" autocomplete="off" class="form-control">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label"><a onclick="GetCashCustomerModal('cashcustomer');" data-toggle="modal" data-target="#get_cashcustomer_modal_details" href="">Cust Code</a></label>
										            <div class="col-sm-6">
										                <input id="cust_code" class="form-control" placeholder="Customer Code" name="cust_code" onkeyup="this.value = this.value.toUpperCase();">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Cust Name</label>
										            <div class="col-sm-6">
										                <input id="cust_code" class="form-control" placeholder="Customer Name" name="cust_name">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">NRIC/FIN/PP</label>
										            <div class="col-sm-6">
										                <input type="text" name="CustNRICNO" id="CustNRICNO" placeholder="CustNRICNO" class="form-control custNRICdis" autocomplete="off">
										            </div>
										        </div>
										    </div>
										</div>
										<div class="form-group row">
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Comp Name</label>
										            <div class="col-sm-6">
										            <input type="text" name="compname" id="compname" autocomplete="off" class="form-control">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">From Date</label>
										            <div class="col-sm-6">
										                <input type="text" id="from_date" class="form-control" placeholder="From Date" name="from_date">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">To Date</label>
										            <div class="col-sm-6">
										                <input type="text" id="to_date" class="form-control" placeholder="To Date" name="to_date">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Ben Name</label>
										            <div class="col-sm-6">
										                <input type="text" name="benname" id="benname" placeholder="Ben Name" class="form-control custNRICdis" autocomplete="off">
										            </div>
										        </div>
										    </div>
										</div>
										<div class="form-group row">
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Swift Code</label>
										            <div class="col-sm-6">
										            <input type="text" name="swiftcode" id="swiftcode" placeholder="Swift Code" autocomplete="off" class="form-control" >
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">AC No</label>
										            <div class="col-sm-6">
										                <input id="acno" class="form-control" placeholder="Ac No" name="acno">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">FAmount</label>
										            <div class="col-sm-6">
										                <input id="famount" class="form-control" placeholder="FAmount" name="famount">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label"><a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">Currency</a></label>
										            <div class="col-sm-6">
										                <input id="currency_code" class="form-control" placeholder="Currency Code" name="currency_code" onkeyup="this.value = this.value.toUpperCase();" value="" >
										            </div>
										        </div>
										    </div>
										</div>
										<div class="form-group row">
										    <div class="col-lg-3">
										        <div class="row">
										        	<input type="text" id="customer_code" value="CASH" hidden>
										            <label class="col-sm-4 col-lg-4 col-form-label"><a onclick="GetAgentDetails('agent');" data-toggle="modal" data-target="#get_agent_modal" href="">Agent</a></label>
										            <div class="col-sm-6">
										            <input type="text" name="agent" id="agent" placeholder="Agent" autocomplete="off" class="form-control" >
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-4">
										        <div class="row">
										            <label class="col-sm-4 col-lg-2 col-form-label">In / Out</label>
										            <div class="col-sm-5">
										                <input id="InPut" class="form-control" placeholder="InPut" name="InPut">
										            </div>
										            <div class="col-sm-5">
										                <input id="OutPut" class="form-control" placeholder="OutPut" name="OutPut">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-4">
										        <div class="row">
										            <label class="col-sm-4 col-lg-2 col-form-label">Staff</label>
										            <div class="col-sm-5">
														<select class="form-control" name="staff" id="staff">
															<option>All Staff</option>
										                </select>
										            </div>
										            <div class="col-sm-5">
										                <button class="btn btn-primary m-1" type="button" id="">Today</button>
										            </div>
										        </div>
										    </div>
										</div>
										<div class="row mt-3">
											<div class="col-lg-6 col-sm-12">
											</div>
											<div class="col-lg-6 col-sm-12 text-right">
													<button class="btn btn-primary m-1" type="button" id="convertPost">Convert Post</button>
													<button class="btn btn-primary m-1" type="button" id="">Clear Serach</button>
													<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Search</button>
													<button class="btn btn-primary m-1" type="button" id="">Export</button>
											</div>
										</div>
					            	</form>
					            </div>
					        </div>
					    </div>
					    <div class="table-responsive">
					    	 <table id="table_ddtransactionlist" class="display nowrap  table-striped table-bordered table" style="width:100%">
					    	 	<thead>
					    	 	<tr>
					    	 		<th>id</th>
					    	 		<th>Tran Date</th>
					    	 		<th>Customer</th>
					    	 		<th>Bank</th>
					    	 		<th>Currency</th>
					    	 		<th>Exchange Rate</th>
					    	 		<th>FAmount</th>
					    	 		<th>LAmount</th>
					    	 		<th>Comm</th>
					    	 		<th>Total Amount</th>
					    	 		<th>Paid Status</th>
					    	 		<th><label class="checkbox checkbox-primary"><input class="form-control" type="checkbox" id="selectAll" name="selectAll"><span class="checkmark"></span></label></th>
					    	 	</tr>
					    	 	</thead>
                            </table>	
					    </div>
					</div>	
				</div>
			</div>
		</div>	
		<div id="convertPostModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <h4 class="modal-title">Convert To Post</h4>
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				    </div>

				    <div class="col-md-12 mb-12 master-main-sec" style="margin-top: 10px;">
			            <div class="card">
			                <h5 class="m-2">Agent Details</h5>
			                <div class="card-body">
			                    <div class="row">
			                       <div class="col-lg-12 col-sm-12">
						            	<form  class="needs-validation" method="post" novalidate action="#" id="convertToPost" name="convertToPost">
						            		{{csrf_field()}}
						            		<input class=" form-control" type="text" name="customer_code" id="customer_code" value="CASH" hidden>
						            		<input class=" form-control" type="text" name="convertId" id="convertId" hidden>
						            		<input class=" form-control" type="text" name="amount" id="amount" hidden>
											<div class="form-group row">
                                                <label for="agentcode" class="col-sm-5 col-form-label">
                                                    <a onclick="GetAgentDetails('agent');" data-toggle="modal" data-target="#get_agent_modal" href="">Agent Code</a> <span class="mandatory">*</span>
                                                </label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="agentcode" autocomplete="off" id="agentcode" placeholder="Please Enter Agent Code" required>
                                                    <div class="invalid-feedback"> 
                                                        Please Enter Agent Code
                                                    </div>
                                                </div>
                                           </div>
                                           <div class="form-group row">
                                               <label for="agentname" class="col-sm-5 col-form-label text-rights">Agent Name</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="agentname" autocomplete="off" id="agentname" placeholder="Please Enter Agent Name">
                                                </div>
                                           </div>
                                           <div class="form-group row">
                                               <label for="agentrate" class="col-sm-5 col-form-label text-rights">Agent Rate</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="agentrate" autocomplete="off" id="agentrate" placeholder="0.00" required>
                                                    <div class="invalid-feedback"> 
                                                        Please Enter Agent Rate
                                                    </div>
                                                </div>
                                           </div>
                                           <div class="form-group row">
                                               <label for="agentcomm" class="col-sm-5 col-form-label text-rights">Agent Comm</label>
                                               <div class="col-sm-7">
                                                    <input class=" form-control" type="textbox" name="agentcomm" autocomplete="off" id="agentcomm" placeholder="0.00">
                                                </div>
                                           </div>
                        					<button type="button" class="btn btn-primary m-1" style="float:right;" name="convertMultiPost" id="convertMultiPost">Convert Post</button>
										</form>
									</div>
								</div>
							</div>
						</div>
				    </div>
			    </div>
		  	</div>
		</div>
	@include('modal.customer')	
	@include('modal.currency')
	@include('modal.unpostview')
	@include('modal.agent')
	@include('modal.cashcustomer')


@endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/modal.details.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
	<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){

		$('#selectAll').on('click',function() {
			if ($('#selectAll').is(":checked")) {
				$(".checkboxgroup").prop('checked', true);
			} else {
				$(".checkboxgroup").prop('checked', false);
			}
		});

		$('#convertPost').on('click', function(){
			var unpostChecked = [], unpostCurrency = '', count = 0, uniqueCur = 0;
       		$(':checkbox:checked').each(function(i){
       			if($(this).attr('id') != 'selectAll'){
					unpostChecked[i] = $(this).val();
					unpostCurrency = $(this).attr('data-ref');
	       			$(':checkbox:checked').each(function(i){
						if(unpostCurrency != $(this).attr('data-ref') && $(this).attr('id') != 'selectAll'){
							uniqueCur++;
						}
					});
					count++;
				}
			});
			if(count>0 && uniqueCur == 0 ){
				$('#agentcode').val('');
				$('#agentname').val('');
				$('#agentrate').val('');
				$('#agentcomm').val('');
				$('#convertPostModal').modal('toggle');
			} else {
				if(uniqueCur > 0){
					$.alert("Please Select list with similar Currency");
				}
				else {
					$.alert("Please Select Unpost");
				}
			}
		});

		$('#convertMultiPost').on('click',function() {
			var code = $('#agentcode').val();
			var name = $('#agentname').val();
			var rate = $('#agentrate').val();
			if(code == '' || name == '' || rate == ''){
	            $('#convertToPost').addClass('was-validated');
	            return false;
        	}
			var unpostChecked = [];
	   		$(':checkbox:checked').each(function(i){
				unpostChecked[i] = $(this).val();
				$('#convertId').val(unpostChecked[i]);
				if(unpostChecked[i] > 0) {
					$.ajax({
				        url : "{{ route('ddtransaction.convertToPost') }}",
				        type: "POST",
				        async: false,
				        headers: {
				            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				        },
				        data :  $('#convertToPost').serialize(),
				        success: function(response){		            
				        }      
				    });
				}
			});
            $('#convertToPost').removeClass('was-validated');
			$('#convertPostModal').modal('toggle');	
			setTimeout(function(){ getdatas(); }, 300);

		});

	    var date = new Date();
	    var mnt = date.getMonth()+1;
	    var cdate = date.getDate()+'/'+mnt+'/'+date.getFullYear();
	    $('#from_date, #to_date').val(cdate);

	    $('#from_date, #to_date').inputmask("dd/mm/yyyy", {
	        separator: "/",
	        alias: "dd/mm/yyyy",
	        placeholder: "dd/mm/yyyy"
	    });
	    $(document).on("click", "input[name='agent_id_detail']",function() {
	        $('#get_agent_modal').modal('toggle');
	        var datas = $(this).val().split("|");
	        $('input[name="agent"]').val(datas[0]);
		});
	    $(document).on("click", "input[name='cashcustomer_id_detail']",function() {
	        $('#get_cashcustomer_modal_details').modal('toggle');
	        $('input[name="CustNRICNO"]').val($(this).val()); 
	        $('input[name="cust_code"]').val($(this).attr('data-ref')); 
	        $('input[name="cust_name"]').val($(this).attr('data-name')); 
	    });

	    $(document).on("click", ".viewUnpost",function() {
	    	var val = $(this).attr('data-ref');
	        $.ajax({
		        url : "{{route('ddtransaction.getddtransaction')}}",
		        type: "POST",
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        },
		        data : {
		            val : val,
		        },
		        beforeSend: function() {},
		        success: function(response){
		            if(response.status == 200){
		            	console.log(response);
                		$('input[name="v_transno"]').val(response.data.id);
                		$('input[name="v_custname"]').val(response.data.CustName);
                		$('input[name="v_benename"]').val(response.data.BenName);
                		$('input[name="v_curcode"]').val(response.data.CurrencyCode);
                		$('input[name="v_exchrate"]').val(response.data.ExchRate);
                		$('input[name="v_famount"]').val(response.data.FAmount);
                		$('input[name="v_sgd"]').val(response.data.LAmount);
                		$('input[name="v_tot"]').val(response.data.TotalAmount);
                		if(response.data.PaidStatus == 'Y'){
                			$('input[name="v_status"]').val("Yes");
                		} else {
                			$('input[name="v_status"]').val("No");
                		}
                		$('input[name="v_mode"]').val(response.data.PayMode);
                		$('input[name="v_paiddate"]').val(response.data.PaymentDate);
                		$('input[name="v_paidby"]').val(response.data.PaidBy);
                		$('input[name="v_loccode"]').val(response.data.PaymentLocationCode);
                		$('input[name="v_tercode"]').val(response.data.PaymentTerminalCode);
			        }
			    }
            });
	    });

	    $(document).on("click", "input[name='currency_id_detail']",function() {
	        $('#get_currency_modal_details').modal('toggle');
	        var val = $(this).val();
	        var valarray = val.split('|');
	        $('input[name="currency_code"]').val(valarray[0]);
	        $('input[name="currency_name"]').val($(this).attr('data-ref'));
	        $( "#currency_code" ).trigger( "blur" );
    	});

    	$(document).on("blur", "input[name='cust_code']",function() {
	        var cval = $(this).val();
	        $.ajax({
	        		headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                  url:"{{ route('getcustomerdetails') }}",
                  method:"POST",
                  data:{code:cval},
                  success:function(cdata){
                  	if(cdata.CustName != undefined){
	        			$('input[name="cust_name"]').val(cdata.CustName);
                  	}else{
                  		$.alert('Customer detail not found');
                  	}
                  }
            });
    	});

    	$(document).on("blur", "input[name='currency_code']",function() {
	        var cval = $(this).val();
	        $.ajax({
	        	 headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },
	              url:"{{ route('getcurrencydetails') }}",
	              method:"POST",
	              data:{currencycode:cval},
	              success:function(cdata){
	              	if(cdata.CurrencyCode != undefined){
	        			$('input[name="currency_name"]').val(cdata.CurrencyName);
	              	}else{
                  		$.alert('Currency Code not founded');
                  	}
	              }
	        });
    	});   	
		setTimeout(function(){ getdatas(); }, 300);
	});

	$("#agentcode").blur(function(){
        if($(this).val() != ''){
            getagentcode($(this).val());
        }
    });

    $(document).on("click", "input[name='agent_id_detail']",function() {
        $('#get_agent_modal').modal('toggle');
        var datas = $(this).val().split("|");
        $('input[name="agentcode"]').val(datas[0]);
        getagentcode(datas[0]);
    });

    function getagentcode(code)
	{
	    var ccode = $('input[name="customer_code"]').val();
	    $.ajax({
	        url : "{{ route('ddtransaction.getagentcode') }}",
	        type: "POST",
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        },
	        data : {
	            code : code,
	            ccode : ccode,
	        },
	        success: function(response){
	            if(response.status == 200){
	                $('input[name="agentname"]').val(response.data.CustName);
	                $('input[name="agentrate"]').val('0.0000000');
	            } else {
	                $('input[name="agentname"]').val('');
	                $('input[name="agentrate"]').val('');
	                $.alert({
	                    title: 'Agent Code Not Found!',
	                    content: 'Agent Code Not Found!',
	                });
	            }
	        },      
	    });
	}

	function submitForm(type) {	
		getdatas();
	}

	function getdatas() {
		$('#table_ddtransactionlist').DataTable({
		    paging    : false,
		    destroy   : true,
		    searching : false,
		    autoWidth : false,
		    info      : false,
		    order     : [[ 0, "desc" ]],
		    aoColumnDefs: [{
		    	bSortable: false,
			    aTargets: [ -1 ]}],
		    ajax: {
		            url:"{{ route('ddtransaction.unpostsearchlist') }}",
		            type: "POST",
		            headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
					data: function (d) {
						d.cust_code = $('input[name="cust_code"]').val();
						d.currency_code = $('input[name="currency_code"]').val();
						d.bank_name = $('input[name="bank_name"]').val();
						d.bank_code = $('input[name="bank_code"]').val();
						d.from_date = $('input[name="from_date"]').val();
						d.to_date = $('input[name="to_date"]').val();
						d.swiftcode = $('input[name="swiftcode"]').val();
						d.acno = $('input[name="acno"]').val();
						d.benname = $('input[name="benname"]').val();
						d.CustNRICNO = $('input[name="CustNRICNO"]').val();
						/*d.tranno = $('input[name="beneficiary_id"]').val();*/
					},
		        },
			columns: [
			    {data: 'id', name: 'id'},
			    {data: 'TranDate', name: 'TranDate'},
			    {data: 'CustCode', name: 'CustCode'},
			    {data: 'BenBank', name: 'BenBank'},
			    {data: 'CurrencyCode', name: 'CurrencyCode'},
			    {data: 'ExchRate', name: 'ExchRate', class:"text-right"},
			    {data: 'FAmount', name: 'FAmount', class:"text-right"},
			    {data: 'LAmount', name: 'LAmount', class:"text-right"},
			    {data: 'Comm', name: 'Comm'},
			    {data: 'TotalAmount', name: 'TotalAmount', class:"text-right"},
			    {data: 'PaidStatus', name: 'PaidStatus'},
			    {data: 'Select', name: 'Select'},
			],
		});
	}
$( function() {
	$( "#from_date, #to_date").datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
@endsection