@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
.badge{
	font-size: 100%;
}
#to_date_root .picker__holder {
   right: 30px;
}
#from_date_root .picker__holder {
   right: 30px;
}

</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>DD/TT Purpose</h1>
			<ul>
			    <li>create</li>
			    <li>Listing</li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

					    <div class="col-lg-12 mb-12">
					        <div class="card">
					            <div class="card-body">
					            	<form action="{{ route('dealtransactionreportexport')}}" method="POST" id="purpose_form">
					            		@csrf
										<div class="row">
											<div class="col-lg-6	 col-sm-12 mb-1">
												<div class="row  form-group">
													<div class="col-lg-6 col-sm-6 mb-1">
														<div class=" row">
														 	<label for="location" class="col-sm-4 col-form-label">
														 		Name <span class="mandatory">*</span>
														 	</label>
														 	<div class="col-sm-8">
														 		<input id="purpose_name" class="form-control" placeholder="Enter Name" name="purpose_name" required>
														 		<div class="invalid-feedback error">
																    Please enter Name
																</div>
														 	</div>
														</div>
													</div>
													<div class="col-lg-6 col-sm-6 mb-1">
													<div class="row">
													 	<label for="currency_code" class="col-sm-4 col-form-label">
													 		Status
													 	</label>
													 	<div class="col-sm-8">
														    <input type="hidden" name="id" id="p_id">
													 		<select class="form-control" name="status" id="status">
													 			<option value="1">Active</option>
													 			<option value="0">In Active</option>
													 		</select>
													 	</div>
													</div>
												 </div>
											  </div>
										   </div>
										</div>

										<div class="row mt-3">
											<div class="col-lg-6 col-sm-12">
											</div>
											<div class="col-lg-6 col-sm-12 text-right">
												<button class="btn btn-primary m-1" type="button" onclick="cancelPurpose();">Cancel</button>
	 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
											</div>
										</div>
					            	</form>
					            </div>
					        </div>
					    </div>
					    <div class="table-responsive">
					    	 <table id="table_purposelist" class="display nowrap  table-striped table-bordered table" style="width:100%">
                            </table>	
					    </div>
					</div>	
				</div>
			</div>
		</div>	
	@include('modal.customer')	
	@include('modal.currency')

@endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/modal.details.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')

<script type="text/javascript">

	$(document).ready(function(){
		var type = 'load';
		setTimeout(function(){ getdatas(type); }, 300);
	});

	function submitForm(type) {
		var id     = $("#p_id").val();
		var type   = '';
		id!='' ? type='Update' : type='Add';
		var name   = $("#purpose_name").val();
		var status = $("#status").val();

		if(name==''){
	        $('#purpose_name').css('border-color','#ff0b0b');
	        $('.error').css('display', 'block');
		}else{
	        $('#purpose_name').css('border-color','#ced4da');
	        $('.error').css('display', 'none');
		}

		if(name !='' && status !=''){
			$.ajax({
	            url:"{{ route('ddtransaction.purpose_insert') }}",
	            type: "POST",
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	            	data: {id:id, type:type, name:name, status:status
	            },
	            success:function(data){
	            	$("#p_id").val('');
	            	$("#purpose_name").val('');
	            	$("#status").val('1');
	            	if(data.type=='Add'){
	            		success_toastr(data.message);
	            	}
	            	if(data.type=='Delete'){
	            		danger_toastr(data.message);
	            	}
	   				getdatas();
	            },
	       	});
		}
	}
	function cancelPurpose(){
		$("#purpose_name").val('');
		$("#status").val('1');
		$("#p_id").val('');
		return true;
	}
	$(document).on("click", ".edit_purpose",function() {
		var id   = $(this).attr("data-purposeid");
		if(id!=''){
			$.ajax({
	            url:"{{ route('ddtransaction.getpurposedata') }}",
	            type: "POST",
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	            data: {id:id} ,
	            success:function(response){
	            	var data = response.data;
	            	$("#p_id").val(data.id);
	            	$("#purpose_name").val(data.Name);
	            	$("#status").val(data.Status).trigger('selected',true);
	            },
	       	});
		}
	});
	$(document).on("click", ".delete_purpose",function() {
		var id   = $(this).attr("data-purposeid");
		if(id!=''){
			if(confirm("Are you sure Delete ?")){

			$.ajax({
	            url:"{{ route('ddtransaction.deletepurposedata') }}",
	            type: "POST",
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	            data: {id:id } ,
	            success:function(data){
	            	getdatas();
	            	success_toastr(data.message);
	            },
	       	});
			}
		}
	});
	function getdatas(type) {
		$('#table_purposelist').DataTable({
		    paging    : false,
		    destroy   : true,
		    searching : false,
		    autoWidth : false,
		    info      : false,
		    ordering  : true,
		    ajax: {
		            url:"{{ route('ddtransaction.purpose') }}",
		            type: "POST",
		            headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            }, 
		        },
			columns: [
			    {data: 'DT_RowIndex', name: 'DT_RowIndex',title:'Sno'},
			    {data: 'Name', name: 'Name',title:'Name'},
			    {data: 'Status', name: 'Status',title:'Status'},
			    {data: 'Created_At', name: 'Created_At',title:'Created At'},
			    {data: 'Action', name: 'Action',title:'Action'},
			],
		});
	}
	function success_toastr(msg) {
	    toastr.success(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}
	function danger_toastr(msg) {
	    toastr.error(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}
</script>
@endsection