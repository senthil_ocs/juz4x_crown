@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<style type="text/css">
    .label-denomination{
        border: 1px solid #dee2e6;
        border-radius: 1px;
        background: #f2f2f2;
    }
</style>
@endsection
@section('main-content')
<div class="breadcrumb">
    <h1>APT DD/TT Transaction</h1>
    <ul>
        <li>Create</li>
        <li><a href="{{route('ddtransaction.searchlist')}}">List</a></li>
    </ul>
</div>

<div class="separator-breadcrumb border-top">
    
</div>
<form id="frm_ddtransaction" name="frm_ddtransaction" class="needs-validation" method="post" novalidate action="#">
{{csrf_field()}}
    <div class="row mb-12 master-main">

        <div class="col-md-12 mb-12 master-main-sec">
            <div class="card text-left">
                <div class="card-body">
                   <div class="row">
                       <div class="col-lg-6 col-sm-12">
                           <div class="row m-1">
                                <label for="ref_no" class="col-sm-2 col-form-label text-rights">RefNo</label>
                                <input type="textbox" name="ref_no"  class="col-sm-2 form-control allownumeric" id="ref_no" value="{{$dis_TranNo}}" placeholder="Enter Ref No">

                                <label for="ddno" class="col-sm-2 col-form-label text-right">DDNo</label>
                                <input type="textbox" name="ddno"  class="col-sm-2 form-control allownumeric" id="ddno" placeholder="Enter DD No">

                                <label for="rpt_no" class="col-sm-2 col-form-label text-right">RptNo</label>
                                <input type="textbox" name="rpt_no"  class="col-sm-2 form-control allownumeric" id="rpt_no" placeholder="Enter Rpt No">
                           </div>
                       </div>
                        <div class="col-lg-6 col-sm-12">
                          <div class="row">
                            <div class="col-md-3 text-right">
                                <button class="btn btn-primary m-1" type="button" id="check_details">Check Details</button>
                            </div>
                            <div class="col-md-3 text-right">
                                <button class="btn btn-primary m-1" type="button" id="check_details">ACCUITY SEARCH</button>
                            </div>
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary m-1" type="button" id="accuity_search">BEN ACCUITY SEARCH</button>
                            </div>
                          </div>
                        </div>
                   </div> 
                </div>
            </div>        
        </div>

        <div class="col-md-12 mb-12 master-main-sec">
            <div class="card text-center">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="row m-1">
                                <label for="app_date" class="col-sm-6 col-form-label text-rights">App Date</label>
                                <input id="app_date" class="col-sm-6 form-control" placeholder="dd-mm-yyyy" name="app_date">
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 text-right">
                                <button class="btn btn-primary m-1" type="button" id="check_details">View Transaction</button>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="row m-1">
                                <label for="agent_date" class="col-sm-6 col-form-label text-rights">Agent Date</label>
                                <input id="agent_date" class="col-sm-6 form-control" placeholder="dd-mm-yyyy" name="agent_date">
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="row m-1">
                                <label for="payment" class="col-sm-6 col-form-label text-rights">Payment</label>
                                <input type="textbox" name="payment"  class="col-sm-6 form-control allownumeric" id="payment" placeholder="Enter Payment">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-6">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="company_name" class="col-sm-3 col-form-label">
                                <a onclick="GetModalDetails('customer');" data-toggle="modal" data-target="#get_modal_details" href="">Customer Code</a> <span class="mandatory">*</span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="customer_code" class="form-control" autocomplete="off" id="customer_code" placeholder="Enter Customer code" onkeyup="this.value = this.value.toUpperCase();" required >
                                    <div class="invalid-feedback">Enter Customer code</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="customer_name" class="form-control" autocomplete="off" id="customer_name" required >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pno_nric" class="col-sm-3 col-form-label">P.No / Nric <span class="mandatory">*</span></label>
                                <div class="col-sm-9 input-group mb-3">
                                    <input type="textbox" name="pno_nric" class="form-control" autocomplete="off" id="pno_nric" placeholder="Enter P.No / Nric" required >

                                    <div class="input-group-append" id="pno_nric_id" onclick="GetCashCustomerModal('cashcustomer');"data-toggle="modal" data-target="#get_cashcustomer_modal_details" style="cursor: pointer;">
                                        <span class="input-group-text" id="basic-addon2">
                                            <i class="i-Business-Mens"></i>
                                        </span>
                                    </div>

                                    <div class="invalid-feedback">Enter P.No / Nric</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="dob-datepicker" class="col-sm-3 col-form-label">Birth Date</label>
                                <div class="col-sm-9">
                                    <input id="dob-datepicker" class="form-control" placeholder="dd-mm-yyyy" name="dob">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="nationality" class="col-sm-3 col-form-label">Nationality</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="nationality" id="nationality">
                                        <option>Select Nationality</option>
                                        @foreach($nationality as $value)
                                        <option value="{{trim($value->Code,' ')}}">{{trim($value->Code,' ')}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                   <?php /* <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="contacts" class="col-sm-3 col-form-label">Contacts <span class="mandatory">*</span></label>
                                <div class="col-sm-9 input-group mb-3">
                                    <input type="textbox" name="contacts" class="form-control" autocomplete="off" id="contacts" placeholder="Enter Contacts" required >
                                    <div class="input-group-append" id="cconact_id"  onclick="GetCustomerContactaModal('customercontacts');" data-toggle="modal" data-target="#get_customercontacts_modal_details" style="cursor: pointer;">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="i-Business-Mens"></i>
                                        </span>
                                    </div>
                                    <div class="invalid-feedback">Enter Contacts</div>
                                </div>
                            </div>
                        </div>
                    </div> */ ?>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="nationality" class="col-sm-3 col-form-label">Purpose <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="Purpose" id="Purpose" required>
                                        <option value="">Select Purpose</option>
                                        @foreach($purpose as $value)
                                        <option value="{{$value->Name}}">{{$value->Name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Select Purpose</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-6">                            
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="address1" class="col-sm-3 col-form-label">Address1 <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="address1" class="form-control" autocomplete="off" id="address1" placeholder="Enter Address1" required >
                                    <div class="invalid-feedback">Enter Address1</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="address2" class="col-sm-3 col-form-label">Address2 <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="address2" class="form-control" autocomplete="off" id="address2" placeholder="Enter Address2" required >
                                    <div class="invalid-feedback">Enter Address2</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label class="col-sm-3 col-form-label">Country <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="country" class="form-control" autocomplete="off" id="country" placeholder="Enter Country" required >
                                    <div class="invalid-feedback">Enter Country</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label class="col-sm-3 col-form-label">Postal Code <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="postal_code" class="form-control" autocomplete="off" id="postal_code" placeholder="Enter Postal Code" required >
                                    <div class="invalid-feedback">Postal Code</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label class="col-sm-3 col-form-label">Phone No <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="phone_no" class="form-control" autocomplete="off" id="phone_no" placeholder="Enter Phone No" required >
                                    <div class="invalid-feedback">Enter Phone No</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label class="col-sm-3 col-form-label">Customer Type 
                                </label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="customer_type" id="customer_type">
                                        <option value="">Select Customer Type</option>
                                        @foreach($customertype as $value)
                                        <option value="{{$value->Name}}">{{$value->Name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12 mb-12 master-main-sec">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                       <div class="col-lg-7 col-sm-12">
                           <div class="row">
                               <div class="col-sm-6 mb-6">
                                   <div class="form-group row">
                                       <label for="ref_no" class="col-sm-4 col-form-label text-rights"><a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">Currency</a> <span class="mandatory">*</span></label>
                                       <div class="col-sm-7">
                                        <input type="textbox" class="form-control" name="code" class="form-control" autocomplete="off" id="code" placeholder="Enter Currency Code" list="json-datalist"  onkeyup="this.value = this.value.toUpperCase();" required>
                                        <div class="invalid-feedback"> 
                                            Please Enter Currency
                                        </div>
                                       </div>
                                   </div>
                                   <div class="form-group row">
                                       <label for="ex_rate" class="col-sm-4 col-form-label text-rights">Ex Rate <span class="mandatory">*</span></label>
                                       <div class="col-sm-7">
                                            <input class=" form-control allownumeric" type="textbox" name="ex_rate" autocomplete="off" id="ex_rate" placeholder="0.0000000000" required>
                                            <div class="invalid-feedback"> 
                                                Please Enter Ex Rate
                                            </div>
                                        </div>
                                   </div>
                                   <div class="form-group row">
                                       <label for="amount" class="col-sm-4 col-form-label text-rights">Amount <span class="mandatory">*</span></label>
                                       <div class="col-sm-7">
                                            <input class=" form-control allownumeric" type="textbox" name="amount" autocomplete="off" id="amount" placeholder="0.00" required>
                                            <div class="invalid-feedback"> 
                                                Please Enter Amount
                                            </div>
                                        </div>
                                   </div>
                               </div>
                               <div class="col-sm-6 mb-6">
                                <div class="form-group row">
                                    <input type="textbox" name="curr_name" class="col-sm-12 form-control" id="curr_name" placeholder="Enter Currency Name" autocomplete="off">
                                   </div>
                                   <div class="form-group row">
                                       <label for="conv_rate" class="col-sm-4 col-form-label text-rights">Conv.Rate <span class="mandatory">*</span></label>
                                       <input class="col-sm-8 form-control allownumeric" type="textbox" name="conv_rate" autocomplete="off" id="conv_rate" placeholder="0.0000000000" required>
                                        <div class="invalid-feedback text-center"> 
                                            Please Enter Conv Rate
                                        </div>
                                   </div>

                               </div>
                           </div>
                       </div>
                       <div class="col-lg-5 col-sm-12">
                           <div class="table-responsive">
                               <table id="ct_bal" class="display nowrap table table-striped table-bordered locationcurrencylist_new_table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-right">Cur.BALANCE</th>
                                        <th class="text-right">TT.BALANCE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-right">0.00</td>
                                        <td class="text-right">0.00</td>
                                    </tr>
                                </tbody>
                               </table>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-6">
                <h4>Beneficiary Information</h4>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_nric" class="col-sm-2 col-form-label">PP / Nric</label>
                                <div class="col-sm-10 input-group mb-3">
                                    <input type="textbox" name="pp_nric" class="form-control" id="pp_nric" placeholder="Enter PP / Nric" autocomplete="off">

                                    <div class="input-group-append" id="pp_nric" onclick="GetBeneficiaryModal('beneficiary');"data-toggle="modal" data-target="#get_beneficiary_modal_details" style="cursor: pointer;">
                                        <span class="input-group-text" id="basic-addon2">
                                            <i class="i-Business-Mens"></i>
                                        </span>
                                    </div>

                                    <div class="invalid-feedback">
                                        Please Enter PP / Nric
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="textbox" name="pp_name" class="form-control" id="pp_name" placeholder="Enter Name" autocomplete="off" >
                                <!--<div class="invalid-feedback">
                                        Please Enter Name
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_address1" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-5">
                                    <input type="textbox" name="pp_address1" class="form-control" id="pp_address1" placeholder="Enter Address1" autocomplete="off">
                                </div>
                                <div class="col-sm-5">
                                    <input type="textbox" name="pp_address2" class="form-control" id="pp_address2" placeholder="Enter Address2" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_country" class="col-sm-2 col-form-label">Country</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="pp_country" id="pp_country">
                                        <option value="">Select Country</option>
                                        @foreach($country as $value)
                                        <option value="{{trim($value->Country, ' ')}}">{{trim($value->Country, ' ')}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-6">
                <h4>Beneficiary Bank Details </h4>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="bank" class="col-sm-3 col-form-label">Bank</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="bank" class="form-control" id="bank" placeholder="Enter Bank" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="branch_address" class="col-sm-3 col-form-label">Branch/Address</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="branch_address" class="form-control" id="branch_address" placeholder="Enter Name" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="ac_no" class="col-sm-3 col-form-label">Account No</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="ac_no" class="form-control" id="ac_no" placeholder="Account No" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="swift_code" class="col-sm-3 col-form-label">Swift Code</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="swift_code" class="form-control" id="swift_code" placeholder="Enter Address" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-6">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="dd_bank" class="col-sm-3 col-form-label">Bank</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="dd_bank" class="form-control" autocomplete="off" id="dd_bank" placeholder="Enter Bank">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
       </div>

        <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-6">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="charges" class="col-sm-3 col-form-label label-denomination">Charges</label>
                                <div class="col-sm-9">
                                    <label for="charges" class="col-sm-12 col-form-label text-center label-denomination">Customer</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="sgd" class="col-sm-3 col-form-label">SGD</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="sgd" class="form-control text-right" autocomplete="off" id="sgd" placeholder="0.00">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="comm" class="col-sm-3 col-form-label">Comm.</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="comm" class="form-control text-right" autocomplete="off" id="comm" placeholder="0.00">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="ocharge" class="col-sm-3 col-form-label">O.Charge</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="ocharge" class="form-control text-right" autocomplete="off" id="ocharge" placeholder="0.00">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="scharge" class="col-sm-3 col-form-label">S.Charge</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="scharge" class="form-control text-right" autocomplete="off" id="scharge" placeholder="0.00">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pcharge" class="col-sm-3 col-form-label">P.Charge</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="pcharge" class="form-control text-right" autocomplete="off" id="pcharge" placeholder="0.00">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="total" class="col-sm-3 col-form-label">Total</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="total" class="form-control text-right" autocomplete="off" id="total" placeholder="0.00">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 


        <div class="col-md-12 mb-12 master-main-sec">
            <div class="card text-center">
                <div class="card-body">
                   <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn  btn-primary m-1" type="button" id="update_btn" onclick="submitForm('update');">Update</button>
                        <button class="btn  btn-primary m-1" type="button">Agent Advice</button>
                        <button class="btn  btn-primary m-1" type="button">Reprint (F10)</button>
                        <button class="btn  btn-primary m-1" type="button" onclick="submitForm('save');" id="save_btn">Save (F4)</button>
                        <button class="btn  btn-primary m-1" type="button" onclick="submitForm('clear');">Clear (F2)</button>
                        <button class="btn  btn-primary m-1" type="button">Delete (F6)</button>
                        <button class="btn  btn-primary m-1" type="button">Close (Esc)</button>
                    </div>
                   </div> 
                </div>
            </div>        
        </div>

    </div>
</form>
@include('modal.customer')
@include('modal.customercontact')
@include('modal.cashcustomer')
@include('modal.currency')
@include('modal.beneficiary')
@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
    <script src="{{asset('assets/js/ladda.script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script src="{{asset('assets/js/modal.details.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
$(document).ready(function(){

    $('input[name="customer_code"]').val("CASH");
    $('input[name="customer_name"]').val("CASH CUSTOMER");
    if($("input[name='customer_code']").val() != 'CASH'){
        $('#pno_nric_id').hide();
        $('#cconact_id').show();
        $('input[name="contacts"]').prop('readonly', false);
        $('input[name="pno_nric"]').prop('readonly', true);
        $('input[name="customer_name"]').val("");
    } else {
        $('#pno_nric_id').show()
        $('#cconact_id').hide();
        $('input[name="contacts"]').prop('readonly', true);
        $('input[name="pno_nric"]').prop('readonly', false);
        $('input[name="customer_name"]').val("CASH CUSTOMER");
    }
    $('#update_btn').prop('disabled', true);
    var date = new Date();
    var cdate = date.getDate()+'-'+date.getMonth()+'-'+date.getFullYear();
    $('#app_date').val(cdate);
    $('#agent_date').val(cdate);
    var dformat = "{{ env('DATE_FORMAT') }}";
    $("#app_date, #agent_date, #dob-datepicker").inputmask("dd-mm-yyyy", {
        separator: "-",
        alias: "dd-mm-yyyy",
        placeholder: "dd-mm-yyyy"
    });

    $(document).on("click", "input[name='customer_id_detail']",function() {
        $('#get_modal_details').modal('toggle');
        $('input[name="customer_code"]').val($(this).val());
        setTimeout(function(){ 
            $("input[name='customer_code']").trigger('blur');
        }, 300);
    });
    $("input[name='customer_code']").blur(function() {
        getCustomerDetails();
    });
    $(document).on("click", "input[name='cashcustomer_id_detail']",function() {
        $('#get_cashcustomer_modal_details').modal('toggle');
        $('input[name="pno_nric"]').val($(this).val());
        setTimeout(function(){ 
            getCashCustomerDetails();
        },300);
    });
    $("input[name='pno_nric']").blur(function() {
        getCashCustomerDetails();
    });
    $(document).on("click", "input[name='ccontact_id_detail']",function() {
       $('#get_customercontacts_modal_details').modal('toggle');
        var val = $(this).val();
        var valarray = val.split('|');
        $('input[name="contacts"]').val(valarray[0]);
        $('input[name="contacts"]').attr('readonly', true);
        $('input[name="remarks"]').val(valarray[1]);
        $('input[name="country"]').val(valarray[3]);
        $('input[name="country"]').attr('readonly', true);
    });
    $(document).on("click", "input[name='currency_id_detail']",function() {
        $('#get_currency_modal_details').modal('toggle');
        var val = $(this).val();
        var valarray = val.split('|');
        $('input[name="code"]').val(valarray[0]);
        $( "#code" ).trigger( "blur" );
    });

    $(document).on("click", "input[name='beneficiary_id_detail']",function() {
        $('#get_beneficiary_modal_details').modal('toggle');
        var val = $(this).val();
        var valarray = val.split('|');
        $('input[name="pp_nric"]').val(valarray[1]);
        setTimeout(function(){ 
            getbeneficiaryDetails();
        },300);
    });

    $("input[name='pp_nric']").blur(function() {
        getbeneficiaryDetails();
    });
    $("input[name='ref_no']").blur(function() {
        var val = $(this).val();
        getddtransactionDetails(val);        
    });
    $("input[name='code']").blur(function() {
        getCurrency();
    });
});



function getCustomerDetails() {
    if($("input[name='customer_code']").val() != 'CASH'){
        $('#pno_nric_id').hide();
        $('#cconact_id').show();
        $('input[name="contacts"]').prop('readonly', false);
        $('input[name="pno_nric"]').prop('readonly', true);
    } else {
        $('#pno_nric_id').show();
        $('#cconact_id').hide();
        $('input[name="contacts"]').prop('readonly', true);
        $('input[name="pno_nric"]').prop('readonly', false);
    }
    var cusid = $("input[name='customer_code']").val();
    $.ajax({
        url: "{{route('buysell.customerdetails')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            cusid : cusid,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                $('input[name="customer_name"]').val(response.data.CustName);
                $('input[name="pno_nric"]').val(response.data.NRICNo);
                $('input[name="dob"]').val(response.data.DOB);
                $('#nationality').val(response.data.Nationality);
                $('input[name="contacts"]').val(response.data.Phone);
                $('input[name="address1"]').val(response.data.CompAddress1);
                $('input[name="address2"]').val(response.data.CompAddress2);
                $('input[name="country"]').val(response.data.Country);
                $('input[name="postal_code"]').val(response.data.PostalCode);
                $('input[name="phone_no"]').val(response.data.CompPhone1);

                $('input[name="phone_no"]').attr('readonly', true);
                $('input[name="country"]').attr('readonly', true);
                $('input[name="address"]').attr('readonly', true);
                $('input[name="postal_code"]').attr('readonly', true);
            } else {
                $('input[name="customer_name"]').val('');
                $('input[name="pno_nric"]').val('');
                $('input[name="dob"]').val('');
                $('#nationality').val('Select Nationality');
                $('input[name="contacts"]').val('');
                $('input[name="address1"]').val('');
                $('input[name="address2"]').val('');
                $('input[name="country"]').val('');
                $('input[name="postal_code"]').val('');
                $('input[name="phone_no"]').val('');
            }
        },      
    });
}

function getCashCustomerDetails() {
    var id = $("input[name='pno_nric']").val();
    $.ajax({
        url: "{{route('cashcustomer.getcashcustomerdetails')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            id : id,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                $('input[name="customer_name"]').val(response.data.Name);
                $('input[name="dob"]').val(response.data.DOB);
                $('input[name="contacts"]').val(response.data.PhoneNo);
                $('input[name="address1"]').val(response.data.Address1);
                $('input[name="address2"]').val(response.data.Address2);
                $('input[name="postal_code"]').val(response.data.Address3);
                $('input[name="phone_no"]').val(response.data.PhoneNo);
                $('input[name="country"]').val(response.data.Address4);
                $('#nationality').val(response.data.Nationality);

                $('input[name="phone_no"]').attr('readonly', true);
                $('input[name="country"]').attr('readonly', true);
                $('input[name="address"]').attr('readonly', true);
                $('input[name="postal_code"]').attr('readonly', true);
            } else {
                $('input[name="customer_name"]').val('');
                $('input[name="dob"]').val('');
                $('input[name="contacts"]').val('');
                $('input[name="address1"]').val('');
                $('input[name="address2"]').val('');
                $('input[name="postal_code"]').val('');
                $('input[name="phone_no"]').val('');
                $('input[name="country"]').val('');
                $('#nationality').val('Select Nationality');
            }
        },      
    });
}

function getbeneficiaryDetails() {
    var pp_nric = $('input[name="pp_nric"]').val();
    $.ajax({
        url: "{{route('ddtransaction.getbeneficiary')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            pp_nric : pp_nric,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                $('input[name="pp_name"]').val(response.data.Name);
                $('input[name="pp_address1"]').val(response.data.Address1);
                $('input[name="pp_address2"]').val(response.data.Address2);
                $('#pp_country').val(response.data.Country);
                $('input[name="bank"]').val(response.data.BankName);
                $('input[name="branch_address"]').val(response.data.BankBranch);
                $('input[name="ac_no"]').val(response.data.BankAcNo);
                $('input[name="swift_code"]').val(response.data.SwiftCode);
            } else {
                $('input[name="pp_name"]').val('');
                $('input[name="pp_address1"]').val('');
                $('input[name="pp_address2"]').val('');
                $('#pp_country').val('');
                $('input[name="bank"]').val('');
                $('input[name="branch_address"]').val('');
                $('input[name="ac_no"]').val('');
                $('input[name="swift_code"]').val('');
            }
        },      
    });  
}

function getddtransactionDetails(val) {
    $.ajax({
        url : "{{route('ddtransaction.getddtransaction')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            val : val,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                $('#save_btn').prop('disabled', true);
                $('#update_btn').prop('disabled', false);
                $('input[name="customer_code"]').val(response.data.CustCode);
                $('input[name="customer_code"]').val(response.data.CustCode);
                $('input[name="customer_name"]').val(response.data.CustName);
                $('input[name="pno_nric"]').val(response.data.CustPPNo);
                $('#dob-datepicker').val(response.data.DOB);
                $('#nationality').val(response.data.CustNationality);
                $('input[name="contacts"]').val(response.data.Name);
                $('#Purpose').val(response.data.Purpose);
                $('input[name="address1"]').val(response.data.CompAddress1);
                $('input[name="address2"]').val(response.data.CompAddress2);
                $('input[name="country"]').val(response.data.CompAddress3);
                $('input[name="postal_code"]').val(response.data.CompAddress4);
                $('input[name="phone_no"]').val(response.data.Telephone);
                $('#customer_type').val(response.data.CustTTType);
                $('input[name="code"]').val(response.data.CurrencyCode);
                $('input[name="curr_name"]').val(response.data.CustCode);
                $('input[name="ex_rate"]').val(response.data.ExchRate);
                $('input[name="conv_rate"]').val(response.data.FAmount);
                $('input[name="amount"]').val(response.data.LAmount);
                $('input[name="pp_nric"]').val(response.data.BenPPNo);
                $('input[name="pp_name"]').val(response.data.BenName);
                $('input[name="pp_address1"]').val(response.data.BenAddress1);
                $('input[name="pp_address2"]').val(response.data.BenAddress2);
                $('#pp_country').val(response.data.BenAddress3);
                $('input[name="bank"]').val(response.data.BenBank);
                $('input[name="branch_address"]').val(response.data.BenBankBranch);
                $('input[name="ac_no"]').val(response.data.BenAccNo);
                $('input[name="swift_code"]').val(response.data.SwiftCode);

            } else {
                $('#save_btn').prop('disabled', false);
                $('#update_btn').prop('disabled', true);
                $('input[name="customer_code"]').val('');
                $('input[name="customer_name"]').val('');
                $('input[name="pno_nric"]').val('');
                $('#dob-datepicker').val('');
                $('#nationality').val('');
                $('input[name="contacts"]').val('');
                $('#Purpose').val('');
                $('input[name="address1"]').val('');
                $('input[name="address2"]').val('');
                $('input[name="country"]').val('');
                $('input[name="postal_code"]').val('');
                $('input[name="phone_no"]').val('');
                $('#customer_type').val('');
                $('input[name="code"]').val('');
                $('input[name="curr_name"]').val('');
                $('input[name="ex_rate"]').val('');
                $('input[name="conv_rate"]').val('');
                $('input[name="amount"]').val('');
                $('input[name="pp_nric"]').val('');
                $('input[name="pp_name"]').val('');
                $('input[name="pp_address1"]').val('');
                $('input[name="pp_address2"]').val('');
                $('#pp_country').val('');
                $('input[name="bank"]').val('');
                $('input[name="branch_address"]').val('');
                $('input[name="ac_no"]').val('');
                $('input[name="swift_code"]').val('');
                var date = new Date();
                var cdate = date.getDate()+'-'+date.getMonth()+'-'+date.getFullYear();
                $('#app_date').val(cdate);
                $('#agent_date').val(cdate);
                $('input[name="customer_code"]').val("CASH");
                $('input[name="customer_name"]').val("CASH CUSTOMER");
            }
        },      
    });  
}

function submitForm(type) {
    if(type == 'save'){
        var purpose = $('#Purpose').val();
        var amount = $('input[name="amount"]').val();
        var ex_rate = $('input[name="ex_rate"]').val();
        var conv_rate = $('input[name="conv_rate"]').val();

        if(purpose == '' || amount == '' || ex_rate == '' || conv_rate == ''){
            $('#frm_ddtransaction').addClass('was-validated');
            return false;
        }
        $.ajax({
            url: "./ddtransaction/insert",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : $('#frm_ddtransaction').serialize(),
            success: function(response){
                $('#frm_ddtransaction').removeClass('was-validated');
                if(response.status == 200){
                    success_toastr(response.content);
                    $('#frm_ddtransaction')[0].reset();
                    RefNo();
                }else {
                    $.each(response.content, function(v, t){
                        danger_toastr(t);
                    });
                    $('#frm_ddtransaction')[0].reset();
                }
            },
        });   
    } else if(type == 'update'){
        var purpose = $('input[name="Purpose"]').val();
        var amount = $('input[name="amount"]').val();
        var ex_rate = $('input[name="ex_rate"]').val();
        var conv_rate = $('input[name="conv_rate"]').val();
        if(purpose == '' || amount == '' || ex_rate == '' || conv_rate == ''){
            $('#frm_ddtransaction').addClass('was-validated');
            return false;
        }
        $.ajax({
            url: "./ddtransaction/update",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : $('#frm_ddtransaction').serialize(),
            success: function(response){
                $('#frm_ddtransaction').removeClass('was-validated');
                if(response.status == 200){
                    success_toastr(response.content);
                    $('#frm_ddtransaction')[0].reset();
                    RefNo();
                }else {
                    $.each(response.content, function(v, t){
                        danger_toastr(t);
                    });
                    $('#frm_ddtransaction')[0].reset();
                }
            },
        });  
    } else if (type == 'clear') {
        $('#frm_ddtransaction')[0].reset();
        RefNo();
    }       
}

function RefNo() {
    $.ajax({
        url : "{{ route('ddtransaction.getRefNo') }}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response){
            if(response.status == 200){
                $('input[name="ref_no"]').val(response.data);
            }
        },      
    });
}

function getCurrency() {
    $.ajax({
        url : "{{ route('ddtransaction.getCurrency') }}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            id : $('input[name="code"]').val(),
        },
        success: function(response){
            if(response.status == 200){
                $('input[name="curr_name"]').val(response.data.CurrencyName);
            } else {
                $('input[name="curr_name"]').val('');
            }
        },      
    });
}

function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}

function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
</script>
@endsection
