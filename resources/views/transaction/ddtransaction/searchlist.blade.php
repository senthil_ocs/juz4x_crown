@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
.badge{
	font-size: 100%;
}
#to_date_root .picker__holder {
   right: 30px;
}
#from_date_root .picker__holder {
   right: 30px;
}

</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>DD/TT Post Search</h1>
			<ul>
			    <li>Listing</li>
			    <li><a href="{{route('ddtransaction')}}">Create</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

					    <div class="col-lg-12 mb-12">
					        <div class="card">
					            <div class="card-body">
					            	<form action="{{ route('dealtransactionreportexport')}}" method="POST" id="ddtransactionform">
					            		@csrf
										<div class="form-group row">
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Receipt No</label>
										            <div class="col-sm-6">
										            <input type="text" name="beneficiary_id" id="beneficiary_id" placeholder="Beneficiary Id" autocomplete="off" class="form-control">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label"><a onclick="GetCashCustomerModal('cashcustomer');" data-toggle="modal" data-target="#get_cashcustomer_modal_details" href="">Cust Code</a></label>
										            <div class="col-sm-6">
										                <input id="cust_code" class="form-control" placeholder="Customer Code" name="cust_code" onkeyup="this.value = this.value.toUpperCase();">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Cust Name</label>
										            <div class="col-sm-6">
										                <input id="cust_code" class="form-control" placeholder="Customer Name" name="cust_name">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">NRIC/FIN/PP</label>
										            <div class="col-sm-6">
										                <input type="text" name="CustNRICNO" id="CustNRICNO" placeholder="CustNRICNO" class="form-control custNRICdis" autocomplete="off">
										            </div>
										        </div>
										    </div>
										</div>
										<div class="form-group row">
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Comp Name</label>
										            <div class="col-sm-6">
										            <input type="text" name="compname" id="compname" autocomplete="off" class="form-control">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">From Date</label>
										            <div class="col-sm-6">
										                <input type="text" id="from_date" class="form-control" placeholder="From Date" name="from_date">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">To Date</label>
										            <div class="col-sm-6">
										                <input type="text" id="to_date" class="form-control" placeholder="To Date" name="to_date" >
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Ben Name</label>
										            <div class="col-sm-6">
										                <input type="text" name="benname" id="benname" placeholder="Ben Name" class="form-control custNRICdis" autocomplete="off">
										            </div>
										        </div>
										    </div>
										</div>
										<div class="form-group row">
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">Swift Code</label>
										            <div class="col-sm-6">
										            <input type="text" name="swiftcode" id="swiftcode" placeholder="Swift Code" autocomplete="off" class="form-control" >
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">AC No</label>
										            <div class="col-sm-6">
										                <input id="acno" class="form-control" placeholder="Ac No" name="acno">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label">FAmount</label>
										            <div class="col-sm-6">
										                <input id="famount" class="form-control" placeholder="FAmount" name="famount">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-3">
										        <div class="row">
										            <label class="col-sm-4 col-lg-4 col-form-label"><a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">Currency</a></label>
										            <div class="col-sm-6">
										                <input id="currency_code" class="form-control" placeholder="Currency Code" name="currency_code" onkeyup="this.value = this.value.toUpperCase();" value="" >
										            </div>
										        </div>
										    </div>
										</div>
										<div class="form-group row">
										    <div class="col-lg-3">
										        <div class="row">
										        	<input type="text" id="customer_code" value="CASH" hidden>
										            <label class="col-sm-4 col-lg-4 col-form-label"><a onclick="GetAgentDetails('agent');" data-toggle="modal" data-target="#get_agent_modal" href="">Agent</a></label>
										            <div class="col-sm-6">
										            <input type="text" name="agent" id="agent" placeholder="Agent" autocomplete="off" class="form-control" >
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-4">
										        <div class="row">
										            <label class="col-sm-4 col-lg-2 col-form-label">In / Out</label>
										            <div class="col-sm-5">
										                <input id="InPut" class="form-control" placeholder="InPut" name="InPut">
										            </div>
										            <div class="col-sm-5">
										                <input id="OutPut" class="form-control" placeholder="OutPut" name="OutPut">
										            </div>
										        </div>
										    </div>
										    <div class="col-lg-4">
										        <div class="row">
										            <label class="col-sm-4 col-lg-2 col-form-label">Staff</label>
										            <div class="col-sm-5">
														<select class="form-control" name="staff" id="staff">
															<option>All Staff</option>
										                </select>
										            </div>
										            <div class="col-sm-5">
										                <button class="btn btn-primary m-1" type="button" id="">Today</button>
										            </div>
										        </div>
										    </div>
										</div>
										<div class="row mt-3">
											<div class="col-lg-6 col-sm-12" id="exportbtn"> <!---/***added by anushya***/-->
											</div>
											<div class="col-lg-6 col-sm-12 text-right">
													<button class="btn btn-primary m-1" type="button" id="">Clear Serach</button>
													<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Search</button>
													<button class="btn btn-primary m-1" type="button" id="">Export</button>
											</div>
										</div>
					            	</form>
					            </div>
					        </div>
					    </div>
					    <div class="table-responsive">
					    	 <table id="table_ddtransactionlist" class="display nowrap  table-striped table-bordered table" style="width:100%">
                            </table>	
					    </div>
					</div>	
				</div>
			</div>
		</div>	
		<div id="changeBeneficiaryModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
				    <div class="modal-header">
				        <h4 class="modal-title">Change Beneficiary</h4>
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				    </div>
				    <div class="col-md-12 mb-12 master-main-sec" style="margin-top: 10px;">
			            <div class="card">
			                <h5 class="m-2">Beneficiary Details</h5>
			                <div class="card-body">
			                    <div class="row">
			                       <div class="col-lg-12 col-sm-12">
						            	<form  class="needs-validation" method="post" novalidate action="#" id="changeBeneficiary" name="changeBeneficiary">
						            		{{csrf_field()}}
											<div class="form-group row">
	                                            <label for="beneficiaryName" class="col-sm-4 col-form-label text-rights">Beneficiary Name</label>
	                                           <div class="col-sm-7">
	                                                <select class="form-control" name="beneficiaryName" id="beneficiaryName" >
		                                                <option value="">Select beneficiary</option>
		                                                @foreach($beneficiary as $value)
		                                                <option value="{{trim($value->id,' ')}}">{{$value->BeneName}}</option>
		                                                @endforeach
		                                            </select>
		                                            <div class="invalid-feedback">
				                                        Please Select Beneficiary
				                                    </div>
	                                            </div>
	                                       </div>
	                    					<button type="button" class="btn btn-primary m-1" style="float:right;" name="changeBene" id="changeBene">Change Beneficiary</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
			    </div>
		  	</div>
		</div>
	@include('modal.customer')	
	@include('modal.currency')
	@include('modal.agent')
	@include('modal.cashcustomer')

@endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/modal.details.js')}}"></script>
	<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){
		
	    var date = new Date();
	    var mnt = date.getMonth()+1;
	    var cdate = date.getDate()+'/'+mnt+'/'+date.getFullYear();
	    $('#from_date,#to_date').val(cdate);

	    $("#from_date,#to_date").inputmask("dd/mm/yyyy", {
	        separator: "/",
	        alias: "dd/mm/yyyy",
	        placeholder: "dd/mm/yyyy"
	    });

	    var changeId = '';
	    $(document).on("click","#changeBenName", function () {
	    	changeId = $(this).attr('data-ref');
		});

	    $('#changeBene').on('click', function(){
	    	var beneId = $('#beneficiaryName').val();
	    	if(beneId !='') {
	    		$.ajax({
			        url : "{{ route('ddtransaction.changeBeneficiary') }}",
			        type: "POST",
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        },
			        data :{
						changeId : changeId,
						beneId : beneId
			        },
			        success: function(response){
				        if(response.status == 200){
							$('#changeBeneficiaryModal').modal('toggle');	
							setTimeout(function(){ getdatas(); }, 300);		
						}            
			        }      
			    });
	    	} else {
	    		$.alert('Please Select Beneficiary');
	    	}
	    });
	    /*$(document).on("click", ".sendDBS",function() {
	        var dataval = $(this).attr('data-ref');
	        console.log(dataval);
	        $.ajax({
	            url: "{{route('ddtransaction.posttransaction')}}",
	            type: "GET",
	            async: false,
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	            data : {
	                dataval : dataval,
	            },
	            beforeSend: function() {},
	            success: function(response){
	            	console.log(response);
	            	var transdetails = response.transdetails;
	                $.alert('posttransaction');
	                $.ajax({
	                    url: "{{route('ddtransaction.updatetransaction')}}",
	                    type: "POST",
	            		async: false,
	                    headers: {
	                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                    },
	                    data : {
	                        dataval : dataval,
	                    },
	                    beforeSend: function() {},
	                    success: function(response){
	            			var Updatetransdetails = response.transdetails;
	                        $.alert('updatetransaction');
	                    }
	                });
	            }
	        });        
	    });*/
	    $('#changeBeneficiaryModal').on('hidden.bs.modal', function (e) {
		  $(this).find("select").val('').end();
		});
		$(document).on("click", "input[name='agent_id_detail']",function() {
	        $('#get_agent_modal').modal('toggle');
	        var datas = $(this).val().split("|");
	        $('input[name="agent"]').val(datas[0]);
		});
	    $(document).on("click", "input[name='cashcustomer_id_detail']",function() {
	        $('#get_cashcustomer_modal_details').modal('toggle');
	        /*$('input[name="cust_code"]').val($(this).val()); 
	        $('input[name="cust_name"]').val($(this).attr('data-ref')); */
	        $('input[name="CustNRICNO"]').val($(this).val()); 
	        $('input[name="cust_code"]').val($(this).attr('data-ref')); 
	        $('input[name="cust_name"]').val($(this).attr('data-name')); 
	    });

	    $(document).on("click", "input[name='currency_id_detail']",function() {
	        $('#get_currency_modal_details').modal('toggle');
	        var val = $(this).val();
	        var valarray = val.split('|');
	        $('input[name="currency_code"]').val(valarray[0]);
	        $('input[name="currency_name"]').val($(this).attr('data-ref'));
	        $( "#currency_code" ).trigger( "blur" );
    	});

    	$(document).on("blur", "input[name='cust_code']",function() {
	        var cval = $(this).val();
	        $.ajax({
	        		headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                  url:"{{ route('getcustomerdetails') }}",
                  method:"POST",
                  data:{code:cval},
                  success:function(cdata){
                  	if(cdata.CustName != undefined){
	        			$('input[name="cust_name"]').val(cdata.CustName);
                  	}else{
                  		$.alert('Customer detail not found');
                  	}
                  }
            });
    	});

    	$(document).on("blur", "input[name='currency_code']",function() {
	        var cval = $(this).val();
	        $.ajax({
	        	 headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },
	              url:"{{ route('getcurrencydetails') }}",
	              method:"POST",
	              data:{currencycode:cval},
	              success:function(cdata){
	              	if(cdata.CurrencyCode != undefined){
	        			$('input[name="currency_name"]').val(cdata.CurrencyName);
	              	}else{
                  		$.alert('Currency Code not founded');
                  	}
	              }
	        });
    	});   	
		setTimeout(function(){ getdatas(); }, 300);
	});

	function submitForm(type) {	
		/***added by anushya***/	
		var agtcode = $('input[name="agent"]').val();
		if(typeof(agtcode) != "undefined" && agtcode !== null) {
    		$('#exportbtn').html('<button class="btn btn-primary m-1" type="button" id="exportsearch" onclick="exportDetails();">Export '+agtcode+'</button>');
    	}
    	/***added by anushya***/
		getdatas();
	}

	function getdatas() {
		$('#table_ddtransactionlist').DataTable({
		    paging:   false,
		    destroy: true,
		    searching : false,
		    autoWidth : false,
		    info:     false,
		    ordering: false,
		    ajax: {
		            url:"{{ route('ddtransaction.searchlist') }}",
		            type: "POST",
		            headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
					data: function (d) {
						d.cust_code = $('input[name="cust_code"]').val();
						d.currency_code = $('input[name="currency_code"]').val();
						d.bank_name = $('input[name="bank_name"]').val();
						d.bank_code = $('input[name="bank_code"]').val();
						d.from_date = $('input[name="from_date"]').val();
						d.to_date = $('input[name="to_date"]').val();
						d.swiftcode = $('input[name="swiftcode"]').val();
						d.acno = $('input[name="acno"]').val();
						d.benname = $('input[name="benname"]').val();
						d.tranno = $('input[name="beneficiary_id"]').val();
						d.CustNRICNO = $('input[name="CustNRICNO"]').val();
					},
		        },
			columns: [
			    {data: 'TranNo', name: 'TranNo',title:'Tran No'},
			    {data: 'DBS', name: 'DBS',title:'Send DBS'},
			    {data: 'TranDate', name: 'TranDate',title:'Tran Date'},
			    {data: 'CustCode', name: 'CustCode',title:'Customer Code'},
			    {data: 'BenBank', name: 'BenBank',title:'Bank Code'},
			    {data: 'CurrencyCode', name: 'CurrencyCode',title:'Currency Code'},
			    {className: 'text-right',data: 'ExchRate', name: 'ExchRate',title:'Exchange Rate'},
			    {className: 'text-right',data: 'FAmount', name: 'FAmount',title:'FAmount'},
			    {className: 'text-right',data: 'LAmount', name: 'LAmount',title:'LAmount'},
			    {className: 'text-right',data: 'Comm', name: 'Comm',title:'Comm'},
			    {className: 'text-right',data: 'TotalAmount', name: 'TotalAmount',title:'Total Amount'},
			    {data: 'AgentCode', name: 'AgentCode',title:'Agent Code'},
			    {className: 'text-right',data: 'AgentRate', name: 'AgentRate',title:'Agent Rate'},
			    {className: 'text-right',data: 'AgentTotal', name: 'AgentTotal',title:'Agent Amount'},
			    {data: 'PaymentDate', name: 'PaymentDate',title:'Payment Date'},
			    {data: 'BenName', name: 'BenName',title:'Beneficiary Name'},
			    {data: 'Select', name: 'Select',title:'Select'},
			],
		});
	} 
$( function() {
	$( "#from_date,#to_date").datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true
	});
});

/***added by anushya***/
function exportDetails() {	
	exportdatas();
}

function exportdatas() {
	var alldata = $('input[name="select[]"]:checked').map(function(){ 
                return this.value; 
            }).get();
	$.ajax({
      type: "POST",
      url: "{{ route('ddtransaction.exportpostlist') }}",
      headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
		data: {
            'trans[]': alldata,
        },
      success: function(data) {
      		if(data=='nodata') {
      			alert("Choose the transactions to export");
      		} else {
      			var encodedUri = 'data:application/csv;charset=utf-8,' + encodeURIComponent(data);
				var link = document.createElement("a");
				link.setAttribute("href", encodedUri);
				link.setAttribute("download", "transactions.csv");
				link.innerHTML= "Download do Relatório";
				document.body.appendChild(link);
				link.click();
      		}
        	
      },
      error: function(result) {
      }
    });
}
/***added by anushya***/
</script>
@endsection