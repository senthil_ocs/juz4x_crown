@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    .label-denomination{
        border: 1px solid #dee2e6;
        border-radius: 1px;
        background: #f2f2f2;
    }
    #suggesstion-box :hover {
      background-color: #e9e9e9; 
    }
</style>
@endsection
@section('main-content')
<div class="breadcrumb">
    <h1>Unpost DD/TT Transaction</h1>
    <ul>
        <li>Create</li>
        <li><a href="{{route('ddtransaction.unpostsearchlist')}}">List</a></li>
    </ul>
</div>

<div class="separator-breadcrumb border-top">
    
</div>
<form id="frm_ddtransaction" name="frm_ddtransaction" class="needs-validation" method="post" novalidate action="#">
{{csrf_field()}}
    <div class="row mb-12 master-main">

        <div class="col-md-12 mb-12 master-main-sec">
            <div class="card text-left">
                <div class="card-body">
                   <div class="row">
                       <div class="col-lg-6 col-sm-12">
                           <div class="row m-1">
                                <label for="ref_no" class="col-sm-2 col-form-label text-rights">RefNo</label>
                                <input type="textbox" name="ref_no"  class="col-sm-2 form-control allownumeric" id="ref_no"  placeholder="Enter Ref No" value="{{$unpost_id}}"><!-- value="{{$dis_TranNo}}" -->

                                <label for="ddno" class="col-sm-2 col-form-label text-right">DDNo</label>
                                <input type="textbox" name="ddno"  class="col-sm-2 form-control allownumeric" id="ddno" placeholder="Enter DD No"> <!-- value="{{$dis_TranNo}}" -->

                                <label for="rpt_no" class="col-sm-2 col-form-label text-right">RptNo</label>
                                <input type="textbox" name="rpt_no"  class="col-sm-2 form-control allownumeric" id="rpt_no" placeholder="Enter Rpt No">
                           </div>
                       </div>
                        <div class="col-lg-6 col-sm-12">
                          <div class="row">
                            <div class="col-md-3 text-right">
                                <button class="btn btn-primary m-1" type="button" id="check_details">Check Details</button>
                            </div>
                            <div class="col-md-3 text-right">
                                <button class="btn btn-primary m-1" type="button" id="check_details">ACCUITY SEARCH</button>
                            </div>
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary m-1" type="button" id="accuity_search">BEN ACCUITY SEARCH</button>
                            </div>
                          </div>
                        </div>
                   </div> 
                </div>
            </div>        
        </div>

        <div class="col-md-12 mb-12 master-main-sec">
            <div class="card text-center">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="row m-1">
                                <label for="app_date" class="col-sm-5 col-form-label text-left">App Date</label>
                                <input type="text" id="app_date" class="col-sm-7 form-control" placeholder="dd-mm-yyyy" name="app_date">
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 text-right">
                                <button class="btn btn-primary m-1" type="button" id="check_details">View Transaction</button>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="row m-1">
                                <label for="agent_date" class="col-sm-5 col-form-label text-rights">Agent Date</label>
                                <input type="text" id="agent_date" class="col-sm-7 form-control" placeholder="dd-mm-yyyy" name="agent_date">
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="row m-1">
                                <label for="payment" class="col-sm-6 col-form-label text-rights">Payment</label>
                                <input type="textbox" name="payment"  class="col-sm-6 form-control allownumeric" id="payment" placeholder="Enter Payment">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-6">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="company_name" class="col-sm-3 col-form-label">
                                <a onclick="GetModalDetails('customer');" data-toggle="modal" data-target="#get_modal_details" href="">Customer Code</a> <span class="mandatory">*</span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="customer_code" class="form-control" autocomplete="off" id="customer_code" placeholder="Enter Customer code" onkeyup="this.value = this.value.toUpperCase();" required >
                                    <div class="invalid-feedback">Enter Customer code</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="customer_name" class="form-control" autocomplete="off" id="customer_name" required >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pno_nric" class="col-sm-3 col-form-label">P.No / Nric <span class="mandatory">*</span></label>
                                <div class="col-sm-9 input-group mb-3">
                                    <input type="textbox" name="pno_nric" class="form-control" autocomplete="off" id="pno_nric" placeholder="Enter P.No / Nric" required >

                                    <div class="input-group-append" id="pno_nric_id" onclick="GetCashCustomerModal('cashcustomer');"data-toggle="modal" data-target="#get_cashcustomer_modal_details" style="cursor: pointer;">
                                        <span class="input-group-text" id="basic-addon2">
                                            <i class="i-Business-Mens"></i>
                                        </span>
                                    </div>

                                    <div class="invalid-feedback">Enter P.No / Nric</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="dob-datepicker" class="col-sm-3 col-form-label">Birth Date</label>
                                <div class="col-sm-9">
                                    <input type="text" id="dob-datepicker" class="form-control" placeholder="dd-mm-yyyy" name="dob">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="nationality" class="col-sm-3 col-form-label">Nationality <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="nationality" id="nationality" required>
                                        <option value="">Select Nationality</option>
                                        @foreach($nationality as $value)
                                        <option value="{{trim($value->Code,' ')}}">{{trim($value->Description,' ')}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Select Nationality</div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <?php /* <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="contacts" class="col-sm-3 col-form-label">Contacts <span class="mandatory">*</span></label>
                                <div class="col-sm-9 input-group mb-3">
                                    <input type="textbox" name="contacts" class="form-control" autocomplete="off" id="contacts" placeholder="Enter Contacts" required >
                                    <div class="input-group-append" id="cconact_id"  onclick="GetCustomerContactaModal('customercontacts');" data-toggle="modal" data-target="#get_customercontacts_modal_details" style="cursor: pointer;">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="i-Business-Mens"></i>
                                        </span>
                                    </div>
                                    <div class="invalid-feedback">Enter Contacts</div>
                                </div>
                            </div>
                        </div>
                    </div> */ ?>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="nationality" class="col-sm-3 col-form-label">Purpose <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="Purpose" id="Purpose" required>
                                        <option value="">Select Purpose</option>
                                        @foreach($purpose as $value)
                                        <option value="{{$value->Name}}">{{$value->Name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Select Purpose</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-6">                            
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <input type="hidden" name="MCType">
                                <input type="hidden" name="CompName">
                                <input type="hidden" name="AgentTTType">
                                <label for="address1" class="col-sm-3 col-form-label">Address1</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="address1" class="form-control" autocomplete="off" id="address1" placeholder="Enter Address1"  >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="address2" class="col-sm-3 col-form-label">Address2</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="address2" class="form-control" autocomplete="off" id="address2" placeholder="Enter Address2"  >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label class="col-sm-3 col-form-label">Country</label>
                                <div class="col-sm-9">
                                    <!-- <input type="textbox" name="country" class="form-control" autocomplete="off" id="country" placeholder="Enter Country"  > -->
                                    <select class="form-control" name="country" id="country">
                                        <option value="">Select Country</option>
                                        @foreach($country as $value)
                                        <option value="{{trim($value->Country, ' ')}}">{{trim($value->Country, ' ')}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label class="col-sm-3 col-form-label">Postal Code</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="postal_code" class="form-control" autocomplete="off" id="postal_code" placeholder="Enter Postal Code"  >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label class="col-sm-3 col-form-label">Phone No</label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="phone_no" class="form-control" autocomplete="off" id="phone_no" placeholder="Enter Phone No"  >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label class="col-sm-3 col-form-label">Customer Type 
                                </label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="customer_type" id="customer_type">
                                        <option value="">Select Customer Type</option>
                                        @foreach($customertype as $value)
                                        <option value="{{$value->Name}}">{{$value->Name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12 mb-12 master-main-sec">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                       <div class="col-lg-12 col-sm-12">
                           <div class="row">
                               <div class="col-lg-4 col-sm-6">
                                   <div class="form-group row">
                                       <label for="code" class="col-sm-3 col-form-label text-rights"><a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">Currency</a> <span class="mandatory">*</span></label>
                                       <div class="col-sm-4">
                                        <input type="textbox" class="form-control" name="code" class="form-control" autocomplete="off" id="code" placeholder="Enter Currency Code" list="json-datalist"  onkeyup="this.value = this.value.toUpperCase();" required>
                                        <div class="invalid-feedback"> 
                                            Please Enter Currency
                                        </div>
                                       </div>
                                        <div class="col-sm-4">
                                            <input type="textbox" name="curr_name" class="form-control" id="curr_name" placeholder="Enter Currency Name" autocomplete="off">
                                        </div>
                                   </div>                                   
                               </div> 
                               <div class="col-lg-2 col-sm-6">
                                   <div class="form-group row">
                                       <label for="ex_rate" class="col-sm-5 col-form-label text-rights">Ex.Rate <span class="mandatory">*</span></label>
                                       <div class="col-sm-6">
                                            <input class=" form-control" type="textbox" name="ex_rate" autocomplete="off" id="ex_rate" placeholder="0.0000000000" required>
                                            <div class="invalid-feedback"> 
                                                Please Enter Ex Rate
                                            </div>
                                        </div>
                                   </div>
                               </div>
                               <div class="col-lg-3 col-sm-6">
                                   <div class="form-group row">
                                       <label for="conv_rate" class="col-sm-4 col-form-label text-rights">Conv.Rate <span class="mandatory">*</span></label>
                                       <input class="col-sm-7 form-control" type="textbox" name="conv_rate" autocomplete="off" id="conv_rate" placeholder="0.0000000000" required>
                                        <div class="invalid-feedback text-center"> 
                                            Please Enter Conv Rate
                                        </div>
                                   </div>
                               </div>
                               <div class="col-lg-3 col-sm-6">
                                   <div class="form-group row">
                                       <label for="amount" class="col-sm-4 col-form-label text-rights">Amount <span class="mandatory">*</span></label>
                                       <div class="col-sm-6">
                                            <input class=" form-control" type="textbox" name="amount" autocomplete="off" id="amount" placeholder="0.00" required>
                                            <div class="invalid-feedback"> 
                                                Please Enter Amount
                                            </div>
                                        </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-6">
                <h4 class="m-2">Beneficiary Information</h4>
                <div class="card-body">

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="CollectPayment" class="col-sm-3 col-form-label">Collect Payment</label>
                                <input type="radio" id="bankTransfer" class="bankTransfer" name="collectPaymentType" checked value="Bank" style="margin-left: 15px;">
                                <label for="bankTransfer" class="bankTransfer">Bank Transfer</label>
                                <input type="radio" class="cashPickup" id="cashPickup" name="collectPaymentType" value="Cash" style="margin-left: 20px">
                                <label for="cashPickup" class="cashPickup">Cash Pickup</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_nric" class="col-sm-3 col-form-label">PP/Nric / Name <span class="mandatory">*</span></label>
                                <!-- <div class="col-sm-4 input-group mb-3"> -->
                                    <input type="textbox" name="pp_nric" class="form-control" id="pp_nric" placeholder="Enter PP / Nric" autocomplete="off" hidden>
                                    <!-- <div class="input-group-append" id="pp_nric" onclick="GetBeneficiaryModal('beneficiary');"data-toggle="modal" data-target="#get_beneficiary_modal_details" style="cursor: pointer;">
                                        <span class="input-group-text" id="basic-addon2">
                                            <i class="i-Business-Mens"></i>
                                        </span>
                                    </div>
                                    <div class="invalid-feedback">Please Enter PP / Nric</div> -->
                                <!-- </div> -->
                                <div class="col-sm-9 input-group mb-3">
                                    <input type="textbox" name="pp_name" class="form-control" id="pp_name" placeholder="Enter Name" autocomplete="off" required>
                                    <div class="input-group-append" id="pp_nric" onclick="GetBeneficiaryModal('beneficiary');"data-toggle="modal" data-target="#get_beneficiary_modal_details" style="cursor: pointer;">
                                        <span class="input-group-text" id="basic-addon2">
                                            <i class="i-Business-Mens"></i>
                                        </span>
                                    </div>
                                    <div class="invalid-feedback">Please Enter PP / Nric / Name</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_address1" class="col-sm-3 col-form-label">Address</label>
                                <div class="col-sm-4">
                                    <input type="textbox" name="pp_address1" class="form-control" id="pp_address1" placeholder="Enter Address1" autocomplete="off" >
                                </div>
                                <div class="col-sm-5">
                                    <input type="textbox" name="pp_address2" class="form-control" id="pp_address2" placeholder="Enter Address2" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_telephone" class="col-sm-3 col-form-label">Telephone <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="pp_telephone" class="form-control" id="pp_telephone" placeholder="Enter Telephone" autocomplete="off" required>
                                    <div class="invalid-feedback">Please Enter Telephone</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row bankPayment">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_country" class="col-sm-3 col-form-label">Country <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="pp_country" id="pp_country" required>
                                        <option value="">Select Country</option>
                                        @foreach($country as $value)
                                        <option value="{{trim($value->Country, ' ')}}" group="{{$value->CountryGroup}}">{{trim($value->Country, ' ')}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Please Select Country</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" id="clearCode">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_clearing_code" class="col-sm-3 col-form-label">Clearing Code<span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="pp_clearing_code" class="form-control" id="pp_clearing_code" placeholder="Enter Clearing Code" autocomplete="off" required>
                                    <div class="invalid-feedback">Please Enter Clearing Code</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" id="iban">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="pp_IBAN" class="col-sm-3 col-form-label">IBAN<span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <input type="textbox" name="pp_IBAN" class="form-control" id="pp_IBAN" placeholder="Enter IBAN" autocomplete="off" required>
                                    <div class="invalid-feedback">Please Enter IBAN</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row bankPayment">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="ac_no" class="col-sm-3 col-form-label">Swift Code / Account No <span class="mandatory">*</span></label>
                                <div class="col-sm-4">
                                    <input type="textbox" name="swift_code" class="form-control" id="swift_code" placeholder="Swift Code" autocomplete="off" required onkeyup="getSwiftAutoFill(this.value);">
                                    <div id="suggesstion-box"></div>
                                </div>
                                <div class="col-sm-5">
                                    <input type="textbox" name="ac_no" class="form-control" id="ac_no" placeholder="Account No" autocomplete="off" required>
                                    <div class="invalid-feedback">Please Enter Account No / Swift Code</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row bankPayment">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="bank" class="col-sm-3 col-form-label">Bank / Branch Address <span class="mandatory">*</span></label>
                                <div class="col-sm-4">
                                    <input type="textbox" name="bank" class="form-control" id="bank" placeholder="Enter Bank" autocomplete="off" required>
                                    <div class="invalid-feedback">Please Enter Bank / Branch Address</div>
                                </div>
                                <div class="col-sm-5">
                                    <input type="textbox" name="branch_address" class="form-control" id="branch_address" placeholder="Branch Address" autocomplete="off" required>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12 master-main-sec">
            <div class="card mb-2">
                <h4 class="m-2">Transaction Details</h4>
                <div class="card-body">
                    <div class="form-group row">
                        <div class=" col-sm-12">
                            <h5 class="m-2" style="position: relative;right: 20px;">Customer</h5>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <label for="exrate" class="col-sm-4 col-form-label">SGD Amount</label>
                                        <div class="col-sm-8">
                                            <input type="textbox" name="c_exrate" class="form-control text-right" id="c_exrate" placeholder="Enter Rate" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <label for="comm" class="col-sm-4 col-form-label">Comm</label>
                                        <div class="col-sm-8">
                                            <input type="textbox" name="c_comm" class="form-control text-right" id="c_comm" placeholder="Enter Comm" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <label for="sgdamount" class="col-sm-4 col-form-label"></label>
                                        <div class="col-sm-8">
                                            <input type="textbox" name="c_sgdamount" class="form-control text-right" id="c_sgdamount" placeholder="Enter SGD Amount" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <label for="tot" class="col-sm-4 col-form-label">Total (S$)</label>
                                        <div class="col-sm-8">
                                            <input type="textbox" name="c_tot" class="form-control text-right" id="c_tot" placeholder="Enter Total" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 mb-12 master-main-sec">
            <div class="card text-center">
                <div class="card-body">
                   <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn  btn-primary m-1" type="button" id="update_btn" onclick="submitForm('update');">Update</button>
                        <button class="btn  btn-primary m-1" type="button">Agent Advice</button>
                        <button class="btn  btn-primary m-1" type="button">Reprint (F10)</button>
                        <button class="btn  btn-primary m-1" type="button" onclick="submitForm('save');" id="save_btn">Save (F4)</button>
                        <button class="btn  btn-primary m-1" type="button" onclick="submitForm('clear');">Clear (F2)</button>
                        <button class="btn  btn-primary m-1" type="button">Delete (F6)</button>
                        <button class="btn  btn-primary m-1" type="button">Close (Esc)</button>
                    </div>
                   </div> 
                </div>
            </div>        
        </div>

    </div>
</form>
@include('modal.customer')
@include('modal.customercontact')
@include('modal.cashcustomer')
@include('modal.currency')
@include('modal.beneficiary')
@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
    <script src="{{asset('assets/js/ladda.script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

    <script src="{{asset('assets/js/modal.details.js')}}"></script>
    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>

@endsection

@section('bottom-js')
<script type="text/javascript">
$(document).ready(function(){
    $('#amount,#c_exrate,#c_tot').mask("000,000,000.00", {reverse: true});
    $('#clearCode').hide();
    $('#iban').hide();
    $('.bankPayment').show();
    $('input[type=radio][name=collectPaymentType]').change(function() {
        if (this.value == 'Cash') {
            $('.bankPayment').hide();
        }
        else if (this.value == 'Bank') {
            $('.bankPayment').show();
        }
    });

    if($('#ref_no').val()>=1){
        val = $('#ref_no').val()
        getddtransactionDetails(val);
    }

    $('#pp_country').on('change', function() {
        if($('#pp_country').find(':selected').attr('group') == "USA"){
            $('#clearCode').show();
            $('#iban').hide();
        } else if($('#pp_country').find(':selected').attr('group') == "Europe"){
            $('#iban').show();
            $('#clearCode').hide();
        } else {
            $('#clearCode').hide();
            $('#iban').hide();
        }
    });

    $('input[name="ex_rate"]').on('keyup keypress', function(e) {
        this.value = this.value.replace(/[^0-9\.]/g,'');
        cust_cla();
        if($(this).val() != ''){
            var conv = $(this).val();
            var conv1 = 1;
            var tot = conv1/conv;
            $('input[name="conv_rate"]').val(tot.toFixed(10));
        }
    });

    $('input[name="c_exrate"]').on('keyup keypress', function(e) {
        this.value = this.value.replace(/[^0-9\.\,]/g,'');
    });
    $('input[name="c_comm"]').on('keyup keypress', function(e) {
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });
    $('input[name="c_tot"]').on('keyup keypress', function(e) {
        this.value = this.value.replace(/[^0-9\.\,]/g,'');
    });

    $('input[name="amount"]').on('keyup keypress', function(e) {
        this.value = this.value.replace(/[^0-9\.\,]/g,'');
        cust_cla();
    });

    $('input[name="conv_rate"]').on('keyup keypress', function(e) {
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });
    $('input[name="conv_rate"]').on('blur', function(e) {
        if($(this).val() != ''){
            var conv = $(this).val();
            var conv1 = 1;
            var tot = conv1/conv;
            $('input[name="ex_rate"]').val(tot.toFixed(10));
        }
    });

    $('input[name="c_comm"]').on('keyup keypress', function(e) {
        if($('input[name="ex_rate"]').val() != ''){
            cust_tot();
        }
    });

    $('input[name="c_comm"]').focus(function() {
      if($(this).val() == 0){
        $('input[name="c_comm"]').val('');
      }
    });
    $('input[name="c_comm"]').focusout(function() {
      if($(this).val() == 0){
        $('input[name="c_comm"]').val('0');
      }
    });
    
    $('input[name="c_comm"]').on('blur', function(e) {
        if($('input[name="ex_rate"]').val() != ''){
            cust_tot();
        }
    });

    $('input[name="customer_code"]').val("CASH");
    $('input[name="customer_name"]').val("CASH CUSTOMER");
    if($("input[name='customer_code']").val() != 'CASH'){
        $('#pno_nric_id').hide();
        $('#cconact_id').show();
        $('input[name="contacts"]').prop('readonly', false);
        $('input[name="pno_nric"]').prop('readonly', true);
        $('input[name="customer_name"]').val("");
    } else {
        $('#pno_nric_id').show()
        $('#cconact_id').hide();;
        $('input[name="contacts"]').prop('readonly', true);
        $('input[name="pno_nric"]').prop('readonly', false);
        $('input[name="customer_name"]').val("CASH CUSTOMER");
    }
    $('#update_btn').prop('disabled', true);
    var dformat = "{{ env('DATE_FORMAT') }}";
    var date = new Date();
    var mnt = date.getMonth()+1;
    var cdate = date.getDate()+'-'+mnt+'-'+date.getFullYear();
    $('#agent_date, #app_date').val(cdate);
    var dformat = "{{ env('DATE_FORMAT') }}";
    $("#app_date, #agent_date, #dob-datepicker").inputmask("dd-mm-yyyy", {
        separator: "-",
        alias: "dd-mm-yyyy",
        placeholder: "dd-mm-yyyy"
    });
    $(document).on("click", "input[name='customer_id_detail']",function() {
        $('#get_modal_details').modal('toggle');
        $('input[name="customer_code"]').val($(this).val());
        setTimeout(function(){ 
            $("input[name='customer_code']").trigger('blur');
        }, 300);
    });
    $("input[name='customer_code']").blur(function() {
        getCustomerDetails();
    });

    $('input[name="swift_code"]').on('blur', function(e) {
        getSwiftDetails();
    });
    $(document).on("click", "input[name='cashcustomer_id_detail']",function() {
        $('#get_cashcustomer_modal_details').modal('toggle');
        $('input[name="pno_nric"]').val($(this).val());
        setTimeout(function(){ 
            getCashCustomerDetails();
        },300);
    });
    $("input[name='pno_nric']").blur(function() {
        getCashCustomerDetails();
    });
    $(document).on("click", "input[name='ccontact_id_detail']",function() {
       $('#get_customercontacts_modal_details').modal('toggle');
        var val = $(this).val();
        var valarray = val.split('|');
        $('input[name="contacts"]').val(valarray[0]);
        $('input[name="contacts"]').attr('readonly', true);
        $('input[name="remarks"]').val(valarray[1]);
        $('#country').val(valarray[3]);
        $('#country').attr('readonly', true);
    });
    $(document).on("click", "input[name='currency_id_detail']",function() {
        $('#get_currency_modal_details').modal('toggle');
        var val = $(this).val();
        var valarray = val.split('|');
        $('input[name="code"]').val(valarray[0]);
        $( "#code" ).trigger( "blur" );
    });

    $(document).on("click", "input[name='beneficiary_id_detail']",function() {
        $('#get_beneficiary_modal_details').modal('toggle');
        var val = $(this).val();
        var valarray = val.split('|');
        $('input[name="pp_name"]').val(valarray[1]);
        setTimeout(function(){ 
            getbeneficiaryDetails();
        },300);
    });

    $("input[name='pp_name']").blur(function() {
        getbeneficiaryDetails();
    });
    $("input[name='ref_no']").blur(function() {
        var val = $(this).val();
        getddtransactionDetails(val);        
    });
    $("input[name='code']").blur(function() {
        getCurrency();
    });
});

function getSwiftAutoFill(swift){
    if(swift.length > 3){
        $.ajax({
            type: "POST",
            url: "{{route('swift.getSwiftAutoFill')}}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : {
                code : swift,
            },
            beforeSend: function() {},
            success:function(result){
                if(result.status == 200){
                    $("#suggesstion-box").html(result.str);
                } else{
                    $("#suggesstion-box").html('');
                }
            }
        });
    } else {
        $("#suggesstion-box").html('');
    }
}
function selectedCode(code) {
    $('#swift_code').val(code);
    $("#suggesstion-box").html('');
    getSwiftDetails();
}
function cust_cla() {
    var rate = $('input[name="ex_rate"]').val();
    var amount = $('input[name="amount"]').val().split(",").join("");

    if(rate){        
        var lamount = rate*amount;
        lamount = lamount.toFixed(2);
        lamount = lamount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $('input[name="c_exrate"]').val(lamount); 
        $('input[name="c_exrate"]').mask("000,000,000.00", {reverse: true});
        $('input[name="c_comm"]').val(0.00);               
        /*$('input[name="c_sgdamount"]').val(0.00);*/
        cust_tot();                        
    }
}

function cust_tot() {
    var c_total = parseFloat($('input[name="c_exrate"]').val().split(",").join(""))+parseFloat($('input[name="c_comm"]').val());
        c_total = c_total.toFixed(2);
        c_total = c_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $('input[name="c_tot"]').val(c_total);    
}

function getSwiftDetails() {
    var swift_code = $("input[name='swift_code']").val();
    $.ajax({
        url: "{{route('swift.getdetailsbycode')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            code : swift_code,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                $('input[name="bank"]').val(response.details.Bank);
                $('input[name="branch_address"]').val(response.details.BankBranch);
            } else {
                $('input[name="bank"]').val('');
                $('input[name="branch_address"]').val('');
            }   
        }  
    });
}
function getCustomerDetails() {
    if($("input[name='customer_code']").val() != 'CASH'){
        $('#pno_nric_id').hide();
        $('#cconact_id').show();
        $('input[name="contacts"]').prop('readonly', false);
        $('input[name="pno_nric"]').prop('readonly', true);
    } else {
        $('#pno_nric_id').show();
        $('#cconact_id').hide();
        $('input[name="contacts"]').prop('readonly', true);
        $('input[name="pno_nric"]').prop('readonly', false);
    }
    $('#ref_no').val('');
    $('.bankPayment').show();
    var cusid = $("input[name='customer_code']").val();
    $.ajax({
        url: "{{route('buysell.customerdetails')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            cusid : cusid,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                $('input[name="customer_name"]').val(response.data.CustName);
                $('input[name="pno_nric"]').val(response.data.NRICNo);
                $('input[name="dob"]').val(response.data.DOB);
                $('#nationality').val(response.data.Nationality);
                $('input[name="contacts"]').val(response.data.Phone);
                $('input[name="address1"]').val(response.data.CompAddress1);
                $('input[name="address2"]').val(response.data.CompAddress2);
                $('#country').val(response.data.Country);
                $('input[name="postal_code"]').val(response.data.PostalCode);
                $('input[name="phone_no"]').val(response.data.CompPhone1);
            } else {
                $('input[name="customer_name"]').val('');
                $('input[name="pno_nric"]').val('');
                $('input[name="dob"]').val('');
                $('#nationality').val('Select Nationality');
                $('input[name="contacts"]').val('');
                $('input[name="address1"]').val('');
                $('input[name="address2"]').val('');
                $('#country').val('');
                $('input[name="postal_code"]').val('');
                $('input[name="phone_no"]').val('');
                $.alert({
                    title: 'Customer Code Not Found!',
                    content: 'Customer Code Not Found!',
                });
            }
        },      
    });
}

function getCashCustomerDetails() {
    var id = $("input[name='pno_nric']").val();
    if( id != ''){
        $.ajax({
            url: "{{route('cashcustomer.getcashcustomerdetails')}}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : {
                id : id,
            },
            beforeSend: function() {},
            success: function(response){
                if(response.status == 200){
                    $('input[name="customer_name"]').val(response.data.Name);
                    $('input[name="dob"]').val(response.data.DOB);
                    $('input[name="contacts"]').val(response.data.PhoneNo);
                    $('input[name="address1"]').val(response.data.Address1);
                    $('input[name="address2"]').val(response.data.Address2);
                    $('input[name="postal_code"]').val(response.data.PostalCode);
                    $('input[name="phone_no"]').val(response.data.PhoneNo);
                    $('#country').val(response.data.Address4);
                    $('#nationality').val(response.data.Nationality);
                    $('#customer_type').val(response.data.MCType);
                } else {
                    $('input[name="customer_name"]').val('');
                    $('input[name="dob"]').val('');
                    $('input[name="contacts"]').val('');
                    $('input[name="address1"]').val('');
                    $('input[name="address2"]').val('');
                    $('input[name="postal_code"]').val('');
                    $('input[name="phone_no"]').val('');
                    $('#country').val('');
                    $('#customer_type').val('');
                    $('#nationality').val('Select Nationality');
                    $.alert({
                        title: 'Cash Customer Code Not Found!',
                        content: 'Cash Customer Code Not Found!',
                    });
                }
            },      
        });
    }
}

function getbeneficiaryDetails() {
    var customer_code = $('input[name="customer_code"]').val();
    var pp_nric = $('input[name="pno_nric"]').val();
    var pp_name = $('input[name="pp_name"]').val();
   /* if(pp_nric != ''){
        pp_name = '';
    }*/
    $.ajax({
        url: "{{route('ddtransaction.getbeneficiary')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            customer_code : customer_code,
            pp_nric : pp_nric,
            pp_name : pp_name,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                $('input[name="pp_nric"]').val(response.data.beneficiary_id);
                $('input[name="pp_name"]').val(response.data.BeneName);
                $('input[name="pp_address1"]').val(response.data.BeneAddress1);
                $('input[name="pp_address2"]').val(response.data.BeneAddress2);
                $('input[name="pp_telephone"]').val(response.data.BeneMobileNo);
                $('#pp_country').val(response.data.BeneCountry);
                $('input[name="bank"]').val(response.data.BeneBankName);
                $('input[name="branch_address"]').val(response.data.BeneBankBranch);
                $('input[name="ac_no"]').val(response.data.BeneBankAccNo);
                $('input[name="swift_code"]').val(response.data.SwiftCode);
                if($('#pp_country').find(':selected').attr('group') == "USA"){
                    if($("#pp_country").is(":visible")){
                        $('#clearCode').show();
                        $('#iban').hide();
                    } else {
                        $('#clearCode').hide();
                        $('#iban').hide();
                    }
                } else if($('#pp_country').find(':selected').attr('group') == "Europe"){
                    if($("#pp_country").is(":visible")){
                        $('#clearCode').hide();
                        $('#iban').show();
                    } else {
                        $('#iban').hide();
                        $('#clearCode').hide();
                    }
                } else {
                    $('#clearCode').hide();
                    $('#iban').hide();
                }
            } else {
                $('input[name="pp_address1"]').val('');
                $('input[name="pp_address2"]').val('');
                $('#pp_country').val('');
                $('input[name="bank"]').val('');
                $('input[name="branch_address"]').val('');
                $('input[name="ac_no"]').val('');
                $('input[name="swift_code"]').val('');
            }
        },      
    });  
}

function getddtransactionDetails(val) {
    $.ajax({
        url : "{{route('ddtransaction.getddtransaction')}}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            val : val,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                $('#save_btn').prop('disabled', true);
                $('#update_btn').prop('disabled', false);
                $('input[name="customer_code"]').val(response.data.CustCode);
                $('input[name="customer_name"]').val(response.data.CustName);
                $('input[name="pno_nric"]').val(response.data.CustPPNo);
                $('#dob-datepicker').val(response.data.DOB);
                $('#nationality').val(response.data.CustNationality);
                $('input[name="contacts"]').val(response.data.Name);
                $('#Purpose').val(response.data.Purpose);
                $('input[name="address1"]').val(response.data.CompAddress1);
                $('input[name="address2"]').val(response.data.CompAddress2);
                $('#country').val(response.data.CompAddress3);
                $('input[name="postal_code"]').val(response.data.Postal);
                $('input[name="phone_no"]').val(response.data.Telephone);
                $('#customer_type').val(response.data.CustTTType);
                $('input[name="code"]').val(response.data.CurrencyCode);
                $('input[name="curr_name"]').val(response.data.CustCode);
                $('input[name="ex_rate"]').val(response.data.ExchRate);
                /*$('input[name="conv_rate"]').val(parseFloat(response.data.FAmount).toFixed(2));*/
                $('input[name="conv_rate"]').val(parseFloat(1/response.data.ExchRate).toFixed(8));
                $('input[name="amount"]').val(response.data.FAmount);
                $('input[name="pp_nric"]').val(response.data.BenPPNo);
                $('input[name="pp_name"]').val(response.data.BenName);
                $('input[name="pp_address1"]').val(response.data.BenAddress1);
                $('input[name="pp_address2"]').val(response.data.BenAddress2);
                $('#pp_country').val(response.data.BenAddress3);
                $('#pp_telephone').val(response.data.BeneMobileNo);   
                $('input[name="bank"]').val(response.data.BenBank);
                $('input[name="branch_address"]').val(response.data.BenBankBranch);
                $('input[name="ac_no"]').val(response.data.BenAccNo);
                $('input[name="swift_code"]').val(response.data.SwiftCode);
                $('input[name="agentcode"]').val(response.data.AgentCode);
                $('input[name="agentrate"]').val(response.data.AgentRate);
                $('input[name="c_exrate"]').val(response.data.LAmount);
                $('input[name="c_comm"]').val(parseFloat(response.data.Comm).toFixed(2));
                $('input[name="c_tot"]').val(response.data.TotalAmount);   
                $('input[name="MCType"]').val(response.data.MCType);
                $('input[name="CompName"]').val(response.data.CompName);
                $('input[name="AgentTTType"]').val(response.data.AgentTTType);
                $('input[name="c_sgdamount"]').val('0.00');
                $('input[name="a_sgdamount"]').val('0.00');
                $('input[name="a_exrate"]').val(parseFloat(response.data.AgentExRate).toFixed(2));
                $('input[name="a_tot"]').val(parseFloat(response.data.AgentTotal).toFixed(2));
                $('input[name="a_comm"]').val(parseFloat(response.data.AgentCommission).toFixed(2));
                $('input[name="ddno"]').val(response.data.DDNo);
                $('input[name="agentname"]').val(response.data.AgentName);
                $('input[name="collectPaymentType"]').val([response.data.CollectPayment]);
                var date = new Date(response.data.TranDate);
                /*var cdate = date.getDate()+'-'+date.getMonth()+'-'+date.getFullYear();*/
                var cdate = ("0" + date.getDate()).slice(-2)+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+date.getFullYear();
                $('#app_date').val(cdate);
                $('#agent_date').val(cdate);
                 if($('#pp_country').find(':selected').attr('group') == "USA"){
                    if($("#pp_country").is(":visible")){
                        $('#clearCode').show();
                        $('#iban').hide();
                    } else {
                        $('#clearCode').hide();
                        $('#iban').hide();
                    }
                } else if($('#pp_country').find(':selected').attr('group') == "Europe"){
                    if($("#pp_country").is(":visible")){
                        $('#clearCode').hide();
                        $('#iban').show();
                    } else {
                        $('#iban').hide();
                        $('#clearCode').hide();
                    }
                } else {
                    $('#clearCode').hide();
                    $('#iban').hide();
                }
                if(response.data.CollectPayment == 'Cash'){
                    $('.cashPickup').show();
                    $('.bankTransfer').hide();
                    $('.bankPayment').hide();
                } else {
                    $('.bankTransfer').show();
                    $('.cashPickup').hide();
                    $('.bankPayment').show();
                }
                $('#amount,#c_exrate,#c_tot').trigger("change");
            } else {
                $('#save_btn').prop('disabled', false);
                $('#update_btn').prop('disabled', true);
                $('input[name="customer_code"]').val('');
                $('input[name="customer_name"]').val('');
                $('input[name="pno_nric"]').val('');
                $('#dob-datepicker').val('');
                $('#nationality').val('');
                $('input[name="contacts"]').val('');
                $('#Purpose').val('');
                $('input[name="address1"]').val('');
                $('input[name="address2"]').val('');
                $('#country').val('');
                $('input[name="postal_code"]').val('');
                $('input[name="phone_no"]').val('');
                $('#customer_type').val('');
                $('input[name="code"]').val('');
                $('input[name="curr_name"]').val('');
                $('input[name="ex_rate"]').val('');
                $('input[name="conv_rate"]').val('');
                $('input[name="amount"]').val('');
                $('input[name="pp_nric"]').val('');
                $('input[name="pp_name"]').val('');
                $('input[name="pp_address1"]').val('');
                $('input[name="pp_address2"]').val('');
                $('#pp_country').val('');
                $('input[name="bank"]').val('');
                $('input[name="branch_address"]').val('');
                $('input[name="ac_no"]').val('');
                $('input[name="swift_code"]').val('');
                $('#app_date').val('');
                $('#agent_date').val('');
                $('input[name="customer_code"]').val("CASH");
                $('input[name="customer_name"]').val("CASH CUSTOMER");
                $('input[name="agentcode"]').val('');
                $('input[name="agentrate"]').val('');
                $('input[name="c_exrate"]').val('');
                $('input[name="c_comm"]').val('');
                $('input[name="c_tot"]').val('');
                $('input[name="MCType"]').val('');
                $('input[name="CompName"]').val('');
                $('input[name="AgentTTType"]').val('');
                $('input[name="ddno"]').val('');
                $('input[name="a_exrate"]').val('');
                $('input[name="c_sgdamount"]').val('');
                $('input[name="a_sgdamount"]').val('');
                $('input[name="a_tot"]').val('');
                $('input[name="a_exrate"]').val('');
                $('input[name="a_tot"]').val('');
                $('input[name="agentname"]').val('');
                $.alert('Unpost not found');
            }
        },      
    });  
}

function submitForm(type) {
    if(type == 'save'){
        var purpose = $('#Purpose').val();
        var amount = $('input[name="amount"]').val();
        var ex_rate = $('input[name="ex_rate"]').val();
        var conv_rate = $('input[name="conv_rate"]').val();
        var nationality = $('#nationality').val();
        /*var pp_nric = $('input[name="pp_nric"]').val();*/
        var pp_name = $('input[name="pp_name"]').val();
        var bank = $('input[name="bank"]').val();
        var branch_address = $('input[name="branch_address"]').val();
        var ac_no = $('input[name="ac_no"]').val();
        var swift_code = $('input[name="swift_code"]').val();  
        var pp_telephone = $('input[name="pp_telephone"]').val();  
        var collectPaymentText = $('input[name="collectPaymentType"]:checked').val();
        if(purpose == '' || amount == '' || ex_rate == '' || conv_rate == '' || nationality == '' || pp_name == '' || pp_telephone == '' || (collectPaymentText == 'Bank' && (bank == '' || branch_address == '' || ac_no == '' || swift_code == '')) ){
            $('#frm_ddtransaction').addClass('was-validated');
            return false;
        }
        $.ajax({
            url: "{{ route('ddtransaction.unpost_insert') }}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : $('#frm_ddtransaction').serialize(),
            success: function(response){
                $('#frm_ddtransaction').removeClass('was-validated');
                if(response.status == 200){
                    success_toastr(response.content);
                    $('#frm_ddtransaction')[0].reset();
                    $('.bankPayment').show();
                    /*RefNo();*/
                }else {
                    $.each(response.content, function(v, t){
                        danger_toastr(t);
                    });
                     $('#frm_ddtransaction')[0].reset();
                }
            },
        });   
    } else if(type == 'update'){
        var purpose = $('input[name="Purpose"]').val();
        var amount = $('input[name="amount"]').val();
        var ex_rate = $('input[name="ex_rate"]').val();
        var conv_rate = $('input[name="conv_rate"]').val();
        var bank = $('input[name="bank"]').val();
        var branch_address = $('input[name="branch_address"]').val();
        var ac_no = $('input[name="ac_no"]').val();
        var swift_code = $('input[name="swift_code"]').val();   
        var collectPaymentText = $('input[name="collectPaymentType"]:checked').val();
        if(purpose == '' || amount == '' || ex_rate == '' || conv_rate == '' || (collectPaymentText == ': Bank Transfer' && (bank == '' || branch_address == '' || ac_no == '' || swift_code == '')) ){
            $('#frm_ddtransaction').addClass('was-validated');
            return false;
        }
        $.ajax({
            url: "{{ route('ddtransaction.unpost_update') }}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : $('#frm_ddtransaction').serialize(),
            success: function(response){
                $('#frm_ddtransaction').removeClass('was-validated');
                if(response.status == 200){
                    success_toastr(response.content);
                    $('#frm_ddtransaction')[0].reset();
                    $('.bankPayment').show();
                    /*RefNo();*/
                }else {
                    $.each(response.content, function(v, t){
                        danger_toastr(t);
                    });
                    $('#frm_ddtransaction')[0].reset();
                }
            },
        });  
    } else if (type == 'clear') {
        $('#frm_ddtransaction')[0].reset();
        /*RefNo();*/
    }
}

function RefNo() {
    $.ajax({
        url : "{{ route('ddtransaction.getunpostRefNo') }}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response){
            if(response.status == 200){
                $('input[name="ref_no"]').val(response.data);
            }
        },      
    });
}

function getCurrency() {
    $.ajax({
        url : "{{ route('ddtransaction.getCurrency') }}",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            id : $('input[name="code"]').val(),
        },
        success: function(response){
            if(response.status == 200){
                $('input[name="curr_name"]').val(response.data.CurrencyName);
                $('input[name="ex_rate"]').val(response.data.AvgCost);
                var conv = parseFloat(response.data.AvgCost);
                var conv1 = 1;
                var tot = conv1/conv;
                $('input[name="conv_rate"]').val(tot.toFixed(10));
            } else {
                $('input[name="curr_name"]').val('');
                $('input[name="ex_rate"]').val('');
                $('input[name="conv_rate"]').val('');
            }
        },      
    });
}

function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}

function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}

var dateToday = new Date();
$( function() {
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 15);
    $( "#app_date, #agent_date").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true
    });
    $( "#dob-datepicker").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: start.getFullYear() + ':' + end.getFullYear(),
        maxDate: dateToday,
    });
});
</script>
@endsection
