@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<style type="text/css">
    .picker__holder{
        max-width: none;
        width: 335px;
    }
    #buy_sell_transaction_wrapper{
        padding-right: 0px;
        padding-left: 0px;
    }
</style>
@endsection

@section('main-content')
<div class="breadcrumb">
    <h1>Buy Sell</h1>
    <ul>
        <li>Insert</li>
        <li class="active"><a href="{{route('buyselltransactionreport')}}" >Listing</a></li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>

<form class="needs-validation" novalidate name="customer" id="form_buysell" method="POST" action="#" autocomplete="off">
{{ csrf_field() }}
<div class="row mb-12 master-main">
    <div class="col-md-12 mb-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                     <div class="col-lg-8 col-sm-12">
                        <div class="row m-1">
                            <label for="voucher_no" class="col-sm-2 col-form-label text-rights">VoucherNo</label>
                            <input type="textbox" name="voucher_no"  class="col-sm-2 form-control allownumeric" id="voucher_no" placeholder="Enter Voucher No" value="{{$vid}}" readonly>
                            <label for="location" class="col-sm-2 col-form-label text-rights">Location</label>
                            <input type="textbox" name="location" value="{{ Auth::user()->Location }}"  class="col-sm-2 form-control allownumeric" id="location" placeholder="Enter Location">
                            <label for="order-datepicker" class="col-sm-1 col-form-label text-rights" style="   padding: 7px 2px;text-align: right;">Date</label>
                            <div class="col-sm-3">
                                <input type="text" value="<?php echo date("Y-m-d"); ?>"  id="order-datepicker" class="form-control" placeholder="dd-mm-yyyy" name="date">
                            </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-sm-12">
                          <div class="row">
                            <div class="col-md-12 text-left">
                                <span class="danger" style="color:red;">Press F9 to Release Deal Transaction</span>
                               <!--  <button class="btn btn-primary m-1" type="button" id="check_details">Check Details</button> -->

                            </div>
                            <div class="col-md-6 text-right">
                                <!-- <button class="btn btn-primary m-1" type="button" id="accuity_search">Accuity Search</button> -->
                            </div>
                          </div>
                     </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 mb-12">
       <div class="card">
           <div class="card-body">
            <div class="card-title">Customer Details</div>
               <div class="row">
                    <div class="col-lg-6 col-sm-12 master-main-sec">
                        <div class="card mb-6">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="company_name" class="col-sm-3 col-form-label">
                                            <a onclick="GetModalDetails('customer');" data-toggle="modal" data-target="#get_modal_details" href="">Customer Code</a> <span class="mandatory">*</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input type="textbox" name="customer_code" class="form-control" autocomplete="off" id="customer_code" placeholder="Enter Customer code" onkeyup="this.value = this.value.toUpperCase();" required >
                                                <div class="invalid-feedback">Enter Customer code</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-9">
                                                <input type="textbox" name="customer_name" class="form-control" autocomplete="off" id="customer_name" required >
                                                <div class="invalid-feedback">Enter Customer Name</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="pno_nric" class="col-sm-3 col-form-label">P.No / Nric <span class="mandatory">*</span></label>
                                            <div class="col-sm-9 input-group mb-3">
                                                <input type="textbox" name="pno_nric" class="form-control" autocomplete="off" id="pno_nric" placeholder="Enter P.No / Nric" required >

                                                <div class="input-group-append" id="pno_nric_id" onclick="GetCashCustomerModal('cashcustomer');"data-toggle="modal" data-target="#get_cashcustomer_modal_details" style="cursor: pointer;">
                                                    <span class="input-group-text" id="basic-addon2">
                                                        <i class="i-Business-Mens"></i>
                                                    </span>
                                                </div>

                                                <div class="invalid-feedback">Enter P.No / Nric</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="dob-datepicker" class="col-sm-3 col-form-label">Birth Date</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="dob-datepicker" class="form-control" placeholder="dd-mm-yyyy" name="dob">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="nationality" class="col-sm-3 col-form-label">Nationality</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="nationality" id="nationality">
                                                    <option value="">Select Nationality</option>
                                                    @foreach($nationality as $value)
                                                    <option value="{{trim($value->Code,' ')}}">{{trim($value->Description,' ')}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="contacts" class="col-sm-3 col-form-label">Contacts <span class="mandatory">*</span></label>
                                            <div class="col-sm-9 input-group mb-3">
                                                <input type="textbox" name="contacts" class="form-control" autocomplete="off" id="contacts" placeholder="Enter Contacts" required >
                                                <div class="input-group-append" id="cconact_id"  onclick="GetCustomerContactaModal('customercontacts');" data-toggle="modal" data-target="#get_customercontacts_modal_details" style="cursor: pointer;">
                                                    <span class="input-group-text" id="basic-addon1">
                                                        <i class="i-Business-Mens"></i>
                                                    </span>
                                                </div>
                                                <div class="invalid-feedback">Enter Contacts</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="remarks" class="col-sm-3 col-form-label">Remarks 
                                                <!-- <span class="mandatory">*</span> -->
                                            </label>
                                            <div class="col-sm-9">
                                                <input type="textbox" name="remarks" class="form-control" autocomplete="off" id="remarks" placeholder="Enter Remarks" >
                                                <!-- <div class="invalid-feedback">Enter Remarks</div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3 col-form-label">Mode of Payment</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="mode_payment" id="mode_payment">
                                                    <option>Select Payment</option>
                                                    @foreach($settlementmode as $value)
                                                    <option value="{{$value->id}}" <?php if($value->SettlementCode == 'CASH') { echo "selected"; }?> >{{$value->SettlementCode}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 master-main-sec">
                        <div class="card-body balancecls">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <label for="address" class="col-sm-5 col-form-label balancelabel">Customer Balance </label>
                                        <div class="col-sm-7">
                                            <input type="textbox" class="form-control balancetext" name="cusbalance" id="cusbalance" readonly value="">
                                            <input type="hidden" name="chosendealstr" id="chosendealstr" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-6">                            
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">Country <span class="mandatory">*</span></label>
                                            <div class="col-sm-9">
                                                <!-- <input type="textbox" name="country" class="form-control" autocomplete="off" id="country" placeholder="Enter Country" required > -->
                                                <select class="form-control" name="country" id="country">
                                                    <option value="">Select Country</option>
                                                    @foreach($country as $value)
                                                    <option value="{{trim($value->Country, ' ')}}">{{trim($value->Country, ' ')}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">Enter Country</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">Postal Code <span class="mandatory">*</span></label>
                                            <div class="col-sm-4 input-group mb-3">
                                                <input type="textbox" name="postal_code" class="form-control" autocomplete="off" id="postal_code" placeholder="Enter Postal Code" required >
                                                <div class="input-group-append" id="postalcodeserach" style="cursor: pointer;">
                                                    <span class="input-group-text" id="basic-addon1">
                                                        <i class="i-Business-Mens"></i>
                                                    </span>
                                                </div>
                                                <div class="invalid-feedback">Postal Code</div>

                                            </div>
                                            <label class="col-sm-2 col-form-label">Phone No <span class="mandatory">*</span></label>
                                            <div class="col-sm-3">
                                                <input type="textbox" name="phone_no" class="form-control" autocomplete="off" id="phone_no" placeholder="Enter Phone No" required >
                                                <div class="invalid-feedback">Enter Phone No</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="address" class="col-sm-3 col-form-label">Address <span class="mandatory">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="textbox" name="address" class="form-control" autocomplete="off" id="address" placeholder="Enter Address" required >
                                                <div class="invalid-feedback">Enter Address</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label for="address1" class="col-sm-3 col-form-label">Address1 </label>
                                            <div class="col-sm-9">
                                                <input type="textbox" name="address1" class="form-control" autocomplete="off" id="address1" placeholder="Enter Address1" >
                                                <div class="invalid-feedback">Enter Address1</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">Unit No </label>
                                            <div class="col-sm-9">
                                                <input type="textbox" name="unit_no" class="form-control" autocomplete="off" id="unit_no" placeholder="Enter Unit No"  >
                                                <div class="invalid-feedback">Enter Unit No</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">Mc Type 
                                            </label>
                                            <div class="col-sm-9">

                                                <select class="form-control" name="mc_type" id="mc_type" >
                                                    <option value="">Select MC Type</option> 
                                                    @foreach ($general_type as $key => $value)
                                                        <option value="{{$value}}" @if(old("mc_type")==$value) selected="selected" @endif>{{$value}}</option> 
                                                    @endforeach                                                              
                                                </select>
                                                <!-- <input type="textbox" name="mc_type" class="form-control" autocomplete="off" id="mc_type" placeholder="Enter Mc Type"> -->
                                                <!-- <div class="invalid-feedback">Enter Mc Type</div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">Identifier Type</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" name="identifier_type" id="identifier_type">
                                                    <option>Select Identifier Type</option>
                                                    @foreach($identifire as $value)
                                                    <option value="{{$value}}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <label class="col-sm-2 col-form-label">DOE</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="doe" class="form-control" autocomplete="off" id="doe-datepicker" placeholder="dd-mm-yyyy" required >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
           </div>
       </div> 
    </div>

    <div class="col-md-12 mb-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Buy Sell Transaction</div>
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table id="buy_sell_transaction" class="display nowrap table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col" class="text-center">#</th>
                                    <th scope="col" class="text-center">Type</th>
                                    <th scope="col" class="text-center">Currency</th>
                                    <th scope="col" class="text-center" style="width: 350px;">Description</th>
                                    <th scope="col" class="text-center">F.Amount</th>
                                    <th scope="col" class="text-center">Rate</th>
                                    <th scope="col" class="text-center">L.Amount</th>
                                </tr>
                            </thead>
                            <tbody id="buy_sell_transaction_table_body">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>

                <form id='frm_buy_sell'> 
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-hover mb-3 table-bordered">
                                <thead class="">
                                    <tr>
                                        <th class="text-center" scope="col" style="width: 150px;">Type</th>
                                        <th class="text-center" scope="col">
                                            <a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">Currency</a>
                                        </th>
                                        <th class="text-center" scope="col" style="width: 350px;">Description</th>
                                        <th class="text-center" scope="col">F.Amount</th>
                                        <th class="text-center" scope="col">Rate</th>
                                        <th class="text-center" scope="col">L.Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input value="" type="hidden" name="buy_sell_tr_td_id" id="buy_sell_tr_td_id">
                                            <select class="form-control" name="in_type" id="in_type">
                                                <option value="Buy">Buy</option>
                                                <option value="Sell">Sell</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="hidden" name="nextid" value="0">
                                            <input value="" type="text" id="in_currency" name="in_currency" class="form-control" placeholder="Currency" onkeyup="this.value = this.value.toUpperCase();">
                                        </td>
                                        <td>
                                            <input value="" type="text" name="in_description" class="form-control" placeholder="Description" id="in_description">
                                        </td>
                                        <td>
                                            <input value="" type="text" name="in_famount" id="in_famount" class="form-control" placeholder="F.Amount">
                                        </td>
                                        <td>
                                            <input value="" type="text" name="in_rate" class="form-control" placeholder="Rate" max="" min="">
                                            <input type="hidden" name="allowedmaxrate" id="allowedmaxrate" value="">
                                            <input type="hidden" name="allowedminrate" id="allowedminrate" value="">
                                            <input type="hidden" name="dealdetailid" id="dealdetailid" value="">
                                        </td>
                                        <td>
                                            <input value="" type="text" name="in_lamount" id="in_lamount" class="form-control" placeholder="L.Amount">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-12 table-responsive">
                            <table class="table table-hover mb-3 table-bordered">
                                <thead class="">
                                    <tr>
                                        <th class="text-center" scope="col">Average Cost</th>
                                        <th class="text-center" scope="col">Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input value="" name="in_average_cost" type="text" class="form-control" placeholder="Average Cost" readonly>
                                        </td>
                                        <td>
                                            <input value="" name="in_stock" type="text" class="form-control" placeholder="Stock" readonly>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-8 col-sm-12 mt-4 mb-3 text-right">
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="row">
                                        <label for="in_total" class="col-sm-6 mt-4 col-form-label text-right">Total</label>
                                        <input type="textbox" name="in_total" class="col-sm-6 mt-4 form-control" autocomplete="off" id="in_total" placeholder="Enter Total">

                                        <input type="hidden" name="buyamt" id="buyamt" value="">
                                        <input type="hidden" name="sellamt" id="sellamt" value="">
                                        <input type="hidden" name="curravgcost" id="curravgcost" value="">
                                        <input type="hidden" name="variance" id="variance" value="">
                                        
                                    </div>
                                </div>
                                <div class="col-sm-5 mt-3">
                                    <button class="btn m-1 btn-primary" type="button" id="Add_data_d" onclick="Add_data();">Add</button>
                                    <button class=" btn m-1 btn-primary" type="button" id="Clear_data_d" onclick="Clear_data();">Clear</button>
                                    <button class=" btn m-1 btn-primary" type="button" id="Delete_data_d" onclick="Delete_data();">Delete</button> 
                                </div>
                            </div>
                         </div>
                    </div>
                </form>
            </div>
        </div>    
    </div>


    <div class="col-md-12 mb-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-sm-12">
                        <div class="row">
                            <label style="padding: 7px 0px 10px 5px;" for="" class="col-form-label col-md-2">Total Transactions</label>
                            <input type="textbox" name="tot_transaction"  class="col-md-2 form-control allownumeric" id="tot_transaction" value="0">
                            <label for="" class="col-form-label col-md-2">Last Buy</label>
                            <input type="textbox" name="last_buy"  class="col-md-2 form-control allownumeric" id="last_buy" value="0.00">
                            <label for="" class="col-form-label col-md-2">Last Sell</label>
                            <input type="textbox" name="last_sell"  class=" col-md-2 form-control allownumeric" id="last_sell" value="0.00">
                        </div>
                    </div>

                    <div class="col-lg-6 col-sm-12 text-right">
                        <a href="{{route('buysell.view')}}" class="btn btn-primary m-1" id="clear_form"> Clear </a>
                        <button class="btn btn-primary m-1" type="button" onclick="submitForm('save');" id=""> Save </button>
                        <!-- <button class="btn btn-primary m-1" type="button" id="">Save/Print (F3)</button> -->
                        <!-- <button class="btn btn-primary m-1" type="button" onclick="submitForm('esc');">Close (Esc)</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</form>
@include('modal.currency')
@include('modal.customer')
@include('modal.customercontact')
@include('modal.cashcustomer')
@include('modal.dealtransactions')
@endsection


@section('page-js')
<!-- <script src="{{asset('assets/js/form.validation.script.js')}}"></script> -->
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
<script src="{{asset('assets/js/ladda.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
<script src="{{asset('assets/js/modal.script.js')}}"></script>
<script src="{{asset('assets/js/modal.details.js')}}"></script>
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
@endsection

@section('bottom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script type="text/javascript">

 
$(document).ready(function(){

    $('#postalcodeserach').hide();
    $('input[name="postal_code"]').keypress(function(){
        if($(this).val().length >= 4){
            $('#postalcodeserach').show();
        } else {
            $('#postalcodeserach').hide();
        }
    });
    $('input[name="postal_code"]').blur(function(){
        if($(this).val().length >= 4){
            $('#postalcodeserach').show();
        } else {
            $('#postalcodeserach').hide();
        }
    });
    $(document).on('click', '#postalcodeserach', function(){
        $("#country").val("SINGAPORE");
        var postal = $('input[name="postal_code"]').val();
        if(postal.length >= 4){
            $.ajax({
                url: "./postalcode",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    postal : postal,
                },
                beforeSend: function() {},
                success: function(response){
                    if(response.status == 200 && response.postal != 'Error'){
                        console.log(response.postal);
                        var mix = response.postal.block+' '+response.postal.Road;
                        $('input[name="address"]').val(mix);
                        $('input[name="address1"]').val(response.postal.buliding);
                    } else {
                        console.log(response.postal);
                        $('input[name="address"]').val('');
                        $('input[name="address1"]').val('');

                    }
                }
            });
        } else {
            $('input[name="address"]').val('');
            $('input[name="address1"]').val('');
        }
    });

    $('#identifier_type').on('change', function() {
        $('#doe-datepicker').val('');
        if($('#identifier_type').val() == "Singapore Citizen" || $('#identifier_type').val() == "Permanent Residence"){
            var date = new Date();
            var mnt = date.getMonth()+1;
            var yrs = date.getFullYear()+30;
            var cdate = date.getDate()+'-'+mnt+'-'+yrs;
            $('#doe-datepicker').val(cdate);
        } 
    });

    $('#in_famount,#in_lamount,#in_total').mask("000,000,000.00", {reverse: true});

    var date = new Date();
    var mnt = date.getMonth()+1;
    var cdate = date.getDate()+'-'+mnt+'-'+date.getFullYear();
    $('#order-datepicker').val(cdate);

    $("#order-datepicker,#dob-datepicker,#doe-datepicker").inputmask("dd-mm-yyyy", {
        separator: "-",
        alias: "dd-mm-yyyy",
        placeholder: "dd-mm-yyyy"
    });

    $('input[name="customer_code"]').val("CASH");
    $('input[name="customer_name"]').val("CASH CUSTOMER");
    if($("input[name='customer_code']").val() != 'CASH'){
        $('#pno_nric_id').hide();
        $('#cconact_id').show();
        $('input[name="contacts"]').prop('readonly', false);
        $('input[name="pno_nric"]').prop('readonly', true);
        $('input[name="customer_name"]').val("");
    } else {
        $('#pno_nric_id').show()
        $('#cconact_id').hide();;
        //$('input[name="contacts"]').prop('readonly', true);
        //$('input[name="pno_nric"]').prop('readonly', false);
        $('input[name="customer_name"]').val("CASH CUSTOMER");
    }
    $('#buy_sell_transaction_insert').DataTable({
        "paging": false,
        "bInfo": false,
        "searching": false
    });
    $('#buy_sell_transaction_insert1').DataTable({
        "paging": false,
        "bInfo": false,
        "searching": false
    });

    $('#in_type').change(function() {
      $('#in_type').css('border', '1px solid #ced4da');  
    });
    $('input[name="in_currency"]').keypress(function() {
      $('input[name="in_currency"]').css('border', '1px solid #ced4da');  
    });
    $('input[name="in_description"]').keypress(function() {
      $('input[name="in_description"]').css('border', '1px solid #ced4da');  
    });
    $('input[name="in_famount"]').keypress(function() {
      $('input[name="in_famount"]').css('border', '1px solid #ced4da');  
    });
    $('input[name="in_rate"]').keypress(function() {
      $('input[name="in_rate"]').css('border', '1px solid #ced4da');  
    });
    $('input[name="in_lamount"]').keypress(function() {
      $('input[name="in_lamount"]').css('border', '1px solid #ced4da');  
    });

    $(document).on("click", "#buy_sell_transaction tr",function() { 
        var cols = [];
        var nextid = $(this).next('tr').attr('id');
        //console.log(nextid);
        if(nextid != '' || nextid != 'undefined' || nextid != undefined){
            $('input[name="nextid"]').val(nextid);
        }else{
            $('input[name="nextid"]').val('0');
        }
        $(this).find('input').each(function (colIndex, c) {
            cols.push(c.value);
        });
        cols['4'] = cols['4'].replace(',', '');
        cols['6'] = cols['6'].replace(',', '');
        var final_data = cols.join(',');
        var final_data_arr = final_data.split(',');
        console.log(final_data_arr);
        final_data_arr[4] = final_data_arr[4].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        final_data_arr[6] = final_data_arr[6].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $('#buy_sell_tr_td_id').val(final_data_arr[0]);  
       // $('#in_type').html('<option value="Buy">Buy</option><option value="Sell">Sell</option>');
        $('#in_type').val(final_data_arr[1]);  
        $('input[name="in_currency"]').val(final_data_arr[2]);  
        $('input[name="in_description"]').val(final_data_arr[3]);
        $('input[name="in_famount"]').val(final_data_arr[4]);
        $('input[name="in_rate"]').val(final_data_arr[5]);
        $('input[name="in_average_cost"]').val(final_data_arr[5]);
        $('input[name="in_lamount"]').val(final_data_arr[6]);
        $('input[name="in_stock"]').val(final_data_arr[7]);
        $('input[name="dealdetailid"]').val('');

        // deal transaction list 
          //$('#in_type').html('<option value="'+final_data_arr[1]+'">'+final_data_arr[1]+'</option>');
        if(final_data_arr[8] != undefined && final_data_arr[8] !=''){
          $('#in_type').val(final_data_arr[1]);  
          $('#in_type').attr('disabled',true);   
          $('input[name="in_currency"]').attr('readonly',true); 
          $('input[name="in_description"]').attr('readonly',true); 
          $('input[name="in_rate"]').attr('readonly',true);  
          $('input[name="in_lamount"]').attr('readonly',true);
          $('input[name="dealdetailid"]').val(final_data_arr[8]);
        }else{
          $('#in_type').attr('disabled',false);   
          $('input[name="in_currency"]').attr('readonly',false); 
          $('input[name="in_description"]').attr('readonly',false); 
          $('input[name="in_rate"]').attr('readonly',false);  
          $('input[name="in_lamount"]').attr('readonly',false);
        }

    });


    $(document).on("click", "input[name='customer_id_detail']",function() {
        $('#get_modal_details').modal('toggle');
        $('input[name="customer_code"]').val($(this).val());
        setTimeout(function(){ 
            $("input[name='customer_code']").trigger('blur');
        }, 300);
    });
    
    $("input[name='customer_code']").blur(function() {
    	var val = $('input[name="customer_code"]').val();
    	if(val!='CASH'){
        	getCustomerDetails();
    	}
        //var uno = $('#unit_no').val();
        //alert(uno);
        //if(uno != '') {
        	// ragu need to work here
            
        //}
    });
    
    $(document).on("click", "input[name='currency_id_detail']",function() {
        $('#get_currency_modal_details').modal('toggle');
        var val = $(this).val();
        var valarray = val.split('|');
        $('input[name="in_currency"]').val(valarray[0]);
        setTimeout(function(){
            $("input[name='in_currency']").trigger('blur');
        }, 300);
    });

    $("input[name='in_currency']").blur(function() {
        //alert('sdfsd');
        getincurrencydetails();
    });

    $(document).on("click", "input[name='ccontact_id_detail']",function() {
       $('#get_customercontacts_modal_details').modal('toggle');
            var val = $(this).val();
            var valarray = val.split('|');
            $('input[name="contacts"]').val(valarray[0]);
            $('input[name="contacts"]').attr('readonly', true);
            $('input[name="remarks"]').val(valarray[1]);
            // $('input[name="remarks"]').attr('readonly', true);
            $('#doe-datepicker').val(valarray[2]);
            $('#doe-datepicker').attr('readonly', true);
            $('#country').val(valarray[3]);
            $('#country').attr('readonly', true);
    });
    
    $(document).on("click", "input[name='cashcustomer_id_detail']",function() {
        $('#get_cashcustomer_modal_details').modal('toggle');
        $('input[name="pno_nric"]').val($(this).val());
        setTimeout(function(){ 
            getCashCustomerDetails();
            
        }, 300);
    });

    $("input[name='pno_nric']").blur(function() {
        getCashCustomerDetails();
       // $('#get_deal_details').modal('toggle');
    });
});

$('input[name="in_famount"]').keyup(function(e) {
	this.value = this.value.replace(/[^0-9\.]/g,'');
    var avgRate = $('input[name="in_rate"]').val();
    var famount = $('input[name="in_famount"]').val().split(",").join(""); 
    if(avgRate){        
        var lamount = avgRate*famount;
        lamount = lamount.toFixed(2);
        lamount = lamount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $('input[name="in_lamount"]').val(lamount);               
    }
});

$('input[name="in_famount"]').blur(function(e) {
    var existingstock = $('input[name="in_stock"]').val();
    var transtype = $('#in_type').children("option:selected").html(); 
    existingstock = parseFloat(existingstock.replace(',',''));
    var enteramt = parseFloat($('input[name="in_famount"]').val().split(",").join(""));
    if((existingstock <= 0 || enteramt > existingstock) && transtype=='Sell') {
        $.alert("No Stock Available!");
        $('input[name="in_famount"]').val('0.00');
        $('input[name="in_lamount"]').val('0.00');
    }
});


$('input[name="in_rate"]').keyup(function(e) {
    this.value = this.value.replace(/[^0-9\.]/g,'');
    var avgRate = $('input[name="in_rate"]').val();
    var famount = $('input[name="in_famount"]').val().split(",").join("");
    //var maxrate = $('input[name="allowedmaxrate"]').val();
    //var minrate = $('input[name="allowedminrate"]').val();
    if(avgRate){        
        var lamount = avgRate*famount;
        lamount = lamount.toFixed(2);
        lamount = lamount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $('input[name="in_lamount"]').val(lamount);  
    }
});

$('input[name="in_rate"]').blur(function(e) {
    this.value = this.value.replace(/[^0-9\.]/g,'');
    var avgRate = $('input[name="in_rate"]').val();
    var famount = $('input[name="in_famount"]').val().split(",").join("");
    var maxrate = $('input[name="allowedmaxrate"]').val();
    var minrate = $('input[name="allowedminrate"]').val();
    var variance = $('input[name="variance"]').val();

    if(avgRate){
        if(variance!='' && variance > 0) {
            if(avgRate > maxrate) {
                $.alert("Rate entered is greater than Currency Varience...");
                $('input[name="in_lamount"]').val('0.00');
            } else if(avgRate < minrate) {
                $.alert("Rate entered is less than Currency Varience...");
                $('input[name="in_lamount"]').val('0.00');
            } else {
                var lamount = avgRate*famount;
                lamount = lamount.toFixed(2);
                lamount = lamount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                $('input[name="in_lamount"]').val(lamount);  
            }
        } else {
            var lamount = avgRate*famount;
            lamount = lamount.toFixed(2);
            lamount = lamount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('input[name="in_lamount"]').val(lamount); 
        }
             
    }

});



$('#checkall').on('click', function() {
    if (this.checked == true)
        $('#table_getdeals').find('input[id="chosendeal"]').prop('checked', true);
    else
        $('#table_getdeals').find('input[id="chosendeal"]').prop('checked', false);
});

//Deal Submit
$('#submitdeal').on('click', function() {
    $('#get_deal_details').modal('hide');
    if($('#table_getdeals input:checked').length > 0) {
        var chosedeal = new Array();
        $("#table_getdeals input:checkbox[id=chosendeal]:checked").each(function(){
            if($(this).prop('disabled')==false){
              chosedeal.push($(this).val());
            }
        });
        var chosendealstr = chosedeal.join(",");
        $('#chosendealstr').val(chosendealstr);
        var cusid = $("input[name='customer_code']").val();
        var dealbalance;
        $.ajax({
            url: "./deal/getcustomerdetails",
            type: "POST",
            async: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : {
                cusid : cusid,
            },
            success: function(dresponse){
                if(dresponse.status == 200){
                    dealbalance = dresponse.data.customerbalance;
                }
            }
        });
        

        $('#cusbalance').val(dealbalance);
        
        if(dealbalance < 0) {
            $('#cusbalance').removeClass('balancetext');
            $('#cusbalance').addClass('negbalancetext');
        } else {
            $('#cusbalance').removeClass('negbalancetext');
            $('#cusbalance').addClass('balancetext');
        }

        $.ajax({
            url: "./deal/releasedeal",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : {
                chosendeals : chosedeal,
            },
            beforeSend: function() {},
            success: function(response){
                    console.log(response);

                if(response.status == 200){
                    var datacnt = response.data.length;
                    var text_html ='';
                    var sid =0;
                    for(var m=0;m<=datacnt;m++) {
                        /*var lstid = $('#buy_sell_transaction_table_body tr:last').attr('id'); 
                        if(lstid == 'undefined' || lstid == undefined){
                            
                             //sid++;
                        } else {
                          //  sid =1;// parseInt(lstid)+1;
                        }*/
                         sid++;
                        //console.log(response.data);
                        if(response.data[m] != undefined) {
                            var in_dealno     = response.data[m].TranNo;
                            var in_type     = response.data[m].TranType;
                            var in_currency = response.data[m].CurrencyCode;                        
                            var in_famount  = response.data[m].FAmount;
                            var in_rate     = response.data[m].Rate;
                            var in_lamount  = response.data[m].LAmount;  
                         
                                               
                        var currname, currstock;
                        $.ajax({
                            url: "./buysell/getincurrencydetails",
                            type: "POST",
                            async: false,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data : {
                                in_currency : in_currency,
                            },
                            success: function(cresponse){
                                if(cresponse.status == 200){
                                    currname = cresponse.data[0].CurrencyName;
                                    currstock = cresponse.data[0].DealStock;
                                }
                            }
                        });
                        var in_stock = currstock;
                        var in_description = currname;
                        text_html +='<tr style="cursor: pointer;" id="'+sid+'"><td><input type="hidden" value="'+sid+'">'+sid+'</td><td><input value='+in_type+' type="text" name="td_type[]" id="intype'+sid+'" class="form-control"></td><td><input value='+in_currency+' type="text" name="td_currency[]" class="form-control" id="incurrency'+sid+'"></td><td><input value='+in_description+' type="text" name="td_description[]" class="form-control"></td><td class="text-right"><input value='+in_famount+' type="text" name="td_famount[]" class="form-control" id="famount'+sid+'"></td><td class="text-right"><input value='+in_rate+' type="text" name="td_rate[]" class="form-control"></td><td class="text-right"><input value='+in_lamount+' type="text" name="td_lamount[]" class="form-control lamtclas" data-ref='+in_type+'><input  type="hidden" name="in_stock[]" class="form-control" value='+in_stock+' id="instock'+sid+'"><input  type="hidden" name="in_deal[]" class="form-control" value='+in_dealno+' id="indeal'+sid+'"></tr>';
                       
                        }
                    }
                    $('#buy_sell_transaction tbody').html(text_html);
                    $('#get_deal_details').modal('hide');
                    Clear_data();

                } else {
                    $.alert('failed');
                }
                
            }

        });

        calculateTotal();

    } else {
        $.alert("Please select deal to Realise");
    }
});
//Deal Submit

//Cancel Deal
$('#canceldeal').on('click', function() {
    $('#get_deal_details').modal('hide');
    //location.reload();
});
//Cancel Deal


function getCustomerDetails(keypress='') {
    if($("input[name='customer_code']").val() != 'CASH'){
        $('#pno_nric_id').hide();
        $('#cconact_id').show();
        $('input[name="contacts"]').prop('readonly', false);
        $('input[name="pno_nric"]').prop('readonly', true);
    } else {
        $('#pno_nric_id').show()
        $('#cconact_id').hide();;
        $('input[name="contacts"]').prop('readonly', true);
        $('input[name="pno_nric"]').prop('readonly', false);
    }
    var cusid = $("input[name='customer_code']").val();
    $.ajax({
        url: "./buysell/getcustomerdetails",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            cusid : cusid,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                //return false;
                
                $('input[name="customer_name"]').val(response.data.CustName);
                // var s_dob = response.data.DOB.split('-');
                // var d_o_b = s_dob[2]+'-'+s_dob[1]+'-'+s_dob[0];
                $('input[name="dob"]').val(response.data.DOB);
                $('input[name="contacts"]').val(response.data.Phone);
                $('input[name="remarks"]').val(response.data.Remarks);
                $('input[name="pno_nric"]').val(response.data.NRICNo);
                $('#nationality').val(response.data.Nationality);
                $("#identifier_type").val(response.data.TypeOfIdentification);
                $('#mc_type').val(response.data.MCType);
                $('input[name="phone_no"]').val(response.data.CompPhone1);
                $('#country').val(response.data.Country);
                $('input[name="address"]').val(response.data.CompAddress1);
                $('input[name="address1"]').val(response.data.CompAddress2);
                $('input[name="postal_code"]').val(response.data.PostalCode);
                $('input[name="unit_no"]').val(response.data.UnitNo);
                $('input[name="doe"]').val(response.data.DOE);
                $('#mode_payment').attr('readonly', true);
                // $('input[name="remarks"]').attr('readonly', true);
                $("#identifier_type").attr('readonly', true);
                $('#mc_type').attr('readonly', true);
                $('input[name="phone_no"]').attr('readonly', true);
                $('#country').attr('readonly', true);
                $('input[name="address"]').attr('readonly', true);
                $('input[name="unit_no"]').attr('readonly', true);
                $('input[name="cusbalance"]').val(response.data.customerbalance);
                if(response.data.customerbalance < 0) {
                    $('#cusbalance').removeClass('balancetext');
                    $('#cusbalance').addClass('negbalancetext');
                } else {
                    $('#cusbalance').removeClass('negbalancetext');
                    $('#cusbalance').addClass('balancetext');
                }
                if(response.data.dealentry == undefined){
                    $('#buy_sell_transaction_table_body').html('');
                    $("#in_total").val(0.00);
                    Clear_data();
                }
                if(response.data.dealentry != undefined){
                   $('#deallist').html(response.data.dealentry);
                   $('#get_deal_details').modal('toggle');
                   $('#in_type').focus();
                }else if(keypress != ''){
                    $.alert("No Deal Records Found for this Customer");
                } 
                
                if($('input[name="postal_code"]').val().length >= 4){
                    $('#postalcodeserach').show();
                } else {
                    $('#postalcodeserach').hide();
                }
                //$('#in_currency').focus();
                //$('#in_type').focus();
            } else {
                $('input[name="customer_name"]').val('');
                $('input[name="dob"]').val('');
                $('input[name="contacts"]').val('');
                $('input[name="remarks"]').val('');
                $('input[name="address"]').val('');
                $('input[name="postal_code"]').val('');
                $('#mc_type').val('');
                $('input[name="pno_nric"]').val('');
                $('#nationality').val('');
                $("#identifier_type").val('');
                $('input[name="phone_no"]').val('');
                $('#country').val('');
                $('input[name="unit_no"]').val('');
                $('input[name="doe"]').val('');
                $('input[id="cusbalance"]').val('');
                $('input[name="unit_no"]').val('');
                $.alert({
                    title: 'Customer Code Not Found!2',
                    content: 'Customer Code Not Found!2',
                });
            }
        },      
    });
}

function getincurrencydetails() {
    var in_currency = $('input[name="in_currency"]').val(); 
    $.ajax({
        url: "./buysell/getincurrencydetails",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            in_currency : in_currency,
        },
        success: function(response){
            if(response.status == 200){
                $('input[name="in_description"]').val(response.data[0].CurrencyName);
                $('input[name="in_rate"]').val(response.data[0].AvgCost);
                $('input[name="in_average_cost"]').val(response.data[0].AvgCost);
                $('input[name="in_stock"]').val(response.data[0].Stock);
                $('input[name="curravgcost"]').val(response.data[0].AvgCost);
                var varianceperc = 0;
                if(response.data[0].Varience!='' && response.data[0].Varience!=0) {
                    var varianceperc = response.data[0].Varience / 100;
                }
                $('input[name="variance"]').val(varianceperc);
                $('input[name="in_famount"]').focus();
                getMinMaxRate();
            } else {
                if(in_currency!="") {
                    $.alert("Currency Not Found!");
                    $('input[name="in_currency"]').val('');
                    $('#in_currency').focus();
                }
                $('input[name="in_description"]').val('');
                $('input[name="in_rate"]').val('');
                $('input[name="in_average_cost"]').val('');
                $('input[name="in_stock"]').val('');
            }
        },      
    });
}

function getMinMaxRate()
{

    var avgcost = $('input[name="curravgcost"]').val();
    var varperc = $('input[name="variance"]').val();
    var minrate,maxrate;
    maxrate  = parseFloat(avgcost) + parseFloat(varperc);
    //alert(parseFloat(avgcost)+'---'+parseFloat(varperc))
    minrate = parseFloat(avgcost) - parseFloat(varperc);
    $('#allowedmaxrate').val(maxrate);
    $('#allowedminrate').val(minrate);
    $('input[name="in_rate"]').attr('max',maxrate);
    $('input[name="in_rate"]').attr('min',minrate);

}

function getCashCustomerDetails(keypress = '') {
    var id = $("input[name='pno_nric']").val();
    if(id != ''){
        $.ajax({
            url: "./cashcustomer/getcashcustomerdetails",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : {
                id : id,
            },
            beforeSend: function() {},
            success: function(response){
                if(response.status == 200){
                    $('input[name="customer_name"]').val(response.data.Name);
                    $('input[name="dob"]').val(response.data.DOB);
                    $('input[name="contacts"]').val(response.data.PhoneNo);
                    $('input[name="remarks"]').val(response.data.PendingCustomerRemarks);
                    $('input[name="address"]').val(response.data.Address1);
                    $('input[name="address1"]').val(response.data.Address2);
                    $('input[name="postal_code"]').val(response.data.PostalCode);
                    $('#mc_type').val(response.data.MCType);
                    $('input[name="phone_no"]').val(response.data.PhoneNo);
                    $('#country').val(response.data.Address4);
                    $('#nationality').val(response.data.Nationality);
                    $("#identifier_type").val(response.data.IdentifierType);
                    $('input[name="doe"]').val(response.data.DOE);
                    $('input[name="unit_no"]').val(response.data.UnitNo);
                    $("#identifier_type").attr('readonly', true);
                    $('input[name="doe"]').val(response.data.passexpiry);

                    // $('input[name="remarks"]').attr('readonly', true);
                    $('#mode_payment').attr('readonly', true);
                    $('#mc_type').attr('readonly', true);
                    $('input[name="phone_no"]').attr('readonly', true);
                    $('#country').attr('readonly', true);
                    $('input[name="address"]').attr('readonly', true);
                    $('input[name="unit_no"]').attr('readonly', true);
                    if(response.data.dealentry != undefined){
                        $('#deallist').html(response.data.dealentry);
                        $('#get_deal_details').modal('toggle');
                        $('#in_type').focus();

                    }else if(keypress != ''){
                        $.alert("No Deal Records Found for this Customer");
                    }
                    if(response.data.dealentry == undefined){
                        $('#buy_sell_transaction_table_body').html('');
                        $("#in_total").val(0.00);
                        Clear_data();
                    }                    
                    if($('input[name="postal_code"]').val().length >= 4){
                        $('#postalcodeserach').show();
                    } else {
                        $('#postalcodeserach').hide();
                    }
                    //$('#in_currency').focus();
                   // $('#in_type').focus();
                } else {

                    $('input[name="customer_name"]').val('');
                    $('input[name="dob"]').val('');
                    $('input[name="contacts"]').val('');
                    $('input[name="remarks"]').val('');
                    $('input[name="address"]').val('');
                    $('input[name="postal_code"]').val('');
                    $('#mc_type').val('');
                    $('input[name="phone_no"]').val('');
                    $('#country').val('');
                    $('#nationality').val('Select Nationality');
                    $("#identifier_type").val('Select Identifier Type');
                    $('input[name="doe"]').val('');
                    $('input[name="unit_no"]').val('');
                    /*$.alert({
                        title: 'Cash Customer Code Not Found!1',
                        content: 'Cash Customer Code Not Found!1',
                    });*/
                }
            },      
        });
    }
}

function Add_data() {
    var in_type          = $('#in_type').children("option:selected").html();  
    var in_currency      = $('input[name="in_currency"]').val();  
    var in_description   = $('input[name="in_description"]').val();  
    var in_famount       = $('input[name="in_famount"]').val();  
    var in_rate          = $('input[name="in_rate"]').val();
    var in_lamount       = $('input[name="in_lamount"]').val();
    var in_stock         = $('input[name="in_stock"]').val();
    var id               = $('#buy_sell_tr_td_id').val();  
    var dealdetailid       = $('input[name="dealdetailid"]').val();
    var dealamount =0;
    if(dealdetailid != ''){
        var dealamount = function () {
                        var tmp = null;
                        $.ajax({
                            async: false,
                            type: "POST",
                             headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            global: false,
                            dataType: 'json',
                            url: "{{route('getDealDetail')}}",
                            data : {dealid : dealdetailid},
                            success: function (response) {
                                tmp = response.data.FAmount;
                            }
                        });
                        return tmp;
                    }();
    }
    if(parseFloat(in_famount) > parseFloat(dealamount) && dealdetailid !=''){
        $.alert("F.amount should not greater than deal amount");
        Clear_data();
        return false;
    }else if(in_rate==0) {
        $.alert("Rate must not be 0");
        Clear_data();
        return false;
    } else if(in_lamount==0) {
        $.alert("L.Amount must not be 0");
        Clear_data();
        return false;
    } /*else if(in_type=="Sell" && in_stock==0) {
        $.alert("Stock must not be 0");
        Clear_data();
        return false;
    }*/ else {
        if(in_type == 'Select Type' || in_currency == '' || in_description == '' || in_famount == '' || in_rate == '' || in_lamount == ''){
            error_data();
            return false;
        } else {
            //Total Buy Amount Calculation          
            if(in_type=='Buy') {
                if($('#buyamt').val() != '') {
                    var assbuyamt = $('#buyamt').val();
                } else {
                    var assbuyamt = 0;
                }
                var enteredlamount = $('input[name="in_lamount"]').val().split(",").join("");
                var assbuyamt = parseFloat(assbuyamt) + parseFloat(enteredlamount);
                $('input[name="buyamt"]').val(assbuyamt);
                //var totallamt = parseFloat(intotal) + parseFloat(enteredlamount);
                //$('input[name="in_total"]').val(totallamt);

                var lsum = 0;
                $("input[class *= 'lamtclas']").each(function(){
                    lsum += +$(this).val().split(",").join("");
                });
                //var totallamt = parseFloat(intotal) + parseFloat(enteredlamount);
                //$('input[name="in_total"]').val(totallamt);                  
            }
            
            //Total Buy Amount Calculation

            //Total Sell Amount Calculation
            if(in_type=='Sell') {
                if($('#sellamt').val() != '') {
                    var asssellamt = $('#sellamt').val();
                } else {
                    var asssellamt = 0;
                }
                var enteredlamount = $('input[name="in_lamount"]').val().split(",").join("");
                var asssellamt = parseFloat(asssellamt) + parseFloat(enteredlamount);
                $('input[name="sellamt"]').val(asssellamt);
                //var totallamt = parseFloat(intotal) + parseFloat(enteredlamount);
                //$('input[name="in_total"]').val(totallamt);
            }

            
            //Total Sell Amount Calculation


            if(id == ''){
                var lstid = $('#buy_sell_transaction_table_body tr:last').attr('id'); 
                if(lstid == 'undefined' || lstid == undefined){
                    var sid = 1;
                } else {
                    sid = parseInt(lstid)+1;
                }
                $('#buy_sell_transaction tbody').append('<tr style="cursor: pointer;" id="'+sid+'"><td><input type="hidden" value="'+sid+'">'+sid+'</td><td><input value='+in_type+' type="text" name="td_type[]" id="intype'+sid+'" class="form-control"></td><td><input value='+in_currency+' type="text" name="td_currency[]" class="form-control" id="incurrency'+sid+'"></td><td><input value='+in_description+' type="text" name="td_description[]" class="form-control"></td><td class="text-right"><input value='+in_famount+' type="text" name="td_famount[]" class="form-control" id="famount'+sid+'"></td><td class="text-right"><input value='+in_rate+' type="text" name="td_rate[]" class="form-control"></td><td class="text-right"><input value='+in_lamount+' type="text" name="td_lamount[]" class="form-control lamtclas" data-ref='+in_type+'><input  type="hidden" name="in_stock[]" class="form-control" value='+in_stock+' id="instock'+sid+'"><input type="hidden" name="in_deal[]" value=""></tr>');
                Clear_data();
            } else {
                var datas = '<tr style="cursor: pointer;" id="'+id+'"><td><input type="hidden" value="'+id+'">'+id+'</td><td><input value='+in_type+' type="text" name="td_type[]" id="intype'+sid+'" class="form-control"></td><td><input value='+in_currency+' type="text" name="td_currency[]" class="form-control" id="incurrency'+sid+'"></td><td><input value='+in_description+' type="text" name="td_description[]" class="form-control"></td><td class="text-right"><input value='+in_famount+' type="text" name="td_famount[]" class="form-control" id="famount'+sid+'"></td><td class="text-right"><input value='+in_rate+' type="text" name="td_rate[]" class="form-control"></td><td class="text-right"><input value='+in_lamount+' type="text" name="td_lamount[]" class="form-control lamtclas" data-ref='+in_type+'><input  type="hidden"  name="in_stock[]" class="form-control" value='+in_stock+' id="instock'+sid+'"><input type="hidden" name="in_deal[]"  value="'+dealdetailid+'"></tr>';
                $('#buy_sell_transaction tr#'+id).remove();
                var nextid = $('input[name="nextid"]').val();
                if(nextid == 0 || nextid == ''){
                    var nextid = id;
                    console.log(nextid);
                    $('#buy_sell_transaction tbody').append(datas);
                } else {
                    console.log(nextid);
                    $('#buy_sell_transaction tbody tr#'+nextid).before(datas);
                }
                Clear_data();
            }

            calculateTotal();




            /*var sellarrayOfVar = []
            var buyarrayOfVar = []
            var arrayOfVar = []
            
            $('.lamtclas').each(function(){
            var ttype = $(this).attr("data-ref");
            arrayOfVar.push($(this).val() +'--'+ttype);
            });

            jQuery.each(arrayOfVar, function(index, item) {
                if (item.indexOf("Buy") >= 0) {
                    var buyamtdetails = item.split('--');
                    buyarrayOfVar.push(buyamtdetails[0]);
                }
                if (item.indexOf("Sell") >= 0) {
                    var sellamtdetails = item.split('--');
                    sellarrayOfVar.push(sellamtdetails[0]);
                }
            });
            //console.log(buyarrayOfVar);
            //console.log(sellarrayOfVar);

            var totbuyamts = eval(buyarrayOfVar.join("+"));
            $('input[name="buyamt"]').val(totbuyamts);
            var totsellamts = eval(sellarrayOfVar.join("+"));
            $('input[name="sellamt"]').val(totsellamts);
            //alert(totbuyamts);

            if(totbuyamts==undefined){
                totbuyamts = '0.00';
            }
            if(totsellamts==undefined){
                totsellamts = '0.00';
            }

            var totbuyamt = '-'+totbuyamts;
            var totsellamt = totsellamts;
            //alert(totbuyamt+'---'+totsellamt);
            var intotalval  = parseFloat(totbuyamt) + parseFloat(totsellamt);
            intotalval = intotalval.toFixed(2);
            //console.log(totbuyamt);
            $('input[name="in_total"]').val(intotalval);*/


        }
    }
}

function calculateTotal()
{
    var sellarrayOfVar = []
    var buyarrayOfVar = []
    var arrayOfVar = []
    //alert('sadsadsa');
    
    $('.lamtclas').each(function(){
        var ttype = $(this).attr("data-ref");
        arrayOfVar.push($(this).val().split(",").join("") +'--'+ttype);
    });

    jQuery.each(arrayOfVar, function(index, item) {
        if (item.indexOf("Buy") >= 0) {
            var buyamtdetails = item.split('--');
            buyarrayOfVar.push(buyamtdetails[0]);
        }
        if (item.indexOf("Sell") >= 0) {
            var sellamtdetails = item.split('--');
            sellarrayOfVar.push(sellamtdetails[0]);
        }
    });
    //console.log(buyarrayOfVar);
    //console.log(sellarrayOfVar);

    var totbuyamts = eval(buyarrayOfVar.join("+"));
    $('input[name="buyamt"]').val(totbuyamts);
    var totsellamts = eval(sellarrayOfVar.join("+"));
    $('input[name="sellamt"]').val(totsellamts);
    //alert(totbuyamts);

    if(totbuyamts==undefined){
        totbuyamts = '0.00';
    }
    if(totsellamts==undefined){
        totsellamts = '0.00';
    }

    var totbuyamt = '-'+totbuyamts;
    var totsellamt = totsellamts;
    //alert(totbuyamt+'---'+totsellamt);
    var intotalval  = parseFloat(totbuyamt) + parseFloat(totsellamt);
    intotalval = intotalval.toFixed(2);
    //console.log(totbuyamt);
    intotalval = intotalval.replace('-','');
    $('input[name="in_total"]').val(intotalval);
}
hotkeys('f9', function(event, handler){
    if($("#pno_nric").val() == ''){
          $.alert("Please Choose the Customer to Release Deal");  
    }else{
        if($("input[name='customer_code']").val() != 'CASH'){
            getCustomerDetails('keypress');
        }else{
            getCashCustomerDetails('keypress');
        }
        //$("input[name='customer_code']").trigger('blur');
    }
});
//$(document).bind('keyup', function(event){ 
hotkeys('f5', function(event, handler){
    //alert(event.keyCode);
    //if(event.altKey) {    // F5
        //alert(event.keyCode);
      //if (event.keyCode ==117) {
         //totalamt();
         event.preventDefault()
        var ttype = '';
        var arrayOfVar = [];
        $('.lamtclas').each(function(){
            ttype = $(this).attr("data-ref");
            arrayOfVar.push($(this).val().split(",").join("") +'--'+ttype);
        });
        // console.log(''+arrayOfVar);       
        var fullarr = ''+arrayOfVar;
        var containsell = fullarr.indexOf("Sell");
        // alert(containsell); 

        var in_currency = 'SGD'; 
        $.ajax({
            url: "./buysell/getSGDdetails",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : {
                in_currency : 'SGD',
            },
            success: function(response){
                //console.log(response); return false;
                if(response.status == 200){
                    $('input[name="in_description"]').val(response.data[0].CurrencyName);
                    $('input[name="in_rate"]').val(response.data[0].AvgCost);
                    //$('input[name="in_average_cost"]').val(response.data[0].AvgCost);
                    $('input[name="in_stock"]').val(response.data[0].Stock);
                    //$('input[name="curravgcost"]').val(response.data[0].AvgCost);
                    /*var varianceperc = 0;
                    if(response.data[0].Varience!='' && response.data[0].Varience!=0) {
                        var varianceperc = response.data[0].Varience / 100;
                    }
                    $('input[name="variance"]').val(varianceperc);
                    $('input[name="in_famount"]').focus();
                    getMinMaxRate();*/

                    var lstid = $('#buy_sell_transaction_table_body tr:last').attr('id'); 
                    if(lstid == 'undefined' || lstid == undefined){
                        var sid = 1;
                    } else {
                        sid = parseInt(lstid)+1;
                    }
                    if(ttype == 'Buy') {
                        var in_type = 'Sell';
                    } else{
                        var in_type = 'Buy';
                    }

                    var in_description = $('input[name="in_description"]').val();
                    var in_famount = $('input[name="in_total"]').val().split(",").join("");
                    var in_rate = 1;
                    var in_lamount = in_famount * in_rate;
                    in_lamount = in_lamount.toFixed(2);
                    var in_stock         = $('input[name="in_stock"]').val();
                    $('#buy_sell_transaction tbody').append('<tr style="cursor: pointer;" id="'+sid+'"><td><input type="hidden" value="'+sid+'">'+sid+'</td><td><input value='+in_type+' type="text" name="td_type[]" id="intype'+sid+'" class="form-control"></td><td><input value='+in_currency+' type="text" name="td_currency[]" class="form-control" onkeyup="this.value = this.value.toUpperCase();" id="incurrency'+sid+'"></td><td><input value='+in_description+' type="text" name="td_description[]" class="form-control"></td><td class="text-right"><input value='+in_famount+' type="text" name="td_famount[]" class="form-control" id="famount'+sid+'"></td><td class="text-right"><input value='+in_rate+' type="text" name="td_rate[]" class="form-control"></td><td class="text-right"><input value='+in_lamount+' type="text" name="td_lamount[]" class="form-control lamtclas" data-ref='+in_type+'><input  type="hidden" name="in_stock[]" id="instock'+sid+'" class="form-control" value='+in_stock+'></tr>');     
                    calculateTotal();

                } else {
                    if(in_currency!="") {
                        $.alert("Currency Not Found!");
                        $('input[name="in_currency"]').val('');
                        $('#in_currency').focus();
                    }
                    $('input[name="in_description"]').val('');
                    $('input[name="in_rate"]').val('');
                    $('input[name="in_average_cost"]').val('');
                    $('input[name="in_stock"]').val('');
                }
            },      
        });
        //return false;

        //if(containsell < 0) 
        //{
            
            /*var in_currency = 'SGD';
            var in_description = 'SGD';
            var in_famount = $('input[name="in_total"]').val();
            var in_rate = '1.000000';
            var in_lamount = in_famount * in_rate;
            in_lamount = in_lamount.toFixed(2);
            var in_stock         = $('input[name="in_stock"]').val();
             $('#buy_sell_transaction tbody').append('<tr style="cursor: pointer;" id="'+sid+'"><td><input type="hidden" value="'+sid+'">'+sid+'</td><td><input value='+in_type+' type="text" name="td_type[]" class="form-control"></td><td><input value='+in_currency+' type="text" name="td_currency[]" class="form-control"></td><td><input value='+in_description+' type="text" name="td_description[]" class="form-control"></td><td class="text-right"><input value='+in_famount+' type="text" name="td_famount[]" class="form-control"></td><td class="text-right"><input value='+in_rate+' type="text" name="td_rate[]" class="form-control"></td><td class="text-right"><input value='+in_lamount+' type="text" name="td_lamount[]" class="form-control lamtclas" data-ref='+in_type+'><input  type="hidden" name="in_stock[]" class="form-control" value='+in_stock+'></tr>');     
             calculateTotal();*/
         //}
      //}
    //}
});


function Delete_data() {
    var id = $('#buy_sell_tr_td_id').val();  
    if(id == ''){
        return false;
    }
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete the Currency?',
        buttons: {
            Ok: function () {
                $('#buy_sell_transaction tr#'+id).remove();
                Clear_data();
                calculateTotal();
            },
            cancel: function () {

            }
        },
    });
}

function Clear_data() {
    $('#buy_sell_tr_td_id').val('');  
    $('input[name="in_currency"]').val('');  
    $('input[name="in_description"]').val('');  
    $('input[name="in_famount"]').val('');  
    $('input[name="in_rate"]').val('');
    $('input[name="in_lamount"]').val(''); 
    $('input[name="in_average_cost"]').val('');
    $('input[name="in_stock"]').val('');
    $('#in_type').css('border', '1px solid #ced4da');  
    $('input[name="in_currency"]').css('border', '1px solid #ced4da');  
    $('input[name="in_description"]').css('border', '1px solid #ced4da');  
    $('input[name="in_famount"]').css('border', '1px solid #ced4da');  
    $('input[name="in_rate"]').css('border', '1px solid #ced4da');
    $('input[name="in_lamount"]').css('border', '1px solid #ced4da');
    $('#in_type').attr('disabled',false);   
    $('input[name="in_currency"]').attr('readonly',false); 
    $('input[name="in_description"]').attr('readonly',false); 
    $('input[name="in_rate"]').attr('readonly',false);  
    $('input[name="in_lamount"]').attr('readonly',false);
}


function error_data() {
    if($('#in_type').children("option:selected").html() == 'Select Type'){
        $('#in_type').css('border', '1px solid #ffa5b1');  
    }
    if($('input[name="in_currency"]').val() == ''){
        $('input[name="in_currency"]').css('border', '1px solid #ffa5b1');  
    } 
    if($('input[name="in_description"]').val() == ''){
        $('input[name="in_description"]').css('border', '1px solid #ffa5b1');  
    } 
    if($('input[name="in_famount"]').val() == ''){
        $('input[name="in_famount"]').css('border', '1px solid #ffa5b1');  
    } 
    if($('input[name="in_rate"]').val() == ''){
        $('input[name="in_rate"]').css('border', '1px solid #ffa5b1');
    }
    if( $('input[name="in_lamount"]').val() == ''){
        $('input[name="in_lamount"]').css('border', '1px solid #ffa5b1'); 
    }
}

function submitForm(submit_type){
    if(submit_type == 'save'){
        var buyamt = $('#buyamt').val();
        var sellamt = $('#sellamt').val();      
        var tablelength = $('#buy_sell_transaction_table_body tr').length;
        var customer_code = $('input[name="customer_code"]').val();
        if(sellamt > 5000){
            var voucher_no = $('input[name="voucher_no"]').val();
            var location = $('input[name="location"]').val();
            var orderdate = $('#order-datepicker').val();
            var customer_name = $('input[name="customer_name"]').val();
            var pno_nric = $('input[name="pno_nric"]').val();
            var contacts = $('input[name="contacts"]').val();
            var remarks = $('input[name="remarks"]').val();
            var address = $('input[name="address"]').val();
            var country = $('#country').val();
            var postal_code = $('input[name="postal_code"]').val();
            var phone_no = $('input[name="phone_no"]').val();
            var mc_type = $('#mc_type').val();


            
            if(voucher_no == '' || location == '' || orderdate == '' || customer_name == '' || customer_code == '' || pno_nric == '' || contacts == '' || address == '' || country == '' || postal_code == '' || phone_no == ''){
                $('#form_buysell').addClass('was-validated');
                return false;
            }
        }
        //alert('333');
        //alert('dfgdf'); return false;
        if(tablelength == 0){
            $.alert("Buy Sell Transaction Not Found!");
            return false;
        }

        //Check for Cash Customer if Buy and Sell amount match
        if(customer_code=='CASH') {
            /*if(buyamt > 5000 || sellamt > 5000){
                $.alert("CASH customer Transaction Limit Exceed");
                return false;
            }*/
            
            if(buyamt!=sellamt) {
                $.alert("Buy Sell Amount Not Matching!");
                return false;
            }

           
        }
        //Check for Cash Customer if Buy and Sell amount match

        var dealstr = $('#chosendealstr').val();
        if(dealstr != '') {
            var famtdeal = $('input[name="td_famount"]').val();
            //console.log(famtdeal);
            var cnt =0;
            $('input[name^="td_famount"]').each(function(i) {
                var indexval = i+1;
                //alert('instock'+indexval);
                var intype = $('#intype'+indexval).val();
                var instock = $('#instock'+indexval).val();
                var famount = $('#famount'+indexval).val();
                var incurr  = $('#incurrency'+indexval).val();
                if(intype!='Buy') {
                    if(parseFloat(famount) > parseFloat(instock)) {
                        $.alert("No Stock Available for "+incurr);
                        cnt++;
                    }
                }
                
            });    
            if(cnt > 0) {
                //$.alert("No Stock Available!");
                return false;
            } else {
                var buyurl = "./deal/dealinsertupdate";
            }
        } else {
            var buyurl = "./buysell/insert_update";
        }

        $('#form_buysell').removeClass('was-validated');
        $.ajax({
            url: buyurl,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : $('#form_buysell').serialize(),
            success: function(response){
                if(response.status == 200){
                    $('#form_buysell')[0].reset();
                    $('#buy_sell_transaction_table_body tr').remove();
                    voucherno();
                    success_toastr(response.content);
                } else {
                    danger_toastr(response.content);
                }
            },      
        });
    }
}

function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
function voucherno() {
    $.ajax({
        url: "./buysell/voucherno",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response){
            if(response.status == 200){
                $('input[name="voucher_no"]').val(response.data);
            }
        },      
    });
}
var dateToday = new Date();
$( function() {
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 15);
    $("#order-datepicker").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true
    });
    $("#doe-datepicker").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        minDate: dateToday,
    });
    $("#dob-datepicker").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: start.getFullYear() + ':' + end.getFullYear(),
        maxDate: dateToday,
    });
});
</script>

@endsection