@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
@endsection

@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<style type="text/css">
    .picker__holder{
        max-width: none;
        width: 335px;
    }
</style>
@endsection
@section('main-content')
    <div class="breadcrumb">
        <h1>Balances</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    <div class="row mb-12 master-main">
        <div class="col-md-12 mb-12">
            <div class="card text-left">
            <form class="needs-validation" novalidate name="currency" id="frmcurrency" method="POST" action="{{ route('currency_balance') }}" autocomplete="off">
            <input type="hidden" name="optype" id="optype" value="">
            {{ csrf_field() }}
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="customer-basic-tab" data-toggle="tab" href="#customerBasic" role="tab" aria-controls="customerBasic" aria-selected="true">Customer</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="currency-basic-tab" data-toggle="tab" href="#currencyBasic" role="tab" aria-controls="currencyBasic" aria-selected="false">Currency</a>
                    </li>                                
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="customerBasic" role="tabpanel" aria-labelledby="customer-basic-tab">
                        <div class="row">
                         <div class="col-lg-12 col-sm-12 master-main-sec">
                            <div class="card mb-6">
                                <div class="card-body">  
                                    <div class="row">
                                        <label for="clocation" class="col-lg-2 col-sm-4  col-form-label">Location</label>
                                        <select class="form-control col-lg-3 col-sm-4" name="clocation" id="clocation">
                                            @if(count($alllocations) > 0)
                                                @foreach($alllocations as $location)
                                                    <option value="{{ $location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){ echo "selected"; }?> >{{ $location->LocationCode}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>                                                 
                                    <hr />
                                    <div class="table-responsive">
                                        <input type="hidden" name="enabledeal" id="enabledeal" value="{{env('ENABLE_DEAL')}}">
                                        <table id="customerbalance_table" class="display nowrap table table-striped table-bordered locationcurrencylist_new_table" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>S.No.</th>
                                                    <th>Customer Code</th>
                                                    <th>Name</th>
                                                    <th>Balance</th>
                                                    @if(env('ENABLE_DEAL') == 1)
                                                    <th>Deal Balance</th>
                                                    @endif
                                                    <th>Total Balance</th>
                                                </tr>
                                            </thead>
                                            <tbody id="customerbalancelist">
                                            </tbody>
                                        </table>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>
                    <div class="tab-pane fade" id="currencyBasic" role="tabpanel" aria-labelledby="currency-basic-tab">
                        <div class="row">
                             <div class="col-lg-12 col-sm-12 master-main-sec">
                                <div class="card mb-6">
                                    <div class="card-body">    
                                        <div class="row">
                                            <!-- <div class="col-lg-12"> -->
                                                <label for="location" class="col-lg-2 col-sm-4  col-form-label">Location</label>
                                                <select class="form-control col-lg-3 col-sm-4" name="location" id="location">
                                                    @if(count($alllocations) > 0)
                                                        @foreach($alllocations as $location)
                                                            <option value="{{ $location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){ echo "selected"; }?> >{{ $location->LocationCode}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            <!-- </div> -->
                                                <label for="location" class="col-lg-2 col-sm-4  col-form-label">Currency</label>
                                                <select class="form-control col-lg-3 col-sm-4" name="currency_code" id="currency_code">
                                                    <option value="">Select Currency</option>
                                                    @if(count($currency) > 0)
                                                        @foreach($currency as $curCode)
                                                            <option value="{{ $curCode->CurrencyCode}}"  >{{ $curCode->currencies['CurrencyName']}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                        </div>                                             
                                        <hr />
                                        <div class="table-responsive">
                                            <table id="currencybalance_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th>Currency</th>
                                                        <!-- <th>Currency Name</th> -->
                                                        <th>Stock</th>
                                                        <th>Average Cost</th>
                                                        <th>LValue</th>
                                                        @if(env('ENABLE_DEAL') == 1)
                                                        <th>Deal Stock</th>
                                                        @endif
                                                        <th>LValue(DS)</th>
                                                        <!-- <th>LocationCode</th> -->
                                                    </tr>
                                                </thead>
                                                
                                            </table>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                                
                </div>
                <!-- End of Tab -->
            </div>
            </form>
            </div>
            </div>
</div>
            <!-- end of row -->
           
            <!-- end of row -->
@include('modal.currency')            
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
<script src="{{asset('assets/js/ladda.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<!-- <script src="{{asset('assets/js/datatables.script.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="{{asset('assets/js/modal.details.js')}}"></script>
@endsection



@section('bottom-js')
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
<script type="text/javascript">

    $(document).ready(function(){
        var enabledeal = $('#enabledeal').val();
        if(enabledeal == 1) {
            var columnse = [
                {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable: false, searchable: false},
                {data: 'CurrencyCode', name: 'CurrencyCode'},
                // {data: 'CurrencyName', name: 'CurrencyName'},
                {data: 'Stock', name: 'Stock',className: "text-right"},
                {data: 'AvgCost', name: 'AvgCost',className: "text-right"},
                {data: 'LValue', name: 'LValue',className: "text-right"},
                {data: 'DealStock', name: 'DealStock',className: "text-right"},
                {data: 'LValueDS', name: 'LValueDS',className: "text-right"},
                // {data: 'LocationCode', name: 'LocationCode'},
            ];
        } else {
            var columnse = [
                {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable: false, searchable: false},
                {data: 'CurrencyCode', name: 'CurrencyCode'},
                // {data: 'CurrencyName', name: 'CurrencyName'},
                {data: 'Stock', name: 'Stock',className: "text-right"},
                {data: 'AvgCost', name: 'AvgCost',className: "text-right"},
                {data: 'LValue', name: 'LValue',className: "text-right"},
                // {data: 'DealStock', name: 'DealStock',className: "text-right"},
                {data: 'LValueDS', name: 'LValueDS',className: "text-right"},
                // {data: 'LocationCode', name: 'LocationCode'},
            ];
        }
        var table = $('#currencybalance_table').DataTable({
            paging:   true,
            pageLength: 25,
            destroy: true,
            searching : false,
            autoWidth : false,
            info:     false,
            serverSide: true,
            ajax: {
                url: "./balances",
                type: "GET",
                data: function (d) {
                  d.locationcode = $('#location').val();
                  d.currency_code = $('#currency_code').val();
                }              
            },
            order: [],
            columns: columnse,
        });

        $('#location').on('change', function(){
            table.search(this.value).draw();   
        });
        $('#currency_code').on('change', function(){
            table.search(this.value).draw();   
        });
        $("#currency-basic-tab").on('click',function(){
            $('#currency_code').val("");
            $('#location').val("<?php echo Auth::user()->Location; ?>");
            $('#currency_code').trigger('change');
        });
        var enabledeal = $('#enabledeal').val();
        if(enabledeal == 1) {
            var columnse = [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable: false, searchable: false},
                    {data: 'Custcode', name: 'Custcode'},
                    {data: 'CustName', name: 'CustName'},
                    {data: 'Balance', name: 'Balance',className: "text-right"},
                    {data: 'DealBalance', name: 'DealBalance',className: "text-right"},
                    {data: 'tot', name: 'tot',className: "text-right"},
            ];
        } else {
            var columnse = [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable: false, searchable: false},
                    {data: 'Custcode', name: 'Custcode'},
                    {data: 'CustName', name: 'CustName'},
                    {data: 'Balance', name: 'Balance',className: "text-right"},
                    {data: 'tot', name: 'tot',className: "text-right"},
            ];
        }
        var customertable = $('#customerbalance_table').DataTable({
            paging:   true,
            pageLength: 25,
            destroy: true,
            serverSide: true,
            autoWidth : false,
            info:     false,
            searching : false,
            ajax: {
                url: "./customerbalances",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function (d) {
                  d.locationcode = $('#clocation').val();
                }              
            },
            order: [],
            columns: columnse,
        });
        
        $('#clocation').on('change', function(){
            customertable.search(this.value).draw();   
        });
        $('#customer-basic-tab').on('click', function(){
            $('#clocation').val('<?php echo Auth::user()->Location; ?>');
            $('#clocation').trigger('change');  
        });
    });
    /*$(document).on("click", "input[name='currency_id_detail']",function() {
        $('#get_currency_modal_details').modal('toggle');
        var val = $(this).val();
        var valarray = val.split('|');
        $('input[name="currency_code"]').val(valarray[0]);
        $('#currency_code').trigger('change');
    });*/
</script>
@endsection