<!-- Modal -->
<div class="modal fade" id="get_deal_list" tabindex="-1" role="dialog" aria-labelledby="Modal_Title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Modal_Title">Edit Deal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class="col-lg-12 mb-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="#" method="POST" id="deallisteditform">
                                @csrf
                                <input type="hidden" name="edit_id" id="edit_id" value=""   >
                                <div class="row">
                                    
                                    <div class="col-lg-6 col-sm-6 mb-1">
                                        <div class="form-group row">
                                            <label for="dealno" class="col-sm-6 col-form-label">
                                                Deal No
                                            </label>
                                            <div class="col-sm-6">
                                                <input id="dealno" class="form-control" placeholder="Deal No." name="dealno" readonly='readonly' >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="validtilldate" class="col-sm-6 col-form-label">
                                                Valid Till
                                            </label>
                                            <div class="col-sm-6">
                                                <input id="validtilldate" class="form-control" placeholder="Valid Till Date" name="validtilldate" readonly='readonly' >
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label for="customercode" class="col-sm-6 col-form-label">
                                                Customer Code
                                            </label>
                                            <div class="col-sm-6">
                                                <input id="customercode" class="form-control" placeholder="Customer Code" name="customercode" readonly='readonly' >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6 mb-1 trantype" id="edit_trantype">
                                    </div>
                                    <div class="col-lg-12 col-sm-12 mb-1">
                                        <div class="row form-group">
                                            <label for="customername" class="col-sm-3 col-form-label">
                                                Customer Name
                                            </label>
                                            <div class="col-sm-6">
                                                <input id="customername" class="form-control" placeholder="Customer Name" name="customername" readonly='readonly'>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label for="contact" class="col-sm-3 col-form-label">
                                                Contact
                                            </label>
                                            <div class="col-sm-6">
                                                <input id="contact" class="form-control" placeholder="Contact" name="contact" readonly='readonly'>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label for="currency" class="col-sm-3 col-form-label">
                                                Currency
                                            </label>
                                            <span class="col-sm-2">
                                                <input id="currencycode" class="form-control" placeholder="currency" name="currencycode" readonly='readonly'>
                                            </span>
                                             <span class="col-sm-4">
                                                <input id="currencyname" class="form-control" placeholder="currency" name="currencyname" readonly='readonly'>
                                            </span>
                                        </div>
                                        <div class="row form-group">
                                            <label for="rate" class="col-sm-3 col-form-label">
                                                Rate
                                            </label>
                                            <div class="col-sm-6">
                                                <input id="rate" class="form-control" placeholder="Rate" name="rate" readonly='readonly'>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label for="amount" class="col-sm-3 col-form-label">
                                                Amount
                                            </label>
                                            <div class="col-sm-6">
                                                <input id="amount" class="form-control" placeholder="Amount" name="amount" readonly='readonly'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 mb-1">
                                        <div class="row">
                                            <label for="edit" class="col-sm-3 col-form-label">
                                                Edit
                                            </label>
                                            <label for="edit_validtilldate" class="col-sm-2 col-form-label">
                                                Valid Till
                                            </label>
                                            <label for="edit_famount" class="col-sm-2 col-form-label">
                                                F.Amount
                                            </label>
                                            <label for="edit_rate" class="col-sm-2 col-form-label">
                                            Rate
                                            </label>
                                           
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                            </div>
                                            <div class="col-sm-2">
                                                <input id="edit_validtilldate" class="form-control" placeholder="Amount" name="edit_validtilldate">
                                            </div>
                                            <div class="col-sm-2">
                                                <input id="edit_famount" class="form-control" placeholder="Amount" name="edit_famount" >
                                            </div>
                                            <div class="col-sm-2">
                                                <input id="edit_rate" class="form-control" placeholder="Amount" name="edit_rate" readonly='readonly'>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                            </div>
                                            <label for="edit_remark" class="col-sm-2 col-form-label">
                                             Remarks
                                            </label>
                                            <div class="col-sm-4">
                                                <input id="edit_remark" class="form-control" placeholder="Remarks" name="edit_remark">
                                            </div>
                                        </div> 
                                    </div>  
                                   
                                </div>

                                <div class="row mt-2">
                                    <div class="col-lg-12 col-sm-12 text-right">
                                        <button class="btn btn-primary m-1" type="button" id="edit_deallistform">Submit</button>
                                        <input  class="btn btn-primary m-1" type="button" data-dismiss="modal"  value="Cancel">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->