<!-- Modal -->
<div class="modal fade" id="get_deal_details" tabindex="-1" role="dialog" aria-labelledby="Deal_Transactions" aria-hidden="true">
    <div class="modal-dialog modal-deal-table" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Modal_Title">Deal Transaction</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="table_getdeals" class="table table-hover mb-3 table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <td class="text-center font-weight-bold" scope="col"></td>
                                <td class="text-center font-weight-bold" scope="col">No</td>
                                <td class="text-center font-weight-bold" scope="col">Date</td>
                                <td class="text-center font-weight-bold" scope="col">Cur. Code</td>
                                <td class="text-center font-weight-bold" scope="col">Type</td>
                                <td class="text-center font-weight-bold" scope="col">Amount</td>
                                <td class="text-center font-weight-bold" scope="col">Realised Amount</td>
                                <td class="text-center font-weight-bold" scope="col">Balance Amount</td>
                                <td class="text-center font-weight-bold" scope="col">Rate</td>
                                <td class="text-center font-weight-bold" scope="col">Valid Till</td>
                                <td class="text-center font-weight-bold" scope="col">Status</td>
                                <td class="text-center font-weight-bold" scope="col">Select</td>
                            </tr>
                        </thead>
                        <tbody id="deallist">

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="10"></td>
                                <td colspan="2" align="right">Select All <input type="checkbox" name="checkall" id="checkall" checked="checked"></td>
                            </tr>
                            <tr>
                                <td align="right" colspan="12">
                                    <input type="button" class="btn m-1 btn-primary" name="submitdeal" id="submitdeal" value="Ok" />
                                    <input type="button" class="btn m-1 btn-primary" name="canceldeal" id="canceldeal" value="Cancel" />
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->