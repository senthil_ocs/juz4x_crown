<div class="modal fade" id="get_cashcust_list" tabindex="-1" role="dialog" aria-labelledby="Modal_Title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Modal_Title">Edit Cash Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class="col-lg-12 mb-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="#" method="POST" id="cashcustlisteditform">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 master-main-sec">
                                        <div class="card mb-6">
                                            <div class="card-body">
                                                <input type="hidden" name="cashcustomer_id" id="cashcustomer_id" value=""   >
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="cust_code" class="col-sm-3 col-form-label">
                                                                Cust Code
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="cust_code" class="form-control" autocomplete="off" id="cust_code" value="CASH"  placeholder="Enter Customer code" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="nric_no" class="col-sm-3 col-form-label">
                                                                NRIC No<span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="nric_no" class="form-control" autocomplete="off" id="nric_no" placeholder="Enter NRIC No" required readonly>
                                                                    <div class="invalid-feedback"> 
                                                                        Please Enter NRIC No
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="cname" class="col-sm-3 col-form-label">
                                                                Name<span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="cname" class="form-control" autocomplete="off" id="cname" placeholder="Enter Name" required>
                                                                    <div class="invalid-feedback"> 
                                                                        Please Enter Name
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="date_of_birth" class="col-sm-3 col-form-label">
                                                                Date of Birth<span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="text" name="date_of_birth" class="form-control" autocomplete="off" id="date_of_birth" placeholder="dd-mm-yyyy" required>
                                                                    <div class="invalid-feedback"> 
                                                                        Please Enter Date of Birth
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="nationality" class="col-sm-3 col-form-label">Nationality<span class="mandatory">*</span></label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control" name="nationality" id="nationality" required>
                                                                    <option>Select Nationality</option>
                                                                    @foreach($nationality as $value)
                                                                        <option value="{{trim($value->Code,' ')}}">{{trim($value->Description,' ')}}</option>
                                                                    @endforeach
                                                                </select>                                                       
                                                                <div class="invalid-feedback">Select Nationality</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="active" class="col-sm-3 col-form-label">Active</label>
                                                            <div class="col-sm-9 mt-2">
                                                                <label class="checkbox checkbox-primary">
                                                                    <input type="checkbox" id="active" name="active" checked="checked" value="1">
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                             </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="identifier_type" class="col-sm-3 col-form-label">Identifier Type<span class="mandatory">*</span></label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control" name="identifier_type" id="identifier_type" required>
                                                                    <option>Select Identifier Type</option>
                                                                    @foreach($identifire as $value)
                                                                        <option value="{{$value}}">{{$value}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <div class="invalid-feedback"> 
                                                                    Please select Identifire Type
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="identifier_other" class="col-sm-3 col-form-label">
                                                                
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="text" name="identifier_other" id="identifier_other" class="form-control"  value="{{isset($cashCustomer->IdentifierTypeOther)?$cashCustomer->IdentifierTypeOther:''}}">
                                                                <div class="invalid-feedback"> 
                                                                    Please Enter Identifire Type
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="date_of_expiry" class="col-sm-3 col-form-label">
                                                                Date of Expiry<span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="text" name="date_of_expiry" class="form-control" autocomplete="off" id="date_of_expiry" placeholder="dd-mm-yyyy" required>
                                                                <div class="invalid-feedback"> 
                                                                    Please Enter Date of Expiry
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="pending_documents" class="col-sm-3 col-form-label">Pending Documents</label>
                                                            <div class="col-sm-9 mt-2">
                                                                <label class="checkbox checkbox-primary">
                                                                    <input type="checkbox" id="pending_documents" name="pending_documents" value="0">
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                             </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group row" id="imageRow">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="date_of_expiry" class="col-sm-3 col-form-label">Image</label>
                                                            <div class="col-sm-3">
                                                                <input type="image" id="nricfront" alt="nricfront" width="50" height="50" style="border-radius: 10px;">
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="image" id="nricback" alt="nricback" width="50" height="50" style="border-radius: 10px;">
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="image" id="photo" alt="photo" width="50" height="50" style="border-radius: 10px;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 master-main-sec">
                                        <div class="card mb-6">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="country" class="col-sm-3 col-form-label">
                                                                Country <span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control" name="country" id="country" required>
                                                                    <option value="">Select Country</option>
                                                                    @foreach($country as $value)
                                                                        <option value="{{trim($value->Country, ' ')}}">{{trim($value->Country, ' ')}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <div class="invalid-feedback">Please Select Country</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="postal_code" class="col-sm-3 col-form-label">
                                                                Postal Code <span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9 input-group mb-3">
                                                                <input type="textbox" name="postal_code" class="form-control allownumeric" autocomplete="off" id="postal_code" placeholder="Enter Postal Code" required>
                                                                    <div class="invalid-feedback"> 
                                                                        Please Enter Postal Code
                                                                    </div>
                                                                <div class="input-group-append" id="postalcodeserach" style="cursor: pointer;">
                                                                    <span class="input-group-text" id="basic-addon1">
                                                                        <i class="i-Business-Mens"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="address" class="col-sm-3 col-form-label">
                                                                Address <span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="address" class="form-control" autocomplete="off" id="address" placeholder="Enter Address" required>
                                                                    <div class="invalid-feedback"> 
                                                                        Please Enter Address
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="address2" class="col-sm-3 col-form-label">
                                                                Address 2
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="address2" class="form-control" autocomplete="off" id="address2" placeholder="Enter Address2">
                                                                    <div class="invalid-feedback"> 
                                                                        Please Enter Address
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="unit" class="col-sm-3 col-form-label">
                                                                Unit
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="unit" class="form-control" autocomplete="off" id="unit" placeholder="Enter Unit">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="tele_no" class="col-sm-3 col-form-label">
                                                                Telephone No <span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="tele_no" class="form-control allownumeric" autocomplete="off" id="tele_no" placeholder="Enter Telephone No" required>
                                                                    <div class="invalid-feedback"> 
                                                                        Please Enter Telephone No
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="mc_type" class="col-sm-3 col-form-label">
                                                                MC Type
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control" name="mc_type" id="mc_type">
                                                                    <option value="">Select MC Type</option> 
                                                                    @foreach ($general_type as $key => $value)
                                                                        <option value="{{$value}}" @if(old("mc_type")==$value) selected="selected" @endif>{{$value}}</option> 
                                                                    @endforeach                                                              
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="pending_remarks" class="col-sm-3 col-form-label">
                                                                Pending Remarks
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <textarea rows="4" class="form-control pending_remarks"  id="pending_remarks"  name="pending_remarks" placeholder="Enter Pending Remarks"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row" id="dueIntelligence" >
                                                            <label for="intelligence" class="col-sm-3 col-form-label">
                                                                Due Intelligence <span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="radio" id="intelligenceYes" name="intelligence" value="Yes" required>
                                                                <label for="Yes">Yes</label>
                                                                <input type="radio" id="intelligenceNo" name="intelligence" value="No" style="margin-left: 20px" checked>
                                                                <label for="No">No</label>
                                                                <div class="invalid-feedback"> 
                                                                    Please Select Due Intelligence
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row" id="allowOnline">
                                                            <label for="allow" class="col-sm-3 col-form-label">
                                                                Allow Online <span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type="radio" id="allowYes" name="allow" value="Yes" required>
                                                                <label for="Yes">Yes</label>
                                                                <input type="radio" id="allowNo" name="allow" value="No" style="margin-left: 20px" checked>
                                                                <label for="No">No</label>
                                                                <div class="invalid-feedback"> 
                                                                    Please Select Allow Online
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row mt-2">
                                <div class="col-lg-12 col-sm-12 text-right">
                                    <button class="btn btn-primary m-1" type="button" id="edit_cashlistform">Submit</button>
                                    <input  class="btn btn-primary m-1" type="button" data-dismiss="modal"  value="Cancel">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal