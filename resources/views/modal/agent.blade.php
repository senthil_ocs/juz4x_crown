<!-- Modal -->
<div class="modal fade" id="get_agent_modal" tabindex="-1" role="dialog" aria-labelledby="Modal_Title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Modal_Title">Agent Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="table_getagent" class="table table-hover mb-3 table-bordered" style="width:100%">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->