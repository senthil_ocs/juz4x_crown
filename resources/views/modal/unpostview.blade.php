<div class="modal fade" id="unpost_view" tabindex="-1" role="dialog" aria-labelledby="Modal_Title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Modal_Title">Payment Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class="col-lg-12 mb-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="#" method="POST" id="unpostviewform">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 master-main-sec">
                                        <div class="card mb-6">
                                            <div class="card-body">
                                                <input type="hidden" name="cashcustomer_id" id="cashcustomer_id" value=""   >
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_transno" class="col-sm-3 col-form-label">Trans No</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_transno" class="form-control" autocomplete="off" id="v_transno" placeholder="Trans No" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_custname" class="col-sm-3 col-form-label">Cust Name</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_custname" class="form-control" autocomplete="off" id="v_custname"  placeholder="Cust Name" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_benename" class="col-sm-3 col-form-label">Beneficiary Name </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_benename" class="form-control" autocomplete="off" id="v_benename" placeholder="Beneficiary Name" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_curcode" class="col-sm-3 col-form-label">Currency Code </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_curcode" class="form-control" autocomplete="off" id="v_curcode" placeholder="Currency Code" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_exchrate" class="col-sm-3 col-form-label">Exch Rate </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_exchrate" class="form-control" autocomplete="off" id="v_exchrate" placeholder="Exch Rate" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_famount" class="col-sm-3 col-form-label">Foreign Amount </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_famount" class="form-control" autocomplete="off" id="v_famount" placeholder="Foreign Amount" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_sgd" class="col-sm-3 col-form-label">SGD Amount </label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_sgd" class="form-control" autocomplete="off" id="v_sgd" placeholder="SGD Amount" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_tot" class="col-sm-3 col-form-label">Total Amount</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_tot" class="form-control" autocomplete="off" id="v_tot" placeholder="Total Amount" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 master-main-sec">
                                        <div class="card mb-6">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_status" class="col-sm-3 col-form-label">Payment Status</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_status" class="form-control" autocomplete="off" id="v_status" placeholder="Payment Status" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_mode" class="col-sm-3 col-form-label">Payment Mode</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_mode" class="form-control" autocomplete="off" id="v_mode" placeholder="Payment Mode" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_paiddate" class="col-sm-3 col-form-label">Paid Date</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_paiddate" class="form-control" autocomplete="off" id="v_paiddate" placeholder="Paid Date" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_paidby" class="col-sm-3 col-form-label">Paid By</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_paidby" class="form-control" autocomplete="off" id="v_paidby" placeholder="Paid By" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_loccode" class="col-sm-3 col-form-label">Payment Location Code</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_loccode" class="form-control" autocomplete="off" id="v_loccode" placeholder="Payment Location Code" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>    
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="v_tercode" class="col-sm-3 col-form-label">Payment Terminal Code</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="v_tercode" class="form-control" autocomplete="off" id="v_tercode" placeholder="Payment Terminal Code" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row mt-2">
                                <div class="col-lg-12 col-sm-12 text-right">
                                    <input  class="btn btn-primary m-1" type="button" data-dismiss="modal"  value="Close">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal