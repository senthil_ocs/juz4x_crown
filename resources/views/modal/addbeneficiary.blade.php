<div class="modal fade" id="add_cust_bene" tabindex="-1" role="dialog" aria-labelledby="Modal_Title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Modal_Title">Add Customer Beneficiary</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class="col-lg-12 mb-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="#" method="POST" id="addbeneform">
                            @csrf
                                <input type="hidden" name="addnric_no" id="addnric_no">
                                <div class="card-title">Beneficiary</div>
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 master-main-sec">
                                        <div class="card mb-4">
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="beneficiary_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No.</th>
                                                                <th> BeneficiaryId </th>
                                                                <th> CustCode </th>
                                                                <th> CurrencyCode </th>
                                                                <th> CustNRICNO </th>
                                                                <th> BeneName </th>
                                                                <th> BeneBankName </th>
                                                                <th> BeneBankAccNo </th>
                                                                <th> BeneAddress1 </th>
                                                                <th> BeneAddress2 </th>
                                                                <th> BeneCountry </th>
                                                                <th> BeneMobileNo </th>
                                                                <th> BeneBankBranch </th>
                                                                <th> SwiftCode </th>
                                                                <th> Active </th>
                                                                <th> purpose </th>
                                                            </tr>
                                                        </thead>    
                                                        <tbody id="beneficiary_table_tbody">
                                                            <input type="hidden" name="delbeneficiaryclsid" id="delbeneficiaryclsid" value="">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 master-main-sec">
                                        <div class="card mb-4">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                            <input type="text" name="beneficiary_id" id="beneficiary_id" placeholder="Beneficiary Id" hidden readonly autocomplete="off" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="custcode" id="custcode" placeholder="CustCode" size="50" value="CASH" hidden class="form-control custCodedis" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <!-- <div class="col-sm-12 m-1">
                                                                <input type="text" name="CurrencyCode" id="CurrencyCode" placeholder="CurrencyCode" placeholder="Date" class="form-control" autocomplete="off">
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="CustNRICNO" id="CustNRICNO" placeholder="CustNRICNO" class="form-control custNRICdis" hidden autocomplete="off" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                            <input type="text" name="BeneName" id="BeneName" placeholder="BeneName" autocomplete="off" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="BeneBankName" id="BeneBankName" placeholder="BeneBankName" size="50" class="form-control" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="BeneBankAccNo" id="BeneBankAccNo" placeholder="BeneBankAccNo" class="form-control" autocomplete="off" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="BeneAddress1" id="BeneAddress1" placeholder="BeneAddress1" class="form-control" autocomplete="off" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                            <input type="text" name="BeneAddress2" id="BeneAddress2" placeholder="BeneAddress2" autocomplete="off" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="BeneCountry" id="BeneCountry" placeholder="BeneCountry" size="50" class="form-control" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="BeneMobileNo" id="BeneMobileNo" placeholder="BeneMobileNo" class="form-control" autocomplete="off" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="BeneBankBranch" id="BeneBankBranch" placeholder="BeneBankBranch" class="form-control" autocomplete="off" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                             <div class="col-sm-12 m-1">
                                                                    <input type="text" name="CurrencyCode" id="CurrencyCode" placeholder="CurrencyCode" class="form-control" autocomplete="off">
                                                             </div>    
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                             <div class="col-sm-12 m-1">
                                                                 <input type="text" name="SwiftCode" id="SwiftCode" placeholder="SwiftCode" class="form-control" autocomplete="off">
                                                             </div>    
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                             <div class="col-sm-12 m-1">
                                                                <label class="checkbox checkbox-primary">
                                                                <input type="checkbox" id="beneficiaryactive" name="beneficiaryactive" checked="checked">
                                                                <span style="padding-left: 20px;">Active</span>
                                                                <span class="checkmark"></span>
                                                                </label>
                                                             </div>  
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-5 m-1 text-left">
                                                                 <input type="button" class="btn btn-primary" name="addbeneficiary" id="addbeneficiary" value="ADD">
                                                            </div>    
                                                            <div class="col-sm-5 m-1 text-right">
                                                                 <input type="button" class="btn btn-primary" name="deletebeneficiary" id="deletebeneficiary" value="DELETE">
                                                            </div>    
                                                        </div>
                                                    </div> -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row mt-2">
                                <div class="col-lg-12 col-sm-12 text-right">
                                    <button class="btn btn-primary m-1" type="button" id="add_beneficiaryform">Submit</button>
                                    <input  class="btn btn-primary m-1" type="button" data-dismiss="modal"  value="Cancel">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>