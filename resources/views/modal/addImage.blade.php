<div class="modal fade" id="add_image" tabindex="-1" role="dialog" aria-labelledby="Modal_Title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Modal_Title">Add Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class="col-lg-12 mb-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('cashcustomer.addImage')}}" method="POST" id="addimageform" enctype="multipart/form-data">
                            @csrf
                                <input type="hidden" name="nric_no" id="nric_no">
                                <input type="hidden" name="filterName" id="addfilterName">
                                <input type="hidden" name="filterNric" id="addfilterNric">
                                <input type="hidden" name="filterPhno" id="addfilterPhno">
                                <input type="hidden" name="filterAllow" id="addfilterAllow">
                                <div class="card-title">Image</div>
                                
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 master-main-sec">
                                        <div class="card mb-4">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <label>Nric front :</label><br>
                                                                <input type="file" name="nricfront" id="nricfront" class="form-control nricfront">
                                                                <label style="font-size: small;" class="nricfront">Allowed format .pdf,.png,.jpg, Max upload size 2MB</label>
                                                                <div style="text-align: center;">
                                                                    <img src=""  onclick="openModal();currentSlide(1)" id="img_nricfront" class="preview_nricfront" width="100px">
                                                                    <a href="" target="_blank" id="pdf_nricfront" class="preview_nricfront"><button type="button" class="btn btn-primary">Preview PDF</button></a>
                                                                </div>
                                                                <div style="text-align: end;">
                                                                    <button class="btn btn-primary preview_nricfront" id="remove_nricfront" type="button">Remove</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <label>Nric back :</label><br>
                                                                <input type="file" name="nricback" id="nricback" class="form-control nricback">
                                                                <label style="font-size: small;" class="nricback">Allowed format .pdf,.png,.jpg, Max upload size 2MB</label>
                                                                <div style="text-align: center;">
                                                                    <img src=""  onclick="openModal();currentSlide(2)" id="img_nricback" class="preview_nricback" width="100px">
                                                                    <a href="" target="_blank" id="pdf_nricback" class="preview_nricback"><button type="button" class="btn btn-primary">Preview PDF</button></a>
                                                                </div>
                                                                <div style="text-align: end;">
                                                                    <button class="btn btn-primary preview_nricback" id="remove_nricback" type="button">Remove</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <label>Customer face :</label><br>
                                                                <input type="file" name="customerface" id="customerface" class="form-control customerface">
                                                                <label style="font-size: small;" class="customerface">Allowed format .pdf,.png,.jpg, Max upload size 2MB</label>
                                                                <div style="text-align: center;">
                                                                    <img src=""  onclick="openModal();currentSlide(3)" id="img_customerface" class="preview_customerface" width="100px">
                                                                    <a href="" target="_blank" id="pdf_customerface" class="preview_customerface"><button type="button" class="btn btn-primary">Preview PDF</button></a>
                                                                </div>
                                                                <div style="text-align: end;">
                                                                    <button class="btn btn-primary preview_customerface" id="remove_customerface" type="button">Remove</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row mt-2">
                                <div class="col-lg-12 col-sm-12 text-right">
                                    <button class="btn btn-primary m-1" id="add_imageform" name="add_imageform">Submit</button>
                                    <button type="button" class="btn btn-primary m-1" id="cancelform" name="cancelform">Cancel</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="lightbox_image" tabindex="-1" role="dialog" aria-labelledby="Modal_Title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 1200px;">
        <div class="modal-content"  id="lightbox_content">  <br>
            <div style="float: right;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-right: 25px;">
                    <span aria-hidden="true">&times;</span>
                </button> <br>
            </div>
            <div class="mySlides" style="text-align: center;">
                <img id="ligthbox_nricfront" src="" style="max-width: 1000px; max-height: 550px;">
            </div>

            <div class="mySlides" style="text-align: center;">
                <img id="ligthbox_nricback" src="" style="max-width: 1000px; max-height: 550px;">
            </div>

            <div class="mySlides" style="text-align: center;">
                <img id="ligthbox_custface" src="" style="max-width: 1000px; max-height: 550px;">
            </div>
            <div class="caption-container">
                <p id="lightbox_caption"></p>
            </div>
        </div>
    </div>
</div>
