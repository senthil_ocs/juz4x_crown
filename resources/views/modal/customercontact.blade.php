<!-- Modal -->
<div class="modal fade" id="get_customercontacts_modal_details" tabindex="-1" role="dialog" aria-labelledby="Modal_customercontacts_Title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Modal_customercontacts_Title">Contact Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="table_customercontact" class="table table-hover mb-3 table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <td class="text-center font-weight-bold" scope="col"></td>
                                <td class="text-center font-weight-bold" scope="col">ContactNricNo</td>
                                <td class="text-center font-weight-bold" scope="col">ContactName</td>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->