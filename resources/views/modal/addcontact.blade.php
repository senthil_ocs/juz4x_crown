<div class="modal fade" id="add_cust_contact" tabindex="-1" role="dialog" aria-labelledby="Modal_Title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="Modal_Title">Add Customer Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class="col-lg-12 mb-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="#" method="POST" id="addcontactform">
                            @csrf
                                <input type="hidden" name="optype" id="optype" value="">
                                <input type="hidden" name="cusid" id="cusid" value="">
                                <input type="hidden" name="code" id="code" value="">
                                <div class="row justify-content-md-center">
                                    <div class="col-lg-6 col-xl-6 mb-4 text-center">
                                        <button class="btn  btn-outline-primary m-1" type="button" id="owners" onclick="fillters('owner')">Owners</button>
                                        <button class="btn  btn-outline-primary m-1" type="button" id="dealers" onclick="fillters('dealer')">Dealers</button>
                                        <button class="btn  btn-outline-primary m-1" type="button" id="runners" onclick="fillters('runner')">Runners</button>
                                    </div>
                                </div>
                                <div class="row">                                        
                                    <div class="col-lg-12 col-sm-12 master-main-sec">
                                        <div class="card mb-4">
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="contact_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No.</th>
                                                                <th>Name as in NRIC/Passport</th>
                                                                <th>NRIC /EP/Passport</th>
                                                                <th>Nationality</th>
                                                                <th>DOB</th>
                                                                <th>Position</th>
                                                                <th>Signature</th>
                                                                <th>Active</th>
                                                                <th>Remarks</th>
                                                                <th>Date</th>
                                                            </tr>
                                                        </thead>    
                                                        <tbody id="contact_table_tbody">
                                                            <input type="hidden" name="delcontactid" id="delcontactid" value="">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">    
                                <!-- <form class="needs-validation" novalidate name="ccustomer" id="ccustomer"> -->
                                    <div class="col-lg-12 col-sm-12 master-main-sec">
                                        <div class="card mb-4">
                                            <div class="card-body">
                                            <div class="form-group row">
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                            <input type="text" name="contacttype" id="contacttype" autocomplete="off" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="cusname" id="cusname" placeholder="Name as in NRIC/Passport" size="50" class="form-control" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="nricno" id="nricno" placeholder="NRIC /EP/Passport" class="form-control" autocomplete="off" onkeyup="this.value = this.value.toUpperCase();">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <select class="form-control" name="contactnationality" id="contactnationality">
                                                                    <option>Select Nationality</option>
                                                                    @foreach($nationality as $value)
                                                                    <option value="{{trim($value->Code,' ')}}">{{trim($value->Description,' ')}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <?php /*<input type="text" name="contactnationality" id="contactnationality" placeholder="Nationality" class="form-control" autocomplete="off"> */ ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="text" name="contactdob" id="contactdob" placeholder="D.O.B" class="form-control" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <select class="form-control" name="contactposition" id="contactposition">
                                                                    <option value="">Select Position</option>
                                                                    <option value="Owner">Owner</option>
                                                                    <option value="Manager">Manager</option>
                                                                    <option value="Office Executive">Office Executive</option>
                                                                    <option value="Dealer">Dealer</option>
                                                                    <option value="Runner">Runner</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <input type="file" name="signature" id="signature" placeholder="Signature" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                            <div class="col-sm-12 m-1">
                                                                <label class="checkbox checkbox-primary">
                                                                    <input type="checkbox" id="contactactive" name="contactactive" checked="checked">
                                                                    <span style="padding-left: 20px;">Active</span>
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                             <div class="col-sm-12 m-1">
                                                                 <input type="text" name="contactremarks" id="contactremarks" placeholder="Remarks" size="80" class="form-control" autocomplete="off">
                                                             </div>    
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                             <div class="col-sm-12 m-1">
                                                                 <input type="text" name="doe" id="doe" placeholder="Date" class="form-control" autocomplete="off">
                                                             </div>    
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-lg-3">
                                                        <div class="row">
                                                             <div class="col-sm-12 m-1 text-right">
                                                                 <input type="button" class="btn btn-primary" name="addcontact" id="addcontact" value="ADD">
                                                             </div>    
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="row">
                                                             <div class="col-sm-12 m-1">
                                                                 <input type="button" class="btn btn-primary" name="deletecontact" id="deletecontact" value="DELETE">
                                                             </div>    
                                                        </div>
                                                    </div> -->
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row mt-2">
                                <div class="col-lg-12 col-sm-12 text-right">
                                    <button class="btn btn-primary m-1" type="button" id="add_contactform">Submit</button>
                                    <input  class="btn btn-primary m-1" type="button" data-dismiss="modal"  value="Cancel">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal