<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            
            @if(env('ENABLE_DEAL') == 1)
            <li class="nav-item {{ (strpos(Request::segment(1), 'deal') !== false) && (Request::segment(1) != 'deallist') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('dealview')}}"  >
                    <i><img src="{{asset('assets/images/deal.png')}}"></i>
                    <span class="nav-text">FxDeal</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
            @if(env('ENABLE_DD_TT') == 1)
            <li class="nav-item {{ strpos(Request::segment(2), 'unpost') !== false ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('ddtransaction.unpost')}}"  >
                    <i><img src="{{asset('assets/images/dd-tt.png')}}"></i>
                    <span class="nav-text">Remit Online</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ (strpos(Request::segment(1), 'reports') !== false) && (Request::segment(2) != 'ledger') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('reports.ledger')}}"  >
                    <i><img src="{{asset('assets/images/dd-tt.png')}}"></i>
                    <span class="nav-text">Ledger Journal</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
            <li class="nav-item {{ strpos(Request::segment(1), 'buysell')  !== false ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('buysell.view')}}">
                    <i><img src="{{asset('assets/images/bs.png')}}"></i>
                    <span class="nav-text">Forex</span>
                </a>
                <div class="triangle"></div>
            </li>
            <!-- <li class="nav-item {{ (strpos(Request::segment(1), 'currency')  !== false) || (strpos(Request::segment(2), 'currency')  !== false) ? 'active' : '' }}">
                <a class="nav-item-hold  {{ Route::currentRouteName()=='currency_master' ? 'open' : '' }}" href="{{route('currency_master_list')}}">
                    <i><img src="{{asset('assets/images/currency.png')}}"></i>
                    <span class="nav-text">Currency</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item {{ (strpos(Request::segment(1), 'location')  !== false) || (strpos(Request::segment(2), 'location')  !== false) ? 'active' : '' }}">
                <a class="nav-item-hold {{ Route::currentRouteName()=='location_stock' ? 'open' : '' }}" href="{{route('location_stock')}}">
                    <i><img src="{{asset('assets/images/stock.png')}}"></i>
                    <span class="nav-text">Stock</span>
                </a>
                <div class="triangle"></div>
            </li> -->
            <!--  || (strpos(Request::segment(2), 'customer')  !== false) -->

            <li class="nav-item {{ (strpos(Request::segment(1), 'cashcustomer')  !== false) ? 'active' : '' }}">
                <a class="nav-item-hold " href="{{route('cashcustomer.view')}}">
                    <i><img src="{{asset('assets/images/customer.png')}}"></i>
                    <span class="nav-text">Cash Customer</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ (strpos(Request::segment(1), 'customer_master')  !== false) ? 'active' : '' }}">
                <a class="nav-item-hold {{ Route::currentRouteName()=='customer_master' ? 'open' : '' }}" href="{{route('customer_master')}}">
                    <i><img src="{{asset('assets/images/customer.png')}}"></i>
                    <span class="nav-text">Corporate Customer</span>
                </a>
                <div class="triangle"></div>
            </li>

             <li class="nav-item {{ strpos(Request::segment(1), 'balances')  !== false ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('currency_balance')}}" >
                    <i><img src="{{asset('assets/images/balance.png')}}"></i>
                    <span class="nav-text">Balance</span>
                </a>
                <div class="triangle"></div>
            </li>
             <li class="nav-item {{ strpos(Request::segment(1), 'logout')  !== false ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i><img src="{{asset('assets/images/logout.jpg')}}"></i>
                    <span class="nav-text">Logout</span>
                </a>
                <div class="triangle"></div>
            </li>
            @if(env('ENABLE_DEAL') == 1)
           <!--  <li class="nav-item  {{ Request::segment(1) == 'deallist' ? 'active' : '' }} ">
                <a class="nav-item-hold {{ Route::currentRouteName()=='deallist' ? 'open' : '' }}" href="{{route('deallist')}}">
                    <i><img src="{{asset('assets/images/deallist.png')}}"></i>
                    <span class="nav-text">Deal List</span>
                </a>
                <div class="triangle"></div>
            </li> -->
            @endif
             <!-- <li class="nav-item">
                <a class="nav-item-hold" href="#" >
                    <i><img src="{{asset('assets/images/dayend.png')}}"></i>
                    <span class="nav-text">Day End</span>
                </a>
                <div class="triangle"></div>
            </li> -->

        </ul>
    </div>

    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        <ul class="childNav" data-parent="starter">
            <li class="nav-item ">
                <a class="{{ Route::currentRouteName()=='dashboard' ? 'open' : '' }}" href="{{route('dashboard')}}">
                    <i><img src="{{asset('assets/images/db.png')}}"></i>
                    <span class="item-name">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('normal')}}" class="{{ Route::currentRouteName()=='normal' ? 'open' : '' }}">
                    <i class="nav-icon i-Clock-4"></i>
                    <span class="item-name">Normal Sidebar</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='compact' ? 'open' : '' }}" href="{{route('compact')}}">
                    <i class="nav-icon i-Over-Time"></i>
                    <span class="item-name">Compact Sidebar</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="{{ Route::currentRouteName()=='horizontal' ? 'open' : '' }}" href="{{route('horizontal')}}">
                    <i class="nav-icon i-Clock"></i>
                    <span class="item-name">Horizontal Sidebar</span>
                </a>
            </li>
        </ul>

    </div>
    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->