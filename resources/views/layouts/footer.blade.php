<!-- Footer Start -->
<div class="flex-grow-1"></div>
<div class="app-footer card">
    <div class="footer-bottom pt-3 d-flex flex-column flex-sm-row align-items-center">
        <a class="btn btn-primary text-white btn-rounded" href="https://www.juzapps.com" target="_blank">Visit More</a>
        <span class="flex-grow-1"></span>
        <div class="d-flex align-items-center">
            <div>
                <p class="m-0">&copy; {{date('Y')}} {{ env('COPY_RIGHT') }}</p>
                <p class="m-0">All rights reserved</p>
            </div>
        </div>
    </div>
</div>
<!-- fotter end -->
<script src="{{asset('assets/js/hotkeys.js')}}"></script>
<script src="{{asset('assets/js/keyboard_shotcuts.js')}}"></script>