<style type="text/css">
    .links, .d-inline-block {
        font-size: 0.99rem;
    }
</style>
<div class="main-header">
    <div class="inner-logo">
        @if(env('ENABLE_DD_TT') == '1')
            <a href="{{route('dashboard')}}">
                <img src="{{asset('assets/images/logo.png')}}" alt="">
                <p>{{ env('LOGO_TITLE') }}</p> 
            </a>
        @else
            <a href="{{route('home')}}">
                <img src="{{asset('assets/images/logo.png')}}" alt="">
                <p>{{ env('LOGO_TITLE') }}</p> 
            </a>
        @endif
    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div class="d-flex align-items-center">
        <!-- Mega menu -->

        <div class="dropdown mega-menu d-none d-md-block">     
            <a href="#" class="btn text-muted dropdown-toggle mr-3" style="font-size:0.99rem;"  id="masterMenuDropdown" alt="" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><span style="text-decoration: underline;">M</span>aster</a>
                <div class="dropdown-menu dropdown-menu-left " aria-labelledby="masterMenuDropdown" id="masterMenuDropdownSection">

                    <div class="row m-0">
                        <div class="col-md-2 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">System</p>
                            <ul class="links">
                                <li><a href="{{route('company')}}">Company Master</a></li>
                            </ul>
                        </div>

                        <div class="col-md-2 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Currency</p>
                            <ul class="links">
                                <li><a href="{{route('currency_master_list')}}">Currency</a></li>
                                <li><a href="{{url('/location')}}">Location Currency Stock</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Customer</p>
                            <ul class="links">
                                <li><a href="{{route('customer_master')}}">Customer(Corporate)</a></li>
                                <li><a href="{{route('cashcustomer.view')}}">Customer(Cash)</a></li>
                                <!-- <li><a href="{{route('cashcustomer.view')}}">Cash Customer Master</a></li> -->
                                @if (env('ENABLE_DD_TT')=='1')
                                <li><a href="{{route('orginator.view')}}">Orginator</a></li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-3 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Others</p>
                            <ul class="links">
                                <li><a href="{{route('nationality.view')}}">Nationality Master</a></li>
                                <li><a href="{{route('settlement.view')}}">Settlement Mode Master</a></li>
                                <li><a href="{{route('sourceofincome.view')}}">Source Of Income</a></li>
                                <li><a href="{{route('bank.view')}}">Bank Master</a></li>
                                <li><a href="{{route('swift.view')}}">Swift Master</a></li>
                                <li><a href="{{ route('ddtransaction.purpose') }}">Purpose Master</a></li>
                            </ul>
                        </div>
                        <!-- <div class="col-md-3 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Master Section</p>
                            <ul class="links">
                                <li><a href="{{route('currency_master_group')}}">Currency Group Master</a></li>
                                <li><a href="{{route('service.view')}}">Service</a></li>
                                <li><a >Message</a></li>
                                <li><a>Customer Display</a></li>
                                <li><a>Currency Display</a></li>
                                <li><a>Broad Cast</a></li>
                                <li><a>Customers Runner</a></li>
                            </ul>
                        </div> -->
                    </div>
                </div>
         </div>

        <div class="dropdown mega-menu d-none d-md-block">
            <a href="#" class="btn text-muted dropdown-toggle mr-3" style="font-size:0.99rem;"  alt="" data-toggle="dropdown" id="transactionMenuDropdown"
                    aria-haspopup="true" aria-expanded="false"><span style="text-decoration: underline;">T</span>ransaction</a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="transactionMenuDropdown" id="transactionMenuDropdownSection">
                    <div class="row m-0">
                        @if(env('ENABLE_DEAL') == 1)
                        <div class="col-md-2 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Booking</p>
                            <ul class="links">
                                <li><a href="{{route('dealview')}}">Deal Transaction</a></li>
                                <li><a href="{{route('deallist')}}">Deal Transaction List</a></li>
                                <li><a href="{{route('reports.ledger')}}">Ledger</a></li>
                                <!-- @if (env('ENABLE_DD_TT')=='1') -->
                                <!-- @endif -->
                            </ul>
                        </div>
                        @if (env('ENABLE_DD_TT')=='1')
                        <div class="col-md-2 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">DD Unpost</p>
                            <ul class="links">
                                <li><a href="{{route('ddtransaction.unpost')}}">Unpost</a></li>
                                <li><a href="{{ route('ddtransaction.unpostsearchlist') }}">Search</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">DD Entry</p>
                            <ul class="links">
                                <li><a href="{{route('ddtransaction')}}">Entry</a></li>
                                <li><a href="{{ route('ddtransaction.searchlist') }}">List</a></li>
                            </ul>
                        </div>
                         @endif
                        @endif
                        <div class="col-md-2 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Buy/Sell</p>
                            <ul class="links">
                                <li><a href="{{route('buysell.view')}}">Entry</a></li>
                                <li><a href="{{route('buyselltransactionreport')}}">List</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Balance</p>
                            <ul class="links">
                                <li><a href="{{route('currency_balance')}}">Balances</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- <a class="dropdown-item">Cum.Balance</a>
                    <a class="dropdown-item">Day End</a> -->
                </div>
        </div>

        @if(env('ENABLE_DD_TT') == 1)
            <!-- <div class="dropdown test">
                <a href="#" class="btn text-muted dropdown-toggle mr-3" style="font-size:0.99rem;"  alt="" data-toggle="dropdown" id="transactionMenuDropdown"
                        aria-haspopup="true" aria-expanded="false">DD/TT</a>
                    <div class="dropdown-menu dropdown-menu-left" aria-labelledby="transactionMenuDropdown" id="transactionMenuDropdownSection">
                         <a class="dropdown-item" href="{{route('AptDDtranscation')}}">APT DD/TT Entry</a> 
                    </div>
            </div> -->
        @endif

        @if (env('ENABLE_DD_TT')=='1')
        <div class="dropdown">
            <a href="#" class="btn text-muted dropdown-toggle mr-3" style="font-size:0.99rem;"  alt="" data-toggle="dropdown" id="kycMenuDropdown"
                    aria-haspopup="true" aria-expanded="false">KYC Analysis</a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="kycMenuDropdown" id="kycMenuDropdownSection">
                    <a class="dropdown-item">Customer Category</a>
                    <a class="dropdown-item">Customer Tran Limit</a>
                    <a class="dropdown-item">KYC Analysis Report</a>
                    <a class="dropdown-item">Suspected Customer Reports</a>
                    <a class="dropdown-item">Suspicious Transaction</a>
                </div>
        </div>
        @endif


       <!-- <div class="dropdown">
            <a href="#" class="btn text-muted dropdown-toggle mr-3" style="font-size:0.99rem;"  alt="" data-toggle="dropdown" id="amlMenuDropdown"
                    aria-haspopup="true" aria-expanded="false">AML Watch List</a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="amlMenuDropdown" id="amlMenuDropdownSection">
                    <a class="dropdown-item">AML search</a>
                </div>
        </div> -->

        <div class="dropdown">
            <a href="#" class="btn text-muted dropdown-toggle mr-3" style="font-size:0.99rem;"  alt="" data-toggle="dropdown" id="securityMenuDropdown"
                    aria-haspopup="true" aria-expanded="false">Se<span style="text-decoration: underline;">c</span>urity</a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="securityMenuDropdown" id="securityMenuDropdownSection">
                    <a class="dropdown-item" href="{{route('profile')}}">User Group</a>
                    <a class="dropdown-item" href="{{route('user_master')}}">User</a>                    
                </div>
        </div>


         <div class="dropdown mega-menu d-none d-md-block">
            <a href="#" class="btn text-muted dropdown-toggle mr-3" style="font-size:0.99rem;" id="dropdownMegaMenuButton" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false"><span style="text-decoration: underline;">R</span>eports</a>
            <div class="dropdown-menu text-left" aria-labelledby="dropdownMenuButton" id="reportMenuDropdownSection">
                <div class="row m-0">
                    
                    <div class="col-md-3 p-4">
                        <p class="text-primary text--cap border-bottom-primary d-inline-block">MAS Report</p>                       
                        <ul class="links">
                            <li><a href="#">Transaction Volume Report</a></li>
                            <li><a href="#">All Money Changing Transaction Report</a></li>
                            <li><a href="#">All Money Changing Transaction of S$5000 & above</a></li>
                            <li><a href="#">Above 5000 of same day</a></li>
                            <li><a href="#">Statement of Transaction</a></li>
                            <li><a href="#">Above 5000 of same day May Bank</a></li>                            
                        </ul>
                    </div>
                    <div class="col-md-3 p-4">
                        <p class="text-primary text--cap border-bottom-primary d-inline-block">Transaction Report</p>
                        <ul class="links">
                            <li><a href="{{route('buyselltransactionreport')}}">Buy Sell Transaction Report</a></li>
                            @if(env('ENABLE_DEAL') == 1)
                            <li><a href="{{route('dealtransactionreport')}}">Deal Transaction Report</a></li>
                            @endif
                            <li><a href="{{route('profitreport')}}">Profit Report</a></li>
                        </ul>
                        @if(env('ENABLE_DD_TT') == 0)
                        </div>
                        <div class="col-md-3 p-4">
                        @endif 
                        <p class="text-primary text--cap border-bottom-primary d-inline-block">Currency Report</p>
                        <ul class="links">
                            <li><a href="{{route('reports.locationStockReport')}}">Locationwise Stock Report</a></li>
                            <li><a href="{{route('reports.ledgercurrency')}}">Locationwise Currency Ledger</a></li>
                            <li><a href="{{route('reports.currencyWiseTransactionReport')}}">Currency wise Transaction Report</a></li>
                        </ul>
                    </div>

                    <div class="col-md-3 p-4">
                        <p class="text-primary text--cap border-bottom-primary d-inline-block">Customer Reports</p>
                        <ul class="links">
                            <li><a href="{{route('reports.customer')}}">Customer List (Corporate)</a></li>
                            <li><a href="{{route('reports.cashcustomer')}}">Customer List (Cash)</a></li>
                            <li><a href="{{route('reports.customerbalancereport')}}">Customer Balance Report</a></li>
                            <li><a href="{{route('reports.ledgercustomer')}}">Customer Ledger</a></li>
                            <li><a href="#">Top Customer</a></li>
                        </ul>
                    </div>

                    @if(env('ENABLE_DD_TT') == 1)
                    <div class="col-md-3 p-4">
                        <p class="text-primary text--cap border-bottom-primary d-inline-block">Booking Report</p>
                        <ul class="links">
                            <li><a href="{{route('reports.ttbookingreport')}}">Booking</a></li>
                        </ul>
                    <!-- </div>
                    <div class="col-md-3 p-4"> -->
                        <p class="text-primary text--cap border-bottom-primary d-inline-block">TT Report</p>
                        <ul class="links">
                            <li><a href="{{route('reports.ttmasreport')}}">TT MAS Report</a></li>
                            <li><a href="{{route('reports.tttransactionsummary')}}">TT Transaction Summary </a></li>
                            <li><a href="{{route('reports.tttransactiondetail')}}">TT Transaction Detail</a></li>
                            <li><a href="{{route('reports.ttfundsaccepted')}}">TT Funds Accepted</a></li>
                            <li><a href="{{route('reports.ttfundsremitted')}}">TT Funds Remitted</a></li>
                            <li><a href="{{route('reports.ttdeletedreport')}}">TT Deleted Report</a></li>
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
        </div>    

        
               
       
        <!-- / Mega menu -->
       
    </div>

    <div style="margin: auto" id="topspace"></div>

    <div class="header-part-right">
        <!-- User avatar dropdown -->
            <div class="user col align-self-end">
                <div class="card-icon">
                    <label style="font-size: medium;">
                        {{Auth::user()->username}} ( {{Auth::user()->Location}} )
                    </label>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="i-Lock"></i>
                    </a>
                </div>
            </div>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
       
    </div>

</div>
<!-- header top menu end -->