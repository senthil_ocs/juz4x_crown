@extends('layouts.master')

@section('main-content')
<div class="breadcrumb">
    <h1>Company</h1>
    <ul>
        <li><a href="">System</a></li>
        <li>Company Master</li>
    </ul>
</div>

<div class="separator-breadcrumb border-top">
    
</div>
<div class="row mb-12 master-main">
    <div class="col-md-12 mb-12">
        <div class="card text-left">
            <div class="card-body">
                 <form id="frmcompany" name="frmcompany" class="needs-validation" method="post" novalidate action="{{ route('updatecompany') }}">
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6 col-sm-12 master-main-sec">
                            <div class="card mb-6">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="company_name" class="col-sm-3 col-form-label">Company Name</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" name="company_name" id="company_name" placeholder="" 
                                                    value="{{$companyDetails['Description']}}" required>
                                                    <div class="invalid-feedback">
                                                            Please Enter Company Name.
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="address1" class="col-sm-3 col-form-label">Company Address</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="address1" name="address1" placeholder="" value="{{$companyDetails['Address1']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="address2" class="col-sm-3 col-form-label"></label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="address2" name="address2" placeholder="" value="{{$companyDetails['Address2']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="address3" class="col-sm-3 col-form-label"></label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="address3" name="address3" placeholder="" value="{{$companyDetails['Address3']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="postal_code" class="col-sm-3 col-form-label">Postal Code</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="postal_code" name="postal_code" placeholder="" value="{{$companyDetails['Postalcode']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="email" class="col-sm-3 col-form-label">Email</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="email" name="email" placeholder="" value="{{$companyDetails['Email']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="url" class="col-sm-3 col-form-label">URL</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="url" name="url" placeholder="" value="{{$companyDetails['url']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="url" class="col-sm-3 col-form-label">Phone</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="phone" name="phone" placeholder="" value="{{$companyDetails['Phone']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-12 master-main-sec">
                            <div class="card mb-6">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="url" class="col-sm-3 col-form-label">Fax</label>
                                                <div class="col-sm-9">
                                                     <input type="textbox" class="form-control" id="fax" name="fax" placeholder="" value="{{$companyDetails['Fax']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="url" class="col-sm-3 col-form-label">UEN NO</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="uen_no" name="uen_no" placeholder="" value="{{$companyDetails['UENNo']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="tax_ref" class="col-sm-3 col-form-label">Lic.No</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="tax_ref" name="tax_ref" placeholder="" value="{{$companyDetails['TaxRef']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="tax_perc" class="col-sm-3 col-form-label">Tax Perc</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="tax_perc" name="tax_perc" placeholder="" value="{{$companyDetails['TaxPerc']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(env('ENABLE_DD_TT') == '1')
                                     <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="mass_limit" class="col-sm-3 col-form-label">Mass Limit</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="mass_limit" name="mass_limit" placeholder="" value="{{$companyDetails['MassLimit']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="mass_password_attampt" class="col-sm-3 col-form-label">Mass Password Attampt</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="mass_password_attampt" name="mass_password_attampt" placeholder="" value="{{$companyDetails['MaxPasswordAttempt']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label for="password_validity" class="col-sm-3 col-form-label">Password Validity</label>
                                                <div class="col-sm-9">
                                                    <input type="textbox" class="form-control" id="password_validity" placeholder="" value="{{$companyDetails['PasswordValidity']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 master-main-sec text-right">
                            <div class="card mb-6">
                                <div class="card-body">
                                    <button class="btn  btn-primary m-1" type="submit">Save( F3 )</button>
                                    <button type="button" class="btn btn-outline-secondary m-1">Close( Esc )</button>
                                </div> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>        
    </div>            
</div>


@endsection

@section('page-js')

@endsection

@section('bottom-js')
 <script src="{{asset('assets/js/form.validation.script.js')}}"></script>
 @if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
    hotkeys('f3', function(event, handler){
      event.preventDefault();
      $('#frmcompany').submit();
    });
</script>
@endsection
