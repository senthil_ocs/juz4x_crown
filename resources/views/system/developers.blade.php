@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Developer</h1>
			<ul>
			    <li>logs</li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>
		<div class="row mb-12 master-main">

			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Delete Log</div>
						<form id="logdelete" name="logdelete" class="needs-validation"  novalidate action="{{route('clearlog')}}" method="POST">
							@csrf
							<div class="form-group row m-0">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="Code" class="col-lg-1 col-sm-4 col-form-label">Date<span class="mandatory">*</span></label>
							            <div class="col-lg-3 col-sm-4 ">
											<input id="logdate" class="form-control" placeholder="dd-mm-yyyy" name="logdate" required>
		                                    <div class="invalid-feedback code_error">
		                                        Please enter Dete
		                                    </div>
							            </div>
							            <div class="col-lg-2 col-sm-4">
	                                    	<button class="btn  btn-primary" type="submit">Delete</button>
							            </div>
							        </div>
							    </div>
							</div>
                    	</form>
					</div>	
				</div>
			</div>
		</div>	
    @endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection
@section('bottom-js')
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
$(document).ready(function(){
	var dformat = "{{ env('DATE_FORMAT') }}";
    $('#logdate').pickadate({
        format: dformat,
    });
});
</script>
@endsection