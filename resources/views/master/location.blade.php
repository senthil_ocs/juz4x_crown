@extends('layouts.master')

@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<style type="text/css">
.tableclicked{
    cursor: pointer;
}
.addbtn {
    float: left;
}
.breadcrumbbtn{
    text-align: right;
}
</style>
@endsection


@section('main-content')
<div class="breadcrumb addbtn">
    <h1>Location Stock</h1>
    <ul>
        <li><a></a></li>
    </ul>
</div>
<div class="breadcrumbbtn">
    <a type="button" class="btn btn-primary text-white m-1" href="{{route('currency_master')}}">Create</a>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row mb-12">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card mb-4">
            <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="location_code" class="col-sm-2 col-form-label">
                                    <a onclick="GetLocationModalDetails('location');" data-toggle="modal" data-target="#get_location_modal_details" href="">
                                        Location Code
                                    </a>  
                                </label>
                                <div class="col-sm-6">
                                    <input type="hidden" name="location_code_id" value="">
                                    <input type="textbox" class="form-control ui-autocomplete-input" id="location_code" name="location_code" placeholder="Location Code" autocomplete="off" value="{{Auth::user()->Location}}" onkeyup="this.value = this.value.toUpperCase();">
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <label for="location_name" class="col-sm-2 col-form-label">Location Name</label>
                                <div class="col-sm-6">
                                    <input type="textbox" class="form-control" id="location_name" name="location_name" placeholder="Location Name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table id="scroll_horizontal_vertical_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width: 34px;">Sno</th>
                                    <th>CurrencyCode</th>
                                    <th>Description</th>
                                    <th>BuyRate</th>
                                    <th>SellRate</th>
                                    <th>Stock</th>
                                    <th>AvgCost</th>
                                    <th>Varience</th>
                                </tr>
                            </thead>
                        </table>    
                    </div>
                    <hr>
                    <form id="currency_update" name="currency_update" class="needs-validation" method="post" novalidate action="#" >
                        {{ csrf_field() }}
                        <input type="hidden" name="currency_update_id" value="" id="currency_update_id">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <label for="currency" class="col-sm-2 col-form-label">Currency</label>
                                    <div class="col-sm-2">
                                        <input type="textbox" class="form-control" id="currency" name="currency" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                        <div class="invalid-feedback currency_required">
                                            Please enter Currency
                                        </div>
                                    </div>

                                    <label for="buy_rate" class="col-sm-2 col-form-label">Buy Rate</label>
                                    <div class="col-sm-2">
                                        <input type="textbox" class="form-control" id="buy_rate" name="buy_rate">
                                    </div>

                                    <label for="stock" class="col-sm-2 col-form-label">Stock</label>
                                    <div class="col-sm-2">
                                        <input type="textbox" class="form-control" id="stock" name="stock">
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <label for="description" class="col-sm-2 col-form-label">Description</label>
                                    <div class="col-sm-2">
                                        <input type="textbox" class="form-control" id="description" name="description">
                                    </div>

                                    <label for="sell_rate" class="col-sm-2 col-form-label">Sell Rate</label>
                                    <div class="col-sm-2">
                                        <input type="textbox" class="form-control" id="sell_rate" name="sell_rate">
                                    </div>

                                    <label for="variance" class="col-sm-2 col-form-label">Variance</label>
                                    <div class="col-sm-2">
                                        <input type="textbox" class="form-control" id="variance" name="variance" readonly>
                                    </div>                                  
                                </div>    
                            </div>   
                        </div>  

                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <label for="dealstock" class="col-sm-2 col-form-label">Deal Stock</label>
                                    <div class="col-sm-2">
                                        <input type="textbox" class="form-control" id="dealstock" name="dealstock">
                                    </div>
                                    <label for="avg_cost" class="col-sm-2 col-form-label">Avg Cost</label>
                                    <div class="col-sm-2">
                                        <input type="textbox" class="form-control" id="avg_cost" name="avg_cost">
                                    </div>                                  
                                </div>    
                            </div>   
                        </div>    

                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-sm-6 col-form-label"></div>
                                    <div class="col-sm-6 text-right">
                                        <button class="btn  btn-primary m-1" type="button" onclick="submitForm('update');" >Update (F2)</button>
                                        <button class="btn  btn-primary m-1" type="button" onclick="submitForm('clear');">Clear (F3)</button>
                                        <button class="btn  btn-primary m-1" type="button" onclick="submitForm('cancel');">Cancel (F5)</button>
                                        <button class="btn  btn-primary m-1" type="button" onclick="submitForm('esc');">Close (Esc)</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@include('modal.location')
@endsection

@section('page-js')
<script src="{{asset('assets/js/modal.details.js')}}"></script>
@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#currency_update').hide();
        setTimeout(function(){
            $("#location_code").trigger("blur");
        }, 100);
        $('#scroll_horizontal_vertical_table').DataTable({
            paging:   false,
            autoWidth : false,
            info:     false,
        });
        $(document).on("click", "input[name='location_id_detail']",function() {
            $('#get_location_modal_details').modal('toggle');
            $('input[name="location_code"]').val($(this).val());
            $("#location_code").trigger("blur");
        });

       /* jQuery.extend( jQuery.fn.dataTableExt.oSort, {
            "formatted-num-pre": function ( a ) {
                a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
                console.log(parseInt(a));
                return parseInt( a );
            },
            "formatted-num-asc": function ( a, b ) {
                console.log(parseInt(b));
                return a - b;
            },
            "formatted-num-desc": function ( a, b ) {
                return b - a;
            }
        }); */

        $('#location_code').blur(function() {
            submitForm('cancel');
            var l_code = $('#location_code').val();
            $.ajax({
                url: "./location/get_location_name",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    data : l_code,
                },
                 beforeSend: function() {
                    $('input[name="location_name"]').val('');
                 },
                success: function(response){
                    if(response.status == 200){
                        $('input[name="location_name"]').val(response.data.LocationName);
                    } else {
                        $('input[name="location_name"]').val('');
                    }
                },      
            });
            var scroll = '{{ env('TABLESCROLL') }}';
            var vtable = $('#scroll_horizontal_vertical_table').DataTable({
                paging:   false,
                destroy: true,
                serverSide: true,
                autoWidth : false,
                info:     false,
                scrollY: scroll,
                ajax: {
                    url: "./location/get_location_order",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function (d) {
                        d.data = $('#location_code').val();
                    },
                },
                order: [],
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'CurrencyCode', name: 'CurrencyCode'},
                    {data: 'Description', name: 'Description'},
                    {data: 'BuyRate', name: 'BuyRate', class:"text-right"},
                    {data: 'SellRate', name: 'SellRate', class:"text-right"},
                    {data: 'Stock', name: 'Stock', class:"text-right"},
                    {data: 'AvgCost', name: 'AvgCost', class:"text-right"},
                    {data: 'Varience', name: 'Varience', class:"text-right"},
                ],
            });
        });

        $(document).on("click", "#scroll_horizontal_vertical_table tbody tr",function() {
            $.ajax({
                url: "./location/get_location_order_details",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    data : $(this).attr('id'),
                },
                beforeSend: function() {

                },
                success: function(response){
                    if(response.status == 200){
                        $('#currency_update').show();
                        $('#currency_update_id').val(response.data.id)
                        $('input[name="currency"]').val(response.data.CurrencyCode);
                        $('input[name="buy_rate"]').val(response.data.BuyRate);
                        $('input[name="stock"]').val(response.data.Stock);
                        $('input[name="dealstock"]').val(response.data.DealStock);
                        var Stock=response.data.Stock.replace(/\,/g,'');
                        if(parseInt(Stock) > 0){
                            $('input[name="stock"]').prop('readonly', true);
                        } else {
                            $('input[name="stock"]').prop('readonly', false);
                        }
                        $('input[name="description"]').val(response.data.Description);
                        $('input[name="sell_rate"]').val(response.data.SellRate);
                        $('input[name="variance"]').val(response.data.Varience);
                        $('input[name="avg_cost"]').val(response.data.AvgCost);
                    }
                },
                error: function(){
                }
            });
        });
    });

    function submitForm(submit_type){
        if(submit_type == 'clear'){
                clear_currency_error();
                $('#currency_update_id').val('')
                $('input[name="currency"]').val('');
                $('input[name="buy_rate"]').val('');
                $('input[name="stock"]').val('');
                $('input[name="dealstock"]').val('');
                $('input[name="description"]').val('');
                $('input[name="sell_rate"]').val('');
                $('input[name="variance"]').val('');
                $('input[name="avg_cost"]').val('');                
        }else if(submit_type == 'cancel'){
            clear_currency_error();
            $('#currency_update').hide();
        } else if(submit_type == 'close') {
            clear_currency_error();
            $('#currency_update').hide();
        } else if(submit_type == 'update') {
            if($('input[name="currency"]').val() == ''){
                $('input[name="currency"]').css('border-color','#f44336');
                $('.currency_required').css('display', 'block');
                return false;
            }
            clear_currency_error();
            $.ajax({
                url: "./location/get_location_order_update",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    currency_update_id : $('#currency_update_id').val(),
                    currency    : $('input[name="currency"]').val(),
                    buy_rate    : $('input[name="buy_rate"]').val(),
                    stock       : $('input[name="stock"]').val(),
                    dealstock   : $('input[name="dealstock"]').val(),
                    sell_rate   : $('input[name="sell_rate"]').val(),
                    avg_cost    : $('input[name="avg_cost"]').val(),
                },
                success: function(response){
                    if(response.status == 200){
                        $( "#location_code" ).blur();
                        submitForm('clear');
                        toastr.success(response.content,{
                            "positionClass": "toast-top-right",
                            timeOut: 5000,
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut",
                            "tapToDismiss": false
                        })
                    } else {
                        $( "#location_code" ).blur();
                        submitForm('clear');
                        toastr.error(response.content,{
                            "positionClass": "toast-top-right",
                            timeOut: 5000,
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut",
                            "tapToDismiss": false
                        })
                    }
                },      
            });
        } else if(submit_type == 'esc') {
            window.location.href = 'dashboard';
        }
    }
    function locationclick(location_name) {
        $('input[name="location_name"]').val('location_name')
    }
    function clear_currency_error() {
        $('input[name="currency"]').css('border-color','#ced4da');
        $('.currency_required').css('display', 'none');
    }

    hotkeys('f2', function(event, handler){
      event.preventDefault();
        submitForm('update');
    });
    hotkeys('f3', function(event, handler){
      event.preventDefault();
        submitForm('clear');
    });
    hotkeys('f5', function(event, handler){
      event.preventDefault();
        submitForm('cancel');
    });
</script>


@endsection
