@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<style type="text/css">
table#table_service > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Service Maintenance</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			    <li>Create</li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>
		<div class="row mb-12 master-main">
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Listing</div>
                        <div class="table-responsive">
                            <table id="table_service" class="display table table-striped table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Service Code</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                            </table>    
                        </div>
					</div>	
				</div>
			</div>	
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Create</div>
						
						<form id="frm_service" name="frm_service" class="needs-validation"  novalidate action="#" method="post">
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="service_code" class="col-sm-4 col-form-label">Service Code</label>
							            <div class="col-sm-8">
							            	<input type="hidden" name="service_id" id="service_id">
							                <input type="textbox" class="form-control" name="service_code" id="service_code" placeholder="Enter Service Code" required onkeyup="this.value = this.value.toUpperCase();">
		                                    <div class="invalid-feedback">
		                                        Please enter Service code
		                                    </div>
							            </div>	
							        </div>
							    </div>
							</div>
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="description" class="col-sm-4 col-form-label">Description</label>
							            <div class="col-sm-8">
							                <textarea class="form-control description"  id="description"  name="description" placeholder="Enter Description" required> {{ old('description') }}</textarea>
											<div class="invalid-feedback">
											    Please enter description
											</div>
							            </div>
							        </div>
							    </div>
							</div>	
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="amount" class="col-sm-4 col-form-label">Amount</label>
							            <div class="col-sm-8">
							                <input type="textbox" class="form-control" name="amount" id="amount" placeholder="Enter Amount" required>
		                                    <div class="invalid-feedback">
		                                        Please enter Amount
		                                    </div>
							            </div>	
							        </div>
							    </div>
							</div>
							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyDate</label>
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyDate" class=" col-form-label"></label>
										</div>
									</div>
								</div>	
							</div>	
							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyUser</label>	
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyUser" class=" col-form-label"></label>	
										</div>									
									</div>	
								</div>	
							</div>	

	                		<div class="card mb-5">
	                            <div class="card-body">
	                                <div class="row row-xs">
	                                    <div class="col-lg-12 text-center">

	                                        <button class="btn btn-primary m-1" onclick="submitForm('new');" type="button">New (F2)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('save');" type="button" >Save (F3)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('delete');"type="button">Delete (F4)</button>

	                                        <button type="button" class="btn btn-primary m-1" onclick="submitForm('esc');" data-style="expand-left">Exit (Esc)</button>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
                    	</form>

					</div>	
				</div>
			</div>
		</div>	
    @endsection
    
@section('page-js')
	<script src="{{asset('assets/js/es5/dashboard.v2.script.js')}}"></script>
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
    $(document).ready(function(){
    	var scroll = '{{ env('TABLESCROLL') }}';
		$('#table_service').DataTable({
			paging:   false,
			destroy: true,
			processing: true,
			serverSide: true,
			autoWidth : false,
			info:     false,
			scrollY : scroll,
			scrollX: true,
			ajax: {
				url: "./service",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'ServiceCode', name: 'ServiceCode'},
				{data: 'Description', name: 'Description'},
				{data: 'Amount', name: 'Amount',className: "text-right"},
			],
		});

		$(document).on("click", "#table_service tbody tr",function() {
			
			$.ajax({
	    		url: "./service/getdetails",
	    		type: "POST",
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					id : $(this).attr('id'),
				},
				beforeSend: function() {},
				success: function(response){
					if(response.status == 200){
						$('#service_id').val(response.details.id);
						$('input[name="service_code"]').val(response.details.ServiceCode);
						$('.description').val(response.details.Description);
						$('input[name="amount"]').val(response.details.Amount);
						$('.Modify_hidden').show();
						$('#ModifyDate').html(response.details.ModifyDate);
						$('#ModifyUser').html(response.details.ModifyUser);
					}	
				},
				error: function(){}
	    	});
		});
    });

    function submitForm(submit_type){
    	var service_table = $('#table_service').dataTable();
    	if(submit_type == 'new'){

			$('#service_id').val('');
			$('input[name="service_code"]').val('');
			$('.description').val('');
			$('input[name="amount"]').val('');
			$('#ModifyDate').html('');
			$('#ModifyUser').html('');
			$('.Modify_hidden').hide(); 
			$('#frm_service').removeClass('was-validated');
			return false;  
			
    	} else if (submit_type == 'save') {

    		var service_code = $('input[name="service_code"]').val(); 
    		var description  = $('.description').val();
    		var amount = $('input[name="amount"]').val(); 
    		var service_id = $('#service_id').val();
    		if(service_code == '' ||  description == '' || amount == ''){
    			$('#frm_service').addClass('was-validated');
    			return false;
    		}
    		var type = '';
    		if(service_id == ''){
            	type = 'save';
            	id = '';
            } else {
            	type = 'update';
            	id = service_id;
            }
			$.ajax({
			    url: "./service/insert_update",
			    type: "POST",
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    data : {
			    	type 		: type,
			    	id   		: id,
			    	Code  		: service_code,
			    	description : description,
			    	amount      : amount,
			    },
			    success: function(response){
			        if(response.status == 200){
			            submitForm('new');
			            success_toastr(response.content);
						service_table.fnDraw(false);
			        } else {
			            submitForm('new');
			            $.each(response.content, function(v, t){
			            	danger_toastr(t);
			            });
			            service_table.fnDraw(false);
			        }
			    },      
			});
    	} else if (submit_type == 'delete') {
            var id = $('#service_id').val();
        	if(id == ''){
                return false;
            }
           
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete the Service?',
                buttons: {
                    confirm: function () {
						$.ajax({
						    url: "./service/delete",
						    type: "POST",
						    headers: {
						        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						    },
						    data : {
						    	id : id,
						    },
						    success: function(response){
						        if(response.status == 200){
						            submitForm('new');
						            success_toastr(response.content);
						            service_table.fnDraw(false);
						        } else {
						            submitForm('new');
						            danger_toastr(response.content);
						            service_table.fnDraw(false);
						        }
						    },      
						});
                    },
                    cancel: function () {}
                }
            });
    	} else if(submit_type == 'esc'){
    		window.location.href = 'dashboard'; 
    	}
    }

	function success_toastr(msg) {
	    toastr.success(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}

	function danger_toastr(msg) {
	    toastr.error(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}

	hotkeys('f2', function(event, handler){
	  event.preventDefault();
	    submitForm('new');
	});
	hotkeys('f3', function(event, handler){
	  event.preventDefault();
	    submitForm('save');
	});
	hotkeys('f4', function(event, handler){
	  event.preventDefault();
	    submitForm('delete');
	});
</script>
@endsection