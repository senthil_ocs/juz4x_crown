@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<style type="text/css">
table#scroll_horizontal_vertical_table > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Currency master group</h1>
			<ul>
			    <li><a href="">Listing</a></li>
			    <li>Create</li>
			</ul>
		</div>

		<div class="separator-breadcrumb border-top"></div>
		<div class="row mb-12 master-main">

			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Listing</div>
                        <div class="table-responsive" style="height: 250px;">
                            <table id="scroll_horizontal_vertical_table" class="display nowrap table table-striped table-bordered table_currency_master_group" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>GroupCode</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                            </table>    
                        </div>
					</div>	
				</div>
			</div>	

			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Create</div>
						
						<!-- <form id="frm_currency_master_group" name="frm_currency_master_group" class="frm_currency_master_group needs-validation"  novalidate action="#" method="post"> -->
							{{ csrf_field() }}

							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="inputEmail3" class="col-sm-4 col-form-label">Group Name</label>
							            <div class="col-sm-8">
							            	<input type="hidden" name="currency_master_group_id" id="currency_master_group_ids" value="">
							                <input type="textbox" class="form-control" name="group_name" id="inputEmail3" placeholder="Enter Group Name" required>
		                                    <div class="invalid-feedback group_name_error">
		                                        Please enter Group Name
		                                    </div>
							            </div>
							        </div>
							    </div>
							</div>
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="inputEmail3" class="col-sm-4 col-form-label">Description</label>
							            <div class="col-sm-8">
							                <textarea class="form-control description"  id="inputEmail3"  name="description" placeholder="Enter Description" required></textarea>
											<div class="invalid-feedback description_error">
											    Please enter Description
											</div>
							            </div>
							        </div>
							    </div>
							</div>	
							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyDate</label>
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyDate" class=" col-form-label"></label>
										</div>
									</div>
								</div>	
							</div>	
							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyUser</label>	
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyUser" class=" col-form-label"></label>	
										</div>									
									</div>	
								</div>	
							</div>	
	                		<div class="card mb-5">
	                            <div class="card-body">
	                                <div class="row row-xs">
	                                    <div class="col-lg-12 text-center">

	                                        <button class="btn btn-primary m-1" onclick="submitForm('new');" type="button">New (F2)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('save');" type="button" >Save (F3)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('delete');" type="button">Delete (F4)</button>

	                                        <button type="button" class="btn btn-primary m-1"  data-style="expand-left" type="button" onclick="submitForm('ecs');" >Exit (Esc)</button>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
                    	<!-- </form> -->
					</div>	
				</div>
			</div>
		</div>	
    @endsection
@section('page-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
	
    $(document).ready(function(){
		var table = $('#scroll_horizontal_vertical_table').DataTable({
			paging:   false,
			destroy: true,
			processing: true,
			serverSide: true,
			autoWidth : false,
			info:     false,
			ajax: {
				url: "./currency_master_group",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex' },
				{data: 'GroupCode', name: 'GroupCode'},
				{data: 'Description', name: 'Description'},
			],
		});

		$(document).on("click", ".table_currency_master_group tbody tr",function() {
			clear_currency_master_error();
			$.ajax({
	    		url: "./currency_master_group/getdetails",
	    		type: "POST",
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					id : $(this).attr('id'),
				},
				beforeSend: function() {},
				success: function(response){
					if(response.status == 200){
						$('#currency_master_group_ids').val(response.details.id);
						$('input[name="group_name"]').val(response.details.GroupCode);
						$('.description').val(response.details.Description);
						$('.Modify_hidden').show();
						$('#ModifyDate').html(response.details.ModifyDate);
						$('#ModifyUser').html(response.details.ModifyUser);
					}	
				},
				error: function(){}
	    	});
		});
    });

	function submitForm(submit_type){
		var oTable = $('#scroll_horizontal_vertical_table').dataTable();
        if(submit_type == 'new'){
			$('#currency_master_group_ids').val('');
			$('input[name="group_name"]').val('');
			$('.description').val('');
			$('#ModifyDate').html('');
			$('#ModifyUser').html('');
			$('.Modify_hidden').hide();   
			clear_currency_master_error();
        } else if(submit_type == 'save'){

        	if($('input[name="group_name"]').val() == '' || $('input[name="description"]').val() == '' ){
                currency_master_error();
                return false;
            }
            clear_currency_master_error();
            var type = '';
            if($('#currency_master_group_ids').val() == ''){
            	type = 'save';
            	id = '';
            } else {
            	type = 'update';
            	id = $('#currency_master_group_ids').val();
            }
			$.ajax({
			    url: "./currency_master_group/insert_update",
			    type: "POST",
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    data : {
			    	type 		: type,
			    	id   		: id,
			    	group_name  : $('input[name="group_name"]').val(),
			    	description : $('.description').val(),
			    },
			    success: function(response){
			        if(response.status == 200){
			            submitForm('new');
			            success_toastr(response.content);
						oTable.fnDraw(false);
			        } else {
			            submitForm('new');
			            danger_toastr(response.content);
			            oTable.fnDraw(false);
			        }
			    },      
			});

        } else if(submit_type == 'delete') {
        	if($('#currency_master_group_ids').val() == ''){
                return false;
            }
            var id = $('#currency_master_group_ids').val();
            clear_currency_master_error();
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete the Currency?',
                buttons: {
                    confirm: function () {
						$.ajax({
						    url: "./currency_master_group/delete",
						    type: "POST",
						    headers: {
						        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						    },
						    data : {
						    	id : id,
						    },
						    success: function(response){
						        if(response.status == 200){
						            submitForm('new');
						            success_toastr(response.content);
						            oTable.fnDraw(false);
						        } else {
						            submitForm('new');
						            danger_toastr(response.content);
						            oTable.fnDraw(false);
						        }
						    },      
						});
                    },
                    cancel: function () {
                    }
                }
            });
        } else if(submit_type == 'esc') {
    		window.location.href = 'dashboard'; 
    	}
	}

	function success_toastr(msg) {
	    toastr.success(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}
	function danger_toastr(msg) {
	    toastr.error(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}

    function clear_currency_master_error() {
        $('input[name="group_name"]').css('border-color','#ced4da');
        $('.description').css('border-color','#ced4da');
        $('.group_name_error').css('display', 'none');
        $('.description_error').css('display', 'none');
    }
    function currency_master_error() {
		$('input[name="group_name"]').css('border-color','#f44336');
		$('.description').css('border-color','#f44336');
		$('.group_name_error').css('display', 'block');
		$('.description_error').css('display', 'block');
    }

	hotkeys('f2', function(event, handler){
	  event.preventDefault();
	    submitForm('new');
	});
	hotkeys('f3', function(event, handler){
	  event.preventDefault();
	    submitForm('save');
	});
	hotkeys('f4', function(event, handler){
	  event.preventDefault();
	    submitForm('delete');
	});
</script>
@endsection