@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<style type="text/css">
    .picker__holder{
        max-width: none;
        width: 335px;
    }
    .addbtn {
        float: left;
    }
    .breadcrumbbtn{
        text-align: right;
    }
</style>
@endsection
@section('main-content')
<div class="breadcrumb addbtn">
    <h1>Currency</h1>
    <ul>
        <!-- <li><a href="{{route('location_stock')}}">Listing</a></li>  -->
    </ul>
</div>

<div class="breadcrumbbtn">
    <a type="button" class="btn btn-primary text-white m-1" href="{{route('location_stock')}}">Listing</a>
</div>
            <div class="separator-breadcrumb border-top"></div>
            <div class="row mb-12 master-main">
                <div class="col-md-12 mb-12">
                    <div class="card text-left">
                        <form class="needs-validation" novalidate name="currency" id="frmcurrency" method="POST" action="{{ route('createcurrency') }}" autocomplete="off">
                        <input type="hidden" name="optype" id="optype" value="">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="currency-basic-tab" data-toggle="tab" href="#currencyBasic" role="tab" aria-controls="currencyBasic" aria-selected="true">Currency</a>
                                </li>
                                <li class="nav-item" style="display: none;">
                                    <a class="nav-link" id="transaction-basic-tab" data-toggle="tab" href="#transactionBasic" role="tab" aria-controls="transactionBasic" aria-selected="false">Transaction</a>
                                </li>
<!--                                 <li class="nav-item">
                                    <a class="nav-link" id="analysis-basic-tab" data-toggle="tab" href="#analysisBasic" role="tab" aria-controls="analysisBasic" aria-selected="false">Analysis</a>
                                </li> -->
                            </ul>
                            <input type="hidden" name="denomarr" id="denomarr" value="">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="currencyBasic" role="tabpanel" aria-labelledby="currency-basic-tab">
                                    <div class="row">
                                     <div class="col-lg-12 col-sm-12 master-main-sec">
                                        <div class="card mb-6">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="code" class="col-sm-2 col-form-label">
                                                                <a onclick="GetCurrencyModal('currency');" data-toggle="modal" data-target="#get_currency_modal_details" href="">Code</a>
                                                                <span class="mandatory">*</span>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <input type="textbox" name="code" class="form-control" autocomplete="off" id="code" placeholder="Enter Currency Code" list="json-datalist"  onkeyup="this.value = this.value.toUpperCase();" required>
                                                                <!-- <datalist id="json-datalist"></datalist> -->
                                                                <div class="invalid-feedback">
                                                                    Please enter code
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">Name <span class="mandatory">*</span></label>
                                                            <div class="col-sm-10">
                                                                <input type="textbox" name="name" class="form-control" id="name" placeholder="Enter Currency Name" autocomplete="off" required>
                                                                <div class="invalid-feedback">
                                                                    Please Enter Currency Name
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                             

                                                <div class="form-group row">
                                                    <div class="col-lg-6 ">
                                                        <div class="row">
                                                            <label for="major" class="col-sm-4 col-form-label">Major <span class="mandatory">*</span></label>
                                                            <div class="col-sm-8">
                                                                <input type="textbox" name="major" class="form-control" id="major" placeholder="Enter Currency Major" autocomplete="off" required>
                                                                <div class="invalid-feedback">
                                                                    Please Enter Currency Major
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="row">
                                                            <label for="minor" class="col-sm-3 col-form-label">Minor <span class="mandatory">*</span></label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="minor" class="form-control" id="minor" placeholder="Enter Currency Minor" autocomplete="off" required>
                                                                <div class="invalid-feedback">
                                                                    Please Enter Currency Minor
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-6 ">
                                                        <div class="row">
                                                            <label for="variance" class="col-sm-4 col-form-label">Variance in (%)</label>
                                                            <div class="col-sm-8">
                                                                <input type="textbox" name="variance" class="form-control allownumericwithdecimal" id="variance" placeholder="Enter Variance in (%)">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="row">
                                                            <label for="avgcost" class="col-sm-3 col-form-label">Avg Cost</label>
                                                            <div class="col-sm-9">
                                                                <input type="textbox" name="avgcost" class="form-control allownumericwithdecimal" id="avgcost" placeholder="Enter Average Cost">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                        <div class="col-lg-6">
                                                            <div class="row">
                                                                <label for="buyrate" class="col-sm-4 col-form-label">Buy Rate</label>
                                                                <div class="col-sm-8">
                                                                    <input type="textbox" name="buyrate" class="form-control allownumericwithdecimal" id="buyrate" placeholder="Enter Buy Rate">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="row">
                                                                <label for="sellrate" class="col-sm-3 col-form-label">Sell Rate</label>
                                                                <div class="col-sm-9">
                                                                    <input type="textbox" name="sellrate" class="form-control allownumericwithdecimal" id="sellrate" placeholder="Enter Sell Rate">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="form-group row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <label for="units" class="col-sm-4 col-form-label">Units From</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="textbox" name="units" class="form-control" id="units" placeholder="Enter Units">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            

                                                            <div class="col-lg-6">
                                                            <div class="row">
                                                                <label for="local" class="col-sm-3 col-form-label">Local</label>
                                                                <div class="col-sm-9 mt-2">
                                                                    <label class="checkbox checkbox-primary">
                                                                    <input type="checkbox" name="local" class="form-check-input checkbox-align" id="local">
                                                                    <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        </div>

                                                        <div class="form-group row" hidden>
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <label for="ttcurrency" class="col-sm-4 col-form-label">Is TT Currency?</label>
                                                                    <div class="col-sm-8 mt-2">
                                                                        <label class="checkbox checkbox-primary">
                                                                            <input type="checkbox" name="ttcurrency" class="form-check-input checkbox-align" id="ttcurrency">
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6" hidden>
                                                                <div class="row">
                                                                    <label for="currencygroup" class="col-sm-3 col-form-label">Currency Group</label>
                                                                    <div class="col-sm-9">
                                                                        <select class="form-control" name="currencygroup" id="currencygroup">
                                                                            <option value="">Select Currency Group</option>
                                                                            @if(count($currencygroupdetails) > 0)
                                                                                @foreach($currencygroupdetails as $group)
                                                                                <option value="{{ $group->GroupCode }}">{{ $group->Description }}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>  

                                                        <div class="form-group row" hidden>
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <label for="ttcustomer" class="col-sm-4 col-form-label">Default TT Customer</label>
                                                                    <div class="col-sm-8 mt-2">
                                                                        <label class="checkbox checkbox-primary">
                                                                            <input type="checkbox" name="ttcustomer" class="form-check-input checkbox-align" id="ttcustomer">
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6" hidden>
                                                                <div class="row">
                                                                    <label for="region" class="col-sm-3 col-form-label">Region</label>
                                                                    <div class="col-sm-9">
                                                                        <select class="form-control" name="region" id="region">
                                                                            <option value="">Select Region</option>
                                                                            @if(count($regions) > 0)
                                                                                @foreach($regions as $region)
                                                                                <option value="{{ $region->id }}">{{ $region->Description }}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>   
                                                        <hr />
                                                    <div class="table-responsive">
                                                        <table id="scroll_horizontal_vertical_table" class="display nowrap table table-striped table-bordered locationcurrencylist_new_table" style="width:100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No.</th>
                                                                    <th>LocationCode</th>
                                                                    <th class="text-right">BuyRate</th>
                                                                    <th class="text-right">SellRate</th>
                                                                    <th class="text-right">Stock</th>
                                                                    <th class="text-right">AvgCost</th>
                                                                    <th class="text-right">Varience</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="locationcurrencylist">
                                                            </tbody>
                                                        </table>    
                                                    </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                </div>
                                <div class="tab-pane fade" id="transactionBasic" role="tabpanel" aria-labelledby="transaction-basic-tab">                                   

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <label for="region" class="col-sm-3 col-form-label">Date</label>
                                                <div class="col-sm-9">
                                                    <input name="doe" class="form-control" autocomplete="off" id="t_date" placeholder="dd-mm-yyyy" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="row">
                                                <label for="region" class="col-sm-4 col-form-label">Location</label>
                                                <select class="form-control col-sm-8" name="clocation" id="clocation">
                                                    <option value="">Select location</option>
                                                    @if(count($alllocations) > 0)
                                                        @foreach($alllocations as $location)
                                                        <option value="{{$location->LocationCode}}" <?php if($location->LocationCode == Auth::user()->Location){echo 'selected'; } ?> >{{ $location->LocationCode}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <label for="region" class="col-sm-4 col-form-label">Opening Balance</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="balance" id="balance" value="" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>&nbsp;</div>
                                    <div class="table-responsive">
                                        <table id="currency_transaction_details" class="display nowrap table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Tran No.</th>
                                                    <th>Date</th>
                                                    <th>Tran Type</th>
                                                    <th>Customer</th>
                                                    <th>Rate</th>
                                                    <th>Buy</th>
                                                    <th>Sell</th>
                                                    <th>L.Amount</th>
                                                    <th>Balance</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>    
                                    </div>
                                </div>

 <!--                                <div class="tab-pane fade" id="analysisBasic" role="tabpanel" aria-labelledby="analysis-basic-tab">
                                    Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore.
                                </div> -->

                            </div>
                            <!-- End of Tab -->
                    		<div class="card mb-5">
                                <div class="card-body">
                                    <div class="row row-xs">
                                        <div class="col-lg-12 text-center">

                                            <button name="sbmtbtn" id="sbmtbtn" class="btn btn-primary m-1" type="submit">Save (F6)</button>
                                            
                                            <button type="button" id="deletebtn" class="btn btn-primary m-1">Delete (F8)</button>
                                            
                                            <button class="btn btn-primary m-1" type="button" id="cancelbtn">Cancel (F5)</button>

                                            <button type="button" class="btn btn-primary m-1" onclick="submitForm('esc');" data-style="expand-left">Close (ESC)</button>

                                            <button class="btn btn-primary ladda-button example-button m-1" data-style="expand-left"><span class="ladda-label">Print</span><span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div></button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                <!-- end of col -->
<!-- 
                <div class="col-md-3 mb-3" style="display: none;">
                    <form class="needs-validation" novalidate id="denomination_form" name="denomination_form" method="POST">
                        {{ csrf_field() }}
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="card-title">Denomination</div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <input type="checkbox" name="settlement" id="settlement" value="">&nbsp;&nbsp;Denomination on Settlement
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="textbox" class="form-control allownumericwithdecimal" name="amount" id="amount" placeholder="0.00" maxlength="15" required>
                                            <div class="invalid-feedback">
                                                Please enter amount
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="hidden" name="rowcnt" id="rowcnt" value="1">
                                            <input type="textbox" class="form-control" name="desc" id="desc" placeholder="dollars" required>
                                            <div class="invalid-feedback">
                                                Please enter Description
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <input type="submit" class="btn btn-primary" name="adddenomination" id="adddenomination" value="ADD">
                                    <input type="button" class="btn btn-primary" name="adddenomination" id="deldenomination" value="DELETE">
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                    <div class="table-responsive">
                        <table id="scroll_horizontal_vertical_table newdenom" class="display nowrap table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Amount</th>
                                    <th>Desc</th>
                                    
                                </tr>
                            </thead>                            
                            <input type="hidden" name="deldenomid" id="deldenomid" value="">
                            <tbody id="denomination_list">
                            </tbody>
                        </table>
                    </div>  
                </div> -->
                <!-- end of col -->
            </div>
            <!-- end of row -->
           
            <!-- end of row -->
@include('modal.currency')            
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
<script src="{{asset('assets/js/ladda.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="{{asset('assets/js/modal.details.js')}}"></script>
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection



@section('bottom-js')
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>

<!-- <script type="text/javascript">  
        toastr.error("dsds",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script>  -->
@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif

<script type="text/javascript">

    $(document).ready(function(){
        /*var dformat = '{{ env('DATE_FORMAT') }}';
        var date = new Date();
        $('#t_date').pickadate({
            format: dformat,
            onStart: function() {
                this.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);
            },
            labelMonthNext: 'Go to the next month',
            labelMonthPrev: 'Go to the previous month',
            labelMonthSelect: 'Pick a month from the dropdown',
            labelYearSelect: 'Pick a year from the dropdown',
            selectMonths: true,
            selectYears: true
        });*/
        var date = new Date();
        var mnt = date.getMonth()+1;
        var cdate = date.getDate()+'-'+mnt+'-'+date.getFullYear();
        $('#t_date').val(cdate);

        $("#t_date").inputmask("dd-mm-yyyy", {
            separator: "-",
            alias: "dd-mm-yyyy",
            placeholder: "dd-mm-yyyy"
        });

        $('#currency_transaction_details').DataTable({
            paging:   false,
            destroy: true,
            processing: true,
            searching : false,
            autoWidth : false,
            info:     false,
        });

        var dataList = $('#json-datalist');
        var autofill = "{{ env('AUTOFILL_LENGHT') }}";

        $(document).on("click", "input[name='currency_id_detail']",function() {
            $('#get_currency_modal_details').modal('toggle');
            var val = $(this).val();
            var valarray = val.split('|');
            $('input[name="code"]').val(valarray[0]);
            $( "#code" ).trigger( "blur" );
        });
        $("#code").blur(function(){
            dataList.html("");
            var cval = $(this).val();
            if(cval!='') {
                var _token = $('input[name="_token"]').val();
                ajaxcurrency(_token,cval);
            }
        });        
             
        $(document).on('click', '.clickcls', function(){ 
            var cval = $(this).html();
            $('#code').val(cval); 
            var _token = $('input[name="_token"]').val();
            ajaxcurrency(_token,cval);
        });

        $(document).on('click', '.denomcls', function(){
            $(this).toggleClass("highlight");
            var denomid = $(this).attr('id');
            $('#deldenomid').val(denomid);
        });

        $(document).on('click', '#deldenomination', function(){
           var denomiddel = $('#deldenomid').val();
           if(denomiddel=='') {
                return false;
           }
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete the Currency?',
                buttons: {
                    Ok: function () {
                        var denomiddel = $('#deldenomid').val();
                        $('#'+denomiddel).remove();
                        var data = [];            
                        $('#denomination_list tr').each(function() {
                            var cols = [];
                            $(this).find('td:eq(1), td:eq(2)').each(function (colIndex, c) {
                                cols.push(c.textContent);
                            });
                        data.push(cols.join('_'));
                        });
                        var final = data.join(',');
                        $('#denomarr').val(final);
                    },
                    cancel: function () {

                    }
                }
            });
        });

        function ajaxcurrency(_token,cval)
        {
            $.ajax({
                  url:"{{ route('getcurrencydetails') }}",
                  method:"POST",
                  data:{currencycode:cval, _token:_token},
                  success:function(cdata){ 
                      
                    var loccurrdata = cdata['loccurrencylist'];
                    var denomccurrdata = cdata['denomcurrencylist'];
                     
                    //alert(loccurrdata.length);

                    if(loccurrdata.length > 0) {
                        var str = '';
                        var c = 1;
                        // console.log(loccurrdata);
                        $.each(loccurrdata, function(index, value){
                            if(value.BuyRate==null) {
                                value.BuyRate = '';
                            }
                            if(value.SellRate==null) {
                                value.SellRate = '';
                            }
                            if(value.Stock==null) {
                                value.Stock = '';
                            }
                            if(value.AvgCost==null) {
                                value.AvgCost = '';
                            }
                            if(value.Varience==null) {
                                value.Varience = '';
                            }
                            //alert(index+'---'+value.AvgCost);
                            str += '<tr><td>'+c+'</td><td>'+value.LocationCode+'</td><td class="text-right">'+value.BuyRate+'</td><td class="text-right">'+value.SellRate+'</td><td class="text-right">'+value.Stock+'</td><td class="text-right">'+value.AvgCost+'</td><td class="text-right">'+value.Varience+'</td></tr>';
                            c++;
                        });
                        $('#locationcurrencylist').html(str);
                    }

                    if(denomccurrdata.length > 0) {
                        
                        var dstr = '';
                        var alstr = '';
                        var d = 1;
                        $.each(denomccurrdata, function(dindex, dvalue){
                            //alert(index+'---'+value.AvgCost);
                            dstr += '<tr id="'+d+'" class="denomcls"><td>'+d+'</td><td class="d_value">'+dvalue.Denomination+'</td><td class="d_value">'+dvalue.Description+'</td></tr>';
                            alstr += dvalue.Denomination+'_'+dvalue.Description+',';
                            d++;
                        });
                        $('#denomination_list').html(dstr);
                        $('#denomarr').val(alstr);
                        var rowCount = (denomccurrdata.length) + 1;
                        $('#rowcnt').val(rowCount);
                    }

                    if(cdata.CurrencyName!=undefined) {
                        $('#optype').val('update');
                    } 
                    if(cdata.CurrencyName==undefined) {
                        $('#optype').val('');
                        $('#scroll_horizontal_vertical_table tbody').html('');
                        $('#denomination_list').html('');
                        /*$('#region').val("");
                        $('#region option[value=""]').attr('selected', true);
                        $('#currencygroup').val("");
                        $('#currencygroup option[value=""]').attr('selected', true);
                        $('#ttcurrency').prop('checked', false);*/
                    }              
                    $('#name').val(cdata.CurrencyName);
                    $('#major').val(cdata.Major);
                    $('#minor').val(cdata.Minor);
                    $('#variance').val(cdata.Varience);
                    $('#avgcost').val(cdata.AvgCost);
                    $('#buyrate').val(cdata.BuyRate);
                    $('#sellrate').val(cdata.SellRate);
                    $('#units').val(cdata.Units);
                    //alert(cdata.LocalCurrency);
                    if(cdata.LocalCurrency=="1") {
                        $('#local').prop('checked', true);
                    }
                    /*if(cdata.TTCurrency=="1") {
                        $('#ttcurrency').prop('checked', true);
                    }
                    $("#currencygroup").val(cdata.CurrencyGroup);
                    if(cdata.TTCustomerUpdate=="1") {
                        $('#ttcustomer').prop('checked', true);
                    }
                    $('#region option[value="'+cdata.Region+'"]').attr('selected','selected');*/
                    $('#currencyList').fadeOut(); 
                  }
            });
            tbl_transaction();
        }
    });
    $('#clocation').change(function(){
        tbl_transaction()        
    });
    $('#t_date').change(function(){
        tbl_transaction()        
    });

    function tbl_transaction() {
        var location =  $('#clocation').val();
        if($('input[name="code"]').val() == '' || location == ''){
            return false;
        } else {
            $('#currency_transaction_details').DataTable({
                paging:   false,
                destroy: true,
                searching : false,
                autoWidth : false,
                info:     false,
                serverSide: true,
                ajax: {
                    url:"{{ route('buysellcurrencydetails') }}",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function (d) {
                        d.cdata = $('input[name="code"]').val();
                        d.cdate = $('#t_date').val();
                        d.location = location
                    },
                },
                columns: [
                    {data: 'TranNo', name: 'TranNo'},
                    {data: 'TranDate', name: 'TranDate'},
                    {data: 'TranType', name: 'TranType'},
                    {data: 'CustName', name: 'CustName'},
                    {data: 'Rate', name: 'Rate', className: "text-right"},
                    {data: 'Tbuy', name: 'Tbuy', className: "text-right"},
                    {data: 'Tsell', name: 'Tsell', className: "text-right"},
                    {data: 'LAmount', name: 'LAmount', className: "text-right"},
                    {data: 'bal', name: 'bal', className: "text-right"},
                ],
            });
        }
    }

    $('#denomination_form').ajaxForm({
        url: "./currency_master/denomination/add",
        type: 'post',
        data: $(this).serialize(),
        beforeSend: function() {
           var amt = $('#amount').val();
           if(amt=='') {
                return false;
           }
        },
        success: function(responseText) {
            var res = responseText.split('---');
            var htmlres = res[0];
            var valres = res[1];
            var indexval = res[2];
            //$('#denomination_list').html('');
            $('#denomination_list').append(htmlres);
            //$('#denomarr').val(valres);
            var denom = $('#denomarr').val();
            var denomval = valres;
            //denomfull = new Array();
            //denomfull.push(denom+","+denomval);
            denom =denom+","+denomval;
            $('#denomarr').val(denom);
            $('#rowcnt').val(indexval);
            $('input[name="amount"]').val('');
            $('input[name="desc"]').val('');
            $("#denomination_form").removeClass("was-validated");
        }
    });

    $(document).ready(function(){
        $('#picker3').pickadate();
         /*$('#scroll_horizontal_vertical_table').DataTable({
            "scrollY": 200,
            "scrollX": true
        });*/

        $(document).on('click', '#cancelbtn', function(){
            location.reload();
        });

        $(document).on('click', '#deletebtn', function(){
            var _token = $('input[name="_token"]').val();
            var code = $('#code').val();
            if(code=="") {
                alert('Enter Currency Code to delete!');
            } else {
                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure to delete the Currency?',
                    buttons: {
                        confirm: function () {
                            $.ajax({
                                url:"{{ route('deletecurrency') }}",
                                method:"POST",
                                data:{currencycode:code, _token:_token},
                                success:function(cdata){
                                    $.alert('Currency Deleted Successfully!');
                                    location.reload();
                                }
                            });                            
                            
                        },
                        cancel: function () {
                            $.alert('Canceled!');
                        }
                    }
                });
            }
        });
    });
    function submitForm(submit_type){
        if(submit_type == 'esc') {
            window.location.href = 'dashboard';
        }      
    }

    hotkeys('f6', function(event, handler){
      event.preventDefault();
      $("#sbmtbtn").trigger( "click" ); 
    });

    hotkeys('escape', function(event, handler){
      event.preventDefault();
      window.location.href="dashboard";
    });

    hotkeys('f8', function(event, handler){
      event.preventDefault();
      $("#deletebtn").trigger( "click" ); 
    });

    hotkeys('f5', function(event, handler){
      event.preventDefault();
      $("#cancelbtn").trigger( "click" ); 
    });
$( function() {
    $( "#t_date").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true
    });
} );

</script>
@endsection