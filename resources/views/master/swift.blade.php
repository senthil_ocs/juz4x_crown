@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<style type="text/css">
table#table_swift > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
div.DTS div.dataTables_scrollBody {
     background: #fff; 
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Swift Master</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			    <li>Create</li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>
		<div class="row mb-12 master-main">
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Listing</div>
                        <div class="table-responsive">
                            <table id="table_swift" class="display nowrap table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Code</th>
                                        <th>Bank</th>
                                        <th>Bank Branch</th>
                                        <th>Country</th>
                                    </tr>
                                </thead>
                                <?php /*<tbody>
                                	@php $i=1;@endphp
                                	@foreach($datas as $val)
                                		<tr>
                                			<td>{{$i}}</td>
                                			<td>{{$val->Code}}</td>
                                			<td>{{$val->Bank}}</td>
                                			<td>{{$val->BankBranch}}</td>
                                			<td>{{$val->Country}}</td>
                                		</tr>
                                		@php $i++;@endphp
                                	@endforeach
                                </tbody>*/ ?>
                            </table>    
                        </div>
					</div>	
				</div>
			</div>	
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Create</div>
						
						<form id="frm_swift" name="frm_swift" class="needs-validation"  novalidate action="#" method="post">
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="Code" class="col-sm-4 col-form-label">Code<span class="mandatory">*</span></label>
							            <div class="col-sm-8">
							            	<input type="hidden" name="swift_id" id="swift_id">
							                <input type="textbox" class="form-control" name="code" id="Code" placeholder="Enter Code" required value="{{ old('code') }}">
		                                    <div class="invalid-feedback code_error">
		                                        Please Enter Swift Code
		                                    </div>
							            </div>	
							        </div>
							    </div>
							</div>

							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="bank" class="col-sm-4 col-form-label">Bank<span class="mandatory">*</span></label>
							            <div class="col-sm-8">
							                <input type="textbox" class="form-control" name="bank" id="Bank" placeholder="Enter Bank Name" required value="{{ old('bank') }}">
		                                    <div class="invalid-feedback code_error">
		                                        Please Enter Bank Name
		                                    </div>
							            </div>	
							        </div>
							    </div>
							</div>

							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="bank_branch" class="col-sm-4 col-form-label">Bank Branch<span class="mandatory">*</span></label>
							            <div class="col-sm-8">
							                <input type="textbox" class="form-control" name="bank_branch" id="bank_branch" placeholder="Enter Bank Branch Name" required value="{{ old('bank_branch') }}">
		                                    <div class="invalid-feedback code_error">
		                                        Please Enter Bank Branch Name
		                                    </div>
							            </div>	
							        </div>
							    </div>
							</div>

							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="country" class="col-sm-4 col-form-label">Country<span class="mandatory">*</span></label>
							            <div class="col-sm-8">
							                <input type="textbox" class="form-control" name="country" id="country" placeholder="Enter Country Name" required value="{{ old('country') }}">
		                                    <div class="invalid-feedback code_error">
		                                        Please Enter Country Name
		                                    </div>
							            </div>	
							        </div>
							    </div>
							</div>


							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyDate</label>
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyDate" class=" col-form-label"></label>
										</div>
									</div>
								</div>	
							</div>

							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyUser</label>	
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyUser" class=" col-form-label"></label>	
										</div>									
									</div>	
								</div>	
							</div>	

	                		<div class="card mb-5">
	                            <div class="card-body">
	                                <div class="row row-xs">
	                                    <div class="col-lg-12 text-center">

	                                        <button class="btn btn-primary m-1" onclick="submitForm('new');" type="button">New (F2)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('save');" type="button" >Save (F3)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('delete');"type="button">Delete (F4)</button>

	                                        <button type="button" class="btn btn-primary m-1" onclick="submitForm('esc');" data-style="expand-left">Exit (Esc)</button>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
                    	</form>
					</div>	
				</div>
			</div>
		</div>	
    @endsection
    
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdn.datatables.net/scroller/2.0.1/js/dataTables.scroller.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
    $(document).ready(function(){
    	var scroll = "{{ env('TABLESCROLL') }}";

		/*$('#table_swift').DataTable({
			paging:   false,
			destroy: true,
			processing: true,
			autoWidth : false,
			info:     false,
			scrollX: true,
			scrollY: scroll,
	        scroller: {
	            loadingIndicator: true
	        },
    	});*/

		$('#table_swift').DataTable({
			dom: 'Pfrtip',			
			paging: false,
			destroy: true,
			processing: true,
			/*serverSide: true,
			autoWidth : false,
			info:     false,*/
			scrollY: scroll,
			scrollX: true,
			ajax: {
				url: "{{route('swift.view')}}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'Code', name: 'Code'},
				{data: 'Bank', name: 'Bank'},
				{data: 'BankBranch', name: 'BankBranch'},
				{data: 'Country', name: 'Country'},
			],
		});

		$(document).on("click", "#table_swift tbody tr",function() {
			$.ajax({
	    		url: "{{route('swift.getdetails')}}",
	    		type: "POST",
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					id : $(this).attr('id'),
				},
				beforeSend: function() {},
				success: function(response){
					if(response.status == 200){
						$('#swift_id').val(response.details.id);
						$('input[name="code"]').val(response.details.Code);
						$('input[name="bank"]').val(response.details.Bank);
						$('input[name="bank_branch"]').val(response.details.BankBranch);
						$('input[name="country"]').val(response.details.Country);
						$('.Modify_hidden').show();
						if(response.details.ModifyDate2 == 0){
							$('#ModifyDate').html(response.details.ModifyDate);
						} else {
							$('#ModifyDate').html('');
						}
						$('#ModifyUser').html(response.details.ModifyUser);
					}	
				},
				error: function(){}
	    	});
		});
    });

    function submitForm(submit_type){
    	var ntable = $('#table_swift').dataTable();
    	if(submit_type == 'new'){
			$('#swift_id').val('');
			$('input[name="code"]').val('');
			$('input[name="bank"]').val('');
			$('input[name="bank_branch"]').val('');
			$('input[name="country"]').val('');
			$('#ModifyDate').html('');
			$('#ModifyUser').html('');
			$('.Modify_hidden').hide();   
			$('#frm_swift').removeClass('was-validated');
    	} else if (submit_type == 'save') {
    		if($('input[name="code"]').val() == '' || $('input[name="bank"]').val() == '' || $('input[name="bank_branch"]').val() == '' || $('input[name="country"]').val() == ''){
    			$('#frm_swift').addClass('was-validated');
    			return false;
    		}
    		var type = '';
    		if($('#swift_id').val() == ''){
            	type = 'save';
            	id = '';
            } else {
            	type = 'update';
            	id = $('#swift_id').val();
            }
			$.ajax({
			    url: "{{route('swift.insert_update')}}",
			    type: "POST",
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    data : {
			    	type  : type,
			    	id    : id,
			    	code  : $('input[name="code"]').val(),
			    	bank  : $('input[name="bank"]').val(),
			    	bank_branch  : $('input[name="bank_branch"]').val(),
			    	country  : $('input[name="country"]').val(),
			    },
			    success: function(response){
			        if(response.status == 200){
			            submitForm('new');
			            success_toastr(response.content);
						ntable.fnDraw(false);
			        } else {
			            if(response.content == 'Swift Code already Exists!'){
			            	danger_toastr(response.content);
			            } else {
				            /*submitForm('new');*/
				            $.each(response.content, function(v, t){
				            	danger_toastr(t);
				            });
				            ntable.fnDraw(false);
				        }
			        }
			    },      
			});
    	} else if (submit_type == 'delete') {
        	if($('#swift_id').val() == ''){
                return false;
            }
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete?',
                buttons: {
                    confirm: function () {
						$.ajax({
						    url: "{{route('swift.delete')}}",
						    type: "POST",
						    headers: {
						        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						    },
						    data : {
						    	id : $('#swift_id').val(),
						    },
						    success: function(response){
						        if(response.status == 200){
						            submitForm('new');
						            success_toastr(response.content);
						            ntable.fnDraw(false);
						        } else {
						            submitForm('new');
						            danger_toastr(response.content);
						            ntable.fnDraw(false);
						        }
						    },      
						});
                    },
                    cancel: function () {
                    }
                }
            });
    	} else if(submit_type == 'esc'){
    		window.location.href = 'dashboard'; 
    	}
    }

	function success_toastr(msg) {
	    toastr.success(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}
	function danger_toastr(msg) {
	    toastr.error(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}

	hotkeys('f2', function(event, handler){
	  event.preventDefault();
	    submitForm('new');
	});
	hotkeys('f3', function(event, handler){
	  event.preventDefault();
	    submitForm('save');
	});
	hotkeys('f4', function(event, handler){
	  event.preventDefault();
	    submitForm('delete');
	});

</script>
@endsection