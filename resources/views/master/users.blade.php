@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
@endsection

@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<style type="text/css">
    .highlight { background-color: #c8ebf7 !important; }
    table#user_table > tbody > tr { cursor: pointer; }
</style>
@endsection
@section('main-content')
   <div class="breadcrumb">
                <h1>Users</h1>
                <ul>
                    <li><a href="">Listing</a></li>
                    <li>Insert</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
            <div class="row mb-12 master-main">
                <div class="col-md-12 mb-12">
                    <div class="card text-left">
                        <form class="needs-validation" novalidate name="users" id="frmusers" method="POST" action="{{ route('createuser') }}" autocomplete="off">
                        <input type="hidden" name="optype" id="optype" value="">
                        <input type="hidden" name="user_id" id="user_id">
                        {{ csrf_field() }}
                        <div class="card-body">
                                    <div class="row">
                                     <div class="col-lg-12 col-sm-12 master-main-sec">
                                        <div class="card mb-6">
                                            <div class="card-body">

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">
                                                            <a onclick="GetUserModal('user');" data-toggle="modal" data-target="#get_user_modal_details" href="">
                                                            User Id </a><span class="mandatory">*</span></label>
                                                            <div class="col-sm-10">
                                                                <input type="textbox" name="userid" class="form-control" id="userid" placeholder="Enter User Id" autocomplete="off" required>
                                                                <div class="invalid-feedback">
                                                                    Please Enter User Id
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">Description <span class="mandatory">*</span></label>
                                                            <div class="col-sm-10">
                                                                <input type="textbox" name="description" class="form-control" id="description" placeholder="Enter Description" autocomplete="off" required>
                                                                <div class="invalid-feedback">
                                                                    Please Enter Description
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">Password <span class="mandatory onlyinsert">*</span></label>
                                                            <div class="col-sm-10">
                                                                <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" autocomplete="off" >
                                                                <div class="invalid-feedback">
                                                                    Please Enter Password
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">Confirm Password <span class="mandatory onlyinsert">*</span></label>
                                                            <div class="col-sm-10">
                                                                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Enter Confirm Password" autocomplete="off" >
                                                                <div class="invalid-feedback">
                                                                    Please Enter Confirm Password
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">User Group Id <span class="mandatory">*</span></label>
                                                            <div class="col-sm-10">
                                                                <select name="profileid" id="profileid" class="form-control" required>
                                                                    <option value="">-Select User Group-</option>
                                                                    @if(count($allprofiles) > 0)
                                                                        @foreach($allprofiles as $profiles)
                                                                        <option value="{{ $profiles-> ProfileId }}" @if(Input::old('description') == $profiles->ProfileId)selected @endif>{{ $profiles-> ProfileId }}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                                <div class="invalid-feedback">
                                                                    Please Enter User Group Id
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">Location <span class="mandatory">*</span></label>
                                                            <div class="col-sm-10">
                                                                <select class="form-control" name="location" id="location" required>
                                                                    <option value="">Select Location</option>
                                                                @if(count($alllocations) > 0)
                                                                    @foreach($alllocations as $location)
                                                                    <option value="{{$location->LocationCode}}">{{ $location->LocationCode}}</option>
                                                                    @endforeach
                                                                @endif
                                                                </select>
                                                                <div class="invalid-feedback">
                                                                    Please Select Location
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">User Type <span class="mandatory">*</span></label>
                                                            <div class="col-sm-10">
                                                                <input type="radio" name="usertype" id="usertype" value="Cashier" checked="checked" required>&nbsp;<label>Cashier</label>
                                                                <input type="radio" name="usertype" id="usertype" value="Dealer" required>&nbsp;
                                                                <label>Dealer</label>  
                                                                <input type="radio" name="usertype" id="usertype" value="Both" required>&nbsp;<label>Both</label>                                                            
                                                                <div class="invalid-feedback">
                                                                    Please Enter User Type
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">Active ?</label>
                                                            <div class="col-sm-10 mt-2">
                                                                <label class="checkbox checkbox-primary">
                                                                    <input type="checkbox" name="active" class="form-check-input checkbox-align" id="active">&nbsp;&nbsp;
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">Administrator ?</label>
                                                            <div class="col-sm-10 mt-2">
                                                                <label class="checkbox checkbox-primary">
                                                                    <input type="checkbox" name="administrator" class="form-check-input checkbox-align" id="administrator">&nbsp;&nbsp;
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="card mb-5">
                                    <div class="card-body">
                                        <div class="row row-xs">
                                            <div class="table-responsive">
                                                <table id="user_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No.</th>
                                                            <th>User Id</th>
                                                            <th>Description</th>
                                                            <th>User Group Id</th>
                                                            <th>Active ?</th>
                                                            <th>Valid Till</th>
                                                            <th>User Type</th>
                                                            <th>Location</th>
                                                        </tr>
                                                        <tbody>
                                                        @php
                                                        if(count($allusers) > 0) {
                                                            $i = 1;
                                                            foreach($allusers as $profile) {
                                                            $activeflag = '<span class="badge badge-warning">No</span>';
                                                            if($profile->Active=='1') {
                                                                $activeflag = '<span class="badge badge-success">Yes</span>';
                                                            }
                                                        @endphp
                                                            <tr id="{{ $profile->username }}" class="trclass">
                                                                <td>{{ $i }}</td>
                                                                <td>{{ $profile->username }}</td>
                                                                <td>{{ $profile->Description }}</td>
                                                                <td>{{ $profile->ProfileId }}</td>
                                                                <td>{!! $activeflag !!}</td>
                                                                <td></td>
                                                                <td>{{ $profile->UserType }}</td>
                                                                <td>{{ $profile->Location }}</td>
                                                            </tr>
                                                        @php
                                                            $i++;
                                                           }
                                                        }
                                                        @endphp
                                                        </tbody>
                                                    </thead>
                                                    <tbody id="locationcurrencylist">
                                                    </tbody>
                                                </table>    
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            <!-- End of Tab -->
                    		<div class="card mb-5">
                                <div class="card-body">
                                    <div class="row row-xs">
                                        <div class="col-lg-12 text-center">

                                            <button name="sbmtbtn" id="sbmtbtn" class="btn btn-primary m-1" type="submit">Add (F6)</button>
                                            <button class="btn btn-primary m-1" type="button" id="updatebtn">Update (F7)</button>
                                            <button type="button" id="deletebtn" class="btn btn-primary m-1">Delete (F8)</button>
                                            
                                            <button class="btn btn-primary m-1" type="button" id="cancelbtn">Cancel (F5)</button>

                                            <button type="button" class="btn btn-primary m-1" onclick="submitForm('esc');" data-style="expand-left">Clear (ESC)</button>

                                            <!-- <button type="button" class="btn btn-primary m-1" data-style="expand-left">Print</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                <!-- end of col -->

                <!-- end of col -->
            </div>
            <!-- end of row -->
           
            <!-- end of row -->
@include('modal.currency')  
@include('modal.user')       
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
<script src="{{asset('assets/js/ladda.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="{{asset('assets/js/modal.details.js')}}"></script>
@endsection



@section('bottom-js')
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif

<script type="text/javascript">
    $(document).ready(function(){
        setTimeout(function(){ 
            $('#cancelbtn').trigger('click');
        }, 1000);
        $('#user_table').DataTable({
            paging    : false,
            destroy   : true,
            processing: true,
            autoWidth : false,
            info      : false,
            stateSave : true,
        });
    });
        $(document).on("click", "input[name='user_id_detail']",function() {
            $('#get_user_modal_details').modal('toggle');
            $('input[name="userid"]').val($(this).val());
            getUserDetails($(this).val());
        });
        $("input[name='userid']").blur(function() {
            var userid = $("input[name='userid']").val();
            getUserDetails(userid);
        });
        $(document).on("click", "#user_table tbody tr",function() {
            $('.trclass').removeClass("highlight");
            $(this).toggleClass("highlight");
            getUserDetails($(this).attr('id'));
            $('#sbmtbtn').prop("disabled",true);
            $('#updatebtn').prop("disabled",false);
            $('.onlyinsert').hide();
            /*$('#userid').attr('readonly','readonly');*/
        });
        $('#updatebtn').on('click', function(){
            $.ajax({
                url:"{{ route('updateuser') }}",
                method:"POST",
                data:$('#frmusers').serialize(),
                success:function(response){
                    if(response.status == 200){
                        success_toastr(response.content);
                        $('#cancelbtn').trigger('click');
                        location.reload();
                    } else {
                        danger_toastr(response.content);
                        if(response.content == 'User Id already exists !'){
                            location.reload();
                        }
                    }
                }
            });
        });
        $('#cancelbtn').on('click', function(){
            $('#userid').val('');
            $('#description').val('');
            $('#password').val('');
            $('#profileid').val('');
            $('#location').val('');
            $('#password_confirmation').val('');
            $('#sbmtbtn').prop("disabled",false);
            $('#updatebtn').prop("disabled",true);
            $('.onlyinsert').show();
            /*$('#userid').removeAttr('readonly');*/
        });

    function getUserDetails(userid){
        $.ajax({
            url: "./user/getdetails",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id : userid,
            },
            beforeSend: function() {},
            success: function(response){
                if(response.status == 200){
                    $('#user_id').val(response.details.id);
                    $('#userid').val(response.details.username);
                    $('#profileid').val(response.details.ProfileId);
                    $('#location').val(response.details.Location);
                    $('#description').val(response.details.Description);
                    $('#password').val(response.details.password);
                    $('#cpassword').val(response.details.cpassword);
                    if(response.details.Active =='1') {
                        $('#active').prop('checked',true);
                    } else{
                        $('#active').prop('checked',false);
                    }
                    /*if(response.details.Admin =='1') {
                        $('#administrator').prop('checked',true);
                    } else{
                        $('#administrator').prop('checked',false);
                    }*/
                    $("#usertype[value='"+response.details.UserType+"']").prop('checked', true);
                    $('#createdby').html(response.details.CreatedBy);
                    $('#sbmtbtn').prop("disabled",true);
                    $('#updatebtn').prop("disabled",false);
                    $('.onlyinsert').hide();
                    /*$('#userid').attr('readonly','readonly');*/
                }   
            },
            error: function(){}
        });
    }



    $(document).on('click', '#deletebtn', function(){
        var _token = $('input[name="_token"]').val();
        var userid = $('#userid').val();
        if(userid=="") {
            alert('Click on User to delete!');
        } else {
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete the User?',
                buttons: {
                    confirm: function () {
                        $.ajax({
                            url:"{{ route('deleteuser') }}",
                            method:"POST",
                            data:{userid:userid, _token:_token},
                            success:function(cdata){
                                // $.alert('User Deleted Successfully!');
                                location.reload();
                            }
                        });                            
                        
                    },
                    cancel: function () {
                        $.alert('Canceled!');
                    }
                }
            });
        }
    });

    function submitForm(submit_type){
        if(submit_type == 'esc'){
            window.location.href = 'dashboard'; 
        }
    }
    function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}

</script>
@endsection