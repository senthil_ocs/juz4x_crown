@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
table#table_orginator > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Management Originator</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			    <li>Create</li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>
		<div class="row mb-12 master-main">
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Listing</div>
                        <div class="table-responsive">
                            <table id="table_orginator" class="display table table-striped table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Name</th>
                                        <th>NRIC/PP</th>
                                        <th>DOB</th>
                                        <th>Nationality</th>
                                        <th>Address</th>
                                        <th>Code</th>
                                    </tr>
                                </thead>
                            </table>    
                        </div>
					</div>	
				</div>
			</div>	
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Create</div>
						
						<form id="frm_orginator" name="frm_orginator" class="needs-validation"  novalidate action="#" method="post">
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="orgbeneficiary" class="col-sm-4 col-form-label text-rights">beneficiary</label>
                                       	<div class="col-sm-8">
                                            <select class="form-control" name="orgbeneficiary" id="orgbeneficiary" >
                                                <option value="">Select beneficiary</option>
                                                @foreach($beneficiary as $value)
                                                <option value="{{trim($value->id,' ')}}">{{trim($value->beneficiary_id,' ')}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
		                                        Please Select Beneficiary
		                                    </div>
                                        </div>
							        </div>
							    </div>
							</div>
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <input type="hidden" name="orginator_id" id="orginator_id">
							            <label for="orginatorName" class="col-sm-4 col-form-label text-rights">Name</label>
                                       	<div class="col-sm-8">
                                            <input class=" form-control" type="textbox" name="orginatorName" autocomplete="off" id="orginatorName" placeholder="Please Enter Name">
                                            <div class="invalid-feedback">
		                                        Please enter Name
		                                    </div>
                                        </div>	
							        </div>
							    </div>
							</div>
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="orginatorNRIC" class="col-sm-4 col-form-label text-rights">NRIC/PP</label>
                                       	<div class="col-sm-8">
                                            <input class=" form-control" type="textbox" name="orginatorNRIC" autocomplete="off" id="orginatorNRIC" placeholder="Please Enter NRIC/PP">
                                            <div class="invalid-feedback">
		                                        Please enter NRICC/PP
		                                    </div>
                                        </div>
							        </div>
							    </div>
							</div>	
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="orginatorDOB" class="col-sm-4 col-form-label text-rights">DOB</label>
                                       	<div class="col-sm-8">
                                            <input type="text" id="orginatorDOB" class="form-control" placeholder="dd-mm-yyyy" name="orginatorDOB">
                                            <div class="invalid-feedback">
		                                        Please enter DOB
		                                    </div>
                                        </div>
							        </div>
							    </div>
							</div>
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="orgNationality" class="col-sm-4 col-form-label text-rights">Nationality</label>
                                       	<div class="col-sm-8">
                                            <select class="form-control" name="orgNationality" id="orgNationality" required>
                                                <option value="">Select Nationality</option>
                                                @foreach($nationality as $value)
                                                <option value="{{trim($value->Code,' ')}}">{{trim($value->Code,' ')}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
		                                        Please Select Nationality
		                                    </div>
                                        </div>
							        </div>
							    </div>
							</div>
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="orginatorAddress" class="col-sm-4 col-form-label text-rights">Address</label>
                                      	<div class="col-sm-8">
                                            <textarea  class=" form-control" type="textbox" name="orginatorAddress" autocomplete="off" id="orginatorAddress" placeholder="Please Enter Address"></textarea>
                                            <div class="invalid-feedback">
		                                        Please enter Address
		                                    </div>
                                        </div>
							        </div>
							    </div>
							</div>
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="orginatorCode" class="col-sm-4 col-form-label text-rights">Code</label>
                                       <div class="col-sm-8">
                                            <input class=" form-control" type="textbox" name="orginatorCode" autocomplete="off" id="orginatorCode" placeholder="Please Enter Code">
                                            <div class="invalid-feedback">
		                                        Please enter Code
		                                    </div>
                                        </div>
							        </div>
							    </div>
							</div>

	                		<div class="card mb-5">
	                            <div class="card-body">
	                                <div class="row row-xs">
	                                    <div class="col-lg-12 text-center">

	                                        <button class="btn btn-primary m-1" onclick="submitForm('new');" type="button">New (F2)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('save');" type="button" >Save (F3)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('delete');"type="button">Delete (F4)</button>

	                                        <button type="button" class="btn btn-primary m-1" onclick="submitForm('esc');" data-style="expand-left">Exit (Esc)</button>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
                    	</form>

					</div>	
				</div>
			</div>
		</div>	
    @endsection
    
@section('page-js')
	<script src="{{asset('assets/js/es5/dashboard.v2.script.js')}}"></script>
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
    <script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
    $(document).ready(function(){
    	$("#orginatorDOB").inputmask("dd-mm-yyyy", {
	        separator: "-",
	        alias: "dd-mm-yyyy",
	        placeholder: "dd-mm-yyyy"
	    });
    	var scroll = '{{ env('TABLESCROLL') }}';
		$('#table_orginator').DataTable({
			paging:   false,
			destroy: true,
			processing: true,
			serverSide: true,
			autoWidth : false,
			info:     false,
			scrollY : scroll,
			scrollX: true,
			ajax: {
				url: "./orginator",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'Name', name: 'Name'},
				{data: 'NRICNo', name: 'NRICNo'},
				{data: 'DOB', name: 'DOB'},
				{data: 'Nationality', name: 'Nationality'},
				{data: 'Address', name: 'Address'},
				{data: 'Code', name: 'Code'},
			],
		});

		$(document).on("click", "#table_orginator tbody tr",function() {
			
			$.ajax({
	    		url: "./orginator/getdetails",
	    		type: "POST",
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					id : $(this).attr('id'),
				},
				beforeSend: function() {},
				success: function(response){
					if(response.status == 200){
						$('#orginator_id').val(response.details.id);						
						$('#orgbeneficiary').val(response.details.Beneficiary_id);
						$('#orginatorName').val(response.details.Name);
						$('#orginatorNRIC').val(response.details.NRICNo);
						$('#orginatorDOB').val(response.details.DOB);
						$('#orgNationality').val(response.details.Nationality);
						$('#orginatorAddress').val(response.details.Address);
						$('#orginatorCode').val(response.details.Code);
					}	
				},
				error: function(){}
	    	});
		});
    });

    function submitForm(submit_type){
    	var orginator_table = $('#table_orginator').dataTable();
    	if(submit_type == 'new'){

			$('#orginator_id').val('');
			$('#orgbeneficiary').val('');
			$('#orginatorName').val('');
			$('#orginatorNRIC').val('');
			$('#orginatorDOB').val('');
			$('#orgNationality').val('');
			$('#orginatorAddress').val('');
			$('#orginatorCode').val('');
			$('#frm_orginator').removeClass('was-validated');
			return false;  
			
    	} else if (submit_type == 'save') {

    		var orgbeneficiary = $('#orgbeneficiary').val(); 
    		var orginatorName = $('#orginatorName').val(); 
    		var orginatorNRIC = $('#orginatorNRIC').val(); 
    		var orginatorDOB = $('#orginatorDOB').val(); 
    		var orgNationality = $('#orgNationality').val(); 
    		var orginatorAddress = $('#orginatorAddress').val(); 
    		var orginatorCode = $('#orginatorCode').val(); 
    		var orginator_id = $('#orginator_id').val();
    		if(orgbeneficiary == '' ||  orginatorName == '' ||  orginatorNRIC == '' || orginatorDOB == '' || orgNationality == '' ||  orginatorAddress == '' || orginatorCode == ''){
    			$('#frm_orginator').addClass('was-validated');
    			return false;
    		}
    		var type = '';
    		if(orginator_id == ''){
            	type = 'save';
            	id = '';
            } else {
            	type = 'update';
            	id = orginator_id;
            }
			$.ajax({
			    url: "./orginator/insert_update",
			    type: "POST",
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    data : {
			    	type 		: type,
			    	id   		: id,
			    	beneId 		: orgbeneficiary,
			    	name   		: orginatorName,
			    	nric   		: orginatorNRIC,
			    	dob   		: orginatorDOB,
			    	nationality : orgNationality,
			    	address  	: orginatorAddress,
			    	code   		: orginatorCode,
			    },
			    success: function(response){
			        if(response.status == 200){
			            submitForm('new');
			            success_toastr(response.content);
						orginator_table.fnDraw(false);
			        } else {
			            submitForm('new');
			            $.each(response.content, function(v, t){
			            	danger_toastr(t);
			            });
			            orginator_table.fnDraw(false);
			        }
			    },      
			});
    	} else if (submit_type == 'delete') {
            var id = $('#orginator_id').val();
        	if(id == ''){
                return false;
            }
           
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete the Orginator?',
                buttons: {
                    confirm: function () {
						$.ajax({
						    url: "./orginator/delete",
						    type: "POST",
						    headers: {
						        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						    },
						    data : {
						    	id : id,
						    },
						    success: function(response){
						        if(response.status == 200){
						            submitForm('new');
						            success_toastr(response.content);
						            orginator_table.fnDraw(false);
						        } else {
						            submitForm('new');
						            danger_toastr(response.content);
						            orginator_table.fnDraw(false);
						        }
						    },      
						});
                    },
                    cancel: function () {}
                }
            });
    	} else if(submit_type == 'esc'){
    		window.location.href = 'dashboard'; 
    	}
    }

	function success_toastr(msg) {
	    toastr.success(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}

	function danger_toastr(msg) {
	    toastr.error(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}

	hotkeys('f2', function(event, handler){
	  event.preventDefault();
	    submitForm('new');
	});
	hotkeys('f3', function(event, handler){
	  event.preventDefault();
	    submitForm('save');
	});
	hotkeys('f4', function(event, handler){
	  event.preventDefault();
	    submitForm('delete');
	});
var dateToday = new Date();
$( function() {
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 15);
	$( "#orginatorDOB").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
        yearRange: start.getFullYear() + ':' + end.getFullYear(),
        maxDate: dateToday,
	});
});
</script>
@endsection