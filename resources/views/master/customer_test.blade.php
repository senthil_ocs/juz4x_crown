@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
@endsection

@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection


@section('main-content')
   <div class="breadcrumb">
        <h1>Customers</h1>
        <ul>
            <li>Insert</li>
        </ul>
    </div>
    <div class="separator-breadcrumb border-top"></div>


    <div class="row mb-12 master-main">
        <div class="col-md-12 mb-12">
            <div class="card text-left">

                <div class="card-body">

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="general-basic-tab" data-toggle="tab" href="#generalBasic" role="tab" aria-controls="generalBasic" aria-selected="true">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contacts-basic-tab" data-toggle="tab" href="#contactsBasic" role="tab" aria-controls="contactsBasic" aria-selected="false">Contacts</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="other-basic-tab" data-toggle="tab" href="#otherBasic" role="tab" aria-controls="otherBasic" aria-selected="false">Other Information</a>
                        </li>
                    </ul>

                    <!-- Start of Tab -->
                    <div class="tab-content" id="myTabContent">

                    <!-- Start of General Tab -->
                        <div class="tab-pane fade show active" id="generalBasic" role="tabpanel" aria-labelledby="general-basic-tab">
                            <div class="card-title">General</div>
                            <div class="card mb-5">
                                <div class="card-body">
                                    <div class="row row-xs">
                                        <div class="col-md-auto">
                                            <label class="radio radio-primary">
                                                <input type="radio" name="radio" value="1" formcontrolname="radio">
                                                <span>Trading Company</span>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-auto">
                                            <label class="radio radio-primary">
                                                <input type="radio" name="radio" value="2" formcontrolname="radio">
                                                <span>Individual</span>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-auto">
                                            <label class="radio radio-primary">
                                                <input type="radio" name="radio" value="3" formcontrolname="radio">
                                                <span>Forex Company</span>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-auto">
                                            <label class="radio radio-primary">
                                                <input type="radio" name="radio" value="4" formcontrolname="radio">
                                                <span>Bank</span>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-auto">
                                            <button class="btn btn-primary btn-block">View Transaction</button>
                                        </div>
                                        <div class="col-md-auto">
                                            <button class="btn btn-primary btn-block">View Transaction</button>
                                        </div>
                                        <div class="col-md-auto">
                                            <button class="btn btn-primary btn-block">View Buy and Sell Total</button>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="row border-top">
                                                <div class="card-body">
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-lg-1 col-form-label">Code</label>
                                                        <div class="col-lg-2">
                                                            <input type="textbox" class="form-control" name="code"  placeholder="Code">
                                                        </div>
                                                        <div class="col-lg-2"> 
                                                            <label class="col-sm-2 col-lg-6 col-form-label master-main-checkbox">Active</label>
                                                            <input class="form-check-input" type="checkbox" name="codeactive" id="codeactive">
                                                        </div>
                                                        <label class="col-sm-3 col-lg-1 col-form-label">Type</label>
                                                        <div class="col-lg-2">
                                                            <select class="form-control">
                                                                <option>Option 1</option>
                                                                <option>Option 2</option>
                                                                <option>Option 3</option>
                                                            </select>
                                                        </div>
                                                        <label class="col-sm-4 col-lg-2 col-form-label">Mas Customer Type</label>
                                                        <div class="col-lg-2">
                                                            <select class="form-control">
                                                                <option>Option 1</option>
                                                                <option>Option 2</option>
                                                                <option>Option 3</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-lg-1 col-form-label">Balance</label>
                                                        <div class="col-lg-2">
                                                            <input type="textbox" class="form-control" placeholder="Balance">
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <label class="col-sm-2 col-lg-6 col-form-label master-main-checkbox">High Risk</label>
                                                            <input class="form-check-input" type="checkbox" id="gridCheck1">
                                                        </div>
                                                        <label class="col-sm-3 col-lg-1 col-form-label">Deal balance</label>
                                                        <div class="col-lg-2">
                                                            <input type="textbox" class="form-control" placeholder="Deal balance">
                                                        </div>
                                                        <label class="col-sm-3 col-lg-2 col-form-label">Date</label>
                                                        <div class="col-lg-2">
                                                            <input type="textbox" class="form-control" placeholder="Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12 master-main-sec">
                                    <div class="card mb-6">
                                        <div class="card-body">
                                            <div class="card-title">Company</div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_name" class="col-sm-2 col-form-label">Name</label>
                                                        <div class="col-sm-10">
                                                             <input type="textbox" name="company_name" class="form-control" id="company_name" placeholder="Enter Company name">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_regn_no" class="col-sm-4 col-form-label">Regn No</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" class="form-control" name="company_regn_no" id="company_regn_no" placeholder="Enter Regn No(ROC No)">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_type" name="company_regn_no" id="company_regn_no" class="col-sm-4 col-form-label">Type</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control">
                                                                <option>Option 1</option>
                                                                <option>Option 2</option>
                                                                <option>Option 3</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6 ">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Date</label>
                                                        <div class="col-sm-8">
                                                            <div class="input-group">
                                                                <input id="picker3" class="form-control" placeholder="yyyy-mm-dd" name="dp" >
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-secondary"  type="button">
                                                                        <i class="icon-regular i-Calendar-4"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_place" class="col-sm-4 col-form-label">Place</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" class="form-control" name="company_place" id="company_place" placeholder="Enter Place">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_license" class="col-sm-4 col-form-label">License</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" class="form-control" name="company_license" id="company_license" placeholder="Enter MC/Trading License">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_turn_over" class="col-sm-4 col-form-label">Turn Over</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" class="form-control" name="company_turn_over" id="company_turn_over" placeholder="Enter Turn Over">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_valid_from" class="col-sm-4 col-form-label">Valid From</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" name="company_valid_from" class="form-control" id="company_valid_from" placeholder="Enter Valid From">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_valid_till" class="col-sm-4 col-form-label">Valid Till</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" name="company_valid_till" class="form-control" id="company_valid_till" placeholder="Enter Valid Till">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_issuing_authority" class="col-sm-4 col-form-label">Issuing Authority</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" name="company_issuing_authority" class="form-control" id="company_issuing_authority" placeholder="Enter Issuing Authority">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="company_overseas" name="company_overseas">
                                                            <span style="padding-left: 20px;">Overseas</span>
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_postal" class="col-sm-2 col-form-label">Postal Code</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" name="company_postal"  class="form-control" id="company_postal" placeholder="Enter Postal Code">
                                                        </div>
                                                    </div>
                                                </div>                                                           
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_address" class="col-sm-2 col-form-label">Address</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" name="company_address" class="form-control" id="company_address" placeholder="Enter Address">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_address2" class="col-sm-2 col-form-label"></label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" name="company_address2"  class="form-control" id="company_address2" placeholder="Enter Address">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_country" class="col-sm-4 col-form-label">Country</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" name="company_country" class="form-control" id="company_country" placeholder="Enter Country">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_phone1" class="col-sm-3 col-form-label">Phone1</label>
                                                        <div class="col-sm-9">
                                                            <input type="textbox" name="company_phone1" class="form-control" id="company_phone1" placeholder="Enter Phone Number1">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_phone2" class="col-sm-4 col-form-label">Phone2</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" name="company_phone2" class="form-control" id="company_phone2" placeholder="Enter Phone Number2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_fax" class="col-sm-3 col-form-label">Fax</label>
                                                        <div class="col-sm-9">
                                                            <input type="textbox" name="company_fax" class="form-control" id="company_fax" placeholder="Enter Fax">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_email" class="col-sm-2 col-form-label">Email</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" name="company_email" class="form-control" id="company_email" placeholder="Enter Email">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_remarks" class="col-sm-2 col-form-label">Remarks</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" name="company_remarks" class="form-control" id="company_remarks" placeholder="Enter Remarks">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_check_list" class="col-sm-2 col-form-label">Check List</label>
                                                        <div class="col-sm-10 checklist-bg">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label class="checkbox checkbox-primary">
                                                                        <input type="checkbox" id="company_check_list1" name="company_check_list1">
                                                                        <span style="padding-left: 20px;">ACRA Cop</span>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label class="checkbox checkbox-primary">
                                                                        <input type="checkbox" id="company_check_list2" name="company_check_list2">
                                                                        <span style="padding-left: 20px;">IC Copy of persons listed above</span>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label class="checkbox checkbox-primary">
                                                                        <input type="checkbox" id="company_check_list3" name="company_check_list3">
                                                                        <span style="padding-left: 20px;">ACRA Copy</span>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-sm-12 master-main-sec">
                                    <div class="card mb-6">
                                        <div class="card-body">
                                            <div class="card-title">Company Representative</div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_contact" class="col-sm-2 col-form-label">Contact</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" name="company_contact" class="form-control" id="company_contact" placeholder="Enter Primary contact name">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_nric_no" class="col-sm-4 col-form-label">NRIC No</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" name="company_nric_no"  class="form-control" id="company_nric_no" placeholder="Enter Regn No(ROC No)">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_identifiertype" class="col-sm-3 col-form-label">IdentifierType</label>
                                                        <div class="col-sm-9">
                                                           <select class="form-control">
 <!--  -->
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Nationality</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Nationality">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Postal Code</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Postal Code">
                                                        </div>
                                                    </div>
                                                </div>
                                               <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8">Copy Address</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Address</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Address">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label"></label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Address">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Country</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Country">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Phone Number</label>
                                                        <div class="col-sm-8">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Phone Number">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-3 col-form-label">Fax</label>
                                                        <div class="col-sm-9">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Fax">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Email">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">DOB</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Position</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Position">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Employement</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Employement Details">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Yearly Income</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Yearly Income">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Source of Income</label>
                                                        <div class="col-sm-10">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="Enter Source of Income">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <!-- End of General Tab -->

                        <!-- Start of Contacts Tab -->
                        <div class="tab-pane fade" id="contactsBasic" role="tabpanel" aria-labelledby="contacts-basic-tab"> 
                            <div class="card-title">Contacts</div>
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="owner-basic-tab" data-toggle="tab" href="#ownerBasic" role="tab" aria-controls="ownerBasic" aria-selected="true">Owners</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="dealer-basic-tab" data-toggle="tab" href="#dealerBasic" role="tab" aria-controls="dealerBasic" aria-selected="false">Dealers</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="runner-basic-tab" data-toggle="tab" href="#runnerBasic" role="tab" aria-controls="runnerBasic" aria-selected="false">Runners</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="ownerBasic" role="tabpanel" aria-labelledby="owner-basic-tab">
                                        <div class="card mb-12">
                                            <div class="card-body dffgdfg">
                                                <div class="row">
                                                    <div class="col-lg-12 col-sm-12 master-main-sec">
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table id="owner_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No.</th>
                                                                            <th>Name as in NRIC/Passport</th>
                                                                            <th>NRIC /EP/Passport</th>
                                                                            <th>Nationality</th>
                                                                            <th>DOB</th>
                                                                            <th>Position</th>
                                                                            <th>Signature</th>
                                                                            <th>Active</th>
                                                                            <th>Remarks</th>
                                                                            <th>Date</th>
                                                                        </tr>
                                                                    </thead>    
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="dealerBasic" role="tabpanel" aria-labelledby="dealer-basic-tab">
                                        <div class="card mb-12">
                                            <div class="card-body dffgdfg">
                                                <div class="row">
                                                    <div class="col-lg-12 col-sm-12 master-main-sec">
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table id="owner_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No.</th>
                                                                            <th>Name as in NRIC/Passport</th>
                                                                            <th>NRIC /EP/Passport</th>
                                                                            <th>Nationality</th>
                                                                            <th>DOB</th>
                                                                            <th>Position</th>
                                                                            <th>Signature</th>
                                                                            <th>Active</th>
                                                                            <th>Remarks</th>
                                                                            <th>Date</th>
                                                                        </tr>
                                                                    </thead>    
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="tab-pane fade" id="runnerBasic" role="tabpanel" aria-labelledby="runner-basic-tab">
                                        <div class="card mb-12">
                                            <div class="card-body dffgdfg">
                                                <div class="row">
                                                    <div class="col-lg-12 col-sm-12 master-main-sec">
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table id="owner_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No.</th>
                                                                            <th>Name as in NRIC/Passport</th>
                                                                            <th>NRIC /EP/Passport</th>
                                                                            <th>Nationality</th>
                                                                            <th>DOB</th>
                                                                            <th>Position</th>
                                                                            <th>Signature</th>
                                                                            <th>Active</th>
                                                                            <th>Remarks</th>
                                                                            <th>Date</th>
                                                                        </tr>
                                                                    </thead>    
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of Contacts Tab -->

                         <!-- Start of Others Tab -->
                        <div class="tab-pane fade" id="otherBasic" role="tabpanel" aria-labelledby="other-basic-tab"> 
                            <div class="card-title">Other Information</div>
                            <div class="row">                                        
                                <div class="col-lg-12 col-sm-12 master-main-sec">
                                    <div class="card mb-4">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="sourcefund" class="col-sm-4 col-form-label">Company Source Of Fund</label>
                                                        <div class="col-sm-8">
                                                            <div class="checkboxcontainer">
                                                                <input type="checkbox" /> Bank Credit Line <br />
                                                                <input type="checkbox" /> Directors/Share Holders/Sole Proprietor Investments <br />
                                                                <input type="checkbox" /> Others <br />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox" id="cusinfo">&nbsp;
                                                                Pending Customer Info
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Top 3 Foreign Currencies</label>
                                                        <div class="col-sm-3">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="textbox" class="form-control" id="inputEmail3" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                               <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="sourcefund" class="col-sm-4 col-form-label">Pending Customer Remarks</label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control description" id="inputEmail3" name="description" placeholder="Pending Customer Remarks" required=""></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="sourcefund" class="col-sm-4 col-form-label">Dealings with Local/Foreign Government Agencies</label>
                                                        <div class="col-sm-8">
                                                            <div class="checkboxcontainer">
                                                                <input type="checkbox" /> Yes <br />
                                                                <input type="checkbox" /> No 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="form-check">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="sourcefund" class="col-sm-4 col-form-label">Type of Customers</label>
                                                        <div class="col-sm-8">
                                                            <div class="checkboxcontainer">
                                                                <input type="checkbox" /> Retail/Walkin <br />
                                                                <input type="checkbox" /> Local Moneychangers <br />
                                                                <input type="checkbox" /> Local Corporate<br />
                                                                <input type="checkbox" /> Local Bank <br />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="form-check">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="email" class="col-sm-4 col-form-label">Email Address</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="email" id="email" class="form-control" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="form-check">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="website" class="col-sm-4 col-form-label">Website</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="website" id="website" class="form-control" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="form-check">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="totalemployees" class="col-sm-4 col-form-label">Total No Of Employees</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="totalemployees" id="totalemployees" class="form-control" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="form-check">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12" style="margin-top: 27px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                         <!-- End of Others Tab -->

                    </div>    
                    <!-- End of Tab -->

            		<div class="card mb-5">
                        <div class="card-body">
                            <div class="row row-xs">
                                <div class="col-lg-12 text-center">
                                    <button class="btn  btn-primary m-1" type="button">Save (F6)</button>
                                    <button class="btn  btn-primary m-1" type="button">Delete (F8)</button>
                                    <button class="btn  btn-primary m-1" type="button">Cancel (F5)</button>
                                    <button class="btn  btn-primary m-1" type="button">Close (ESC)</button>
                                    <button class="btn  btn-primary m-1" type="button">Print</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection


@section('page-js')
    <script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
    <script src="{{asset('assets/js/ladda.script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
@endsection



@section('bottom-js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#picker3').pickadate();
        });
        $('#owner_table,#dealer_table,#runner_table').DataTable({
            "paging": false,
            "bInfo": false
        });
    </script>
@endsection