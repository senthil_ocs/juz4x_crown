@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
table#table_sourceofincome > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Customer List</h1>
			<ul>
			    <li>Listing</li>
			    <li><a href="{{route('customer_master')}}">Create</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>
		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-1">
					<div class="card-body">
						<div class="col-lg-12 mb-12">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('customerExport') }}" method="POST" id="">
					            		@csrf
									<div class="row">
										<div class="col-lg-6 col-sm-8 mb-1">
											<div class="row">
												<label for="voucher_no" class="col-sm-8  col-form-label">Cust Code/Name/Comp, Name/BenName/AccNo</label>
												<div class="col-sm-4 text-left">
													<input type="text" class="form-control" placeholder="" name="data" id="data">
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-sm-4 mb-1">
											<div class="row">
												<select class="form-control" name="CustomerType" id="CustomerType">
													<option value="" >Select CustomerType</option>
													<?php if(count($general_type) > 0) { 
														foreach ($general_type as $type) {
													?>
													<option value="<?php echo $type; ?>"><?php echo $type; ?></option>
													<?php } } ?>


												</select>
											</div>
										</div>
										<div class="col-lg-3 col-sm-4 mb-1 mt-2">
											<div class="row">
												<div class="col-lg-12">
													<label class="checkbox checkbox-primary">
														<input class="form-check-input" type="checkbox" name="codeactive" id="codeactive" value="active" checked>
														<span class="ml-3">Active</span>
														<span class="checkmark"></span>
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-7 col-sm-12"></div>
										<div class="col-lg-5 col-sm-12 text-right">
 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Search</button>
 											@if (env('ENABLE_DD_TT')=='1')
												<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('beneficiary');">Search Beneficiary</button>
        									@endif
											<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>

						<div class="card-title">Customer Listing</div>
                        <div class="table-responsive">
                            <table id="table_customer" class="display nowrap table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>CustomerCode</th>
                                        <th>Customer Name</th>
                                        <th>CustomerType</th>
                                        <th>DOB</th>
                                        <th>Nationality</th>
                                        <th>Phone</th>
                                        <th class="text-right">Current Balance</th>
                                        <th>Address1</th>
                                        <th>Address2</th>
                                        <th class="text-right">Balance</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>    
                        </div>
                        <br>
                        <div class="card-title">Beneficiary Listing</div>
                        <div class="table-responsive">
                            <table id="table_beneficiary" class="display nowrap table table-striped table-bordered" style="width:100%">
                            	<input type="hidden" name="ccode">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Bank</th>
                                        <th>BankAccNo</th>
                                        <th>BranchName</th>
                                        <th>Address1</th>
                                        <th>Address2</th>
                                    </tr>
                                </thead>
                            </table>    
                        </div>

					</div>	
				</div>
			</div>	
		</div>	
	@include('modal.addbeneficiary')
	@include('modal.addcontact')
    @endsection
    
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
	<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif

<script type="text/javascript">
    $(document).ready(function(){

	    $("#contactdob, #doe").inputmask("dd-mm-yyyy", {
	        separator: "-",
	        alias: "dd-mm-yyyy",
	        placeholder: "dd-mm-yyyy"
	    });
    	var code = '';
    	var active = '';
    	tablecustomer(code,active) 
		$('#table_beneficiary').DataTable({searching:false,});
		$(document).on("click", "#table_customer tbody tr",function() {
			var code = '';
			var active = '';
			var ccode = $(this).attr('id');
			$('input[name="ccode"]').val(ccode);
			tablebeneficiary(ccode,code,active) 
		});
		$('input[name="codeactive"]').click(function(){
		    if($(this).prop("checked") == true){
		       $('input[name="codeactive"]').val('active');
		    }
		    else if($(this).prop("checked") == false){
		        $('input[name="codeactive"]').val('inactive');
		    }
		});

		$(document).on("click", "#add_contactform",function() 
		{	var contactcount = $('#contactcount').val();
			var contacttype = $("#contacttype").val();
			var nriccusname = $('#cusname').val();
			var nricno = $('#nricno').val();
		    var contactnationality = $("#contactnationality").val();
		    var contactdob = $("#contactdob").val();
		    var contactposition = $("#contactposition").val();
		    var contactremarks = $("#contactremarks").val();
		    if($('#contactactive').prop("checked") == true){
		    	var contactactive = 'Yes';
		    } else {
		    	var contactactive = 'No';
		    }
		    var doe = $("#doe").val();
	        if(contactcount!=undefined) {
	            var cindex = parseInt(contactcount) + 1;
	            $('#contactcount').val(cindex);
	        } else {
	            var cindex = c;
	        }
	    
	        if(nriccusname == '' || nricno == '' || contactnationality == '' || contactdob == '' || contactposition == '' || contactremarks == ''){
	            errorcustomer();
	            return false;
	        }
	        $.ajax({
                url: "./addContact",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : $('#addcontactform').serialize(),
                success: function(response){
                    if(response.status == 200){
                        submitForm('cancel')
                        success_toastr(response.content);
				        $('#contacttype').val('Select Type');
				        $('input[name="cusname"]').val('');
				        $('input[name="nricno"]').val('');
				        $('input[name="contactnationality"]').val('');
				        $('input[name="contactdob"]').val('');
				        $('#contactposition').val('');
				        $('input[name="contactremarks"]').val('');
				        $('input[name="doe"]').val('');
	        			$('#add_cust_contact').modal('toggle');
                    } else {
                        danger_toastr(response.content);
                    }
                },      
            });
	    });

		$(document).on("click", ".addContact",function() {
	    	var edit_id = $(this).attr('data-ref');
	    	$('#code').val(edit_id);
	        $('#add_cust_contact').modal('toggle');
	         $.ajax({
                url:"{{ route('getcontact') }}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    code : edit_id,
                },
                success: function(response){
                    if(response.status == 200){
				        var contactdata = response.cuscontactdata;
						$("#contact_table tbody").html(contactdata);
						fillters('owner');
                    } else {
						$("#contact_table tbody").html('');
                    }
                },      
            });
	    });

	    $(document).on("click", ".addBeneficiary",function() {
	    	var custCode = $(this).attr('data-custcode');
	    	var nric_no = $(this).attr('data-ref');
	    	$('#custcode').val(custCode);
	    	$('#addnric_no').val(nric_no);
	    	$('#CustNRICNO').val(nric_no);
	    	$.ajax({
                url:"{{ route('getBeneficiary') }}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    custCode : custCode,
                    nric_no  : nric_no
                },
                success: function(response){
					$("#beneficiary_id").val(response.beneId);
                    if(response.status == 200){
				        var benificiery = response.benificiery;
						$("#beneficiary_table tbody").html(benificiery);
                    } else {
						$("#beneficiary_table tbody").html('');
                    }
                },      
            });
	        $('#add_cust_bene').modal('toggle');
		});

		$(document).on('click',"#add_beneficiaryform",function(){
	        var BeneName = $("#BeneName").val();
	        var BeneMobileNo = $("#BeneMobileNo").val();
	        if($('#beneficiaryactive'). prop("checked") == true){
	            var beneficiaryactive = 'Active';
	        } else {
	            var beneficiaryactive = 'Inactive';
	        }
	        var doe = $("#doe").val();
	        if(BeneName == '' || BeneMobileNo == '' ){
	            errorbeneficiary();
	            return false;
	        }
	        $.ajax({
                url: "./addCustBeneficiary",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : $('#addbeneform').serialize(),
                success: function(response){
                    if(response.status == 200){
                        success_toastr(response.content);
                        $('#beneficiary_id').val('');
				        $("#custcode").val('');
				        $('#CurrencyCode').val('');
				        $('#CustNRICNO').val('');
				        $("#BeneName").val('');
				        $("#BeneBankName").val('');
				        $("#BeneBankAccNo").val('');
				        $("#BeneAddress1").val('');
				        $("#BeneAddress2").val('');
				        $("#BeneCountry").val('');
				        $("#BeneMobileNo").val('');
				        $("#BeneBankBranch").val('');
				        $("#SwiftCode").val('');
				        $("#purpose").val('');
	        			$('#add_cust_bene').modal('toggle');
                    } else {
                        danger_toastr(response.content);
                    }
                },      
            });
	    });

	    setTimeout(function(){
	        $('#owners').trigger('click');
	    },100);
	    var c=0;
		/*$(document).on('click',"#addcontact",function(){
			c++;
	        var contactcount = $('#contactcount').val();
			var contacttype = $("#contacttype").val();
			var nriccusname = $('#cusname').val();
			var nricno = $('#nricno').val();
		    var contactnationality = $("#contactnationality").val();
		    var contactdob = $("#contactdob").val();
		    var contactposition = $("#contactposition").val();
		    var contactremarks = $("#contactremarks").val();
		    if($('#contactactive'). prop("checked") == true){
		    	var contactactive = 'Yes';
		    } else {
		    	var contactactive = 'No';
		    }
		    var doe = $("#doe").val();
	        if(contactcount!=undefined) {
	            var cindex = parseInt(contactcount) + 1;
	            $('#contactcount').val(cindex);
	        } else {
	            var cindex = c;
	        }
	    
	        if(nriccusname == '' || nricno == '' || contactnationality == '' || contactdob == '' || contactposition == '' || contactremarks == ''){
	            errorcustomer();
	            return false;
	        }
		    var contactdata = "<tr id='cid"+cindex+"' class='contactcls'><td>"+cindex+"</td><td><input type='hidden' value='"+contacttype+"' name='ctype[]'><input type='hidden' value='"+nriccusname+"' name='nriccustomername[]'>"+nriccusname+"</td><td><input type='hidden' value='"+nricno+"' name='nricnumber[]'>" + nricno + "</td><td><input type='hidden' value='"+contactnationality+"' name='cnationality[]'>" + contactnationality + "</td><td><input type='hidden' value='"+contactdob+"' name='cdob[]'>" + contactdob + "</td><td><input type='hidden' value='"+contactposition+"' name='cposition[]'>" + contactposition + "</td><td></td><td><input type='hidden' value='"+contactactive+"' name='cactive[]'>" + contactactive + "</td><td><input type='hidden' value='"+contactremarks+"' name='cremarks[]'>" + contactremarks + "</td><td><input type='hidden' value='"+doe+"' name='cdoe[]'>" + doe + "</td></tr>";
	        //$("#contact_table tbody").html("");
		    $("#contact_table tbody").append(contactdata);
	        $('#contacttype').val('Select Type');
	        $('input[name="cusname"]').val('');
	        $('input[name="nricno"]').val('');
	        $('input[name="contactnationality"]').val('');
	        $('input[name="contactdob"]').val('');
	        $('#contactposition').val('');
	        $('input[name="contactremarks"]').val('');
	        $('input[name="doe"]').val('');
		});*/
    });

	function submitForm(type) {
		if(type == 'submit'){
			var type = $('#CustomerType').find(":selected").val();
			var code = $('#data').val()+'_'+type;
			var active =  $('input[name="codeactive"]').val();
			tablecustomer(code,active)
		} else if(type == 'beneficiary'){
			var code = $('#data').val();
			var ccode = $('input[name="ccode"]').val();
			var active =  $('input[name="codeactive"]').val();
			tablebeneficiary(ccode,code,active)
		} else if(type == 'csv'){
			var code = $('#data').val();
			var active =  $('input[name="codeactive"]').val();
			tablecustomer_csv(code,active)			
		}
	}

	function tablecustomer(code,active) 
	{	
		var codevals = code.split('_');
		$('#table_customer').DataTable({
			paging:   false,
			destroy: true,
			processing: true,
			serverSide: true,
			autoWidth : false,
			info:     false,
			searching:false,
			ajax: {
				url: "{{route('customer_list')}}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
	            data: function (d) {
	                d.code = codevals[0];
	                d.type = codevals[1];
	                d.active = active;
	            },
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'Custcode', name: 'Custcode'},
				{data: 'CustName', name: 'CustName'},
				{data: 'BuySelltype', name: 'BuySelltype'},
				{data: 'DOB', name: 'DOB'},
				{data: 'nationalitydesc', name: 'nationalitydesc'},
				{data: 'Phone', name: 'Phone'},
				{data: 'bal', name: 'bal', class : 'text-right'},
				{data: 'CompAddress1', name: 'CompAddress1'},
				{data: 'CompAddress2', name: 'CompAddress2'},
				{data: 'bal', name: 'bal', class : 'text-right'},
				{data: 'Action', name: 'Action'},

			],
		});
	}

	function tablebeneficiary(ccode,code,active) 
	{
		$('#table_beneficiary').DataTable({
			paging:   false,
			destroy: true,
			processing: true,
			serverSide: true,
			autoWidth : false,
			info:     false,
			searching:false,
			ajax: {
				url: "{{route('beneficiary_list')}}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
	            data: function (d) {
	                d.ccode = ccode;
	                d.code = code;
	                d.active = active;
	            },
			},
	        columns: [
	        	{data: 'DT_RowIndex', name: 'DT_RowIndex'},
				{data: 'BeneName', name: 'BeneName'},
				{data: 'BeneBankName', name: 'BeneBankName'},
				{data: 'BeneBankBranch', name: 'BeneBankBranch'},
				{data: 'BeneBankAccNo', name: 'BeneBankAccNo'},
				{data: 'BeneAddress1', name: 'BeneAddress1'},
				{data: 'BeneAddress2', name: 'BeneAddress2'},
	        ],
		});	
	}
	function fillters(ftype) {
    if(ftype == 'owner'){
        $('#owners').css('background','#38a9d1');
        $('#owners').css('color', '#fff');
        $('#dealers').css('background', '#fff');
        $('#dealers').css('color', '#38a9d1');
        $('#runners').css('background', '#fff');
        $('#runners').css('color', '#38a9d1');
        $('input[name="contacttype"]').val('Owners');
        $('#contact_table_tbody tr').each(function() {
            var stype = $(this).find('input[name="ctype[]"]').val();
            if(stype == 'Owners'){
                $('#contact_table_tbody tr#'+$(this).attr('id')).show();
            } else{
                $('#contact_table_tbody tr#'+$(this).attr('id')).hide();
            }
        });
    } else if(ftype == 'dealer'){
        $('#owners').css('background', '#fff');
        $('#owners').css('color', '#38a9d1');
        $('#dealers').css('background', '#38a9d1');
        $('#dealers').css('color', '#fff');
        $('#runners').css('background', '#fff');
        $('#runners').css('color', '#38a9d1');
        $('input[name="contacttype"]').val('Dealers');
        $('#contact_table_tbody tr').each(function() {
            var stype = $(this).find('input[name="ctype[]"]').val();
            if(stype == 'Dealers'){
                $('#contact_table_tbody tr#'+$(this).attr('id')).show();
            } else{
                $('#contact_table_tbody tr#'+$(this).attr('id')).hide();
            }
        });
    } else if(ftype == 'runner'){
        $('#owners').css('background', '#fff');
        $('#owners').css('color', '#38a9d1');
        $('#dealers').css('background', '#fff');
        $('#dealers').css('color', '#38a9d1');
        $('#runners').css('background', '#38a9d1');
        $('#runners').css('color', '#fff');
        $('input[name="contacttype"]').val('Runners');
        $('#contact_table_tbody tr').each(function() {
            var stype = $(this).find('input[name="ctype[]"]').val();
            if(stype == 'Runners'){
                $('#contact_table_tbody tr#'+$(this).attr('id')).show();
            } else{
                $('#contact_table_tbody tr#'+$(this).attr('id')).hide();
            }
        });
    }
}
function errorcustomer(){
    var nriccusname = $('#cusname').val();
    var nricno = $('#nricno').val();
    var contactnationality = $("#contactnationality").val();
    var contactdob = $("#contactdob").val();
    var contactposition = $("#contactposition").val();
    var contactremarks = $("#contactremarks").val();

    if(nriccusname == '') {
        $('#cusname').css('border', '1px solid #f52604'); 
    } else {
        $('#cusname').css('border', '1px solid #ced4da'); 
    }
    if(nricno == '') {
        $('#nricno').css('border', '1px solid #f52604'); 
    } else {
        $('#nricno').css('border', '1px solid #ced4da'); 
    }
    if(contactnationality == '') {
        $("#contactnationality").css('border', '1px solid #f52604'); 
    } else {
        $("#contactnationality").css('border', '1px solid #ced4da'); 
    }
    if(contactdob == '') {
        $("#contactdob").css('border', '1px solid #f52604'); 
    } else {
        $("#contactdob").css('border', '1px solid #ced4da'); 
    }
    if(contactposition == '') {
        $("#contactposition").css('border', '1px solid #f52604'); 
    } else {
        $("#contactposition").css('border', '1px solid #ced4da'); 
    }
    if(contactremarks == '') {
        $("#contactremarks").css('border', '1px solid #f52604'); 
    } else {
        $("#contactremarks").css('border', '1px solid #ced4da'); 
    }
}
function errorbeneficiary(){
    var beneficiary_id = $('#beneficiary_id').val();
    var custcode = $("#custcode").val();
    var CurrencyCode = $('#CurrencyCode').val();
    var CustNRICNO = $('#CustNRICNO').val();
    var BeneName = $("#BeneName").val();
    var BeneBankName = $("#BeneBankName").val();
    var BeneBankAccNo = $("#BeneBankAccNo").val();
    var BeneAddress1 = $("#BeneAddress1").val();
    var BeneAddress2 = $("#BeneAddress2").val();
    var BeneCountry = $("#BeneCountry").val();
    var BeneMobileNo = $("#BeneMobileNo").val();
    var BeneBankBranch = $("#BeneBankBranch").val();
    var SwiftCode = $("#SwiftCode").val();
    if(BeneName == '') {
        $("#BeneName").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneName").css('border', '1px solid #ced4da'); 
    }   
    if(BeneMobileNo == '') {
        $("#BeneMobileNo").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneMobileNo").css('border', '1px solid #ced4da'); 
    }
}
var dateToday = new Date();
$( function() {
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 15);

    $( "#doe").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        minDate: dateToday,
    });

    $( "#contactdob").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: start.getFullYear() + ':' + end.getFullYear(),
        maxDate: dateToday
    });
} );

function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
</script>
@endsection