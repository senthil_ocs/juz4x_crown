@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@endsection
@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<style type="text/css">
    .picker__holder{
        width: 345px;
        max-width: none;
    }
</style>
@endsection
@section('main-content')
<div class="breadcrumb">
    <h1>Customers</h1>
    <ul>
        <li>Create</li>
        <li><a href="{{ route('customer_list') }}">Listing</a></li>
    </ul>
</div>

<div class="separator-breadcrumb border-top"></div>

<div class="row mb-12 master-main">
    <div class="col-md-12 mb-12">
        <div class="card text-left">
            <div class="card-body">
                <form class="needs-validation" novalidate name="customer" id="frmcustomer" method="POST" action="#"autocomplete="off">
                    {{ csrf_field() }}
                    <input type="hidden" name="optype" id="optype" value="">
                    <input type="hidden" name="cusid" id="cusid" value="">
                   	<input type="hidden" name="cuscode" id="cuscode" value="">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="general-basic-tab" data-toggle="tab" href="#generalBasic" role="tab" aria-controls="generalBasic" aria-selected="true">General</a>
                        </li>
                        
                    </ul>
                    <!-- Start of Tab -->
                    <div class="tab-content" id="myTabContent">
                        <!-- Start of General Tab -->
                        <div class="tab-pane fade show active" id="generalBasic" role="tabpanel" aria-labelledby="general-basic-tab">
                            <div class="card-title">General</div>
                            <div class="card mb-5">
                                <div class="card-body">
                                    <div class="row row-xs">
                                        <div class="col-md-auto mb-2">
                                            <label class="radio radio-primary">
                                                <input class="businessnature" type="radio" name="businessnature" value="Trading Company" formcontrolname="radio" checked="checked">
                                                <span>Trading Company</span>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-auto mb-2">
                                            <label class="radio radio-primary">
                                                <input class="businessnature" type="radio" name="businessnature" value="Individual" formcontrolname="radio">
                                                <span>Individual</span>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-auto mb-2">
                                            <label class="radio radio-primary">
                                                <input class="businessnature" type="radio" name="businessnature" value="Forex Company" formcontrolname="radio">
                                                <span>Forex Company</span>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-auto mb-2">
                                            <label class="radio radio-primary">
                                                <input class="businessnature" type="radio" name="businessnature" value="Bank" formcontrolname="radio">
                                                <span>Bank</span>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-auto mb-2">
                                            <!-- <button class="btn btn-primary btn-block">View Transaction</button> -->
                                        </div>
                            <!-- <div class="col-md-auto mb-2">
                                            <button class="btn btn-primary btn-block">View Transaction</button>
                                        </div> -->
                                        <div class="col-md-auto mb-2">
                                            <!-- <button class="btn btn-primary btn-block">View Buy and Sell Total</button> -->
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="row border-top">
                                                <div class="card-body">
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-lg-1 col-form-label">
                                                            <a onclick="GetModalDetails('customer');" data-toggle="modal" data-target="#get_modal_details" href="">
                                                                Code
                                                            </a>
                                                            <span class="mandatory">*</span>
                                                        </label>
                                                        <div class="col-lg-2">
                                                            <input type="text" class="form-control" name="code" id="code" placeholder="Code" required="" value="{{ old('code') }}" list="json-datalist" onkeyup="this.value = this.value.toUpperCase();">
                                                            <div class="invalid-feedback">
                                                                Please enter Code
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <label class="checkbox checkbox-primary">
                                                                <input class="form-check-input" type="checkbox" name="codeactive" id="codeactive" value="1" checked="checked">
                                                                <span class="ml-3" style="padding-left: 19px">Active</span>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                        <label class="col-sm-3 col-lg-1 col-form-label">Type</label>
                                                        <div class="col-lg-2">
                                                            <select class="form-control" name="buyselltype" id="buyselltype">
                                                                <option value="">-Select-</option> 
                                                                @foreach ($general_type as $key => $value)
                                                                <option value="{{$value}}" @if(old("buyselltype")==$value) selected="selected" @endif>{{$value}}</option> 
                                                                @endforeach                                                              
                                                            </select>
                                                        </div>
                                                        <label class="col-sm-4 col-lg-2 col-form-label">Mas Customer Type</label>
                                                        <div class="col-lg-2">
                                                            <select class="form-control" name="mascustomertype" id="mascustomertype">
                                                                <option value="">-Select-</option> 
                                                                @foreach ($mas_customer_type as $key => $value)
                                                                <option value="{{$value}}" @if(old("mascustomertype")==$value) selected="selected" @endif>{{$value}}</option> 
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-lg-1 col-form-label">Balance</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" class="form-control" placeholder="Balance" name="balance" id="balance" value="{{ old('balance') }}">
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <label class="checkbox checkbox-primary">
                                                                <input class="form-check-input" type="checkbox" id="highrisk" name="highrisk" value="1" @if(old('highrisk')==1) checked="checked" @endif>
                                                                <span style="padding-left: 35px;">High Risk</span>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                        <label class="col-sm-3 col-lg-1 col-form-label">Deal balance</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" class="form-control" placeholder="Deal balance" name="dealbalance" id="dealbalance" value="{{ old('dealbalance') }}">
                                                        </div>
                                                        <label class="col-sm-3 col-lg-2 col-form-label">Date</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" class="form-control" placeholder="Date" id="createdate" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-lg-2">
                                                            <label class="checkbox checkbox-primary">
                                                                <input class="form-check-input" type="checkbox" name="agent" id="agent" value="0">
                                                                <span style="padding-left: 40px">Agent</span>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                        <label class="col-sm-3 col-lg-1 col-form-label agentLicence">Licenced<span class="mandatory">*</span></label>
                                                        <div class="col-lg-2 agentLicence">
                                                            <select class="form-control" name="licence" id="licence" required>
                                                                <option value="">-Select-</option> 
                                                                <option value="licenced">Licenced</option> 
                                                                <option value="nonlicenced">Non Licenced</option> 
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12 master-main-sec">
                                    <div class="card mb-6">
                                        <div class="card-body">
                                            <div class="card-title">Company</div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_name" class="col-sm-2 col-form-label">Customer Name <span class="mandatory">*</span></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="customer_name" class="form-control" autocomplete="off" id="customer_name" placeholder="Enter Customer name" required value="{{ old('customer_name') }}">
                                                            <div class="invalid-feedback">
                                                                Please enter Customer Name
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_regn_no" class="col-sm-4 col-form-label">Regn No</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="company_regn_no" id="company_regn_no" value="{{ old('company_regn_no') }}" placeholder="Enter Regn No(ROC No)">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_type" name="company_regn_no" id="company_regn_no" class="col-sm-4 col-form-label">Type</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" name="comptype" id="comptype">
                                                                <option value="">-Select-</option> 
                                                                @foreach ($company_type as $key => $value)
                                                                <option @if(old("comptype")==$value) selected="selected" @endif>{{$value}}</option> 
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6 ">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Date</label>
                                                        <div class="col-sm-8">
                                                            <div class="input-group">
                                                                <input type="text" id="picker3" class="form-control" placeholder="dd-mm-yyyy" name="incorporationdate" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_place" class="col-sm-4 col-form-label">Place</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="company_place" id="company_place" placeholder="Enter Place" value="{{ old('company_place') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_license" class="col-sm-4 col-form-label">License</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="company_license" id="company_license" placeholder="Enter MC/Trading License" value="{{ old('company_license') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_turn_over" class="col-sm-4 col-form-label">Turn Over</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="company_turn_over" id="company_turn_over" placeholder="Enter Turn Over" value="{{ old('company_turn_over') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_valid_from" class="col-sm-4 col-form-label">Valid From</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="company_valid_from" class="form-control" id="company_valid_from" placeholder="Enter Valid From" value="{{ old('company_valid_from') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_valid_till" class="col-sm-4 col-form-label">Valid Till</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="company_valid_till" class="form-control" id="company_valid_till" placeholder="Enter Valid Till" value="{{ old('company_valid_till') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_issuing_authority" class="col-sm-4 col-form-label">Issuing Authority</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="company_issuing_authority" class="form-control" id="company_issuing_authority" placeholder="Enter Issuing Authority" value="{{ old('company_issuing_authority') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="company_overseas" name="company_overseas" value="1" @if(old('company_overseas')==1) checked="checked" @endif>
                                                            <span style="padding-left: 20px;">Overseas</span>
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_postal" class="col-sm-2 col-form-label">Postal Code <span class="mandatory">*</span></label>
                                                        <div class="col-sm-10 input-group mb-3">
                                                            <input type="text" name="company_postal"  class="form-control allownumeric" id="company_postal" placeholder="Enter Postal Code" value="{{ old('company_postal') }}" required>
                                                            <div class="invalid-feedback"> 
                                                                Please Enter Postal Code
                                                            </div>
                                                            <div class="input-group-append" id="postalcodeserach" style="cursor: pointer;">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="i-Business-Mens"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                           
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_address" class="col-sm-2 col-form-label">Address<span class="mandatory">*</span></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="company_address1" class="form-control" id="company_address1" placeholder="Enter Address" value="{{ old('company_address1') }}" required>
                                                            <div class="invalid-feedback"> 
                                                                Please Enter Address
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_address2" class="col-sm-2 col-form-label"></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="company_address2"  class="form-control" id="company_address2" placeholder="Enter Address" value="{{ old('company_address2') }}"> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="UnitNo" class="col-sm-2 col-form-label">UnitNo <span class="mandatory">*</span></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="UnitNo" class="form-control" id="UnitNo" placeholder="Enter UnitNo" value="{{ old('UnitNo') }}" required>
                                                            <div class="invalid-feedback">
                                                                Please Enter UnitNo
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_country" class="col-sm-4 col-form-label">Country <span class="mandatory">*</span></label>
                                                        <div class="col-sm-8">
                                                            <!-- <input type="text" name="company_country" class="form-control" id="company_country" placeholder="Enter Country" value="{{ old('company_country') }}" required>
                                                            <div class="invalid-feedback">
                                                                Please Enter Country
                                                            </div> -->
                                                            <select class="form-control" name="company_country" id="company_country" required>
                                                                <option value="">Select Country</option>
                                                                @foreach($country as $value)
                                                                <option value="{{trim($value->Country,' ')}}">{{trim($value->Country,' ')}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_phone1" class="col-sm-3 col-form-label">Phone1 <span class="mandatory">*</span></label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="company_phone1" class="form-control allownumeric" id="company_phone1" placeholder="Enter Phone Number1" value="{{ old('company_phone1') }}" required>
                                                            <div class="invalid-feedback">
                                                                Please Enter Phone Number
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_phone2" class="col-sm-4 col-form-label">Phone2</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="company_phone2" class="form-control allownumeric" id="company_phone2" placeholder="Enter Phone Number2" value="{{ old('company_phone2') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_fax" class="col-sm-3 col-form-label">Fax</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="company_fax" class="form-control allownumeric" id="company_fax" placeholder="Enter Fax" value="{{ old('company_fax') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="mobile" class="col-sm-2 col-form-label">Mobile</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="mobile" class="form-control" id="mobile" placeholder="Enter Mobile" value="{{ old('mobile') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_email" class="col-sm-2 col-form-label">Email</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="company_email" class="form-control" id="company_email" placeholder="Enter Email" value="{{ old('company_email') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_remarks" class="col-sm-2 col-form-label">Remarks <span class="mandatory">*</span></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="company_remarks" class="form-control" id="company_remarks" placeholder="Enter Remarks" value="{{ old('company_remarks') }}" required>
                                                            <div class="invalid-feedback">
                                                                Please Enter Remarks
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_check_list" class="col-sm-2 col-form-label">Check List</label>
                                                        <div class="col-sm-10 checklist-bg">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label class="checkbox checkbox-primary">
                                                                        <input type="checkbox" id="company_check_list1" name="company_check_list[]" value="ACRA Cop" {{ (is_array(old('company_check_list')) && in_array("ACRA Cop", old('company_check_list'))) ? ' checked' : '' }}>
                                                                        <span style="padding-left: 20px;">ACRA Cop</span>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label class="checkbox checkbox-primary">
                                                                        <input type="checkbox" id="company_check_list2" name="company_check_list[]" value="IC Copy of persons listed above" {{ (is_array(old('company_check_list')) && in_array("IC Copy of persons listed above", old('company_check_list'))) ? ' checked' : '' }}>
                                                                        <span style="padding-left: 20px;">IC Copy of persons listed above</span>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label class="checkbox checkbox-primary">
                                                                        <input type="checkbox" id="company_check_list3" name="company_check_list[]" value="ACRA Copy" {{ (is_array(old('company_check_list')) && in_array("ACRA Copy", old('company_check_list'))) ? ' checked' : '' }}>
                                                                        <span style="padding-left: 20px;">ACRA Copy</span>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-sm-12 master-main-sec">
                                    <div class="card mb-6">
                                        <div class="card-body">
                                            <div class="card-title">Company Representative</div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="company_contact" class="col-sm-2 col-form-label">Contact <span class="mandatory">*</span></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="company_contact" class="form-control" id="company_contact" placeholder="Enter Primary contact name" value="{{ old('company_contact') }}" required>
                                                            <div class="invalid-feedback">
                                                                Please enter Primary contact name
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_nric_no" class="col-sm-4 col-form-label">NRIC No <span class="mandatory">*</span></label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="company_nric_no"  class="form-control" id="company_nric_no" placeholder="Enter Regn No(ROC No)" value="{{ old('company_nric_no') }}" required>
                                                            <div class="invalid-feedback">
                                                                Please Enter Regn No
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="company_identifiertype" class="col-sm-3 col-form-label">IdentifierType</label>
                                                        <div class="col-sm-9">
                                                           <select class="form-control" name="identifiertype" id="identifiertype">
                                                                <option value="">-Select-</option>  
                                                                @foreach ($identifier_type as $key => $value)
                                                                    <option value="{{$value}}" @if(old("identifiertype")==$value) selected="selected" @endif>{{$value}}</option> 
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Nationality <span class="mandatory">*</span></label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="nationality" id="nationality" required>
                                                                <option value="">Select Nationality</option>
                                                                @foreach($nationality as $value)
                                                                <option value="{{trim($value->Code,' ')}}">{{trim($value->Description,' ')}}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="invalid-feedback">Select Nationality</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-8">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-3 col-form-label">Postal Code</label>
                                                        <div class="col-sm-9 input-group mb-3">
                                                            <input type="text" class="form-control allownumeric" name="postalcode" id="postalcode" placeholder="Enter Postal Code" value="{{ old('postalcode') }}">
                                                            <div class="input-group-append" id="postalcoderep" style="cursor: pointer;">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="i-Business-Mens"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <label class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="copyaddress">
                                                            <span style="padding-left: 20px;">Copy Address</span>
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Address</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="address1" name="address1" placeholder="Enter Address" value="{{ old('address1') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label"></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="address2" name="address2" placeholder="Enter Address" value="{{ old('address2') }}">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="UnitNo" class="col-sm-2 col-form-label">UnitNo </label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="unit_no" class="form-control" id="unit_no" placeholder="Enter UnitNo" value="{{ old('unit_no') }}" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Country</label>
                                                        <div class="col-sm-10">
                                                            <!-- <input type="text" class="form-control" id="overseascountry" name="overseascountry" value="{{ old('overseascountry') }}" placeholder="Enter Country"> -->
                                                            <select class="form-control" name="overseascountry" id="overseascountry" required>
                                                                <option value="">Select Country</option>
                                                                @foreach($country as $value)
                                                                <option value="{{trim($value->Country,' ')}}">{{trim($value->Country,' ')}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Phone Number <span class="mandatory">*</span></label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control allownumeric" id="phone" name="phone" placeholder="Enter Phone Number" value="{{ old('phone') }}" required>
                                                            <div class="invalid-feedback">
                                                                Please Enter Phone Number
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-3 col-form-label">Fax</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control allownumeric" id="fax" name="fax" value="{{ old('fax') }}" placeholder="Enter Fax">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="email" name="emailcomps" value="{{ old('email') }}" placeholder="Enter Email">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">DOB</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control" placeholder="dd-mm-yyyy" type="text" id="dob" name="dob" value="{{ old('dob') }}" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Position</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="position" name="position" placeholder="Enter Position" value="{{ old('position') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Employement</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="employmentdetail" id="employmentdetail">
                                                                <option value="">-Select-</option> 
                                                                @foreach ($employment_detail as $key => $value)
                                                                    <option value="{{$value}}" @if(old("employmentdetail")==$value) selected="selected" @endif>{{$value}}</option> 
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Yearly Income</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="yearlyincome" id="yearlyincome">
                                                                <option value="">-Select-</option> 
                                                                @foreach ($yearly_income as $key => $value)
                                                                    <option value="{{$value}}" @if(old("yearlyincome")==$value) selected="selected" @endif>{{$value}}</option> 
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Source of Income</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="sourceofincome" id="sourceofincome">
                                                                <option value="">-Select-</option> 
                                                                @foreach ($sourceofincome as $key => $value)
                                                                    <option value="{{$value}}" @if(old("sourceofincome")==$value) selected="selected" @endif>{{$value}}</option> 
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <label for="inputEmail3" class="col-sm-2 col-form-label">DOE</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control" placeholder="dd-mm-yyyy" type="text" id="doe" name="doe" value="{{ old('doe') }}" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <!-- End of General Tab -->
                    </div>    
                    <!-- End of Tab -->

                    <div class="card mb-5">
                        <div class="card-body">
                            <div class="row row-xs">
                                <div class="col-lg-12 text-center">
                                    <button class="btn btn-primary m-1" type="button" id="sbmtbtn" onclick="submitForm('save');">Save (F6)</button>
                                    <button class="btn  btn-primary m-1" type="button" onclick="submitForm('delete');">Delete (F8)</button>
                                    <button class="btn btn-primary m-1" type="button" onclick="submitForm('cancel');">Cancel (F5)</button>
                                    <button class="btn btn-primary m-1" type="button" onclick="submitForm('esc');" >Close (Esc)</button>
                                    <button class="btn btn-primary m-1" type="button">Print</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('modal.customer')
@endsection


@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
<script src="{{asset('assets/js/ladda.script.js')}}"></script>
<!-- <script src="{{asset('assets/js/vendor/fs.min.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
@endsection

@section('bottom-js')
<script src="{{asset('assets/js/modal.details.js')}}"></script>
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
$(document).ready(function(){
    $('.agentLicence').hide();
    $('input[name="agent"]').click(function(){
        if($(this).prop("checked") == true){
           $('input[name="agent"]').val('1');
            $('.agentLicence').show();
        } else if($(this).prop("checked") == false){
            $('input[name="agent"]').val('0');
            $('.agentLicence').hide(); 
        }
    });
    $('#postalcodeserach').hide();
    $('#postalcoderep').hide();
    $('#company_country').on('change',function(){
        if($('#company_country').val() == "SINGAPORE")
        {
            $('#company_phone1').attr('maxlength',"8");
        } else {
            $('#company_phone1').removeAttr('maxlength');
        }
    });
    $('#identifiertype').on('change',function(){
        if($('#identifiertype').val() == "Singapore Citizen" || $('#identifiertype').val() == "Permanent Residence"){
            var date = new Date();
            var mnt = date.getMonth()+1;
            var yrs = date.getFullYear()+30;
            var cdate = date.getDate()+'-'+mnt+'-'+yrs;
            $('#doe').val(cdate);
        } else {
            $('#doe').val('');
        }
    });
	/******Append Rows on click Add in Contact tab***/
	var c=0;
	/*$(document).on('click',"#addcontact",function(){
		c++;
        var contactcount = $('#contactcount').val();
		var contacttype = $("#contacttype").val();
		var nriccusname = $('#cusname').val();
		var nricno = $('#nricno').val();
	    var contactnationality = $("#contactnationality").val();
	    var contactdob = $("#contactdob").val();
	    var contactposition = $("#contactposition").val();
	    var contactremarks = $("#contactremarks").val();
	    if($('#contactactive'). prop("checked") == true){
	    	var contactactive = 'Yes';
	    } else {
	    	var contactactive = 'No';
	    }
	    var doe = $("#doe").val();
        if(contactcount!=undefined) {
            var cindex = parseInt(contactcount) + 1;
            $('#contactcount').val(cindex);
        } else {
            var cindex = c;
        }
    
        if(nriccusname == '' || nricno == '' || contactnationality == '' || contactdob == '' || contactposition == '' || contactremarks == ''){
            errorcustomer();
            return false;
        }
	    var contactdata = "<tr id='cid"+cindex+"' class='contactcls'><td>"+cindex+"</td><td><input type='hidden' value='"+contacttype+"' name='ctype[]'><input type='hidden' value='"+nriccusname+"' name='nriccustomername[]'>"+nriccusname+"</td><td><input type='hidden' value='"+nricno+"' name='nricnumber[]'>" + nricno + "</td><td><input type='hidden' value='"+contactnationality+"' name='cnationality[]'>" + contactnationality + "</td><td><input type='hidden' value='"+contactdob+"' name='cdob[]'>" + contactdob + "</td><td><input type='hidden' value='"+contactposition+"' name='cposition[]'>" + contactposition + "</td><td></td><td><input type='hidden' value='"+contactactive+"' name='cactive[]'>" + contactactive + "</td><td><input type='hidden' value='"+contactremarks+"' name='cremarks[]'>" + contactremarks + "</td><td><input type='hidden' value='"+doe+"' name='cdoe[]'>" + doe + "</td></tr>";
        //$("#contact_table tbody").html("");
	    $("#contact_table tbody").append(contactdata);
        $('#contacttype').val('Select Type');
        $('input[name="cusname"]').val('');
        $('input[name="nricno"]').val('');
        $('input[name="contactnationality"]').val('');
        $('input[name="contactdob"]').val('');
        $('#contactposition').val('');
        $('input[name="contactremarks"]').val('');
        $('input[name="doe"]').val('');
	});*/

    var b=0;
    /*$(document).on('click',"#addbeneficiary",function(){
        b++;
        var delbeneficiaryclsid = $('#delbeneficiaryclsid').val();
        var beneficiary_id = $('#beneficiary_id').val();
        var custcode = $("#custcode").val();
        var CurrencyCode = $('#CurrencyCode').val();
        var CustNRICNO = $('#CustNRICNO').val();
        var BeneName = $("#BeneName").val();
        var BeneBankName = $("#BeneBankName").val();
        var BeneBankAccNo = $("#BeneBankAccNo").val();
        var BeneAddress1 = $("#BeneAddress1").val();
        var BeneAddress2 = $("#BeneAddress2").val();
        var BeneCountry = $("#BeneCountry").val();
        var BeneMobileNo = $("#BeneMobileNo").val();
        var BeneBankBranch = $("#BeneBankBranch").val();
        var SwiftCode = $("#SwiftCode").val();
        var purpose = $("#purpose").val();
        if($('#beneficiaryactive'). prop("checked") == true){
            var beneficiaryactive = 'Active';
        } else {
            var beneficiaryactive = 'Inactive';
        }
        var doe = $("#doe").val();
        if(delbeneficiaryclsid!=undefined) {
            var bindex = parseInt(delbeneficiaryclsid) + 1;
            $('#delbeneficiaryclsid').val(bindex);
        } else {
            var bindex = b;
        }
        if(beneficiary_id == '' || custcode == '' || CurrencyCode == '' || CustNRICNO == '' || BeneName == '' || BeneBankName == '' || BeneBankAccNo == '' || BeneAddress1 == '' || BeneAddress2 == '' || BeneCountry == '' || BeneMobileNo == '' || BeneBankBranch == '' || SwiftCode == '' || purpose == ''){
            errorbeneficiary();
            return false;
        }
        var contactdata = "<tr id='bid"+bindex+"' class='beneficiarycls'><td>"+bindex+"</td><td><input type='hidden' value='"+beneficiary_id+"' name='td_beneficiary_id[]'>"+beneficiary_id+"</td><td><input type='hidden' value='"+custcode+"' name='td_custcode[]'>" + custcode + "</td><td><input type='hidden' value='"+CurrencyCode+"' name='td_CurrencyCode[]'>" + CurrencyCode + "</td><td><input type='hidden' value='"+CustNRICNO+"' name='td_CustNRICNO[]'>" + CustNRICNO + "</td><td><input type='hidden' value='"+BeneName+"' name='td_BeneName[]'>" + BeneName + "</td><td><input type='hidden' value='"+BeneBankName+"' name='td_BeneBankName[]'>" + BeneBankName + "</td><td><input type='hidden' value='"+BeneBankAccNo+"' name='td_BeneBankAccNo[]'>" + BeneBankAccNo + "</td><td><input type='hidden' value='"+BeneAddress1+"' name='td_BeneAddress1[]'>" + BeneAddress1 + "</td><td><input type='hidden' value='"+BeneAddress2+"' name='td_BeneAddress2[]'>" + BeneAddress2 + "</td><td><input type='hidden' value='"+BeneCountry+"' name='td_BeneCountry[]'>" + BeneCountry + "</td><td><input type='hidden' value='"+BeneMobileNo+"' name='td_BeneMobileNo[]'>" + BeneMobileNo + "</td><td><input type='hidden' value='"+BeneBankBranch+"' name='td_BeneBankBranch[]'>" + BeneBankBranch + "</td><td><input type='hidden' value='"+SwiftCode+"' name='td_SwiftCode[]'>" + SwiftCode + "</td><td><input type='hidden' value='"+beneficiaryactive+"' name='td_beneficiaryactive[]'>" + beneficiaryactive + "</td><td><input type='hidden' value='"+purpose+"' name='td_purpose[]'>" + purpose + "</td></tr>";
        $("#beneficiary_table tbody").append(contactdata);
        $('#beneficiary_id').val('');
        $("#custcode").val('');
        $('#CurrencyCode').val('');
        $('#CustNRICNO').val('');
        $("#BeneName").val('');
        $("#BeneBankName").val('');
        $("#BeneBankAccNo").val('');
        $("#BeneAddress1").val('');
        $("#BeneAddress2").val('');
        $("#BeneCountry").val('');
        $("#BeneMobileNo").val('');
        $("#BeneBankBranch").val('');
        $("#SwiftCode").val('');
        $("#purpose").val(''); 
    });*/
    var date = new Date();
    var mnt = date.getMonth()+1;
    var cdate = date.getDate()+'-'+mnt+'-'+date.getFullYear();
    $('#picker3').val(cdate);

    $("#createdate, #company_valid_from, #company_valid_till, #picker3, #dob").inputmask("dd-mm-yyyy", {
        separator: "-",
        alias: "dd-mm-yyyy",
        placeholder: "dd-mm-yyyy"
    });
    /*$(document).on('click',".contactcls",function(){
        $('.contactcls').removeClass("highlight");
        $(this).toggleClass("highlight");
        var cclickid = $(this).attr('id');

        $('#delcontactid').val(cclickid);
    });

	$(document).on('click',".beneficiarycls",function(){
        $('.beneficiarycls').removeClass("highlight");
		$(this).toggleClass("highlight");
		var cclickid = $(this).attr('id');
		$('#delbeneficiaryclsid').val(cclickid);
	});*/

    $('input[name="company_postal"]').keypress(function(){
        if($(this).val().length >= 4){
            $('#postalcodeserach').show();
        } else {
            $('#postalcodeserach').hide();
        }
    });
    $('input[name="company_postal"]').blur(function(){
        if($(this).val().length >= 4){
            $('#postalcodeserach').show();
        } else {
            $('#postalcodeserach').hide();
        }
    });

    $(document).on('click', '#postalcodeserach', function(){
        $("#company_country").val("SINGAPORE");
        var postal = $('input[name="company_postal"]').val();
        if(postal.length >= 4){
            $.ajax({
                url: "./postalcode",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    postal : postal,
                },
                beforeSend: function() {},
                success: function(response){
                    if(response.status == 200 && response.postal != 'Error'){
                        console.log(response.postal);
                        var mix = response.postal.block+' '+response.postal.Road;
                        $('input[name="company_address1"]').val(mix);
                    } else {
                        console.log(response.postal);
                        $('input[name="company_address1"]').val('');

                    }
                }
            });
        } else {
            $('input[name="company_address1"]').val('');
            $('input[name="company_address2"]').val('');
        }
    });

    $('input[name="postalcode"]').keypress(function(){
        if($(this).val().length >= 4){
            $('#postalcoderep').show();
        } else {
            $('#postalcoderep').hide();
        }
    });
    $('input[name="postalcode"]').blur(function(){
        if($(this).val().length >= 4){
            $('#postalcoderep').show();
        } else {
            $('#postalcoderep').hide();
        }
    });

    $(document).on('click', '#postalcoderep', function(){
        $("#overseascountry").val("SINGAPORE");
        var postal = $('input[name="postalcode"]').val();
        if(postal.length >= 4){
            $.ajax({
                url: "./postalcode",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    postal : postal,
                },
                beforeSend: function() {},
                success: function(response){
                    if(response.status == 200 && response.postal != 'Error'){
                        console.log(response.postal);
                        var mix = response.postal.block+' '+response.postal.Road;
                        $('input[name="address1"]').val(mix);
                    } else {
                        console.log(response.postal);
                        $('input[name="address1"]').val('');

                    }
                }
            });
        } else {
            $('input[name="address1"]').val('');
            $('input[name="address2"]').val('');
        }
    });

	/*$(document).on('click',"#deletecontact",function(){
		var delcid = $('#delcontactid').val();	
		if(delcid!='') {
			$.confirm({
	            title: 'Confirm!',
	            content: 'Are you sure to delete the Contact?',
	            buttons: {
	                Ok: function () {
	                    $('#'+delcid).remove();
						recalcId();
						c--;
	                },
	                cancel: function () {

	                }
	            }
        	});
		} else {
			alert("Choose row to delete!");
		}
	});*/

   /* $(document).on('click',"#deletebeneficiary",function(){
        var delcid = $('#delbeneficiaryclsid').val();  
        alert(delcid);
        if(delcid!='') {
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete the Contact?',
                buttons: {
                    Ok: function () {
                        $('#'+delcid).remove();
                        recalcbeneficiaryId();
                        c--;
                    },
                    cancel: function () {

                    }
                }
            });
        } else {
            alert("Choose row to delete!");
        }
    });*/

	function recalcId(){
	    $.each($("beneficiary_table tr.contactcls"),function (i,el){
	        $(this).find("td:first input").val(i + 1); 
	    });
	}
    function recalcbeneficiaryId(){
        $.each($("contact_table tr.beneficiarycls"),function (i,el){
            $(this).find("td:first input").val(i + 1); 
        });
    }

	/******Append Rows on click Add in Contact tab***/

    $('#employmentdetail').prop("disabled","disabled");
    $('#yearlyincome').prop("disabled","disabled");
    $('#sourceofincome').prop("disabled","disabled");

    $('.businessnature').click(function(){
        var businessnaturedel = $("input[name='businessnature']:checked").val();
        if(businessnaturedel=='Individual') {
            $('#employmentdetail').prop("disabled",false);
            $('#yearlyincome').prop("disabled",false);
            $('#sourceofincome').prop("disabled",false);
        }  else {
            $('#employmentdetail').prop("disabled","disabled");
            $('#yearlyincome').prop("disabled","disabled");
            $('#sourceofincome').prop("disabled","disabled");
            $('#employmentdetail').val("");
            $('#yearlyincome').val("");
            $('#sourceofincome').val("");            
        }
    });

    // $(document).keypress('#code', function() {
    //     $('#code').val($('#code').val().toUpperCase());
    // });
    $(document).on("click", "input[name='customer_id_detail']",function() {
        $('#get_modal_details').modal('toggle');
        $('input[name="code"]').val($(this).val());
        $('input[name="code"]').trigger( "blur" );
    });

    $("#code").blur(function(){
        var cval = $(this).val();
        if(cval!='') {
            var _token = $('input[name="_token"]').val();
            showcustomerdetails(_token,cval);
        }
    }); 

    function showcustomerdetails(_token,cval)
        {

            $.ajax({
                  url:"{{ route('getcustomerdetails') }}",
                  method:"POST",
                  data:{code:cval, _token:_token},
                  success:function(cdata){ 
                    setTimeout(function(){
                        $('#owners').trigger('click');
                    },100);
                  	if(cdata.CustName!=undefined) {
                        $('#optype').val('update');
                        $('#cusid').val(cdata.Id);
                        $('#cuscode').val(cdata.Custcode);
                        $('.custCodedis').val(cdata.Custcode);
                        $('.custCodedis').prop("disabled", true);
                        $('.custNRICdis').val(cdata.NRICNo);
                        $('.custNRICdis').prop("disabled", true);
	                    $('#customer_name').val(cdata.CustName);
	                    $('#company_regn_no').val(cdata.CompanyRegNo);
	                    $('#picker3').val(cdata.IncorporationDate);
	                    $('#comptype option[value="'+cdata.BusinessType+'"]').attr('selected','selected');
	                    $('#mascustomertype option[value="'+cdata.MCType+'"]').attr('selected','selected');
						$('#buyselltype option[value="'+cdata.BuySelltype+'"]').attr('selected','selected');

	                    $('#company_place').val(cdata.IncorporationPlace);
	                    $('#company_license').val(cdata.MoneyChangerLicense);
	                    $('#company_turn_over').val(cdata.AnnualTurnOver);
	                    $('#company_valid_from').val(cdata.LicenseValidFrom);
	                    $('#company_valid_till').val(cdata.LicenseValidTo);
	                    $('#company_issuing_authority').val(cdata.IssuingAuthority);
	                    if(cdata.Active=="1") {
	                        $('#codeactive').prop('checked', true);
	                    }
	                    if(cdata.OverSeasBranches=="1") {
	                        $('#company_overseas').prop('checked', true);
	                    }
	                    if(cdata.HighRisk=="1") {
	                        $('#highrisk').prop('checked', true);
	                    }
                        if(cdata.Agent=="1") {
                            $('#agent').prop('checked', true);
                            $('.agentLicence').show();
                        } else {
                            $('#agent').prop('checked', false);
                            $('.agentLicence').hide();
                        }
                        $('#licence').val(cdata.Licenced);
	                    $('#company_postal').val(cdata.CompPostalCode);
	                    $('#company_address1').val(cdata.CompAddress1);
	                    $('#company_address2').val(cdata.CompAddress2);
	                    $('#company_country').val(cdata.Country);
	                    $('#company_phone1').val(cdata.CompPhone1);
	                    $('#company_phone2').val(cdata.CompPhone2);
	                    $('#company_fax').val(cdata.CompFax);
	                    $('#mobile').val(cdata.HandPhone);
	                    $('#company_email').val(cdata.CompEmail);
	                    $('#company_remarks').val(cdata.Remarks);
	                    if(cdata.CheckList != '') {
	                        var checklist = cdata.CheckList;
	                        var checklistvals = checklist.split(',');
	                        $.each(checklistvals, function( index, value ) {
	                          $("input[value='" + value + "']").prop('checked', true);
	                        });
	                    }
                        $('#balance').val(cdata.customerbalance);
                        $('#dealbalance').val(cdata.cusdealbalance);
	                    $('#company_contact').val(cdata.Name);
	                    $('#company_nric_no').val(cdata.NRICNo);
	                    $('#identifiertype option[value="'+cdata.TypeOfIdentification+'"]').attr('selected','selected');
	                    $('#nationality').val(cdata.Nationality);
                        $("input[name=businessnature][value='" + cdata.NatureOfbusiness + "']").prop('checked', true);
                        /*$("input[name=businessnature][value="+cdata.NatureOfbusiness+"]").prop('checked', true);*/
	                    $('#postalcode').val(cdata.PostalCode);
	                    $('#address1').val(cdata.Address1);
	                    $('#address2').val(cdata.Address2);
                        $('#overseascountry').val(cdata.Address3);
                        $('#UnitNo').val(cdata.UnitNo);
	                    $('#unit_no').val(cdata.UnitNoRepresentative);
	                    $('#phone').val(cdata.Phone);
	                    $('#fax').val(cdata.Fax);
	                    $('#email').val(cdata.Email);
                        $('#dob').val(cdata.DOB);
	                    $('#doe').val(cdata.DOE);
	                    $('#position').val(cdata.Position);
	                    $('#employmentdetail').val(cdata.EmploymentDetail);
	                    $('#yearlyincome').val(cdata.YearlyIncome);
	                    $('#sourceofincome').val(cdata.SourceOfIncome);
	                    /*if(cdata.CompanySourceOfFunds != '') {
	                        var sf = cdata.CompanySourceOfFunds;
	                        var sourcefunds = sf.split(',');
	                        $.each(sourcefunds, function( index, value ) {
	                          $("input[value='" + value + "']").prop('checked', true);
	                        });
	                    }
	                    if(cdata.top3ForeignCurrencies != '') {
	                    	var curr = cdata.top3ForeignCurrencies;
	                    	var fcurr = curr.split(',');
	                    	$('#top3currency1').val(fcurr[0]);
	                    	$('#top3currency2').val(fcurr[1]);
	                    	$('#top3currency3').val(fcurr[2]);
	                    }
	                    if(cdata.DealingswithLocalorForeignGovernment=="Yes") {
	                        $('#dealingyes').prop('checked', true);
	                    } else {
	                        $('#dealingno').prop('checked', true);
	                    }
	                    if(cdata.TypeOfCustomer != '') {
	                    	var tyofcus = cdata.TypeOfCustomer;
	                    	var customertype = tyofcus.split(',');
	                    	$.each(customertype, function( index, value ) {
	                          $("input[value='" + value + "']").prop('checked', true);
	                        });
	                    }
	                    $('#emailaddress').val(cdata.EmailAddress);
                        $('#website').val(cdata.Website);
	                    $('#totalemployees').val(cdata.TotalNoOfEmployees);
	                    $('#cusremarks').html(cdata.PendingCustomerRemarks);
	                    $('#createdate').val(cdata.Createdat);

                        var contactdata = cdata['cuscontactdata'];
	                    var benificiery = cdata['benificiery'];
                        $('#contact_table tbody').html(contactdata);
                        if(benificiery == '' || benificiery == undefined){
                            $('#beneficiary_table tbody').html("");
                        } else {
	                       $('#beneficiary_table tbody').html(benificiery);
                        }
                        */
                        $('#balance').prop('readonly', true);
                        $('#createdate').prop('readonly', true);
                        $('#dealbalance').prop('readonly', true);
                        $('#company_country').trigger('change');
                    } else {
                        $('.custCodedis').val('');
                        $('.custCodedis').prop("disabled", false);
                        $('.custNRICdis').val('');
                        $('.custNRICdis').prop("disabled", true);
                        $('#agent').prop('checked', false);
                        $('.agentLicence').hide();
                        $('#optype').val('');
                        $('#cusid').val('');
                        $('#cuscode').val('');
                        $('#customer_name').val('');
                        $('#company_contact').val('');
                        $('#company_nric_no').val('');
                        $('#postalcode').val('');
                        $('#address1').val('');
                        $('#overseascountry').val('');
                        $('#phone').val('');
                        $('#dob').val('');
                        $('#doe').val('');
                        $('#overseascountry').val('');
                        $('#dealbalance').val('');
                        /*$('#contact_table tbody').html("");
                        $('#beneficiary_table tbody').html("");*/
                        // $("input[name=businessnature]").prop('checked', false);
                        //$('#frmcustomer').find("input[type=text]").val("");

                        //var $txtfields = $('#frmcustomer input[type=text],textarea[name!=code]');
                        // $('#frmcustomer')[0].reset();
                        //$txtfields.val('');
                    }

                    /*if(cdata.CustName==undefined) {
                        $('#optype').val('');
                        alert('fdgdfg'+cdata.CustName);
                        //$('#frmcustomer').find("input[type=text]").val("");
                    }*/
                    
                  }
            });
        }

    var dformat = "{{ env('DATE_FORMAT') }}";

    $("#contactdob ,#doe").inputmask("dd-mm-yyyy", {
        separator: "-",
        alias: "dd-mm-yyyy",
        placeholder: "dd-mm-yyyy"
    });
    
    $('#copyaddress').click(function(){
        var address1 = $('#company_address1').val();
        var address2 = $('#company_address2').val();
        var postal   = $('#company_postal').val();
        var compcountry   = $('#company_country').val();
        var UnitNo   = $('#UnitNo').val();
        if($(this). prop("checked") == true){
            $('#address1').val(address1);
            $('#address2').val(address2);
            $('#postalcode').val(postal);
            $('#overseascountry').val(compcountry);
            $('#unit_no').val(UnitNo);
        } else {
            $('#address1').val("");
            $('#address2').val("");
            $('#postalcode').val("");
            $('#overseascountry').val("");
            $('#unit_no').val("");
        }

    });

    //$(document).on('click',"#sbmtbtn",function()
    /*$("#sbmtbtn").on('click', function() {
        if ($(".tab-content").find('.invalid-feedback').text()) {
        $.each($(".tab-content").find('.invalid-feedback'), function (index, value) {
            if ($(this).text()) {


                //alert($(this).text()+'---'+$(this).css('display')+'---'+$(this).css('visibility'));
                if($(this).css('display')=='block') {
                    var closerdiv = $(this).closest('.tab-pane')[0];
                    closerdiv = closerdiv.closest('.tab-pane');
                    var divid = closerdiv.id;
                    var atagid = $('#'+divid).attr("aria-labelledby");

                    //alert(divid+'--'+atagid);

                    $('.nav-link').removeClass('active');
                    $('#'+atagid).addClass('active');

                    $('.tab-pane').removeClass('show');
                    $('.tab-pane').removeClass('active');
                    $('#'+divid).addClass('show');
                    $('#'+divid).addClass('active');                    
                    return false;
                }
            }
        });
    }

    });*/

});

function submitForm(submit_type) {
    // console.log(submit_type);
    if(submit_type == 'esc'){
        window.location.href = 'dashboard';
    }else if(submit_type == 'delete'){ 
        var cid =  $('#cusid').val();
        var ccode = $('#cuscode').val();
        if(cid == '' && ccode == ''){
            $.alert({
                title: 'Alert!',
                content: 'Select Customer',
            });
            return false;
        } else {
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete the Customer?',
                buttons: {
                    Ok: function () {
                        $.ajax({
                            url:"{{ route('deletecustomerdetails') }}",
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data : {
                                cid : cid,
                                ccode : ccode,
                            },
                            success: function(response){
                                if(response.status == 200){
                                    location.reload();
                                } else {
                                    danger_toastr(response.content);
                                }
                            },      
                        });
                    },
                    cancel: function () {}
                },
            });
        }
    } else if(submit_type == 'save') {
        var code = $('input[name="code"]').val();
        var customer_name = $('input[name="customer_name"]').val();
        var company_contact = $('#company_contact').val();
        var company_nric_no = $('input[name="company_nric_no"]').val();
        var postalcode = $('input[name="company_postal"]').val();
        var address1 = $('input[name="company_address1"]').val();
        var country = $('#company_country').val();
        var cusremarks = $('#cusremarks').val();
        var UnitNo = $('input[name="UnitNo"]').val();
        var company_phone1 = $('input[name="company_phone1"]').val();
        var company_phone = $('input[name="phone"]').val();
        var company_remarks = $('input[name="company_remarks"]').val();
        /*var tablelength = $('#contact_table_tbody tr').length;*/
        var company_email = $('#company_email').val();
        var nationality = $('#nationality').val();
        var licence = $('#licence').val();
        if(code == '' || customer_name == '' || company_contact == '' || company_nric_no == '' || postalcode == '' || address1 == '' || country == '' || cusremarks == '' || nationality == '' || UnitNo == '' || company_phone1 == '' || company_phone == '' || company_remarks == '' || ($('#agent').is(':checked') && licence =='')) {
            $('#frmcustomer').addClass('was-validated');
            return false;
        }else {
            /*if(tablelength == 0){
                $.alert("Contact Details Not Found!");
                return false;
            }*/
            $('#frmcustomer').removeClass('was-validated');
            $.ajax({
                url: "./createcustomer",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : $('#frmcustomer').serialize(),
                success: function(response){
                    if(response.status == 200){
                        submitForm('cancel');
                        success_toastr(response.content);
                    } else {
                        danger_toastr(response.content);
                    }
                },      
            });
        }
    } else if(submit_type == 'cancel') {
        $('#frmcustomer')[0].reset();
        $('#contact_table_tbody tr').remove();
        $('#beneficiary_table_tbody tr').remove();
        $('#contactcount').val('');
        $('#cusremarks').val('');
        $('.agentLicence').hide(); 
        $('#company_phone1').removeAttr('maxlength');
    }
}


function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}
function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}

/*function errorcustomer(){
    var nriccusname = $('#cusname').val();
    var nricno = $('#nricno').val();
    var contactnationality = $("#contactnationality").val();
    var contactdob = $("#contactdob").val();
    var contactposition = $("#contactposition").val();
    var contactremarks = $("#contactremarks").val();

    if(nriccusname == '') {
        $('#cusname').css('border', '1px solid #f52604'); 
    } else {
        $('#cusname').css('border', '1px solid #ced4da'); 
    }
    if(nricno == '') {
        $('#nricno').css('border', '1px solid #f52604'); 
    } else {
        $('#nricno').css('border', '1px solid #ced4da'); 
    }
    if(contactnationality == '') {
        $("#contactnationality").css('border', '1px solid #f52604'); 
    } else {
        $("#contactnationality").css('border', '1px solid #ced4da'); 
    }
    if(contactdob == '') {
        $("#contactdob").css('border', '1px solid #f52604'); 
    } else {
        $("#contactdob").css('border', '1px solid #ced4da'); 
    }
    if(contactposition == '') {
        $("#contactposition").css('border', '1px solid #f52604'); 
    } else {
        $("#contactposition").css('border', '1px solid #ced4da'); 
    }
    if(contactremarks == '') {
        $("#contactremarks").css('border', '1px solid #f52604'); 
    } else {
        $("#contactremarks").css('border', '1px solid #ced4da'); 
    }
}*/

function errorbeneficiary(){
    var beneficiary_id = $('#beneficiary_id').val();
    var custcode = $("#custcode").val();
    var CurrencyCode = $('#CurrencyCode').val();
    var CustNRICNO = $('#CustNRICNO').val();
    var BeneName = $("#BeneName").val();
    var BeneBankName = $("#BeneBankName").val();
    var BeneBankAccNo = $("#BeneBankAccNo").val();
    var BeneAddress1 = $("#BeneAddress1").val();
    var BeneAddress2 = $("#BeneAddress2").val();
    var BeneCountry = $("#BeneCountry").val();
    var BeneMobileNo = $("#BeneMobileNo").val();
    var BeneBankBranch = $("#BeneBankBranch").val();
    var SwiftCode = $("#SwiftCode").val();
    var purpose = $("#purpose").val();
    if(beneficiary_id == '') {
        $('#beneficiary_id').css('border', '1px solid #f52604'); 
    } else {
        $('#beneficiary_id').css('border', '1px solid #ced4da'); 
    }
    if(custcode == '') {
        $('#custcode').css('border', '1px solid #f52604'); 
    } else {
        $('#custcode').css('border', '1px solid #ced4da'); 
    }
    if(CurrencyCode == '') {
        $("#CurrencyCode").css('border', '1px solid #f52604'); 
    } else {
        $("#CurrencyCode").css('border', '1px solid #ced4da'); 
    }
    if(CustNRICNO == '') {
        $("#CustNRICNO").css('border', '1px solid #f52604'); 
    } else {
        $("#CustNRICNO").css('border', '1px solid #ced4da'); 
    }
    if(BeneName == '') {
        $("#BeneName").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneName").css('border', '1px solid #ced4da'); 
    }
    if(BeneBankName == '') {
        $("#BeneBankName").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneBankName").css('border', '1px solid #ced4da'); 
    }
    if(BeneBankAccNo == '') {
        $("#BeneBankAccNo").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneBankAccNo").css('border', '1px solid #ced4da'); 
    }    
    if(BeneAddress1 == '') {
        $("#BeneAddress1").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneAddress1").css('border', '1px solid #ced4da'); 
    }    
    if(BeneCountry == '') {
        $("#BeneCountry").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneCountry").css('border', '1px solid #ced4da'); 
    }    
    if(BeneMobileNo == '') {
        $("#BeneMobileNo").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneMobileNo").css('border', '1px solid #ced4da'); 
    }
}

var dateToday = new Date();
$( function() {
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 15);
    $( "#createdate, #company_valid_from, #company_valid_till, #picker3").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
    });

    $( "#doe").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        minDate: dateToday,
    });

    $( "#dob, #contactdob").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: start.getFullYear() + ':' + end.getFullYear(),
        maxDate: dateToday
    });
} );
</script>
@endsection