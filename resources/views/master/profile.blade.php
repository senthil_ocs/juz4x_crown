@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
@endsection

@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<style type="text/css">
    .highlight { background-color: #c8ebf7 !important; }
    table#profile_table > tbody > tr { cursor: pointer; }
</style>
@endsection
@section('main-content')
   <div class="breadcrumb">
                <h1>User Group</h1>
                <ul>
                    <li><a href="">Listing</a></li>
                    <li>Insert</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>
            <div class="row mb-12 master-main">
                <div class="col-md-12 mb-12">
                    <div class="card text-left">
                        <form class="needs-validation" novalidate name="profile" id="frmprofile" method="POST" action="{{ route('createprofile') }}" autocomplete="off">
                        <input type="hidden" name="optype" id="optype" value="">
                        <input type="hidden" name="profile_id" id="profile_id">
                        {{ csrf_field() }}
                        <div class="card-body">
                                    <div class="row">
                                     <div class="col-lg-12 col-sm-12 master-main-sec">
                                        <div class="card mb-6">
                                            <div class="card-body">

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label"><a onclick="GetProfileModal('profile');" data-toggle="modal" data-target="#get_profile_modal_details" href="">User Group Id </a><span class="mandatory">*</span></label>
                                                            <div class="col-sm-10">
                                                                <input type="textbox" name="profileid" class="form-control" id="profileid" placeholder="Enter Profile Id" autocomplete="off" required>
                                                                <div class="invalid-feedback">
                                                                    Please Enter User Group Id
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">Description <span class="mandatory">*</span></label>
                                                            <div class="col-sm-10">
                                                                <input type="textbox" name="description" class="form-control" id="description" placeholder="Enter Description" autocomplete="off" required>
                                                                <div class="invalid-feedback">
                                                                    Please Enter Description
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <label for="name" class="col-sm-2 col-form-label">Active</label>
                                                            <div class="col-sm-10 mt-2">
                                                                <label class="checkbox checkbox-primary">
                                                                    <input type="checkbox" name="active" class="form-check-input checkbox-align" id="active">&nbsp;&nbsp;
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="card mb-5">
                                    <div class="card-body">
                                        <div class="row row-xs">
                                            <div class="table-responsive" style="height: 250px;">
                                                <table id="profile_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No.</th>
                                                            <th>User Group Id</th>
                                                            <th>Description</th>
                                                            <th>Active ?</th>
                                                            <th>Created By</th>
                                                        </tr>
                                                        <tbody>
                                                        @php
                                                        if(count($allprofiles) > 0) {
                                                            $i = 1;
                                                            foreach($allprofiles as $profile) {
                                                            $activeflag = 'No';
                                                            if($profile->Active=='1') {
                                                                $activeflag = 'Yes';
                                                            }
                                                        @endphp
                                                            <tr id="{{ $profile->id }}" class="trclass" data-profile="{{ $profile->ProfileId }}">
                                                                <td>{{ $i }}</td>
                                                                <td>{{ $profile->ProfileId }}</td>
                                                                <td>{{ $profile->Description }}</td>
                                                                <td>{{ $activeflag }}</td>
                                                                <td>{{ $profile->CreatedBy }}</td>
                                                            </tr>
                                                        @php
                                                            $i++;
                                                           }
                                                        }
                                                        @endphp
                                                        </tbody>
                                                    </thead>
                                                    <tbody id="locationcurrencylist">
                                                    </tbody>
                                                </table>    
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            <!-- End of Tab -->
                    		<div class="card mb-5">
                                <div class="card-body">
                                    <div class="row row-xs">
                                        <div class="col-lg-12 text-center">

                                            <button name="sbmtbtn" id="sbmtbtn" class="btn btn-primary m-1" type="submit">Add (F6)</button>
                                            <button class="btn btn-primary m-1" type="button" id="replacebtn">Replace (F7)</button>
                                            <button type="button" id="deletebtn" class="btn btn-primary m-1">Delete (F8)</button>
                                            
                                            <button class="btn btn-primary m-1" type="button" id="cancelbtn">Cancel (F5)</button>

                                            <button type="button" class="btn btn-primary m-1" onclick="submitForm('esc');" data-style="expand-left">Close (ESC)</button>

                                            <button class="btn btn-primary ladda-button example-button m-1" data-style="expand-left"><span class="ladda-label">Print</span><span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div></button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                <!-- end of col -->

                <!-- end of col -->
            </div>
            <!-- end of row -->
           
            <!-- end of row -->
@include('modal.profile')         
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
<script src="{{asset('assets/js/ladda.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<!-- <script src="{{asset('assets/js/datatables.script.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="{{asset('assets/js/modal.details.js')}}"></script>
@endsection



@section('bottom-js')
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>


@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif

<script type="text/javascript">
$(document).ready(function(){
    $('#profile_table').DataTable({
            "paging" : false,
            "bInfo"  : false
    });
    $(document).on("click", "input[name='profile_id_detail']",function() {
        $('#get_profile_modal_details').modal('toggle');
        $('input[name="profileid"]').val($(this).val());
        getProfileDetails($(this).val());
    });
    $(document).on("click", "#profile_table tbody tr",function() {
        $('.trclass').removeClass("highlight");
        $(this).toggleClass("highlight");
        getProfileDetails($(this).attr('data-profile'));
    });
    $("input[name='profileid']").blur(function() {
       getProfileDetails($(this).val());
    });
});

function getProfileDetails(id) {
    $.ajax({
        url: "./profile/getdetails",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            id : id,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
                $('#profile_id').val(response.details.id);
                $('#profileid').val(response.details.ProfileId);
                $('#description').val(response.details.Description);
                if(response.details.Active=='1') {
                    $('#active').prop('checked',true);
                } else{
                    $('#active').prop('checked',false);
                }
                $('#createdby').html(response.details.CreatedBy);
            } else {
                $('#profile_id').val('');
                $('#profileid').val('');
                $('#description').val('');   
                $('#active').prop('checked',false);
                $('#createdby').html('');      
            }
        },
        error: function(){}
    });
}

$(document).on('click', '#deletebtn', function(){
    var _token = $('input[name="_token"]').val();
    var profile_id = $('#profileid').val();
    if(profile_id=="") {
        alert('Click on profile to delete!');
    } else {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to delete the Profile?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        url:"{{ route('deleteprofile') }}",
                        method:"POST",
                        data:{profile_id:profile_id, _token:_token},
                        success:function(cdata){
                            if(cdata.status == 200){
                                $.alert('Profile Deleted Successfully!');
                                location.reload();
                            } else {
                                $.alert('Profile could not be deleted as it is associated to User!');
                                //location.reload();
                            }
                        }
                    });                            
                    
                },
                cancel: function () {
                    $.alert('Canceled!');
                }
            }
        });
    }
});

</script>
@endsection