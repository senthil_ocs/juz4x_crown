@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css">
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
#suggesstion-box-nric :hover {
  background-color: #e9e9e9; 
}
#suggesstion-box-name :hover {
  background-color: #e9e9e9; 
}
#suggesstion-box-phno :hover {
  background-color: #e9e9e9; 
}
.mySlides {
  display: none;
}

.cursor {
  cursor: pointer;
}

.caption-container {
  text-align: center;
  background-color: white;
  color: black;
  padding: 2px 16px;
}
</style>

@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Cash Customer List</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			    <li><a href="{{route('cashcustomer.view')}}">Create</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="col-lg-12 mb-12">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('cashcustomer_list')}}" method="POST" id="formcashcustomeredit">
					            		@csrf
									<div class="row">
										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="filterName" class="col-sm-4  col-form-label">Name</label>
												<div class="col-sm-8 text-left">
													<input type="textbox" name="filterName" class="form-control" autocomplete="off" id="filterName" placeholder="Enter Name" onkeyup="getNameAutoFill(this.value);" value="{{ old('filterName') }}">
                                   					<div id="suggesstion-box-name"></div>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="filterNric" class="col-sm-4  col-form-label">NRIC No</label>
												<div class="col-sm-8 text-left">
													<input type="textbox" name="filterNric" class="form-control" autocomplete="off" id="filterNric" placeholder="Enter NRIC No" onkeyup="getNricAutoFill(this.value);" value="{{ old('filterNric') }}">
                                   					<div id="suggesstion-box-nric"></div>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row">
												<label for="filterPhno" class="col-sm-4  col-form-label">Phone</label>
												<div class="col-sm-8 text-left">
													<input type="textbox" name="filterPhno" class="form-control" autocomplete="off" id="filterPhno" placeholder="Enter Phone No"  onkeyup="getPhnoAutoFill(this.value);" value="{{ old('filterPhno') }}">
                                   					<div id="suggesstion-box-phno"></div>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-sm-6 mb-1">
											<div class="row" style="display: flex;align-items: center;">
												<label for="filterAllow" class="col-sm-5  col-form-label">Allow Online</label>
												<div class="col-sm-7 text-left">
	                                                <input type="checkbox" id="filterAllow" name="filterAllow" value="<?php echo old('filterAllow') != '' ? old('filterAllow') : 'No'; ?>" <?php if(old('filterAllow') == 'Yes'){ echo 'checked';} ?>>
	                                                <span class="checkmark"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-7 col-sm-12"></div>
										<div class="col-lg-5 col-sm-12 text-right">
											<button type="button" class="btn btn-primary m-1" id="check_details" onclick="submitForm('submit');">Submit</button>
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>

                        <div class="table-responsive">
                            <table id="table_cashcustomerlist" class="display nowrap table table-striped table-bordered" style="width:100%">
                                
                            </table>    
                        </div>

					</div>	
				</div>
			</div>
		</div>	
	@include('modal.addbeneficiary')
	@include('modal.addImage')
	@include('modal.cashcustomeredit')
    @endsection

@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
	<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>
@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">

	$(document).ready(function(){
		getdatas();
		$('#country').val("SINGAPORE");
	    $('#tele_no').attr('maxlength',"8");
		$('#imageRow').hide();
	    $('#postalcodeserach').hide();
	    $('#identifier_other').hide();
		$('#identifier_other').val('');

	    $("#date_of_birth, #date_of_expiry").inputmask("dd-mm-yyyy", {
	        separator: "-",
	        alias: "dd-mm-yyyy",
	        placeholder: "dd-mm-yyyy"
	    });
	    $('input[name="postal_code"]').blur(function(){
	        if($(this).val().length >= 4 && $('#country').val() == "SINGAPORE"){
	            $('#postalcodeserach').show();
	        } else {
	            $('#postalcodeserach').hide();
	        }
	    });

	    $('#country').on('change', function() {
	    	if($('#postalcodeserach').val().length >= 4 && $('#country').val() == "SINGAPORE"){
	            $('#postalcodeserach').show();
	    		$('#tele_no').attr('maxlength',"8");
	        } else if($('#country').val() == "SINGAPORE"){
	    		$('#tele_no').attr('maxlength',"8");
	        } else {
	            $('#postalcodeserach').hide();
    			$('#tele_no').removeAttr('maxlength');
	        }
	    });
	    $('#identifier_type').on('change', function() {
			$('#identifier_other').val('');
	        $('#date_of_expiry').val('');
		    if($('#identifier_type').val() == "Others"){
	            $('#identifier_other').show();
	            $('#identifier_other').prop('required',true);
	        } else if($('#identifier_type').val() == "Singapore Citizen" || $('#identifier_type').val() == "Permanent Residence"){
	        	var date = new Date();
			    var mnt = date.getMonth()+1;
			    var yrs = date.getFullYear()+30;
			    var cdate = date.getDate()+'-'+mnt+'-'+yrs;
			    $('#date_of_expiry').val(cdate);
	            $('#identifier_other').hide();
	            $('#identifier_other').prop('required',false);
	        } else {
	            $('#identifier_other').hide();
	            $('#identifier_other').prop('required',false);
	        }
	    });

		$(document).on('change', '#active', function() {
	    	if ($(this).is(':checked')) {
	    		$(this).attr('value', '1');
	    	}else {
	    		$(this).attr('value', '0');
	    	}
		});
		$(document).on('change', '#pending_documents', function() {
	    	if ($(this).is(':checked')) {
	    		$(this).attr('value', '1');
	    	}else {
	    		$(this).attr('value', '0');
	    	}
		});

		$(document).on('change', '#filterAllow', function() {
	    	if ($(this).is(':checked')) {
	    		$(this).attr('value', 'Yes');
	    	}else {
	    		$(this).attr('value', 'No');
	    	}
		});
	    $(document).on('click', '#postalcodeserach', function(){
	        var postal = $('input[name="postal_code"]').val();
	        if(postal.length >= 4){
	            $.ajax({
	                url: "./postalcode",
	                type: "POST",
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },
	                data : {
	                    postal : postal,
	                },
	                beforeSend: function() {},
	                success: function(response){
	                    if(response.status == 200 && response.postal != 'Error'){
	                        console.log(response.postal);
	                        var mix = response.postal.block+' '+response.postal.Road;
	                        $('input[name="address"]').val(mix);
                        	$('input[name="address2"]').val(response.postal.buliding);
	                    } else {
	                        console.log(response.postal);
	                        $('input[name="address"]').val('');
	                        $('input[name="address2"]').val('');
	                        $.alert("Enter Valid postal code");
	                    }
	                }
	            });
	        } else {
	            $('input[name="company_address1"]').val('');
	            $('input[name="company_address2"]').val('');
	        }
	    });

	    $(document).on('click', '#edit_cashlistform', function(){
	    	var nric_no = $('input[name="nric_no"]').val();
			var cname = $('input[name="cname"]').val();
			var date_of_birth= $('#date_of_birth').val();
			var address = $('input[name="address"]').val();
			var country = $('input[name="country"]').val();
			var postal_code = $('input[name="postal_code"]').val();
			var tele_no = $('input[name="tele_no"]').val();
	        var identifier_type = $('#identifier_type').val();
	        var identifier_other = $('#identifier_other').val();
	        var date_of_expiry = $('#date_of_expiry').val();
	        var intelligence = $('input[name="intelligence"]:checked').val();
	        var allow = $('input[name="allow"]:checked').val();
			if(nric_no == '' ||cname  == '' ||date_of_birth  == '' ||date_of_expiry  == '' || address == '' || country =='' || postal_code == '' || tele_no == '' || nationality == '' || intelligence == undefined || allow == undefined || identifier_type == '' || (identifier_type == 'Others' && identifier_other == '')){
				$('#cashcustlisteditform').addClass('was-validated');
				return false;
			}
			if($('#cashcustomer_id').val() == ''){
	        	type = 'save';
	        	id = '';
	        } else {
	        	type = 'update';
	        	id = $('#cashcustomer_id').val();
	        }
			$.ajax({
				url: "./cashcustomer/insert_update",
				type: "POST",
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    data : $('#cashcustlisteditform').serialize() +'&type='+type,
				success: function(response){
					if(response.status == 200){
	           		$('#get_cashcust_list').modal('toggle');
						success_toastr(response.content);
	           			$('#check_details').trigger('click');
					}else {
						$.each(response.content, function(v, t){
			            	danger_toastr(t);
			            });
					}
				},
			});
		});
	    $(document).on('click',"#add_beneficiaryform",function(){	
	        /*var CurrencyCode = $('#CurrencyCode').val();
	        var CustNRICNO = $('#CustNRICNO').val();*/
	        var BeneName = $("#BeneName").val();
	        /*var BeneBankName = $("#BeneBankName").val();
	        var BeneBankAccNo = $("#BeneBankAccNo").val();
	        var BeneAddress1 = $("#BeneAddress1").val();
	        var BeneAddress2 = $("#BeneAddress2").val();
	        var BeneCountry = $("#BeneCountry").val();*/
	        var BeneMobileNo = $("#BeneMobileNo").val();
	        /*var BeneBankBranch = $("#BeneBankBranch").val();
	        var SwiftCode = $("#SwiftCode").val();*/
	        if($('#beneficiaryactive'). prop("checked") == true){
	            var beneficiaryactive = 'Active';
	        } else {
	            var beneficiaryactive = 'Inactive';
	        }
	        var doe = $("#doe").val();
	        /*if(delbeneficiaryclsid!=undefined) {
	            var bindex = parseInt(delbeneficiaryclsid) + 1;
	            $('#delbeneficiaryclsid').val(bindex);
	        } else {
	            var bindex = b;
	        }*/
	        /*if(custcode == '' || CurrencyCode == '' || CustNRICNO == '' || BeneName == '' || BeneMobileNo == ''  || BeneBankName == '' || BeneBankAccNo == '' || BeneAddress1 == '' || BeneAddress2 == '' || BeneCountry == '' || BeneMobileNo == '' || BeneBankBranch == '' || SwiftCode == '')*/
	        if(BeneName == '' || BeneMobileNo == '' ){
	            errorbeneficiary();
	            return false;
	        }
	        $.ajax({
                url: "./addBeneficiary",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : $('#addbeneform').serialize(),
                success: function(response){
                    if(response.status == 200){
                        success_toastr(response.content);
                        $('#beneficiary_id').val('');
				        $("#custcode").val('');
				        $('#CurrencyCode').val('');
				        $('#CustNRICNO').val('');
				        $("#BeneName").val('');
				        $("#BeneBankName").val('');
				        $("#BeneBankAccNo").val('');
				        $("#BeneAddress1").val('');
				        $("#BeneAddress2").val('');
				        $("#BeneCountry").val('');
				        $("#BeneMobileNo").val('');
				        $("#BeneBankBranch").val('');
				        $("#SwiftCode").val('');
				        $("#purpose").val('');
	        			$('#add_cust_bene').modal('toggle');
                    } else {
                        danger_toastr(response.content);
                    }
                },      
            });
	    });
    	var b=0;
	    /*$(document).on('click',"#addbeneficiary",function(){
	        b++;
	        var delbeneficiaryclsid = $('#delbeneficiaryclsid').val();
	        var beneficiary_id = $('#beneficiary_id').val();
	        var custcode = $("#custcode").val();
	        var CurrencyCode = $('#CurrencyCode').val();
	        var CustNRICNO = $('#CustNRICNO').val();
	        var BeneName = $("#BeneName").val();
	        var BeneBankName = $("#BeneBankName").val();
	        var BeneBankAccNo = $("#BeneBankAccNo").val();
	        var BeneAddress1 = $("#BeneAddress1").val();
	        var BeneAddress2 = $("#BeneAddress2").val();
	        var BeneCountry = $("#BeneCountry").val();
	        var BeneMobileNo = $("#BeneMobileNo").val();
	        var BeneBankBranch = $("#BeneBankBranch").val();
	        var SwiftCode = $("#SwiftCode").val();
	        var purpose = $("#purpose").val();
	        if($('#beneficiaryactive'). prop("checked") == true){
	            var beneficiaryactive = 'Active';
	        } else {
	            var beneficiaryactive = 'Inactive';
	        }
	        var doe = $("#doe").val();
	        if(delbeneficiaryclsid!=undefined) {
	            var bindex = parseInt(delbeneficiaryclsid) + 1;
	            $('#delbeneficiaryclsid').val(bindex);
	        } else {
	            var bindex = b;
	        }
	        if(beneficiary_id == '' || custcode == '' || CurrencyCode == '' || CustNRICNO == '' || BeneName == '' || BeneBankName == '' || BeneBankAccNo == '' || BeneAddress1 == '' || BeneAddress2 == '' || BeneCountry == '' || BeneMobileNo == '' || BeneBankBranch == '' || SwiftCode == '' || purpose == ''){
	            errorbeneficiary();
	            return false;
	        }
	        var contactdata = "<tr id='bid"+bindex+"' class='beneficiarycls'><td>"+bindex+"</td><td><input type='hidden' value='"+beneficiary_id+"' name='td_beneficiary_id[]'>"+beneficiary_id+"</td><td><input type='hidden' value='"+custcode+"' name='td_custcode[]'>" + custcode + "</td><td><input type='hidden' value='"+CurrencyCode+"' name='td_CurrencyCode[]'>" + CurrencyCode + "</td><td><input type='hidden' value='"+CustNRICNO+"' name='td_CustNRICNO[]'>" + CustNRICNO + "</td><td><input type='hidden' value='"+BeneName+"' name='td_BeneName[]'>" + BeneName + "</td><td><input type='hidden' value='"+BeneBankName+"' name='td_BeneBankName[]'>" + BeneBankName + "</td><td><input type='hidden' value='"+BeneBankAccNo+"' name='td_BeneBankAccNo[]'>" + BeneBankAccNo + "</td><td><input type='hidden' value='"+BeneAddress1+"' name='td_BeneAddress1[]'>" + BeneAddress1 + "</td><td><input type='hidden' value='"+BeneAddress2+"' name='td_BeneAddress2[]'>" + BeneAddress2 + "</td><td><input type='hidden' value='"+BeneCountry+"' name='td_BeneCountry[]'>" + BeneCountry + "</td><td><input type='hidden' value='"+BeneMobileNo+"' name='td_BeneMobileNo[]'>" + BeneMobileNo + "</td><td><input type='hidden' value='"+BeneBankBranch+"' name='td_BeneBankBranch[]'>" + BeneBankBranch + "</td><td><input type='hidden' value='"+SwiftCode+"' name='td_SwiftCode[]'>" + SwiftCode + "</td><td><input type='hidden' value='"+beneficiaryactive+"' name='td_beneficiaryactive[]'>" + beneficiaryactive + "</td><td><input type='hidden' value='"+purpose+"' name='td_purpose[]'>" + purpose + "</td></tr>";
	        $("#beneficiary_table tbody").append(contactdata);
	        $('#beneficiary_id').val('');
	        $("#custcode").val('');
	        $('#CurrencyCode').val('');
	        $('#CustNRICNO').val('');
	        $("#BeneName").val('');
	        $("#BeneBankName").val('');
	        $("#BeneBankAccNo").val('');
	        $("#BeneAddress1").val('');
	        $("#BeneAddress2").val('');
	        $("#BeneCountry").val('');
	        $("#BeneMobileNo").val('');
	        $("#BeneBankBranch").val('');
	        $("#SwiftCode").val('');
	        $("#purpose").val(''); 
	    });*/
		$(document).on("click", ".addCustBeneficiary",function() {
	    	var nric_no = $(this).attr('data-ref');
	    	$('#addnric_no').val(nric_no);
	    	$('#CustNRICNO').val(nric_no);
	    	$.ajax({
                url:"{{ route('getBeneficiary') }}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    nric_no : nric_no,
                },
                success: function(response){
					$("#beneficiary_id").val(response.beneId);
                    if(response.status == 200){
				        var benificiery = response.benificiery;
						$("#beneficiary_table tbody").html(benificiery);
                    } else {
						$("#beneficiary_table tbody").html('');
                    }
                },      
            });
	        $('#add_cust_bene').modal('toggle');
		});
		$(document).on("click", "#cancelform",function() {
			$('#add_image').modal('toggle');
			getdatas();
		});
		$(document).on("click", ".addImage",function() {
	    	var nric_no = $(this).attr('data-ref');

	    	var nricfront = $(this).attr('nricfront');
	    	var nricback = $(this).attr('nricback');
	    	var customerface = $(this).attr('customerface');

	    	var img_nricfront = '<?php echo env('APP_URl')."storage/"; ?>' + nricfront;
	    	var img_nricback = '<?php echo env('APP_URl')."storage/"; ?>'+nricback;
	    	var img_customerface = '<?php echo env('APP_URl')."storage/"; ?>'+customerface;

	    	$('#nric_no').val(nric_no);
	    	if(nricfront != ''){
	    		$('.nricfront').hide();
	    		$('.preview_nricfront').show();
	    		var type = nricfront.split('.');
	    		if(type[1] == 'pdf'){
		    		$('#pdf_nricfront').attr('img',nricfront);
		    		$("#pdf_nricfront").attr("href", img_nricfront);
		    		$('#img_nricfront').hide();
		    		$('#pdf_nricfront').show();
	    		} else {
		    		$('#img_nricfront').attr('img',nricfront);
		    		$("#img_nricfront").attr("src", img_nricfront);
		    		$("#ligthbox_nricfront").attr("src", img_nricfront);
		    		$('#pdf_nricfront').hide();
		    		$('#img_nricfront').show();
	    		}
	    	} else {
	    		$('.nricfront').show();
	    		$('.preview_nricfront').hide();
	    	}
	    	if(nricback != ''){
	    		$('.nricback').hide();
	    		$('.preview_nricback').show();
	    		var type = nricback.split('.');
	    		if(type[1] == 'pdf'){
		    		$('#pdf_nricback').attr('img',nricback);
		    		$("#pdf_nricback").attr("href", img_nricback);
		    		$('#img_nricback').hide();
		    		$('#pdf_nricback').show();
	    		} else {
		    		$('#img_nricback').attr('img',nricback);
		    		$('#img_nricback').attr('src',img_nricback);
		    		$('#ligthbox_nricback').attr('src',img_nricback);
		    		$('#pdf_nricback').hide();
		    		$('#img_nricback').show();
	    		}
	    	} else {
	    		$('.nricback').show();
	    		$('.preview_nricback').hide();
	    	}
	    	if(customerface != ''){
	    		$('.customerface').hide();
	    		$('.preview_customerface').show();
	    		var type = customerface.split('.');
	    		if(type[1] == 'pdf'){
		    		$('#pdf_customerface').attr('img',customerface);
		    		$("#pdf_customerface").attr("href", img_customerface);
		    		$('#img_customerface').hide();
		    		$('#pdf_customerface').show();
	    		} else {
		    		$('#img_customerface').attr('img',customerface);
		    		$('#img_customerface').attr('src',img_customerface);
		    		$('#ligthbox_custface').attr('src',img_customerface);
		    		$('#pdf_customerface').hide();
		    		$('#img_customerface').show();
	    		}
	    	} else {
	    		$('.customerface').show();
	    		$('.preview_customerface').hide();
	    	}
	        $('#add_image').modal('toggle');
		});
		 $('#nricfront').on('change', function() { 
            const size = (this.files[0].size / 1024 / 1024).toFixed(2); 
            if (size > 2 ) { 
                alert("Maximum File Upload Size is 2MB"); 
                $('#nricfront').val('');
            }
            var ext = $('#nricfront').val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['pdf','png','jpg','jpeg']) == -1) {
			    alert('Allowed file format PDF , PNG and JPG !');
                $('#nricfront').val('');
			}
        });
		 $('#nricback').on('change', function() { 
            const size = (this.files[0].size / 1024 / 1024).toFixed(2); 
            if (size > 2 ) { 
                alert("Maximum File Upload Size is 2MB"); 
                $('#nricback').val('');
            }
            var ext = $('#nricback').val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['pdf','png','jpg','jpeg']) == -1) {
			    alert('Allowed file format PDF , PNG and JPG !');
                $('#nricback').val('');
			}
        });
		 $('#customerface').on('change', function() { 
            const size = (this.files[0].size / 1024 / 1024).toFixed(2); 
            if (size > 2 ) { 
                alert("Maximum File Upload Size is 2MB"); 
                $('#customerface').val('');
            }
            var ext = $('#customerface').val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['pdf','png','jpg','jpeg']) == -1) {
			    alert('Allowed file format PDF , PNG and JPG !');
                $('#customerface').val('');
			}
        });
		$(document).on("click", "#remove_nricfront",function() {
	    	var nric_no = $('#nric_no').val();
	    	var img = $('#img_nricfront').attr('img');
	    	var pdf = $('#pdf_nricfront').attr('img');
	    	if(img != undefined) {
	    		delete_file = img;
	    	} else if (pdf != undefined) {
	    		delete_file = pdf;
	    	} else {
	    		delete_file = '';
	    	}
	    	$.ajax({
                url:"{{ route('removeImage') }}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    nric_no : nric_no,
                    field : 'nricfront',
                    img : delete_file,
                },
                success: function(response){
                    if(response.status == 200){
			    		$('.nricfront').show();
			    		$('.preview_nricfront').hide();
                    	success_toastr(response.Msg);
                    } else {
                    	danger_toastr(response.Msg);
                    }
                },      
            });
		});
		$(document).on("click", "#remove_nricback",function() {
	    	var nric_no = $('#nric_no').val();
	    	var img = $('#img_nricback').attr('img');
	    	var pdf = $('#pdf_nricback').attr('img');
	    	var delete_file = '';
	    	if(img != undefined) {
	    		delete_file = img;
	    	} else if (pdf != undefined) {
	    		delete_file = pdf;
	    	}
	    	console.log(delete_file);
	    	$.ajax({
                url:"{{ route('removeImage') }}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    nric_no : nric_no,
                    field : 'nricback',
                    img : delete_file,
                },
                success: function(response){
                    if(response.status == 200){
			    		$('.nricback').show();
			    		$('.preview_nricback').hide();
                    	success_toastr(response.Msg);
                    } else {
                    	danger_toastr(response.Msg);
                    }
                },      
            });
		});
		$(document).on("click", "#remove_customerface",function() {
	    	var nric_no = $('#nric_no').val();
	    	var img = $('#img_customerface').attr('img');
	    	var pdf = $('#pdf_customerface').attr('img');
	    	if(img != undefined) {
	    		delete_file = img;
	    	} else if (pdf != undefined) {
	    		delete_file = pdf;
	    	} else {
	    		delete_file = '';
	    	}
	    	$.ajax({
                url:"{{ route('removeImage') }}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    nric_no : nric_no,
                    field : 'customerface',
                    img : delete_file,
                },
                success: function(response){
                    if(response.status == 200){
			    		$('.customerface').show();
			    		$('.preview_customerface').hide();
                    	success_toastr(response.Msg);
                    } else {
                    	danger_toastr(response.Msg);
                    }
                },      
            });
		});
		$(document).on("click", ".edit_cashcust",function() {
	    	var edit_id = $(this).attr('data-ref');
	    	if(edit_id != ''){
	    		 $.ajax({
		           type:'POST',
		           url:'./cashcustomer/'+edit_id,
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
		           success:function(result){
			           	if(result.status == 200){
			           		data = result.data;
				           	$('input[name="cashcustomer_id"]').val(data.id);
				           	$('input[name="nric_no"]').val(data.PPNo);
				           	$('input[name="cname"]').val(data.Name);
				           	$('#date_of_birth').val(data.DOB);
				           	$('#nationality').val(data.Nationality);
				           	$('#identifier_type').val(data.IdentifierType);
				           	$('input[name="identifier_other"]').val(data.IdentifierTypeOther);
				           	$('#date_of_expiry').val(data.passexpiry);
				           	$('#country').val(data.Address4);
				           	$('input[name="postal_code"]').val(data.PostalCode);
				           	$('input[name="address"]').val(data.Address1);
				           	$('input[name="address2"]').val(data.Address2);
				           	$('input[name="unit"]').val(data.UnitNo);
				           	$('#mc_type').val(data.MCType);
				           	$('input[name="tele_no"]').val(data.PhoneNo);
				           	$('#pending_remarks').val(data.PendingCustomerRemarks);
				           	$('input[name="intelligence"]').val([data.Intelligence]);
				           	$('input[name="allow"]').val([data.Allow]);
				           	$('#country').trigger('change');
				           	if(data.Intelligence == null){
				            	$("#intelligenceNo").prop("checked", true);
				            	$('input[name="intelligence"]').val(['No']);
			            	} 
			            	if(data.Allow == null){
				            	$("#allowNo").prop("checked", true);
				            	$('input[name="allow"]').val(['No']);
			            	}
			            	if(data.Activebit == 1){
				            	$("#active").prop("checked", true);
				            	$("#active").val('1');
			            	} else {
				            	$("#active").prop("checked", false);
				            	$("#active").val('0');          		
			            	}
			            	if(data.PendingDocuments == 1){
				            	$("#pending_documents").prop("checked", true);
				            	$("#pending_documents").val('1');
			            	} else {
				            	$("#pending_documents").prop("checked", false);
				            	$("#pending_documents").val('0');          		
			            	}
			            	if(data.nricfront != '' && data.nricback !='' && data.customerface!=''){
				            	var nricfrontpath = '{{ env('CUSTOMER_IMAGE_PATH') }}' + data.nricfront;
				            	var nricbackpath = '{{ env('CUSTOMER_IMAGE_PATH') }}' + data.nricback;
				            	var photopath = '{{ env('CUSTOMER_IMAGE_PATH') }}' + data.customerface;
				            	$('#nricfront').attr('src', nricfrontpath);
				            	$('#nricfront').on('click', function(event){
				            		event.preventDefault();
				            		window.open($(this).attr('src'));
				            	});
				            	$('#nricback').attr('src', nricbackpath);
				            	$('#nricback').on('click', function(event){
				            		event.preventDefault();
				            		window.open($(this).attr('src'));
				            	});
				            	$('#photo').attr('src', photopath);
				            	$('#photo').on('click', function(event){
				            		event.preventDefault();
				            		window.open($(this).attr('src'));
				            	});
			            		$('#imageRow').show();
			            	}else {
			            		$('#imageRow').hide();
			            	}
			           	}
	           		$('#get_cashcust_list').modal('toggle');
		           }
		        });
	    	}
	    });

	});
	$(document).on("click", function(e) {
	    if ($(e.target).is("#nric_autocomplete") === false) {
			$("#suggesstion-box-nric").html('');
	    }
	    if ($(e.target).is("#name_autocomplete") === false) {
			$("#suggesstion-box-name").html('');
	    }
	    if ($(e.target).is("#phno_autocomplete") === false) {
			$("#suggesstion-box-phno").html('');
	    }
	 });
	function getNricAutoFill(nric) {
		if(nric.length < '{{env('AUTOCOMPLETE_LENGTH')}}'){
        	$("#suggesstion-box-nric").html('');
	        return false;
	    }
		$.ajax({
           type:'POST',
           url:'./getNricNo',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            data : {
                nric : nric,
            },
            beforeSend: function() {},
           success:function(result){
           	$("#suggesstion-box-nric").html(result.str);
           }
       });
	}
	function selectedNric(nric) {
		$('#filterNric').val(nric);
        $("#suggesstion-box-nric").html('');
	}
	function getNameAutoFill(name) {
		if(name.length < '{{env('AUTOCOMPLETE_LENGTH')}}'){
			$("#suggesstion-box-name").html('');
	        return false;
	    }
		$.ajax({
           type:'POST',
           url:'./getNameAutoFill',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            data : {
                name : name,
            },
            beforeSend: function() {},
           success:function(result){
           	$("#suggesstion-box-name").html(result.str);
           }
       });
	}
	function selectedName(name) {
		$('#filterName').val(name);
        $("#suggesstion-box-name").html('');
	}
	function getPhnoAutoFill(phno) {
		if(phno.length < '{{env('AUTOCOMPLETE_LENGTH')}}'){
			$("#suggesstion-box-phno").html('');
	        return false;
	    }
		$.ajax({
           type:'POST',
           url:'./getPhnoAutoFill',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            data : {
                phno : phno,
            },
            beforeSend: function() {},
           success:function(result){
           	$("#suggesstion-box-phno").html(result.str);
           }
       });
	}
	function selectedPhno(phno) {
		$('#filterPhno').val(phno);
        $("#suggesstion-box-phno").html('');
	}
	function submitForm(type) {	
		var filterName = $('#filterName').val();
		var filterNric = $('#filterNric').val();
		var filterAllow = $('#filterAllow').val();
		$('#addfilterPhno').val($('#filterPhno').val());
		$('#addfilterName').val(filterName);
		$('#addfilterNric').val(filterNric);
		$('#addfilterAllow').val(filterAllow);
		getdatas();
	}

	function getdatas() {
		$('#table_cashcustomerlist').DataTable({
			paging:   true,
			destroy: true,
			autoWidth : false,
			info:     false,
			searching:false,
	        ordering: false,
			ajax: {
				url: "{{route('cashcustomer_list')}}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: function (d) {
					d.filterName = $('input[name="filterName"]').val();
					d.filterNric = $('input[name="filterNric"]').val();
					d.filterPhno = $('input[name="filterPhno"]').val();
					d.filterAllow = $('#filterAllow').val();
				},
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', title:'S.No.'},
				{data: 'CustCode', name: 'CustCode', title:'CustCode'},
				{data: 'Name', name: 'Name', title:'Name'},
				{data: 'PhoneNo', name: 'PhoneNo', title:'Phone'},
				{data: 'DOB', name: 'DOB', title:'DOB'},
				{data: 'PPNo', name: 'PPNo', title:'NRICNo'},
				{data: 'Intelligence', name: 'Intelligence', title:'Due Intelligence'},
				{data: 'Allow', name: 'Allow', title:'Allow Online'},
				{data: 'Action', name: 'Action', title:'Action'},
			],
		});
	}

function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
var dateToday = new Date();
$( function() {
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 15);
	$( "#date_of_expiry").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		minDate: dateToday,
	});
	$( "#date_of_birth").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
        yearRange: start.getFullYear() + ':' + end.getFullYear(),
    	maxDate: dateToday
	});
});
function errorbeneficiary(){
    var beneficiary_id = $('#beneficiary_id').val();
    var custcode = $("#custcode").val();
    var CurrencyCode = $('#CurrencyCode').val();
    var CustNRICNO = $('#CustNRICNO').val();
    var BeneName = $("#BeneName").val();
    var BeneBankName = $("#BeneBankName").val();
    var BeneBankAccNo = $("#BeneBankAccNo").val();
    var BeneAddress1 = $("#BeneAddress1").val();
    var BeneAddress2 = $("#BeneAddress2").val();
    var BeneCountry = $("#BeneCountry").val();
    var BeneMobileNo = $("#BeneMobileNo").val();
    var BeneBankBranch = $("#BeneBankBranch").val();
    var SwiftCode = $("#SwiftCode").val();
    /*var purpose = $("#purpose").val();*/
    /*if(beneficiary_id == '') {
        $('#beneficiary_id').css('border', '1px solid #f52604'); 
    } else {
        $('#beneficiary_id').css('border', '1px solid #ced4da'); 
    }
    if(custcode == '') {
        $('#custcode').css('border', '1px solid #f52604'); 
    } else {
        $('#custcode').css('border', '1px solid #ced4da'); 
    }
    if(CurrencyCode == '') {
        $("#CurrencyCode").css('border', '1px solid #f52604'); 
    } else {
        $("#CurrencyCode").css('border', '1px solid #ced4da'); 
    }
    if(CustNRICNO == '') {
        $("#CustNRICNO").css('border', '1px solid #f52604'); 
    } else {
        $("#CustNRICNO").css('border', '1px solid #ced4da'); 
    }*/
    if(BeneName == '') {
        $("#BeneName").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneName").css('border', '1px solid #ced4da'); 
    }
    /*if(BeneBankName == '') {
        $("#BeneBankName").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneBankName").css('border', '1px solid #ced4da'); 
    }
    if(BeneBankAccNo == '') {
        $("#BeneBankAccNo").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneBankAccNo").css('border', '1px solid #ced4da'); 
    }    
    if(BeneAddress1 == '') {
        $("#BeneAddress1").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneAddress1").css('border', '1px solid #ced4da'); 
    }     
    if(BeneAddress2 == '') {
        $("#BeneAddress2").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneAddress2").css('border', '1px solid #ced4da'); 
    }    
    if(BeneCountry == '') {
        $("#BeneCountry").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneCountry").css('border', '1px solid #ced4da'); 
    } */   
    if(BeneMobileNo == '') {
        $("#BeneMobileNo").css('border', '1px solid #f52604'); 
    } else {
        $("#BeneMobileNo").css('border', '1px solid #ced4da'); 
    }    
    /*if(SwiftCode == '') {
        $("#SwiftCode").css('border', '1px solid #f52604'); 
    } else {
        $("#SwiftCode").css('border', '1px solid #ced4da'); 
    }*/
}
function openModal() {
	$('#lightbox_image').modal('toggle');
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var captionText = document.getElementById("lightbox_caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "block";
}
</script>
@endsection