@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('page-css')
	<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
	<div class="breadcrumb">
		<h1>Cash Customer Master</h1>
		<ul>
			<li>Create</li>
		    <li><a href="{{ route('cashcustomer_list') }}">Listing</a></li>
		</ul>
	</div>
	<div class="separator-breadcrumb border-top"></div>

	<form class="needs-validation" novalidate name="cash_customer" id="frm_cash_customer" method="POST" action="#" autocomplete="off">
		<div class="row mb-12 master-main">
		    <div class="col-md-12 mb-12">
		       <div class="card">
		           <div class="card-body">
		               <div class="row">
		                    <div class="col-lg-6 col-sm-12 master-main-sec">
		                        <div class="card mb-6">
		                            <div class="card-body">
			                            <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="cust_code" class="col-sm-3 col-form-label">
		                                            	Cust Code
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <input type="textbox" name="cust_code" class="form-control" autocomplete="off" id="cust_code" value="CASH"  placeholder="Enter Customer code" readonly>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
			                            <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="nric_no" class="col-sm-3 col-form-label">
		                                            	<input type="hidden" name="cashcustomer_id" id="cashcustomer_id">
		                                            	<a onclick="GetCashCustomerModal('cashcustomer');" data-toggle="modal" data-target="#get_cashcustomer_modal_details" href="">
		                                            		NRIC No<span class="mandatory">*</span>
		                                            	</a>
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <input type="textbox" name="nric_no" class="form-control" autocomplete="off" id="nric_no" placeholder="Enter NRIC No" required>
                                                            <div class="invalid-feedback"> 
                                                                Please Enter NRIC No
                                                            </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
										<div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="cname" class="col-sm-3 col-form-label">
		                                            	Name<span class="mandatory">*</span>
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <input type="textbox" name="cname" class="form-control" autocomplete="off" id="cname" placeholder="Enter Name" onkeyup="this.value = this.value.toUpperCase();" required>
                                                            <div class="invalid-feedback"> 
                                                                Please Enter Name
                                                            </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
			                            <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="date_of_birth" class="col-sm-3 col-form-label">
		                                            	Date of Birth<span class="mandatory">*</span>
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <input type="text" name="date_of_birth" class="form-control" autocomplete="off" id="date_of_birth" placeholder="dd-mm-yyyy" required>
                                                            <div class="invalid-feedback"> 
                                                                Please Enter Date of Birth
                                                            </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="nationality" class="col-sm-3 col-form-label">Nationality<span class="mandatory">*</span></label>
		                                            <div class="col-sm-9">
		                                                <select class="form-control" name="nationality" id="nationality" required>
		                                                    <option>Select Nationality</option>
		                                                    @foreach($nationality as $value)
		                                                    	<option value="{{trim($value->Code,' ')}}">{{trim($value->Description,' ')}}</option>
		                                                    @endforeach
		                                                </select>		                                                
		                                                <div class="invalid-feedback">Select Nationality</div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="identifier_type" class="col-sm-3 col-form-label">Identifier Type<span class="mandatory">*</span></label>
		                                            <div class="col-sm-9">
		                                                <select class="form-control" name="identifier_type" id="identifier_type" required>
		                                                    <option>Select Identifier Type</option>
		                                                    @foreach($identifire as $value)
		                                                    	<option value="{{$value}}">{{$value}}</option>
		                                                    @endforeach
		                                                </select>
		                                                <div class="invalid-feedback"> 
                                                            Please select Identifire Type
                                                        </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="form-group row" id="identifier_other_row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="identifier_other" class="col-sm-3 col-form-label">
		                                            	
		                                            </label>
		                                            <div class="col-sm-9">
		                                        		<input type="text" name="identifier_other" id="identifier_other" class="form-control">
		                                        		<div class="invalid-feedback"> 
                                                            Please Enter Identifire Type
                                                        </div>
		                                        	</div>
		                                        </div>
		                                    </div>
		                                </div>
			                            <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="date_of_expiry" class="col-sm-3 col-form-label">
		                                            	Date of Expiry<span class="mandatory">*</span>
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <input type="text" name="date_of_expiry" class="form-control" autocomplete="off" id="date_of_expiry" placeholder="dd-mm-yyyy" required>
                                                            <div class="invalid-feedback"> 
                                                                Please Enter Date of Expiry
                                                            </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
			                            <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="mc_type" class="col-sm-3 col-form-label">
		                                            	MC Type <span class="mandatory">*</span>
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <select class="form-control" name="mc_type" id="mc_type" required>
                                                            <option value="">Select MC Type</option> 
                                                            @foreach ($general_type as $key => $value)
                                                            	<option value="{{$value}}" @if(old("mc_type")==$value) selected="selected" @endif>{{$value}}</option> 
                                                            @endforeach                                                              
                                                        </select>
                                                        <div class="invalid-feedback"> 
                                                            Please Select MC Type
                                                        </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                	<div class="col-lg-12">
		                                		<div class="row">
		                                			<label for="pending_documents" class="col-sm-3 col-form-label">Pending Documents</label>
		                                			<div class="col-sm-9 mt-2">
	 													<label class="checkbox checkbox-primary">
	                                                        <input type="checkbox" id="pending_documents" name="pending_documents" value="0">
	                                                        <span class="checkmark"></span>
	                                                    </label>
		                                			 </div>
		                                		</div>
		                                	</div>
		                                </div>
		                                <div class="form-group row">
		                                	<div class="col-lg-12">
		                                		<div class="row">
		                                			<label for="active" class="col-sm-3 col-form-label">Active</label>
		                                			<div class="col-sm-9 mt-2">
	 													<label class="checkbox checkbox-primary">
	                                                        <input type="checkbox" id="active" name="active" checked="checked" value="1">
	                                                        <span class="checkmark"></span>
	                                                    </label>
		                                			 </div>
		                                		</div>
		                                	</div>
		                                </div>
		                            </div>
		                        </div>
		                    </div>

		                    <div class="col-lg-6 col-sm-12 master-main-sec">
		                        <div class="card mb-6">
		                            <div class="card-body">
										<div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="country" class="col-sm-3 col-form-label">
		                                            	Country <span class="mandatory">*</span>
		                                            </label>
		                                            <div class="col-sm-9">
		                                            	<select class="form-control" name="country" id="country" required>
					                                        <option value="">Select Country</option>
					                                        @foreach($country as $value)
					                                        	<option value="{{trim($value->Country, ' ')}}">{{trim($value->Country, ' ')}}</option>
					                                        @endforeach
					                                    </select>
					                                    <div class="invalid-feedback">Please Select Country</div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                            	<div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="postal_code" class="col-sm-3 col-form-label">
		                                            	Postal Code <span class="mandatory">*</span>
		                                            </label>
		                                            <div class="col-sm-9 input-group mb-3">
		                                                <input type="textbox" name="postal_code" class="form-control allownumeric" autocomplete="off" id="postal_code" placeholder="Enter Postal Code" required>
                                                            <div class="invalid-feedback"> 
                                                                Please Enter Postal Code
                                                            </div>
                                                        <div class="input-group-append" id="postalcodeserach" style="cursor: pointer;">
                                                            <span class="input-group-text" id="basic-addon1">
                                                                <i class="i-Business-Mens"></i>
                                                            </span>
                                                        </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="address" class="col-sm-3 col-form-label">
		                                            	Address <span class="mandatory">*</span>
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <input type="textbox" name="address" class="form-control" autocomplete="off" id="address" placeholder="Enter Address" required>
                                                            <div class="invalid-feedback"> 
                                                                Please Enter Address
                                                            </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="address2" class="col-sm-3 col-form-label">
		                                            	Address 2
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <input type="textbox" name="address2" class="form-control" autocomplete="off" id="address2" placeholder="Enter Address2">
                                                            <div class="invalid-feedback"> 
                                                                Please Enter Address
                                                            </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
			                            <div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="unit" class="col-sm-3 col-form-label">
		                                            	Unit
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <input type="textbox" name="unit" class="form-control" autocomplete="off" id="unit" placeholder="Enter Unit">
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
			                            
	 									<div class="form-group row">
		                                    <div class="col-lg-12">
		                                        <div class="row">
		                                            <label for="tele_no" class="col-sm-3 col-form-label">
		                                            	Telephone No <span class="mandatory">*</span>
		                                            </label>
		                                            <div class="col-sm-9">
		                                                <input type="textbox" name="tele_no" class="form-control allownumeric" autocomplete="off" id="tele_no" placeholder="Enter Telephone No" required>
                                                            <div class="invalid-feedback"> 
                                                                Please Enter Telephone No
                                                            </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
										<div class="form-group row">
										    <div class="col-lg-12">
										        <div class="row">
										        	<label for="pending_remarks" class="col-sm-3 col-form-label">
		                                            	Remarks
		                                            </label>
										            <div class="col-sm-9">
										                <textarea rows="4" class="form-control pending_remarks"  id="pending_remarks"  name="pending_remarks" placeholder="Enter Remarks" style="height: 73px !important;"></textarea>
										            </div>
										        </div>
										    </div>
										</div>
	                                    <div class="form-group row">
	                                        <div class="col-lg-12">
	                                            <div class="row" id="dueIntelligence" >
	                                                <label for="intelligence" class="col-sm-3 col-form-label">
	                                                    Due Intelligence
	                                                </label>
	                                                <div class="col-sm-9">
	                                                    <input type="radio" id="intelligenceYes" name="intelligence" value="Yes" >
	                                                    <label for="Yes">Yes</label>
	                                                    <input type="radio" id="intelligenceNo" name="intelligence" value="No" style="margin-left: 20px" checked>
	                                                    <label for="No">No</label>
	                                                    <div class="invalid-feedback"> 
	                                                        Please Select Due Intelligence
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="form-group row">
	                                        <div class="col-lg-12">
	                                            <div class="row" id="allowOnline">
	                                                <label for="allow" class="col-sm-3 col-form-label">
	                                                    Allow Online
	                                                </label>
	                                                <div class="col-sm-9">
	                                                    <input type="radio" id="allowYes" name="allow" value="Yes" >
	                                                    <label for="Yes">Yes</label>
	                                                    <input type="radio" id="allowNo" name="allow" value="No" style="margin-left: 20px" checked>
	                                                    <label for="No">No</label>
	                                                    <div class="invalid-feedback"> 
	                                                        Please Select Allow Online
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
		                            </div>
		                        </div>
		                    </div>
		               	</div>
		               	<div class="row">
		               		<div class="col-lg-12 col-sm-12 master-main-sec text-right mt-3">
				                <button class="btn btn-primary m-1" type="button" onclick="submitForm('save');" id="bt_save">Save (F7)</button>
				                <button class="btn btn-primary m-1" type="button" onclick="submitForm('delete');" id="bt_delete">Delete (F8)</button>
				               	<button class="btn btn-primary m-1" type="button" onclick="submitForm('clear');" id="bt_clear">Clear (F5)</button>
				                <button class="btn btn-primary m-1" type="button" onclick="submitForm('esc');" id="bt_close">Close (Esc)</button>
		               		</div>
		               	</div>
		           </div>
		       </div> 
		    </div>
		</div>
		
	</form>
@include('modal.cashcustomer')
@include('modal.cashcustomeredit')
@endsection

@section('page-js')
<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/js/modal.details.js')}}"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
$(document).ready(function(){
	$('#country').val("SINGAPORE");
    $('#tele_no').attr('maxlength',"8");
    $('#postalcodeserach').hide();
    $('#identifier_other_row').hide();
    /*$('#dueIntelligence').hide();
    $('#allowOnline').hide();*/
	$('#identifier_other').val('');
	var dformat = '{{ env('DATE_FORMAT') }}';

    $("#date_of_birth, #date_of_expiry").inputmask("dd-mm-yyyy", {
        separator: "-",
        alias: "dd-mm-yyyy",
        placeholder: "dd-mm-yyyy"
    });
    $('input[name="postal_code"]').blur(function(){
        if($(this).val().length >= 4 && $('#country').val() == "SINGAPORE"){
            $('#postalcodeserach').show();
        } else {
            $('#postalcodeserach').hide();
        }
    });
    $('#country').on('change', function() {
    	if($('#postalcodeserach').val().length >= 4 && $('#country').val() == "SINGAPORE"){
            $('#postalcodeserach').show();
    		$('#tele_no').attr('maxlength',"8");
        } else if($('#country').val() == "SINGAPORE"){
    		$('#tele_no').attr('maxlength',"8");
        } else {
            $('#postalcodeserach').hide();
    		$('#tele_no').removeAttr('maxlength');
        }
    });
    $('#identifier_type').on('change', function() {
		$('#identifier_other').val('');
        $('#date_of_expiry').val('');
	    if($('#identifier_type').val() == "Others"){
            $('#identifier_other_row').show();
            $('#identifier_other').prop('required',true);
        } else if($('#identifier_type').val() == "Singapore Citizen" || $('#identifier_type').val() == "Permanent Residence"){
        	var date = new Date();
		    var mnt = date.getMonth()+1;
		    var yrs = date.getFullYear()+30;
		    var cdate = date.getDate()+'-'+mnt+'-'+yrs;
		    $('#date_of_expiry').val(cdate);
            $('#identifier_other_row').hide();
            $('#identifier_other').prop('required',false);
        } else {
            $('#identifier_other_row').hide();
            $('#identifier_other').prop('required',false);
        }
    });

    $(document).on('click', '#postalcodeserach', function(){
        var postal = $('input[name="postal_code"]').val();
        if(postal.length >= 4){
            $.ajax({
                url: "./postalcode",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    postal : postal,
                },
                beforeSend: function() {},
                success: function(response){
                    if(response.status == 200 && response.postal != 'Error'){
                        console.log(response.postal);
                        var mix = response.postal.block+' '+response.postal.Road;
                        $('input[name="address"]').val(mix);
                        $('input[name="address2"]').val(response.postal.buliding);
                    } else {
                        console.log(response.postal);
                        $('input[name="address"]').val('');
                        $('input[name="address2"]').val('');
                        $.alert("Enter Valid postal code");
                    }
                }
            });
        } else {
            $('input[name="company_address1"]').val('');
            $('input[name="company_address2"]').val('');
        }
    });
    $(document).on("click", "input[name='cashcustomer_id_detail']",function() {
        $('#get_cashcustomer_modal_details').modal('toggle');
        $('input[name="nric_no"]').val($(this).val());
        getCashCustomerDetails();
    });
    $("input[name='nric_no']").blur(function() {
        getCashCustomerDetails();
    });

	$(document).on('change', '#active', function() {
    	if ($(this).is(':checked')) {
    		$(this).attr('value', '1');
    	}else {
    		$(this).attr('value', '0');
    	}
	});
	$(document).on('change', '#pending_documents', function() {
    	if ($(this).is(':checked')) {
    		$(this).attr('value', '1');
    	}else {
    		$(this).attr('value', '0');
    	}
	});
    $(document).on("change", "input[name='nric_no']",function() {
      	$('input[name="nric_no"]').css('border', '1px solid #ced4da');
    });
	$(document).on("change", "input[name='cname']",function() {
		$('input[name="cname"]').css('border-color','#ced4da');
    });
	$(document).on("change", "#date_of_birth",function() {
		$('#date_of_birth').css('border-color','#ced4da');
    });
	$(document).on("change", "#date_of_expiry",function() {
		$('#date_of_expiry').css('border-color','#ced4da');
    });
});

function getCashCustomerDetails() {
	var id = $("input[name='nric_no']").val();
	$.ajax({
        url: "./cashcustomer/getcashcustomerdetails",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            id : id,
        },
        beforeSend: function() {},
        success: function(response){
            if(response.status == 200){
            	$('input[name="cashcustomer_id"]').val(response.data.id);
            	$('input[name="cname"]').val(response.data.Name);
            	$('input[name="unit"]').val(response.data.UnitNo);
            	$('input[name="tele_no"]').val(response.data.PhoneNo);
            	$('#pending_remarks').val(response.data.PendingCustomerRemarks);
            	$('input[name="address"]').val(response.data.Address1);
            	$('input[name="address2"]').val(response.data.Address2);
            	$('input[name="postal_code"]').val(response.data.PostalCode);
            	$('#country').val(response.data.Address4);
            	$('#nationality').val(response.data.Nationality);
            	$('#identifier_type').val(response.data.IdentifierType);
            	$('#identifier_other').val(response.data.IdentifierTypeOther);
            	$('#country').trigger('change');
            	if(response.data.MCType == 'Local Individual' || response.data.MCType == 'Overseas Individual'){
            		$('#mc_type').val(response.data.MCType);
            	} else {
            		$('#mc_type').val('');
            	}
            	if((response.data.Intelligence == '' || response.data.Intelligence == null) && ( response.data.Allow == null || response.data.Allow == '')){
	            	$('input[name="intelligence"]').val(['No']);
		           	$('input[name="allow"]').val(['No']);
				    /*$('#dueIntelligence').hide();
    				$('#allowOnline').hide();*/
            	} else {
				    /*$('#dueIntelligence').show();
				    $('#allowOnline').show();*/
		           	$('input[name="intelligence"]').val([response.data.Intelligence]);
		           	$('input[name="allow"]').val([response.data.Allow]);
            	}
            	if($('#identifier_type').val() == "Others"){
		            $('#identifier_other_row').show();
		            $('#identifier_other').prop('required',true);
		        } else {
		            $('#identifier_other_row').hide();
		            $('#identifier_other').prop('required',false);
		        }

            	if(response.data.Activebit == 1){
	            	$("#active").prop("checked", true);
	            	$("#active").val('1');
            	} else {
	            	$("#active").prop("checked", false);
	            	$("#active").val('0');          		
            	}
            	if(response.data.PendingDocuments == 1){
	            	$("#pending_documents").prop("checked", true);
	            	$("#pending_documents").val('1');
            	} else {
	            	$("#pending_documents").prop("checked", false);
	            	$("#pending_documents").val('0');          		
            	}
                $('#date_of_birth').val(response.data.DOB);
                $('#date_of_expiry').val(response.data.passexpiry);
            } else {
            	$('input[name="cashcustomer_id"]').val('');
            	$('input[name="cname"]').val('');
            	$('input[name="unit"]').val('');
            	$('input[name="tele_no"]').val('');
            	$('#pending_remarks').val('');
            	$('input[name="address"]').val('');
            	$('input[name="address2"]').val('');
            	$('input[name="postal_code"]').val('');
            	$('input[name="country"]').val('');
            	$('#nationality').val('Select Nationality');
            	$('#identifier_type').val('Select Identifier Type');
            	$("#active").prop("checked", false);
            	$("#active").val('0');
            	$("#pending_documents").prop("checked", false);
            	$("#pending_documents").val('0');
            	$('#date_of_birth').val('');
            	$('#date_of_expiry').val('');
            	$('#imageRow').hide();
            	$('input[name="intelligence"]').val(['No']);
	           	$('input[name="allow"]').val(['No']);
			    /*$('#dueIntelligence').hide();
				$('#allowOnline').hide();*/
            }
        },      
    });
}

function submitForm(type) {
	if(type == 'save'){
		var nric_no = $('input[name="nric_no"]').val();
		var cname = $('input[name="cname"]').val();
		var date_of_birth= $('input[name="date_of_birth"]').val();
		var address = $('input[name="address"]').val();
		var country = $('input[name="country"]').val();
		var postal_code = $('input[name="postal_code"]').val();
		var tele_no = $('input[name="tele_no"]').val();
        var mc_type = $('#mc_type').val();
        var identifier_type = $('#identifier_type').val();
        var identifier_other = $('#identifier_other').val();
		if(nric_no == '' ||cname  == '' ||date_of_birth  == '' ||date_of_expiry  == '' || address == '' || country =='' || postal_code == '' || tele_no == '' || nationality == '' || mc_type == '' || identifier_type == '' || (identifier_type == 'Others' && identifier_other == '') ){
			$('#frm_cash_customer').addClass('was-validated');
			return false;
		}
		if($('#cashcustomer_id').val() == ''){
        	type = 'save';
        	id = '';
        } else {
        	type = 'update';
        	id = $('#cashcustomer_id').val();
        }
		$.ajax({
			url: "./cashcustomer/insert_update",
			type: "POST",
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
		    data : $('#frm_cash_customer').serialize() +'&type='+type,
			success: function(response){
				if(response.status == 200){
					submitForm('clear');
					success_toastr(response.content);
				}else {
					$.each(response.content, function(v, t){
		            	danger_toastr(t);
		            });
				}
			},
		});
	} else if(type == 'delete'){
		var id = $('#cashcustomer_id').val();
		if(id == ''){
			return false;
		}
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to delete the Nationality?',
			buttons: {
				confirm: function () {
					$.ajax({
						url: "./cashcustomer/delete",
						type: "POST",
					    headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
					    data : {
					    	id : id,
					    },
						success: function(response){
							if(response.status == 200){
								submitForm('clear');
								success_toastr(response.content);
							}else {
								$.each(response.content, function(v, t){
					            	danger_toastr(t);
					            });
							}
						},
					});
				},
				 cancel: function () {},
			}
		});
	} else if(type == 'clear'){
		$('input[name="cashcustomer_id"]').val('');
		$('input[name="nric_no"]').val('');
		$('input[name="cname"]').val('');
		$('#date_of_birth').val('');
		$('input[name="date_of_expiry"]').val('');
		$('#nationality').val('Select Nationality');
		$('#identifier_type').val('Select Identifier Type');
		$('#mc_type').val('');
		$('#identifier_other').val('');
		$('#date_of_expiry').val('');
		$('input[name="address"]').val('');
		$('input[name="address2"]').val('');
		$('input[name="unit"]').val('');
		$('input[name="country"]').val('');
		$('input[name="postal_code"]').val('');
		$('input[name="tele_no"]').val('');
		$('#pending_remarks').val('');
    	$('#identifier_other').hide();
		$("#active").val(0);
		$("#active").prop("checked", false);
		$("#pending_documents").val(0);
		$("#pending_documents").prop("checked", false);
    	$('input[name="intelligence"]').val(['No']);
       	$('input[name="allow"]').val(['No']);
	} else if(type == 'esc'){
		window.location.href = 'dashboard';
	}
}

function cashcustomer_error() {
	if($('input[name="nric_no"]').val() == ''){
		$('input[name="nric_no"]').css('border-color','#f44336');
    }
	if($('input[name="cname"]').val() == ''){
		$('input[name="cname"]').css('border-color','#f44336');
    }
	if($('#date_of_birth').val() == ''){
		$('#date_of_birth').css('border-color','#f44336');
    }
	if($('#date_of_expiry').val() == ''){
		$('#date_of_expiry').css('border-color','#f44336');
    }
}
function success_toastr(msg) {
    toastr.success(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
function danger_toastr(msg) {
    toastr.error(msg,{
        "positionClass": "toast-top-right",
        timeOut: 5000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    })
}
var dateToday = new Date();
$( function() {
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 15);
	$( "#date_of_expiry").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		minDate: dateToday,
	});
	$( "#date_of_birth").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
        yearRange: start.getFullYear() + ':' + end.getFullYear(),
    	maxDate: dateToday
	});
});
</script>
@endsection