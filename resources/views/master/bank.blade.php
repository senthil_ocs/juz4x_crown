@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<style type="text/css">
table#table_bank > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Bank Master</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			    <li>Create</li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>
		<div class="row mb-12 master-main">
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Listing</div>
                        <div class="table-responsive">
                            <table id="table_bank" class="display table table-striped table-bordered" style="width:100%">
                            </table>    
                        </div>
					</div>	
				</div>
			</div>	
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Create</div>
						
						<form id="frm_bank" name="frm_bank" class="needs-validation"  novalidate action="#" method="post">
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="Name" class="col-sm-4 col-form-label">Name<span class="mandatory">*</span></label>
							            <div class="col-sm-8">
							            	<input type="hidden" name="bank_id" id="bank_id">
							                <input type="textbox" class="form-control" name="bank" id="Name" placeholder="Enter Bank Name" required value="{{ old('bank') }}">
		                                    <div class="invalid-feedback code_error">
		                                        Please Enter Bank Name
		                                    </div>
							            </div>	
							        </div>
							    </div>
							</div>

							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="description" class="col-sm-4 col-form-label">Description
							            	<!-- <span class="mandatory">*</span> -->
							            </label>
							            <div class="col-sm-8">
							                <textarea class="form-control description"  id="description"  name="description" placeholder="Enter Description"> {{ old('description') }}</textarea>
											<!-- <div class="invalid-feedback code_error">
											    Please enter description
											</div> -->
							            </div>
							        </div>
							    </div>
							</div>	

							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyDate</label>
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyDate" class=" col-form-label"></label>
										</div>
									</div>
								</div>	
							</div>

							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyUser</label>	
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyUser" class=" col-form-label"></label>	
										</div>									
									</div>	
								</div>	
							</div>	

	                		<div class="card mb-5">
	                            <div class="card-body">
	                                <div class="row row-xs">
	                                    <div class="col-lg-12 text-center">

	                                        <button class="btn btn-primary m-1" onclick="submitForm('new');" type="button">New (F2)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('save');" type="button" >Save (F3)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('delete');"type="button">Delete (F4)</button>

	                                        <button type="button" class="btn btn-primary m-1" onclick="submitForm('esc');" data-style="expand-left">Exit (Esc)</button>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
                    	</form>
					</div>	
				</div>
			</div>
		</div>	
    @endsection
    
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
    $(document).ready(function(){
    	var scroll = '{{ env('TABLESCROLL') }}';
		$('#table_bank').DataTable({
			paging:   false,
			destroy: true,
			processing: true,
			serverSide: true,
			autoWidth : false,
			info:     false,
			scrollY: scroll,
			scrollX: true,
			ajax: {
				url: "{{route('bank.view')}}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex',title:'Sno'},
				{data: 'Bank', name: 'Bank',title:'Bank'},
				{data: 'Description', name: 'Description',title:'Description'},
			],
		});

		$(document).on("click", "#table_bank tbody tr",function() {
			$.ajax({
	    		url: "{{route('bank.getdetails')}}",
	    		type: "POST",
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					id : $(this).attr('id'),
				},
				beforeSend: function() {},
				success: function(response){
					if(response.status == 200){
						$('#bank_id').val(response.details.id);
						$('input[name="bank"]').val(response.details.Bank);
						$('.description').val(response.details.Description);
						$('.Modify_hidden').show();
						$('#ModifyDate').html(response.details.ModifyDate);
						$('#ModifyUser').html(response.details.ModifyUser);
					}	
				},
				error: function(){}
	    	});
		});
    });

    function submitForm(submit_type){
    	var ntable = $('#table_bank').dataTable();
    	if(submit_type == 'new'){
			$('#bank_id').val('');
			$('input[name="bank"]').val('');
			$('.description').val('');
			$('#ModifyDate').html('');
			$('#ModifyUser').html('');
			$('.Modify_hidden').hide();   
			$('#frm_bank').removeClass('was-validated');
    	} else if (submit_type == 'save') {
    		if($('input[name="bank"]').val() == ''){
    			$('#frm_bank').addClass('was-validated');
    			return false;
    		}
    		var type = '';
    		if($('#bank_id').val() == ''){
            	type = 'save';
            	id = '';
            } else {
            	type = 'update';
            	id = $('#bank_id').val();
            }
			$.ajax({
			    url: "{{route('bank.insert_update')}}",
			    type: "POST",
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    data : {
			    	type  : type,
			    	id    : id,
			    	bank  : $('input[name="bank"]').val(),
			    	description : $('.description').val(),
			    },
			    success: function(response){
			        if(response.status == 200){
			            submitForm('new');
			            success_toastr(response.content);
						ntable.fnDraw(false);
			        } else {
			            if(response.content == 'Bank Name already Exists!'){
			            	danger_toastr(response.content);
			            } else {
				            /*submitForm('new');*/
				            $.each(response.content, function(v, t){
				            	danger_toastr(t);
				            });
				            ntable.fnDraw(false);
				        }
			        }
			    },      
			});
    	} else if (submit_type == 'delete') {
        	if($('#bank_id').val() == ''){
                return false;
            }
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete?',
                buttons: {
                    confirm: function () {
						$.ajax({
						    url: "{{route('bank.delete')}}",
						    type: "POST",
						    headers: {
						        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						    },
						    data : {
						    	id : $('#bank_id').val(),
						    },
						    success: function(response){
						        if(response.status == 200){
						            submitForm('new');
						            success_toastr(response.content);
						            ntable.fnDraw(false);
						        } else {
						            submitForm('new');
						            danger_toastr(response.content);
						            ntable.fnDraw(false);
						        }
						    },      
						});
                    },
                    cancel: function () {
                    }
                }
            });
    	} else if(submit_type == 'esc'){
    		window.location.href = 'dashboard'; 
    	}
    }

	function success_toastr(msg) {
	    toastr.success(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}
	function danger_toastr(msg) {
	    toastr.error(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}

	hotkeys('f2', function(event, handler){
	  event.preventDefault();
	    submitForm('new');
	});
	hotkeys('f3', function(event, handler){
	  event.preventDefault();
	    submitForm('save');
	});
	hotkeys('f4', function(event, handler){
	  event.preventDefault();
	    submitForm('delete');
	});

</script>
@endsection