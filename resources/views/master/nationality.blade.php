@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Nationality</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			    <li>Create</li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>
		<div class="row mb-12 master-main">
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Listing</div>
                        <div class="table-responsive">
                            <table id="table_nationality" class="display table table-striped table-bordered" style="width:100%">
                            </table>    
                        </div>
					</div>	
				</div>
			</div>	
			<div class="col-lg-6 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">
						<div class="card-title">Create</div>
						
						<!-- <form id="frm_nationality" name="frm_nationality" class="needs-validation"  novalidate action="#" method="post"> -->
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="Code" class="col-sm-4 col-form-label">Code<span class="mandatory">*</span></label>
							            <div class="col-sm-8">
							            	<input type="hidden" name="nationality_id" id="nationality_id">
							                <input type="textbox" class="form-control" name="Code" id="Code" placeholder="Enter Code" required value="{{ old('code') }}" onkeyup="this.value = this.value.toUpperCase();">
		                                    <div class="invalid-feedback code_error">
		                                        Please enter code
		                                    </div>
							            </div>	
							        </div>
							    </div>
							</div>
							<div class="form-group row">
							    <div class="col-lg-12">
							        <div class="row">
							            <label for="description" class="col-sm-4 col-form-label">Description<span class="mandatory">*</span></label>
							            <div class="col-sm-8">
							                <textarea class="form-control description"  id="description"  name="description" placeholder="Enter Description" required> {{ old('description') }}</textarea>
											<div class="invalid-feedback description_error">
											    Please enter description
											</div>
							            </div>
							        </div>
							    </div>
							</div>	

							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyDate</label>
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyDate" class=" col-form-label"></label>
										</div>
									</div>
								</div>	
							</div>	
							<div class="form-group row Modify_hidden">
								<div class="col-lg-12">
									<div class="row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">ModifyUser</label>	
										<div class="col-sm-8">
											<label for="inputEmail3" id="ModifyUser" class=" col-form-label"></label>	
										</div>									
									</div>	
								</div>	
							</div>	

	                		<div class="card mb-5">
	                            <div class="card-body">
	                                <div class="row row-xs">
	                                    <div class="col-lg-12 text-center">

	                                        <button class="btn btn-primary m-1" onclick="submitForm('new');" type="button">New (F2)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('save');" type="button" >Save (F3)</button>

	                                        <button class="btn  btn-primary m-1" onclick="submitForm('delete');"type="button">Delete (F4)</button>

	                                        <button type="button" class="btn btn-primary m-1" onclick="submitForm('esc');" data-style="expand-left">Exit (Esc)</button>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
                    	<!-- </form> -->
					</div>	
				</div>
			</div>
		</div>	
    @endsection
    
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')
<script type="text/javascript">
    $(document).ready(function(){
    	var scroll = "{{ env('TABLESCROLL') }}";
		$('#table_nationality').DataTable({
			paging:   false,
			destroy: true,
			processing: true,
			serverSide: true,
			autoWidth : false,
			info:     false,
			scrollY: scroll,
			scrollX: true,
			ajax: {
				url: "./nationality",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex',title:'S.No'},
				{data: 'Code', name: 'Code',title:'Code'},
				{data: 'Description', name: 'Description',title:'Description'},
			],
		});

		$(document).on("click", "#table_nationality tbody tr",function() {
			clear_nationality_error();
			$.ajax({
	    		url: "./nationality/getdetails",
	    		type: "POST",
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					id : $(this).attr('id'),
				},
				beforeSend: function() {},
				success: function(response){
					if(response.status == 200){
						$('#nationality_id').val(response.details.id);
						$('input[name="Code"]').val(response.details.Code);
						$('.description').val(response.details.Description);
						$('.Modify_hidden').show();
						$('#ModifyDate').html(response.details.ModifyDate);
						$('#ModifyUser').html(response.details.ModifyUser);
					}	
				},
				error: function(){}
	    	});
		});
    });

    function submitForm(submit_type){
    	var ntable = $('#table_nationality').dataTable();
    	if(submit_type == 'new'){
			$('#nationality_id').val('');
			$('input[name="Code"]').val('');
			$('.description').val('');
			$('#ModifyDate').html('');
			$('#ModifyUser').html('');
			$('.Modify_hidden').hide();   
			clear_nationality_error();
    	} else if (submit_type == 'save') {
    		if($('input[name="Code"]').val() == '' || $('input[name="description"]').val() == '' ){
    			nationality_error();
    			return false;
    		}
    		clear_nationality_error();
    		var type = '';
    		if($('#nationality_id').val() == ''){
            	type = 'save';
            	id = '';
            } else {
            	type = 'update';
            	id = $('#nationality_id').val();
            }
			$.ajax({
			    url: "./nationality/insert_update",
			    type: "POST",
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    data : {
			    	type 		: type,
			    	id   		: id,
			    	Code  : $('input[name="Code"]').val(),
			    	description : $('.description').val(),
			    },
			    success: function(response){
			        if(response.status == 200){
			            submitForm('new');
			            success_toastr(response.content);
						ntable.fnDraw(false);
			        } else {
			            if(response.content == 'Nationality Code already Exists!'){
			            	danger_toastr(response.content);
			            } else {
			            	submitForm('new');
				            $.each(response.content, function(v, t){
				            	danger_toastr(t);
				            });
			            }
			            ntable.fnDraw(false);
			        }
			    },      
			});
    	} else if (submit_type == 'delete') {
        	if($('#nationality_id').val() == ''){
                return false;
            }
            var id = $('#nationality_id').val();
            clear_nationality_error();
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete the Nationality?',
                buttons: {
                    confirm: function () {
						$.ajax({
						    url: "./nationality/delete",
						    type: "POST",
						    headers: {
						        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						    },
						    data : {
						    	id : id,
						    },
						    success: function(response){
						        if(response.status == 200){
						            submitForm('new');
						            success_toastr(response.content);
						            ntable.fnDraw(false);
						        } else {
						            submitForm('new');
						            danger_toastr(response.content);
						            ntable.fnDraw(false);
						        }
						    },      
						});
                    },
                    cancel: function () {
                    }
                }
            });
    	} else if(submit_type == 'esc'){
    		window.location.href = 'dashboard'; 
    	}
    }

	function success_toastr(msg) {
	    toastr.success(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}
	function danger_toastr(msg) {
	    toastr.error(msg,{
	        "positionClass": "toast-top-right",
	        timeOut: 5000,
	        "closeButton": true,
	        "debug": false,
	        "newestOnTop": true,
	        "progressBar": true,
	        "preventDuplicates": true,
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut",
	        "tapToDismiss": false
	    })
	}

    function clear_nationality_error(argument) {
    	$('input[name="Code"]').css('border-color','#ced4da');
        $('.description').css('border-color','#ced4da');
        $('.code_error').css('display', 'none');
        $('.description_error').css('display', 'none');
    }
    function nationality_error() {
		$('input[name="Code"]').css('border-color','#f44336');
		$('.description').css('border-color','#f44336');
		$('.code_error').css('display', 'block');
		$('.description_error').css('display', 'block');
    }
	hotkeys('f2', function(event, handler){
	  event.preventDefault();
	    submitForm('new');
	});
	hotkeys('f3', function(event, handler){
	  event.preventDefault();
	    submitForm('save');
	});
	hotkeys('f4', function(event, handler){
	  event.preventDefault();
	    submitForm('delete');
	});

</script>
@endsection