<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JuzFX</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/styles/css/themes/lite-purple.min.css')}}">
</head>

<body>
    <div class="auth-layout-wrap" style="background-image: url({{asset('assets/images/loginbg.jpg')}})">
        <div class="auth-content">
            <div class="card o-hidden">
                <div class="row">
                    <div class="col-lg-12">
                         <div class="p-4" style="padding-bottom: 0 !important;">
                            <div class="auth-logo text-center mb-4" style="margin-bottom: 0 !important;">
                                <img src="{{asset('assets/images/logo.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="p-4">
                            <h1 class="mb-3 text-18">Sign In</h1>
                            <form method="POST" action="{{ route('login') }}" id="form_login" name="form_login">
                                {{ csrf_field() }}
                                <div class="form-group" style="margin-bottom: 20px;">
                                    <label for="username">Username</label>
                                    <input id="login" type="text"
                                               class="form-control-rounded form-control{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                                               name="login" value="{{ old('username') ?: old('email') }}" required autofocus>
                                    @if ($errors->has('username') || $errors->has('email'))
                                        <span class="help-block" style="color:red; !important">
                                                <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>                                

                                <div class="form-group" style="margin-bottom: 20px;">
                                    <label for="password">Password</label>
                                    <input id="password" class="form-control form-control-rounded required" type="password" name="password" value="{{ old('password') }}">
                                    @if ($errors->has('password'))
                                        <span class="help-block" style="color:red !important">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <button class="btn btn-rounded btn-primary btn-block mt-2">Sign In</button>

                            </form>

                            <div class="mt-3 text-center" style="display: none;">
                                <a href="forgot.html" class="text-muted"><u>Forgot Password?</u></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 text-center ">
                        <div class="pr-3 auth-right" >
                            @if (env('APP_ENV')=='MC')
                            @else
                            @endif

                            
                        </div>
                    </div>
                    <div class="col-lg-12">
                         <div class="p-4" style="padding-bottom: 0 !important; padding-top: 0 !important;">
                            <div class="auth-logo text-right mb-4" style="margin-bottom: 10px !important;">
                               <i><img src="{{asset('assets/images/powered.png')}}"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('assets/js/common-bundle-script.js')}}"></script>
    <script src="{{asset('assets/js/script.js')}}"></script>
</body>
</html>