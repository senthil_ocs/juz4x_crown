@extends('pdf.layout')

@section('main-content')
<?php 
$i = 1;
?>
	@foreach($datas as $value)

        @if(isset($value->customer))
		<table style="border-bottom: 1px solid #000; width: 100%; line-height: 15px;">
			<tr>
				<td class="sno">{{$i}}</td>
				<td>
					<table>
						<tr>
							<td class="cusname">
								<p class="fontclass">CUSTOMER NAME</p>
							</td>
							<td class="cusname">{{$value->CustName}}</td>
						</tr>
						<tr>
							<td>
								<div class="fontclass">PRIMARY CONTACT</div>
							</td>
							<td>{{$value->customer->NameOfcompany}}</td>
						</tr>
						<tr>
							<td class="col_tr">
								<table style="max-height: 200px;">
									<tr>
										<td class="fontclass">
											ADDRESS
										</td>
									</tr>
									@if($value->customer->CompAddress1 != '')
									<tr>
										<td>{{$value->customer->CompAddress1}}</td>
									</tr>
									@endif
									@if($value->customer->CompAddress2 != '')
									<tr>
										<td>{{$value->customer->CompAddress2}}</td>
										@if($value->customer->CompAddress3 != '')
											{{$value->customer->CompAddress3}}
										@endif
									</tr>
									@endif
								</table>
								<table class="bottom_row">
									<tr>
										<td style="width: 30px;">{{$value->customer->Country}}</td>
										<td>{{$value->customer->CompPostalCode}}</td>
									</tr>
								</table>
							</td>
							<td class="col_tr">
								<table>
									<tr>
										<td class="fontclass" colspan="2"> CONTACT DETAILS</td>
									</tr>
									<tr>
										<td>PHONE 1</td>
										<td>: {{$value->customer->CompPhone1}}</td>
									</tr>
									<tr>
										<td>PHONE 2</td>
										<td>: {{$value->customer->CompPhone2}}</td>
									</tr>
								</table>
								<table class="bottom_row">
									<tr>
										<td style="width: 55px;">FAX </td>
										<td>: {{$value->customer->CompFax}}</td>
									</tr>
								</table>
							</td>

							<td class="col_tr">
								<table>
									<tr>
										<td class="fontclass" colspan="2">KYC ESSENTIALS</td>
									</tr>
									<tr>
										<td>TRADING FORM :</td>
										<td><input type="checkbox" name=""></td>
									</tr>
									<tr>
										<td>MC LICENS :</td>
										<td><input type="checkbox" name="" checked="checked"></td>
									</tr>
								</table>
								<table class="bottom_row">
									<tr>
										<td style="width: 105px;">ID COPIES</td>
										<td><input type="checkbox" name=""></td>
									</tr>
								</table>
							</td>

							<td class="col_tr">
								<table>
									<tr>
										<td class="fontclass" colspan="2">OTHER</td>
									</tr>
									<tr>
										<td>CODE </td>
										<td>: {{$value->customer->Custcode}}</td>
									</tr>
									<tr>
										<td>TYPE </td>
										<td>: {{$value->customer->MCType}}</td>
									</tr>
								</table>
								<table class="bottom_row">
									<tr>
										<td>MC NO :</td>
										<td>{{$value->customer->TradingLicense}}</td>
									</tr>
								</table>
							</td>
						</tr>

					</table>
				</td>
			</tr>
		</table>
		<?php $i++; ?>
	@endif
	@endforeach
@endsection