@extends('pdf.layout')

@section('main-content')
<?php 
$i = 1;
?>
	@foreach($datas as $value)
		<table style="border-bottom: 1px solid #000; width: 100%; line-height: 15px;">
			<tr>
				<td class="sno">{{$i}}</td>
				<td>
					<table>
						<tr>
							<td class="cusname">
								<p class="fontclass">CUSTOMER NAME</p>
							</td>
							<td class="cusname">{{$value->Name}}</td>
						</tr>
						<tr>
							<td>
								<div class="fontclass">PRIMARY CONTACT</div>
							</td>
							<td>{{$compName}}</td>
						</tr>
						<tr>
							<td class="col_tr">
								<table style="max-height: 200px;">
									<tr>
										<td class="fontclass">
											ADDRESS
										</td>
									</tr>
									@if($value->Address1 != '')
									<tr>
										<td>{{$value->Address1}}</td>
									</tr>
									@endif
									@if($value->Address2 != '')
									<tr>
										<td>{{$value->Address2}}</td>
										@if($value->Address3 != '')
											{{$value->Address3}}
										@endif
									</tr>
									@endif
								</table>
								<table class="bottom_row">
									<tr>
										<td style="width: 30px;">{{$value->Address4}}</td>
										<td>{{$value->CompPostalCode}}</td>
									</tr>
								</table>
							</td>
							<td class="col_tr">
								<table>
									<tr>
										<td class="fontclass" colspan="2"> CONTACT DETAILS</td>
									</tr>
									<tr>
										<td>PHONE 1</td>
										<td>: {{$value->PhoneNo}}</td>
									</tr>
									<tr>
										<td>PHONE 2</td>
										<td>: {{$value->CompPhone2}}</td>
									</tr>
								</table>
								<table class="bottom_row">
									<tr>
										<td style="width: 55px;">FAX </td>
										<td>: {{$value->CompFax}}</td>
									</tr>
								</table>
							</td>

							<td class="col_tr">
								<table>
									<tr>
										<td class="fontclass" colspan="2">KYC ESSENTIALS</td>
									</tr>
									<tr>
										<td>TRADING FORM :</td>
										<td><input type="checkbox" name=""></td>
									</tr>
									<tr>
										<td>MC LICENS :</td>
										<td><input type="checkbox" name="" checked="checked"></td>
									</tr>
								</table>
								<table class="bottom_row">
									<tr>
										<td style="width: 105px;">ID COPIES</td>
										<td><input type="checkbox" name=""></td>
									</tr>
								</table>
							</td>

							<td class="col_tr">
								<table>
									<tr>
										<td class="fontclass" colspan="2">OTHER</td>
									</tr>
									<tr>
										<td>CODE </td>
										<td>: {{$value->CustCode}}</td>
									</tr>
									<tr>
										<td>TYPE </td>
										<td>: {{$value->MCType}}</td>
									</tr>
								</table>
								<table class="bottom_row">
									<tr>
										<td>MC NO :</td>
										<td>{{$value->TradingLicense}}</td>
									</tr>
								</table>
							</td>
						</tr>

					</table>
				</td>
			</tr>
		</table>
		<?php $i++; ?>
	@endforeach
@endsection