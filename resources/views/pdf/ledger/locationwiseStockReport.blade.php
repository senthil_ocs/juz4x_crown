@extends('pdf.ledger.locationwiseStockReport_layout')
@section('main-content')
<table id="customers">
	<thead id="cushead">
		<tr>
			<td>Currency</td>
			<td align="right">Stock</td>
			<td align="right">Average Cost</td>
			<td align="right">Local Value</td>
			<td>Last P.Date</td>
		
		</tr>
	</thead>
	<tbody>
		@foreach($data as $key=>$val)
		<tr>
			<td id="tot">{{$val['Currency']}}</td>
			<td id="tot" align="right">{{$val['Stock']}}</td>
			<td id="tot" align="right">{{$val['AverageCost']}}</td>
			<td id="tot" align="right"> {{$val['LocalValue']}}</td>
			<td id="tot">{{$val['Date']}}</td>
		
		</tr>
		@endforeach
	</tbody>
	<tr id="footertr">
		<td id="tot"></td>
		<td colspan="2" id="tot"><b>Grand Total :</b></td>
		<td id="tot"><b>{{$totallocalvalue}}</b></td>
		<td id="tot"><b></b></td>
	</tr>
</table>
@endsection