@extends('pdf.ledger.currencyledger_layout')
@section('main-content')
<table id="customers">
	<thead id="cushead">
		<tr>
			<td>Trans No</td>
			<td>Trans Date</td>
			<td>CustCode</td>
			<td>Rate</td>
			<td>Buy</td>
			<td>Sell</td>
		@if(empty($customercode))
			<td>Balance</td>
			<td>Local Bal</td>
		@endif
		</tr>
	</thead>
	<tbody>
		
		@if(empty($customercode))
			<tr>
				<td>{{$data[0]['CurrencyCode']}}</td>
				<td colspan="5">Opening Balance :</td>
				@if($openingbal >0)
					<td>{{$openingbal}}</td>
				@else 
					<td>0</td>
				@endif
			</tr>
		@else
			<tr>
				<td>{{$data[0]['CustName']}}</td>
				<td colspan="3">customer balances:</td>
				@if($openingbal >0)
					<td>{{$openingbal}}</td>
				@else 
					<td>0</td>
				@endif

				@if($openingbal < 0)
					<td>{{$openingbal}}</td>
				@else 
					<td>0</td>
				@endif

			</tr>

		@endif
		@foreach($data as $val)
			<tr>
				<td>{{$val['TranNo']}}</td>
				<td>{{$val['TranDate']}}</td>
				<td>{{$val['CustCode']}}</td>
				<td>{{$val['Rate']}}</td>
				<td id="tdcolor">{{$val['Tbuy']}}</td>
				<td id="tdsell">{{$val['Tsell']}}</td>
			@if(empty($customercode))
				<td id="tdcolor">{{$val['Bal']}}</td>
				<td id="tdsell">{{$val['LocalBal']}}</td>
			@endif
			</tr>	
		@endforeach
	</tbody>
		<tr id="footertr">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td id="tot">{{$fcurr}}</td>
			<td id="tot">{{$sell}}</td>
		@if(empty($customercode))
			<td></td>
			<td></td>
		@endif
		</tr>
		<tr>
			<td></td>
		</tr>
</table>
@endsection