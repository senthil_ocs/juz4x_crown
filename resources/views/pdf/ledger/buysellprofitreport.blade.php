@extends('pdf.ledger.currencyledger_layout')
@section('main-content')
<table id="customers">
	<thead id="cushead">
		<tr>
			<th>Currency Code</th>
			<th style="text-align: right;">Sell in FAmount</th>
			<th style="text-align: right;">Sell in LAmount(SGD)</th>
			<th style="text-align: right;">Total BUY</th>
			<th style="text-align: right;">Profit</th>
			
		</tr>
	</thead>
	<tbody>
		
		@foreach($data as $val)
			<tr>
				<td >{{$val['CurrencyCode']}}</td>
				<td style="text-align: right;">{{$val['totalSell']}}</td>
				<td style="text-align: right;">{{$val['totalSgd']}}</td>
				<td style="text-align: right;">{{$val['buyAmount']}}</td>
				<td style="text-align: right;">{{$val['profit']}}</td>
			
			</tr>	
		@endforeach
	</tbody>
	<tfoot>
	  <tr>
		<td></td>
		<td></td>
		<td></td>
		<td style="text-align: right;">Total</td>
		<td style="text-align: right;">{{$profittotal}}</td>
	  </tr>
	</tfoot>
</table>
@endsection