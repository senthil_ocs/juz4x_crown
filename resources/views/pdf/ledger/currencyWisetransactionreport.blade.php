@extends('pdf.ledger.currencyWisetransactionreport_layout')
@section('main-content')
<table id="customers">
	<thead id="cushead">
		<tr>
			<td colspan="2" align="center">Currency</td>
			<td colspan="2" align="center">Customer</td>
			<td>Buy</td>
			<td>Sell</td>
		
		</tr>
	</thead>
	<tbody>
		@foreach($data as $key=>$val)
		@php 
		 $Currencydata = \App\Currency::select('CurrencyName')->where('CurrencyCode',$key)->first();
		 @endphp
		<tr>
			<td>{{$key}}</td>
			<td colspan='4'>{{$Currencydata->CurrencyName}}</td>
		</tr>
		@if($val)
			@php

				$buyAmount = 0;
				$sellAmount = 0; 
			@endphp
				@foreach($val as $list)
					@php 
						$currency_code = $list['CurrencyCode']; 
						$buyAmount += (float)str_replace(',','',$list['Tbuy']);
						$sellAmount += (float)str_replace(',','',$list['Tsell']);

					@endphp
					<tr>
						<td></td>
						<td></td>
						<td>{{$list['CustCode']}}</td>
						<td>{{$list['CustName']}}</td>
						<td id="">{{$list['Tbuy']}}</td>
						<td id="">{{$list['Tsell']}}</td>
					
					</tr>
				@endforeach
		@endif	
		<tr>
			<td id="tot"></td>
			<td id="tot"></td>
			<td id="tot"><b>Total </b></td>
			<td id="tot"><b>{{$currency_code}}</b></td>
			<td id="tot"><b>{{$buyAmount}}</b></td>
			<td id="tot"><b>{{$sellAmount}}</b></td>
		
		</tr>
		@endforeach
	</tbody>
	<tr id="footertr">
		<td id="tot"></td>
		<td colspan="2" id="tot"><b>Grand Total :</b></td>
		<td id="tot"></td>
		<td id="tot"><b>{{$buy}}</b></td>
		<td id="tot"><b>{{$sell}}</b></td>
	</tr>
</table>
@endsection