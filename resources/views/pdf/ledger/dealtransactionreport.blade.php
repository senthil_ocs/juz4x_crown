@extends('pdf.ledger.dealtransactionreport_layout')
@section('main-content')
<table id="customers">
	<thead id="cushead">
		<tr>
			<td>TranNo</td>
			<td>Type</td>
			<td>User</td>
			<td>Customer</td>
			<td>Currency</td>
			<td>FAmount</td>
			<td>Rate</td>
			<td>LAmount</td>
			<td>ValidTill</td>
			<td>Remarks</td>
			<td>Status</td>
			<td>Date</td>
			<td>RealisedAmount</td>
			<!-- <td colspan="2">
				<table id="innertableheader">
				<tr>
					<td colspan="2" class="text-center">Realised</td>
				</tr>
				<tr>
					<td>Amount</td>
					<td>Tran No</td>
				</tr>
			</table>	
			</td> -->
			<td>Balance</td>
		</tr>
	</thead>
	<tbody>
		@if($data)
			@foreach($data as $date=> $datas)
				<tr>
					<th>{{ $date }}</th>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				@foreach($datas as $details)
				<tr>
					<td>{{$details['DealNo']}}</td>
					<td>{{$details['TranType']}}</td>
					<td>{{$details['User']}}</td>
					<td>{{$details['Customer']}}</td>
					<td>{{$details['Currency']}}</td>
					<td align="right">{{$details['FAmount']}}</td>
					<td>{{$details['Rate']}}</td>
					<td align="right">{{$details['LAmount']}}</td>
					<td>{{$details['ValidTill']}}</td>
					<td>{{$details['Remarks']}}</td>
					<td>{{$details['Status']}}</td>
					<td>{{$details['Date']}}</td>
					<td align="right">{{$details['RealisedAmount']}}</td>
					<td align="right">{{$details['Balance']}}</td>
				</tr>
				@endforeach
			@endforeach
		@endif
	</tbody>
		<tr id="footertr">
			<td colspan="15"></td>
			
		</tr>
</table>
@endsection
