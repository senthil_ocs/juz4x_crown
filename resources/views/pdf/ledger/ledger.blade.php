@extends('pdf.ledger.ledger_layout')
@section('main-content')

{{$data[0]['CustName']}}
<table id="customers">

	<thead id="cushead">
		
		<tr>
			<td>Trans No</td>
			<td>Trans Date</td>
			<td>Curr Description</td>
			<td>Type B/S</td>
			<td style="text-align: right;">Rate</td>
			<td style="text-align: right;">F.Currency</td>
			<td style="text-align: right;">Sell</td>
			<td style="text-align: right;">Buy</td>
			<td style="text-align: right;">Balance</td>
		</tr>
	</thead>


	<tbody>
		<tr>
			
			<td colspan="5">customer balances:</td>
			@if($openingbal >0)
				<td>{{$openingbal}}</td>
			@else 
				<td>0</td>
			@endif

			@if($openingbal < 0)
				<td>{{$openingbal}}</td>
			@else 
				<td>0</td>
			@endif

		</tr>
		@foreach($data as $val)
			<tr>
				<td>{{$val['TranNo']}}</td>
				<td>{{$val['TranDate']}}</td>
				<td>{{$val['CurrencyCode']}}</td>
				<td>{{$val['TranType']}}</td>
				<td style="text-align: right;">{{$val['Rate']}}</td>
				<td style="text-align: right;">{{$val['FAmount']}}</td>
				<td id="tdsell" style="text-align: right;">{{$val['Tsell']}}</td>
				<td id="tdcolor" style="text-align: right;">{{$val['Tbuy']}}</td>
				<td id="tdcolor" style="text-align: right;">{{$val['Bal']}}</td>
			</tr>	
		@endforeach
	</tbody>
		<tr id="footertr">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td id="tdsell"></td>
			<td id="tot" style="text-align: right;">{{$fcurr}}</td>
			<td id="tot" style="text-align: right;">{{$sell}}</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
		</tr>
</table>
@endsection