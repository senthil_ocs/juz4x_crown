@extends('pdf.ledger.currencyledger_layout')
@section('main-content')
<table id="customers">
	<thead id="cushead">
		<tr>
			<th>TransNo</th>
			<th>TransDate</th>
			<th>CustCode</th>
			<th>Currency</th>
			<th>B/S</th>
			<th align="right">FAmount</th>
			<th align="right">Ex.Rate</th>
			<th align="right">LAmount</th>
		</tr>
	</thead>
	<tbody>
		
		@foreach($data as $val)
			<tr>
				<td>{{$val['TranNo']}}</td>
				<td>{{$val['TranDate']}}</td>
				<td>{{$val['CustCode']}}</td>
				<td>{{$val['Currency']}}</td>
				<td>{{$val['bs']}}</td>
				<td align="right">{{$val['FCurrency']}}</td>
				<td align="right">{{$val['Rate']}}</td>
				<td align="right">{{$val['LAmount']}}</td>
			</tr>	
		@endforeach
	</tbody>
</table>
@endsection