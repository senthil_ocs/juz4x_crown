<!DOCTYPE html>
<html>
<head>
<style>
@page {
	header: page-header;
	footer: page-footer;
	header {
		size: 200mm 200mm;
		margin-top: 2.5cm;
		margin-bottom: 1.5cm;
	}
}	
body {
	font-family: "Nunito", sans-serif;
	padding: 50px;
}
#customers {
	border-collapse: collapse;
	width: 100%;
	border-bottom: 1px solid #000;
	border-width: 1px;
	padding: 8px;
}
#customers td, #customers th {
	padding: 5px;	
}
#cushead td {
	border-bottom: 1px solid #000;
	border-top: 1px solid #000;
	font-weight: bold;
}
#innertableheader td {
	border-bottom: 0px solid #000;
	border-top: 0px solid #000;
	font-weight: bold;
}
#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  color: #000;
}

#tot{
	border-bottom: 1px solid #000;
	border-top: 1px solid #000;
	padding: 10px 10px;
}
#tdcolor{
	color: red;
	text-align: right;
}
#tdsell{
	color: blue;
	text-align: right;
}
</style>
</head>
<body>
	<htmlpageheader name="page-header">
		<table style= "width:100%; margin-bottom: 150px;">
			<tr>
				<td style= "width:75%;line-height: 25px;">
					<h3>ARCADE PLAZA TRADERS PTE LTD</h3>
					<h5>DEAL TRANSACTION REPORT</h5>
				</td>
				<td style= "width:25%;line-height: 25px;">
					<p>Print Date : {{ date('d/m/Y') }}</p>
					<p>Page No   : {PAGENO}</p>
				</td>
			</tr>
		</table>
	</htmlpageheader>
	
	@yield('main-content')

	<htmlpagefooter name="page-footer">
		@2019
	</htmlpagefooter>
</body>
</html>

