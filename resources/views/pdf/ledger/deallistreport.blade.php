@extends('pdf.ledger.currencyledger_layout')
@section('main-content')
<table id="customers">
	<thead id="cushead">
		<tr>
			<th>Deal No</th>
			<th>Deal Date</th>
			<th>Type</th>
			<th>Customer</th>
			<th>Curr. Code</th>
			<th align="right">Amount</th>
			<th align="right">Rate</th>
			<th>Valid Till</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		
		@foreach($data as $val)
			<tr>
				<td>{{$val['Deal_No']}}</td>
				<td>{{$val['Deal_Date']}}</td>
				<td>{{$val['Tran_type']}}</td>
				<td>{{$val['Customer']}}</td>
				<td>{{$val['Curr_Code']}}</td>
				<td align="right">{{$val['FAmount']}}</td>
				<td align="right">{{$val['Rate']}}</td>
				<td>{{$val['Valid_Till']}}</td>
				<td>{{$val['Status']}}</td>
			</tr>	
		@endforeach
	</tbody>
</table>
@endsection