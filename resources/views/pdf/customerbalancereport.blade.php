@extends('pdf.layout')
@section('main-content')
<?php 
$i = 1;
?>
	<div style="padding:10px;">
		<table style="width: 100%; line-height: 15px; border-collapse: collapse;">
			<thead>
				<tr>
					<td class="fontclass" style="border-bottom: 1px solid #000;">Sno</td> 
					<td class="fontclass" style="border-bottom: 1px solid #000;">Customer Code</td> 
					<td class="fontclass" style="border-bottom: 1px solid #000;">Name</td> 
					<td class="fontclass" style="border-bottom: 1px solid #000;text-align:right;">Balance</td> 
					
				</tr>
			</thead>
			<tbody>
			@foreach($datas as $value)
				<tr>
					<td style="border-bottom: 1px solid #000;">{{$i}}</td>
					<td style="border-bottom: 1px solid #000;">{{$value->Custcode}}</td>
					<td style="border-bottom: 1px solid #000;">{{$value->Name}}</td>
					<td style="border-bottom: 1px solid #000;text-align:right;">{{ currency_format($value->Balance,2) }}</td>
				</tr>
			<?php $i++; ?>
			@endforeach
			</tbody>
		</table>
	</div>
		
@endsection