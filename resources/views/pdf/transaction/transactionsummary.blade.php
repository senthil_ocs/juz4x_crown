<!DOCTYPE html>
<html>
<head>
<style>
@page {
	header: page-header;
	footer: page-footer;
	header {
		size: 200mm 200mm;
		margin-top: 2.5cm;
		margin-bottom: 1.5cm;
	}
}	
body {
	font-family: "Nunito", sans-serif;
	padding: 50px;
}
.summaryHead th {
	border: 1px solid #000;
	font-weight: bold;
}
.summaryHead td{
	border: 1px solid #000;
}
#tableBorder {
	border-collapse: collapse;
	width: 100%;
	border-bottom: 1px solid #000;
	border-width: 1px;
	padding: 8px;
}
#tableBorder td, #tableBorder th {
	padding: 5px;	
}
.tableHead th{
  	padding-right: 12px; 
	font-weight: bold;
    font-size: 13px !important;	
}
.tableRow td {
  padding-right: 12px;
  padding-bottom: 12px;
  text-align: left;
  color: #000;
  font-size: 12px !important;
}
</style>
</head>
<body>
	<htmlpageheader name="page-header">
		<?php 
		$fromDate = date("d/m/Y",strtotime($fromDate));
		$toDate = date("d/m/Y",strtotime($toDate));
		?>
		<table style= "width:100%; margin-bottom: 150px;">
			<tr>
				<td style= "width:75%;line-height: 25px;">
					<h3>{{$compName}}</h3><br>
					<h5>Register of all Remittance Transactions From {{ $fromDate }} To {{ $toDate }}<span id="locationCode"> {!! $locationc !!}</span></h5> 
				</td>
				<td style= "width:25%;line-height: 25px;">
					<p>Print Date : {{ date('d/m/Y') }}</p>
					<p>Page No   : {PAGENO}</p>
				</td>
			</tr>
		</table>
	</htmlpageheader>

		<table id="tableBorder" style="border: 1px solid #000;">
            <thead class="summaryHead"> 
                <tr class="tableHead" >
                	<th rowspan="2">Date</th>
                    <th colspan="3" style="text-align: center;">Customer</th>
                    <th colspan="3" style="text-align: center;">Agent</th>
                    <th colspan="3" style="text-align: center;">Profit</th>
                </tr>
                <tr class="tableHead">
                	<th>Amount</th>
                	<th>Commission</th>
                	<th>Total</th>
                	<th>Amount</th>
                	<th>Commission</th>
                	<th>Total</th>
                	<th>Amount</th>
                	<th>Commission</th>
                	<th>Total</th>
                </tr>
            </thead>
           	@foreach($datasSummary as $key => $val)
				@php 
					$totalAmount = $val->customerAmount - $val->agentAmount;
					$commission = $val->customerComm - $val->agentComm;
					$totalProfit = $val->customerTotalAmount - $val->agentTotal;
				@endphp
                <tbody class="summaryHead" style="border: 1px solid #000;">
                	<tr class="tableRow">	
                		<td>{{date("d-m-Y", strtotime($val->TranDate))}}</td>
                		<td>{{number_format($val->customerAmount,2)}}</td>
                		<td>{{number_format($val->customerComm,2)}}</td>
                		<td>{{number_format($val->customerTotalAmount,2)}}</td>
                		<td>{{number_format($val->agentAmount,2)}}</td>
                		<td>{{number_format($val->agentComm,2)}}</td>
                		<td>{{number_format($val->agentTotal,2)}}</td>
                		<td>{{number_format($totalAmount,2)}}</td>
                		<td>{{number_format($commission,2)}}</td>
                		<td>{{number_format($totalProfit,2)}}</td>
                	</tr>
                </tbody>
			@endforeach	
        </table>

	<htmlpagefooter name="page-footer">
	@2019
	</htmlpagefooter>
</body>
</html>