<!DOCTYPE html>
<html>
<head>
<style>
@page {
	header: page-header;
	footer: page-footer;
	header {
		size: 200mm 200mm;
		margin-top: 2.5cm;
		margin-bottom: 1.5cm;
	}
}	
body {
	font-family: "Nunito", sans-serif;
	padding: 50px;
}
.summaryHead th {
	border: 1px solid #000;
	font-weight: bold;
}
.summaryHead td{
	border: 1px solid #000;
}
#tableBorder {
	border-collapse: collapse;
	width: 100%;
	border-bottom: 1px solid #000;
	border-width: 1px;
	padding: 8px;
}
#tableBorder td, #tableBorder th {
	padding: 5px;	
}
.tableHead th{
  	padding-right: 12px; 
	font-weight: bold;
    font-size: 13px !important;	
}
.tableRow td {
  padding-right: 12px;
  padding-bottom: 12px;
  text-align: left;
  color: #000;
  font-size: 12px !important;
}
</style>
</head>
<body>
	<htmlpageheader name="page-header">
		<?php 
		$fromDate = date("d/m/Y",strtotime($fromDate));
		$toDate = date("d/m/Y",strtotime($toDate));
		?>
		<table style= "width:100%; margin-bottom: 150px;">
			<tr>
				<td style= "width:75%;line-height: 25px;">
					<h3>{{$compName}}</h3><br>
					<h5>Register of all Remittance Transactions From {{ $fromDate }} To {{ $toDate }}<span id="locationCode"> {!! $locationc !!}</span></h5> 
				</td>
				<td style= "width:25%;line-height: 25px;">
					<p>Print Date : {{ date('d/m/Y') }}</p>
					<p>Page No   : {PAGENO}</p>
				</td>
			</tr>
		</table>
	</htmlpageheader>

		<table id="tableBorder" style="border: 1px solid #000;">
            <thead class="summaryHead"> 
                <tr class="tableHead" >
                	<th>Date of transfer /<br> Transaction Date/<br> Recived Date/<br> Transfer No</th>
                    <th>CustName / <br> Nationality/ <br> NRIC Passport No/ <br> DOB / Address</th>
                    <th>Local Amount/ <br> Ex Rate/ <br> Commission</th>
                    <th>Currency/ <br> Foreign Amt/ <br> Agent Name</th>
                    <th>Beneficiary's Details</th>
                </tr>
            </thead>
           	@foreach($datasSummary as $key => $val)
                <tbody class="summaryHead" style="border: 1px solid #000;">
                	<tr class="tableRow">
                        <td>
                            {{$val->TransmissionDate != ""? date("d-m-Y", strtotime($val->TransmissionDate)):""}}<br>
                            {{$val->TranDate != ""? date("d-m-Y", strtotime($val->TranDate)):""}}<br>
                            {{$val->PaymentDate != ""? date("d-m-Y", strtotime($val->PaymentDate)):""}}<br>
                            {{$val->TranNo}}
                        </td>
                        <td>
                            {{$val->CustName}}<br>
                            {{$val->CustNationality}}<br>
                            {{str_repeat('X', strlen($val->CustPPNo) - 4) . substr($val->CustPPNo, -4)}}<br>
                            {{$val->DOB != ""? date("d-m-Y", strtotime($val->DOB)):""}}<br>
                            {{$val->Address1}}
                            {{$val->Address2}}
                            {{$val->Address3}}
                            {{$val->Address4}}
                            {{$val->Address5}}
                        </td>
                		<td>
                			{{number_format($val->LAmount,2)}}<br>
                			{{$val->ExchRate}}<br>
                			{{number_format($val->Comm,2)}}
                		</td>
                		<td>
                			{{$val->CurrencyCode}}<br>
                			{{number_format($val->FAmount,2)}}<br>
                			{{$val->AgentName}}
                		</td>
                		<td>
                			{{$val->BenName}}<br>
                			{{$val->BenAddress1}}<br>
                			{{$val->BenAddress2}}<br>
                			{{$val->BenAddress3}}
                		</td>
                	</tr>
                </tbody>
			@endforeach	
        </table>

	<htmlpagefooter name="page-footer">
	@2019
	</htmlpagefooter>
</body>
</html>