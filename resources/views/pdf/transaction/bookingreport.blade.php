<!DOCTYPE html>
<html>
<head>
<style>
@page {
    header: page-header;
    footer: page-footer;
    header {
        size: 200mm 200mm;
        margin-top: 2.5cm;
        margin-bottom: 1.5cm;
    }
}   
body {
    font-family: "Nunito", sans-serif;
    padding: 50px;
}
#tableBorder {
    border-bottom: 1px solid #000;
    border-top: 1px solid #000;
    border-collapse: collapse;
    width: 100%;
    border-width: 1px;
    padding: 8px;
}
#tableBorder td, #tableBorder th {
    padding: 5px;   
}
#tableHead th{
    border-bottom: 1px solid #000;
    padding-right: 12px;
    font-weight: bold;
    font-size: 10px !important; 
}
#tableSubHead th{
    border-bottom: 1px solid #000;
    padding-right: 12px;
    font-size: 8px !important; 
    text-align: left;
}
.tableRow td {
  padding-right: 12px;
  padding-bottom: 12px;
  text-align: left;
  color: #000;
  font-size: 9px !important;
}
</style>
</head>
<body>
    <htmlpageheader name="page-header">
        <table style= "width:100%; margin-bottom: 150px;">
            <tr>
                <td style= "width:75%;line-height: 25px;">
                    <h3>{{$compName}}</h3><br>
                    <h5>Register of all Remittance Transactions From {{ $fromDate }} To {{ $toDate }}<span id="locationCode"> {!! $locationc !!}</span></h5> 
                </td>
                <td style= "width:25%;line-height: 25px;">
                    <p>Print Date : {{ date('d/m/Y') }}</p>
                    <p>Page No   : {PAGENO}</p>
                </td>
            </tr>
        </table>
    </htmlpageheader>
    
    <h4>Booking Transaction Report </h4>

        <table id="tableBorder">
            <thead style="border-bottom: solid 1px #000;"> 
                <tr id="tableHead" >
                    <th>RefNo</th>
                    <th>Type</th>
                    <th>Customer</th>
                    <th>Booking Date</th>
                    <th>Value Date</th>
                    <th>Valid Till</th>
                    <th>Currency</th>
                    <th>FAmount</th>
                    <th>Rate</th>
                    <th>Balance</th>
                </tr>
            </thead>
            @if(count($finalBuySell) > 0)
            <?php /*printArray($finalBuySell);die;*/ ?>
            @foreach($finalBuySell as $key => $val)
            <!-- <thead> -->
                <tr id="tableSubHead"><th colspan="11">{{$key}}</th></tr>
            <!-- </thead> -->
                <?php $balanceAmount = 0; ?>
                @if(count($val) > 0)
                @foreach($val as $key => $val)
                    <?php if($val->TranType == 'Buy'){
                        $balanceAmount += $val->FAmount;
                    } else {
                        $balanceAmount -= $val->FAmount;
                    }
                    ?>
            <tbody>
                <tr class="tableRow">
                    <td>{{$val->DealNo}}</td>
                    <td>{{$val->TranType}}</td>
                    <td>{{$val->CustomerName}}</td>
                    <td>{{$val->DealDate != ""? date("d-m-Y H:i:s",strtotime($val->DealDate)):""}}</td>
                    <td>{{$val->DealDate != ""? date("d-m-Y",strtotime($val->DealDate)):""}}</td>
                    <td>{{$val->ValidTill != ""? date("d-m-Y",strtotime($val->ValidTill)):""}}</td>
                    <td>{{$val->CurrencyName}}</td>
                    <td align="right">{{number_format($val->FAmount,2)}}</td>
                    <td align="right">{{$val->Rate}}</td>
                    <td align="right"><?php echo $val->TranType == 'Sell' ? '-' : '' ?>{{number_format($val->BalanceAmount,2)}}</td>
                </tr>
            </tbody>
            @endforeach 
            @endif 
        <tbody>
            <tr id="tableSubHead">
                <th colspan="9"></th>
                <th colspan="2" align="right">{{number_format($balanceAmount,2)}}</th>
            </tr>
        </tbody>
        @endforeach 
        @else 
            <tbody><tr><td colspan="11">Not Found Data!</td></tr></tbody>
        @endif 
    </table>

    <htmlpagefooter name="page-footer">
        @2019
    </htmlpagefooter>
</body>
</html>

