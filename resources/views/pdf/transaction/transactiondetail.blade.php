<!DOCTYPE html>
<html>
<head>
<style>
@page {
	header: page-header;
	footer: page-footer;
	header {
		size: 200mm 200mm;
		margin-top: 2.5cm;
		margin-bottom: 1.5cm;
	}
}	
body {
	font-family: "Nunito", sans-serif;
	padding: 50px;
}
#tableBorder {
	border-bottom: 1px solid #000;
	border-top: 1px solid #000;
	border-collapse: collapse;
	width: 100%;
	border-width: 1px;
	padding: 8px;
}
#tableBorder td, #tableBorder th {
	padding: 5px;	
}
#tableHead th{
	border-bottom: 1px solid #000;
  	padding-right: 12px;
	font-weight: bold;
    font-size: 10px !important;	
}
.tableRow td {
  padding-right: 12px;
  padding-bottom: 12px;
  text-align: left;
  color: #000;
  font-size: 9px !important;
}
</style>
</head>
<body>
	<htmlpageheader name="page-header">
		<?php 
		$fromDate = date("d/m/Y",strtotime($from));
		$toDate = date("d/m/Y",strtotime($to));
		?>
		<table style= "width:100%; margin-bottom: 150px;">
			<tr>
				<td style= "width:75%;line-height: 25px;">
					<h3>{{$compName}}</h3><br>
					<h5>Register of all Remittance Transactions From {{ $fromDate }} To {{ $toDate }}<span id="locationCode"> {!! $locationc !!}</span></h5> 
				</td>
				<td style= "width:25%;line-height: 25px;">
					<p>Print Date : {{ date('d/m/Y') }}</p>
					<p>Page No   : {PAGENO}</p>
				</td>
			</tr>
		</table>
	</htmlpageheader>
	
	@if(count($final) > 0)
		@foreach($final as $key => $val)
			<?php
			$dates = $key;
			$cumulative_cash = 0;
			$cumulative_nets = 0;
			$cumulative_lamount = 0;
			$cumulative_totamount = 0;
			$cumulative_agentamount = 0;
			$cumulative_profit = 0;
			?>
			@foreach($val as $key1 => $val1)
                <table id="tableBorder">
                    <thead style="border-bottom: solid 1px #000;"> 
                        <tr id="tableHead" >
                            <th>Trans No</th>
                            <th>Agent Name</th>
                            <th>Type</th>
                            <th>Currency Code</th>
                            <th>Rate</th>
                            <th>Local Amt</th>
                            <th>Comm</th>
                            <th>Total Amt</th>
                            <th>Agent Rate</th>
                            <th>Agent Comm</th>
                            <th>Agent Amt</th>
                            <th>Profit</th>
                            <th>Comm Profit</th>
                        </tr>
                    </thead>
                    <?php
                    	$cash = 0;
                    	$nets = 0;
                    	$lamount = 0;
                    	$totamount = 0;
                    	$agentamount = 0;
                    	$commprofit = 0;
                    	$a_profit = 0;
                    ?>
					@foreach($val1 as $key => $val2)
					@php 
					$profit = $val2['TotalAmount']-$val2['AgentTotal'];
					@endphp
                    <tbody>
                    	<tr class="tableRow">
                    		<td>{{$val2['TranNo']}}</td>
                    		<td>{{$val2['AgentName']}}</td>
                    		<td>{{$val2['DocType']}}</td>
                    		<td>{{$val2['CurrencyCode']}}</td>
                    		<td class="text-right">{{$val2['ExchRate']}}</td>
                    		<td class="text-right">{{number_format($val2['LAmount'],2)}}</td>
                    		<td class="text-right">{{number_format($val2['Comm'],2)}}</td>
                    		<td class="text-right">{{number_format($val2['TotalAmount'],2)}}</td>
                    		<td class="text-right">{{number_format($val2['AgentRate'],8)}}</td>
                    		<td class="text-right">{{number_format($val2['AgentCommission'],2)}}</td>
                    		<td class="text-right">{{number_format($val2['AgentTotal'],2)}}</td>
                    		<td class="text-right">{{number_format($profit,2)}}</td>
                    		@php 
                    		$commprofit +=$profit;
                    		@endphp
                    		<td class="text-right">{{number_format($commprofit,2)}}</td>
                    		<?php
                    			if(strtolower($val2['PayMode']) == 'cash'){
                    				$cash+=$val2['LAmount']; 
                    			} else {
                    				$nets+=$val2['LAmount'];
                    			} 
                    			$lamount+=$val2['LAmount'];
                    			$totamount+=$val2['TotalAmount'];
                    			$agentamount+=$val2['AgentTotal'];
                    			$a_profit+=$profit;
                    		?>
                    	</tr>
                    </tbody>
					@endforeach
					<tfoot style="border-bottom: solid 1px #dee2e6;background: #d6d4d4;">
						<tr class="tableRow">
							<?php
								$cumulative_cash+=$cash;
								$cumulative_nets+=$nets;
								$cumulative_lamount+=$lamount;
                    			$cumulative_totamount+=$totamount;
                    			$cumulative_agentamount+=$agentamount;
                    			$cumulative_profit+=$a_profit;
							?>
							<td colspan='5'><b>{{$key1}}</b></td>
							<td ><b>{{number_format($lamount,2)}}</b></td>
							<td colspan='2' style="text-align: right;"><b>{{number_format($totamount,2)}}</b></td>
							<td colspan='3' style="text-align: right;"><b>{{number_format($agentamount,2)}}</b></td>
							<td ><b>{{number_format($a_profit,2)}}</b></td>
							<td></td>
						</tr>
					</tfoot>
                </table>   
			@endforeach
				<table id="table_reporttransdetail" class="table table-striped" style="width:100%;">

					<tbody >
						<tr class="tableRow">
							<td style="width: 38%;"><b>{{date("d-m-Y", strtotime($dates))}}</b></td>
							<td style="width: 11%; text-align: right;"><b>{{number_format($cumulative_lamount,2)}}</b></td>
							<td colspan='2' style="width: 8%; text-align: right;"><b>{{number_format($cumulative_totamount,2)}}</b></td>
							<td colspan='3' style="width: 8%; text-align: right;"><b>{{number_format($cumulative_agentamount,2)}}</b></td>
							<td style="width: 10%; "><b>{{number_format($cumulative_profit,2)}}</b></td>
							<td></td>
						</tr>
						<tr class="tableRow">
							<td style="width: 38%;"></td>
							<td colspan='2' style="width: 11%;"><b>CASH : {{number_format($cumulative_cash,2)}}</b></td>
							<td colspan='3' style="width: 24%;"><b>NETS : {{number_format($cumulative_nets,2)}}</b></td>
							<td colspan="3"></td>
						</tr>
					</tbody>
				</table>
        @endforeach
    @else 
    	<p>Not Found Data!</p>
    @endif 

	<htmlpagefooter name="page-footer">
		@2019
	</htmlpagefooter>
</body>
</html>

