<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">	
		@page {
		header: page-header;
		footer: page-footer;
			header {
				size: 100mm 100mm;
				margin-top: 2cm;
			}
		}
		body {
			font-family: "Nunito", sans-serif;
		}
		.fontclass{
			font-weight: bold;
			font-size: 12px;
		}
		.col_tr{
			width: 190px;
			vertical-align: text-top;
		}
		.custname{
			margin-bottom: 15px;
		}
		.bottom_row{
			margin-top: 25px;
		}
		.custname{
			line-height: 15px;
		}
		.sno{
			vertical-align: text-top;
			padding-top: 3px;
		}
		.cusname{
			padding-bottom: 10px;			
		}
	</style>
	@yield('page-css')
</head>
<body>
	<htmlpageheader name="page-header">
		<table style= "width:100%; border-bottom: 1px solid #000;s">
			<tr>
				<td style= "width:75%;line-height: 25px;">
					<h3>{{$compName}} ({{$locationc}})</h3>
					<h4>@if(isset($title)) {{$title}} @else Customer Master Report @endif</h4>
				</td>
				<td style= "width:25%;line-height: 25px;">
					<table>
						<tr>
							<td>Report Ref </td>
							<td>: Test</td>
						</tr>
						<tr>
							<td>Print Date </td>
							<td>: <?php echo date("d/m/Y"); ?></td>
						</tr>
						<tr>
							<td>Print By </td>
							<td>: <?php echo Auth::user()->username; ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</htmlpageheader>

    <div class="maincontent">
        @yield('main-content')
    </div>

	<htmlpagefooter name="page-footer">
		<table>
			<tr>
				<td>
					<h4>@JUZ MONEY 2019</h4>
				</td>
			</tr>
		</table>
	</htmlpagefooter>
</body>
</html>
