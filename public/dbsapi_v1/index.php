<?php

/**
 * Step 1: Require the Slim Framework using Composer's autoloader
 *
 * If you are not using Composer, you need to load Slim Framework with your own
 * PSR-4 autoloader.
 */
require_once 'vendor/autoload.php';
require_once 'config/config.php';
require_once 'Crypt/GPG.php';
// Uncomment below line if you want db interaction and update db.php file with your db details
//require 'db.php';

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new Slim\App($c);

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */
 // This method is used for fast/paynow request 
$app->get('/', function ($request, $response, $args) {
    printArray($_REQUEST);die;
    try {
        common_log('<===============================Synchronous request start================================>');
        // $gpg = new Crypt_GPG(array('HOME' => '/home/vagrant/.gnupg', 'digest-algo' => 'SHA256', 'cipher-algo' => 'AES256', 'debug' => true));
        $gpg = new Crypt_GPG(array('homedir' => '/home/ubuntu/.gnupg','digest-algo' => DIGEST_ALGO, 'cipher-algo' => CIPHER_ALGO, 
            'compress-algo' => COMPRESS_ALGO));

        // Prepare json object
        $post_data = array(
            'header' => array(
                'msgId' => 'ARAMCOP3TT1123467',
                'orgId' => 'ARAMCOP3',
                'timeStamp' => '2019-07-24T09:30:53.785',
                'ctry' => 'SG',
                'noOfTxs' => '1',
                'totalTxnAmount' => '1000'
            ),
            'txnInfoDetails' => array(
                'txnInfo' => array(
                    array(
                        'customerReference' => 'ARAMCOP3TT0037',
                        'txnType' => 'SG',
                        'txnDate' => '2020-03-02',
                        'txnCcy' => 'CHF',
                        'txnAmount' => '1000',
                        'debitAccountCcy' => 'CHF',
                        'fxContractRef1' => '',
                        'fxAmountUtilized1' => '',
                        'fxContractRef2' => '',
                        'fxAmountUtilized2' =>'',
                        'chargeBearer' => 'DEBT',
                        'debitAccountForBankCharges' => '0072001360015',
                        'instructionToOrderingBank' => 'Discard txn',
                        'senderParty' => array(
                            'name' => 'GTS IBGGTS Acct Company Name1',
                            'accountNo' => '0072001360015',    
                            'bankCtryCode' => 'SG',
                            'swiftBic' => 'DBSSSGS0',
                        ),
                        'receivingParty' => array(
                            'name' => 'Test Account',
                            'accountNo' => '1',
                            'swiftBic' => 'CITIUS30XXX',
                            'bankCtryCode' => 'SG',
                            'bankName' => 'CITI BANK',
                            'bankAddress' => 'Beneficiary Bank Address',                            
                            'beneficiaryAddresses' => array(
                                array('address' => 'Beneficiary address1'),
                                array('address' => 'Beneficiary address2'),
                            )
                        ),
                        'adviseDelivery' => array(
                            'mode' => 'ES',
                            'emails' => array(
                                array('email' => 'eadvuser2@uat1bank.dbs.com')
                            ),
                            'phoneNumbers' => array(
                                array('phoneNumber' => '6598508204')
                            )
                        ),
                        'rmtInf' => array(
                            'paymentDetails' => array(
                                array('paymentDetail' => 'SG RTGS Details')
                            ),
                            'clientReferences' => array(
                                array('clientReference' => 'SG Client Reference 1'),
                                array('clientReference' => 'client Reference2')
                            ),
                            'invoiceDetails' => array(
                                array('invoice' => 'SG Invoice Details RTGS 01'),
                                array('invoice' => 'SG Invoice Details RTGS 02')
                            )
                        )
                    )
                ),
            ),
        );
        // Construct json
        printArray($post_data);

        common_log('<-----Json data to request start----->');
        $json_output = json_encode($post_data);
        common_log($json_output);

       

        common_log('<-----Json data to request end----->');
        
        //Add sign key and encrypt key
        $gpg->addSignKey(CLI_KEY_ID, CLI_PVT_KEY_PASS);
        //Add encrypt key
        $gpg->addEncryptKey(SER_KEY_ID);
        // Encrypt and sign data    
        common_log('<-----Encrypted signed data start----->');
        $encryptedSignedData = $gpg->encryptAndSign($json_output);       
        /*printArray($encryptedSignedData);die;*/
        common_log($encryptedSignedData);
        common_log('<-----Encrypted signed data end----->');


        // Calling API using curl
        // API URL   
        common_log('<-----API response Start----->');
        $api_response = curl_request(SYNC_API_URL, $encryptedSignedData);
        common_log($api_response);
       
        common_log('<-----API response end----->');
        // Check reponse is json or encrypted message
        if (isJson($api_response)) {
            common_log('Got json response instead of encrypted text');
            return $api_response;
        } else {
            // Add decrypt key
            $gpg->addDecryptKey(CLI_KEY_ID, CLI_PVT_KEY_PASS);
            // Decrypt and verify
            common_log('<-----Decrypted start Start----->');
            $decryptedData = $gpg->decryptAndVerify($api_response);
            common_log($decryptedData['data']);
            common_log('<-----Decrypted data end----->');
            return $decryptedData['data'];
        }
    } catch (Exception $e) {
        echo $e->getMessage();
        common_log($e->getMessage());
    }
    common_log('<===============================Synchronous request end================================>');
});


// This method is used for MEPS request 
/*$app->get('/asynchronous', function ($request, $response, $args) {
    try {
        common_log('<===============================Asynchronous request start================================>');
        // $gpg = new Crypt_GPG(array('HOME' => '/home/vagrant/.gnupg', 'digest-algo' => 'SHA256', 'cipher-algo' => 'AES256', 'debug' => true));
        $gpg = new Crypt_GPG(array('digest-algo' => DIGEST_ALGO, 'cipher-algo' => CIPHER_ALGO, 'compress-algo' => COMPRESS_ALGO));

        // Prepare json object
        $post_data = array(
            'header' => array(
                'msgId' => 'MFSTPRT11221624',
                'orgId' => 'R4AN1527',
                'timeStamp' => '2017-01-26T16:16:43.567',
                'ctry' => 'SG',
                'noOfTxs' => '1',
                'totalTxnAmount' => '10000.00'
            ),
            'txnInfoDetails' => array(
                'txnInfo' => array(
                    array('customerReference' => 'MFSTPRT11221622',
                        'txnType' => 'RTGS',
                        'txnDate' => '2017-01-26',
                        'txnCcy' => 'SGD',
                        'txnAmount' => '10000.00',
                        'debitAccountCcy' => 'SGD',
                        'debitAccountAmount' => '100000.00',
                        'fxContractRef1' => '',
                        'fxAmountUtilized1' => '',
                        'fxContractRef2' => '',
                        'fxAmountUtilized2' => "",
                        'chargeBearer' => 'DEBT',
                        'debitAccountForBankCharges' => "811200096751",
                        'senderParty' => array(
                            'name' => 'CADIS LTD',
                            'accountNo' => '0123456789',
                            'bankCtryCode' => 'SG',
                            'swiftBic' => 'DBSSGSGXXX',
                        ),
                        'receivingParty' => array(
                            'name' => 'JOHN TAN',
                            'accountNo' => '0987654321',
                            'bankCtryCode' => 'SG',
                            'swiftBic' => 'OCBCSGSGXXX',
                            'bankName' => 'Beneficiary Bank Name',
                            'bankAddress' => 'Beneficiary Bank Address',
                            'beneficiaryAddresses' => array(
                                array('address' => 'Beneficiary address1'),
                                array('address' => 'Beneficiary address2'),
                                array('address' => 'Beneficiary address3')
                            )
                        ),
                        'adviseDelivery' => array(
                            'mode' => 'ES',
                            'emails' => array(
                                array('email' => 'eadvuser1@uat1bank.dbs.com')
                            ),
                            'phoneNumbers' => array(
                                array('phoneNumber' => '12345678')
                            )
                        ),
                        'rmtInf' => array(
                            'paymentDetails' => array(
                                array('paymentDetail' => 'SG RTGS Details')
                            ),
                            'clientReferences' => array(
                                array('clientReference' => 'SG Client Reference 1'),
                                array('clientReference' => 'client Reference2')
                            ),
                            'invoiceDetails' => array(
                                array('invoice' => 'SG Invoice Details RTGS 01'),
                                array('invoice' => 'SG Invoice Details RTGS 02')
                            )
                        )
                    )
                ),
            ),
        );
        // Construct json
        common_log('<-----Json data to request start----->');
        $json_output = json_encode($post_data);
        common_log($json_output);
        common_log('<-----Json data to request end----->');        

        //Add sign key and encrypt key
        $gpg->addSignKey(CLI_KEY_ID, CLI_PVT_KEY_PASS);
        //Add encrypt key
        $gpg->addEncryptKey(SER_KEY_ID);
        // Encrypt and sign data   
        common_log('<-----Encrypted signed data start----->');
        $encryptedSignedData = $gpg->encryptAndSign($json_output);
        common_log($encryptedSignedData);
        common_log('<-----Encrypted signed data end----->');

        // Calling API using curl
        // API URL
        common_log('<-----API response Start----->');
        $api_response = curl_request(ASYNC_API_URL, $encryptedSignedData);
        common_log($api_response);
        common_log('<-----API response end----->');
        

        // Check reponse is json or encrypted message
        if (isJson($api_response)) {
            common_log('Got json response instead of encrypted text');
            return $api_response;
        } else {
            // Add decrypt key
            $gpg->addDecryptKey(CLI_KEY_ID, CLI_PVT_KEY_PASS);
            // Decrypt and verify
            common_log('<-----Decrypted start Start----->');
            $decryptedData = $gpg->decryptAndVerify($api_response);
            common_log($decryptedData['data']);
            common_log('<-----Decrypted data end----->');
            return $decryptedData['data'];
        }
    } catch (Exception $e) {
        echo $e->getMessage();
        common_log($e->getMessage());
    }
    common_log('<===============================Asynchronous request end================================>');
});


$app->post('/asynchronous_callback', function ($request, $response, $args) {
    try {
        common_log('<===============================Asynchronous callback start================================>');
        $gpg = new Crypt_GPG(array('digest-algo' => DIGEST_ALGO, 'cipher-algo' => CIPHER_ALGO, 'compress-algo' => COMPRESS_ALGO));
        // Receiving encrypted ACK data
        common_log('<-----Received data start----->');
        $data = file_get_contents("php://input");
        common_log($data);
        common_log('<-----Received data end----->');

        if (isJson($data)) {
            common_log('Got json response instead of encrypted text in asynchronous callback');
            return $data;
        } else {
            // Add decrypt key
            $gpg->addDecryptKey(CLI_KEY_ID, CLI_PVT_KEY_PASS);
            // Decrypt requested data
            common_log('<-----Decrypted data start----->');
            $decryptedData = $gpg->decryptAndVerify($data);
            common_log($decryptedData['data']);
            common_log('<-----Decrypted data end----->');

            if (isset($decryptedData) && !empty($decryptedData)) {
                $api_response['status'] = "success";
                $api_response['message'] = 'Successfully received acknowledgement';
            } else {
                $api_response['status'] = "error";
                $api_response['message'] = 'Something went wrong';
            }
        }
        echo json_encode($api_response);
        common_log('<===============================Asynchronous callback end================================>');
        exit();
    } catch (Exception $e) {
        echo $e->getMessage();
        common_log($e->getMessage());
    }
});

$app->post('/notification', function ($request, $response, $args) {
    try {
        common_log('<===============================Notification start================================>');
        $gpg = new Crypt_GPG(array('digest-algo' => DIGEST_ALGO, 'cipher-algo' => CIPHER_ALGO, 'compress-algo' => COMPRESS_ALGO));
        // Receiving encrypted notification data
        common_log('<-----Received data start----->');
        $data = file_get_contents("php://input");
        common_log($data);
        common_log('<-----Received data end----->');

        if (isJson($data)) {
            common_log('Got json response instead of encrypted text in notification callback');
            return $data;
        } else {
            // Add decrypt key
            $gpg->addDecryptKey(CLI_KEY_ID, CLI_PVT_KEY_PASS);
            // Decrypt requested data
            common_log('<-----Decrypted data start----->');
            $decryptedData = $gpg->decryptAndVerify($data);
            common_log($decryptedData['data']);
            common_log('<-----Decrypted data end----->');

            if (isset($decryptedData) && !empty($decryptedData)) {
                $api_response['status'] = "success";
                $api_response['message'] = 'Successfully received notification';
            } else {
                $api_response['status'] = "error";
                $api_response['message'] = 'Something went wrong';
            }
        }
        echo json_encode($api_response);
        common_log('<===============================Notification end================================>');
        exit();
    } catch (Exception $e) {
        echo $e->getMessage();
        common_log($e->getMessage());
    }
});
*/
function printArray($res){
    echo '<pre>';
    print_r($res);
    echo '</pre>';
}

function curl_request($url, $data) {
    $ch = curl_init($url);
    //Curl request  
    $headers = [
        'KeyId:' . KEY_ID,
        'ORG_ID:' . ORG_ID,
        'Content-Type: application/json'
    ];

    //custom headers.
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        print "Error: " . curl_error($ch);
        common_log(curl_error($ch));
        exit();
    }
    curl_close($ch);
    return $result;
}

function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

function common_log($message) {

// setting error logging to be active 
    ini_set("log_errors", TRUE);

// setting the logging file in php.ini 
    ini_set('error_log', SAC_ERROR_LOG);

// logging the error 
    error_log($message);
}

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */


$app->run();