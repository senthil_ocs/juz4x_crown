<?php

date_default_timezone_set('Asia/Singapore');

# Error log file
define("SAC_ERROR_LOG", "logs/sac_app.log");

# Cipher algorithm used for pgp encryption/decryption
define("CIPHER_ALGO", "AES256");

# Digest algorithm used for pgp encryption/decryption
define("DIGEST_ALGO", "SHA256");

# Compression algorithm type used for pgp encryption/decryption
define("COMPRESS_ALGO", "ZIP");

# Synchronous api url to server
define("SYNC_API_URL", "https://api-ideal-staging.dbs.com/rapid/stpsg/v1/payment/initiatePayment");
define("FXRATE_API_URL", "https://api-ideal.staging.dbs.com/rapid/forex/v1/rfq/rates");

# Asynchronous api url to server
define("ASYNC_API_URL", "https://api-ideal-staging.dbs.com/rapid/stpsg/v1/payment/initiatePayment");
/*define("ASYNC_API_URL", "http://client-api.test/rapid/stpsg/v1/payment/initiatePayment");*/


# Asynchronous call back url
define("ASYNC_CALLBACK", "/asynchronous_callback");

# Notification url
define("NOTIFY_CALLBACK", "/notification");

# Server key id for encryption and decryption
define("SER_KEY_ID", "DBSSG@dbs.com");

# Server key password
define("SER_PVT_KEY_PASS", "K7O5W8866b");

# Client key id for encryption and decryption
define("CLI_KEY_ID", "durai@juzapps.com");

# Client private key password for encryption and decryption
define("CLI_PVT_KEY_PASS", "Admin!@#$%");

# Key id provided by DBS
define("KEY_ID", "ac89c4a6-d382-4f5c-b2dc-b221a602b19d");
define("KEY_ID_FX", "2b3fe66d-2374-4602-8682-123de432b567");

# Organization id provided by DBS 
define("ORG_ID", "ARAMCOP3");


?>

