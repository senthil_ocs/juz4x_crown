<?php 
	/*	Class Function for Mysql Functions	*/
class MysqlFns extends apiCommon
{
	var $ConLink;
	function MysqlFns()
	{
		if(!$this->ConLink)
			$this->makeConnection();
	}
	
	function makeConnection()
	{
		
		$connectionInfo = array( "Database"=>DB_DATABASE, "UID"=>DB_USERNAME, "PWD"=>DB_PASSWORD);
     	if (!$this->ConLink = sqlsrv_connect(DB_HOSTNAME, $connectionInfo)) {
			exit('Error: Could not make a database connection using ' . HQDB_USERNAME . '@' . DB_HOSTNAME);
		}

		@sqlsrv_query("SET NAMES 'utf8'", $this->ConLink);
		@sqlsrv_query("SET CHARACTER SET utf8", $this->ConLink);

	}
	function SelectQryMSSQL($Qry) {
		$resource = sqlsrv_query($this->ConLink,$Qry);		
			if (is_resource($resource)) {
				$i = 0;

				$data = array();
				while ($result = sqlsrv_fetch_array($resource,SQLSRV_FETCH_ASSOC)) {
					$data[] = $result;
				}
				return $data;	
			} else {
				return true;
			}
	}
	public function query($sql) {

		//echo "<br>".$sql;
		$resource = sqlsrv_query($this->ConLink,$sql);

		if ($resource) {
			if (is_resource($resource)) {
				$i = 0;

				$data = array();
				while ($result = sqlsrv_fetch_array($resource,SQLSRV_FETCH_ASSOC)) {
					$data[$i] = $result;

					$i++;
				}

				//sqlsrv_free_result($resource);

				$query = new stdClass();
				$query->row = isset($data[0]) ? $data[0] : array();
				$query->rows = $data;
				$query->num_rows = $i;

				unset($data);

				return $query;	
			} else {
				return true;
			}
		} else {
			//trigger_error('Error: ' . ''. '<br />' . $sql);
			//exit();
		}
	} 
	public function getLastId() {
		$last_id = false;

		$resource =  sqlsrv_query($this->ConLink,"SELECT @@identity AS id");

		if ($row = sqlsrv_fetch_array($resource)) {
			$last_id = trim($row[0]);
		}

		return $last_id;
	}	
	
}
?>
