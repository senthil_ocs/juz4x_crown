function GetModalDetails(type) {    
    setTimeout(function(){ $('#table_getcustomer').DataTable().search('').draw(); }, 100);
    $('#table_getcustomer').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'Custcode', name: 'Custcode'},
            {data: 'CustName', name: 'CustName'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}
function GetledgerDetails(type,name) {    
    setTimeout(function(){ $('#table_getcustomer').DataTable().search('').draw(); }, 100);
    $('#table_getcustomer').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
                d.name = name;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'Custcode', name: 'Custcode'},
            {data: 'CustName', name: 'CustName'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}

function GetRemarkDetails(type,name) {    
    setTimeout(function(){ $('#table_getremark').DataTable().search('').draw(); }, 100);
    $('#table_getremark').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
                d.name = name;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'Name', name: 'Name'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}

function GetAgentDetails(type) {    
    setTimeout(function(){ $('#table_getagent').DataTable().search('').draw(); }, 100);
    var code = $('#customer_code').val();
    $('#table_getagent').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
                d.ccode = code;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false,title:'S no'},
            {data: 'Custcode', name: 'Custcode', title:'Agent Code'},
            {data: 'CustName', name: 'CustName', title:'Agent Name'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}

function GetCurrencyModal(type) {
    setTimeout(function(){ $('#table_getcurrency').DataTable().search('').draw(); }, 100);
    $('#table_getcurrency').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'CurrencyCode', name: 'CurrencyCode'},
            {data: 'CurrencyName', name: 'CurrencyName'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}

function GetCashCustomerModal(type) {
    setTimeout(function(){ $('#table_cashcustomer').DataTable().search('').draw(); }, 100);
    $('#table_cashcustomer').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'PPNo', name: 'PPNo'},
            {data: 'Name', name: 'Name'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}


function GetLocationModalDetails(type) {
    console.log(type)
    setTimeout(function(){ $('#table_location').DataTable().search('').draw(); }, 100);
    $('#table_location').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'LocationCode', name: 'LocationCode'},
            {data: 'LocationName', name: 'LocationName'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);

}

function GetCustomerContactaModal(type) {
    console.log(type)
    setTimeout(function(){ $('#table_customercontact').DataTable().search('').draw(); }, 100);
    var id = $('input[name="customer_code"]').val();
    $('#table_customercontact').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
                d.id   = id;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'ContactNricNo', name: 'ContactNricNo'},
            {data: 'ContactName', name: 'ContactName'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}

function GetUserModal(type) {
    console.log(type)
    setTimeout(function(){ $('#table_usermodal').DataTable().search('').draw(); }, 100);
    var id = $('input[name="customer_code"]').val();
    $('#table_usermodal').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
                d.id   = id;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'username', name: 'username'},
            {data: 'Description', name: 'Description'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}

function GetProfileModal(type) {
    console.log(type)
    setTimeout(function(){ $('#table_profilemodal').DataTable().search('').draw(); }, 100);
    var id = $('input[name="profileid"]').val();
    $('#table_profilemodal').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
                d.id   = id;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'ProfileId', name: 'ProfileId'},
            {data: 'Description', name: 'Description'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}

function GetBeneficiaryModal(type) {
    setTimeout(function(){ $('#table_beneficiary').DataTable().search('').draw(); }, 100);
    var customer_code = $('input[name="customer_code"]').val();
    var pno_nric = $('input[name="pno_nric"]').val();
    $('#table_beneficiary').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
                d.customer_code = customer_code;
                d.pno_nric = pno_nric;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'beneficiary_id', name: 'beneficiary_id'},
            {data: 'BeneName', name: 'BeneName'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}

function GetOrginatorModal(type) {
    setTimeout(function(){ $('#table_orginator').DataTable().search('').draw(); }, 100);
    var customer_code = $('input[name="customer_code"]').val();
    var pno_nric = $('input[name="pno_nric"]').val();
    var pp_nric = $('input[name="pp_nric"]').val();
    $('#table_orginator').DataTable({
        paging    : false,
        destroy   : true,
        serverSide: true,
        autoWidth : false,
        info      : false,
        stateSave : true,
        scrollY: pscroll,
        ajax: {
            url: public_path,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (d) {
                d.type = type;
                d.customer_code = customer_code;
                d.pno_nric = pno_nric;
                d.pp_nric = pp_nric;
            },
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'Name', name: 'Name'},
            {data: 'NRICNo', name: 'NRICNo'},
            {data: 'DOB', name: 'DOB'},
            {data: 'Nationality', name: 'Nationality'},
            {data: 'Address', name: 'Address'},
            {data: 'Code', name: 'Code'},
        ],
    });
    setTimeout(function(){
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    },200);
}