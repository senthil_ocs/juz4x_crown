document.onkeyup = function(e) {
  if (e.altKey && e.which == 77) {
   	 $('#masterMenuDropdownSection').addClass('show');
   	 $("#masterMenuDropdownSection").css({"position": "absolute", "will-change": "transform","top":"0px","left":"0px","transform":"translate3d(0px, 37px, 0px)"});
   	 $('.dropdown').addClass('show');
  }

  if (e.altKey && e.which == 84) {
     $('#transactionMenuDropdownSection').addClass('show');
     $("#transactionMenuDropdownSection").css({"position": "absolute", "will-change": "transform","top":"0px","left":"0px","transform":"translate3d(0px, 37px, 0px)"});
     $('.dropdown').addClass('show');
  }

  if (e.altKey && e.which == 67) {
     $('#securityMenuDropdownSection').addClass('show');
     $("#securityMenuDropdownSection").css({"position": "absolute", "will-change": "transform","top":"0px","left":"0px","transform":"translate3d(0px, 37px, 0px)"});
     $('.dropdown').addClass('show');
  }

  if (e.altKey && e.which == 82) {
     $('#reportMenuDropdownSection').addClass('show');
     $("#reportMenuDropdownSection").css({"position": "absolute", "will-change": "transform","top":"0px","left":"0px","transform":"translate3d(700px, 64px, 0px)"});
     $('.dropdown').addClass('show');
  }

  if (e.altKey && e.which == 83) {
     $('#systemMenuDropdownSection').addClass('show');
     $("#systemMenuDropdownSection").css({"position": "absolute", "will-change": "transform","top":"0px","left":"0px","transform":"translate3d(0px, 38px, 0px)"});
     $('.dropdown').addClass('show');
  }


  //if (e.altKey && e.which == 82) {
    //alert("Alt + R shortcut combination was pressed");
  //}

   //if (e.which == 117) {
   	 //alert("F6 key was pressed");
  //} 

};


// hotkeys('f5', function(event, handler){
//   // Prevent the default refresh event under WINDOWS system
//   event.preventDefault() 
//   alert('you pressed F5!') 
// });

hotkeys('esc', function(event, handler){
  event.preventDefault();
    submitForm('esc');
});
