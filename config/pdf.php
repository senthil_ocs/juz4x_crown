<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Laravel Pdf',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('public/temp/'),
	'default_font_size'    => '10',
	'default_font'         => '"Nunito", sans-serif',

	'margin_left'          => 4,
	'margin_right'         => 4,
	'margin_top'           => 2,
	'margin_bottom'        => 10,

	'margin_header'        => 2,
	'margin_footer'        => 5,
];
