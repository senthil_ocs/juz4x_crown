<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->post('/user', function (Request $request) {
    return $request->user();
});*/




Route::group(['middleware' => 'Api'], function(){
	Route::post('appconfig', 'Api\LoginController@appconfig');
	Route::post('device', 'Api\LoginController@device');
	Route::post('faq', 'Api\LoginController@faq');

	Route::post('login', 'Api\LoginController@login');
	Route::post('login_verify', 'Api\LoginController@loginVerify');

	Route::post('add_application', 'Api\AddapplicationController@Add');
	Route::post('application_image', 'Api\AddapplicationController@application_image');

	Route::post('currency', 'Api\CurrencyController@index');
	Route::post('purpose', 'Api\CurrencyController@purpose');
	Route::post('get_transactions', 'Api\CurrencyController@get_transactions');
	Route::post('get_customer', 'Api\CustomerController@get_customer');
	Route::post('beneficiaryPost', 'Api\CustomerController@beneficiaryPost');

	Route::post('bank_accounts', 'Api\CurrencyController@bank_accounts');

});

