<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', function () {
    return redirect(route('login'));
});

Route::get('large-compact-sidebar/starter/blank-compact', function () {
    //session(['layout' => 'compact']);
    return view('starter.blank-compact');
})->name('compact');
Route::get('large-sidebar/starter/blank-large', function () {
    //session(['layout' => 'normal']);
    return view('starter.blank-large');
})->name('normal');
Route::get('horizontal-bar/starter/blank-horizontal', function () {
    //session(['layout' => 'horizontal']);
    return view('starter.blank-horizontal');
})->name('horizontal');


Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/company', 'system\CompanyMasterController@index')->name('company');
Route::post('updatecompany', 'system\CompanyMasterController@updatecompany')->name('updatecompany');

/* DD/TT Dashboard */
Route::get('/ttdashboard', 'DashboardController@dashboard')->name('dashboard');

/* customer_master */
Route::get('/customer_master', 'master\CustomerController@index')->name('customer_master');
Route::get('/customer_list', 'master\CustomerController@list')->name('customer_list');
Route::post('/customer_list', 'master\CustomerController@list')->name('customer_list');
Route::post('/customerExport', 'master\CustomerController@customerExport')->name('customerExport');
Route::post('/beneficiary_list', 'master\CustomerController@beneficiary_list')->name('beneficiary_list');
Route::post('createcustomer', 'master\CustomerController@createcustomer')->name('createcustomer');
Route::post('addContact', 'master\CustomerController@addContact')->name('addContact');
Route::post('getcontact', 'master\CustomerController@getcontact')->name('getcontact');
Route::post('addCustBeneficiary', 'master\CustomerController@addCustBeneficiary')->name('addCustBeneficiary');
Route::post('getCustBeneficiary', 'master\CustomerController@getCustBeneficiary')->name('getCustBeneficiary');
Route::post('fetchcustomer', 'master\CustomerController@fetchCustomer')->name('fetchcustomer');
Route::post('getcustomerdetails', 'master\CustomerController@getCustomerDetails')->name('getcustomerdetails');
Route::post('customer_master/delete', 'master\CustomerController@deleteCustomer')->name('deletecustomerdetails');


/* Currency Master */
Route::post('/postalcode', 'master\CurrencyController@postalcode')->name('postalcode');
Route::get('/currencymasterlist', 'master\CurrencyController@currencymasterlist')->name('currency_master_list');
Route::get('/currency_master', 'master\CurrencyController@index')->name('currency_master');
Route::post('/currency_master/denomination/add', 'master\CurrencyController@denominationAdd')->name('denominationadd');
Route::post('createcurrency', 'master\CurrencyController@insertCurrency')->name('createcurrency');
Route::post('fetchcurrency', 'master\CurrencyController@fetchCurrency')->name('fetchcurrency');
Route::post('getcurrencydetails', 'master\CurrencyController@getCurrencyDetails')->name('getcurrencydetails');
Route::post('deletecurrency', 'master\CurrencyController@deleteCurrency')->name('deletecurrency');
Route::post('currency_master/buysellcurrencydetails', 'master\CurrencyController@buysellcurrencydetails')->name('buysellcurrencydetails');


/*   location  Stock */
Route::get('location', 'master\LocationController@index')->name('location_stock');
Route::post('location/get_location', 'master\LocationController@getlocation');
Route::post('location/get_location_name', 'master\LocationController@get_location_name');
Route::post('location/get_location_order', 'master\LocationController@get_location_order');
Route::post('location/get_location_order_details', 'master\LocationController@get_location_order_details');
Route::post('location/get_location_order_update', 'master\LocationController@get_location_order_update');


/* Currency Master Group */
Route::get('currency_master_group', 'master\CurrencyController@currency_master_group')->name('currency_master_group');
Route::post('currency_master_group', 'master\CurrencyController@currency_master_group')->name('currency_master_group');
Route::post('currency_master_group/getdetails', 'master\CurrencyController@getdetails_currency_master_group')->name('getdetails_currency_master_group');
Route::post('currency_master_group/insert_update', 'master\CurrencyController@insert_update_currency_master_group')->name('insert_update_currency_master_group');
Route::post('currency_master_group/delete', 'master\CurrencyController@delete_currency_master_group')->name('delete_currency_master_group');


/* Nationality */
Route::get('nationality', 'master\NationalityController@view')->name('nationality.view');
Route::post('nationality', 'master\NationalityController@view')->name('nationality.view');
Route::post('nationality/getdetails', 'master\NationalityController@getdetails')->name('nationality.getdetails');
Route::post('nationality/insert_update', 'master\NationalityController@insert_update')->name('nationality.insert_update');
Route::post('nationality/delete', 'master\NationalityController@delete')->name('nationality.delete');

/*Source of Income */
Route::get('sourceofincome', 'master\SourceofincomeController@view')->name('sourceofincome.view');
Route::post('sourceofincome', 'master\SourceofincomeController@view')->name('sourceofincome.view');
Route::post('sourceofincome/getdetails', 'master\SourceofincomeController@getdetails')->name('sourceofincome.getdetails');
Route::post('sourceofincome/insert_update', 'master\SourceofincomeController@insert_update')->name('sourceofincome.insert_update');
Route::post('sourceofincome/delete', 'master\SourceofincomeController@delete')->name('sourceofincome.delete');

/*Bank Master*/
Route::get('bank', 'master\BankController@view')->name('bank.view');
Route::post('bank', 'master\BankController@view')->name('bank.view');
Route::post('bank/getdetails', 'master\BankController@getdetails')->name('bank.getdetails');
Route::post('bank/insert_update', 'master\BankController@insert_update')->name('bank.insert_update');
Route::post('bank/delete', 'master\BankController@delete')->name('bank.delete');

/*Swift Master*/
Route::get('swift', 'master\SwiftController@view')->name('swift.view');
Route::post('swift', 'master\SwiftController@view')->name('swift.view');
Route::post('swift/getdetails', 'master\SwiftController@getdetails')->name('swift.getdetails');
Route::post('swift/insert_update', 'master\SwiftController@insert_update')->name('swift.insert_update');
Route::post('swift/delete', 'master\SwiftController@delete')->name('swift.delete');
Route::post('swift/getdetailsbycode', 'master\SwiftController@getdetailsbycode')->name('swift.getdetailsbycode');
Route::post('swift/getSwiftAutoFill', 'master\SwiftController@getSwiftAutoFill')->name('swift.getSwiftAutoFill');


/* Settlement */
Route::get('settlement', 'master\SettlementModeController@view')->name('settlement.view');
Route::post('settlement', 'master\SettlementModeController@view')->name('settlement.view');
Route::post('settlement/getdetails', 'master\SettlementModeController@getdetails')->name('settlement.getdetails');
Route::post('settlement/insert_update', 'master\SettlementModeController@insert_update')->name('settlement.insert_update');
Route::post('settlement/delete', 'master\SettlementModeController@delete')->name('settlement.delete');


/* Service */
Route::get('service', 'master\ServiceController@view')->name('service.view');
Route::post('service', 'master\ServiceController@view')->name('service.view');
Route::post('service/getdetails', 'master\ServiceController@getdetails')->name('service.getdetails');
Route::post('service/insert_update', 'master\ServiceController@insert_update')->name('service.insert_update');
Route::post('service/delete', 'master\ServiceController@delete')->name('service.delete');

/* orginator */
Route::get('orginator', 'master\OrginatorController@view')->name('orginator.view');
Route::post('orginator', 'master\OrginatorController@view')->name('orginator.view');
Route::post('orginator/getdetails', 'master\OrginatorController@getdetails')->name('orginator.getdetails');
Route::post('orginator/insert_update', 'master\OrginatorController@insert_update')->name('orginator.insert_update');
Route::post('orginator/delete', 'master\OrginatorController@delete')->name('orginator.delete');

/* Buy Selling */
Route::get('buysell', 'transaction\BuySellController@view')->name('buysell.view');
Route::post('buysell/getcustomerdetails', 'transaction\BuySellController@getcustomerdetails')->name('buysell.customerdetails');
Route::post('buysell/insert_update', 'transaction\BuySellController@insert_update')->name('buysell.insert_update');
Route::post('buysell/getincurrencydetails', 'transaction\BuySellController@getincurrencydetails')->name('buysell.getincurrencydetails');
Route::post('buysell/getSGDdetails', 'transaction\BuySellController@getSGDdetails')->name('buysell.getSGDdetails');

Route::post('buysell/voucherno', 'transaction\BuySellController@voucherno')->name('buysell.voucherno');

Route::get('buyselltransactionreport', 'reports\BuySellTransactionController@index')->name('buyselltransactionreport');
Route::post('buyselltransactionreport', 'reports\BuySellTransactionController@index')->name('buyselltransactionreport');
Route::post('buyselltransactionreport/export', 'reports\BuySellTransactionController@buyselltransactionreportexport')->name('buyselltransactionreport.export');
Route::get('profitreport', 'reports\BuySellTransactionController@buysellprofitreport')->name('profitreport');
Route::post('profitreport', 'reports\BuySellTransactionController@buysellprofitreport')->name('profitreport');
Route::post('profitreport/export', 'reports\BuySellTransactionController@buysellprofitreportexport')->name('profitreport.export');
Route::get('total/profitreport', 'reports\BuySellTransactionController@totalprofit')->name('profittotal');
Route::post('total/profitreport', 'reports\BuySellTransactionController@totalprofit')->name('profittotal');

/* Get Modal Details */
Route::post('modal/getdetails', 'model\GetmodalDetailController@getdetails')->name('modal.getdetails');
Route::get('modal/getdetails', 'model\GetmodalDetailController@getdetails')->name('modal.getdetails');

/* Cash Customer */
Route::get('cashcustomer', 'master\CashCustomerController@view')->name('cashcustomer.view');
Route::get('cashcustomer_list', 'master\CashCustomerController@list')->name('cashcustomer_list');
Route::post('cashcustomer_list', 'master\CashCustomerController@list')->name('cashcustomer_list');
Route::post('cashcustomer/insert_update', 'master\CashCustomerController@insert_update')->name('cashcustomer.insert_update');
Route::post('cashcustomer/getcashcustomerdetails', 'master\CashCustomerController@getcashcustomerdetails')->name('cashcustomer.getcashcustomerdetails');
Route::post('cashcustomer/delete', 'master\CashCustomerController@delete')->name('cashcustomer.delete');
Route::post('addBeneficiary', 'master\CashCustomerController@addBeneficiary')->name('addBeneficiary');
Route::post('getBeneficiary', 'master\CashCustomerController@getBeneficiary')->name('getBeneficiary');
Route::post('addImage', 'master\CashCustomerController@addImage')->name('cashcustomer.addImage');
Route::post('removeImage', 'master\CashCustomerController@removeImage')->name('removeImage');

Route::post('getNricNo', 'master\CashCustomerController@getNricNo')->name('getNricNo');
Route::post('getNameAutoFill', 'master\CashCustomerController@getNameAutoFill')->name('getNameAutoFill');
Route::post('getPhnoAutoFill', 'master\CashCustomerController@getPhnoAutoFill')->name('getPhnoAutoFill');

/* Cash Customer edit from list */
Route::post('cashcustomer/{id}', 'master\CashCustomerController@edit')->name('cashcustomer.viewedit');

/* User Master */
Route::get('/user_master', 'master\UserController@index')->name('user_master');
Route::post('createuser', 'master\UserController@createUser')->name('createuser');
Route::post('user/getdetails', 'master\UserController@getuserdetails')->name('getuserdetails');
Route::post('deleteuser', 'master\UserController@deleteUser')->name('deleteuser');
Route::post('updateuser', 'master\UserController@updateuser')->name('updateuser');
/* Profile Master */
Route::get('/profile', 'master\ProfileController@index')->name('profile');
Route::post('createprofile', 'master\ProfileController@createProfile')->name('createprofile');
Route::post('profile/getdetails', 'master\ProfileController@getprofiledetails')->name('getprofiledetails');
Route::post('deleteprofile', 'master\ProfileController@deleteProfile')->name('deleteprofile');

/* Currency Balances */
Route::get('/balances', 'transaction\BalanceController@index')->name('currency_balance');
Route::post('/customerbalances', 'transaction\BalanceController@customerbalances')->name('customerbalances');


/* Customer Reporting */
Route::get('reports/customer', 'reports\CustomerController@index')->name('reports.customer');
Route::get('reports/ledger', 'reports\CustomerController@ledger')->name('reports.ledger');
Route::post('reports/ledger', 'reports\CustomerController@ledger')->name('reports.ledger');

Route::post('reports/ledger/getCurrency', 'reports\CustomerController@getCurrency')->name('ledger.getCurrency');
Route::post('reports/customerdetails', 'reports\CustomerController@getcustomerdetails')->name('reports.customerdetails');
Route::post('reports/customer', 'reports\CustomerController@index')->name('reports.customer');
Route::post('reports/customerexport', 'reports\CustomerController@customerexport')->name('reports.customerexport');

Route::get('reports/cashcustomer', 'reports\CustomerController@cashcustomer')->name('reports.cashcustomer');
Route::post('reports/cashcustomer', 'reports\CustomerController@cashcustomer')->name('reports.cashcustomer');
Route::get('reports/cashcustomerpdf', 'reports\CustomerController@CashCustomerPdf')->name('reports.cashcustomerpdf');
Route::get('reports/cashcustomercsv', 'reports\CustomerController@CashCustomercsv')->name('reports.cashcustomercsv');
Route::post('reports/cashcustomerexport', 'reports\CustomerController@cashcustomerexport')->name('reports.cashcustomerexport');

Route::get('reports/ledgercustomer', 'reports\CustomerController@ledgercustomer')->name('reports.ledgercustomer');
Route::post('reports/ledgercustomer', 'reports\CustomerController@ledgercustomer')->name('reports.ledgercustomer');
Route::post('reports/ledgercustomerexport', 'reports\CustomerController@ledgercustomerexport')->name('reports.ledgercustomerexport');
Route::post('reports/ledgercustomerdetails', 'reports\CustomerController@ledgercustomerdetails')->name('reports.ledgercustomerdetails');
Route::post('reports/customerbalance', 'reports\CustomerController@customerbalance')->name('reports.customerbalance');


Route::get('reports/customerbalancereport', 'reports\CustomerController@customerbalancereport')->name('reports.customerbalancereport');
Route::post('reports/customerbalancereport', 'reports\CustomerController@customerbalancereport')->name('reports.customerbalancereport');
Route::post('reports/customerbalancereportexport', 'reports\CustomerController@customerbalancereportexport')->name('reports.customerbalancereportexport');

/* Currency Report */
//Route::get('reports/locationwisecurrencyledgerreport', 'reports\CurrencyController@locationwisecurrencyledgerreport')->name('reports.locationwisecurrencyledgerreport');
Route::get('reports/ledgercurrency', 'reports\CurrencyController@ledgercurrency')->name('reports.ledgercurrency');
Route::post('reports/ledgercurrency', 'reports\CurrencyController@ledgercurrency')->name('reports.ledgercurrency');
Route::post('reports/ledgercurrencyexport', 'reports\CurrencyController@ledgercurrencyexport')->name('reports.ledgercurrencyexport');

Route::get('reports/locationStockReport', 'reports\CurrencyController@locationStockReport')->name('reports.locationStockReport');
Route::post('reports/locationStockReport', 'reports\CurrencyController@locationStockReport')->name('reports.locationStockReport');
Route::post('reports/locationStockReportExport', 'reports\CurrencyController@locationStockReportExport')->name('reports.locationStockReportExport');
Route::get('reports/currencyWiseTransactionReport','reports\CurrencyController@currencyWiseTransactionReport')->name('reports.currencyWiseTransactionReport');
Route::post('reports/currencyWiseTransactionReport','reports\CurrencyController@currencyWiseTransactionReport')->name('reports.currencyWiseTransactionReport');


Route::post('reports/currencyWiseTransactionexport', 'reports\CurrencyController@currencyWiseTransactionexport')->name('reports.currencyWiseTransactionexport');

/* Currency Report */
Route::get('dealtransactionreport', 'reports\DealTransactionController@index')->name('dealtransactionreport');
Route::post('dealtransactionreport', 'reports\DealTransactionController@index')->name('dealtransactionreport');
Route::post('dealTransactionReportExport', 'reports\DealTransactionController@dealTransactionReportExport')->name('dealtransactionreportexport');


Route::get('reports/ttmasreport', 'reports\CustomerController@ttmasreport')->name('reports.ttmasreport');
Route::post('reports/ttmasreport', 'reports\CustomerController@ttmasreport')->name('reports.ttmasreport');
Route::get('reports/tttransactionsummary', 'reports\CustomerController@tttransactionsummary')->name('reports.tttransactionsummary');
Route::post('reports/tttransactionsummary', 'reports\CustomerController@tttransactionsummary')->name('reports.tttransactionsummary');
Route::get('reports/tttransactiondetail', 'reports\CustomerController@tttransactiondetail')->name('reports.tttransactiondetail');
Route::post('reports/tttransactiondetail', 'reports\CustomerController@tttransactiondetail')->name('reports.tttransactiondetail');
Route::get('reports/ttfundsaccepted', 'reports\CustomerController@ttfundsaccepted')->name('reports.ttfundsaccepted');
Route::get('reports/ttfundsremitted', 'reports\CustomerController@ttfundsremitted')->name('reports.ttfundsremitted');
Route::get('reports/ttdeletedreport', 'reports\CustomerController@ttdeletedreport')->name('reports.ttdeletedreport');
Route::get('reports/ttbookingreport', 'reports\CustomerController@ttbookingreport')->name('reports.ttbookingreport');
Route::post('reports/ttbookingreport', 'reports\CustomerController@ttbookingreport')->name('reports.ttbookingreport');
Route::get('reports/getBookingReport', 'reports\CustomerController@getBookingReport')->name('reports.getBookingReport');


/* Deal Transaction */
Route::get('deal', 'transaction\DealController@view')->name('dealview');
Route::post('deal/voucherno', 'transaction\DealController@voucherno')->name('dealvoucherno');
Route::post('deal/getcustomerdetails', 'transaction\DealController@getcustomerdetails')->name('dealcustomerdetails');
Route::post('deal/insert_update', 'transaction\DealController@insert_update')->name('dealinsert_update');
Route::post('deal/getincurrencydetails', 'transaction\DealController@getincurrencydetails')->name('dealgetincurrencydetails');
Route::post('deal/getSGDdetails', 'transaction\DealController@getSGDdetails')->name('dealgetSGDdetails');
Route::post('deal/releasedeal', 'transaction\DealController@releasedeal')->name('releasedeal');
Route::post('deal/getDealDetail', 'transaction\DealController@getDealDetail')->name('getDealDetail');
Route::post('deal/dealinsertupdate', 'transaction\DealController@dealinsertupdate')->name('dealinsertupdate');
Route::get('deallist', 'transaction\DealController@index')->name('deallist');
Route::post('deallist', 'transaction\DealController@index')->name('deallist');
Route::get('deallist/edit/{id}', 'transaction\DealController@edit')->name('deallistedit');
Route::post('deallist/update', 'transaction\DealController@update')->name('deallistupdate');
Route::post('deallist/status_update', 'transaction\DealController@statusupdate')->name('dealliststatusupdate');
Route::post('deallist/export', 'transaction\DealController@deallistexport')->name('deallistexport');



/* DD Transactions */
Route::get('ddtransaction', 'transaction\DDtransactionController@index')->name('ddtransaction');
Route::post('ddtransaction/getCustDealDetail', 'transaction\DDtransactionController@getCustDealDetail')->name('ddtransaction.getCustDealDetail');
Route::post('ddtransaction/getAgentDealDetail', 'transaction\DDtransactionController@getAgentDealDetail')->name('ddtransaction.getAgentDealDetail');

Route::get('ddtransaction/AptDDtranscation', 'transaction\DDtransactionController@AptDDtranscation')->name('AptDDtranscation');
Route::post('ddtransaction/getagentcode', 'transaction\DDtransactionController@getagentcode')->name('ddtransaction.getagentcode');
Route::post('ddtransaction/insert', 'transaction\DDtransactionController@insert')->name('ddtransaction.insert');
Route::post('ddtransaction/update', 'transaction\DDtransactionController@update')->name('ddtransaction.update');
Route::post('ddtransaction/getddtransaction', 'transaction\DDtransactionController@getddtransaction')->name('ddtransaction.getddtransaction');
Route::post('ddtransaction/getDDTransactionNric', 'transaction\DDtransactionController@getDDTransactionNric')->name('ddtransaction.getDDTransactionNric');
Route::post('ddtransaction/convertToPost', 'transaction\DDtransactionController@convertToPost')->name('ddtransaction.convertToPost');
Route::post('ddtransaction/changeBeneficiary', 'transaction\DDtransactionController@changeBeneficiary')->name('ddtransaction.changeBeneficiary');
Route::post('ddtransaction/updateRemark', 'transaction\DDtransactionController@updateRemark')->name('ddtransaction.updateRemark');
Route::any('ddtransaction/exportpostlist', 'transaction\DDtransactionController@exportpostlist')->name('ddtransaction.exportpostlist');

Route::get('ddtransaction/unpost', 'transaction\DDtransactionController@unpost')->name('ddtransaction.unpost');
Route::post('ddtransaction/unpost_insert', 'transaction\DDtransactionController@unpost_insert')->name('ddtransaction.unpost_insert');
Route::post('ddtransaction/unpost_update', 'transaction\DDtransactionController@unpost_update')->name('ddtransaction.unpost_update');
Route::post('ddtransaction/getddtransactionpost', 'transaction\DDtransactionController@getddtransactionpost')->name('ddtransaction.getddtransactionpost');

Route::post('ddtransaction/getbeneficiary', 'transaction\DDtransactionController@getbeneficiary')->name('ddtransaction.getbeneficiary');
Route::post('ddtransaction/getRefNo', 'transaction\DDtransactionController@getRefNo')->name('ddtransaction.getRefNo');
Route::post('ddtransaction/getunpostRefNo', 'transaction\DDtransactionController@getunpostRefNo')->name('ddtransaction.getunpostRefNo');
Route::post('ddtransaction/getCurrency', 'transaction\DDtransactionController@getCurrency')->name('ddtransaction.getCurrency');

Route::get('ddtransaction/posttransaction', 'transaction\DDtransactionController@posttransaction')->name('ddtransaction.posttransaction');
Route::post('ddtransaction/updatetransaction', 'transaction\DDtransactionController@updatetransaction')->name('ddtransaction.updatetransaction');
Route::post('ddtransaction/updateDeal', 'transaction\DDtransactionController@updateDeal')->name('ddtransaction.updateDeal');

Route::any('ddtransaction/searchlist', 'transaction\DDtransactionController@searchlist')->name('ddtransaction.searchlist');
Route::any('ddtransaction/unpostsearchlist', 'transaction\DDtransactionController@unpostsearchlist')->name('ddtransaction.unpostsearchlist');
Route::any('ddtransaction/purpose', 'transaction\DDtransactionController@purpose')->name('ddtransaction.purpose');
Route::any('ddtransaction/purpose_insert', 'transaction\DDtransactionController@purpose_insert')->name('ddtransaction.purpose_insert');
Route::any('ddtransaction/getpurposedata', 'transaction\DDtransactionController@getpurposedata')->name('ddtransaction.getpurposedata');
Route::any('ddtransaction/deletepurposedata', 'transaction\DDtransactionController@deletepurposedata')->name('ddtransaction.deletepurposedata');


/* For Developers */
Route::get('/clear', 'system\DevelopersController@index')->name('clear');
Route::post('/clearlog', 'system\DevelopersController@clearlog')->name('clearlog');