var elixir = require('laravel-elixir');
 
elixir(function(mix) {
    mix.styles([
        "assets/styles/css/themes/lite-purple.css",
        "assets/styles/vendor/perfect-scrollbar.css",
        "assets/styles/vendor/toastr.css",
        "assets/styles/vendor/toastr.min.css"
    ], 'public/assets/styles/all.css');
 
min.version(["public/assets/styles/all.css"]);
 
});