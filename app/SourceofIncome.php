<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SourceofIncome extends Model
{
	const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
    protected $table    = 'sourceofincome'; 
    protected $fillable = ['Name','CreateUser','CreateDate','ModifyUser','ModifyDate'];
}

