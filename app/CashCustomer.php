<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashCustomer extends Model
{
	const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
    protected $table    = 'cash_customer';
    protected $fillable = [
    	'id','CustCode', 'PPNo', 'Name', 'DOB', 'Nationality', 'Address1', 'Address2', 'Address3', 'Address4', 'MCType', 'PhoneNo', 'PendingCustomerRemarks', 'Activebit', 'IdentifierType', 'IdentifierTypeOther', 'DOE', 'UnitNo', 'CreateDate', 'CreateUser', 'ModifyDate', 'ModifyUser','Intelligence','Allow'
	];
}
