<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purpose extends Model
{
    protected $table    = 'purpose';
    public $timestamps = false;
    const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
    protected $fillable = ['Name','Status'];
}
