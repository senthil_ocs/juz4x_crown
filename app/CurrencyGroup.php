<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyGroup extends Model
{
    protected $table    = 'currency_group'; 
    protected $fillable = [
        'GroupCode', 'Description', 'CreateUser', 'CreateDate', 'ModifyUser', 'ModifyDate'
    ];
}
