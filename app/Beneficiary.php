<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    protected $table    = 'beneficiary';
    protected $fillable = [
    	'BeneId', 'CustCode',	'CustNric', 'NRICNo','Name','Address1','Address2','Country','BankName','BankBranch','BankAcNo','SwiftCode'
	];
}
