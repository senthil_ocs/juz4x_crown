<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileUser extends Model
{
    protected $table    = 'profile_users'; 
    public $timestamps = false;
    protected $fillable = [
        'UserId', 'Description', 'Password', 'ProfileId', 'UserType', 'Active', 'Administrator', 'CreatedBy'
    ];
    
}
