<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationCurrencyTransaction extends Model
{
    protected $table    = 'location_currency_transaction';
    public $timestamps = false;
    protected $fillable = [
        'LocationCode', 'CurrencyCode', 'Date', 'OpeningStock', 'OpeningAvgCost', 'BuyFamount', 'BuyLamount','SellFAmount', 'SellLAmount','OpeningDealStock','CreatedBy', 'CreatedDate','ModifiedBy', 'ModifiedDate'
    ];    
}
