<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealTransactionHeader extends Model
{
    protected $table    = 'deal_tran_header';
    public $timestamps = false; 
    protected $fillable = [
        'DealNo', 'DealDate', 'NRICNo', 'ContactName', 'CustomerCode', 'CustomerName', 'LocationCode','TerminalCode', 'CreatedBy','CreatedDate','ModifiedBy', 'ModifiedDate'
    ];
}
