<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table    = 'currency_master'; 
    protected $fillable = [
        'CurrencyCode', 'CurrencyName', 'Major', 'Minor', 'LocalCurrency', 'Varience', 'BuyRate','SellRate', 'Units','Stock', 'AvgCost','LastPdate', 'LastSDate','TTCurrency', 'TTCustomerCode','TTCustomerUpdate', 'DealStock','Denomination', 'CurrencyGroup','Region', 'CreatedBy','CreatedDate', 'ModifiedBy','ModifiedDate'
    ];
}
