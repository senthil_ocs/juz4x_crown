<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denomination extends Model
{
    protected $table    = 'denomination'; 
    protected $fillable = [
        'CurrencyCode', 'Denomination', 'Description'
    ];
}
