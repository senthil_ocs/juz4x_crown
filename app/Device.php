<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table    = 'devices'; 
    public $timestamps = false;
    const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';

	protected $fillable = [
		'user_id', 'mobile_no', 'device_type', 'device_token', 'version'
	];
}
