<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table    = 'customer_master'; 
    public $timestamps = false;
    protected $fillable = [
        'Custcode', 'CustName', 'CompanyRegNo', 'IncorporationDate', 'IncorporationPlace', 'NatureOfbusiness', 'CompAddress1','CompAddress2', 'CompAddress3','Country', 'CompPhone1','CompFax', 'CompEmail','MoneyChangerLicense', 'RemittanceLicensee','Name', 'NRICNo','Nationality', 'Address1','Address2', 'Address3','Address4', 'Address5','UnitNo','Phone','Fax','HandPhone','Email','DOB','Position','NameOfcompany','SourceOfIncome','Ob','CreditLimit','Remarks','TTCustomer','TradingLicense','MCType','TTType','DailyCreditLimit','WebCustAllowed','SpecimenSignature','AuthorisedSignature','Active','CreatedBy','CreatedDate','ModifiedBy','ModifiedDate','CompPhone2','CompPostalCode','PostalCode','EmploymentDetail','YearlyIncome','CheckList','BusinessType','LicenseValidFrom','LicenseValidTo','IssuingAuthority','OverSeasBranches','LocalBranchesCount','AnnualTurnOver','TypeOfIdentification','OverSeasCompAddress','OverSeasCountry','OverSeasPostalCode','OverSeasTelephone','BuySelltype','BannedNo','TypeOfCustomer','CompanySourceOfFunds','top3ForeignCurrencies','DealingswithLocalorForeignGovernment','EmailAddress','Website','TotalNoOfEmployees','Top3Countries','ReasonForReTT','RemitananceVolume','SGDVolume','PCActive','PendingCustomerRemarks','HighRisk','Agent'
    ];
    
    public function CLocation() {
        return $this->hasMany('\App\LocationCustomer','Custcode','Custcode');
    }

    public function CContact() {
        return $this->hasMany('\App\CustomerContact','CustCode','Custcode');
    }

    public function Beneficiary()
    {
        return $this->hasMany('\App\Beneficiary','CustCode','Custcode');   
    }

    public function Nationalitys()
    {
        return $this->hasOne('\App\Nationality','Code','Nationality');   
    }
}
