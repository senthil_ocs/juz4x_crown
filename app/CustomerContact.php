<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerContact extends Model
{
    protected $table    = 'customer_contact'; 
    public $timestamps = false;
    protected $fillable = [
        'CustCode', 'ContactNricNo', 'ContactName', 'ContactNationality', 'DOB', 'CreatedBy', 'ModifiedBy','ModifiedDate', 'ContactType','ContactPosition', 'ContactSignature','Active', 'Remarks','DOE'
    ];
}
