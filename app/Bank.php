<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
	const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
    protected $table    = 'bank'; 
    protected $fillable = ['Bank','Description','CreateUser','CreateDate','ModifyUser','ModifyDate'];
}

