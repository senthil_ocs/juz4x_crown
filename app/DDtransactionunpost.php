<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DDtransactionunpost extends Model
{
    protected $table    = 'dd_transaction_unpost';
    public $timestamps = false;
    const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
    protected $fillable = [
    	'TranNo','DocType','TranType','CustCode','CustPPNo','CustNationality','Purpose','CustTTType','CompName','CompAddress1','CompAddress2','CompAddress3','CompAddress4','BenName','BenAddress1','BenAddress2','BenAddress3','BenPPNo','AgentCode','AgentTTType','AgentRate','AgentPayStatus','MessageCode','CurrencyCode','ExchRate','FAmount','LAmount','Comm','Other','Service','Postal','TotalAmount','AgentCommission','AgentOther','AgentService','AgentPostal','DDNo','TranDate','TransmissionDate','PaymentDate','BenBank','BenAccNo','BenBankBranch','MCRef1','MCRef2','DOB','Terminalname','PaidStatus','PaidBy','CreatedBy','CreatedDate','ModifiedBy','ModifiedDate','Telephone','Remarks','SwiftCode'
	];
}