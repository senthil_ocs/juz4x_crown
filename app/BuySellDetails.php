<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuySellDetails extends Model
{
    protected $table    = 'buysell_details'; 
    public $timestamps = false;
    const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';

    protected $fillable = [
        'TranNo', 'Line', 'TranType', 'CurrencyCode', 'Rate', 'FAmount', 'LAmount','TTRefNo', 'LastAverageCost', 'CreatedBy', 'CreatedDate','ModifiedBy','ModifiedDate'
    ];

    public function buysellheader()
    {
    	return $this->hasOne('App\BuySell', 'TranNo', 'TranNo');
    }
}
