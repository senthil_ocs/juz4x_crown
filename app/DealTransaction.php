<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealTransaction extends Model
{
    protected $table    = 'deal_transaction';
    public $timestamps = false; 
    protected $fillable = [
        'DealTranNo', 'TranNo', 'TranType', 'CurrencyCode', 'Rate', 'FAmount', 'LAmount','TranAmount', 'Remarks','Mode','ReferenceNo','CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate'
    ];
}
