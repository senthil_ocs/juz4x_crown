<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\CurrencyGroup;
use Validator;
use App\Customer;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //echo 'sdfdsfsd'.Auth::id();
        // return view('home');
        return view('starter.dashboard');
    }

    public function dashboard()
    {
        $customer = Customer::limit(4)->get();
        return view('starter.dashboard', compact('customer'));
    }

    public function ttpayment()
    {
        require_once 'Crypt/GPG.php';
        $gpg = new Crypt_GPG(array('digest-algo' => 'SHA256', 'cipher-algo' => 'AES256'));
        printArray('tet');
        printArray($gpg);die;
    }


    // public function currency_master_group_view()
    // {
    //     $currencydetails =  CurrencyGroup::orderBy('id', 'DESC')->get();
    //     // printArray($currencydetails->toArray()s);die;
    //     return view('master.currency_master', compact('currencydetails'));
    // }
  
    // public function currency_master_group_crud(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'group_name' => 'required',
    //         'description' => 'required',
    //     ]);
    //     if($validator->fails()){
    //         if($validator->errors()->has('group_name')){
    //             $request->session()->flash('message.level', 'danger');
    //             $request->session()->flash('message.content', 'Error:'.$validator->errors()->first('group_name'));
    //         }
    //         return redirect()->back()->withInput()->with('message',$validator->messages()->first());
    //     } else {
    //         if($request->currency_master_group_id == ''){
    //              $CurrencyGroup = new CurrencyGroup();
    //                 $CurrencyGroup->GroupCode   = $request->group_name;
    //                 $CurrencyGroup->Description = $request->description;
    //                 $CurrencyGroup->CreateUser  = 'ups';
    //                 $CurrencyGroup->CreateDate  = now();
    //                 $CurrencyGroup->ModifyUser  = 'ups';
    //                 $CurrencyGroup->ModifyDate  = now();
    //                 $CurrencyGroup->save();
    //                 $request->session()->flash('message.level', 'success');
    //                 $request->session()->flash('message.content', 'Location Details inserted successfully!'); 
    //         } else {                            
    //             if($request->type == 'delete'){
    //                 CurrencyGroup::where('id', $request->currency_master_group_id)->delete();
    //                 $request->session()->flash('message.level', 'success');
    //                 $request->session()->flash('message.content', 'Location Details Delete successfully!'); 
    //             } else {
    //                 CurrencyGroup::where('id', $request->currency_master_group_id)->update([
    //                     'GroupCode' => $request->group_name,
    //                     'Description' => $request->description,
    //                     'ModifyUser'  => 'ups',
    //                     'ModifyDate'  => now(),
    //                 ]);
    //                 $request->session()->flash('message.level', 'success');
    //                 $request->session()->flash('message.content', 'Location Details Update successfully!'); 
    //             }
    //         }
    //         return redirect()->route('currency_master_group');
    //     }
    // }

    // public function getdetails_currency_master_group(Request $request)
    // {
    //     $id = $request->id;
    //     $data = CurrencyGroup::where('id', $id)->first();
    //     return response()->json(['status' => '200', 'details' => $data]);
    // }
}
