<?php

namespace App\Http\Controllers\reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Config;
use App\Nationality;
use App\SettlementMode;
use App\Customer;
use App\DealTransactionHeader;
use App\Currency;
use App\Location;
use App\DealTransactionDetail;
use App\LocationCurrency;
use App\LocationCustomer;
use App\CurrencyTransaction;
use App\LocationCurrencyTransaction;
use App\LocationCustomerTransaction;
use DB;
use App\BuySell;
use App\BuySellDetails;
use Auth;
use PDF;
use Carbon\Carbon;

class DealTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {   
        
        $fromDate = $request->from_date ? Carbon::parse($request->from_date)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->to_date ? Carbon::parse($request->to_date)->endOfDay() : Carbon::parse(now())->endOfDay();
        $alllocations = Location::get();
        $from = Carbon::parse($request->from_date)->startOfDay();
        $to = Carbon::parse($request->to_date)->endOfDay();
        $finaldata = array();
        if ($request->ajax()) {
           
           /* $datas = DealTransactionHeader::where('CustomerCode',$request->cust_code);
            if(!empty($request->from_date)) {
                $datas = $datas->where('CreatedDate', '>=', $from)->where('CreatedDate', '<=', $to);
            }
            if(!empty($request->user)) {
                $datas = $datas->where('CreatedBy',$request->user);
            }
            $datas = $datas->orderBy('CreatedDate')->get();*/
            
         /*   if(count($datas) > 0) {
                $datas = $datas->toArray();
                foreach ($datas as $headerdata) {*/

                    $detaildata = DB::table('deal_trans_detail')
                    ->select('deal_trans_detail.TranNo','deal_trans_detail.Status','deal_trans_detail.DealNo','deal_trans_detail.ValidTill','deal_trans_detail.Remarks','deal_trans_detail.CreatedBy', 'deal_trans_detail.TranType as TranType','deal_trans_detail.TranNo as realisedTranNo','deal_trans_detail.CurrencyCode as CurrencyCode','deal_trans_detail.Rate','deal_trans_detail.FAmount','deal_trans_detail.LAmount','deal_trans_detail.Rate','deal_trans_detail.CreatedDate','deal_trans_detail.CreatedBy','deal_trans_detail.BalanceAmount','deal_trans_detail.RealisedAmount','deal_tran_header.CustomerName','deal_trans_detail.CreatedDate as tranDate','deal_trans_detail.Remarks');

                    
                    //$detaildata  = $detaildata->leftJoin('deal_trans_detail', 'deal_trans_detail.TranNo', '=', 'deal_transaction.DealTranNo');
                    $detaildata  = $detaildata->leftJoin('deal_tran_header', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo');

                     if(!empty($request->cust_code)) {
                        $detaildata = $detaildata->where('deal_tran_header.CustomerCode',$request->cust_code);
                    }

                    if(!empty($request->from_date) && !empty($request->to_date)) {

                        $detaildata = $detaildata->whereBetween('deal_tran_header.CreatedDate', [Carbon::parse($request->from_date)->startOfDay(), Carbon::parse($request->to_date)->endOfDay()]);
                        // $detaildata = $detaildata->whereDate('deal_transaction.CreatedDate', '>=', $from)->where('deal_transaction.CreatedDate', '<=', $to);
                    }
                    if(!empty($request->user)) {
                        $detaildata = $detaildata->where('deal_tran_header.CreatedBy',$request->user);
                    }
                    if(!empty($request->trantype)) {
                        $detaildata = $detaildata->where('deal_trans_detail.TranType',$request->trantype);
                    }
                    if(!empty($request->currency_code)) {
                        $detaildata = $detaildata->where('deal_trans_detail.CurrencyCode',$request->currency_code);
                    }
                    if(!empty($request->status)) {
                        $detaildata = $detaildata->where('deal_trans_detail.Status',$request->status);
                    } else {
                        $detaildata = $detaildata->where('deal_trans_detail.Status','Open')->orWhere('deal_trans_detail.Status','Partial');
                    }
                    if(!empty($request->valid_from) && !empty($request->valid_to)) {
                        $detaildata = $detaildata->whereBetween('deal_trans_detail.ValidTill', [Carbon::parse($request->valid_from)->startOfDay(), Carbon::parse($request->valid_to)->endOfDay()]);
                        // $validfrom = Carbon::parse($request->valid_from)->startOfDay();
                        // $validto   = Carbon::parse($request->valid_to)->endOfDay();
                        // $detaildata = $detaildata->where('deal_trans_detail.ValidTill', '>=', $validfrom)->where('deal_trans_detail.ValidTill', '<=', $validto);
                    }
                    /*printArray(Carbon::parse($request->valid_from)->startOfDay());
                    printArray(Carbon::parse($request->valid_to)->endOfDay());
                    printArray($detaildata->toSql());die;*/
                    $detaildata = $detaildata->get()->toArray();
                    //printArray($detaildata);die;
                    if(count($detaildata) > 0) {
                        $alldata = array();
                        foreach ($detaildata as $details) {
                           $alldata['TranNo'] = $details->TranNo;
                           $alldata['DealNo'] = $details->DealNo;
                           $alldata['TranType'] = $details->TranType;
                           $alldata['Time'] = displayCorrectTimeFormat($details->CreatedDate);
                           $alldata['User'] = $details->CreatedBy;
                           $alldata['Customer'] = $details->CustomerName;
                           $alldata['Currency'] = $details->CurrencyCode;
                           $alldata['Rate'] = $details->Rate;
                           $alldata['FAmount'] = $details->FAmount;
                           $alldata['LAmount'] = $details->LAmount;
                           $alldata['ValidTill'] = displayCorrectDateFormat($details->ValidTill);
                           $alldata['Status'] =  $details->Status;
                           $alldata['Date'] = displayCorrectDateFormat($details->CreatedDate);
                           $alldata['tranDate'] = displayCorrectDateFormat($details->tranDate);
                           $alldata['User'] = $details->CreatedBy;
                           $alldata['Balance'] =  $details->BalanceAmount;
                           $alldata['RealisedAmount'] =  $details->RealisedAmount;
                           $alldata['Remarks'] =  $details->Remarks;

                           $finaldata[] = $alldata;
                        }
                    }
            //     }
            // }
            $alldata = $finaldata;
            /*printArray($alldata); 
            exit;*/
            return Datatables::of($alldata)
                ->addIndexColumn()
                    ->editColumn('DealNo', function ($alldata){
                        return $alldata['DealNo'];
                    })
                   
                    ->editColumn('TranType', function ($alldata) {
                        return $alldata['TranType'];
                    })
                    ->editColumn('Remarks', function ($alldata) {
                        return $alldata['Remarks'];
                    })
                    ->editColumn('User', function ($alldata) {
                        return $alldata['User'];
                    })
                    ->editColumn('Customer', function ($alldata) {
                        return $alldata['Customer'];
                    })
                    ->editColumn('Currency', function ($alldata) {
                        return $alldata['Currency'];
                    })
                    ->editColumn('Rate', function ($alldata) {
                        return currency_format($alldata['Rate']);
                    })
                    ->editColumn('FAmount', function ($alldata) {
                        return currency_format($alldata['FAmount'],2);
                    })
                    ->editColumn('LAmount', function ($alldata) {
                        return currency_format($alldata['LAmount'],2);
                    })
                    ->editColumn('ValidTill', function ($alldata) {
                        return $alldata['ValidTill'];
                    })
                    ->editColumn('Status', function ($alldata) {
                        return $alldata['Status'];
                    })
                    ->editColumn('RealisedAmount', function ($alldata) {
                        return  currency_format($alldata['RealisedAmount'],2);
                    })
                    ->editColumn('Date',function ($alldata)use($request) {
                        return ($request->status == 'Open' || $request->status == 'Partial' )?$alldata['tranDate'] : $alldata['Date'];
                    })
                    ->editColumn('User', function ($alldata) {
                        return $alldata['User'];
                    }) 
                     ->editColumn('Balance', function ($alldata) {
                        return currency_format($alldata['Balance'],2);
                    })                  
                    ->setRowId(function ($alldata) {
                        return $alldata['TranNo'];
                    })->setRowClass(function () {
                        return 'tableclicked';
            })->make(true);
                  }   
            $fromDate = date_format($fromDate,"d-m-Y");
            $toDate = date_format($toDate,"d-m-Y");
            if(env('ENABLE_DEAL') == 1){
                return view('reports.dealtransactionreport',compact('alllocations','fromDate','toDate'));
            } else {
                return redirect()->route('home');   
            }
    }
    public function dealTransactionReportExport(Request $request)
    {
        $fromDate = $request->from_date ? Carbon::parse($request->from_date)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->to_date ? Carbon::parse($request->to_date)->endOfDay() : Carbon::parse(now())->endOfDay();
        /*if($request->cust_code){*/
            $data = DB::table('deal_tran_header');
            $data  = $data->leftJoin('deal_trans_detail', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo');
            if(!empty($request->cust_code)){
                $data  = $data->where('deal_tran_header.CustomerCode',$request->cust_code);
            }
            if(!empty($request->currency_code)){
                $data  = $data->where('deal_trans_detail.CurrencyCode',$request->currency_code);
            }
            if(!empty($request->transtype)){
                $data  = $data->where('deal_trans_detail.TranType',$request->transtype);
            }
            
            if(!empty($request->from_date) && !empty($request->to_date)){
                $data  = $data->whereBetween('deal_tran_header.DealDate', [Carbon::parse($request->from_date)->startOfDay(), Carbon::parse($request->to_date)->endOfDay()]);
            }
            $data = $data->orderBy('deal_tran_header.CreatedDate')->get();
            
            if($request->export == 'PDF' && count($data) > 0 ){
                $fcurr = 0; 
                $sell = 0; 
               if(count($data) > 0) {
                    foreach ($data as $details) {
                       $alldata =array();
                       $alldata['TranNo'] = $details->TranNo;
                       $alldata['DealNo'] = $details->DealNo;
                       $alldata['TranType'] = $details->TranType;
                       $alldata['Remarks'] = ($details->Remarks);
                       $alldata['User'] = $details->CreatedBy;
                       $alldata['Customer'] = $details->CustomerName;
                       $alldata['Currency'] = $details->CurrencyCode;
                       $alldata['Rate'] = currency_format($details->Rate);
                       $alldata['FAmount'] = currency_format($details->FAmount,2);
                       $alldata['LAmount'] = currency_format($details->LAmount,2);
                       $alldata['ValidTill'] = displayCorrectDateFormat($details->ValidTill);
                       $alldata['Status'] = $details->Status;
                       $alldata['Date'] = displayCorrectDateFormat($details->CreatedDate);
                       $alldata['User'] = $details->CreatedBy;
                       $alldata['Balance'] = currency_format($details->BalanceAmount,2);
                       $alldata['RealisedAmount'] = currency_format($details->RealisedAmount,2);
                       $finaldata[displayCorrectDateFormat($details->CreatedDate)][] = $alldata;
                    }
                }
                $data =$finaldata;
               /* $fcurr =  currency_format($fcurr,2);
                $sell = currency_format($sell,2);*/
                $strFile  = 'dealtransactionreport.pdf';
                $pdf = PDF::loadView('pdf/ledger/dealtransactionreport',compact('data'));
                return $pdf->stream($strFile);
            } else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'No data available');
                return Redirect::back();
            }
        /*} else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Please select customer details');
            return Redirect::back();
        }*/
    }
    /* END LEDGER CUSTOMER*/

    public function view(Request $request)
    {   
        $maxTransNo = 1;
        $data = DealTransactionDetail::select('TranNo')->orderBy('id', 'DESC')->first();
        if($data != ''){
            $maxTransNo = $data->TranNo+1;
        }
        $vid = str_pad($maxTransNo, env('DEAL_VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
        $identifire = Config::get('app.identifire_type');
        
        $nationality = Nationality::orderBy('Code')->get();
        $settlementmode = SettlementMode::orderBy('SettlementCode')->get();

        return view('transaction.deal', compact('nationality','settlementmode','identifire','vid'));
    }
    
    public function voucherno(Request $request)
    {
        $maxTransNo = 1;
        $data = DealTransactionDetail::select('TranNo')->orderBy('id', 'DESC')->first();
        if($data != ''){
            $maxTransNo = $data->TranNo+1;
        }
        $vid = str_pad($maxTransNo, env('DEAL_VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
        return response()->json(['status' => '200', 'data' => $vid]);
    }

    public function getcustomerdetails(Request $request)
    {
        $loccode = Auth::user()->Location;

        $customerlocationdetails = LocationCustomer::where('Custcode',$request->cusid)->where('Location',$loccode)->pluck('DealBalance')->all();
        $cusbalance = !empty($customerlocationdetails[0]) ? $customerlocationdetails[0] : '0.00';

        $data = Customer::where('Custcode', $request->cusid)->first();
        $data['customerbalance'] = currency_format($cusbalance,2);
        if($data){
            return response()->json(['status' => '200', 'data' => $data]);
        } else {
            return response()->json(['status' => '201']);
        }
    }

    public function getDealNo()
    {
        $maxDealNo = 1;
        $data = DealTransactionHeader::select('DealNo')->orderBy('id', 'DESC')->first();
        if($data != ''){
            $data->DealNo = str_replace('DL', '', $data->DealNo);
            $maxDealNo = $data->DealNo+1;
        }
        $dealno = str_pad($maxDealNo, env('DEAL_NO_PREFIX'), '0', STR_PAD_LEFT);
        return $dealno;
    }
   
    
}
