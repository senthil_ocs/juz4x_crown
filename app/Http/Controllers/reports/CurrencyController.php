<?php

namespace App\Http\Controllers\reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use DataTables;
use App\Customer;
use App\CashCustomer;
use App\Location;
use App\Currency;
use App\BuySell;
use App\LocationCustomer;
use App\CustomerContact;
use PDF;
use Excel;
use Response;
use Carbon\Carbon;
use DB;
use Redirect;
use App\BuySellDetails;
use App\LocationCurrencyTransaction;
use App\LocationCurrency;
use Session;
use Auth;

class CurrencyController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function ledgercurrency(Request $request)
    {
        $fromDate = $request->fdate ? Carbon::parse($request->fdate)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->tdate ? Carbon::parse($request->tdate)->endOfDay() : Carbon::parse(now())->endOfDay();
        $alllocations = Location::get();


        if ($request->ajax() && $request->currency_code) {
            
            $cbal = LocationCurrencyTransaction::where('CurrencyCode',$request->currency_code)->where('LocationCode',$request->location)->whereBetween('Date', [Carbon::parse($request->fdate)->startOfDay(), Carbon::parse($request->fdate)->endOfDay()])->first();
            $currcode = $request->currency_code;
              
                $datas = DB::table('buysell');
                if(!empty( $request->code)){
                   $datas= $datas->where('CustCode', $request->code);
                }
                
                $datas =  $datas->leftJoin('buysell_details', function($join) use ($currcode)
                {
                    $join->on('buysell.TranNo', '=', 'buysell_details.TranNo');
                })->where('buysell_details.CurrencyCode', $currcode);

                if($request->fdate && $request->tdate){
                    $from = Carbon::parse($request->fdate)->startOfDay();
                    $to = Carbon::parse($request->tdate)->endOfDay();
                    $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
                }
                $datas = $datas->groupBy('buysell.TranNo')->get();
                //echo 'cnt-->'.count($datas);
                if($cbal){
                    $bal = $cbal->OpeningBalance;
                } else {
                    $bal = 0;
                }
                foreach ($datas as $key => $value) {
                    if($value->TranType == 'Buy'){
                        $bal +=$value->FAmount; 
                    } else if($value->TranType == 'Sell') {
                        $bal -=$value->FAmount;
                    }
                    $datas[$key]->bal =  currency_format($bal,2);
                    $datas[$key]->localbal =  currency_format($bal,2);
                }
                

                return Datatables::of($datas)
                    ->addIndexColumn()
                        ->editColumn('TranNo', function ($datas){
                            return str_pad($datas->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
                        })

                        ->editColumn('TranDate', function ($datas) {
                            $tradate = date('d/m/Y H:i:s',strtotime($datas->TranDate));
                            return $tradate;
                        })
                       
                        ->editColumn('Rate', function ($datas) {
                            return currency_format($datas->Rate);
                        })
                        ->editColumn('FAmount', function ($datas) {
                            return currency_format($datas->FAmount,2);
                        })
                        ->editColumn('LAmount', function ($datas) {
                            return currency_format($datas->LAmount,2);
                        })
                      ->addColumn('Tsell', function ($datas) {
                        if($datas->TranType == 'Sell'){
                          return '<span class="badge badge-info">'.currency_format($datas->FAmount,2).'</span>'; 
                        } else {
                          return currency_format(0,2);
                        }              
                      })
                      ->addColumn('Tbuy', function ($datas) {
                        if($datas->TranType == 'Buy'){
                        return '<span class="badge badge-success">'.currency_format($datas->FAmount,2).'</span>'; 
                        } else {
                         return currency_format(0,2);
                        }
                      })->escapeColumns([])
                    ->setRowId(function ($datas) {
                        return $datas->id;
                    })->setRowClass(function () {
                        return 'tableclicked';
                    })->make(true);
        }
        //printArray($alllocations); exit;
        $fromDate = date_format($fromDate,"d-m-Y");
        $toDate = date_format($toDate,"d-m-Y");
        return view('reports.ledgercurrency',compact('alllocations','fromDate','toDate'));
    }

    public function ledgercurrencyexport(Request $request)
    {
              if($request->currency_code){  

        $cbal = LocationCurrencyTransaction::where('CurrencyCode',$request->currency_code)->where('LocationCode',$request->location)->whereBetween('Date', [Carbon::parse($request->from_date)->startOfDay(), Carbon::parse($request->from_date)->endOfDay()])->first();    
        $currcode = $request->currency_code;
        $customercode = $request->cust_code;
            if($cbal){
                $bal = $cbal->OpeningStock;
                $openingbal = currency_format($cbal->OpeningStock,2);
            } else {
                $bal = 0;
                $openingbal = currency_format(0,2);
            }
            $datas = DB::table('buysell');

            if($request->cust_code){
                $datas = $datas->where('CustCode', $request->cust_code);
            }

            $datas = $datas->leftJoin('buysell_details', function($join) use ($currcode)
                {
                    $join->on('buysell.TranNo', '=', 'buysell_details.TranNo');
                })->where('buysell_details.CurrencyCode', $currcode);
                if($request->from_date && $request->to_date){
                    $from = Carbon::parse($request->from_date)->startOfDay();
                    $to = Carbon::parse($request->to_date)->endOfDay();
                    $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
                }
                $datas = $datas->groupBy('buysell.TranNo')->get();
                foreach ($datas as $key => $value) {
                    if($value->TranType == 'Buy'){
                        $bal -=$value->FAmount; 
                    } else if($value->TranType == 'Sell') {
                        $bal +=$value->FAmount;
                    }
                    $datas[$key]->bal =  currency_format($bal,2);
                    $datas[$key]->localbal =  currency_format($bal,2);
                }
            if($request->export == 'PDF' && count($datas) > 0 ){
                $data = [];
                $fcurr = 0; 
                $sell = 0; 
                foreach ($datas as $key => $value) {
                    $fcurr += $value->FAmount;
                    if($value->TranType == 'Sell'){
                        $sell += $value->LAmount;
                    }
                    $TranDate = \Carbon\Carbon::parse($value->TranDate);
                    $TranDate =  $TranDate->format('d/m/Y H:i:s');
                    $data[] =   array(
                        'CustName' => $value->CustName,
                        'TranNo' => str_pad($value->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT),
                        'TranDate' => $TranDate,
                        'CurrencyCode' => $value->CurrencyCode,
                        'CustCode' => $value->CustCode,
                        'TranType' => $value->TranType,
                        'Rate' => currency_format($value->Rate),
                        'FAmount' => currency_format($value->FAmount),
                        'Tsell' => ($value->TranType == 'Sell') ? currency_format($value->FAmount,2) : currency_format(0,2),
                        'Tbuy' => ($value->TranType == 'Buy') ? currency_format($value->FAmount,2) : currency_format(0,2),
                        'Bal' => $value->bal,
                        'LocalBal' => currency_format($value->LAmount,2),
                    );
                }
                $fcurr =  currency_format($fcurr,2);
                $sell = currency_format($sell,2);
                $fromdate = \Carbon\Carbon::parse($from);
                $fromdate =  $fromdate->format('d/m/Y');
                $todate = \Carbon\Carbon::parse($to);
                $todate =  $todate->format('d/m/Y');
                $strFile  = 'ledgercurrency.pdf';
                $compArray = DB::table('company')->select('Description')->get();
                $compName = $compArray[0]->Description;

                $pdf = PDF::loadView('pdf/ledger/currencyledger',compact('data','fcurr','sell','openingbal','fromdate','todate','customercode','compName'));
                return $pdf->download($strFile);
            } else if($request->export == 'CSV' && count($datas) > 0 ){

                $headers = array(
                    "Content-type" => "text/csv",
                    "Content-Disposition" => "attachment; filename=ledgercurrency.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
                if(!empty($request->cust_code)){
                   $columns = array('TranNo','TranDate','CurrencyCode','TranType','Rate','FAmount','Tsell','Tbuy');
                }else{
                    $columns = array('TranNo','TranDate','CurrencyCode','TranType','Rate','FAmount','Tsell','Tbuy','Bal','Local Bal');
                }

                $callback = function() use ($datas, $columns,$customercode)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $columns);
                    foreach($datas as $value) {
                         if(!empty($customercode)){
                            fputcsv($file, array(str_pad($value->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT),$value->TranDate,$value->CurrencyCode,$value->TranType,currency_format($value->Rate),currency_format($value->FAmount,2),($value->TranType == 'Sell') ? currency_format($value->FAmount,2) : currency_format(0,2),($value->TranType == 'Buy') ? currency_format($value->FAmount,2) : currency_format(0,2)));
                         }else{
                            fputcsv($file, array(str_pad($value->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT),$value->TranDate,$value->CurrencyCode,$value->TranType,currency_format($value->Rate),currency_format($value->FAmount,2),($value->TranType == 'Sell') ? currency_format($value->FAmount,2) : currency_format(0,2),($value->TranType == 'Buy') ? currency_format($value->FAmount,2) : currency_format(0,2),$value->bal,currency_format($value->LAmount,2)));
                         }
                        
                    } 
                    fclose($file);
                };
                return Response::stream($callback, 200, $headers);
            }   else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'No data available');
                return Redirect::back();
            }
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Please selcte customer details');
            return Redirect::back();
        }
    }
    public function locationStockReport(Request $request){
         
         $alllocations = Location::select('LocationCode')->get();

         if($request->ajax()){
            $data = DB::table('location_currency');
                  
                   
            if($request->businessnature=='deal stock'){
              $data =  $data->where('location_currency.DealStock','!=',0);
            }else{
              $data =  $data->where('location_currency.Stock','!=',0);
            }

            $data = $data->leftJoin('currency_master', 'currency_master.CurrencyCode', '=', 'location_currency.CurrencyCode')
                    ->select('location_currency.DealStock','location_currency.Stock','location_currency.AvgCost','location_currency.ModifiedDate','location_currency.id','location_currency.LocationCode','currency_master.CurrencyName')
                    ->where('LocationCode',$request->location)->get();

            
            return Datatables::of($data)
                    ->editColumn('Stock', function ($data)use($request) {
                        if($request->businessnature == 'current stock'){
                           $stock =  $data->Stock;
                        }else{
                            $stock =  $data->DealStock;
                        }
                        return currency_format($stock,2);
                    })
                    ->editColumn('CurrencyCode', function ($data){
                        return $data->CurrencyName ? $data->CurrencyName : '';
                    })
                    ->editColumn('AvgCost', function ($data) {
                        return $data->AvgCost ? currency_format($data->AvgCost,8) : '';
                    })
                    ->addColumn('Date', function ($data) {
                        $date = \Carbon\Carbon::parse($data->ModifiedDate);
                        return $data->ModifiedDate ? $date->format('d/m/Y') : '';
                    })
                    ->addColumn('LValue', function ($data)use($request){
                         $sstock = $data->Stock;
                         if($request->businessnature == 'deal stock'){
                            $sstock = $data->DealStock;
                          }
                        return currency_format(($data->AvgCost*$sstock),2);
                    })
                    ->setRowId(function ($data) {
                        return $data->id;
                    })->setRowClass(function () {
                        return 'tableclicked';
                    })->make(true);
         }

         return view('reports.locationstockreport',compact('alllocations'));
    }
    public function locationStockReportExport(Request $request)
    {
   
          $alllocations = Location::select('LocationCode')->get();
                      
           $data = DB::table('location_currency');

        
         if($request->businessnature=='deal stock'){
              $data =  $data->where('location_currency.DealStock','!=',0);
            }else{
              $data =  $data->where('location_currency.Stock','!=',0);
            }

            $data = $data->leftJoin('currency_master', 'currency_master.CurrencyCode', '=', 'location_currency.CurrencyCode')
                    ->select('location_currency.DealStock','location_currency.Stock','location_currency.AvgCost','location_currency.ModifiedDate','location_currency.id','location_currency.LocationCode','currency_master.CurrencyName','currency_master.CurrencyCode')
                    ->where('LocationCode',$request->location)->get();


            $locationCode = $request->location;

            if($request->export == 'PDF' && count($data) > 0 ){
                $totallocalvalue =0;
                 foreach ($data as $key => $value) {
                    if($request->businessnature == 'deal stock'){
                       $stock = $value->DealStock; 
                    }else{
                         $stock = $value->Stock;
                    }
                    $totallocalvalue +=0; 
               
                  $LocalValue = currency_format(($value->AvgCost*$stock),2);
                    $Currency = Currency::select('CurrencyName')->where('CurrencyCode',$value->CurrencyCode)->first(); 
                    $date = \Carbon\Carbon::parse($value->ModifiedDate);
                  

                    $pdfdata[] =   array(
                        'Currency' => $Currency->CurrencyName,
                        'CurrencyCode' => $value->CurrencyCode,
                        'Stock' => !empty($stock)?currency_format($stock,2):currency_format(0,2),
                        'AverageCost' => !empty($value->AvgCost)?currency_format($value->AvgCost,2):currency_format(0,2),
                        'LocalValue' => $LocalValue,
                        'Date' => $date->format('d/m/Y'),
                    );
                }
                $data = $pdfdata;
                $strFile  = 'locationwisestockReport.pdf';
                $compArray = DB::table('company')->select('Description')->get();
                $compName = $compArray[0]->Description;
                 
                $pdf = PDF::loadView('pdf/ledger/locationwiseStockReport',compact('data','locationCode','totallocalvalue','compName'));
                return $pdf->download($strFile);
            } else if($request->export == 'CSV' && count($data) > 0 ){

                $headers = array(
                    "Content-type" => "text/csv",
                    "Content-Disposition" => "attachment; filename=locationwisestockReport.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
               
                $columns = array('Currency','Stock','Average Cost','Local Value','Last P.Date');

                $callback = function() use ($data, $columns,$request)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $columns);
                    foreach($data as $value) {
                        $Currency = Currency::select('CurrencyName')->where('CurrencyCode',$value->CurrencyCode)->first(); 
                        if($request->businessnature == 'deal stock'){
                       $stock = $value->DealStock; 
                    }else{
                         $stock = $value->Stock;
                    }
                        $date = \Carbon\Carbon::parse($value->ModifiedDate);
                        fputcsv($file, array($Currency->CurrencyName,!empty($stock) ? currency_format($stock,2) : currency_format(0,2),!empty($value->AvgCost) ? currency_format($value->AvgCost,2) : currency_format(0,2),currency_format(($value->AvgCost*$stock),2),$date->format('d/m/Y')));
                        
                    } 
                    fclose($file);
                };
                return Response::stream($callback, 200, $headers);
            }   else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'No data available');
                return Redirect::back();
            }
        
    }

    public function currencyWiseTransactionReport(Request $request)
    {
         $fromDate = $request->fdate ? Carbon::parse($request->fdate)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->tdate ? Carbon::parse($request->tdate)->endOfDay() : Carbon::parse(now())->endOfDay();
        if ($request->ajax()) {
           $data = DB::table('buysell')  
            ->select(DB::raw("tbl_buysell.id,tbl_buysell.CustCode,tbl_buysell.CustName,tbl_buysell_details.TranType,tbl_buysell_details.TranNo,tbl_buysell_details.CurrencyCode,sum(tbl_buysell_details.LAmount),tbl_buysell_details.FAmount,tbl_buysell_details.Rate,tbl_buysell_details.LAmount"))
            ->join('buysell_details', 'buysell_details.TranNo', '=', 'buysell.TranNo');
            $cust_code = $request->cust_code;
            if(!empty( $request->cust_code)){
               $data= $data->where('buysell.CustCode', $request->cust_code);
            }
            if($request->fdate && $request->tdate){
                $from = Carbon::parse($request->fdate)->startOfDay();
                $to = Carbon::parse($request->tdate)->endOfDay();
                $data = $data->whereBetween('buysell.TranDate', [$from, $to]);
            }
            $data =$data->groupBy('buysell_details.CurrencyCode','buysell_details.TranType')
            ->get();
           return Datatables::of($data)
                ->addIndexColumn()
                    ->editColumn('TranNo', function ($data){
                        return str_pad($data->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
                    })
                   
                    ->editColumn('Rate', function ($data) {
                        return currency_format($data->Rate);
                    })
                    ->editColumn('FAmount', function ($data) {
                        return currency_format($data->FAmount,2);
                    })
                  ->addColumn('CurrName', function ($data) {
                    if(!empty($data->CustCode)){
                        $Currency = Currency::select('CurrencyName')->where('CurrencyCode',$data->CurrencyCode)->first(); 
                        return $Currency->CurrencyName; 
                    }             
                  })
                  ->addColumn('Tsell', function ($data) {
                    if($data->TranType == 'Sell'){
                      return '<span class="badge badge-info">'.currency_format($data->LAmount,2).'</span>'; 
                    } else {
                      return currency_format(0,2);
                    }              
                  })
                  ->addColumn('Tbuy', function ($data) {
                    if($data->TranType == 'Buy'){
                      return '<span class="badge badge-success">'.currency_format($data->LAmount,2).'</span>'; 
                    } else {
                      return currency_format(0,2);
                    }
                  })->escapeColumns([])
                ->setRowId(function ($data) {
                    return $data->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                    })->make(true);
        }

        $fromDate = date_format($fromDate,"d-m-Y");
        $toDate = date_format($toDate,"d-m-Y");
       return view('reports.currencyWisetransactionreport');
    }

    public function currencyWiseTransactionexport(Request $request)
    {
        /*printArray($request->all());die;*/
        if(!empty($request->from_date) || !empty($request->to_date)){  

            
             $data = DB::table('buysell')  
            ->select(DB::raw("tbl_buysell.id,tbl_buysell.CustCode,tbl_buysell.CustName,tbl_buysell_details.TranType,tbl_buysell.TranDate,tbl_buysell_details.TranNo,tbl_buysell_details.CurrencyCode,sum(tbl_buysell_details.LAmount),tbl_buysell_details.FAmount,tbl_buysell_details.Rate,tbl_buysell_details.LAmount"))
            ->join('buysell_details', 'buysell_details.TranNo', '=', 'buysell.TranNo');
            $cust_code = $request->cust_code;
            if(!empty( $request->cust_code)){
               $data= $data->where('buysell.CustCode', $request->cust_code);
            }
            if($request->from_date && $request->to_date){
                $from = Carbon::parse($request->from_date)->startOfDay();
                $to = Carbon::parse($request->to_date)->endOfDay();
                $data = $data->whereBetween('buysell.TranDate', [$from, $to]);
            }
            $data =$data->groupBy('buysell_details.CurrencyCode','buysell_details.TranType');
            $data = $data->orderBy('CurrencyCode')->get();
            if($request->export == 'PDF' && count($data) > 0 ){
                $buy = 0; 
                $sell = 0; 
                foreach ($data->toArray() as $key => $value) {
                    if($value->TranType == 'Sell'){
                        $sell += $value->LAmount;
                    }else{
                         $buy += $value->LAmount;
                    }

                    $pdfdata[$value->CurrencyCode][] =   array(
                        'CustName' => $value->CustName,
                        'TranNo' => str_pad($value->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT),
                        'TranDate' => $value->TranDate,
                        'CurrencyCode' => $value->CurrencyCode,
                        'CustCode' => $value->CustCode,
                        'TranType' => $value->TranType,
                        'Rate' => currency_format($value->Rate),
                        'FAmount' => currency_format($value->FAmount),
                        'Tsell' => ($value->TranType == 'Sell') ? currency_format($value->LAmount,2) : currency_format(0,2),
                        'Tbuy' => ($value->TranType == 'Buy') ? currency_format($value->LAmount,2) : currency_format(0,2),
                       
                    );

                }
                $buy =  currency_format($buy,2);
                $sell = currency_format($sell,2);
                $fromdate = displayDateformat($from);
                $todate = displayDateformat($to);
                $data = $pdfdata;
                $strFile  = 'currencywiseTransactionReport.pdf';
                $compArray = DB::table('company')->select('Description')->get();
                $compName = $compArray[0]->Description; 
                $fromDate = \Carbon\Carbon::parse($fromdate);
                $fromdate =  $fromDate->format('d-m-Y');
                $toDate = \Carbon\Carbon::parse($todate);
                $todate =  $toDate->format('d-m-Y');
                $pdf = PDF::loadView('pdf/ledger/currencyWisetransactionreport',compact('data','buy','sell','fromdate','todate','compName'));
                return $pdf->download($strFile);
            } else if($request->export == 'CSV' && count($data) > 0 ){

                $headers = array(
                    "Content-type" => "text/csv",
                    "Content-Disposition" => "attachment; filename=currencyWisetransactionreport.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
               
                $columns = array('CurrencyCode','Currency','Cust Code','Cust Name','Sell','Buy');

                $callback = function() use ($data, $columns)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $columns);
                    foreach($data as $value) {
                        $Currency = Currency::select('CurrencyName')->where('CurrencyCode',$value->CurrencyCode)->first(); 
                        fputcsv($file, array($value->CurrencyCode, $Currency->CurrencyName,$value->CustCode,$value->CustName,($value->TranType == 'Sell') ? currency_format($value->LAmount,2) : currency_format(0,2),($value->TranType == 'Buy') ? currency_format($value->LAmount,2) : currency_format(0,2)));
                        
                    } 
                    fclose($file);
                };
                return Response::stream($callback, 200, $headers);
            }   else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'No data available');
                return Redirect::back();
            }
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Please selcte customer details');
            return Redirect::back();
        }
    }
}
