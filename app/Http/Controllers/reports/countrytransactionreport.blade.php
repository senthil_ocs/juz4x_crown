@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" />
@endsection

@section('page-css')
<style type="text/css">
table#table_nationality > tbody >tr {
    cursor: pointer;
}
.form-group.row.Modify_hidden {
    display: none;
}    
.picker__holder{
    max-width: none;
    width: 335px;
}
#to_date_root .picker__holder {
    right: 30px;
}
.badge{
	font-size: 100%;
}
</style>
@endsection
	@section('main-content')
		<div class="breadcrumb">
			<h1>Locationwise Stock Report</h1>
			<ul>
			    <li><a href="#">Listing</a></li>
			</ul>
		</div>
		<div class="separator-breadcrumb border-top"></div>

		<div class="row mb-12 master-main">
			<div class="col-lg-12 col-sm-12 master-main-sec">
				<div class="card mb-6">
					<div class="card-body">

					    <div class="col-lg-12 mb-12">
					        <div class="card">
					            <div class="card-body">
					            	<form action="{{ route('reports.locationStockReport')}}" method="POST" id="ledgercurrencyfilterform">
					            		@csrf
										<div class="row">
											<div class="col-lg-4 col-sm-4 mb-1">
												<div class="row">
													<label for="location" class="col-sm-5  col-form-label">Location Name</label>
													<div class="col-sm-7 text-left">
														<input type="text" name="location" class="form-control" >
													</div>
												</div>
											</div>	

											<div class="col-lg-3 col-sm-4 mb-1">
												<label class="radio radio-primary">
														<input class="form-control" type="radio" name="businessnature" value="Trading Company" formcontrolname="radio" checked="checked">
													<span>Current Stock</span>
													<span class="checkmark"></span>
												</label>
											</div>

											<div class="col-lg-3 col-sm-4 mb-1">
												<label class="radio radio-primary">
														<input class="form-control" type="radio" name="businessnature" value="Trading Company" formcontrolname="radio">
													<span>Deal Stock</span>
													<span class="checkmark"></span>
												</label>
											</div>
										</div>

										<div class="row mt-3">
											<div class="col-lg-7 col-sm-12">
											</div>
											<div class="col-lg-5 col-sm-12 text-right">
	 											<button class="btn btn-primary m-1" type="button" id="check_details" onclick="submitForm('submit');">Submit</button>
												<input  class="btn btn-primary m-1" type="submit" name="export"  value="PDF">

												<input  class="btn btn-primary m-1" type="submit" name="export" value="CSV">
											</div>
										</div>
					            	</form>
					            </div>
					        </div>
					    </div>
					    <div class="table-responsive">
					       <table id="table_locationstockreport" class="display nowrap  table-striped table-bordered table" style="width:100%">
                           
                           </table>	
					    </div>
					</div>	
				</div>
			</div>
		</div>	
	@include('modal.customer')	
	@include('modal.currency')
@endsection
@section('page-js')
	<script src="{{asset('assets/js/form.validation.script.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
	<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
	<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
	<script src="{{asset('assets/js/modal.details.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@endsection

@section('bottom-js')

@if(session()->has('message.level') && session('message.level')=="success")                           
<script type="text/javascript">  
        toastr.success('<?php echo session('message.content'); ?>',{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
</script> 
@endif 
@if(session()->has('message.level') && session('message.level')=="danger") 
<script type="text/javascript">  
        toastr.error("<?php echo session('message.content'); ?>",{
            "positionClass": "toast-top-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        })
    </script> 
@endif
<script type="text/javascript">
	$(document).ready(function(){
		$('#bbuy').html(0);
		$('#bsell').html(0);
		var date = new Date();

	    $('#table_locationstockreport').DataTable({
            paging:   false,
            destroy: true,
            searching : false,
            autoWidth : false,
            info:     false,
            ordering: false,
            columns: [
                    {data: 'Currency', name: 'Currency',title: "Currency"},
                    {data: 'Stock', name: 'Stock',title: "Stock"},
                    {data: 'Average Cost', name: 'Average Cost',title: "Average Cost"},
                    {data: 'Value', name: 'Value',title: "Local Value"},
                    {data: 'Date', name: 'Date',title: "Last P.Date"},
                ],
            
	    });
	});

	function submitForm(type) {
		var location = 	$('#location').val();
		var code = 	$('input[name="cust_code"]').val();
		var currency_code = 	$('input[name="currency_code"]').val();
		var fdate = $('input[name="from_date"]').val();
		var tdate = $('input[name="to_date"]').val();
		if(currency_code=='') {
			$('input[name="currency_code"]').css('border','1px solid #ff7d7d');
			return false;
		} else {
			$('input[name="cust_code"]').css('border','1px solid #ced4db');
            var oTable =$('#table_locationstockreport').DataTable({
                paging:   false,
                destroy: true,
                searching : false,
                autoWidth : false,
                info:     false,
                serverSide: true,
                ordering: false,
                ajax: {
                    url:"{{ route('reports.ledgercurrency') }}",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function (d) {
                    	console.log(d);
						d.location=location;
						d.code=code;
						d.currency_code=currency_code;
						d.fdate=fdate;
						d.tdate=tdate;
                    },
                },
                columns: [
                    {data: 'Currency', name: 'Currency',title: "Currency"},
                    {data: 'Stock', name: 'Stock',title: "Stock"},
                    {data: 'Average Cost', name: 'Average Cost',title: "Average Cost"},
                    {data: 'Value', name: 'Value',title: "Local Value"},
                    {data: 'Date', name: 'Date',title: "Last P.Date"},
                ],               
            });

            if(code == ''){
            	oTable.column(6).visible(true);
                oTable.column(7).visible(true);
            }else{
            	oTable.column(6).visible(false);
             	oTable.column(7).visible(false);
            }
            

			$.ajax({
	    		url: "./customerbalance",
	    		type: "POST",
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: {
					location : location,
					code : code,
					fdate : fdate,
					tdate : tdate,
				},
				beforeSend: function() {},
				success: function(response){
					return false;
					if(response.status == 200){
						var finder = response.bal.OpeningBalance;
						if(finder >= 0){
							$('#bbuy').html(response.bal);
						} else {
							$('#bsell').html(response.bal);
						}
					} else {
						$('#bbuy').html(0);
						$('#bsell').html(0);
					}	
				},
				error: function(){}
	    	});
		}
	}	
	
	$( "#ledgercurrencyfilterform" ).submit(function( event ) {
		var currency_code = 	$('input[name="currency_code"]').val();
	 	if(currency_code=='') {
			$('input[name="currency_code"]').css('border','1px solid #ff7d7d');
			return false;
		}else{
			return true;
		}
	  event.preventDefault();
	});

</script>
@endsection