<?php

namespace App\Http\Controllers\reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use DataTables;
use PDF;
use Excel;
use Response;
use Carbon\Carbon;
use DB;
use Redirect;

class TTReportController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
}
