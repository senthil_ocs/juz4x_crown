<?php

namespace App\Http\Controllers\reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use DataTables;
use App\Customer;
use App\CashCustomer;
use App\Location;
use App\BuySell;
use App\LocationCustomer;
use App\DDtransaction;
use App\CustomerContact;
use PDF;
use Excel;
use Response;
use Carbon\Carbon;
use DB;
use Redirect;
use App\BuySellDetails;
use App\LocationCustomerTransaction;
use App\Currency;
use App\GeneralLedger;
use Auth;   

class CustomerController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    /* START CUSTOMER*/
    public function index(Request $request)
    {
    	$alllocations = Location::select('LocationCode')->get();

        if($request->ajax() && ($request->location || $request->active)){

            if($request->active == 'active'){
                $active = '1';
            } else if($request->active == 'inactive'){
                $active = '0';
            } else {
                $active = '1';
            }
            $datas = LocationCustomer::select('Custcode','CustName')->where('Location',$request->location)->where('Active',$active)->with('customer')->get();
            return Datatables::of($datas)
                ->addIndexColumn()
                ->editColumn('customer.Phone', function ($datas) {
                    if(isset($datas->customer['Phone'])){
                        return $datas->customer['Phone'];
                    } else {
                        return '';
                    }
                })
                ->editColumn('customer.Email', function ($datas) {
                    if(isset($datas->customer['Email'])){
                        return $datas->customer['Email'];
                    } else {
                        return '';
                    }
                })
                ->editColumn('customer.DOB', function ($datas) {
                    if(isset($datas->customer['DOB'])){
                        return $datas->customer['DOB'];
                    } else {
                        return '';
                    }
                })
                ->editColumn('customer.CompanyRegNo', function ($datas) {
                    if(isset($datas->customer['CompanyRegNo'])){
                        return $datas->customer['CompanyRegNo'];
                    } else {
                        return '';
                    }
                })
                ->editColumn('customer.MoneyChangerLicense', function ($datas) {
                    if(isset($datas->customer['MoneyChangerLicense'])){
                        return $datas->customer['MoneyChangerLicense'];
                    } else {
                        return '';
                    }
                })
                ->editColumn('customer.NRICNo', function ($datas) {
                    if(isset($datas->customer['NRICNo'])){
                        return $datas->customer['NRICNo'];
                    } else {
                        return '';
                    }
                })
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
    	return view('reports.customer',compact('alllocations'));
    }

    public function customerexport(Request $request)
    {
        // printArray($request->all());die;

        if($request->location){
            if($request->active == 'active'){
                $active = '1';
            } else if($request->active == 'inactive'){
                $active = '0';
            } else {
                $active = '1';
            }
            $datas = LocationCustomer::where('Location',$request->location)->where('Active',$active)->with('customer')->get();
            
            $locationc = $request->location;
            if($request->export == 'PDF' && count($datas) > 0 ){
                $compArray = DB::table('company')->select('Description')->get();
                $compName = $compArray[0]->Description;

                $strFile  = 'customers.pdf';
                $pdf = PDF::loadView('pdf/customer',compact('datas','locationc','compName'));
                return $pdf->download($strFile);

            } else if($request->export == 'CSV' && count($datas) > 0 ) {

                $headers = array(
                    "Content-type" => "text/csv",
                    "Content-Disposition" => "attachment; filename=customer.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
                $columns = array('Customer', 'Name', 'Phone', 'Email', 'DOB', 'CompanyRegNo', 'MoneyChangerLicense', 'NRICNo');

                $callback = function() use ($datas, $columns)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $columns);

                    foreach($datas as $value) {
                        if(isset($value->customer)){
                            fputcsv($file, array($value->customer->Custcode, $value->customer->CustName,$value->customer->Phone, $value->customer->Email,$value->customer->DOB, $value->customer->CompanyRegNo, $value->customer->MoneyChangerLicense, $value->customer->NRICNo));
                        }
                    }
                    fclose($file);
                };
                return Response::stream($callback, 200, $headers);

            }else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'No data available');
                return Redirect::back();
            }
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Please select location details');
            return Redirect::back();
        }
    }
    /* END CUSTOMER*/


    /* START CASH CUSTOMER*/
    public function cashcustomer(Request $request)
    {

        $alllocations = Location::select('LocationCode')->get();
        $datas = CashCustomer::select('CustCode','Name','PhoneNo','DOB','PPNo');
        if(isset($request->cust_name))
        {
            $datas = $datas->where('Name', 'like', '%' . $request->cust_name . '%');
        }
        if(isset($request->cust_phone))
        {
            $datas = $datas->where('PhoneNo', 'like', '%' . $request->cust_phone . '%');
        }
        $datas = $datas->get();
        if ($request->ajax()) {
            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        return view('reports.cashcustomer',compact('alllocations'));
    }

    public function cashcustomerexport(Request $request)
    {
        // printArray($request->all());die;

        if($request->location){
            $datas = CashCustomer::get(); 
            if($request->export == 'PDF' && count($datas) > 0 ){
                $compArray = DB::table('company')->select('Description')->get();
                $compName = $compArray[0]->Description;
                $locationc = $request->location;
                $strFile  = 'cashcustomers.pdf';
                $pdf = PDF::loadView('pdf/cashcustomer',compact('datas','locationc','compName'));
                return $pdf->download($strFile);

            } else if($request->export == 'CSV' && count($datas) > 0 ){

                $headers = array(
                    "Content-type" => "text/csv",
                    "Content-Disposition" => "attachment; filename=cashcustomer.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
                $columns = array('Customer', 'Name', 'Phone', 'DOB', 'NRICNo');
                $callback = function() use ($datas, $columns)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $columns);

                    foreach($datas as $value) {
                        fputcsv($file, array($value->CustCode, $value->Name,$value->PhoneNo,$value->DOB,$value->PPNo));
                    }
                    fclose($file);
                };
                return Response::stream($callback, 200, $headers);

            } else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'No data available');
                return Redirect::back();
            }
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Please select location details');
            return Redirect::back();
        }
    }
    /* END CASH CUSTOMER*/




    /* START LEDGER CUSTOMER*/
    public function ledgercustomer(Request $request)
    {
        $fromDate = $request->fdate ? Carbon::parse($request->fdate)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->tdate ? Carbon::parse($request->tdate)->endOfDay() : Carbon::parse(now())->endOfDay();

        $alllocations = Location::select('LocationCode')->get();

        if ($request->ajax() && $request->code) {
            
            $cbal = LocationCustomerTransaction::where('CustCode',$request->code)->where('LocationCode',$request->location)->whereBetween('Date', [Carbon::parse($request->fdate)->startOfDay(), Carbon::parse($request->fdate)->endOfDay()])->first();

            $datas = DB::table('buysell')->where('CustCode', $request->code)
                ->leftJoin('buysell_details', 'buysell.TranNo', '=', 'buysell_details.TranNo')
                ->select('buysell.id','buysell.TranNo','buysell.TranDate','buysell_details.TranType','buysell_details.LAmount','buysell_details.CurrencyCode','buysell_details.Rate','buysell_details.FAmount');
                if($request->fdate && $request->tdate){
                    $from = Carbon::parse($request->fdate)->startOfDay();
                    $to = Carbon::parse($request->tdate)->endOfDay();
                    $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
                }
                $datas = $datas->get();
                if($cbal){
                    $bal = $cbal->OpeningBalance;
                } else {
                    $bal = 0;
                }
                foreach ($datas as $key => $value) {
                    if($value->TranType == 'Buy'){
                        $bal -=$value->LAmount; 
                    } else if($value->TranType == 'Sell') {
                        $bal +=$value->LAmount;
                    }
                    $datas[$key]->bal =  currency_format($bal,2);
                }
            return Datatables::of($datas)
                ->addIndexColumn()
                    ->editColumn('TranNo', function ($datas){
                        return str_pad($datas->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
                    })
                    ->editColumn('Rate', function ($datas) {
                        return currency_format($datas->Rate);
                    })
                    ->editColumn('FAmount', function ($datas) {
                        return currency_format($datas->FAmount,2);
                    })
                  ->addColumn('TranDate', function ($datas) {
                        if(!empty($datas->TranDate)){
                            $date = \Carbon\Carbon::parse($datas->TranDate);
                            return $date->format('d/m/Y');
                        }else{
                            return '';
                        }
                    })
                  ->addColumn('Tsell', function ($datas) {
                    if($datas->TranType == 'Sell'){
                      return '<span class="badge badge-info">'.currency_format($datas->LAmount,2).'</span>'; 
                    } else {
                      return currency_format(0,2);
                    }              
                  })
                  ->addColumn('Tbuy', function ($datas) {
                    if($datas->TranType == 'Buy'){
                      return '<span class="badge badge-success">'.currency_format($datas->LAmount,2).'</span>'; 
                    } else {
                      return currency_format(0,2);
                    }
                  })->escapeColumns([])
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        $fromDate = date_format($fromDate,"d-m-Y");
        $toDate = date_format($toDate,"d-m-Y");
        return view('reports.ledgercustomer',compact('alllocations','fromDate','toDate'));
    }

    public function customerbalance(Request $request)
    {
        $cbal = LocationCustomerTransaction::select('OpeningBalance')->where('CustCode',$request->code)->where('LocationCode',$request->location)->whereBetween('Date', [Carbon::parse($request->fdate)->startOfDay(), Carbon::parse($request->fdate)->endOfDay()])->first();
        if($cbal){
            return response()->json(['status' => '200', 'bal' => currency_format($cbal->OpeningBalance,2)]);
        } else {
            return response()->json(['status' => '201']);
        }
    }
    public function ledgercustomerdetails(Request $request)
    {
        $locationCode = Auth::user()->Location;
        if($request->all()){
            $fbal = str_replace(',', '', $request->fromBal);
            $tbal = str_replace(',', '', $request->toBal);
            $amt = str_replace(',', '', $request->amount);
            $fromtot = (float)$fbal-(float)$amt;
            $totot = (float)$tbal+(float)$amt;
            $ledger                      = new GeneralLedger;
            $ledger->date                = now();
            $ledger->from_customer_code  = $request->from_customer_code;
            $ledger->to_customer_code    = $request->to_customer_code;
            $ledger->currency            = $request->currency;
            $ledger->amount              = $request->amount;
            $ledger->remark_from         = $request->remark_from;
            $ledger->remark_to           = $request->remark_to;
            $ledger->CreateDate          = now();
            $ledger->CreateUser          = Auth::user()->username;
            $ledger->save();
           
            if($request->from_customer_code!=''){
               $data = BuySell::select('TranNo')->orderBy('id', 'DESC')->first();
                if($data != ''){
                    $maxTransNo = $data->TranNo+1;
                }
                $data = ["TranNo"           => $maxTransNo,
                         "TranDate"         => now(),
                         "CustCode"         => $request->from_customer_code,
                         "LocationCode"     => $locationCode,
                         "Deleted"          => "",
                         "TerminalName"     => "",
                         "CreatedBy"        => Auth::user()->username,
                         "CreatedDate"      => now(),
                         "ModifiedBy"       => Auth::user()->username,
                         "ModifiedDate"     => now(),
                         "SettlementMode"   => "",
                         "SuspiciousTran"   => 0                        
                         ];
                        BuySell::create($data);
                        $Customerdetailsdata = ["TranNo"            => $maxTransNo,
                                        "Line"              => '1',
                                        "TranType"          => 'Buy',
                                        "CurrencyCode"      => $request->currency,
                                        "Rate"              => $request->exchangerate,
                                        "FAmount"           => $request->amount,
                                        "LAmount"           => $request->amount,
                                        "TTRefNo"           => 0,
                                        "LastAverageCost"   => '',
                                        "CreatedBy"         => Auth::user()->username,
                                        "CreatedDate"       => now(),
                                        "ModifiedBy"        => Auth::user()->username,
                                        "ModifiedDate"      => now()
                                       ]; 
                        BuySellDetails::create($Customerdetailsdata);                       
            }
            if($request->to_customer_code!=''){
               $data = BuySell::select('TranNo')->orderBy('id', 'DESC')->first();
                if($data != ''){
                    $maxTransNo = $data->TranNo+1;
                }
                $data = ["TranNo"           => $maxTransNo,
                         "TranDate"         => now(),
                         "CustCode"         => $request->to_customer_code,
                         "LocationCode"     => $locationCode,
                         "Deleted"          => "",
                         "TerminalName"     => "",
                         "CreatedBy"        => Auth::user()->username,
                         "CreatedDate"      => now(),
                         "ModifiedBy"       => Auth::user()->username,
                         "ModifiedDate"     => now(),
                         "SettlementMode"   => "",
                         "SuspiciousTran"   => 0                        
                         ];
                        BuySell::create($data);
                        $Customerdetailsdata = ["TranNo"            => $maxTransNo,
                                        "Line"              => '1',
                                        "TranType"          => 'Sell',
                                        "CurrencyCode"      => $request->currency,
                                        "Rate"              => $request->exchangerate,
                                        "FAmount"           => $request->amount,
                                        "LAmount"           => $request->amount,
                                        "TTRefNo"           => 0,
                                        "LastAverageCost"   => '',
                                        "CreatedBy"         => Auth::user()->username,
                                        "CreatedDate"       => now(),
                                        "ModifiedBy"        => Auth::user()->username,
                                        "ModifiedDate"      => now()
                                       ]; 
                        BuySellDetails::create($Customerdetailsdata);                       
       
            }

            $fdata = LocationCustomer::where('Location',$locationCode)->where('Custcode',$request->from_customer_code)->update(['Balance' => $fromtot]);
            $tdata = LocationCustomer::where('Location',$locationCode)->where('Custcode',$request->to_customer_code)->update(['Balance' => $totot]);


            return response()->json(['status' => '200', 'content'=>'General Ledger Details inserted successfully!']);
        } else {
            return response()->json(['status' => '201', 'content'=>'Wrong Detail!']);
        }
    }

    public function ledgercustomerexport(Request $request)
    {
       if($request->cust_code){  

        $cbal = LocationCustomerTransaction::where('CustCode',$request->cust_code)->where('LocationCode',$request->location)->whereBetween('Date', [Carbon::parse($request->from_date)->startOfDay(), Carbon::parse($request->from_date)->endOfDay()])->first();    
            if($cbal){
                $bal = $cbal->OpeningBalance;
                $openingbal = currency_format($cbal->OpeningBalance,2);
            } else {
                $bal = 0;
                $openingbal = currency_format(0,2);
            }
            $datas = DB::table('buysell')->where('CustCode', $request->cust_code)
                ->leftJoin('buysell_details', 'buysell.TranNo', '=', 'buysell_details.TranNo');
                if($request->from_date && $request->to_date){
                    $from = Carbon::parse($request->from_date)->startOfDay();
                    $to = Carbon::parse($request->to_date)->endOfDay();
                    $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
                }
                $datas = $datas->get();
                foreach ($datas as $key => $value) {
                    if($value->TranType == 'Buy'){
                        $bal -=$value->LAmount; 
                    } else if($value->TranType == 'Sell') {
                        $bal +=$value->LAmount;
                    }
                    $datas[$key]->bal =  currency_format($bal,2);
                }
            if($request->export == 'PDF' && count($datas) > 0 ){
                $data = [];
                $fcurr = 0; 
                $sell = 0; 
                foreach ($datas as $key => $value) {
                    $fcurr += $value->FAmount;
                    if($value->TranType == 'Sell'){
                        $sell += $value->LAmount;
                    }
                    if(!empty($value->TranDate)){
                        $date = \Carbon\Carbon::parse($value->TranDate);
                        $TranDate = $date->format('d/m/Y');
                    }else{
                        $TranDate =  '';
                    }
                    $data[] =   array(
                        'CustName' => $value->CustName,
                        'TranNo' => str_pad($value->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT),
                        'TranDate' => $TranDate,
                        'CurrencyCode' => $value->CurrencyCode,
                        'TranType' => $value->TranType,
                        'Rate' => currency_format($value->Rate),
                        'FAmount' => currency_format($value->FAmount,2),
                        'Tsell' => ($value->TranType == 'Sell') ? currency_format($value->LAmount,2) : currency_format(0,2),
                        'Tbuy' => ($value->TranType == 'Buy') ? currency_format($value->LAmount,2) : currency_format(0,2),
                        'Bal' => $value->bal,
                    );
                }
                $compArray = DB::table('company')->select('Description')->get();
                $compName = $compArray[0]->Description;
                $fcurr =  currency_format($fcurr,2);
                $sell = currency_format($sell,2);
                $strFile  = 'ledgercustomer.pdf';
                $fromdate = Carbon::parse($request->from_date)->format('d/m/Y');
                $todate = Carbon::parse($request->to_date)->format('d/m/Y');
                $pdf = PDF::loadView('pdf/ledger/ledger',compact('data','fcurr','sell','openingbal','compName', 'fromdate', 'todate'));
                return $pdf->download($strFile);
            } else if($request->export == 'CSV' && count($datas) > 0 ){
                $headers = array(
                    "Content-type" => "text/csv",
                    "Content-Disposition" => "attachment; filename=ledgercustomer.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
                $columns = array('TranNo','TranDate','CurrencyCode','TranType','Rate','FAmount','Tsell','Tbuy','Bal');

                $callback = function() use ($datas, $columns)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $columns);

                    foreach($datas as $value) {
                        if(!empty($value->TranDate)){
                            $date = \Carbon\Carbon::parse($value->TranDate);
                            $TranDate = $date->format('d/m/Y');
                        }else{
                            $TranDate =  '';
                        }
                        fputcsv($file, array(str_pad($value->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT),$TranDate,$value->CurrencyCode,$value->TranType,currency_format($value->Rate),currency_format($value->FAmount,2),($value->TranType == 'Sell') ? currency_format($value->LAmount,2) : currency_format(0,2),($value->TranType == 'Buy') ? currency_format($value->LAmount,2) : currency_format(0,2),$value->bal));
                    }
                    fclose($file);
                };
                return Response::stream($callback, 200, $headers);
            }   else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'No data available');
                return Redirect::back();
            }
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Please selcte customer details');
            return Redirect::back();
        }
    }
    /* END LEDGER CUSTOMER*/
    /* Start Customer balance Report */ 
     public function customerbalancereport(Request $request)
    {
       $alllocations = Location::select('LocationCode')->get();
        
        $datas = LocationCustomer::select('Custcode','CustName','Balance','DealBalance')->where('Location', $request->locationcode);
        
        if(!empty($request->from_date) && !empty($request->to_date)) {
            $datas = $datas->whereBetween('CreatedDate', [Carbon::parse($request->from_date)->startOfDay(), Carbon::parse($request->to_date)->endOfDay()]);
        }

        if((!empty($request->zeroBalance)) && (!empty($request->zeroDealBalance))) {
            if(($request->zeroBalance == 'No') && ($request->zeroDealBalance == 'No')){
                $datas = $datas->where(function($query) {
                    $query->where('Balance', '!=' , 0)
                          ->orwhere('DealBalance', '!=', 0);
                });
            }
        }
        $datas = $datas->get();
        /*printArray($datas);die;*/
        foreach ($datas as $key => $value) {
            $datas[$key]->tot = $datas[$key]->Balance + $datas[$key]->DealBalance;
        }
        if ($request->ajax()) {
            return Datatables::of($datas)
                ->addIndexColumn()
                ->editColumn('Balance', function ($datas) {
                    return currency_format($datas->Balance,2);
                })   
                ->editColumn('DealBalance', function ($datas) {
                    return currency_format($datas->DealBalance,2);
                })   
                ->addColumn('tot', function ($datas) {
                    return currency_format($datas->tot,2);
                })
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        return view('reports.customerbalancereport',compact('alllocations'));
    }

    public function customerbalancereportexport(Request $request)
    {

        if($request->location){
            $datas = LocationCustomer::where('Location', $request->location);

           if(!empty($request->from_date) && !empty($request->to_date)) {
                $datas = $datas->whereBetween('CreatedDate', [Carbon::parse($request->from_date)->startOfDay(), Carbon::parse($request->to_date)->endOfDay()]);
            }
            if((!empty($request->zeroBalance)) && (!empty($request->zeroDealBalance))) {
                if(($request->zeroBalance == 'No') && ($request->zeroDealBalance == 'No')){
                    $datas = $datas->where(function($query) {
                        $query->where('Balance', '!=' , 0)
                              ->orwhere('DealBalance', '!=', 0);
                    });
                }
            } else if((empty($request->zeroBalance)) && (empty($request->zeroDealBalance))){
                $datas = $datas->where(function($query) {
                        $query->where('Balance', '!=' , 0)
                              ->orwhere('DealBalance', '!=', 0);
                    });
            }
            $datas = $datas->get();
            foreach ($datas as $key => $value) {
                $datas[$key]->tot = $datas[$key]->Balance + $datas[$key]->DealBalance;
            }


            if($request->export == 'PDF' && count($datas) > 0 ){
                $compArray = DB::table('company')->select('Description')->get();
                $compName = $compArray[0]->Description;
                $locationc = $request->location;
                $strFile  = 'cashcustomers.pdf';
                $title ='Customer Balance Report As On '.date('d/m/Y');
                $pdf = PDF::loadView('pdf/customerbalancereport',compact('datas','locationc','title','compName'));
                return $pdf->download($strFile);

            } else if($request->export == 'CSV' && count($datas) > 0 ){

                $headers = array(
                    "Content-type" => "text/csv",
                    "Content-Disposition" => "attachment; filename=cashcustomer.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
                $columns = array('Customer Code', 'Name', 'Balance');
                $callback = function() use ($datas, $columns)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $columns);

                    foreach($datas as $value) {
                        fputcsv($file, array($value->Custcode, $value->Name,$value->Balance));
                    }
                    fclose($file);
                };
                return Response::stream($callback, 200, $headers);

            } else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'No data available');
                return Redirect::back();
            }
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Please select location details');
            return Redirect::back();
        }
    }
    /* End Customer balance Report */


    public function ledger(Request $request){
        $datas = GeneralLedger::get();
        if ($request->ajax()) {
            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })
                  ->addColumn('Status', function ($datas) {
                  return '';             
                  })
                  ->addColumn('Select', function ($datas) {
                  return '';             
                  })
                  ->editColumn('amount', function ($datas) {
                        return currency_format($datas->amount,2);
                    })
                  ->editColumn('date', function ($datas) {
                        return date("d/m/Y", strtotime($datas->date));
                    })
                  ->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        return view('reports.ledger');
    }

    public function ttmasreport(Request $request){
        $fromDate = $request->from_date ? Carbon::parse($request->from_date)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->to_date ? Carbon::parse($request->to_date)->endOfDay() : Carbon::parse(now())->endOfDay();
        $locationc = Auth::user()->Location;
        $datasSummary = DB::table('dd_transaction')
                    ->leftJoin('customer_master', 'dd_transaction.Custcode', '=', 'customer_master.Custcode')
                    ->where('status', 3)
                    ->whereBetween('dd_transaction.TranDate', [Carbon::parse($fromDate)->startOfDay(), Carbon::parse($toDate)->endOfDay()])
                    ->select('dd_transaction.TransmissionDate','dd_transaction.TranDate','dd_transaction.PaymentDate','dd_transaction.TranNo','dd_transaction.CustName','dd_transaction.CustNationality','dd_transaction.CustPPNo','dd_transaction.DOB','customer_master.Address1','customer_master.Address2','customer_master.Address3','customer_master.Address4','customer_master.Address5','dd_transaction.LAmount','dd_transaction.ExchRate','dd_transaction.Comm','dd_transaction.CurrencyCode','dd_transaction.FAmount','dd_transaction.AgentName','dd_transaction.BenName','dd_transaction.BenAddress1','dd_transaction.BenAddress2','dd_transaction.BenAddress3')
                    ->get();
        if($request->export == 'PDF' && count($datasSummary) > 0 ){
            $compArray = DB::table('company')->select('Description')->get();
            $compName = $compArray[0]->Description;
            $strFile  = 'masreport.pdf';
            $pdf = PDF::loadView('pdf/transaction/masreport',compact('datasSummary','fromDate','toDate','locationc','title','compName'));
            return $pdf->download($strFile);
        }
        $fromDate = date_format($fromDate,"d-m-Y");
        $toDate = date_format($toDate,"d-m-Y");
        return view('reports.ttmasreport',compact('datasSummary','fromDate','toDate'));
    }

    public function tttransactionsummary(Request $request)
    {
        $fromDate = $request->from_date ? Carbon::parse($request->from_date)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->to_date ? Carbon::parse($request->to_date)->endOfDay() : Carbon::parse(now())->endOfDay();
        $locationc = Auth::user()->Location;
        $whereCond = "WHERE status = 3 AND TranDate BETWEEN '".$fromDate."' AND '".$toDate."'";
        $datasSummary = DB::select( DB::raw("SELECT  SUM(FAmount*ExchRate) as customerAmount, SUM(Comm) as customerComm, SUM(TotalAmount) as customerTotalAmount, SUM(FAmount*AgentRate) as agentAmount, SUM(AgentCommission) as agentComm, SUM(AgentTotal) as agentTotal, TranDate FROM tbl_dd_transaction ".$whereCond." GROUP BY DATE(tbl_dd_transaction.TranDate)"));
        if($request->export == 'PDF' && count($datasSummary) > 0 ){
            $compArray = DB::table('company')->select('Description')->get();
            $compName = $compArray[0]->Description;
            $strFile  = 'transactionsummary.pdf';
            $pdf = PDF::loadView('pdf/transaction/transactionsummary',compact('datasSummary','fromDate','toDate','locationc','title','compName'));
            return $pdf->download($strFile);
        }
        $fromDate = date_format($fromDate,"d-m-Y");
        $toDate = date_format($toDate,"d-m-Y");
        return view('reports.tttransactionsummary',compact('datasSummary','fromDate','toDate'));
    }

    public function tttransactiondetail(Request $request)
    {
        $from = $request->from_date ? Carbon::parse($request->from_date)->startOfDay() : Carbon::parse(now())->startOfDay();
        $to = $request->to_date ? Carbon::parse($request->to_date)->endOfDay() : Carbon::parse(now())->endOfDay();
        $where = "WHERE TranDate BETWEEN '".$from."' AND '".$to."'";
        $datas = DB::select( DB::raw("SELECT  TranDate FROM tbl_dd_transaction ".$where." GROUP BY DATE(tbl_dd_transaction.TranDate)"));
        $final = [];
        $locationc = Auth::user()->Location;
        foreach ($datas as $key => $value) {
            $data = DB::table('dd_transaction')
                ->whereBetween('TranDate', [Carbon::parse($value->TranDate)->startOfDay(), Carbon::parse($value->TranDate)->endOfDay()])
                ->where('status',3)
                ->select('TranDate','CurrencyCode')
                ->groupBy('CurrencyCode')
                ->get();
            foreach ($data as $key => $value) {
                $curr = Currency::where('CurrencyCode', $value->CurrencyCode)->first();
                $final[date("Y-m-d", strtotime($value->TranDate))][$curr->CurrencyName] = DDtransaction::whereBetween('TranDate', [Carbon::parse($value->TranDate)->startOfDay(), Carbon::parse($value->TranDate)->endOfDay()])->where('CurrencyCode',$value->CurrencyCode)->where('status',3)->get()->toArray();
            }
        }
        $from = date_format($from,"d-m-Y");
        $to = date_format($to,"d-m-Y");
        if($request->export == 'PDF' && count($final) > 0 ){
            $compArray = DB::table('company')->select('Description')->get();
            $compName = $compArray[0]->Description;
            $strFile  = 'transactiondetail.pdf';
            $pdf = PDF::loadView('pdf/transaction/transactiondetail',compact('final','from','to','locationc','title','compName'));
            return $pdf->download($strFile);
        }
        return view('reports.tttransactiondetail',compact('final','from','to'));
    }

    public function ttbookingreport(Request $request){

        $from = $request->from_date ? Carbon::parse($request->from_date)->startOfDay() : '';
        $to = $request->to_date ? Carbon::parse($request->to_date)->endOfDay() : '';
        //Carbon::parse(now())->startOfDay();
        //Carbon::parse(now())->endOfDay();
        $custName =  $request->custName;
        $agentName =  $request->agentName;
        $currency =  $request->currency;
        $compArray = DB::table('company')->select('Description')->get();
        $compName = $compArray[0]->Description;
        $locationc = Auth::user()->Location;

        $dataBuySell = DB::table('deal_tran_header')
                    ->leftJoin('deal_trans_detail', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo')
                    ->leftJoin('currency_master', 'deal_trans_detail.CurrencyCode', '=', 'currency_master.CurrencyCode');
        if(!empty($custName)){
            $dataBuySell  = $dataBuySell->where('deal_tran_header.CustomerName', '=',$request->custName);
        }
        if(!empty($currency)){
            $dataBuySell  = $dataBuySell->where('currency_master.CurrencyName', '=', $request->currency);
        }
        if(!empty( $from) && !empty( $to)){
            $dataBuySell  = $dataBuySell->whereBetween('deal_tran_header.DealDate', [Carbon::parse($from), Carbon::parse($to)]);
        }
        $dataBuySell = $dataBuySell->get();
        $final =[];
        foreach ($dataBuySell as $key => $value) {
            $curr = Currency::where('CurrencyCode', $value->CurrencyCode)->first();
            $final[$curr->CurrencyCode] = DB::table('deal_tran_header')
                    ->leftJoin('deal_trans_detail', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo')
                    ->leftJoin('currency_master', 'deal_trans_detail.CurrencyCode', '=', 'currency_master.CurrencyCode')
                    ->where('deal_trans_detail.CurrencyCode','=',$curr->CurrencyCode)
                    ->select('deal_trans_detail.TranNo','deal_tran_header.DealNo','deal_tran_header.CustomerName','deal_tran_header.DealDate','currency_master.CurrencyName','deal_trans_detail.FAmount','deal_trans_detail.Rate','deal_trans_detail.LAmount','deal_trans_detail.BalanceAmount','deal_trans_detail.ValidTill','deal_trans_detail.TranType','currency_master.CurrencyCode');
            if(!empty($custName)){
                $final[$curr->CurrencyCode]  = $final[$curr->CurrencyCode]->where('deal_tran_header.CustomerName', '=',$request->custName);
            }
            if(!empty($currency)){
                $final[$curr->CurrencyCode]  = $final[$curr->CurrencyCode]->where('currency_master.CurrencyName', '=', $request->currency);
            }
            if(!empty( $from) && !empty( $to)){
                $final[$curr->CurrencyCode]  = $final[$curr->CurrencyCode]->whereBetween('deal_tran_header.DealDate', [Carbon::parse($from), Carbon::parse($to)]);
            }
            $final[$curr->CurrencyCode] = $final[$curr->CurrencyCode]->get()->toArray();
        }
        $fromDate = $from != '' ? date_format($from,"d-m-Y") : '';
        $toDate = $to != '' ? date_format($to,"d-m-Y") : '';

        if($request->export == 'PDF' && (count($dataBuySell) > 0)){
            if($from != '' && $to != '') {
                $where = "WHERE DealDate BETWEEN '".$from."' AND '".$to."'";
            } else {
                $where = '';
            }
            $datas = DB::select( DB::raw("SELECT  DealDate FROM tbl_deal_tran_header ".$where." GROUP BY DATE(tbl_deal_tran_header.DealDate)"));

            foreach ($datas as $key => $value) {
                $data = DB::table('deal_tran_header')
                        ->leftJoin('deal_trans_detail', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo')
                        ->leftJoin('currency_master', 'deal_trans_detail.CurrencyCode', '=', 'currency_master.CurrencyCode')
                        ->whereBetween('DealDate', [Carbon::parse($value->DealDate)->startOfDay(), Carbon::parse($value->DealDate)->endOfDay()])
                        ->select('deal_tran_header.DealDate','currency_master.CurrencyCode')
                        ->groupBy('currency_master.CurrencyCode')
                        ->get();
                $final =[];
                foreach ($data as $key => $value) {
                    $curr = Currency::where('CurrencyCode', $value->CurrencyCode)->first();
                    $finalBuySell[$curr->CurrencyCode] = DB::table('deal_tran_header')
                            ->leftJoin('deal_trans_detail', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo')
                            ->leftJoin('currency_master', 'deal_trans_detail.CurrencyCode', '=', 'currency_master.CurrencyCode')
                            ->where('deal_trans_detail.CurrencyCode','=',$curr->CurrencyCode);
                    if(!empty($custName)){
                        $finalBuySell[$curr->CurrencyCode]  = $finalBuySell[$curr->CurrencyCode]->where('deal_tran_header.CustomerName', '=',$request->custName);
                    }
                    if(!empty($currency)){
                        $finalBuySell[$curr->CurrencyCode]  = $finalBuySell[$curr->CurrencyCode]->where('currency_master.CurrencyName', '=', $request->currency);
                    }
                    if(!empty( $from) && !empty( $to)){
                        $finalBuySell[$curr->CurrencyCode]  = $finalBuySell[$curr->CurrencyCode]->whereBetween('deal_tran_header.DealDate', [Carbon::parse($from), Carbon::parse($to)]);
                    }
                    $finalBuySell[$curr->CurrencyCode] = $finalBuySell[$curr->CurrencyCode]->get()->toArray();
                }
            }
            /*printArray($finalBuySell);die;*/
            $strFile  = 'bookingreport.pdf';
            $pdf = PDF::loadView('pdf/transaction/bookingreport',compact('finalBuySell','fromDate','toDate','locationc','title','compName'));
            return $pdf->download($strFile);
        }
        return view('reports.ttbookingreport',compact('dataBuySell','final','fromDate','toDate','custName','agentName','currency','compName'));
    }

    public function ttfundsaccepted(){
        return view('reports.ttfundsaccepted');
    }
    public function ttfundsremitted(){
        return view('reports.ttfundsremitted');
    }
    public function ttdeletedreport(){
        return view('reports.ttdeletedreport');
    }

    public function getcustomerdetails(Request $request)
    {
        $locationCode = Auth::user()->Location;
        $active ='1';
        $data = DB::table('customer_master')
            ->leftJoin('location_customer', 'customer_master.Custcode', '=', 'location_customer.Custcode')
            ->where('customer_master.Custcode', $request->code)
            ->where('location_customer.Location', $locationCode)
            ->where('customer_master.Active', $active)
            ->first();
        if($data){
            $curBal = $data->Balance;
            $data->Balance = currency_format($curBal,2);
            return response()->json(['status' => '200', 'data'=>$data]);
        } else {
            return response()->json(['status' => '201', 'content'=>'Wrong Detail!']);
        }
    }
    public function getBookingReport(Request $request)
    {
        $data = DB::table('deal_tran_header')
                ->leftJoin('deal_trans_detail', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo')
                ->leftJoin('currency_master', 'deal_trans_detail.CurrencyCode', '=', 'currency_master.CurrencyCode')
                ->where('deal_trans_detail.TranNo', "=", $request->TranNo)
                ->select('deal_tran_header.DealNo','deal_tran_header.CustomerName','deal_tran_header.DealDate','currency_master.CurrencyName','deal_trans_detail.FAmount','deal_trans_detail.Rate','deal_trans_detail.LAmount','deal_trans_detail.BalanceAmount','deal_trans_detail.ValidTill','deal_trans_detail.TranType')->get();

        return response()->json(['status' => '200', 'data' => $data]);
    }
    public function getCurrency(Request $request)
    {
        /*$data = Currency::where('CurrencyCode', $request->id)->first();*/
        $data = DB::table('currency_master')
                ->leftJoin('location_currency', 'location_currency.CurrencyCode', '=', 'currency_master.CurrencyCode')
                ->where('currency_master.CurrencyCode','SGD')
                ->select('location_currency.CurrencyCode','location_currency.AvgCost','location_currency.SellRate','location_currency.BuyRate','location_currency.Stock','currency_master.CurrencyName')
                ->where('currency_master.CurrencyCode', $request->id)
                ->where('location_currency.LocationCode', '=', Auth::user()->Location)
                ->first();
        if($data){
            return response()->json(['status' => '200', 'data' => $data]);
        } else {
            return response()->json(['status' => '201']);
        }
    }
}