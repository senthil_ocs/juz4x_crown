<?php

namespace App\Http\Controllers\reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use PDF;
use Carbon\Carbon;
use DB;
use DataTables;
use Config;
use App\Location;
use App\BuySell;
use App\BuySellDetails;
use Redirect;
use Excel;
use Response;


class BuySellTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $fromDate = $request->from_date ? Carbon::parse($request->from_date)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->to_date ? Carbon::parse($request->to_date)->endOfDay() : Carbon::parse(now())->endOfDay();
        $alllocations = Location::select('LocationCode')->get();

        if($request->ajax()){
            $datas = DB::table('buysell')->leftJoin('buysell_details', 'buysell_details.TranNo', '=', 'buysell.TranNo')->select('buysell.TranDate','buysell.LocationCode','buysell_details.TranType','buysell_details.FAmount','buysell_details.LAmount','buysell_details.Rate','buysell_details.TranType','buysell_details.CurrencyCode','buysell.CustCode','buysell.id','buysell_details.TranNo');
            if($request->location){
                $datas = $datas->where('buysell.LocationCode',$request->location);
            }
            if($request->from_date && $request->to_date){
                $from = Carbon::parse($request->from_date)->startOfDay();
                $to = Carbon::parse($request->to_date)->endOfDay();
                $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
            }
            $datas = $datas->get();

            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })
                ->editColumn('TranDate', function ($datas) {
                    $date = \Carbon\Carbon::parse($datas->TranDate);
                    return $date->format('d/m/Y');
                })
                ->editColumn('FAmount', function ($datas) {
                    return currency_format($datas->FAmount,2);
                })
                ->editColumn('Rate', function ($datas) {
                    return currency_format($datas->Rate);
                })
                ->editColumn('LAmount', function ($datas) {
                    return currency_format($datas->LAmount,2);
                })
                ->addColumn('bs', function ($datas) {
                    if($datas->TranType == 'Sell'){
                        return '<span class="badge badge-info" style="font-size: 13px;">Sell</span>';
                    } else {
                        return '<span class="badge badge-success" style="font-size: 13px;">Buy</span>';
                    }
                })->escapeColumns([])
                ->make(true);
        }
        $fromDate = date_format($fromDate,"d-m-Y");
        $toDate = date_format($toDate,"d-m-Y");
    	return view('reports.buyselltransactionreport', compact('alllocations','fromDate','toDate'));
    }

    public function buyselltransactionreportexport(Request $request)
    {
        $fromDate = $request->from_date ? Carbon::parse($request->from_date)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->to_date ? Carbon::parse($request->to_date)->endOfDay() : Carbon::parse(now())->endOfDay();
        // printArray($request->all());die;
        $datas = DB::table('buysell')->leftJoin('buysell_details', 'buysell_details.TranNo', '=', 'buysell.TranNo');
        if($request->location){
            $datas = $datas->where('buysell.LocationCode',$request->location);
        }
        if($request->from_date && $request->to_date){
            $from = Carbon::parse($request->from_date)->startOfDay();
            $to = Carbon::parse($request->to_date)->endOfDay();
            $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
        }
        $datas = $datas->get();

        if($request->export == 'PDF' && count($datas) > 0 ){

            $strFile  = 'buyselltransactionreport.pdf';
            $data = [];
            foreach ($datas as $key => $value) {
                $date = \Carbon\Carbon::parse($value->TranDate);
                $dates = $date->format('d/m/Y');
                if($value->TranType == 'Sell'){
                    $bs = 'Sell';
                } else {
                    $bs = 'Buy';
                }
                $data[] =   array(
                    'TranNo' => $value->TranNo,
                    'TranDate' => $dates,
                    'CustCode' => $value->CustCode,
                    'Currency' => $value->CurrencyCode,
                    'bs' => $bs,
                    'FCurrency'=>currency_format($value->FAmount,2),
                    'Rate'=>currency_format($value->Rate),
                    'LAmount'=>currency_format($value->LAmount,2),
                );
            }
            $fromdate = displayDateformat($from);
            $todate = displayDateformat($to);
            $compArray = DB::table('company')->select('Description')->get();
            $compName = $compArray[0]->Description;

            $pdf = PDF::loadView('pdf.ledger.buyselltransaction',compact('data','fromdate','todate','compName'));
            return $pdf->download($strFile);

        } else if($request->export == 'CSV' && count($datas) > 0 ){

            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=buyselltransactionreport.csv",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );
            $columns = array('TransNo', 'TransDate', 'CustCode', 'Currency', 'FAmount', 'Ex.Rate','LAmount');
            $callback = function() use ($datas, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach($datas as $value) {
                    $date = \Carbon\Carbon::parse($value->TranDate);
                    $date->format('d/m/Y');
                    if($value->TranType == 'Sell'){
                        $bs = 'Sell';
                    } else {
                        $bs = 'Buy';
                    }
                    fputcsv($file, array($value->TranNo, $date,$value->CustCode,$bs,currency_format($value->FAmount,2),currency_format($value->Rate),currency_format($value->LAmount,2)));
                }
                fclose($file);
            };
            return Response::stream($callback, 200, $headers);

        }else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'No data available');
            return Redirect::back();
        }
    }
 
  public function buysellprofitreport(Request $request)
  {
      $fromDate = $request->fdate ? Carbon::parse($request->fdate)->startOfDay() : Carbon::parse(now())->startOfDay();
        $toDate = $request->tdate ? Carbon::parse($request->tdate)->endOfDay() : Carbon::parse(now())->endOfDay();
        $alllocations = Location::select('LocationCode')->get();
             
           // $datas = DB::table('buysell_details')  
           //  ->select(
           //      DB::raw('SUM(tbl_buysell_details.FAmount) as totalSell'),
           //      DB::raw('SUM(tbl_buysell_details.LAmount) as totalSgd'),DB::raw('SUM(tbl_buysell_details.LAmount*tbl_buysell_details.LastAverageCost) as buyAmount'))
           //  ->join('buysell', 'buysell.TranNo', '=', 'buysell_details.TranNo')->groupBy('buysell_details.CurrencyCode')->get();
           //  print_r($datas);exit;
           
        if($request->ajax()){
         
            $datas = DB::table('buysell_details')  
            ->select(
                DB::raw('SUM(tbl_buysell_details.FAmount) as totalSell'),
                DB::raw('SUM(tbl_buysell_details.LAmount) as totalSgd'),DB::raw('SUM(tbl_buysell_details.FAmount*tbl_buysell_details.LastAverageCost) as buyAmount'),'buysell_details.CurrencyCode','buysell_details.id')->join('buysell', 'buysell.TranNo', '=', 'buysell_details.TranNo');
               // ->where('buysell_details.TranType ', '=', 'Sell');
            
            if($request->location){
                $datas = $datas->where('buysell.LocationCode',$request->location);
            }

            if($request->currency_code)
           {
               $currcode = $request->currency_code;
            $datas = $datas->where('buysell_details.CurrencyCode', $currcode);
           }

            if($request->fdate && $request->tdate){
                $from = Carbon::parse($request->fdate)->startOfDay();
                $to = Carbon::parse($request->tdate)->endOfDay();
                $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
            }
         
            $from = Carbon::parse($request->fdate)->startOfDay();
            $to = Carbon::parse($request->tdate)->endOfDay();
            $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
            $datas = $datas->where('buysell_details.TranType','Sell')
            ->groupBy('buysell_details.CurrencyCode')->get();
          
            
            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })
                ->editColumn('CurrencyCode', function ($datas) {
                    return   $datas->CurrencyCode;
                })
                ->editColumn('totalSell', function ($datas) {
                    return currency_format($datas->totalSell,2);
                })
                ->editColumn('totalSgd', function ($datas) {
                    return currency_format($datas->totalSgd,2);
                })
                ->editColumn('buyAmount', function ($datas) {
                    return currency_format($datas->buyAmount,2);
                })
                ->addColumn('profit', function ($datas) {
                    $profit =$datas->totalSgd-$datas->buyAmount;
                    return   currency_format($profit,2);
                })->escapeColumns([])
                ->make(true);
        }
         
        $fromDate = date_format($fromDate,"d-m-Y");
        $toDate = date_format($toDate,"d-m-Y");
        return view('reports.profitreport', compact('alllocations','fromDate','toDate'));    
  } 

  public function  totalprofit(Request $request)
   {
     $fromDate = $request->fdate ? Carbon::parse($request->fdate)->startOfDay() : Carbon::parse(now())->startOfDay();
    $toDate = $request->tdate ? Carbon::parse($request->tdate)->endOfDay() : Carbon::parse(now())->endOfDay();
     $alllocations = Location::select('LocationCode')->get();


         $datas = DB::table('buysell_details') ->select(
                DB::raw('SUM(tbl_buysell_details.FAmount) as totalSell'),
                DB::raw('SUM(tbl_buysell_details.LAmount) as totalSgd'),
                DB::raw('SUM(tbl_buysell_details.FAmount*tbl_buysell_details.LastAverageCost) as buyAmount'),'buysell_details.CurrencyCode','buysell_details.id')->join('buysell', 'buysell.TranNo', '=', 'buysell_details.TranNo');

            if($request->location){
                $datas = $datas->where('buysell.LocationCode',$request->location);
            }

            if($request->currency_code)
           {
               $currcode = $request->currency_code;
            $datas = $datas->where('buysell_details.CurrencyCode', $currcode);
           }

           if($request->fdate && $request->tdate){
                $from = Carbon::parse($request->fdate)->startOfDay();
                $to = Carbon::parse($request->tdate)->endOfDay();
                $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
            }
            $from = Carbon::parse($request->fdate)->startOfDay();
            $to = Carbon::parse($request->tdate)->endOfDay();
            $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
            $datas = $datas->where('buysell_details.TranType','Sell')
            ->groupBy('buysell_details.CurrencyCode')->get();

            //   $profit =$datas->totalSgd-$datas->buyAmount;
    
             $profittotal=0;
                 $profit =0;
            foreach ($datas as $key => $value) {
      
               $profit =$value->totalSgd-$value->buyAmount;
                $profittotal += $profit;

            }
          $profittotal = currency_format($profittotal,2);
     
             
           
               return response()->json(['status' => '200', 'profittotal'=>$profittotal]);
                 
             
     }


   public function buysellprofitreportexport(Request $request)
    {
     $fromDate = $request->from_date ? Carbon::parse($request->from_date)->startOfDay() : Carbon::parse(now())->startOfDay();
    $toDate = $request->to_date ? Carbon::parse($request->to_date)->endOfDay() : Carbon::parse(now())->endOfDay();
     $alllocations = Location::select('LocationCode')->get();


         $datas = DB::table('buysell_details') ->select(
                DB::raw('SUM(tbl_buysell_details.FAmount) as totalSell'),
                DB::raw('SUM(tbl_buysell_details.LAmount) as totalSgd'),
                DB::raw('SUM(tbl_buysell_details.FAmount*tbl_buysell_details.LastAverageCost) as buyAmount'),'buysell_details.CurrencyCode','buysell_details.id')->join('buysell', 'buysell.TranNo', '=', 'buysell_details.TranNo');

            if($request->location){
                $datas = $datas->where('buysell.LocationCode',$request->location);
            }

            if($request->currency_code)
           {
               $currcode = $request->currency_code;
            $datas = $datas->where('buysell_details.CurrencyCode', $currcode);
           }

           if($request->from_date && $request->to_date){
                $from = Carbon::parse($request->from_date)->startOfDay();
                $to = Carbon::parse($request->to_date)->endOfDay();
                $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
            }
            $from = Carbon::parse($request->from_date)->startOfDay();
            $to = Carbon::parse($request->to_date)->endOfDay();
            $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
            $datas = $datas->where('buysell_details.TranType','Sell')
            ->groupBy('buysell_details.CurrencyCode')->get();

            //   $profit =$datas->totalSgd-$datas->buyAmount;
       
           
        if($request->export == 'PDF' && count($datas) > 0 ){
         
            $strFile  = 'profitreport.pdf';
            $data = [];
             $profittotal=0;

            foreach ($datas as $key => $value) {
      

               $profit =$value->totalSgd-$value->buyAmount;
                $profittotal += $profit;

                $data[] =   array(
                    'CurrencyCode' => $value->CurrencyCode,
                    'totalSell' => currency_format($value->totalSell,2),
                    'totalSgd' => currency_format($value->totalSgd,2),
                    'buyAmount' => currency_format($value->buyAmount,2),
                    'profit' => currency_format($profit,2),
                    
                );
            }
          $profittotal = currency_format($profittotal,2);
          $fromdate = date_format($fromDate,"d-m-Y");
           $todate = date_format($toDate,"d-m-Y");
            // print_r($fromdate);exit;
            $compArray = DB::table('company')->select('Description')->get();
            $compName = $compArray[0]->Description;


            $pdf = PDF::loadView('pdf.ledger.buysellprofitreport',compact('data','profittotal','fromdate','todate','compName'));
            return $pdf->download($strFile);

        } else if($request->export == 'CSV' && count($datas) > 0 ){

            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=profitreport.csv",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );
            $columns = array('CurrencyCode', 'Sell in FAmount', 'Sell in LAmount(SGD)', 'BuyAmount', 'Profit');
            $callback = function() use ($datas, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);
                $profittotal=0;
                foreach($datas as $value) {
                     $profit =$value->totalSgd-$value->buyAmount;
                    $profittotal += $profit;
                    fputcsv($file, array($value->CurrencyCode,currency_format($value->totalSell,2),currency_format($value->totalSgd,2),currency_format($value->buyAmount,2),currency_format($profit,2)));

                }
                fputcsv($file, array('','','','Total',currency_format($profittotal,2)));
                fclose($file);
            };
            return Response::stream($callback, 200, $headers);

        }else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'No data available');
            return Redirect::back();
        }
    }


}
