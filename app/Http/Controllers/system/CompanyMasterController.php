<?php
namespace App\Http\Controllers\system;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\System;
use Auth;
Use Exception;

class CompanyMasterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
      $companyDetails  = System::where('id', 1)->first()->toArray();
      return view('system.company',compact('companyDetails'));
    }

    public function updatecompany(Request $request)
    {
    	try {
	    	$company 						= System::find(1);
	    	$company->Description			= isset($request->company_name) ? $request->company_name : '';
	    	$company->Address1 				= isset($request->address1) ? $request->address1 : '';
	    	$company->Address2 				= isset($request->address2) ? $request->address2 : '';
	    	$company->Address3  			= isset($request->address3) ? $request->address3 : '';
	    	$company->Postalcode 			= isset($request->postal_code) ? $request->postal_code : '';
	    	$company->Phone 				= isset($request->phone) ? $request->phone : '';
	    	$company->Fax 					= isset($request->fax) ? $request->fax : '';
	    	$company->Email 				= isset($request->email) ? $request->email : '';
	    	$company->url 					= isset($request->url) ? $request->url : '';
	    	$company->TaxRef 				= isset($request->tax_ref) ? $request->tax_ref : '';
	    	$company->TaxPerc 				= isset($request->tax_perc) ? $request->tax_perc : '';
	    	$company->MassLimit 			= isset($request->mass_limit) ? $request->mass_limit : '';
	    	$company->TaxRef 				= isset($request->tax_ref) ? $request->tax_ref : '';
	    	$company->MaxPasswordAttempt 	= isset($request->mass_password_attampt) ? $request->mass_password_attampt : '';
	    	$company->UENNo 				= isset($request->uen_no) ? $request->uen_no : '';
	    	$company->save();
	    	$request->session()->flash('message.level', 'success');
	      $request->session()->flash('message.content', 'Company Details updated successfully!');
    	} catch (Exception $exception) {
        $request->session()->flash('message.level', 'danger');
	      $request->session()->flash('message.content', 'Error: Company Details could not be updated');
    	}
    	return redirect('company');        
    }
}
