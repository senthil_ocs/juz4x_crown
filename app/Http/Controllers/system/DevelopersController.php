<?php

namespace App\Http\Controllers\system;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use File;
use App\Nationality;
use Config;

class DevelopersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ddtransaction(Request $request)
    {
        $nationality = Nationality::orderBy('Code')->get();
        $identifire  = Config::get('app.identifire_type');
        return view('ddtransaction', compact('nationality','identifire'));
    }

    public function insert_update(Request $request)
    {
        printArray($request->all());die;
    }

    public function index(Request $request)
    {
    	return view('system.developers');
    }

    public function clearlog(Request $request)
    {	
    	// if($request->logdate){
	    // 	for ($i=1; $i < 10; $i++) { 
	    // 		$date = Carbon::parse($request->logdate)->subDays($i)->format('Y-m-d');
	    // 		if(Storage::disk('log')->exists('/error/'.$date.'.txt')){
	    // 			Storage::disk('log')->delete('/error/'.$date.'.txt');
	    // 		}
	    // 	}
	    // 	$request->session()->flash('message.level', 'success');
	    // 	$request->session()->flash('message.content','successfully delete the log files');
    	// } else {
    	// 	$request->session()->flash('message.level', 'danger');
    	// 	$request->session()->flash('message.content','Please Select the Log Dates');
    	// }
    	
    	if($request->logdate){
    		$date = Carbon::parse($request->logdate)->format('Y-m-d');
    		if(Storage::disk('log')->exists('/error/'.$date.'.txt')){
    			Storage::disk('log')->delete('/error/'.$date.'.txt');
	    		$request->session()->flash('message.level', 'success');
	    		$request->session()->flash('message.content','successfully delete the log files');
    		} else {
    			$request->session()->flash('message.level', 'danger');	
    			$request->session()->flash('message.content','Not Found!');
    		}
    	} else {
    		$request->session()->flash('message.level', 'danger');
    		$request->session()->flash('message.content','Please Select the Log Dates');
    	}
    	return redirect()->back();
    }


}
