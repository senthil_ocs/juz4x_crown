<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Config;
use App\Location;
use App\LocationCurrency;
use App\Currency;
use DB;
use Auth;
use App\LocationCustomer;

class BalanceController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }    

    public function index(Request $request, Datatables $datatable)
    {
        /*printArray($request->all()); exit;*/
        //$currencybalancedetails = LocationCurrency::with('currencies')->where('LocationCode',env('USER_LOCATION'))->get();
        //printArray($currencybalancedetails); exit;
        $currency = [];

        $alllocations = Location::get();
         $locationcode = "";
        $customer = [];
         if(isset($request->locationcode)) {
            $locationcode = $request->locationcode;
            $customer = DB::table('location_customer')
            ->where('Location', '=', $request->locationcode)
            ->select('Custcode','CustName')
            ->get();
        } else {
            $customer = DB::table('location_customer')
            ->where('Location', '=', Auth::user()->Location)
            ->select('Custcode','CustName')
            ->get();
        }
        if(isset($request->locationcode)) {
            $locationcode = $request->locationcode;
            $currency = DB::table('location_currency')
            ->leftJoin('currency_master', 'currency_master.CurrencyCode', '=', 'location_currency.CurrencyCode')
            ->where('location_currency.LocationCode', '=', $request->locationcode)
            ->select('location_currency.CurrencyCode','currency_master.CurrencyName')
            ->get();
        } else {
            $currency = DB::table('location_currency')
            ->leftJoin('currency_master', 'currency_master.CurrencyCode', '=', 'location_currency.CurrencyCode')
            ->where('location_currency.LocationCode', '=', Auth::user()->Location)
            ->select('location_currency.CurrencyCode','currency_master.CurrencyName')
            ->get();
        }
       
        if(isset($request->locationcode)) {
            $locationcode = $request->locationcode;
            $currency = LocationCurrency::with('currencies')->where('LocationCode', '=', $request->locationcode)->get();
        } else {
            $currency = LocationCurrency::with('currencies')->where('LocationCode', '=', Auth::user()->Location)->get();
        }
        if ($request->ajax()) {
            $datas = LocationCurrency::with('currencies');
            if (isset($request->locationcode)) {
                $datas->where('LocationCode', '=', $request->locationcode);
            } else {
                $datas->where('LocationCode', '=', Auth::user()->Location);
            }
            if ($request->has('currency_code')  && !empty($request->currency_code)) {
                $datas->where('CurrencyCode', '=', $request->currency_code);
            } else {
                $datas->where(function($query)
                    {
                        $query->where('Stock','!=',0)
                              ->orWhere('DealStock','!=',0);
                    });
            }
            $datas = $datas->orderBy('id', 'desc')->get();
           
            return Datatables::of($datas)
                ->addIndexColumn()
                ->editColumn('Stock', function ($datas) {
                    return currency_format($datas->Stock,2);
                })
                ->editColumn('AvgCost', function ($datas) {
                    return currency_format($datas->AvgCost);
                })
                ->addColumn('CurrencyName', function ($datas) {
                    return isset($datas->currencies->CurrencyName) ? $datas->currencies->CurrencyName : '';
                })
                ->addColumn('LValue', function ($datas) {
                    $lvalue = $datas->Stock*$datas->AvgCost;
                    return currency_format($lvalue,2);
                }) 
                ->addColumn('DealStock', function ($datas) {
                    return currency_format($datas->DealStock,2);
                })
                ->addColumn('LValueDS', function ($datas) {
                    $dlvalue = $datas->DealStock*$datas->AvgCost;
                    return currency_format($dlvalue,2);
                })       
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        return view('transaction.balances',compact('alllocations','currency'));
    }

    public function customerbalances(Request $request)
    {
        if ($request->ajax()) {
            $datas = LocationCustomer::select('Balance','DealBalance','id','Custcode','CustName')->where('Location', $request->locationcode);
                   

            if (isset($request->customercode)) {
                $datas->where('Custcode', '=', $request->customercode);
            }else{
                 $datas->where('Balance', '!=' , 0)->orWhere('DealBalance','!=',0);
            }
            return Datatables::of($datas)
                ->addIndexColumn()
                ->editColumn('Balance', function ($datas) {
                    return currency_format($datas->Balance,2);
                })   
                ->editColumn('DealBalance', function ($datas) {
                    return currency_format($datas->DealBalance,2);
                })   
                ->addColumn('tot', function ($datas) {
                    return currency_format($datas->Balance + $datas->DealBalance,2);
                })
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
    }
}
