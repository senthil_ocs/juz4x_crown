<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Config;
use App\Nationality;
use App\SettlementMode;
use App\Customer;
use App\BuySell;
use App\Currency;
use App\Country;
use App\BuySellDetails;
use App\LocationCurrency;
use App\LocationCustomer;
use App\CurrencyTransaction;
use App\Location;
use App\LocationCurrencyTransaction;
use App\LocationCustomerTransaction;
use App\DealTransactionHeader;
use App\DealTransactionDetail;
use App\CashCustomer;
use DB;
use Auth;

class BuySellController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request)
    {   
        $maxTransNo = 1;
        $data = BuySell::select('TranNo')->orderBy('id', 'DESC')->first();
        if($data != ''){
            $maxTransNo = $data->TranNo+1;
        }
        $vid = str_pad($maxTransNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
        $identifire = Config::get('app.identifire_type');
        
        $country = Country::select('Country')->orderBy('Country', 'ASC')->get();
    	$nationality = Nationality::orderBy('Code')->get();
    	$settlementmode = SettlementMode::orderBy('SettlementCode')->get();
        $general_type = collect([
            'Local Individual',
            'Overseas Individual'
        ]);

    	return view('transaction.buy_sell', compact('nationality','country','settlementmode','identifire','vid','general_type'));
    }
    
    public function voucherno(Request $request)
    {
        $maxTransNo = 1;
        $data = BuySell::select('TranNo')->orderBy('id', 'DESC')->first();
        if($data != ''){
            $maxTransNo = $data->TranNo+1;
        }
        $vid = str_pad($maxTransNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
        return response()->json(['status' => '200', 'data' => $vid]);
    }

    public function getcustomerdetails(Request $request)
    {
    	$loccode = Auth::user()->Location;

    	$customerlocationdetails = LocationCustomer::where('Custcode',$request->cusid)->where('Location',$loccode)->pluck('Balance')->all();
        $cusbalance = !empty($customerlocationdetails[0]) ? $customerlocationdetails[0] : '0.00';
        $data = Customer::where('Custcode', $request->cusid)->first();
        //Get Deals
        $dealinfo = DealTransactionHeader::where('CustomerCode',$request->cusid)->where('NRICNo',isset($data->NRICNo)?$data->NRICNo:'')->where('LocationCode',$loccode)->get();
        if(count($dealinfo) > 0) {
            foreach($dealinfo as $deal) {
                $dealno = $deal->DealNo;
                $dealdetails = DealTransactionDetail::where('DealNo',$dealno)->get();
                if(count($dealdetails) > 0) {
                    $dealstr = '';
                    $dealstatus='';
                    $d = 1;
                    foreach($dealdetails as $detaildeal) {
                        $dealdate = convertDateformat($detaildeal->CreatedDate);
                        if($detaildeal->Status == 'Realised') {
                            $dealstatus = 'disabled';
                        }else{
                             $dealstatus='';
                        } 
                        $dealstr .= '<tr>
                                     <td>'.$d.'</td>
                                     <td>'.$detaildeal->DealNo.'</td>
                                     <td>'.$dealdate.'</td>
                                     <td>'.$detaildeal->CurrencyCode.'</td>
                                     <td>'.$detaildeal->TranType.'</td>
                                     <td>'.currency_format($detaildeal->FAmount,2).'</td>
                                     <td>'.currency_format($detaildeal->RealisedAmount,2).'</td>
                                     <td>'.currency_format($detaildeal->BalanceAmount,2).'</td>
                                     <td>'.currency_format($detaildeal->Rate).'</td>
                                     <td>'.convertDateformat($detaildeal->ValidTill).'</td>
                                     <td>'.$detaildeal->Status.'</td>
                                     <td><input type="checkbox" id="chosendeal" name="chosendeal[]" value="'.$detaildeal->TranNo.'" checked="checked" '.$dealstatus.' /></td>
                                     </tr>';
                        $d++;
                    }
                    
                }
            }
            $data['dealentry'] = isset($dealstr) ? $dealstr : '';
        }
        //Get Deals
        if($data){
            $data['customerbalance'] = currency_format($cusbalance,2);
            return response()->json(['status' => '200', 'data' => $data]);
        } else {
            return response()->json(['status' => '201']);
        }
    }
    
    public function insert_update(Request $request)
    {
        $rout  = \Route::getFacadeRoot()->current()->uri();
        postLog($rout,"BuySell Details",json_encode($request->all()));
        if($request->pno_nric != '') {
            $cashCustomer = CashCustomer::where('PPNo', $request->pno_nric)->first();
            if(!isset($cashCustomer->PPNo) && $cashCustomer->count() == 0){
                $cashcustomer               = new CashCustomer;
                $cashcustomer->CustCode     = $request->customer_code;
                $cashcustomer->PPNo         = $request->pno_nric;
                $cashcustomer->Name         = $request->customer_name;
                $cashcustomer->DOB          = $request->dob;
                $cashcustomer->Nationality  = $request->nationality;
                $cashcustomer->Address1     = $request->address;
                $cashcustomer->Address2     = $request->address1;
                $cashcustomer->PostalCode   = $request->postal_code;
                $cashcustomer->Address4     = $request->country;
                $cashcustomer->MCType       = $request->mc_type;
                $cashcustomer->PhoneNo      = $request->phone_no;
                $cashcustomer->Activebit    = 1;
                $cashcustomer->IdentifierType =  $request->identifier_type;
                $cashcustomer->passexpiry   = $request->doe;
                $cashcustomer->UnitNo       = $request->unit_no;
                $cashcustomer->CreateDate   = now();
                $cashcustomer->CreateUser   = Auth::user()->username;
                $cashcustomer->ModifyDate   = now();
                $cashcustomer->ModifyUser   = Auth::user()->username;
                $cashcustomer->save();
                $alllocations = Location::get();
                foreach($alllocations as $location) {
                    $locationcustomerdata = ["Custcode" => $request->customer_code,
                         "CustName"            => isset($request->customer_name) ? $request->customer_name : '',
                         "Address1"            => isset($request->address) ? $request->address : '',
                         "Address2"            => isset($request->address1) ? $request->address1 : '',
                         "Address3"            => '',
                         "Phone"               => isset($request->phone_no) ? $request->phone_no : '',
                         "Fax"                 => '',
                         "Balance"             => 0.00,
                         "Name"                => isset($request->customer_name) ? $request->customer_name : '',
                         "NRICNo"              => isset($request->pno_nric) ? $request->pno_nric : '',
                         "Ob"                  => 0.00,
                         "CreditLimit"         => 0.00,
                         "Remarks"             => '',
                         "TTCustomer"          => 0,
                         "RemittanceLicensee"  => 0,
                         "Country"             => isset($request->country) ? $request->country : '',
                         "Location"            => $location->LocationCode,
                         "MoneyChangerLicense" => '',
                         "TradingLicense"      => '',
                         "DOB"                 => isset($request->dob) ? $request->dob : '',
                         "DOE"                 => isset($request->doe) ? $request->doe : '',
                         "MCType"              => isset($request->mc_type) ? $request->mc_type : '',
                         "TTType"              => '',
                         "DailyCreditLimit"    => 0.00,
                         "DealBalance"         => 0.00,
                         "CreatedBy"           => Auth::user()->username,
                         "CreatedDate"         => now(),
                         "ModifiedBy"          =>Auth::user()->username,
                         "ModifiedDate"        =>now(),
                         "WebCustAllowed"      => 0,
                         "WebCustCode"         => '',
                         "Active"              => isset($request->codeactive) ? 1 : 0,
                         "HighRisk"            => isset($request->highrisk) ? 1 : 0,
                        ];
                    LocationCustomer::create($locationcustomerdata);    
                }
            }
        }
        DB::beginTransaction();
        //printArray($request->all()); exit;
        //if(!isset($request->cusbalance)) {
            //$request->cusbalance = 0.00;
        //}
        //Insert into buysell table
        try {            
            if($request->customer_code!='' && (isset($request->td_lamount) && count($request->td_lamount) > 0)) {                
                $data = ["TranNo" 			=> $request->voucher_no,
                         "TranDate" 		=> now(),
                         "DocType" 			=> isset($request->major) ? $request->major : '',
                         "RefNo" 			=> isset($request->minor) ? $request->minor : '',
                         "PPNo" 			=> isset($request->pno_nric) ? $request->pno_nric : '',
                         "Nationality" 		=> $request->nationality,
                         "Name" 			=> "",
                         "CustName" 		=> $request->customer_name,
                         "Address1" 		=> $request->address,
                         "Address2" 		=> $request->address1,
                         "Address3" 		=> "",
                         "Address4" 		=> "",
                         "Address5" 		=> "",
                         "CustCode" 		=> $request->customer_code,
                         "LocationCode" 	=> $request->location,
                         "Remarks" 			=> $request->remarks,
                         "TTRefNo" 			=> 0,
                         "Deleted" 			=> "",
                         "DOB" 				=> !empty($request->dob) ? convertDateTimeformat($request->dob) : '1990-01-01 00:00:00',
                         "MCType" 			=> isset($request->mc_type) ? $request->mc_type : '',
                         "TerminalName" 	=> "",
                         "CreatedBy" 		=> Auth::user()->username,
                         "CreatedDate" 		=> now(),
                         "ModifiedBy" 		=> Auth::user()->username,
                         "ModifiedDate" 	=> now(),
                         "SettlementMode" 	=> "",
                         "IdentifierType" 	=> $request->identifier_type,
                         "SuspiciousTran" 	=> 0,
                         "PhoneNo" 			=> $request->phone_no,
                         ];
                        BuySell::create($data);
                    //Insert into busell_details table
                    for($d=0;$d<count($request->td_lamount);$d++) {

                        $td_famount = str_replace(',', '', $request->td_famount[$d]);
                        $td_famount = (float)$td_famount;

                        $td_lamount = str_replace(',', '', $request->td_lamount[$d]);
                        $td_lamount = (float)$td_lamount;

                       	$currcode = $request->td_currency[$d];
                        $loccode = Auth::user()->Location;

                        $loccurrencydetails = LocationCurrency::where('CurrencyCode', $currcode)->where('LocationCode', $loccode)->first();
                        $lastavgcost = isset($loccurrencydetails->AvgCost) ? $loccurrencydetails->AvgCost : '0.00';

                        $detailsdata = ["TranNo" 			=> $request->voucher_no,
                                        "Line" 				=> $d+1,
                                        "TranType" 			=> $request->td_type[$d],
                                        "CurrencyCode" 		=> $request->td_currency[$d],
                                        "Rate" 				=> $request->td_rate[$d],
                                        "FAmount" 			=> $td_famount,
                                        "LAmount" 			=> $td_lamount,
                                        "TTRefN0" 			=> 0,
                                        "LastAverageCost" 	=> $lastavgcost,
                                        "CreatedBy" 		=> Auth::user()->username,
                                        "CreatedDate" 		=> now(),
                                        "ModifiedBy" 		=> Auth::user()->username,
                                        "ModifiedDate" 		=> now()
                                       ];                 
                        $insertbuselldetails = BuySellDetails::create($detailsdata);
                        $buyselldetailid = $insertbuselldetails->id;

                        //Get Stock from Location Currency based
                        $locstock = getLocationCurrencyStock($loccode,$currcode);
                        //Get Stock from Location Currency based
                        
                        //Update in tbl_location_currency table                        
                        if($request->td_type[$d]=='Buy') {                            
                            $buyamount = $td_famount;
                            $buyrate = $request->td_rate[$d];                          

                            if(env('AVG_METHOD')=='FIFO') {
                                $avgcost = DB::select("CALL GetAvgCostLocation('".$currcode."','".$loccode."','".$buyamount."','".$buyrate."')"); //execute stored procedure
                            } else {
                                $avgcost = DB::select("CALL GetAvgCostLocation('".$currcode."','".$loccode."','".$buyamount."','".$buyrate."')"); //execute stored procedure
                            }                           
                            $avgcost = $avgcost[0]->agvCost;
                            postLog($rout,"Average Cost from Stored Procedure",$avgcost); // write average cost to log file
                            LocationCurrency::where('CurrencyCode', $currcode)
                                              ->where('LocationCode', $loccode)
                                              ->increment('Stock', $td_famount, ['AvgCost' => $avgcost]);
                            LocationCustomer::where('Custcode',$request->customer_code)->where('Location',$loccode)->decrement('Balance', $td_lamount);
                        } else {
                        	$avgcost = $request->td_rate[$d];
                            LocationCurrency::where('CurrencyCode', $request->td_currency[$d])
                                              ->where('LocationCode', $loccode)
                                              ->decrement('Stock', $td_famount);
                            LocationCustomer::where('Custcode',$request->customer_code)->where('Location',$loccode)->increment('Balance', $td_lamount);
                        }                       

                        if($buyselldetailid > 0) {
                            
                            if(empty($avgcost)) {
                                $avgcost = '0.00';
                            }
                            //echo $currcode.'--1-'.$loccode.'--2-'.$request->td_type[$d].'--3--'.$request->td_famount[$d].'--4-'.$request->td_lamount[$d].'--5--'.$avgcost.'--6-'.$locstock.'--7-'.$request->customer_code.'--8-'.$request->cusbalance; exit;
                            $this->insertCurrencyOs($currcode,$loccode,$request->td_type[$d],$td_famount,$td_lamount,$avgcost,$locstock,$request->customer_code,$request->cusbalance);
                        }                 
                        //Update in tbl_location_currency table
                    }
                    //Insert into buysell_details table
                    // $request->session()->flash('message.level', 'success');
                    // $request->session()->flash('message.content', 'Transaction inserted successfully!');    
                    $status = '200';
                    $msg = 'Transaction inserted successfully!';
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            $status = '201';
            $msg = $e->getMessage();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
        } catch(\Exception $e)
        {
            DB::rollback();
            $status = '201';
            $msg = $e->getMessage();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
        }
        
        DB::commit();
        // return redirect('buysell');
        return response()->json(['status' => $status, 'content' => $msg]);
    }

    public function getincurrencydetails(Request $request)
    {
        $locationCode = Auth::user()->Location;
        //printArray($request->all());
        $data = DB::table('currency_master')->where('currency_master.CurrencyCode', '=', $request->in_currency)
            ->leftJoin('location_currency', 'location_currency.CurrencyCode', '=', 'currency_master.CurrencyCode')
            ->where('location_currency.LocationCode', '=', $locationCode)
            ->select('location_currency.CurrencyCode','location_currency.AvgCost','location_currency.SellRate','location_currency.BuyRate','location_currency.Stock','location_currency.Varience','currency_master.CurrencyName','location_currency.DealStock')
            ->get()->toArray();
        //printArray($data); exit;
        if($data){
            $data[0]->AvgCost = currency_format($data[0]->AvgCost);
            $data[0]->SellRate = currency_format($data[0]->SellRate);
            $data[0]->BuyRate = currency_format($data[0]->BuyRate);
            $data[0]->Stock = currency_format($data[0]->Stock,2);
            $data[0]->Stock = str_replace(',', '', $data[0]->Stock);
            $data[0]->DealStock = currency_format($data[0]->DealStock,2);
            $data[0]->DealStock = str_replace(',', '', $data[0]->DealStock);
            return response()->json(['status' => '200', 'data' => $data]);
        } else {
            return response()->json(['status' => '201']);
        }
    }

    public function getSGDdetails(Request $request)
    {
        $locationCode = Auth::user()->Location;
        //printArray($request->all());
        $request->in_currency = 'SGD';
        $data = DB::table('currency_master')->where('currency_master.CurrencyCode', '=', $request->in_currency)
            ->leftJoin('location_currency', 'location_currency.CurrencyCode', '=', 'currency_master.CurrencyCode')
            ->where('location_currency.LocationCode', '=', $locationCode)
            ->select('location_currency.CurrencyCode','location_currency.AvgCost','location_currency.SellRate','location_currency.BuyRate','location_currency.Stock','location_currency.Varience','currency_master.CurrencyName')
            ->get()->toArray();
        //printArray($data); exit;
        if($data){
            $data[0]->AvgCost = currency_format($data[0]->AvgCost);
            $data[0]->SellRate = currency_format($data[0]->SellRate);
            $data[0]->BuyRate = currency_format($data[0]->BuyRate);
            $data[0]->Stock = currency_format($data[0]->Stock,2);
            return response()->json(['status' => '200', 'data' => $data]);
        } else {
            return response()->json(['status' => '201']);
        }
    }

    public function insertCurrencyOs($currencycode,$locationcode,$transtype,$famount,$lamount,$avgcost,$openstock,$custcode,$cusbalance) 
    {
        $currdate = date('Y-m-d');
        //Insert into buysell table
        if(!containsDecimal($famount)) {
        	$famount = number_format($famount,10);
        	$famount = str_replace(',','',$famount);        
        }
        if(!containsDecimal($lamount)) {
        	$lamount = number_format($lamount,10);
        	$lamount = str_replace(',','',$lamount);        
        }
        if($transtype=='Buy') {
        	$buyfamount = $famount;
        	$buylamount = $lamount;
        	$sellfamount = "0.00";
        	$selllamount = "0.00";
            $creditamount = $lamount;
            $debitamount = "0.00";
        } else {
        	$buyfamount = "0.00";
        	$buylamount = "0.00";
        	$sellfamount = $famount;
        	$selllamount = $lamount;
            $creditamount = "0.00";
            $debitamount = $lamount;
        }
        $currtransdetails = CurrencyTransaction::whereDate('Date',$currdate)->where('CurrencyCode',$currencycode)->get();
        if(count($currtransdetails) > 0) {           	
            $curr = CurrencyTransaction::whereDate('Date',$currdate)
                                 ->where('CurrencyCode',$currencycode)
                                 ->update([
									       'BuyFamount'    => DB::raw('BuyFamount + '.$buyfamount),
									       'BuyLamount'    => DB::raw('BuyLamount + '.$buylamount),
									       'SellFAmount'   => DB::raw('SellFAmount + '.$sellfamount),
									       'SellLAmount'   => DB::raw('SellLAmount + '.$selllamount),
                                           'ModifiedBy'    => Auth::user()->username,
                                           'ModifiedDate'  => now()
									   ]);
        } else {

            $currtransdata =   ["CurrencyCode"     => $currencycode, 
                                "Date"             => now(),
                                "OpeningStock"     => $openstock,
                                "OpeningAvgCost"   => $avgcost,
                                "BuyFamount"       => $buyfamount,
                                "BuyLamount"       => $buylamount,
                                "SellFAmount"      => $sellfamount,
                                "SellLAmount"      => $selllamount,
                                "OpeningDealStock" => "0.00",
                                "CreatedBy" 	   => Auth::user()->username,
                                "CreatedDate" 	   => now(),
                                "ModifiedBy" 	   => Auth::user()->username,
                                "ModifiedDate" 	   => now()
                             ];
            CurrencyTransaction::create($currtransdata);
        }

        //Store in tbl_location_currency_transaction
        $currtransdetails = LocationCurrencyTransaction::whereDate('Date',$currdate)->where('LocationCode',$locationcode)->where('CurrencyCode',$currencycode)->get();
        if(count($currtransdetails) > 0) {           	
            LocationCurrencyTransaction::whereDate('Date',$currdate)
                                 ->where('CurrencyCode',$currencycode)
                                 ->where('LocationCode',$locationcode)
                                 ->update([
									       'BuyFamount' => DB::raw('BuyFamount + '.$buyfamount),
									       'BuyLamount' => DB::raw('BuyLamount + '.$buylamount),
									       'SellFAmount' => DB::raw('SellFAmount + '.$sellfamount),
									       'SellLAmount' => DB::raw('SellLAmount + '.$selllamount),
                                           "ModifiedBy"    => Auth::user()->username,
                                           "ModifiedDate"  => now()
									   ]);

    	} else {
            $loccurrtransdata = ["LocationCode" => $locationcode,
            					"CurrencyCode" => $currencycode,
            					"Date" => now(),
            					"OpeningStock" => $openstock,
            					"OpeningAvgCost" => $avgcost,
            					"BuyFamount" => $buyfamount,
                                "BuyLamount" => $buylamount,
                                "SellFAmount" => $sellfamount,
                                "SellLAmount" => $selllamount,
                                "OpeningDealStock" => "0.00",
                                "CreatedBy" 	   => Auth::user()->username,
                                "CreatedDate" 	   => now(),
                                "ModifiedBy" 	   => Auth::user()->username,
                                "ModifiedDate" 	   => now()
                                ];
            LocationCurrencyTransaction::create($loccurrtransdata);
        }
        //Store in tbl_location_currency_transaction

        //Store in tbl_location_customer_transaction
        
        $custtransdetails = LocationCustomerTransaction::whereDate('Date',$currdate)->where('LocationCode',$locationcode)->where('CustCode',$custcode)->get();     

        if(count($custtransdetails) > 0) {
            LocationCustomerTransaction::whereDate('Date',$currdate)
                                 ->where('CustCode',$custcode)
                                 ->where('LocationCode',$locationcode)
                                 ->update([
                                           'Dbamt' => DB::raw('Dbamt + '.$debitamount),
                                           'Cramt' => DB::raw('Cramt + '.$creditamount),
                                           "ModifiedBy"    => Auth::user()->username,
                                           "ModifiedDate"  => now()
                                       ]);

        } else {
            $openingbalance = str_replace(',', '', $cusbalance);
            //echo $openingbalance; exit;
            if(empty($openingbalance)) {
                $openingbalance = '0.00';
            }
            $loccusttransdata = ["LocationCode" => $locationcode,
                                "CustCode"      => $custcode,
                                "Date"           => now(),
                                "OpeningBalance" => $openingbalance,
                                "Dbamt"          => $debitamount,
                                "Cramt"          => $creditamount,
                                "CreatedBy"      => Auth::user()->username,
                                "CreatedDate"    => now(),
                                "ModifiedBy"     => Auth::user()->username,
                                "ModifiedDate"   => now()
                                ];
            LocationCustomerTransaction::create($loccusttransdata);
        }
        //Store in tbl_location_customer_transaction        
	}
}
