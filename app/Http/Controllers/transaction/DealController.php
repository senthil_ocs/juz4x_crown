<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Config;
use App\Nationality;
use App\SettlementMode;
use App\Customer;
use App\DealTransactionHeader;
use App\Currency;
use App\Country;
use App\Location;
use App\DealTransactionDetail;
use App\LocationCurrency;
use App\LocationCustomer;
use App\CurrencyTransaction;
use App\LocationCurrencyTransaction;
use App\LocationCustomerTransaction;
use DB;
use App\BuySell;
use App\DealTransaction;
use App\BuySellDetails;
use Carbon\Carbon;
use Auth;
use PDF;

class DealController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {   
        $alllocations = Location::get();
        $postData =$request->formdata;
        $data = array();
        if($request->ajax() && isset($postData['location'])){
            $data = DB::table('deal_tran_header');
            $data  = $data->leftJoin('deal_trans_detail', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo');
            $data  = $data->where('deal_tran_header.LocationCode',$postData['location']);
            if(!empty($postData['currency_code'])){
                $data  = $data->where('deal_trans_detail.CurrencyCode',$postData['currency_code']);
            }
            if(!empty($postData['transtype'])){
                $data  = $data->where('deal_trans_detail.TranType',$postData['transtype']);
            }
            if(!empty($postData['cust_code'])){
                $data  = $data->where('deal_tran_header.CustomerCode',$postData['cust_code']);
            }
            if(!empty($request->dealstatus)){
                $data  = $data->whereIn('deal_trans_detail.Status',$request->dealstatus);
            }
            if(!empty($postData['from_date']) && !empty($postData['to_date'])){
                $data  = $data->whereBetween('deal_tran_header.DealDate', [Carbon::parse($postData['from_date'])->startOfDay(), Carbon::parse($postData['to_date'])->endOfDay()]);
            }
            $data  = $data->get();

            return Datatables::of($data)
                ->addIndexColumn()
                    ->editColumn('DealDate', function ($data){
                        $date = \Carbon\Carbon::parse($data->DealDate);
                        return $date->format('d/m/Y');
                    })
                    ->editColumn('ValidTill', function ($data) {
                        $date = \Carbon\Carbon::parse($data->DealDate);
                        return $date->format('d/m/Y');
                    })
                    ->editColumn('FAmount', function ($data) {
                        return !empty($data->FAmount)?currency_format($data->FAmount,2):currency_format(0,2);
                    })
                    ->editColumn('Rate', function ($data) {
                        return !empty($data->Rate)?currency_format($data->Rate):currency_format(0,2);
                    })
                    ->editColumn('Status', function ($data) {
                        return $data->Status;
                    })
                  ->addColumn('Select', function ($data) {
                    if(!empty($data->TranNo)){
                        return '<div class="row"><div class="col-md-3"><label class="checkbox checkbox-primary"><input class="form-control" type="checkbox" name="select[]" value="'.$data->TranNo.'" id="deallistcheckbox"><span class="checkmark"></span></label></div><div class="col-md-3" style="cursor: pointer;"><a class="text-success mr-2 edit_deal" data-ref="'.$data->TranNo.'"><i class="lg i-Pen-4 font-weight-bold" aria-hidden="true"></i></a></div></div>'; 
                    }             
                  })
                  ->addColumn('TranType', function ($data) {
                    if($data->TranType == 'Sell'){
                      return '<span class="badge badge-info">Sell</span>'; 
                    } else{
                       return '<span class="badge badge-success">Buy</span>'; 
                    }              
                  })->escapeColumns([])
                    ->setRowId(function ($data) {
                        return $data->TranNo;
                    })->setRowClass(function () {
                    return 'tableclicked';
                    })->make(true);

        }
        if(env('ENABLE_DEAL') == 1){
            return view('reports.deallistreport',compact('data','alllocations'));
        }else{
            return redirect()->route('home');
        }
    }

    public function deallistexport(Request $request)
    {

        if(isset($request->selectid)){
            $datas = DB::table('deal_trans_detail')->whereIn('TranNo', explode(",",$request->selectid))
                        ->leftJoin('deal_tran_header', 'deal_tran_header.DealNo', '=', 'deal_trans_detail.DealNo')
                        ->get();
        } else {
            $datas = DB::table('deal_tran_header');
            $datas  = $datas->leftJoin('deal_trans_detail', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo');
            $datas  = $datas->where('deal_tran_header.LocationCode',$request->location);
            if(!empty($request->currency_code)){
                $datas  = $datas->where('deal_trans_detail.CurrencyCode',$request->currency_code);
            }
            if(!empty($request->transtype)){
                $datas  = $datas->where('deal_trans_detail.TranType',$request->transtype);
            }
            if(!empty($request->cust_code)){
                $datas  = $datas->where('deal_tran_header.CustomerCode',$request->cust_code);
            }
            if(!empty($request->dealstatus)){
                $datas  = $datas->whereIn('deal_trans_detail.Status',$request->dealstatus);
            }
            if(!empty($request->from_date) && !empty($request->to_date)){
                $datas  = $datas->whereBetween('deal_tran_header.DealDate', [Carbon::parse($request->from_date)->startOfDay(), Carbon::parse($request->to_date)->endOfDay()]);
            }
            $datas  = $datas->get();
        }

        $strFile  = 'deallistreport.pdf';
        $data = [];
        foreach ($datas as $key => $value) {
            $date = \Carbon\Carbon::parse($value->DealDate);
            $fromatdate = $date->format('d/m/Y');
            $data[] =   array(
                'Deal_No' => $value->DealNo,
                'Deal_Date' => $fromatdate,
                'Tran_type' => $value->TranType,
                'Customer' => $value->CustomerCode,
                'Customer_Name' => $value->CustomerName,
                'Curr_Code' => $value->CurrencyCode,
                'FAmount' => currency_format($value->FAmount,2),
                'Rate' => currency_format($value->Rate),
                'Valid_Till' => $fromatdate,
                'Status' => $value->Status,
            );
        }
        $fromdate = isset($request->from_date) ? date("d/m/Y", strtotime($request->from_date)) : '';
        $todate = isset($request->from_date) ? date("d/m/Y", strtotime($request->to_date)) : '';
        /*$fromdate = displayDateformat(\Carbon\Carbon::parse($request->from_date));
        $todate = displayDateformat( \Carbon\Carbon::parse($request->to_date));*/
        $compArray = DB::table('company')->select('Description')->get();
        $compName = $compArray[0]->Description;
        $pdf = PDF::loadView('pdf.ledger.deallistreport',compact('data','fromdate','todate','compName'));
        return $pdf->stream($strFile);
    }

    public function statusupdate(Request $request)
    {
        if($request->all()){
            if($request->type == 'close'){
                DealTransactionDetail::whereIn('TranNo', $request->id)->update(['Status'=>'Closed']);
                $request->session()->flash('message.level', 'success');
                $request->session()->flash('message.content', 'Deal Transaction status update successfully!'); 
                return response()->json(['status' => '200']);                
            } else if( $request->type == 'cancel' ) {
                DealTransactionDetail::whereIn('TranNo', $request->id)->update(['Status'=>'Cancelled']);
                $request->session()->flash('message.level', 'success');
                $request->session()->flash('message.content', 'Deal Transaction status update successfully!'); 
                return response()->json(['status' => '200']);
            }
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Deal Transaction status update failed '); 
            return response()->json(['status' => '201']);
        }
    }

    public function edit(Request $request,$id)
    { 
        $data = DB::table('deal_tran_header');
            $data  = $data->leftJoin('deal_trans_detail', 'deal_trans_detail.DealNo', '=', 'deal_tran_header.DealNo')
                        ->where('deal_trans_detail.TranNo',$id)->first();
            if($data){
                $data->FAmount = number_format($data->FAmount,2);
                $data->LAmount = number_format($data->LAmount,2);
                $data->BalanceAmount = number_format($data->BalanceAmount,2);
                
                $data->ValidTill = displayCorrectDateFormat($data->ValidTill);
                $Currency = Currency::select('CurrencyName')->where('CurrencyCode',$data->CurrencyCode)->first();
                $data->CurrencyName = $Currency->CurrencyName;

            }
        return response()->json(['status' => '200', 'data' => $data]);
    }
    public function update(Request $request)
    { 
      $request->edit_famount  = str_replace(',', '', $request->edit_famount);
      $request->edit_famount  = (float)$request->edit_famount;
      $update = DealTransactionDetail::where('TranNo',$request->edit_id)
                            ->update(['ValidTill'=>convertDateformat($request->edit_validtilldate),
                                      'Remarks'=>$request->edit_remark,
                                      'FAmount'=>$request->edit_famount]);
        if($update){            
            return response()->json(['status' => '200', 'data' =>'Transaction update successfully!']);
        }else{
            return response()->json(['status' => '201', 'data' => 'Transaction update failed']);
        }
        return redirect()->route('deallist');
    }
    
    public function view(Request $request)
    {   
        $maxTransNo = 1;
        $data = DealTransactionDetail::select('TranNo')->orderBy('TranNo', 'DESC')->first();
        if($data != ''){
            $maxTransNo = $data->TranNo+1;
        }
        $vid = str_pad($maxTransNo, env('DEAL_VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
        $identifire = Config::get('app.identifire_type');
        
        $country = Country::select('Country')->orderBy('Country', 'ASC')->get();
    	$nationality = Nationality::select('Code','Description')->orderBy('Code')->get();
    	$settlementmode = SettlementMode::orderBy('SettlementCode')->get();

    	if(env('ENABLE_DEAL') == 1){
            return view('transaction.deal', compact('nationality','country','settlementmode','identifire','vid'));
        } else {
            return redirect()->route('home');
        }
    }
    
    public function voucherno(Request $request)
    {
        $maxTransNo = 1;
        $data = DealTransactionDetail::select('TranNo')->orderBy('TranNo', 'DESC')->first();
        if($data != ''){
            $maxTransNo = $data->TranNo+1;
        }
        $vid = str_pad($maxTransNo, env('DEAL_VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
        return response()->json(['status' => '200', 'data' => $vid]);
    }

    public function getcustomerdetails(Request $request)
    {
    	$loccode = Auth::user()->Location;

    	$customerlocationdetails = LocationCustomer::where('Custcode',$request->cusid)->where('Location',$loccode)->pluck('DealBalance')->all();
        $cusbalance = !empty($customerlocationdetails[0]) ? $customerlocationdetails[0] : '0.00';

        $data = Customer::where('Custcode', $request->cusid)->first();
        if($data){
            $data['customerbalance'] = currency_format($cusbalance,2);
            return response()->json(['status' => '200', 'data' => $data]);
        } else {
            return response()->json(['status' => '201']);
        }
    }

    public function getDealNo()
    {
        $maxDealNo = 1;
        $data = DealTransactionHeader::select('DealNo')->orderBy('id', 'DESC')->first();
        if($data != ''){
        	$data->DealNo = str_replace('DL', '', $data->DealNo);
            $maxDealNo = $data->DealNo+1;
        }
        $dealno = str_pad($maxDealNo, env('DEAL_NO_PREFIX'), '0', STR_PAD_LEFT);
        return $dealno;
    }
    public function releasedeal(Request $request)
    {
    	
    	//echo count($request['chosendeals']); exit;
    	$chosendeals = $request['chosendeals'];
    	if(isset($request['chosendeals']) && count($request['chosendeals']) > 0) {
    		$dealdetails = DealTransactionDetail::whereIn('TranNo',$chosendeals)->get(); 
            foreach ($dealdetails as $value) {
                $LAmount =0; 
                $FAmount =0; 
                $LAmount =  DealTransaction::where('DealTranNo',$value->TranNo)->sum('LAmount');
                $FAmount =  DealTransaction::where('DealTranNo',$value->TranNo)->sum('FAmount');
                $value->LAmount =  number_format($value->LAmount-$LAmount,2);
               $value->FAmount =  number_format($value->FAmount-$FAmount,2);
               
            }  
    		return response()->json(['status' => '200', 'data' => $dealdetails->toArray()]);
    	} else {
    		return response()->json(['status' => '201']);
    	}
    }
    public function getDealDetail(Request $request)
    {
        
        //echo count($request['chosendeals']); exit;
        $chosendeals = $request['dealid'];
        if(isset($request['dealid']) && !empty($request['dealid']) > 0) {
            $dealdetails = DealTransactionDetail::where('TranNo',$chosendeals)->first();   
            $FAmount =  DealTransaction::where('DealTranNo',$chosendeals)->sum('FAmount');
            $dealdetails->FAmount = $dealdetails->FAmount- $FAmount;
            return response()->json(['status' => '200', 'data' => $dealdetails]);
        } else {
            return response()->json(['status' => '201']);
        }
    }
    
    public function insert_update(Request $request)
    {
        $rout  = \Route::getFacadeRoot()->current()->uri();
        postLog($rout,"Deal Details",json_encode($request->all()));
        DB::beginTransaction();
        //printArray($request->all()); exit;
        //if(!isset($request->cusbalance)) {
            //$request->cusbalance = 0.00;
        //}
        //Insert into tbl_deal_tran_header table
        $dealno = $this->getDealNo();
        $dealno = 'DL'.$dealno;
        try {
            if($request->customer_code!='' && (isset($request->td_lamount) && count($request->td_lamount) > 0)) {                
                $data = ["DealNo" 			=> $dealno,
                         "DealDate" 		=> now(),
                         "NRICNo" 			=> isset($request->pno_nric) ? $request->pno_nric : '',
                         "CustomerName" 	=> $request->customer_name,                         
                         "CustomerCode" 	=> $request->customer_code,
                         "ContactName"      => $request->contacts,                         
                         "LocationCode" 	=> $request->location,                         
                         "TerminalCode" 	=> "",
                         "CreatedBy" 		=> Auth::user()->username,
                         "CreatedDate" 		=> now(),
                         "ModifiedBy" 		=> Auth::user()->username,
                         "ModifiedDate" 	=> now(),
                         ];
                    DealTransactionHeader::create($data);
                    //Insert into tbl_deal_tran_details table
                    for($d=0;$d<count($request->td_lamount);$d++) {

                        $td_famount = str_replace(',', '', $request->td_famount[$d]);
                        $td_famount = (float)$td_famount;

                        $td_lamount = str_replace(',', '', $request->td_lamount[$d]);
                        $td_lamount = (float)$td_lamount;

                       	$currcode = $request->td_currency[$d];
                        $loccode = Auth::user()->Location;                        

                        $detailsdata = ["DealNo"            => $dealno,
                                        "TranType" 			=> $request->td_type[$d],
                                        "CurrencyCode" 		=> $request->td_currency[$d],
                                        "Rate" 				=> $request->td_rate[$d],
                                        "FAmount" 			=> $td_famount,
                                        "LAmount" 			=> $td_lamount,
                                        "ValidTill"         => Carbon::parse($request->td_validtill[$d]),
                                        "Remarks" 			=> $request->td_remarks[$d],
                                        "Status"            => "Open",
                                        "RealisedAmount"    => 0.00,
                                        "BalanceAmount"     => $td_famount,
                                        "CreatedBy" 		=> Auth::user()->username,
                                        "CreatedDate" 		=> now(),
                                        "ModifiedBy" 		=> Auth::user()->username,
                                        "ModifiedDate" 		=> now()
                                       ];                 
                        $insertdealdetails = DealTransactionDetail::create($detailsdata);
                        $dealdetailid = $insertdealdetails->id;
                        //Update in tbl_location_currency table                        
                        if($request->td_type[$d]=='Buy') {                            
                            $buyamount = $td_famount;
                            $buyrate = $request->td_rate[$d];                          

                            LocationCurrency::where('CurrencyCode', $currcode)
                                              ->where('LocationCode', $loccode)
                                              ->increment('DealStock', $td_famount);
                            LocationCustomer::where('Custcode',$request->customer_code)->where('Location',$loccode)->decrement('DealBalance', $td_lamount);
                        } else {
                        	LocationCurrency::where('CurrencyCode', $request->td_currency[$d])
                                              ->where('LocationCode', $loccode)
                                              ->decrement('DealStock', $td_famount);
                            LocationCustomer::where('Custcode',$request->customer_code)->where('Location',$loccode)->increment('DealBalance', $td_lamount);
                        }  
                        
                    }
                    //Insert into buysell_details table
                    // $request->session()->flash('message.level', 'success');
                    // $request->session()->flash('message.content', 'Transaction inserted successfully!');    
                    $status = '200';
                    $msg = 'Deal inserted successfully!';
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            $status = '201';
            $msg = $e->getMessage();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
        } catch(\Exception $e)
        {
            DB::rollback();
            $status = '201';
            $msg = $e->getMessage();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
        }
        
        DB::commit();
        // return redirect('buysell');
        return response()->json(['status' => $status, 'content' => $msg]);
    }

    public function getincurrencydetails(Request $request)
    {
        $locationCode = Auth::user()->Location;
        //printArray($request->all());
        $data = DB::table('currency_master')->where('currency_master.CurrencyCode', '=', $request->in_currency)
            ->leftJoin('location_currency', 'location_currency.CurrencyCode', '=', 'currency_master.CurrencyCode')
            ->where('location_currency.LocationCode', '=', $locationCode)
            ->select('location_currency.CurrencyCode','location_currency.AvgCost','location_currency.SellRate','location_currency.BuyRate','location_currency.Stock','location_currency.Varience','currency_master.CurrencyName','location_currency.DealStock')
            ->get()->toArray();
        //printArray($data); exit;
        if($data){
            $data[0]->AvgCost = currency_format($data[0]->AvgCost);
            $data[0]->SellRate = currency_format($data[0]->SellRate);
            $data[0]->BuyRate = currency_format($data[0]->BuyRate);
            $data[0]->Stock = currency_format($data[0]->Stock,2);
            $data[0]->Stock = str_replace(',', '', $data[0]->Stock);
            $data[0]->DealStock = currency_format($data[0]->DealStock,2);
            $data[0]->DealStock = str_replace(',', '', $data[0]->DealStock);
            return response()->json(['status' => '200', 'data' => $data]);
        } else {
            return response()->json(['status' => '201']);
        }
    }

    public function getSGDdetails(Request $request)
    {
        $locationCode = Auth::user()->Location;
        //printArray($request->all());
        $request->in_currency = 'SGD';
        $data = DB::table('currency_master')->where('currency_master.CurrencyCode', '=', $request->in_currency)
            ->leftJoin('location_currency', 'location_currency.CurrencyCode', '=', 'currency_master.CurrencyCode')
            ->where('location_currency.LocationCode', '=', $locationCode)
            ->select('location_currency.CurrencyCode','location_currency.AvgCost','location_currency.SellRate','location_currency.BuyRate','location_currency.Stock','location_currency.Varience','currency_master.CurrencyName','location_currency.DealStock')
            ->get()->toArray();
        //printArray($data); exit;
        if($data){
            $data[0]->AvgCost = currency_format($data[0]->AvgCost);
            $data[0]->SellRate = currency_format($data[0]->SellRate);
            $data[0]->BuyRate = currency_format($data[0]->BuyRate);
            $data[0]->Stock = currency_format($data[0]->Stock,2);
            $data[0]->Stock = str_replace(',', '', $data[0]->Stock);
            $data[0]->DealStock = currency_format($data[0]->DealStock,2);
            $data[0]->DealStock = str_replace(',', '', $data[0]->DealStock);
            return response()->json(['status' => '200', 'data' => $data]);
        } else {
            return response()->json(['status' => '201']);
        }
    }

    public function dealinsertupdate(Request $request)
    {
        $rout  = \Route::getFacadeRoot()->current()->uri();
        postLog($rout,"BuySell Details",json_encode($request->all()));
        DB::beginTransaction();
        //if(!isset($request->cusbalance)) {
            //$request->cusbalance = 0.00;
        //}
        //Insert into buysell table
        try {
            //Update Deal as closed
            if(isset($request->chosendealstr) && !empty($request->chosendealstr)) {
                $dealids = explode(',',$request->chosendealstr);
                if(count($dealids) > 0) {
                    foreach($dealids as $dealid) {
                        //DealTransactionDetail::where('id',$dealid)->update(["Status" => "0"]);
                        $dealtrandetails = DealTransactionDetail::where('TranNo',$dealid)->first();
                        /*  Deal transaction status */
                        $d  = array_search($dealid,$request->in_deal);
                        $FAmount =0;
                        $FAmount = DealTransaction::where('DealTranNo',$dealtrandetails->TranNo)->sum('FAmount');

                        $td_famount = str_replace(',', '', $request->td_famount[$d]);
                        $td_famount = (float)$td_famount;
                        
                        $td_lamount = str_replace(',', '', $request->td_lamount[$d]);
                        $td_lamount = (float)$td_lamount;

                        if($dealtrandetails->FAmount == $FAmount + $td_famount){
                            $dealstatus = 'Completed';
                        }else{
                            $dealstatus = 'Partial';
                        }

                        $deallistdata = ["DealTranNo"       => $dealtrandetails->TranNo,
                                        "TranType"          => $request->td_type[$d],
                                        "CurrencyCode"      => $request->td_currency[$d],
                                        "Rate"              => $request->td_rate[$d],
                                        "FAmount"           => $td_famount,
                                        "LAmount"           => $td_lamount,
                                        "TranAmount"        => $td_lamount,
                                        "Mode"              => $request->mode_payment,
                                        "ReferenceNo"       => $request->voucher_no,
                                        "CreatedBy"         => Auth::user()->username,
                                        "CreatedDate"       => now(),
                                        "ModifiedBy"        => Auth::user()->username,
                                        "ModifiedDate"      => now()
                                       ]; 

                        DealTransaction::create($deallistdata);
                        
                        $FAmount  = DealTransaction::where('DealTranNo',$dealid)->sum('FAmount');
                        DealTransactionDetail::where('TranNo',$dealid)
                                    ->update(['Status'=>$dealstatus,
                                        'RealisedAmount'=>$FAmount,
                                        'BalanceAmount' => ($dealtrandetails->FAmount-$FAmount)
                                     ]);

                    }
                }
            }
            //Update Deal as closed
            if($request->customer_code!='' && (isset($request->td_lamount) && count($request->td_lamount) > 0)) {                
                $data = ["TranNo"           => $request->voucher_no,
                         "TranDate"         => now(),
                         "DocType"          => isset($request->major) ? $request->major : '',
                         "RefNo"            => isset($request->minor) ? $request->minor : '',
                         "PPNo"             => isset($request->pno_nric) ? $request->pno_nric : '',
                         "Nationality"      => $request->nationality,
                         "Name"             => "",
                         "CustName"         => $request->customer_name,
                         "Address1"         => $request->address,
                         "Address2"         => $request->address1,
                         "Address3"         => "",
                         "Address4"         => "",
                         "Address5"         => "",
                         "CustCode"         => $request->customer_code,
                         "LocationCode"     => $request->location,
                         "Remarks"          => $request->remarks,
                         "TTRefNo"          => 0,
                         "Deleted"          => "",
                         "DOB"              => !empty($request->dob) ? convertDateTimeformat($request->dob) : '',
                         "MCType"           => isset($request->mc_type) ? $request->mc_type : '',
                         "TerminalName"     => "",
                         "CreatedBy"        => Auth::user()->username,
                         "CreatedDate"      => now(),
                         "ModifiedBy"       => Auth::user()->username,
                         "ModifiedDate"     => now(),
                         "SettlementMode"   => "",
                         "IdentifierType"   => $request->identifier_type,
                         "SuspiciousTran"   => 0,
                         "PhoneNo"          => $request->phone_no,
                         ];
                        BuySell::create($data);



                    //Insert into busell_details table
                    for($d=0;$d<count($request->td_lamount);$d++) {

                        $td_famount = str_replace(',', '', $request->td_famount[$d]);
                        $td_famount = (float)$td_famount;

                        $td_lamount = str_replace(',', '', $request->td_lamount[$d]);
                        $td_lamount = (float)$td_lamount;

                        $currcode = $request->td_currency[$d];
                        $loccode = Auth::user()->Location;

                        $loccurrencydetails = LocationCurrency::where('CurrencyCode', $currcode)->where('LocationCode', $loccode)->first();
                        $lastavgcost = '0.00'; //isset($loccurrencydetails->AvgCost) ? $loccurrencydetails->AvgCost : '0.00';

                        $detailsdata = ["TranNo"            => $request->voucher_no,
                                        "Line"              => $d+1,
                                        "TranType"          => $request->td_type[$d],
                                        "CurrencyCode"      => $request->td_currency[$d],
                                        "Rate"              => $request->td_rate[$d],
                                        "FAmount"           => $td_famount,
                                        "LAmount"           => $td_lamount,
                                        "TTRefN0"           => 0,
                                        //"LastAverageCost"   => $lastavgcost,
                                        "CreatedBy"         => Auth::user()->username,
                                        "CreatedDate"       => now(),
                                        "ModifiedBy"        => Auth::user()->username,
                                        "ModifiedDate"      => now()
                                       ];                 
                        $insertbuselldetails = BuySellDetails::create($detailsdata);
                        $buyselldetailid = $insertbuselldetails->id;

                        //Get Stock from Location Currency based
                        $locstock = getLocationCurrencyDealStock($loccode,$currcode);
                        //Get Stock from Location Currency based
                        
                        //Update in tbl_location_currency table                        
                        if($request->td_type[$d]=='Buy') {                            
                            $buyamount = $td_famount;
                            $buyrate = $request->td_rate[$d];                          

                            if(env('AVG_METHOD')=='FIFO') {
                                $avgcost = DB::select("CALL GetAvgCostLocation('".$currcode."','".$loccode."','".$buyamount."','".$buyrate."')"); //execute stored procedure
                            } else {
                                $avgcost = DB::select("CALL GetAvgCostLocation('".$currcode."','".$loccode."','".$buyamount."','".$buyrate."')"); //execute stored procedure
                            }                            
                            $avgcost = $avgcost[0]->agvCost;
                            postLog($rout,"Average Cost from Stored Procedure",$avgcost); // write average cost to log file
                            LocationCurrency::where('CurrencyCode', $currcode)
                                              ->where('LocationCode', $loccode)
                                              ->increment('DealStock', $td_famount, ['AvgCost' => $avgcost]);
                            LocationCustomer::where('Custcode',$request->customer_code)->where('Location',$loccode)->decrement('DealBalance', $td_lamount);
                        } else {
                            $avgcost = $request->td_rate[$d];
                            LocationCurrency::where('CurrencyCode', $request->td_currency[$d])
                                              ->where('LocationCode', $loccode)
                                              ->decrement('DealStock', $td_famount);
                            LocationCustomer::where('Custcode',$request->customer_code)->where('Location',$loccode)->increment('DealBalance', $td_lamount);
                        }                       

                        if($buyselldetailid > 0) {
                            
                            if(empty($avgcost)) {
                                $avgcost = '0.00';
                            }
                            //echo $currcode.'--1-'.$loccode.'--2-'.$request->td_type[$d].'--3--'.$request->td_famount[$d].'--4-'.$request->td_lamount[$d].'--5--'.$avgcost.'--6-'.$locstock.'--7-'.$request->customer_code.'--8-'.$request->cusbalance; exit;
                            $this->insertDealCurrencyOs($currcode,$loccode,$request->td_type[$d],$td_famount,$td_lamount,$avgcost,$locstock,$request->customer_code,$request->cusbalance);
                        }                 
                        //Update in tbl_location_currency table
                    }
                    //Insert into buysell_details table
                    // $request->session()->flash('message.level', 'success');
                    // $request->session()->flash('message.content', 'Transaction inserted successfully!');    
                    $status = '200';
                    $msg = 'Transaction inserted successfully!';
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            $status = '201';
            $msg = $e->getMessage();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
        } catch(\Exception $e)
        {
            DB::rollback();
            $status = '201';
            $msg = $e->getMessage();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
        }
        
        DB::commit();
        // return redirect('buysell');
        return response()->json(['status' => $status, 'content' => $msg]);
    }

    public function insertDealCurrencyOs($currencycode,$locationcode,$transtype,$famount,$lamount,$avgcost,$openstock,$custcode,$cusbalance) 
    {
        $currdate = date('Y-m-d');
        //Insert into buysell table
        if(!containsDecimal($famount)) {
        	$famount = number_format($famount,10);
        	$famount = str_replace(',','',$famount);        
        }
        if(!containsDecimal($lamount)) {
        	$lamount = number_format($lamount,10);
        	$lamount = str_replace(',','',$lamount);        
        }
        if($transtype=='Buy') {
        	$buyfamount = $famount;
        	$buylamount = $lamount;
        	$sellfamount = "0.00";
        	$selllamount = "0.00";
            $creditamount = $lamount;
            $debitamount = "0.00";
        } else {
        	$buyfamount = "0.00";
        	$buylamount = "0.00";
        	$sellfamount = $famount;
        	$selllamount = $lamount;
            $creditamount = "0.00";
            $debitamount = $lamount;
        }
        $currtransdetails = CurrencyTransaction::whereDate('Date',$currdate)->where('CurrencyCode',$currencycode)->get();
        if(count($currtransdetails) > 0) {           	
            $curr = CurrencyTransaction::whereDate('Date',$currdate)
                                 ->where('CurrencyCode',$currencycode)
                                 ->update([
									       'BuyFamount'    => DB::raw('BuyFamount + '.$buyfamount),
									       'BuyLamount'    => DB::raw('BuyLamount + '.$buylamount),
									       'SellFAmount'   => DB::raw('SellFAmount + '.$sellfamount),
									       'SellLAmount'   => DB::raw('SellLAmount + '.$selllamount),
                                           'ModifiedBy'    => Auth::user()->username,
                                           'ModifiedDate'  => now()
									   ]);
        } else {

            $currtransdata =   ["CurrencyCode"     => $currencycode, 
                                "Date"             => now(),
                                //"OpeningStock"     => $openstock,
                                //"OpeningAvgCost"   => $avgcost,
                                "BuyFamount"       => $buyfamount,
                                "BuyLamount"       => $buylamount,
                                "SellFAmount"      => $sellfamount,
                                "SellLAmount"      => $selllamount,
                                "OpeningDealStock" => $openstock,
                                "CreatedBy" 	   => Auth::user()->username,
                                "CreatedDate" 	   => now(),
                                "ModifiedBy" 	   => Auth::user()->username,
                                "ModifiedDate" 	   => now()
                             ];
            CurrencyTransaction::create($currtransdata);
        }

        //Store in tbl_location_currency_transaction
        $currtransdetails = LocationCurrencyTransaction::whereDate('Date',$currdate)->where('LocationCode',$locationcode)->where('CurrencyCode',$currencycode)->get();
        if(count($currtransdetails) > 0) {           	
            LocationCurrencyTransaction::whereDate('Date',$currdate)
                                 ->where('CurrencyCode',$currencycode)
                                 ->where('LocationCode',$locationcode)
                                 ->update([
									       'BuyFamount'    => DB::raw('BuyFamount + '.$buyfamount),
									       'BuyLamount'    => DB::raw('BuyLamount + '.$buylamount),
									       'SellFAmount'   => DB::raw('SellFAmount + '.$sellfamount),
									       'SellLAmount'   => DB::raw('SellLAmount + '.$selllamount),
                                           "ModifiedBy"    => Auth::user()->username,
                                           "ModifiedDate"  => now()
									   ]);

    	} else {
            $loccurrtransdata = ["LocationCode"    => $locationcode,
            					"CurrencyCode"     => $currencycode,
            					"Date"             => now(),
            					//"OpeningStock" => $openstock,
            					//"OpeningAvgCost" => $avgcost,
            					"BuyFamount"       => $buyfamount,
                                "BuyLamount"       => $buylamount,
                                "SellFAmount"      => $sellfamount,
                                "SellLAmount"      => $selllamount,
                                "OpeningDealStock" => $openstock,
                                "CreatedBy" 	   => Auth::user()->username,
                                "CreatedDate" 	   => now(),
                                "ModifiedBy" 	   => Auth::user()->username,
                                "ModifiedDate" 	   => now()
                                ];
            LocationCurrencyTransaction::create($loccurrtransdata);
        }
        //Store in tbl_location_currency_transaction

        //Store in tbl_location_customer_transaction
        
        $custtransdetails = LocationCustomerTransaction::whereDate('Date',$currdate)->where('LocationCode',$locationcode)->where('CustCode',$custcode)->get();     

        if(count($custtransdetails) > 0) {
            LocationCustomerTransaction::whereDate('Date',$currdate)
                                 ->where('CustCode',$custcode)
                                 ->where('LocationCode',$locationcode)
                                 ->update([
                                           'Dbamt'        => DB::raw('Dbamt + '.$debitamount),
                                           'Cramt'        => DB::raw('Cramt + '.$creditamount),
                                           "ModifiedBy"   => Auth::user()->username,
                                           "ModifiedDate" => now()
                                       ]);

        } else {
            $openingbalance = str_replace(',', '', $cusbalance);
            //echo $openingbalance; exit;
            if(empty($openingbalance)) {
                $openingbalance = '0.00';
            }
            $loccusttransdata = ["LocationCode"      => $locationcode,
                                "CustCode"           => $custcode,
                                "Date"               => now(),
                                "OpeningDealBalance" => $openingbalance,
                                "Dbamt"              => $debitamount,
                                "Cramt"              => $creditamount,
                                "CreatedBy"          => Auth::user()->username,
                                "CreatedDate"        => now(),
                                "ModifiedBy"         => Auth::user()->username,
                                "ModifiedDate"       => now()
                                ];
            LocationCustomerTransaction::create($loccusttransdata);
        }
        //Store in tbl_location_customer_transaction        
	}
}
