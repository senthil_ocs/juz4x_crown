<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Nationality;
use App\DDtransaction;
use App\DDtransactionunpost;
use App\Purpose;
use App\Beneficiary;
use App\BenificieryDetails;
use App\Currency;
use App\Country;
use App\CustomerType;
use App\Customer;
use App\SettlementMode;
use App\BuySell;
use App\BuySellDetails;
use App\LocationCurrency;
use App\LocationCustomer;
use App\CurrencyTransaction;
use App\LocationCurrencyTransaction;
use App\LocationCustomerTransaction;
use Config;
use Validator;
use DataTables;
use DB;
use Auth;
use Carbon\Carbon;


class DDtransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	if(isset($_REQUEST['dd_id'])){
        	$dd_id =$_REQUEST['dd_id'];
        }else{
        	$dd_id = '';
        }

        $nationality = Nationality::select('Code','Description')->orderBy('Code')->get();
        $country = Country::select('Country', 'CountryGroup')->orderBy('Country', 'ASC')->get();
        $customertype = CustomerType::get();
        $purpose  = Purpose::where('Status', 1)->get();
        $identifire  = Config::get('app.identifire_type');
		/*$Tranid = DDtransaction::max('id');
		$TranNo = (int)$Tranid+1;
		$dis_TranNo = str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);*/
		$TranNodb = DB::table('company')->select('NextTTTranNo')->get();
		$dis_TranNo = $TranNodb[0]->NextTTTranNo;
		$payMode = SettlementMode::select('SettlementCode')->orderBy('SettlementCode')->get();
		/*$payMode = collect([
            'CASH',
            'NETS',
            'CHEQUE',
            'BANK TRANSFER',
            'OTHERS']);*/
        if(env('ENABLE_DD_TT') == 1){
        	return view('transaction.ddtransaction.ddtransaction', compact('nationality','identifire','dis_TranNo','purpose','country','customertype','beneficiary','payMode','dd_id'));
        }else{
            return redirect()->route('home');
        }
    }

	public function getagentcode(Request $request)
	{
		$locationCode = Auth::user()->Location;
		$active = '1';
        $data = DB::table('location_customer')
            ->leftJoin('customer_master', 'location_customer.CustCode', '=', 'customer_master.CustCode')
            ->where('customer_master.Custcode', $request->code)
            ->whereNotIn('customer_master.CustCode', [$request->ccode])
            ->where('location_customer.Location', $locationCode)
            ->where('customer_master.Agent', $active)
            ->first();
		if($data){
			return response()->json(['status' => '200', 'data'=>$data]);
		} else {
			return response()->json(['status' => '201']);
		}
	}

    public function AptDDtranscation(Request $request)
    {
        $nationality = Nationality::select('Code')->orderBy('Code')->get();
        $country = Country::select('Country')->orderBy('Country', 'ASC')->get();
        $customertype = CustomerType::get();
        $purpose  = Purpose::where('Status', 1)->get();
        $identifire  = Config::get('app.identifire_type');
		$Tranid = DDtransaction::max('id');
		$TranNo = (int)$Tranid+1;
		$dis_TranNo = str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);
        if(env('ENABLE_DD_TT') == 1){
        	return view('transaction.ddtransaction.AptDDtranscation', compact('nationality','identifire','dis_TranNo','purpose','country','customertype'));
        }else{
            return redirect()->route('home');
        }
    }

    public function insert(Request $request)
    {
    	if($request->amount){
    		$request->amount = str_replace(',', '', $request->amount);
    		$request->amount = (float)$request->amount;
    	}
    	if($request->c_exrate){
    		$request->c_exrate = str_replace(',', '', $request->c_exrate);
    		$request->c_exrate = (float)$request->c_exrate;
    	}
    	if($request->c_tot){
    		$request->c_tot = str_replace(',', '', $request->c_tot);
    		$request->c_tot = (float)$request->c_tot;
    	}
    	if($request->a_exrate){
    		$request->a_exrate = str_replace(',', '', $request->a_exrate);
    		$request->a_exrate = (float)$request->a_exrate;
    	}
    	if($request->a_tot){
    		$request->a_tot = str_replace(',', '', $request->a_tot);
    		$request->a_tot = (float)$request->a_tot;
    	}
        try {
        	if($request->ref_no != ""){
        		$ben = DDtransactionunpost::where('TranNo',  $request->ref_no)->update([
					'PaidStatus' => "Posted",
				]);
        	}
			$ben = BenificieryDetails::where('CustCode',  $request->customer_code)->where('CustNRICNO',  $request->pno_nric)->where('BeneName',$request->pp_name)->first();
			if($ben){
				$benid = $ben->id;
				$ben = BenificieryDetails::where('CustCode',  $request->customer_code)->where('CustNRICNO',  $request->pno_nric)->where('BeneName',$request->pp_name)->update([
					'BeneName' => $request->pp_name,
					'BeneAddress1' => $request->pp_address1,
					'BeneAddress2' => $request->pp_address2,
					'BeneMobileNo' => $request->pp_telephone,
					'BeneCountry' => $request->pp_country,
					'BeneBankName' => $request->bank,
					'BeneBankBranch' => $request->branch_address,
					'BeneBankAccNo' => $request->ac_no,
					'SwiftCode' => $request->swift_code,
				]);
			} else {
				$maxid = BenificieryDetails::max('id');
				$beneficiary = new BenificieryDetails;
				$beneficiary->CustCode  =   $request->customer_code;
				$beneficiary->CustNRICNO = $request->pno_nric;
				$beneficiary->beneficiary_id  =  'B'.str_pad($maxid+1, 6, '0', STR_PAD_LEFT);
				$beneficiary->BeneName  =  $request->pp_name;
				$beneficiary->BeneAddress1  =  $request->pp_address1;
				$beneficiary->BeneAddress2  =  $request->pp_address2;
				$beneficiary->BeneMobileNo  =  $request->pp_telephone;
				$beneficiary->BeneCountry  =  $request->pp_country;
				$beneficiary->BeneBankName  =  $request->bank;
				$beneficiary->BeneBankBranch  =  $request->branch_address;
				$beneficiary->BeneBankAccNo  =  $request->ac_no;
				$beneficiary->SwiftCode  =  $request->swift_code;
				$beneficiary->save();
				$benid = $beneficiary->id;
			}
        	/*$Tranid = DDtransaction::max('id');
        	$TranNo = (int)$Tranid+1;*/
    		$TranNodb = DB::table('company')->select('NextTTTranNo')->get();
    		$TranNo = $TranNodb[0]->NextTTTranNo;

			$ddtransaction = new DDtransaction;
			/*$ddtransaction->TranNo  =  str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);*/
			$ddtransaction->TranNo  =  $TranNo;
			$ddtransaction->DocType = 'TT';
			$ddtransaction->TranType = 'S';
			$ddtransaction->CustTTType = $request->MCType;
			$ddtransaction->CompName = $request->CompName;
			$ddtransaction->AgentTTType = $request->AgentTTType;
			$ddtransaction->AgentPayStatus = '1';
			/*$ddtransaction->DDNo = str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);*/
			$ddtransaction->DDNo = $TranNo;
			$ddtransaction->Other = 0;
			$ddtransaction->Service = 0;
			$ddtransaction->TranDate = now();
			$ddtransaction->TransmissionDate = now();
			$ddtransaction->PaymentDate = now();
			$ddtransaction->PaidStatus = 'N';

			$ddtransaction->BeneId  = $benid;
			$ddtransaction->CustCode  =  $request->customer_code;
			$ddtransaction->CustName  =  strtoupper($request->customer_name);
			$ddtransaction->CustPPNo  =  $request->pno_nric;
			$ddtransaction->DOB  =  $request->dob;
			$ddtransaction->CustNationality  =  $request->nationality;
			$ddtransaction->Purpose  =  $request->Purpose;
			$ddtransaction->CustTTType  =  $request->customer_type;
			$ddtransaction->CompAddress1  =  $request->address1;
			$ddtransaction->CompAddress2  =  $request->address2;
			$ddtransaction->CompAddress3  =  $request->country;
			$ddtransaction->Postal =  $request->postal_code; 
			$ddtransaction->Telephone  =  $request->phone_no;
			$ddtransaction->CurrencyCode  =  $request->code;
			$ddtransaction->ExchRate  =  $request->ex_rate;
			$ddtransaction->FAmount  =  $request->amount;
			$ddtransaction->LAmount  =  $request->c_exrate;
			$ddtransaction->Comm  = $request->c_comm;
			$ddtransaction->TotalAmount = $request->c_tot;
			$ddtransaction->BenPPNo  =  $request->pp_nric;
			$ddtransaction->BenName  =  $request->pp_name;
			$ddtransaction->BenAddress1  =  $request->pp_address1;
			$ddtransaction->BenAddress2  =   $request->pp_address2;
			$ddtransaction->BenAddress3  =  $request->pp_country;
			$ddtransaction->BeneMobileNo  =  $request->pp_telephone;
			$ddtransaction->BenBank  =  $request->bank;
			$ddtransaction->BenAccNo  =  $request->ac_no;
			$ddtransaction->BenBankBranch  =  $request->branch_address;
			$ddtransaction->AgentCode = $request->agentcode;
			$ddtransaction->AgentRate = $request->agentrate;
			$ddtransaction->AgentCommission = $request->a_comm;
			$ddtransaction->CreatedDate = now();
			$ddtransaction->CreatedBy = Auth::user()->username;
			$ddtransaction->SwiftCode  =  $request->swift_code;
			$ddtransaction->AgentExRate  =  $request->a_exrate;
			$ddtransaction->AgentTotal  =  $request->a_tot;
			$ddtransaction->AgentName  =  strtoupper($request->agentname);
			$ddtransaction->status  =  3;
			$ddtransaction->orginator  =  $request->pp_orginator;
			$ddtransaction->orginatorId  =  $request->pp_orginatorId;
			$ddtransaction->payment_details_bank  =  $request->paymentDetail;
			$ddtransaction->statement_ref  =  $request->statementRef;
			$ddtransaction->additional_notes  =  $request->addNotes;
			$ddtransaction->Payment_Remarks  =  $request->paymentRemark;
			$ddtransaction->RemarkCollectPaymentCash  =  $request->cashRemark;
			$ddtransaction->CollectPayment  =  $request->collectPaymentType;
			$ddtransaction->PaidStatus  = 'Y';
			$ddtransaction->PaymentDate  = now();
			$ddtransaction->PayMode  = $request->payMode;
			$ddtransaction->PaidBy  = Auth::user()->username;
			$ddtransaction->save();

			$Trimid = ltrim($TranNo, '0');
			$TrimNo = (int)$Trimid+1;
			$NextTranNo = str_pad($TrimNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);

			$comp = DB::table('company')->update([
    			'NextTTTranNo' => $NextTranNo,
			]);

			// buy_sell table entry here 
		    // insert for customer
		    $maxTransNo = 1;		
	        if($request->pno_nric!=''){
	      	   $data = BuySell::select('TranNo')->orderBy('id', 'DESC')->first();
		        if($data != ''){
		            $maxTransNo = $data->TranNo+1;
		        }
	         	$data = ["TranNo" 			=> $maxTransNo,
                         "TranDate" 		=> now(),
                         "PPNo" 			=> isset($request->pno_nric) ? $request->pno_nric : '',
                         "Nationality" 		=> $request->nationality,
                         "Name" 			=> "",
                         "CustName" 		=> strtoupper($request->customer_name),
                         "Address1" 		=> $request->address1,
                         "Address2" 		=> $request->address2,
                         "Address3" 		=> $request->address3,
                         "Address4" 		=> "",
                         "Address5" 		=> "",
                         "CustCode" 		=> $request->customer_code,
                         "LocationCode" 	=> Auth::user()->Location,
                         "Deleted" 			=> "",
                         "DOB" 				=> !empty($request->dob) ? convertDateTimeformat($request->dob) : '1990-01-01 00:00:00',
                         "TerminalName" 	=> "",
                         "CreatedBy" 		=> Auth::user()->username,
                         "CreatedDate" 		=> now(),
                         "ModifiedBy" 		=> Auth::user()->username,
                         "ModifiedDate" 	=> now(),
                         "SettlementMode" 	=> "",
                         "SuspiciousTran" 	=> 0,
                         "PhoneNo" 			=> $request->phone_no,
                         ];
                        BuySell::create($data);
                        $Customerdetailsdata = ["TranNo" 			=> $maxTransNo,
                                        "Line" 				=> '1',
                                        "TranType" 			=> 'Buy',
                                        "CurrencyCode" 		=> $request->code,
                                        "Rate" 				=> $request->ex_rate,
                                        "FAmount" 			=> $request->amount,
                                        //"LAmount" 			=> $request->c_tot,
                                        "LAmount" 			=> $request->c_exrate,
                                        "TTRefNo" 			=> 0,
                                        "LastAverageCost" 	=> '',
                                        "CreatedBy" 		=> Auth::user()->username,
                                        "CreatedDate" 		=> now(),
                                        "ModifiedBy" 		=> Auth::user()->username,
                                        "ModifiedDate" 		=> now()
                                       ]; 
                        BuySellDetails::create($Customerdetailsdata);
                        if($request->c_comm>0){
                        	$CustomerCommdetailsdata = ["TranNo" 			=> $maxTransNo,
                                        "Line" 				=> '2',
                                        "TranType" 			=> 'Buy',
                                        "CurrencyCode" 		=> 'SGD',
                                        "Rate" 				=> '1',
                                        "FAmount" 			=> $request->c_comm,
                                        //"LAmount" 			=> $request->c_tot,
                                        "LAmount" 			=> $request->c_comm,
                                        "TTRefNo" 			=> 0,
                                        "LastAverageCost" 	=> '',
                                        "CreatedBy" 		=> Auth::user()->username,
                                        "CreatedDate" 		=> now(),
                                        "ModifiedBy" 		=> Auth::user()->username,
                                        "ModifiedDate" 		=> now()
                                       ]; 
                        BuySellDetails::create($CustomerCommdetailsdata);
                        }

	                    	$buyamount = $request->amount;
	                        $buyrate   = $request->ex_rate;
	                       	$currcode  = $request->code;
	                        $loccode   = Auth::user()->Location;                          

	                        if(env('AVG_METHOD')=='FIFO') {
	                            $avgcost = DB::select("CALL GetAvgCostLocation('".$currcode."','".$loccode."','".$buyamount."','".$buyrate."')"); //execute stored procedure
	                        } else {
	                            $avgcost = DB::select("CALL GetAvgCostLocation('".$currcode."','".$loccode."','".$buyamount."','".$buyrate."')"); //execute stored procedure
	                        }                            
	                        $avgcost = $avgcost[0]->agvCost;
	                        LocationCurrency::where('CurrencyCode', $currcode)
	                                          ->where('LocationCode', $loccode)
	                                          ->increment('Stock', $request->amount, ['AvgCost' => $avgcost]);
	                        LocationCustomer::where('Custcode',$request->customer_code)->where('Location',$loccode)->decrement('Balance', str_replace(',', '', $request->c_tot));
	                        //Get Stock from Location Currency based
                        	$locstock = getLocationCurrencyStock($loccode,$currcode);
                        	$request->cusbalance = 0;
	                        $this->insertCurrencyOs($currcode,$loccode,'Buy',$request->amount,$request->c_tot,$avgcost,$locstock,$request->customer_code,$request->cusbalance);

	        }
	        // insert for Agent		
	        if($request->agentcode!=''){
	      	   $data = BuySell::select('TranNo')->orderBy('id', 'DESC')->first();
		        if($data != ''){
		            $maxTransNo = $data->TranNo+1;
		        }
	         	$Cdata = ["TranNo" 			=> $maxTransNo,
                         "TranDate" 		=> now(),
                         "PPNo" 			=> "",
                         "Nationality" 		=> "",
                         "Name" 			=> "",
                         "CustName" 		=> strtoupper($request->agentname),
                         "Address1" 		=> "",
                         "Address2" 		=> "",
                         "Address3" 		=> "",
                         "Address4" 		=> "",
                         "Address5" 		=> "",
                         "CustCode" 		=> $request->agentcode,
                         "LocationCode" 	=> Auth::user()->Location,
                         "Deleted" 			=> "",
                         "DOB" 				=> "",
                         "TerminalName" 	=> "",
                         "CreatedBy" 		=> Auth::user()->username,
                         "CreatedDate" 		=> now(),
                         "ModifiedBy" 		=> Auth::user()->username,
                         "ModifiedDate" 	=> now(),
                         "SettlementMode" 	=> "",
                         "SuspiciousTran" 	=> 0,
                         "PhoneNo" 			=> "",
                         ];
                        BuySell::create($Cdata);
                        $AgentDetailsdata = ["TranNo" 			=> $maxTransNo,
                                        "Line" 				=> '1',
                                        "TranType" 			=> 'Sell',
                                        "CurrencyCode" 		=> $request->code,
                                        "Rate" 				=> $request->agentrate,
                                        "FAmount" 			=> $request->amount,
                                        //"LAmount" 			=> $request->a_tot,
                                        "LAmount" 			=> $request->a_exrate,
                                        "TTRefNo" 			=> 0,
                                        "LastAverageCost" 	=> '',
                                        "CreatedBy" 		=> Auth::user()->username,
                                        "CreatedDate" 		=> now(),
                                        "ModifiedBy" 		=> Auth::user()->username,
                                        "ModifiedDate" 		=> now()
                                       ]; 
                        BuySellDetails::create($AgentDetailsdata);
                        if($request->a_comm>0){
                        	$AgentCommDetailsdata = ["TranNo" 			=> $maxTransNo,
                                        "Line" 				=> '2',
                                        "TranType" 			=> 'Sell',
                                        "CurrencyCode" 		=> 'SGD',
                                        "Rate" 				=> '1',
                                        "FAmount" 			=> $request->a_comm,
                                        //"LAmount" 			=> $request->a_tot,
                                        "LAmount" 			=> $request->a_comm,
                                        "TTRefNo" 			=> 0,
                                        "LastAverageCost" 	=> '',
                                        "CreatedBy" 		=> Auth::user()->username,
                                        "CreatedDate" 		=> now(),
                                        "ModifiedBy" 		=> Auth::user()->username,
                                        "ModifiedDate" 		=> now()
                                       ]; 
                        	BuySellDetails::create($AgentCommDetailsdata);
                        }
                        $avgcost = $request->agentrate;

	                        $currcode  = $request->code;
	                        $loccode   = Auth::user()->Location;

                            LocationCurrency::where('CurrencyCode', $currcode)
                                              ->where('LocationCode', $loccode)
                                              ->decrement('Stock', $request->amount);
                            LocationCustomer::where('Custcode',$request->agentcode)->where('Location',$loccode)->increment('Balance', $request->a_tot);
                            $locstock = getLocationCurrencyStock($loccode,$currcode);
                        	$request->cusbalance = 0;
	                        $this->insertCurrencyOs($currcode,$loccode,'Sell',$request->amount,$request->c_tot,$avgcost,$locstock,$request->agentcode,$request->cusbalance);
	        }


			return response()->json(['status' => '200', 'content'=>'DD Transaction Details inserted successfully!']);
        } catch (Exception $e) {
        	return response()->json(['status' => '201', 'content'=>$e]);
        }
    }

    public function insertCurrencyOs($currencycode,$locationcode,$transtype,$famount,$lamount,$avgcost,$openstock,$custcode,$cusbalance) 
    {
        $currdate = date('Y-m-d');
        //Insert into buysell table
        if(!containsDecimal($famount)) {
        	$famount = number_format($famount,10);
        	$famount = str_replace(',','',$famount);        
        }
        if(!containsDecimal($lamount)) {
        	$lamount = number_format($lamount,10);
        	$lamount = str_replace(',','',$lamount);        
        }
        if($transtype=='Buy') {
        	$buyfamount = $famount;
        	$buylamount = $lamount;
        	$sellfamount = "0.00";
        	$selllamount = "0.00";
            $creditamount = $lamount;
            $debitamount = "0.00";
        } else {
        	$buyfamount = "0.00";
        	$buylamount = "0.00";
        	$sellfamount = $famount;
        	$selllamount = $lamount;
            $creditamount = "0.00";
            $debitamount = $lamount;
        }
        $currtransdetails = CurrencyTransaction::whereDate('Date',$currdate)->where('CurrencyCode',$currencycode)->get();
        if(count($currtransdetails) > 0) {           	
            $curr = CurrencyTransaction::whereDate('Date',$currdate)
                                 ->where('CurrencyCode',$currencycode)
                                 ->update([
									       'BuyFamount'    => DB::raw('BuyFamount + '.$buyfamount),
									       'BuyLamount'    => DB::raw('BuyLamount + '.$buylamount),
									       'SellFAmount'   => DB::raw('SellFAmount + '.$sellfamount),
									       'SellLAmount'   => DB::raw('SellLAmount + '.$selllamount),
                                           'ModifiedBy'    => Auth::user()->username,
                                           'ModifiedDate'  => now()
									   ]);
        } else {

            $currtransdata =   ["CurrencyCode"     => $currencycode, 
                                "Date"             => now(),
                                "OpeningStock"     => $openstock,
                                "OpeningAvgCost"   => $avgcost,
                                "BuyFamount"       => $buyfamount,
                                "BuyLamount"       => $buylamount,
                                "SellFAmount"      => $sellfamount,
                                "SellLAmount"      => $selllamount,
                                "OpeningDealStock" => "0.00",
                                "CreatedBy" 	   => Auth::user()->username,
                                "CreatedDate" 	   => now(),
                                "ModifiedBy" 	   => Auth::user()->username,
                                "ModifiedDate" 	   => now()
                             ];
            CurrencyTransaction::create($currtransdata);
        }

        //Store in tbl_location_currency_transaction
        $currtransdetails = LocationCurrencyTransaction::whereDate('Date',$currdate)->where('LocationCode',$locationcode)->where('CurrencyCode',$currencycode)->get();
        if(count($currtransdetails) > 0) {           	
            LocationCurrencyTransaction::whereDate('Date',$currdate)
                                 ->where('CurrencyCode',$currencycode)
                                 ->where('LocationCode',$locationcode)
                                 ->update([
									       'BuyFamount' => DB::raw('BuyFamount + '.$buyfamount),
									       'BuyLamount' => DB::raw('BuyLamount + '.$buylamount),
									       'SellFAmount' => DB::raw('SellFAmount + '.$sellfamount),
									       'SellLAmount' => DB::raw('SellLAmount + '.$selllamount),
                                           "ModifiedBy"    => Auth::user()->username,
                                           "ModifiedDate"  => now()
									   ]);

    	} else {
            $loccurrtransdata = ["LocationCode" => $locationcode,
            					"CurrencyCode" => $currencycode,
            					"Date" => now(),
            					"OpeningStock" => $openstock,
            					"OpeningAvgCost" => $avgcost,
            					"BuyFamount" => $buyfamount,
                                "BuyLamount" => $buylamount,
                                "SellFAmount" => $sellfamount,
                                "SellLAmount" => $selllamount,
                                "OpeningDealStock" => "0.00",
                                "CreatedBy" 	   => Auth::user()->username,
                                "CreatedDate" 	   => now(),
                                "ModifiedBy" 	   => Auth::user()->username,
                                "ModifiedDate" 	   => now()
                                ];
            LocationCurrencyTransaction::create($loccurrtransdata);
        }
        //Store in tbl_location_currency_transaction

        //Store in tbl_location_customer_transaction
        
        $custtransdetails = LocationCustomerTransaction::whereDate('Date',$currdate)->where('LocationCode',$locationcode)->where('CustCode',$custcode)->get();     

        if(count($custtransdetails) > 0) {
            LocationCustomerTransaction::whereDate('Date',$currdate)
                                 ->where('CustCode',$custcode)
                                 ->where('LocationCode',$locationcode)
                                 ->update([
                                           'Dbamt' => DB::raw('Dbamt + '.$debitamount),
                                           'Cramt' => DB::raw('Cramt + '.$creditamount),
                                           "ModifiedBy"    => Auth::user()->username,
                                           "ModifiedDate"  => now()
                                       ]);

        } else {
            $openingbalance = str_replace(',', '', $cusbalance);
            //echo $openingbalance; exit;
            if(empty($openingbalance)) {
                $openingbalance = '0.00';
            }
            $loccusttransdata = ["LocationCode" => $locationcode,
                                "CustCode"      => $custcode,
                                "Date"           => now(),
                                "OpeningBalance" => $openingbalance,
                                "Dbamt"          => $debitamount,
                                "Cramt"          => $creditamount,
                                "CreatedBy"      => Auth::user()->username,
                                "CreatedDate"    => now(),
                                "ModifiedBy"     => Auth::user()->username,
                                "ModifiedDate"   => now()
                                ];
            LocationCustomerTransaction::create($loccusttransdata);
        }
        //Store in tbl_location_customer_transaction        
	}

    public function update(Request $request)
    {
    	if($request->amount){
    		$request->amount = str_replace(',', '', $request->amount);
    		$request->amount = (float)$request->amount;
    	}
    	if($request->c_exrate){
    		$request->c_exrate = str_replace(',', '', $request->c_exrate);
    		$request->c_exrate = (float)$request->c_exrate;
    	}
    	if($request->c_tot){
    		$request->c_tot = str_replace(',', '', $request->c_tot);
    		$request->c_tot = (float)$request->c_tot;
    	}
    	if($request->a_exrate){
    		$request->a_exrate = str_replace(',', '', $request->a_exrate);
    		$request->a_exrate = (float)$request->a_exrate;
    	}
    	if($request->a_tot){
    		$request->a_tot = str_replace(',', '', $request->a_tot);
    		$request->a_tot = (float)$request->a_tot;
    	}
    	try {

    		$updateTranNo = DDtransaction::select('TranNo')->where('id', $request->ref_no)->get();
    		$updateTranNo = $updateTranNo[0]->TranNo;
    		if(is_null($updateTranNo) || $updateTranNo == ''){
    			$TranNodb = DB::table('company')->select('NextTTTranNo')->get();
    			$TranNo = $TranNodb[0]->NextTTTranNo;
    			/*$status = 3;*/
    		} else {
    			$TranNo = $updateTranNo;
    			/*$status = 2;*/
    		}

			$ben = BenificieryDetails::where('CustCode',  $request->customer_code)->where('CustNRICNO',  $request->pno_nric)->where('BeneName',$request->pp_name)->first();
			if($ben){
				$benid = $ben->id;
				BenificieryDetails::where('CustCode',  $request->customer_code)->where('CustNRICNO',  $request->pno_nric)->where('BeneName',$request->pp_name)->update([
					'BeneName' => $request->pp_name,
					'BeneAddress1' => $request->pp_address1,
					'BeneAddress2' => $request->pp_address2,
					'BeneMobileNo' => $request->pp_telephone,
					'BeneCountry' => $request->pp_country,
					'BeneBankName' => $request->bank,
					'BeneBankBranch' => $request->branch_address,
					'BeneBankAccNo' => $request->ac_no,
					'SwiftCode' => $request->swift_code,
				]);
			} else {
				$maxid = BenificieryDetails::max('id');
				$beneficiary = new BenificieryDetails;
				$beneficiary->CustCode  =   $request->customer_code;
				$beneficiary->CustNRICNO = $request->pno_nric;
				$beneficiary->beneficiary_id  =  'B'.str_pad($maxid+1, 6, '0', STR_PAD_LEFT);
				$beneficiary->BeneName  =  $request->pp_name;
				$beneficiary->BeneAddress1  =  $request->pp_address1;
				$beneficiary->BeneAddress2  =  $request->pp_address2;
				$beneficiary->BeneMobileNo  =  $request->pp_telephone;
				$beneficiary->BeneCountry  =  $request->pp_country;
				$beneficiary->BeneBankName  =  $request->bank;
				$beneficiary->BeneBankBranch  =  $request->branch_address;
				$beneficiary->BeneBankAccNo  =  $request->ac_no;
				$beneficiary->SwiftCode  =  $request->swift_code;
				$beneficiary->save();
				$benid = $beneficiary->id;
			}

			/*$TranNo  =  str_pad($request->ref_no, env('DD_PREFIX'), '0', STR_PAD_LEFT);*/

			DDtransaction::where('id', $request->ref_no)->update([

				'TranNo' => $TranNo,
				'BeneId'  => $benid,
				'CustCode'  =>  $request->customer_code,
				'CustName'  =>  strtoupper($request->customer_name),

				'CustTTType' => $request->MCType,
				'CompName' => $request->CompName,
				'AgentTTType' => $request->AgentTTType,
				'ModifiedBy' => Auth::user()->name,

				'CustPPNo'  =>  $request->pno_nric,
				'DOB'  =>  $request->dob,
				'CustNationality'  =>  $request->nationality,
				'Purpose'  =>  $request->Purpose,
				'CustTTType'  =>  $request->customer_type,
				'CompAddress1'  =>  $request->address1,
				'CompAddress2'  =>  $request->address2,
				'CompAddress3'  =>  $request->country,
				'Postal' =>  $request->postal_code,
				'Telephone'  =>  $request->phone_no,
				'CurrencyCode'  =>  $request->code,
				'ExchRate'  =>  $request->ex_rate,
				'FAmount'  =>  $request->amount,
				'LAmount'  =>  $request->c_exrate,
				'Comm'  => $request->c_comm,
				'TotalAmount' => $request->c_tot,
				'BenPPNo'  =>  $request->pp_nric,
				'BenName'  =>  $request->pp_name,
				'BenAddress1'  =>  $request->pp_address1,
				'BenAddress2'  =>   $request->pp_address2,
				'BenAddress3'  =>  $request->pp_country,
				'BeneMobileNo'  =>  $request->pp_telephone,
				'BenBank'  =>  $request->bank,
				'BenAccNo'  =>  $request->ac_no,
				'BenBankBranch'  =>  $request->branch_address,
				'SwiftCode'  =>  $request->swift_code,
				'AgentCode' => $request->agentcode,
				'AgentRate' => $request->agentrate,
				'AgentCommission' => $request->a_comm,
				'AgentExRate' => $request->a_exrate,
				'AgentTotal' => $request->a_tot,
				'AgentName' => strtoupper($request->agentname),
				'status' => 3,
				'orginator' => $request->pp_orginator,
				'orginatorId' => $request->pp_orginatorId,
				'payment_details_bank'  =>  $request->paymentDetail,
				'statement_ref'  =>  $request->statementRef,
				'additional_notes'  =>  $request->addNotes,
				'Payment_Remarks'  =>  $request->paymentRemark,
				'RemarkCollectPaymentCash'  =>  $request->cashRemark,
				'PayMode'  => $request->payMode,
				'ModifiedDate' => now(),
				'ModifiedBy' => Auth::user()->username,
			]);

    		if(is_null($updateTranNo) || $updateTranNo == ''){
				$Trimid = ltrim($TranNo, '0');
				$TrimNo = (int)$Trimid+1;
				$NextTranNo = str_pad($TrimNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);

				$comp = DB::table('company')->update([
	    			'NextTTTranNo' => $NextTranNo,
				]);
			}

    		return response()->json(['status' => '200', 'content'=>'DD Transaction Details Update successfully!']);
    	} catch (Exception $e) {
    		return response()->json(['status' => '201', 'content'=>'DD Transaction Details Not Update successfully!']);
    	}
    }


    public function getddtransaction(Request $request)
    {
    	$data = DDtransaction::where('id', $request->val)->where('status',1)->first();
    	if($data) {
	    	$data['FAmount'] = number_format($data['FAmount'],2);
	    	$data['LAmount'] = number_format($data['LAmount'],2);
	    	$data['TotalAmount'] = number_format($data['TotalAmount'],2);
    		return response()->json(['status' => '200', 'data'=>$data]);
    	} else {
    		return response()->json(['status' => '201']);
    	}
    }

    public function getDDTransactionNric(Request $request)
    {
    	$data = DDtransaction::where('CustPPNo', $request->id)->first();
    	if($data) {
    		return response()->json(['status' => '200', 'data'=>$data]);
    	} else {
    		return response()->json(['status' => '201']);
    	}
    }

    public function getbeneficiary(Request $request)
    {
    	/*->where('beneficiary_id', $request->pp_nric)->orWhere*/
    	$name = $request->pp_name ? $request->pp_name : '';
    	$data = BenificieryDetails::where('CustCode',$request->customer_code)->where('CustNRICNO', $request->pp_nric)->where('BeneName',$name)->first();
    	if($data) {
    		return response()->json(['status' => '200', 'data'=>$data]);
    	} else {
    		return response()->json(['status' => '201']);
    	}
    }

    public function getRefNo(Request $request)
    {
        $data = DDtransaction::select('TranNo')->max('id');
        $TranNo = (int)$data+1;
        $vid = str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);
        return response()->json(['status' => '200', 'data' => $vid]);
    }

    public function getunpostRefNo(Request $request)
    {
        $data = DDtransactionunpost::select('TranNo')->max('id');
        $TranNo = (int)$data+1;
        $vid = str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);
        return response()->json(['status' => '200', 'data' => $vid]);
    }

	public function getCurrency(Request $request)
    {
        /*$data = Currency::where('CurrencyCode', $request->id)->first();*/
        $data = DB::table('currency_master')
                ->leftJoin('location_currency', 'currency_master.CurrencyCode', '=', 'location_currency.CurrencyCode')
                ->select('currency_master.CurrencyName','location_currency.AvgCost','location_currency.Varience')
                ->where('currency_master.CurrencyCode', $request->id)
                ->where('location_currency.LocationCode', '=', Auth::user()->Location)
                ->first();
        if($data){
        	return response()->json(['status' => '200', 'data' => $data]);
        } else {
        	return response()->json(['status' => '201']);
        }
    }    


	/* DD/tt unpost transaction  */
	public function unpost()
    {
        if(isset($_REQUEST['unpost_id'])){
        	$unpost_id =$_REQUEST['unpost_id'];
        }else{
        	$unpost_id = '';
        }

        $nationality = Nationality::select('Code','Description')->orderBy('Code')->get();
        $country = Country::select('Country', 'CountryGroup')->orderBy('Country', 'ASC')->get();
        $customertype = CustomerType::select('Name')->get();
        $purpose  = Purpose::select('Name')->where('Status', 1)->get();
        $identifire  = Config::get('app.identifire_type');
		$Tranid = DDtransactionunpost::max('id');
		$TranNo = (int)$Tranid+1;
		$dis_TranNo = str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);
        if(env('ENABLE_DD_TT') == 1){
        	return view('transaction.ddtransaction.ddtransactionunpost', compact('nationality','identifire','dis_TranNo', 'purpose','country','customertype','unpost_id'));

        }else{
            return redirect()->route('home');
        }
    }

    public function unpost_insert(Request $request)
    {
    	if($request->amount){
    		$request->amount = str_replace(',', '', $request->amount);
    		$request->amount = (float)$request->amount;
    	}
    	if($request->c_exrate){
    		$request->c_exrate = str_replace(',', '', $request->c_exrate);
    		$request->c_exrate = (float)$request->c_exrate;
    	}
    	if($request->c_tot){
    		$request->c_tot = str_replace(',', '', $request->c_tot);
    		$request->c_tot = (float)$request->c_tot;
    	}
        try {
			$ben = BenificieryDetails::where('CustCode',  $request->customer_code)->where('CustNRICNO',  $request->pno_nric)->where('BeneName',$request->pp_name)->first();
			if($ben){
				$benid = $ben->id;
				$ben = BenificieryDetails::where('CustCode',  $request->customer_code)->where('CustNRICNO',  $request->pno_nric)->where('BeneName',$request->pp_name)->update([
					'BeneName' => $request->pp_name,
					'BeneAddress1' => $request->pp_address1,
					'BeneAddress2' => $request->pp_address2,
					'BeneCountry' => $request->pp_country,
					'BeneMobileNo' => $request->pp_telephone,
					'BeneBankName' => $request->bank,
					'BeneBankBranch' => $request->branch_address,
					'BeneBankAccNo' => $request->ac_no,
					'SwiftCode' => $request->swift_code,
				]);
			} else {
				$maxid = BenificieryDetails::max('id');
				$beneficiary = new BenificieryDetails;
				$beneficiary->CustCode  =   $request->customer_code;
				$beneficiary->CustNRICNO = $request->pno_nric;
				$beneficiary->beneficiary_id  =  'B'.str_pad($maxid+1, 6, '0', STR_PAD_LEFT);
				$beneficiary->BeneName  =  $request->pp_name;
				$beneficiary->BeneAddress1  =  $request->pp_address1;
				$beneficiary->BeneAddress2  =  $request->pp_address2;
				$beneficiary->BeneCountry  =  $request->pp_country;
				$beneficiary->BeneMobileNo  =  $request->pp_telephone;
				$beneficiary->BeneBankName  =  $request->bank;
				$beneficiary->BeneBankBranch  =  $request->branch_address;
				$beneficiary->BeneBankAccNo  =  $request->ac_no;
				$beneficiary->SwiftCode  =  $request->swift_code;
				$beneficiary->save();
				$benid = $beneficiary->id;
			}
        	/*$Tranid = DDtransactionunpost::max('id');
        	$TranNo = (int)$Tranid+1;*/

			$ddtransaction = new DDtransaction;
			/*$ddtransaction->TranNo  =  str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);*/

			$ddtransaction->DocType = 'TT';
			$ddtransaction->TranType = 'S';
			$ddtransaction->CustTTType = $request->MCType;
			$ddtransaction->CompName = $request->CompName;
			$ddtransaction->AgentTTType = $request->AgentTTType;
			$ddtransaction->AgentPayStatus = '1';
			/*$ddtransaction->DDNo = str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);*/
			$ddtransaction->Other = 0;
			$ddtransaction->Service = 0;
			$ddtransaction->TranDate = now();
			$ddtransaction->TransmissionDate = now();
			$ddtransaction->PaymentDate = now();
			$ddtransaction->PaidStatus = 'N';

			$ddtransaction->BeneId  = $benid;
			$ddtransaction->CustCode  =  $request->customer_code;
			$ddtransaction->CustName  =  $request->customer_name;
			$ddtransaction->CustPPNo  =  $request->pno_nric;
			$ddtransaction->DOB  =  $request->dob;
			$ddtransaction->CustNationality  =  $request->nationality;
			$ddtransaction->Purpose  =  $request->Purpose;
			$ddtransaction->CustTTType  =  $request->customer_type;
			$ddtransaction->CompAddress1  =  $request->address1;
			$ddtransaction->CompAddress2  =  $request->address2;
			$ddtransaction->CompAddress3  =  $request->country;
			$ddtransaction->Postal =  $request->postal_code; 
			$ddtransaction->Telephone  =  $request->phone_no;
			$ddtransaction->CurrencyCode  =  $request->code;
			$ddtransaction->ExchRate  =  $request->ex_rate;
			$ddtransaction->FAmount  =  $request->amount;
			$ddtransaction->LAmount  =  $request->c_exrate;
			$ddtransaction->Comm  = $request->c_comm;
			$ddtransaction->TotalAmount = $request->c_tot;
			$ddtransaction->BenPPNo  =  $request->pp_nric;
			$ddtransaction->BenName  =  $request->pp_name;
			$ddtransaction->BenAddress1  =  $request->pp_address1;
			$ddtransaction->BenAddress2  =   $request->pp_address2;
			$ddtransaction->BenAddress3  =  $request->pp_country;
			$ddtransaction->BeneMobileNo  =  $request->pp_telephone;
			$ddtransaction->BenBank  =  $request->bank;
			$ddtransaction->BenAccNo  =  $request->ac_no;
			$ddtransaction->BenBankBranch  =  $request->branch_address;
			$ddtransaction->AgentCode = $request->agentcode;
			$ddtransaction->AgentRate = $request->agentrate;
			$ddtransaction->AgentCommission = $request->a_comm;
			$ddtransaction->CreatedDate = now();
			$ddtransaction->CreatedBy = Auth::user()->username;
			$ddtransaction->SwiftCode  =  $request->swift_code;
			$ddtransaction->status  =  1;
			$ddtransaction->CollectPayment  =  $request->collectPaymentType;
			$ddtransaction->save();
			return response()->json(['status' => '200', 'content'=>'DD Transaction Details inserted successfully!']);
        } catch (Exception $e) {
        	return response()->json(['status' => '201', 'content'=>$e]);
        }
    
    } 

	public function unpost_update(Request $request)
    {
    	if($request->amount){
    		$request->amount = str_replace(',', '', $request->amount);
    		$request->amount = (float)$request->amount;
    	}
    	if($request->c_exrate){
    		$request->c_exrate = str_replace(',', '', $request->c_exrate);
    		$request->c_exrate = (float)$request->c_exrate;
    	}
    	if($request->c_tot){
    		$request->c_tot = str_replace(',', '', $request->c_tot);
    		$request->c_tot = (float)$request->c_tot;
    	}
    	try {
			$ben = BenificieryDetails::where('CustCode',  $request->customer_code)->where('CustNRICNO',  $request->pno_nric)->where('BeneName',$request->pp_name)->first();
			if($ben){
				$benid = $ben->id;
				BenificieryDetails::where('CustCode',  $request->customer_code)->where('CustNRICNO',  $request->pno_nric)->where('BeneName',$request->pp_name)->update([
					'BeneName' => $request->pp_name,
					'BeneAddress1' => $request->pp_address1,
					'BeneAddress2' => $request->pp_address2,
					'BeneCountry' => $request->pp_country,
					'BeneMobileNo' => $request->pp_telephone,
					'BeneBankName' => $request->bank,
					'BeneBankBranch' => $request->branch_address,
					'BeneBankAccNo' => $request->ac_no,
					'SwiftCode' => $request->swift_code, 
				]);
			} else {
				$maxid = BenificieryDetails::max('id');
				$beneficiary = new BenificieryDetails;
				$beneficiary->CustCode  =   $request->customer_code;
				$beneficiary->CustNRICNO = $request->pno_nric;
				$beneficiary->beneficiary_id  = 'B'.str_pad($maxid+1, 6, '0', STR_PAD_LEFT);
				$beneficiary->BeneName  =  $request->pp_name;
				$beneficiary->BeneAddress1  =  $request->pp_address1;
				$beneficiary->BeneAddress2  =  $request->pp_address2;
				$beneficiary->BeneCountry  =  $request->pp_country;
				$beneficiary->BeneMobileNo  =  $request->pp_telephone;
				$beneficiary->BeneBankName  =  $request->bank;
				$beneficiary->BeneBankBranch  =  $request->branch_address;
				$beneficiary->BeneBankAccNo  =  $request->ac_no;
				$beneficiary->SwiftCode  =  $request->swift_code;
				$beneficiary->save();
				$benid = $beneficiary->id;
			}
			DDtransaction::where('id', $request->ref_no)->update([

				'BeneId'  => $benid,
				'CustCode'  =>  $request->customer_code,
				'CustName'  =>  $request->customer_name,

				'CustTTType' => $request->MCType,
				'CompName' => $request->CompName,
				'AgentTTType' => $request->AgentTTType,
				'ModifiedBy' => Auth::user()->name,

				'CustPPNo'  =>  $request->pno_nric,
				'DOB'  =>  $request->dob,
				'CustNationality'  =>  $request->nationality,
				'Purpose'  =>  $request->Purpose,
				'CustTTType'  =>  $request->customer_type,
				'CompAddress1'  =>  $request->address1,
				'CompAddress2'  =>  $request->address2,
				'CompAddress3'  =>  $request->country,
				'Postal' =>  $request->postal_code,
				'Telephone'  =>  $request->phone_no,
				'CurrencyCode'  =>  $request->code,
				'ExchRate'  =>  $request->ex_rate,
				'FAmount'  =>  $request->amount,
				'LAmount'  =>  $request->c_exrate,
				'Comm'  => $request->c_comm,
				'TotalAmount' => $request->c_tot,
				'BenPPNo'  =>  $request->pp_nric,
				'BenName'  =>  $request->pp_name,
				'BenAddress1'  =>  $request->pp_address1,
				'BenAddress2'  =>   $request->pp_address2,
				'BenAddress3'  =>  $request->pp_country,
				'BeneMobileNo'  =>  $request->pp_telephone,
				'BenBank'  =>  $request->bank,
				'BenAccNo'  =>  $request->ac_no,
				'BenBankBranch'  =>  $request->branch_address,
				'SwiftCode'  =>  $request->swift_code,
				'AgentCode' => $request->agentcode,
				'AgentRate' => $request->agentrate,
				'AgentCommission' => $request->a_comm,
				'status' => 1,
				'ModifiedDate' => now(),
				'ModifiedBy' => Auth::user()->username,
			]);
    		return response()->json(['status' => '200', 'content'=>'DD Transaction Details Update successfully!']);
    	} catch (Exception $e) {
    		return response()->json(['status' => '201', 'content'=>'DD Transaction Details Not Update successfully!']);
    	}

    } 

    public function getddtransactionpost(Request $request)
    {
    	$data = DDtransaction::where('TranNo', $request->val)->first();
    	if($data) {
	    	$data['FAmount'] = number_format($data['FAmount'],2);
	    	$data['LAmount'] = number_format($data['LAmount'],2);
	    	$data['TotalAmount'] = number_format($data['TotalAmount'],2);
	    	$data['AgentExRate'] = number_format($data['AgentExRate'],2);
	    	$data['AgentTotal'] = number_format($data['AgentTotal'],2);
    		return response()->json(['status' => '200', 'data'=>$data]);
    	} else {
    		return response()->json(['status' => '201']);
    	}
    }



    /*    DD/TT search */
    public function searchlist(Request $request)
    {
    	$beneficiary = BenificieryDetails::select('id','BeneName')->orderBy('BeneName')->get();	
        if($request->ajax()){
           $data  = DDtransaction::where('TranNo','!=',NULL)->where('status','!=',1);
            if(!empty($request->cust_code)){
                $data  = $data->where('CustCode',$request->cust_code);
            }
            if(!empty($request->CustNRICNO)){
                $data  = $data->where('CustPPNo',$request->CustNRICNO);
            }
            if(!empty($request->tranno)){
                $data  = $data->where('TranNo',$request->tranno);
            }
           	if(!empty($request->currency_code)){
                $data  = $data->where('CurrencyCode',$request->currency_code);
            }

            if(!empty($request->bank_code)){
                $data  = $data->where('BenPPNo',$request->bank_code);
            }
	        if(!empty($request->bank_name)){
	            $data  = $data->where('BenBank',$request->bank_name);
	        }
            if(!empty($request->from_date) && !empty($request->to_date)){
            	$from_date = date("d-m-Y", strtotime(str_replace('/', '-', $request->from_date)));
            	$to_date = date("d-m-Y", strtotime(str_replace('/', '-', $request->to_date)));
                $data  = $data->whereBetween('CreatedDate', [Carbon::parse($from_date)->startOfDay(), Carbon::parse($to_date)->endOfDay()]);
            }
	        if(!empty($request->swiftcode)){
	            $data  = $data->where('SwiftCode',$request->swiftcode);
	        }
	        if(!empty($request->acno)){
	            $data  = $data->where('BenAccNo',$request->acno);
	        }
	        if(!empty($request->benname)){
	            $data  = $data->where('BenName',$request->benname);
	        }
            $data  = $data->get();

            return Datatables::of($data)
                ->addIndexColumn()
                    ->editColumn('TranDate', function ($data){
                    	if(!empty($data->TranDate)){
                       		$date = \Carbon\Carbon::parse($data->TranDate);
                            return $date->format('d/m/Y');
                    	}else{
                    		return '';
                    	}
                    })
                    ->editColumn('TranNo', function ($data) {
                        return '<a href="'.route("ddtransaction").'?dd_id='.$data->TranNo.'" target="_blank">'.$data->TranNo.'</a>';
                    })
                    ->editColumn('LAmount', function ($data) { 
                        return !empty($data->LAmount)?currency_format($data->LAmount,2):currency_format(0,2);
                    })
                    ->editColumn('FAmount', function ($data) { 
                        return !empty($data->FAmount)?currency_format($data->FAmount,2):currency_format(0,2);
                    })
                    ->editColumn('ExchRate', function ($data) {
                        return !empty($data->ExchRate)?currency_format($data->ExchRate):currency_format(0,2);
                    }) 
                    ->editColumn('TotalAmount', function ($data) {
                        return !empty($data->TotalAmount)?currency_format($data->TotalAmount,2):currency_format(0,2);
                    }) 
                    ->editColumn('Comm', function ($data) {
                        return !empty($data->Comm)?currency_format($data->Comm,2):currency_format(0,2);
                    }) 
                    ->editColumn('AgentTotal', function ($data) {
                        return !empty($data->AgentTotal)?currency_format($data->AgentTotal,2):currency_format(0,2);
                    }) 
                    ->addColumn('AgentCommission', function ($data) {
                        return !empty($data->AgentCommission)?currency_format($data->AgentCommission,2):currency_format(0,2);
                    })
                     ->addColumn('AgentRate', function ($data) {
                        return !empty($data->AgentRate)?currency_format($data->AgentRate):currency_format(0,2);
                    }) 
                    ->editColumn('PaymentDate', function ($data){
                    	if(!empty($data->PaymentDate)){
                            $date = \Carbon\Carbon::parse($data->PaymentDate);
                            return $date->format('d/m/Y');
                        }else{
                            return '';
                        }
                    })
                    ->editColumn('BenName', function ($data) {
                        return '<a data-toggle="modal" data-target="#changeBeneficiaryModal" href="" data-ref="'.$data->id.'" id="changeBenName">'.$data->BenName.'</a>';
                    })
                  ->addColumn('Select', function ($data) {
                    if(!empty($data->TranNo)){
                        return '<div class="row"><div class="col-md-3"><label class="checkbox checkbox-primary"><input class="form-control" type="checkbox" name="select[]" value="'.$data->TranNo.'" id="deallistcheckbox"><span class="checkmark"></span></label></div></div>'; 
                    }             
                  })
                  ->addColumn('DBS', function ($data) {
                  		if(!$data->dbs_res){
                    		return '<a href="posttransaction?dataval='.$data->id.'" data-ref="'.$data->TranNo.'">Send DBS</a>'; 
                  		}
                  })
                  ->escapeColumns([])
                    ->setRowId(function ($data) {
                        return $data->TranNo;
                    })->setRowClass(function () {
                    return 'tableclicked';
                    })->make(true);

        }
        if(env('ENABLE_DD_TT') == 1){
        	return view('transaction.ddtransaction.searchlist',compact('beneficiary'));
        }else{
            return redirect()->route('home');
        }
    }
    public function unpostsearchlist(Request $request){

        if($request->ajax()){
           $data  = DDtransaction::where('status', 1);
           /*$data  = DDtransactionunpost::where('TranNo','!=',NULL);*/
            if(!empty($request->cust_code)){
                $data  = $data->where('CustCode',$request->cust_code);
            }
            if(!empty($request->CustNRICNO)){
                $data  = $data->where('CustPPNo',$request->CustNRICNO);
            }
            /*if(!empty($request->tranno)){
                $data  = $data->where('TranNo',$request->tranno);
            }*/
           	if(!empty($request->currency_code)){
                $data  = $data->where('CurrencyCode',$request->currency_code);
            }

            if(!empty($request->bank_code)){
                $data  = $data->where('BenPPNo',$request->bank_code);
            }
	        if(!empty($request->bank_name)){
	            $data  = $data->where('BenBank',$request->bank_name);
	        }
            if(!empty($request->from_date) && !empty($request->to_date)){
            	$from_date = date("d-m-Y", strtotime(str_replace('/', '-', $request->from_date)));
            	$to_date = date("d-m-Y", strtotime(str_replace('/', '-', $request->to_date)));
                $data  = $data->whereBetween('CreatedDate', [Carbon::parse($from_date)->startOfDay(), Carbon::parse($to_date)->endOfDay()]);
            }
	        if(!empty($request->swiftcode)){
	            $data  = $data->where('SwiftCode',$request->swiftcode);
	        }
	        if(!empty($request->acno)){
	            $data  = $data->where('BenAccNo',$request->acno);
	        }
	        if(!empty($request->benname)){
	            $data  = $data->where('BenName',$request->benname);
	        }
            $data  = $data->get();
            /*printArray($data->BenName);die;*/
            return Datatables::of($data)
                ->addIndexColumn()
                    ->editColumn('TranDate', function ($data){
                    	if(!empty($data->TranDate)){
                       		$date = \Carbon\Carbon::parse($data->TranDate);
                            return $date->format('d/m/Y');
                    	}else{
                    		return '';
                    	}
                    })
                    ->editColumn('id', function ($data) {
                        return '<a href="unpost?unpost_id='.$data->id.'" target="_blank">'.$data->id.'</a>';
                    })
                    ->editColumn('BenName', function ($data) {
                        return $data->BenName;
                    })
                    ->editColumn('CurrencyCode', function ($data) {
			            /*$currencycode = DB::table('dd_transaction')
			                    ->leftJoin('currency_master', 'dd_transaction.CurrencyCode', '=', 'currency_master.CurrencyCode')
			                    ->where('currency_master.CurrencyCode', '=', $data->CurrencyCode)
			                    ->select('currency_master.CurrencyName')
			                    ->first();
                        return $currencycode->CurrencyName;*/
                        return $data->CurrencyCode;
                    })
                    ->editColumn('LAmount', function ($data) { 
                        return !empty($data->LAmount)?currency_format($data->LAmount,2):currency_format(0,2);
                    })
                    ->editColumn('FAmount', function ($data) { 
                        return !empty($data->FAmount)?currency_format($data->FAmount,2):currency_format(0,2);
                    })
                    ->editColumn('ExchRate', function ($data) {
                        return !empty($data->ExchRate)?currency_format($data->ExchRate):currency_format(0,2);
                    }) 
                    ->editColumn('TotalAmount', function ($data) {
                        return !empty($data->TotalAmount)?currency_format($data->TotalAmount,2):currency_format(0,2);
                    }) 
                    ->editColumn('Comm', function ($data) {
                        return !empty($data->Comm)?currency_format($data->Comm,2):currency_format(0,2);
                    }) 
                   /* ->editColumn('PaymentDate', function ($data){
                    	if(!empty($data->PaymentDate)){
                            $date = \Carbon\Carbon::parse($data->PaymentDate);
                            return $date->format('d/m/Y');
                        }else{
                            return '';
                        }
                    })*/
                    ->editColumn('PaidStatus', function ($data) {
                    	if($data->PaidStatus == 'Y'){
                    		//$paidStatus = '<a data-toggle="modal" data-target="#unpost_view" class="viewUnpost" href="" data-ref="'.$data->id.'">View</a>';
                    		 if(strtolower($data->PayMode)=='cash'){
                    		 	$img = env('APP_URL').'/assets/images/cash.png';
                    		 $paidStatus = '<a data-toggle="modal" data-target="#unpost_view" class="viewUnpost" href="" data-ref="'.$data->id.'"><img src="'.$img.'"></a>';
                    		 }else{
                    		 	 $img = env('APP_URL').'/assets/images/nets.png';
                    		 $paidStatus = '<a data-toggle="modal" data-target="#unpost_view" class="viewUnpost" href="" data-ref="'.$data->id.'"><img src="'.$img.'"></a>';
                    		 }
                    	} else {
                    		$paidStatus = 'No';
                    	}
                        return $paidStatus;
                    })
                  ->addColumn('Select', function ($data) {
                    if(!empty($data->id) && $data->PaidStatus == 'Y'){
                        return '<div class="row"><div class="col-md-3"><label class="checkbox checkbox-primary "><input class="form-control checkboxgroup" type="checkbox" name="select[]" data-ref="'.$data->CurrencyCode.'" value="'.$data->id.'" id="unpostlistcheckbox" name="unpostlistcheckbox"><span class="checkmark "></span></label></div></div>'; 
                    }             
                  })
                  ->escapeColumns([])
                  ->setRowId(function ($data) {
                        return $data->TranNo;
                  })
                  ->setRowClass(function () {
                  	    return 'tableclicked';
                  })->make(true);

        }
        if(env('ENABLE_DD_TT') == 1){
        	return view('transaction.ddtransaction.unpostsearchlist');
        }else{
            return redirect()->route('home');
        }
    }
    public function purpose(Request $request){
		if($request->ajax()){
			$data = Purpose::get();
			return Datatables::of($data)
			->addIndexColumn()
		    ->editColumn('Name', function ($data) {
		        return $data->Name;
		    })
		    ->editColumn('Status', function ($data) {
		        return ($data->Status == '1') ? 'Active' : 'In active';
		    })
		    ->addColumn('Created_At', function ($data) {
		        $date = \Carbon\Carbon::parse($data->CreatedDate);
		          return $date->format('d/m/Y');
		    })
		    ->addColumn('Action', function ($data) {
		        return '<div class="col-md-3" style="cursor: pointer;">
		        <a class="text-success mr-2 edit_purpose" data-purposeid="'.$data->id.'" ><i class="lg i-Pen-4 font-weight-bold " aria-hidden="true"></i></a>
		        <a class="text-danger mr-2 delete_purpose" data-purposeid="'.$data->id.'" ><i class="nav-icon i-Close-Window font-weight-bold"></i></a>
		        </div>';
		    })
		    ->setRowId(function ($data) {
		        return $data->id;
		    })
		    ->escapeColumns([])
		        ->setRowId(function ($data) {
		            return $data->id;
		        })->setRowClass(function () {
		        return 'tableclicked';
		        })->make(true);
		}
		if(env('ENABLE_DD_TT') == 1){
		return view('transaction.ddtransaction.purpose');
		} else {
		return redirect()->route('home');
		}
    }
    public function purpose_insert(Request $request){

	    if(isset($request->type) && $request->type=='Add'){
			$purpose = new Purpose;
			$purpose->Name   = $request->name;
			$purpose->Status = $request->status;
			$purpose->save();
			if($purpose){
				return response()->json(['status' => '200', 'type' => 'Add' , 'message'=> 'New Purpose Added']);
			}
	    }
	    else if(isset($request->type) && $request->type=='Update'){
			if($request->id){
				$purpose = Purpose::where('id',$request->id)->update([
					'Name'   => $request->name,
					'Status' => $request->status
				]);
				return response()->json(['status' => '200', 'type' => 'Add' , 'message'=> 'Purpose Updated Successfully']);
			}
	    }
	    
    }
    public function getpurposedata(Request $request){
		if($request->id){
			$data = Purpose::where('id',$request->id)->first();
			return response()->json(['status' => '200', 'data' => $data]);
		}else{
			return response()->json(['status' => '201']);
		}
    }

    public function deletepurposedata(Request $request){
		if($request->id){
			$data = Purpose::where('id',$request->id)->delete();
			return response()->json(['status' => '200', 'message' => 'Purpose Deleted!']);
		}
    }

    public function getCustDealDetail(Request $request)
    {
    	if($request->currid){
	    	$data = DB::table('deal_trans_detail')
	            ->leftJoin('deal_tran_header', 'deal_tran_header.DealNo', '=', 'deal_trans_detail.DealNo')
	            ->where('deal_trans_detail.TranType', '=', 'Sell')
	            ->where('deal_tran_header.CustomerCode', '=', [$request->cusid])
	            ->where('deal_trans_detail.CurrencyCode','=', [$request->currid])
	            ->select('deal_tran_header.id','deal_trans_detail.TranNo', 'deal_tran_header.DealNo', 'deal_trans_detail.ValidTill', 'deal_trans_detail.Rate', 'deal_trans_detail.FAmount', 'deal_trans_detail.BalanceAmount')
	            ->get(); 
	        if($data){
	        	return response()->json(['status' => '200', 'data' => $data]);
	        } else {
	        	return response()->json(['status' => '201']);
	        }
    	} else {
        	return response()->json(['status' => '201']);
        }
    }

    public function getAgentDealDetail(Request $request)
    {
    	$data = DB::table('deal_trans_detail')
	            ->leftJoin('deal_tran_header', 'deal_tran_header.DealNo', '=', 'deal_trans_detail.DealNo')
            	->where('deal_trans_detail.TranType', '=', 'Buy')
	            ->where('deal_tran_header.CustomerCode', '=', [$request->agentid])
            	->where('deal_trans_detail.CurrencyCode','=', [$request->currid])
	            ->select('deal_tran_header.id','deal_trans_detail.TranNo', 'deal_tran_header.DealNo', 'deal_trans_detail.ValidTill', 'deal_trans_detail.Rate', 'deal_trans_detail.FAmount', 'deal_trans_detail.BalanceAmount')
	            ->get();
        if($data){
        	return response()->json(['status' => '200', 'data' => $data]);
        } else {
        	return response()->json(['status' => '201']);
        }
    }

	/*public function getCustDealDetail(Request $request){
		if($request->ajax()){
	    	$data = DB::table('deal_trans_detail')
	            ->leftJoin('deal_tran_header', 'deal_tran_header.DealNo', '=', 'deal_trans_detail.DealNo')
	            ->where('deal_tran_header.CustomerCode', '=', [$request->cusid])
	            ->select('deal_tran_header.id', 'deal_tran_header.DealNo', 'deal_tran_header.DealDate', 'deal_trans_detail.Rate', 'deal_trans_detail.FAmount', 'deal_trans_detail.BalanceAmount')
	            ->get()->toArray(); 
    			$closeAmt = 0;
                foreach ($data as $key => $value) {
                    $closeAmt += $value->BalanceAmount;		
                    $data[$key]->closeAmt =  currency_format($closeAmt,2);
                }		
	            return Datatables::of($data)
				->addIndexColumn()
			    ->setRowId(function ($data) {
			        return $data->id;
			    })
              	->editColumn('FAmount', function ($data) {
                    return currency_format($data->FAmount,2);
                })
              	->editColumn('BalanceAmount', function ($data) {
                    return currency_format($data->BalanceAmount,2);
                })
              	->editColumn('DealDate', function ($data) {
                    return date("d-m-Y", strtotime($data->DealDate));
                })
			    ->addColumn('CloseAmount', function ($data){
			        return $data->closeAmt;
			    })
			    ->escapeColumns([])
			        ->setRowId(function ($data) {
			            return $data->id;
			        })->setRowClass(function () {
			        return 'tableclicked';
			        })->make(true);
        }
    }*/
   /* public function getAgentDealDetail(Request $request){
		if($request->ajax()){
	    	$data = DB::table('deal_trans_detail')
	            ->leftJoin('deal_tran_header', 'deal_tran_header.DealNo', '=', 'deal_trans_detail.DealNo')
	            ->where('deal_tran_header.CustomerCode', '=', [$request->agentid])
	            ->select('deal_tran_header.id', 'deal_tran_header.DealNo', 'deal_trans_detail.ValidTill', 'deal_trans_detail.Rate', 'deal_trans_detail.FAmount', 'deal_trans_detail.BalanceAmount')
	            ->get()->toArray(); 
    			$closeAmt = 0;
                foreach ($data as $key => $value) {
                    $closeAmt += $value->BalanceAmount;		
                    $data[$key]->closeAmt =  currency_format($closeAmt,2);
                }
	            return Datatables::of($data)
				->addIndexColumn()
			    ->setRowId(function ($data) {
			        return $data->id;
			    })
              	->editColumn('FAmount', function ($data) {
                    return currency_format($data->FAmount,2);
                })
              	->editColumn('BalanceAmount', function ($data) {
                    return currency_format($data->BalanceAmount,2);
                })
              	->editColumn('ValidTill', function ($data) {
                    return date("d/m/Y", strtotime($data->ValidTill));
                })
			    ->addColumn('CloseAmount', function ($data) {
			          return $data->closeAmt;
			    })
			    ->escapeColumns([])
			        ->setRowId(function ($data) {
			            return $data->id;
			        })->setRowClass(function () {
			        return 'tableclicked';
			        })->make(true);
        }
    }*/

    public function convertToPost(Request $request) {
    	$TranNodb = $TranNo = $Trimid = $TrimNo = $NextTranNo = '';
		try {
	    	if($request->convertId != "")
	    	{
	    		$amount = DDtransaction::select('FAmount')->where('id',  $request->convertId)->first();
	    		$agentAmount = $amount->FAmount * $request->agentrate;
	    		if(isset($request->agentcomm)){
	    			$agentTotal = $agentAmount + $request->agentcomm;
	    		} else {
	    			$agentTotal = $agentAmount;
	    		}
	    		$TranNodb = DB::table('company')->select('NextTTTranNo')->get();
	    		$TranNo = $TranNodb[0]->NextTTTranNo;

	    		$postTT = DDtransaction::where('id',  $request->convertId)->update([
	    			'TranNo'    => $TranNo,
	    			'AgentCode' => $request->agentcode,
	    			'AgentName' => $request->agentname,
	    			'AgentRate' => $request->agentrate,
	    			'AgentExRate' => $agentAmount,
	    			'AgentCommission' => $request->agentcomm,
	    			'AgentTotal' => $agentTotal,
					'status' => 3,
				]);
				$Trimid = ltrim($TranNo, '0');
				$TrimNo = (int)$Trimid+1;
				$NextTranNo = str_pad($TrimNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);

				$comp = DB::table('company')->update([
	    			'NextTTTranNo' => $NextTranNo,
				]);

				return response()->json(['status' => '200', 'content'=>'DD Transaction Details Update successfully!']);
    		}
        } catch (Exception $e) {
        	return response()->json(['status' => '201', 'content'=>$e]);
        }
    }

    public function changeBeneficiary(Request $request) {
    	try {
    		if($request->beneId != '')
    		{
    			$bene = BenificieryDetails::where('id',  $request->beneId)->first();
    			$changeBene = DDtransaction::where('id',  $request->changeId)->update([
    				'BenName' => $bene->BeneName,
    				'BenAddress1' => $bene->BeneAddress1,
    				'BenAddress2' => $bene->BeneAddress2,
    				'BenAddress3' => $bene->BeneCountry,
    				'BeneMobileNo' => $bene->BeneMobileNo,
    				'BenPPNo' => $bene->beneficiary_id,
    				'BenBank' => $bene->BeneBankName,
    				'BenAccNo' => $bene->BeneBankAccNo,
    				'BenBankBranch' => $bene->BeneBankBranch,
    				'BeneId' => $bene->id,
    				'SwiftCode' => $bene->SwiftCode,
    			]);
    			return response()->json(['status' => '200', 'content'=>'DD Transaction Details Update successfully!']);
    		}
    	} catch (Exception $e) {
        	return response()->json(['status' => '201', 'content'=>$e]);
        }
    }

    public function updateRemark(Request $request){
    	if($request->ref_no != ""){
    		$remark = DDtransaction::where('id',  $request->ref_no)->update([
				'RemarkCollectPaymentCash' => $request->cashRemark,
			]);
			$this->sendSMS($request->phone_no,$request->cashRemark);
    	}
    }

	public function sendSMS($CustMobileNO,$str){
	   $otpMsg = $str;
	 
	   $post_data = array(
	        'ID' => urlencode(env('SMS_ID')),
	        'Password' => urlencode(env('SMS_PASSWORD')),
	        'Mobile' => urlencode($CustMobileNO),
	        'Type'=>urlencode('A'),
	        'Message'=>urlencode($otpMsg),
	        'Sender'=>urlencode('Payout')
	    );
	    $url = 'https://www.commzgate.net/gateway/SendMsg';
	    $authStr ='BasicYXB0YXV0aDpMazlnYWNkazREOE1nOFdN';
	    if($CustMobileNO!=''){
	    	if(env('SMS_ENABLE')==1){
	    		$this->curlPost($url,$post_data,$authStr);
	    	}
	        
	    }
	}

	public function curlPost($url,$fields,$authStr){
	    $curl = curl_init();
	    $fields_string ='';
	    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }

	    rtrim($fields_string, '&');

	    curl_setopt_array($curl, array(
	         CURLOPT_URL => $url,
	         CURLOPT_RETURNTRANSFER => true,
	         CURLOPT_ENCODING => "",
	         CURLOPT_MAXREDIRS => 10,
	         CURLOPT_TIMEOUT => 30,
	         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	         CURLOPT_CUSTOMREQUEST => "POST",
	         CURLOPT_POSTFIELDS => $fields_string,
	         CURLOPT_HTTPHEADER => array(
	           "authorization:".$authStr,
	           "cache-control: no-cache",
	           "content-type: application/x-www-form-urlencoded",
	           "postman-token: 6396794b-56a5-176c-eac3-1539e122fb07"
	         ),
	    ));

	   echo $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);
	    return $response;
	}

	public function exportpostlist(Request $request)
    {
        if($request->ajax()){
        	$transdetails = $request->all();       
        	if(!empty($transdetails)) {
        	$transdetails = $transdetails['trans'];
	        	if(isset($transdetails) && count($transdetails) > 0) {
	        		$transactions = DDtransaction::select('id','CustCode','PayMode','TranNo','CustName','BenName','TranDate','TotalAmount','BenBankBranch','BenAccNo','BenAddress1','Purpose')->whereIn('TranNo',$transdetails)->orderBy('id','DESC')->get()->toArray();	
					header('Content-Type: text/csv; charset=utf-8');
	    			header('Content-Disposition: attachment; filename=data.csv');
	    			$output = fopen('php://output', 'w');
					fputcsv($output, array('Client_Code', 'Product_Code', 'Payment_Type','Payment_Ref_No.','Payment_Date','Dr_Ac_No','Amount','Bank_Code_Indicator',' ','Beneficiary_Name',' ','Beneficiary_Branch / IFSC Code','Beneficiary_Acc_No',' ',' ',' ',' ',' ',' ',' ',' ','REMITTER','BENE AC TYPE','REMITTER ADDRESS','REMITTER AC NO','REMIT PURPOSE','REMITTER MOBILE',' '));
					foreach($transactions as $row) {
						$paymentdate = date('d-m-Y', strtotime($row['TranDate']));
						fputcsv($output, array($row['CustCode'], '',$row['PayMode'],$row['TranNo'],$paymentdate,'',$row['TotalAmount'],'M','',$row['BenName'],'',$row['BenBankBranch'],$row['BenAccNo'],'','','','','','','','',$row['CustName'],'',$row['BenAddress1'],'','',$row['Purpose'],'',''));
					}

	        	}
	        } else {
	        	echo 'nodata';
	        }
	    }
    }

    public function posttransaction(Request $request)
    {	
    	$transdetails = $request->dataval;       
    	if(!empty($transdetails)) {
    		/*printArray($transdetails);die;*/
    		$data = DDtransaction::where('id',$transdetails)->first();
			$this->sendDBS($data);
    	}
    }

	public function sendDBS($data){
	   $post_data = array(
	        'id' => urlencode($data->id),
	        'TranNo' => urlencode($data->TranNo),
	        'DocType' => urlencode($data->DocType),
	        'TranType' => urlencode($data->TranType),
	        'CustCode' => urlencode($data->CustCode),
	        'CustName' => urlencode($data->CustName),
	        'CustPPNo' => urlencode($data->CustPPNo),
	        'CustNationality' => urlencode($data->CustNationality),
	        'PayMode' => urlencode($data->PayMode),
	        'Purpose' => urlencode($data->Purpose),
	        'CustTTType' => urlencode($data->CustTTType),
	        'CompName' => urlencode($data->CompName),
	        'CompAddress1' => urlencode($data->CompAddress1),
	        'CompAddress2' => urlencode($data->CompAddress2),
	        'CompAddress3' => urlencode($data->CompAddress3),
	        'CompAddress4' => urlencode($data->CompAddress4),
	        'BenName' => urlencode($data->BenName),
	        'BenAddress1' => urlencode($data->BenAddress1),
	        'BenAddress2' => urlencode($data->BenAddress2),
	        'BenAddress3' => urlencode($data->BenAddress3),
	        'BeneMobileNo' => urlencode($data->BeneMobileNo),
	        'BenPPNo' => urlencode($data->BenPPNo),
	        'AgentCode' => urlencode($data->AgentCode),
	        'AgentTTType' => urlencode($data->AgentTTType),
	        'AgentRate' => urlencode($data->AgentRate),
	        'AgentPayStatus' => urlencode($data->AgentPayStatus),
	        'MessageCode' => urlencode($data->MessageCode),
	        'CurrencyCode' => urlencode($data->CurrencyCode),
	        'ExchRate' => urlencode($data->ExchRate),
	        'FAmount' => urlencode($data->FAmount),
	        'LAmount' => urlencode($data->LAmount),
	        'Comm' => urlencode($data->Comm),
	        'Other' => urlencode($data->Other),
	        'Service' => urlencode($data->Service),
	        'Postal' => urlencode($data->Postal),
	        'TotalAmount' => urlencode($data->TotalAmount),
	        'AgentCommission' => urlencode($data->AgentCommission),
	        'AgentOther' => urlencode($data->AgentOther),
	        'AgentService' => urlencode($data->AgentService),
	        'AgentPostal' => urlencode($data->AgentPostal),
	        'DDNo' => urlencode($data->DDNo),
	        'TranDate' => urlencode($data->TranDate),
	        'TransmissionDate' => urlencode($data->TransmissionDate),
	        'PaymentDate' => urlencode($data->PaymentDate),
	        'BenBank' => urlencode($data->BenBank),
	        'BenAccNo' => urlencode($data->BenAccNo),
	        'BenBankBranch' => urlencode($data->BenBankBranch),
	        'MCRef1' => urlencode($data->MCRef1),
	        'MCRef2' => urlencode($data->MCRef2),
	        'DOB' => urlencode($data->DOB),
	        'Terminalname' => urlencode($data->Terminalname),
	        'PaidStatus' => urlencode($data->PaidStatus),
	        'PaidBy' => urlencode($data->PaidBy),
	        'CreatedBy' => urlencode($data->CreatedBy),
	        'CreatedDate' => urlencode($data->CreatedDate),
	        'ModifiedBy' => urlencode($data->ModifiedBy),
	        'ModifiedDate' => urlencode($data->ModifiedDate),
	        'Telephone' => urlencode($data->Telephone),
	        'Remarks' => urlencode($data->Remarks),
	        'SwiftCode' => urlencode($data->SwiftCode),
	        'BeneId' => urlencode($data->BeneId),
	        'AgentExRate' => urlencode($data->AgentExRate),
	        'AgentTotal' => urlencode($data->AgentTotal),
	        'AgentName' => urlencode($data->AgentName),
	        'status' => urlencode($data->status),
	        'orginator' => urlencode($data->orginator),
	        'orginatorId' => urlencode($data->orginatorId),
	        'payment_details_bank' => urlencode($data->payment_details_bank),
	        'statement_ref' => urlencode($data->statement_ref),
	        'additional_notes' => urlencode($data->additional_notes),
	        'Payment_Remarks' => urlencode($data->Payment_Remarks),
	        'Online' => urlencode($data->Online),
	        'PaymentLocationCode' => urlencode($data->PaymentLocationCode),
	        'PaymentTerminalCode' => urlencode($data->PaymentTerminalCode),
	        'CollectPayment' => urlencode($data->CollectPayment),
	        'RemarkCollectPaymentCash' => urlencode($data->RemarkCollectPaymentCash)
	    );
		/*printArray($post_data);die;*/
	    $url = env('DBSAPI_URL');
	    $authStr ='BasicYXB0YXV0aDpMazlnYWNkazREOE1nOFdN';
	    $this->curlPostDBS($url,$post_data,$authStr);
	}
	public function curlPostDBS($url,$fields,$authStr){
	    $curl = curl_init();
	    $fields_string ='';
	    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	    rtrim($fields_string, '&');

	    curl_setopt_array($curl, array(
	         CURLOPT_URL => $url.'?'.$fields_string,
	         CURLOPT_RETURNTRANSFER => true,
	         CURLOPT_ENCODING => "",
	         CURLOPT_MAXREDIRS => 10,
	         CURLOPT_TIMEOUT => 30,
	         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	         CURLOPT_CUSTOMREQUEST => "GET",
/*	         CURLOPT_POSTFIELDS => $fields_string,
*/	         CURLOPT_HTTPHEADER => array(
	           "authorization:".$authStr,
	           "cache-control: no-cache",
	           "content-type: application/x-www-form-urlencoded",
	           "postman-token: 6396794b-56a5-176c-eac3-1539e122fb07"
	         ),
	    ));

	   echo $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);
	    return $response;
	}

    public function updatetransaction(Request $request)
    {	
        if($request->ajax()){
        	$transdetails = $request->all();       
        	if(!empty($transdetails)) {
				return response()->json(['status' => '200', 'transdetails'=>$transdetails]);
        	}
        }
    }

    public function updateDeal(Request $request)
    {
    	if($request->amount){
    		$request->amount = str_replace(',', '', $request->amount);
    		$request->amount = (float)$request->amount;
    	}
        if($request->ajax()){
        	$status = $request->status;
        	$balance = $request->amount;
        	$update = DB::table('deal_trans_detail')->where('TranNo',  $request->deal_id)->update([
				'BalanceAmount' => $balance,
				'Status' => $status,
			]);
        }
    }
}
