<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bank;
use Validator;
use DataTables;
use DB;
use Auth;
use Exception;
use Redirect;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request)
    {
        if ($request->ajax()) {
    	   $datas = Bank::get();
            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
    	return view('master.bank');
    }

    public function insert_update(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'bank' => 'required|min:3|max:50',
        ]);
        if($validator->fails()){
        	return response()->json(['status' => '201', 'content'=>$validator->errors()]);
        } else {
        	if($request->id == '' && $request->type == 'save')
        	{
                $codeExists = Bank::where('Bank', $request->bank)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Bank Name already Exists!']);
                } else {
            		$source = new Bank;
                    $source->Bank = $request->bank;
            		$source->Description = $request->description;
                    $source->CreateUser  = Auth::user()->username;
                    $source->CreateDate  = now();
                    $source->ModifyUser  = Auth::user()->username;
                    $source->ModifyDate  = now();
            		$source->save();
    				return response()->json(['status' => '200', 'content'=>'Bank Details inserted successfully!']);
                }
        	} else if($request->id != '' && $request->type == 'update'){
                $codeExists = Bank::where('id', '!=', $request->id)->where('Bank', $request->bank)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Bank Name already Exists!']);
                } else {
            		Bank::where('id', $request->id)->update([
                        'Bank' => $request->bank,
                        'Description' => $request->description,
                        'ModifyUser'  => Auth::user()->username,
                        'ModifyDate'  => now(),
                    ]);
                    return response()->json(['status' => '200', 'content'=>'Bank Details updated successfully!']);
                }
        	}
        }
    }

    public function delete(Request $request)
    {
      if($request->id){
        Bank::where('id', $request->id)->delete();
        return response()->json(['status' => '200', 'content'=>'Source of Income Details Deleted successfully!']);
      } else {
        return response()->json(['status' => '201', 'content'=>'Something went worng!']);
      }
    }

    public function getdetails(Request $request)
    {
        $id = $request->id;
        $data = Bank::where('id', $id)->first();
        if($data){
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }


}

