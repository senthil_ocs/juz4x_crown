<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
Use Exception;
use DB;
use App\LocationCurrency;
use App\Currency;
use DataTables;
use App\Location;

class LocationController extends Controller
{
    public function __construct()
    {
         $this->middleware('auth');
    }

    public function index()
    { 
       return View('master.location');
    }

    public function getlocation(Request $request)
    {
        $datas = Location::select("LocationCode","LocationName")->where("LocationCode","LIKE","%{$request->data}%")->get();
        return response()->json(['status' => '200', 'data' => $datas]);
    }
   
    public function get_location_name(Request $request)
    {
        $datas = Location::select("LocationName")->where("LocationCode", $request->data)->first();
        if($datas){
            return response()->json(['status' => '200', 'data' => $datas]);
        } else {
            return response()->json(['status' => '201']);
        }
    }

    public function get_location_order(Request $request)
    {   
        if ($request->ajax()) {
            $datas = LocationCurrency::where("LocationCode", $request->data)->with('currencies')->get();
            return Datatables::of($datas)
            ->addIndexColumn()
            ->editColumn('BuyRate', function ($datas) {
                  return currency_format($datas->BuyRate);
            })
            ->editColumn('SellRate', function ($datas) {
                  return currency_format($datas->SellRate);
            })
            ->editColumn('Description', function ($datas) {
                  return $datas['currencies']['CurrencyName'];
            })
            ->editColumn('SellRate', function ($datas) {
                  return currency_format($datas->SellRate);
            })
            ->editColumn('Stock', function ($datas) {
                  return currency_format($datas->Stock,2);
            })
            ->editColumn('AvgCost', function ($datas) {
                  return currency_format($datas->AvgCost);
            })
            ->editColumn('Varience', function ($datas) {
                  return currency_format($datas->Varience,2);
            })
            ->setRowId(function ($datas) {
                return $datas->id;
            })->setRowClass(function () {
                return 'tableclicked';
            })->make(true);
        }
    }

    public function get_location_order_details(Request $request)
    {
        $datas = LocationCurrency::where("id", $request->data)->with('currencies')->get();

        $data = array();
        foreach ($datas as $key => $value) {
            $data = array(
                'id'            => $value->id,
                'CurrencyCode'  => $value->CurrencyCode,
                'Description'   => $value['currencies']['CurrencyName'],
                'BuyRate'       => currency_format($value->BuyRate),
                'Stock'         => currency_format($value->Stock,2),
                'DealStock'     => currency_format($value->DealStock,2),
                'SellRate'      => currency_format($value->SellRate),
                'Varience'      => currency_format($value->Varience,2),
                'AvgCost'       => currency_format($value->AvgCost),
            );    
        }
        return response()->json(['status' => '200', 'data' => $data]);
    }

    public function get_location_order_update(Request $request)
    {
        $id         = $request->currency_update_id;
        $currency   = $request->currency;
        $buy_rate   = $request->buy_rate;
        $stock      = $request->stock;
        $dealstock  = $request->dealstock;
        $sell_rate  = $request->sell_rate;
        $avg_cost   = $request->avg_cost;
        $dealstock = str_replace(',', '', $dealstock);
        $stock = str_replace(',', '', $stock);
        if($id){
            LocationCurrency::where('id', $id)->update([
                'CurrencyCode' =>  $currency,
                'Stock'        => $stock,
                'DealStock'    => $dealstock,
                'BuyRate'      => $buy_rate,
                'SellRate'     => $sell_rate,
                'AvgCost'      => $avg_cost,
                'ModifiedDate' => now()
            ]);
            //Stock update in Currency Master            
            Currency::where('CurrencyCode', $currency)->increment('Stock', $stock);
            Currency::where('CurrencyCode', $currency)->increment('DealStock', $dealstock);
            //Stock update in Currency Master
            return response()->json(['status' => '200', 'message'=>'success', 'content'=>'Currency Master Group Details Update successfully!']);   
        } else {
            return response()->json(['status' => '201', 'message'=>'danger', 'content'=>'Currency Master Group Details Not Updated!']);
        }
    }
}
