<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Orginator;
use App\Nationality;
use App\BenificieryDetails;
use Validator;
use DataTables;

class OrginatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request){
        $nationality = Nationality::select('Code')->orderBy('Code')->get();
        $beneficiary = BenificieryDetails::select('id','beneficiary_id')->orderBy('beneficiary_id')->get();
        if ($request->ajax()) {
           $datas = Orginator::get();
            return Datatables::of($datas)
                ->addIndexColumn()
                ->addColumn('DOB', function ($datas) {
                    if($datas->DOB != ''){
                        return date("d/m/Y", strtotime($datas->DOB) );
                    } else {
                        return '';
                    }
                })
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        return view('master.orginator', compact('nationality','beneficiary'));
    }

    public function getdetails(Request $request)
    {
        $id = $request->id;
        $data = Orginator::where('id', $id)->first();
        if($data){
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }

    public function insert_update(Request $request) 
    {
        $validator = Validator::make($request->all(), [
        ]);

        if($validator->fails()){
            return response()->json(['status' => '201', 'content'=>$validator->errors()]);
        } else {
            if($request->id == '' && $request->type == 'save'){
                $orginator                 = new Orginator;
                $orginator->Name           = $request->name;
                $orginator->NRICNo         = $request->nric;
                $orginator->DOB            = $request->dob;
                $orginator->Nationality    = $request->nationality;
                $orginator->Address        = $request->address;
                $orginator->Code           = $request->code;
                $orginator->Beneficiary_id = $request->beneId;
                $orginator->save();
                return response()->json(['status' => '200', 'content'=>'Orginator Details inserted successfully!']);
            }else if($request->id != '' && $request->type == 'update'){
                Orginator::where('id', $request->id)->update([
                    'Name' => $request->name,
                    'NRICNo' => $request->nric,
                    'DOB' => $request->dob,
                    'Nationality' => $request->nationality,
                    'Address' => $request->address,
                    'Code' => $request->code,
                    'Beneficiary_id' => $request->beneId,
                ]);
                return response()->json(['status' => '200', 'content'=>'Orginator Details updated successfully!']);
            }
        }
    }

    public function delete(Request $request)
    {
      if($request->id){
        Orginator::where('id', $request->id)->delete();
        return response()->json(['status' => '200', 'content'=>'Orginator Details Deleted successfully!']);
      } else {
        return response()->json(['status' => '201', 'content'=>'Something went worng!']);
      }
    }
}
