<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SourceofIncome;
use Validator;
use DataTables;
use DB;
use Auth;
use Exception;
use Redirect;

class SourceofincomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request)
    {
        if ($request->ajax()) {
    	   $datas = SourceofIncome::get();
            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
    	return view('master.sourceofincome');
    }

    public function insert_update(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|min:3|max:50',
        ]);
        if($validator->fails()){
        	return response()->json(['status' => '201', 'content'=>$validator->errors()]);
        } else {
        	if($request->id == '' && $request->type == 'save')
        	{
                $codeExists = SourceofIncome::where('Name', $request->name)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Source of Income Name already Exists!']);
                } else {
            		$source = new SourceofIncome;
            		$source->Name = $request->name;
                    $source->CreateUser  = Auth::user()->username;
                    $source->CreateDate  = now();
                    $source->ModifyUser  = Auth::user()->username;
                    $source->ModifyDate  = now();
            		$source->save();
    				return response()->json(['status' => '200', 'content'=>'Source of Income Details inserted successfully!']);
                }
        	} else if($request->id != '' && $request->type == 'update'){
                $codeExists = SourceofIncome::where('id', '!=', $request->id)->where('Name', $request->name)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Source of Income Name already Exists!']);
                } else {
            		SourceofIncome::where('id', $request->id)->update([
                        'Name' => $request->name,
                        'ModifyUser'  => Auth::user()->username,
                        'ModifyDate'  => now(),
                    ]);
                    return response()->json(['status' => '200', 'content'=>'Source of Income Details updated successfully!']);
                }
        	}
        }
    }

    public function delete(Request $request)
    {
      if($request->id){
        SourceofIncome::where('id', $request->id)->delete();
        return response()->json(['status' => '200', 'content'=>'Source of Income Details Deleted successfully!']);
      } else {
        return response()->json(['status' => '201', 'content'=>'Something went worng!']);
      }
    }

    public function getdetails(Request $request)
    {
        $id = $request->id;
        $data = SourceofIncome::where('id', $id)->first();
        if($data){
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }


}

