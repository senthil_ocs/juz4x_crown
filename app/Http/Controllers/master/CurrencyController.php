<?php

namespace App\Http\Controllers\master;

use App\Currency;
use App\CurrencyGroup;
use App\LocationCurrency;
use App\Denomination;
use App\Location;
use App\Nationality;
use App\BuySellDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Exception;
use DB;
use Validator;
use DataTables;
use Auth;
use Carbon\Carbon;

class CurrencyController extends Controller
{
   //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function currencymasterlist()
    {
      //return redirect()->route('location_stock');
      $currencydetails =  array();//Currency::get();
      $currencygroupdetails =  array();//CurrencyGroup::get();
      $denominationdetails =  array();//Denomination::get();
      $regions =  array();//Nationality::select('id','Code','Description')->get();
      $alllocations = array();//Location::get();
      return view('master.currency',compact('currencydetails','currencygroupdetails','denominationdetails','regions','alllocations'));
    }
    
    public function index()
    {
      $currencydetails =  Currency::get();
      $currencygroupdetails =  CurrencyGroup::get();
      $denominationdetails =  Denomination::get();
      $regions =  Nationality::select('id','Code','Description')->get();
      $alllocations = Location::get();
      return view('master.currency',compact('currencydetails','currencygroupdetails','denominationdetails','regions','alllocations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postalcode(Request $request)
    {
        $postal = doGetAddress($request->postal);
        $data = $postal->original['strResult'];
        return response()->json(['status'=>'200' ,'postal'=>$data]);
    }

    public function insertCurrency(Request $request)
    {
        // printArray($request->all()); exit;      
        DB::beginTransaction();
        try {
            if($request->code!='') {
                $avgcost = $request->avgcost;
                if(empty($request->avgcost)) {
                  $avgcost = $request->buyrate;
                }
                $data = ["CurrencyCode" => isset($request->code) ? $request->code : '',
                         "CurrencyName" => isset($request->name) ? $request->name : '',
                         "Major" => isset($request->major) ? $request->major : '',
                         "Minor" => isset($request->minor) ? $request->minor : '',
                         "LocalCurrency" => isset($request->local) ? '1' : '0',
                         "Varience" => $request->variance,
                         "BuyRate" => $request->buyrate,
                         "SellRate" => $request->sellrate,
                         "Units" => $request->units,
                         "Stock" => '0.00',
                         "AvgCost" => $avgcost,
                         "LastPdate" => date('Y-m-d H:i:s'),
                         "LastSDate" => date('Y-m-d H:i:s'),
                         /*"TTCurrency" => isset($request->ttcurrency) ? '1' : '0',*/
                         "TTCurrency" => '1',
                         "TTCustomerCode" => '',
                         /*"TTCustomerUpdate" => isset($request->ttcustomer) ? '1' : '0',*/
                         "DealStock" => '0.00',
                         "Denomination" => 0,
                         /*"CurrencyGroup" => isset($request->currencygroup) ? $request->currencygroup : '',*/
                         /*"Region" => $request->region,*/
                         "CreatedBy" => Auth::user()->username,
                         "CreatedDate" => now(),
                         "ModifiedBy" => Auth::user()->username,
                         "ModifiedDate" => now()
                         ];
                if(!empty($request->optype) && $request->optype=='update') {
                    $cdatadetails = Currency::where('CurrencyCode',$request->code)->first();
                    if($cdatadetails) {
                        $currencyid = $cdatadetails->id;
                        unset($data['CurrencyCode']);
                        Currency::where('id',$currencyid)->update($data);
                        $request->session()->flash('message.level', 'success');
                        $request->session()->flash('message.content', 'Currency Details updated successfully!');
                    }

                } else {
                    Currency::create($data);
                    $request->session()->flash('message.level', 'success');
                    $request->session()->flash('message.content', 'Currency Details inserted successfully!');    
                }            
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', $e->getMessage());
        } catch(\Exception $e)
        {
            DB::rollback();
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', $e->getMessage());
        }

        //Insert into location_currency table

        $alllocations = Location::get();
        if(count($alllocations) > 0) {
          //foreach($alllocations as $location) {
              
              try {
                  if($request->code!='') {                      
                      if(!empty($request->optype) && $request->optype=='update') {
                          $cdatadetails = LocationCurrency::where('CurrencyCode',$request->code)->delete();
                      } 
                      $avgcost = $request->avgcost;
                      if(empty($request->avgcost)) {
                        $avgcost = $request->buyrate;
                      }    
                      foreach($alllocations as $location) {  
                        $locationcode = $location->LocationCode;
                        $cldata = ["CurrencyCode" => isset($request->code) ? $request->code : '',
                               "LocationCode" => $locationcode,
                               "Varience" => $request->variance,
                               "Stock" => '0.00',
                               "BuyRate" => $request->buyrate,
                               "SellRate" => $request->sellrate,
                               "Units" => $request->units,
                               "AvgCost" => $avgcost,
                               "DealStock" => '0.00',
                               "CreatedBy" => Auth::user()->username,
                               "CreatedDate" => now(),
                               "ModifiedBy" => Auth::user()->username,
                               "ModifiedDate" => now()
                               ];                 
                        LocationCurrency::create($cldata);
                      }
                      //$request->session()->flash('message.level', 'success');
                      //$request->session()->flash('message.content', 'Location Currency Details inserted successfully!');    
                  }
              } catch(ValidationException $e)
              {
                  DB::rollback();
                  $request->session()->flash('message.level', 'danger');
                  $request->session()->flash('message.content', $e->getMessage());
              } catch(\Exception $e)
              {
                  DB::rollback();
                  $request->session()->flash('message.level', 'danger');
                  $request->session()->flash('message.content', $e->getMessage());
              }
            //}

        }


        

        //Insert into tbl_denomination table

        try {
            if(!empty($request->optype) && $request->optype=='update') {
                Denomination::where('CurrencyCode',$request->code)->delete();
            }
            if(!empty($request->denomarr)) {
                $denomarray = explode(',',$request->denomarr);
                if(count($denomarray) > 0) {
                    foreach($denomarray as $denom) {
                        if(!empty($denom)) {
                            $denomdetails = explode('_', $denom);
                            $denomval = $denomdetails[0];
                            $denomdesc = $denomdetails[1];
                            $newdenomination = Denomination::create([
                                'CurrencyCode' => $request->code,
                                'Denomination' => $denomval,
                                'Description' => $denomdesc
                            ]);
                        }
                        
                    }
                }
            } else {
                $newdenomination = Denomination::create([
                    'CurrencyCode' => $request->code,
                    'Denomination' => '0.00',
                    'Description' => ''
                ]);
            }

            
        } catch(ValidationException $e)
        {
            DB::rollback();
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', $e->getMessage());
        } catch(\Exception $e)
        {
            DB::rollback();
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', $e->getMessage());
        }        
        DB::commit();

        return redirect('currencymasterlist');
    }

    
    public function denominationAdd(Request $request)
    {
        //printArray($request->all());
        //$id = Denomination::all()->last()->id;
        //Denomination::where('id',$id)->update(["Denomination"=>$request->amount,"Description"=>$request->desc]);   
        //$denomlist = Denomination::get();
        $denomlist = $request->all(); //Denomination::get();
        //printArray($denomlist); exit;
        $str = '';
        if(!empty($denomlist)) {
            

            $d = $denomlist['rowcnt'];
            //$val = '';
            //foreach($denomlist as $denom) {
                //printArray($denomlist); exit;
                $str .= '<tr id="'.$d.'" class="denomcls"><td>'.$d.'</td><td>'.$denomlist["amount"].'</td><td>'.$denomlist["desc"].'</td></tr>';
                $val = $denomlist["amount"].'_'.$denomlist["desc"];
                $d++;
            //}
        }
        //echo $html ='<tr><td>1</td><td>'.$request->amount.'</td><td>'.$request->desc.'</td></tr>';
        echo $str.'---'.$val.'---'.$d;
        exit;
        //return response()->json($response);

    }

    public function fetchCurrency(Request $request)
    {
        $datas = Currency::select("CurrencyCode")->where("CurrencyCode","LIKE","%{$request->data}%")->get();
        if($datas){
          return response()->json(['status' => '200', 'data' => $datas]);
        } else {
          return response()->json(['status' => '201',]);
        }
    }

    public function getcurrencydetails(Request $request)
    {
        if(!empty($request->currencycode)) {
            $currdata = Currency::where('CurrencyCode',$request->currencycode)->first();
            if($currdata) {
              $datas  = $currdata->toArray();
              $datas['Stock'] = currency_format($datas['Stock'],2);
              $datas['Varience'] = currency_format($datas['Varience'],2);
              $datas['BuyRate'] = currency_format($datas['BuyRate']);
              $datas['SellRate'] = currency_format($datas['SellRate']);
              $datas['AvgCost'] = currency_format($datas['AvgCost']);
              $datas['Units'] = currency_format($datas['Units'],2);
            } else {
              $datas = array();
            }
            $currdata = $datas;
            $currdata['currencylist'] = $currdata;

            $loccurrdata = LocationCurrency::where('CurrencyCode',$request->currencycode)->get();
            if(count($loccurrdata) > 0) {
               foreach ($loccurrdata as $key => $value) {
                  $loccurrdata1[] = array(
                    'LocationCode' => $value['LocationCode'], 
                    'Varience' => currency_format($value['Varience'],2), 
                    'Stock' => currency_format($value['Stock'],2), 
                    'BuyRate' => currency_format($value['BuyRate']), 
                    'SellRate' => currency_format($value['SellRate']), 
                    'AvgCost' => currency_format($value['AvgCost']), 
                  );
                }
            } else {
              $loccurrdata1 = array();
            }
              $currdata['loccurrencylist'] = $loccurrdata1;

            $denomcurrdata = array();//Denomination::where('CurrencyCode',$request->currencycode)->get();
            if(count($denomcurrdata) > 0) {
            foreach ($denomcurrdata as $key => $value) {
                $denomcurrencylist[] = array(
                  'id' => $value['id'],
                  'CurrencyCode' => $value['CurrencyCode'],
                  'Description' => $value['Description'],
                  'created_at' => $value['created_at'],
                  'updated_at' => $value['updated_at'],
                  'Denomination' => currency_format($value['Denomination'],2), 
                );
              }
            } else {
              $denomcurrencylist = array();
            } 
            $currdata['denomcurrencylist'] = $denomcurrencylist;
            return $currdata;       
        }
    }


/* Currency Master Group */

    public function currency_master_group(Request $request)
    {
        if ($request->ajax()) {
            $datas =  CurrencyGroup::orderBy('id', 'DESC')->get();
            return Datatables::of($datas)
            ->addIndexColumn()
            ->setRowId(function ($datas) {
                return $datas->id;
            })->setRowClass(function () {
                return 'tableclicked';
            })->make(true);
        }
        return view('master.currency_master');
    }
    
    public function getdetails_currency_master_group(Request $request)
    {
      $id = $request->id;
      $data = CurrencyGroup::where('id', $id)->first();
      return response()->json(['status' => '200', 'details' => $data]);
    }
    public function insert_update_currency_master_group(Request $request)
    {     
        $validator = Validator::make($request->all(), [
          'group_name' => 'required',
          'description' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['status' => '201', 'message'=>'danger', 'content'=>$validator->errors()->first()]);  
        } else {
            if($request->id == '' && $request->type == 'save'){
              $CurrencyGroup = new CurrencyGroup();
              $CurrencyGroup->GroupCode   = $request->group_name;
              $CurrencyGroup->Description = $request->description;
              $CurrencyGroup->CreateUser  = Auth::user()->username;
              $CurrencyGroup->CreateDate  = now();
              $CurrencyGroup->ModifyUser  = Auth::user()->username;
              $CurrencyGroup->ModifyDate  = now();
              $CurrencyGroup->save();
              return response()->json(['status' => '200', 'content'=>'Currency Master Group Details inserted successfully!']);  
            } else if($request->id != '' && $request->type == 'update'){
                CurrencyGroup::where('id', $request->id)->update([
                    'GroupCode' => $request->group_name,
                    'Description' => $request->description,
                    'ModifyUser'  => Auth::user()->username,
                    'ModifyDate'  => now(),
                ]);
              return response()->json(['status' => '200', 'content'=>'Currency Master Group Details Updated successfully!']);  
            }
        }
    }
    public function delete_currency_master_group(Request $request)
    {
      if($request->id){
        CurrencyGroup::where('id', $request->id)->delete();
        return response()->json(['status' => '200', 'content'=>'Currency Master Group Details Deleted successfully!']);
      } else {
        return response()->json(['status' => '201', 'content'=>'Currency Master Group Details Worng!']);
      }
    }

/* End Currency Master Group */ 

    public function deleteCurrency(Request $request)
    {
        if(!empty($request->currencycode)) {
            Currency::where('CurrencyCode',$request->currencycode)->delete();
            Denomination::where('CurrencyCode',$request->currencycode)->delete();
            LocationCurrency::where('CurrencyCode',$request->currencycode)->delete();
        }
    }

    public function buysellcurrencydetails(Request $request)
    { 

      if ($request->ajax() && $request->cdata) {

            $datas = DB::table('buysell')
              ->leftJoin('buysell_details', 'buysell.TranNo', '=', 'buysell_details.TranNo')
               ->where('buysell_details.CurrencyCode', $request->cdata)
               ->where('buysell.LocationCode', $request->location);
              if($request->cdata){
                  $from = Carbon::parse($request->cdate)->startOfDay();
                  $to = Carbon::parse($request->cdate)->endOfDay();
                  $datas = $datas->whereBetween('buysell.TranDate', [$from, $to]);
              }
              $datas = $datas->get();
              $bal = 0;
              foreach ($datas as $key => $value) {
                if($value->TranType == 'Buy'){
                  $bal +=$value->FAmount;
                } else if($value->TranType == 'Sell') {
                  $bal -=$value->FAmount;
                }
                $datas[$key]->bal =  currency_format($bal,2);
              }
              $buy = 0;
          
          return Datatables::of($datas)
              ->editColumn('TranNo', function ($datas) use($buy){
                return str_pad($datas->TranNo, env('VOUCHER_PREFIX'), '0', STR_PAD_LEFT);
              })
              ->editColumn('TranDate', function ($datas) {
                return date('d-m-Y', strtotime($datas->TranDate));
              })
              ->editColumn('Rate', function ($datas) {
                  return currency_format($datas->Rate);
              })
              ->addColumn('Tbuy', function ($datas) {
                if($datas->TranType == 'Buy'){
                  return currency_format($datas->FAmount,2);
                } else {
                  return '0';
                }
              })
              ->addColumn('Tsell', function ($datas) {
                if($datas->TranType == 'Sell'){
                  return currency_format($datas->FAmount,2);
                } else {
                  return '0';
                }              
              })
              ->editColumn('LAmount', function ($datas) {
                  return currency_format($datas->LAmount,2);
              })
              ->addIndexColumn()
              ->setRowId(function ($datas) {
                  return $datas->id;
              })->setRowClass(function () {
                  return 'tableclicked';
              })->make(true);
      }
    }

}
