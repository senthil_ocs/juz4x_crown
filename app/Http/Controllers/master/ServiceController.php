<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use Validator;
use DataTables;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request){
        if ($request->ajax()) {
           $datas = Service::get();
            return Datatables::of($datas)
                ->addIndexColumn()
                ->editColumn('Amount', function ($datas) {
                    return currency_format($datas->Amount,2);
                })
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        return view('master.service');
    }

    public function getdetails(Request $request)
    {
        $id = $request->id;
        $data = Service::where('id', $id)->first();
        $data->Amount = currency_format($data->Amount,2);
        if($data){
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }

    public function insert_update(Request $request) 
    {
        $validator = Validator::make($request->all(), [
          'Code' => 'required|min:3|max:50',
          'description' => 'required|min:3|max:255',
          'amount' => 'required|numeric|min:0|not_in:0',
        ]);

        if($validator->fails()){
            return response()->json(['status' => '201', 'content'=>$validator->errors()]);
        } else {
            if($request->id == '' && $request->type == 'save'){
                $service = new Service;
                $service->ServiceCode = $request->Code;
                $service->Description = $request->description;
                $service->Amount      = $request->amount;
                $service->CreateUser  = 'ups';
                $service->CreateDate  = now();
                $service->ModifyUser  = 'ups';
                $service->ModifyDate  = now();
                $service->save();
                return response()->json(['status' => '200', 'content'=>'Service Details inserted successfully!']);
            }else if($request->id != '' && $request->type == 'update'){
                Service::where('id', $request->id)->update([
                    'ServiceCode' => $request->Code,
                    'Description' => $request->description,
                    'amount'      => $request->amount
                ]);
                return response()->json(['status' => '200', 'content'=>'Service Details updated successfully!']);
            }
        }
    }

    public function delete(Request $request)
    {
      if($request->id){
        Service::where('id', $request->id)->delete();
        return response()->json(['status' => '200', 'content'=>'Service Details Deleted successfully!']);
      } else {
        return response()->json(['status' => '201', 'content'=>'Something went worng!']);
      }
    }
}
