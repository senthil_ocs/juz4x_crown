<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Swift;
use Validator;
use DataTables;
use DB;
use Auth;
use Exception;
use Redirect;

class SwiftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request)
    {
    	$datas = Swift::get();
        if ($request->ajax()) {
            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
    	return view('master.swift', compact('datas'));
    }

    public function insert_update(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'code' => 'required|min:3|max:50',
          'bank' => 'required|min:3',
          'bank_branch' => 'required|min:3',
          'country' => 'required|min:3|max:50',
        ]);
        if($validator->fails()){
        	return response()->json(['status' => '201', 'content'=>$validator->errors()]);
        } else {
        	if($request->id == '' && $request->type == 'save')
        	{
                $codeExists = Swift::where('Code', $request->code)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Swift Code already Exists!']);
                } else {
            		$swift = new Swift;
                    $swift->Code = $request->code;
                    $swift->Bank = $request->bank;
                    $swift->BankBranch = $request->bank_branch;
                    $swift->Country = $request->country;
                    $swift->CreateUser  = Auth::user()->username;
                    $swift->CreateDate  = now();
                    $swift->ModifyUser  = Auth::user()->username;
                    $swift->ModifyDate  = now();
            		$swift->save();
    				return response()->json(['status' => '200', 'content'=>'Bank Details inserted successfully!']);
                }
        	} else if($request->id != '' && $request->type == 'update'){
                $codeExists = Swift::where('id', '!=', $request->id)->where('Code', $request->code)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Swift Code already Exists!']);
                } else {
            		Swift::where('id', $request->id)->update([
                        'Code' => $request->code,
                        'Bank' => $request->bank,
                        'BankBranch' => $request->bank_branch,
                        'Country' => $request->country,
                        'ModifyUser'  => Auth::user()->username,
                        'ModifyDate'  => now(),
                    ]);
                    return response()->json(['status' => '200', 'content'=>'Bank Details updated successfully!']);
                }
        	}
        }
    }

    public function delete(Request $request)
    {
      if($request->id){
        Swift::where('id', $request->id)->delete();
        return response()->json(['status' => '200', 'content'=>'Source of Income Details Deleted successfully!']);
      } else {
        return response()->json(['status' => '201', 'content'=>'Something went worng!']);
      }
    }

    public function getdetails(Request $request)
    {
        $id = $request->id;
        $data = Swift::where('id', $id)->first();
        if($data){
            $data['ModifyDate2'] = '0';
            if($data['ModifyDate']->format('Ymd') <= 0){
                $data['ModifyDate2'] = '1';
            }
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }
    public function getdetailsbycode(Request $request)
    {
        $code = $request->code;
        $data = Swift::where('Code', $code)->first();
        if($data){
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }
    public function getSwiftAutoFill(Request $request)
    {
        if($request->code){
            $data = Swift::where('Code','like', '%'.$request->code.'%')->get();
            $str = '';
            /*printArray($data);die;*/
            if(count($data)>0){
                $str.='<ul class="list-group" id="code_autocomplete" style="position: absolute;background: #f8f9fa;width:85%; overflow-x: scroll; height: 200px;z-index:10">';
                foreach ($data as $value) {
                    $str.= "<li class='list-group-item' style='cursor:pointer;z-index: 99;' onclick=\"selectedCode('".$value['Code']."');\">".$value['Code']."</li>";
                }
                $str.='</ul>';
            }
            return response()->json(['status' => '200', 'str'=> $str]);
        } else {
            return response()->json(['status' => '201', 'content'=>'Something went worng!']);
        }
    }


}

