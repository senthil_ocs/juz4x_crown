<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Nationality;
use Validator;
use DataTables;
use Hash;
use Auth;

class NationalityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request)
    {
        if ($request->ajax()) {
    	   $datas = Nationality::get();
            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
    	return view('master.nationality');
    }

    public function getdetails(Request $request)
    {
        $id = $request->id;
        $data = Nationality::where('id', $id)->first();
        if($data){
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }

    public function insert_update(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'Code' => 'required|min:2|max:50',
          'description' => 'required|min:3|max:255',
        ]);
        if($validator->fails()){
            return response()->json(['status' => '201', 'content'=>$validator->errors()]);
        }else {
            if($request->id == '' && $request->type == 'save'){
                $codeExists = Nationality::where('Code', $request->Code)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Nationality Code already Exists!']);
                } else {
                    $nationality = new Nationality;
                    $nationality->Code = $request->Code;
                    $nationality->Description = $request->description;
                    $nationality->CreateUser  = Auth::user()->username;
                    $nationality->CreateDate  = now();
                    $nationality->ModifyUser  = Auth::user()->username;
                    $nationality->ModifyDate  = now();
                    $nationality->save();
                    return response()->json(['status' => '200', 'content'=>'Nationality Details inserted successfully!']);
                }
            }else if($request->id != '' && $request->type == 'update'){
                $codeExists = Nationality::where('id', '!=' , $request->id)->where('Code', $request->Code)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Nationality Code already Exists!']);
                } else {
                    Nationality::where('id', $request->id)->update([
                        'Code' => $request->Code,
                        'Description' => $request->description,
                        'ModifyUser'  => Auth::user()->username,
                        'ModifyDate'  => now(),
                    ]);
                    return response()->json(['status' => '200', 'content'=>'Nationality Details updated successfully!']);
                }
            }
        }
    }
    
    public function delete(Request $request)
    {
      if($request->id){
        Nationality::where('id', $request->id)->delete();
        return response()->json(['status' => '200', 'content'=>'Nationality Details Deleted successfully!']);
      } else {
        return response()->json(['status' => '201', 'content'=>'Something went worng!']);
      }
    }
}
