<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\SettlementMode;
use Validator;
use DataTables;

class SettlementModeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request)
    {
        $nationality_details = SettlementMode::get();
        if ($request->ajax()) {
            return Datatables::of($nationality_details)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        return view('master.settlement');

    }
    
    public function getdetails(Request $request)
    {
        $id = $request->id;
        $data = SettlementMode::where('id', $id)->first();
        if($data){
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }

    public function insert_update(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'Code' => 'required|min:3|max:50',
          'description' => 'required|min:3|max:255',
        ]);
        if($validator->fails()){
            return response()->json(['status' => '201', 'content'=>$validator->errors()]);
        }else{
            $data =[
                "SettlementCode" =>  $request->Code,
                "Description" => $request->description,
                "CreateUser" => Auth::user()->username,
                "CreateDate" => now(),
                "ModifyUser" => Auth::user()->username,
                "ModifyDate" => now(),
            ];
            if($request->id == '' && $request->type == 'save'){
                $codeExists = SettlementMode::where('SettlementCode', $request->Code)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Settlement Mode Code already Exists!']);
                } else {
                    SettlementMode::create($data);
                    return response()->json(['status' => '200', 'content'=>'Settlement Mode Details inserted successfully!']);
                }
            }else if($request->id != '' && $request->type == 'update'){
                $codeExists = SettlementMode::where('id', '!=' ,$request->id)->where('SettlementCode', $request->Code)->get();
                if(count($codeExists) > 0){
                    return response()->json(['status' => '201', 'content'=>'Settlement Mode Code already Exists!']);
                } else {
                    unset($data['CreateUser']);
                    unset($data['CreateDate']);
                    SettlementMode::where('id', $request->id)->update($data);
                    return response()->json(['status' => '200', 'content'=>'Settlement Mode Details updated successfully!']);
                }
            }
        }
    }

    public function delete(Request $request)
    {
      if($request->id){
        SettlementMode::where('id', $request->id)->delete();
        return response()->json(['status' => '200', 'content'=>'Settlement Mode Details Deleted successfully!']);
      } else {
        return response()->json(['status' => '201', 'content'=>'Something went worng!']);
      }
    }
}
