<?php

namespace App\Http\Controllers\master;

use App\User;
use App\Profile;
use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Exception;
use DB;
use Validator;
use DataTables;
use Auth;
use Hash;

class UserController extends Controller
{
   //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
      $allusers =  User::get();
      $alllocations = Location::get();
      $allprofiles = Profile::where('Active','1')->get();
      return view('master.users',compact('allusers','allprofiles','alllocations'));
    }

    public function createUser(Request $request)
    { 
          try
          {
            $reqdata = $request->all();
            $rules = array(
              'password' => ['required', 'string', 'min:8', 'confirmed'],
              'password_confirmation' => ['required', 'same:password'],
            );
            // Create a new validator instance.
            $validator = Validator::make($reqdata, $rules);

            /* printArray(implode(" ",$validator->errors()->all()));
            die; */


            if ($validator->passes()) {      
              $data = [
                     "username" => $request->userid,
                     "Description" => $request->description,
                     "password" => Hash::make($request->password),
                     "ProfileId" => $request->profileid,
                     "Location" => $request->location,
                     "UserType" => $request->usertype,
                     "Active" => isset($request->active) ? '1' : '0',
                     /*"Admin" => isset($request->administrator) ? '1' : '0',*/
                     "CreatedBy" => Auth::user()->username,
                     "CreatedDate" => now(),
                     "ModifiedBy" => Auth::user()->username,
                     "ModifiedDate" => now()
                    ];

              if(isset($request->profileid) && isset($request->location) && isset($request->description) && empty($request->user_id)) {
                $userName = User::where('username','=',$data['username'])->get();
                $userCount = $userName->count();
                if($userCount > 0) {
                  $request->session()->flash('message.level', 'danger');
                  $request->session()->flash('message.content', 'User Id already exists!');
                } else {
                  User::create($data);
                  //DB::select('CALL update_currency_table'); //stored procedure
                  $request->session()->flash('message.level', 'success');
                  $request->session()->flash('message.content', 'User details added successfully!');
                }
              } else {
                // unset($data['userid']);
                // User::where('id',$request->user_id)->update($data);
                // $request->session()->flash('message.level', 'success');
                // $request->session()->flash('message.content', 'User details updated successfully!');
              }
            } else {
              DB::rollback();
              $request->session()->flash('message.level', 'danger');
              $request->session()->flash('message.content', implode(" ",$validator->errors()->all()));
            }
          }
          catch(ValidationException $e)
          {
              DB::rollback();
              $request->session()->flash('message.level', 'danger');
              $request->session()->flash('message.content', $e->getMessage());
          } catch(\Exception $e)
          {
              DB::rollback();
              $request->session()->flash('message.level', 'danger');
              $request->session()->flash('message.content', $e->getMessage());
          }
          return redirect('user_master')->withInput();
       
    }

    public function getuserdetails(Request $request)
    { 
        $id = $request->id;
        $data = User::where('username', $id)->first();
        if($data){
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }

    public function deleteUser(Request $request)
    {
        $data = User::where('username', $request->userid)->first();
        if($data->ProfileId != '' || $data->ProfileId != Null){  
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'The user Assigned to profile');
            return response()->json(['status' => '201']);
        } else {
            User::where('username',$request->userid)->delete();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'User Deleted successfully!');
            return response()->json(['status' => '200']);
        }
    }

    public function updateuser(Request $request)
    {
      try
      {
        $reqdata = $request->all();
        if (isset($reqdata)) {
          $userexists = User::select('id')->where("username",$request->userid)->where('id', '!=' ,$request->user_id)->get();
          $userexists = $userexists->count();
          if($userexists == 0){
            if(isset($request->profileid) && isset($request->location) && isset($request->description) && isset($request->user_id)) {
              if($request->password != ''){
                $rules = array(
                  'password' => ['required', 'string', 'min:8', 'confirmed'],
                  'password_confirmation' => ['required', 'same:password'],
                );
                $validator = Validator::make($reqdata, $rules);
                if ($validator->passes()) { 
                  $data = [
                           "username" => $request->userid,
                           "Description" => $request->description,
                           "password" => Hash::make($request->password),
                           "ProfileId" => $request->profileid,
                           "Location" => $request->location,
                           "UserType" => $request->usertype,
                           "Active" => isset($request->active) ? '1' : '0',
                           /*"Admin" => isset($request->administrator) ? '1' : '0',*/
                           "ModifiedBy" => Auth::user()->username,
                           "ModifiedDate" => now()
                          ];
                  $update = User::where('id',$request->user_id)->update($data);
                } else {
                  return response()->json(['status' => '201', 'content'=>'Password Must have minimum 8 Characters, Password and Confirm Password must be same!']);
                }
              } else {
                $data = [
                         "username" => $request->userid,
                         "Description" => $request->description,                       
                         "ProfileId" => $request->profileid,
                         "Location" => $request->location,
                         "UserType" => $request->usertype,
                         "Active" => isset($request->active) ? '1' : '0',
                         /*"Admin" => isset($request->administrator) ? '1' : '0',*/
                         "ModifiedBy" => Auth::user()->username,
                         "ModifiedDate" => now()
                        ];
                $update = User::where('id',$request->user_id)->update($data);
              }
              if($update > 0)
               return response()->json(['status' => '200', 'content'=>'User updated successfully!']);
              } else {
                return response()->json(['status' => '201', 'content'=>'Wrong Details!']);
              }
          }else {
            return response()->json(['status' => '201', 'content'=>'User Id already exists !']);
          }
        }
      }
      catch(ValidationException $e)
      {
          DB::rollback();
          $request->session()->flash('message.level', 'danger');
          $request->session()->flash('message.content', $e->getMessage());
      } catch(\Exception $e)
      {
          DB::rollback();
          $request->session()->flash('message.level', 'danger');
          $request->session()->flash('message.content', $e->getMessage());
      }
      return redirect('user_master')->withInput();
    }
}
