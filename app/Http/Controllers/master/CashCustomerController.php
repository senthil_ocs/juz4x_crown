<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use Validator;
use App\CashCustomer;
use App\Location;
use App\LocationCustomer;
use DataTables;
use App\Nationality;
use App\DealTransactionHeader;
use App\DealTransactionDetail;
use Auth;
use App\Country;
use App\BenificieryDetails;
use DB;

class CashCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request)
    {
        $nationality = Nationality::orderBy('Code')->get();
        $identifire  = Config::get('app.identifire_type');
        $country = Country::select('Country')->orderBy('Country', 'ASC')->get();
        $general_type = collect([
            'Local Individual',
            'Overseas Individual'
        ]);
        return view('master.cashcustomer', compact('identifire','nationality','general_type','country'));
    }

    public function edit(Request $request,$id='')
    {
        $cashCustomer = [];
        $cashCustomer = CashCustomer::where("PPNo",$id)->first();
        $nationality = Nationality::orderBy('Code')->get();
        $identifire  = Config::get('app.identifire_type');
        $country = Country::select('Country')->orderBy('Country', 'ASC')->get();
        $general_type = collect([
            'Local Individual',
            'Overseas Individual'
        ]);
        return response()->json(['status' => '200', 'data' => $cashCustomer]);
    }
    public function list(Request $request)
    {
        $alllocations = Location::select('LocationCode')->get();
        $datas = CashCustomer::select('id','CustCode','Name','PhoneNo','DOB','PPNo','Intelligence','Allow','nricfront','nricback','customerface');
        $nationality = Nationality::orderBy('Code')->get();
        $identifire  = Config::get('app.identifire_type');
        $country = Country::select('Country')->orderBy('Country', 'ASC')->get();
        $general_type = collect([
            'Local Individual',
            'Overseas Individual'
        ]);

        if(!empty($request->filterName)){
            $datas  = $datas->where('Name','like','%'.$request->filterName.'%');
        }
        if(!empty($request->filterPhno)){
            $datas  = $datas->where('PhoneNo','like','%'.$request->filterPhno.'%');
        }
        if(!empty($request->filterNric)){
            $datas  = $datas->where('PPNo','like','%'.$request->filterNric.'%');
        }
        if(!empty($request->filterAllow)){
            $datas  = $datas->where('Allow',$request->filterAllow);
        }
        $datas  = $datas->orderBy('id','desc')->get();

        if ($request->ajax()) {
            return Datatables::of($datas)
                ->addIndexColumn()
                ->addColumn('Intelligence', function ($datas) {
                    if($datas->Intelligence == 'Yes'){
                      return '<span class="badge badge-success">Yes</span>'; 
                    } else if($datas->Intelligence == 'No'){
                       return '<span class="badge badge-danger">No</span>'; 
                    } else {
                        return "";
                    } 
                })
                ->addColumn('Allow', function ($datas) {
                    if($datas->Allow == 'Yes'){
                      return '<span class="badge badge-success">Yes</span>'; 
                    } else if($datas->Allow == 'No'){
                       return '<span class="badge badge-danger">No</span>'; 
                    }    else{
                       return ''; 
                    }    
                })
                ->addColumn('DOB', function ($datas) {
                    if($datas->DOB != ''){
                        return date("d/m/Y", strtotime($datas->DOB) );
                    } else {
                        return '';
                    }
                })
                ->addColumn('Action', function ($datas) {
                    if(!empty($datas->PPNo)) {
                        /*href="cashcustomer/'.$datas->PPNo.'" */
                        $img = env('APP_URL').'/assets/images/beneficiary.png';
                        if(env('ENABLE_DD_TT') == 1 ){
                            $icon = '<a class="text-success mr-2 addCustBeneficiary" data-ref="'.$datas->PPNo.'"><img width="50%" src="'.$img.'"></a>';
                        } else {
                            if($datas->nricfront != '' || $datas->nricback != '' || $datas->customerface != ''){
                                $icon = '<a class="text-success mr-2 addImage" data-ref="'.$datas->PPNo.'" nricfront="'.$datas->nricfront.'" nricback="'.$datas->nricback.'" customerface="'.$datas->customerface.'"><i class="lg i-Eye-Visible font-weight-bold" aria-hidden="true"></i></a>';
                            } else {
                                $icon = '<a class="text-success mr-2 addImage" data-ref="'.$datas->PPNo.'" nricfront="'.$datas->nricfront.'" nricback="'.$datas->nricback.'" customerface="'.$datas->customerface.'"><i class="lg i-Upload1 font-weight-bold" aria-hidden="true"></i></a>';
                            }
                        }
                        return '<div class="row"><div class="container col-md-3" style="cursor: pointer;"><a class="text-success mr-2 edit_cashcust" data-ref="'.$datas->PPNo.'"><i class="lg i-Pen-4 font-weight-bold" aria-hidden="true"></i></a></div><div class="container col-md-4" style="padding: unset;">'.$icon.'</div></div>'; 
                    }
                })->escapeColumns([])
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        /*printArray($datas);die;*/
        return view('master.cashcustomer_list',compact('alllocations','identifire','nationality','general_type','country'));
    }

    public function addBeneficiary(Request $request)
    {
        DB::beginTransaction();
        try {
            if($request->addnric_no){

                /*BenificieryDetails::where('CustNRICNO',$request->addnric_no)->delete();
                for($c=0;$c<count($request->td_beneficiary_id);$c++) {*/
                    $maxid = BenificieryDetails::max('id');
                    $beneId= 'B'.str_pad($maxid+1, 6, '0', STR_PAD_LEFT);
                    $data = [
                        "beneficiary_id" => $beneId,
                        "CustCode" => $request->custcode,
                        "CustNRICNO" => $request->addnric_no,
                        "BeneName" => $request->BeneName,
                        "BeneBankName" => $request->BeneBankName,
                        "BeneBankAccNo" => $request->BeneBankAccNo,
                        "BeneAddress1" => $request->BeneAddress1,
                        "BeneAddress2" => $request->BeneAddress2,
                        "BeneCountry" => $request->BeneCountry,
                        "BeneMobileNo" => $request->BeneMobileNo,
                        "BeneBankBranch" => $request->BeneBankBranch,
                        "SwiftCode" => $request->SwiftCode,
                        "Active" => $request->beneficiaryactive,
                        "currency_code" => $request->CurrencyCode,
                        "purpose" => $request->purpose,
                    ];
                    BenificieryDetails::create($data);
                        $status = '200';
                        $msg = 'Beneficiary Details inserted successfully!';        
                /*}*/
            } 
            DB::commit();
        }catch(ValidationException $e)
        {
            DB::rollback();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
            $status = '201';
            $msg = $e->getMessage();
        } catch(\Exception $e)
        {
            DB::rollback();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
            $status = '201';
            $msg = $e->getMessage();
        }
        return  response()->json(['status' => $status, 'content' => $msg]);
    }

    public function addImage(Request $request)
    {
        
        try
        {
            $reqdata = $request->all();
            $rules = array(
                'nricfront' => 'mimes:jpeg,png,jpg,pdf|max:2048',
                'nricback' => 'mimes:jpeg,png,jpg,pdf|max:2048',
                'customerface' => 'mimes:jpeg,png,jpg,pdf|max:2048'
            );
            $validator = Validator::make($reqdata, $rules);
            /*printArray($validator);die;*/
            if ($validator->passes()) { 
                if ($request->hasFile('nricfront')) {
                    $image = $request->file('nricfront');
                    $name = 'nric_front_'.$request->nric_no.'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/storage/');
                    if($image->move($destinationPath, $name)){
                        CashCustomer::where('PPNo', $request->nric_no)->update(['nricfront' => $name]);
                    }
                }

                if ($request->hasFile('nricback')) {
                    $image = $request->file('nricback');
                    $name = 'nric_back_'.$request->nric_no.'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/storage/');
                    if($image->move($destinationPath, $name)){
                        CashCustomer::where('PPNo', $request->nric_no)->update(['nricback' => $name]);
                    }
                }

                if ($request->hasFile('customerface')) {
                    $image = $request->file('customerface');
                    $name = 'customer_face_'.$request->nric_no.'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/storage/');
                    if($image->move($destinationPath, $name)){
                        CashCustomer::where('PPNo', $request->nric_no)->update(['customerface' => $name]);
                    }
                }
                $request->session()->flash('message.level', 'success');
                $request->session()->flash('message.content', 'Image Uploaded Successfully!');
                return back()->withInput($request->only('filterName', 'filterNric', 'filterPhno', 'filterAllow'));
            } else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'Allowed format .pdf,.png,.jpg, Max upload size 2MB');
                return back()->withInput();
            }
        }
        catch(ValidationException $e)
        {
            DB::rollback();
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', $e->getMessage());
        } catch(\Exception $e)
        {
            DB::rollback();
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', $e->getMessage());
        }
        return back()->withInput();
    }

    public function removeImage(Request $request) 
    {
        if($request->img != ''){
            $imageFileName = $request->img;
            $field = $request->field;
            $oldPicture = public_path('storage/').$imageFileName;
            CashCustomer::where('PPNo', $request->nric_no)->update([$field => '']);
            unlink($oldPicture);
            return  response()->json(['status' => '200', 'Msg' => 'Image Deleted Successfully!']);
        } else {
            return  response()->json(['status' => '201', 'Msg' => 'Unable to Delete image!']);
        }

    }

    public function getBeneficiary(Request $request)
    {
        $maxid = BenificieryDetails::max('id');
        $beneId= 'B'.str_pad($maxid+1, 6, '0', STR_PAD_LEFT);
        $Benificierydata = BenificieryDetails::where('CustNRICNO',$request->nric_no)->get();
        /*printArray($Benificierydata);die;*/
        if(count($Benificierydata) > 0) {
            $constring1 = "<input type='hidden' name='delbeneficiaryclsid' id='delbeneficiaryclsid' value='".count($Benificierydata)."' />";
            $m = 1;
            foreach ($Benificierydata as $key => $value) {
                $constring1 .="<tr id='bid".$m."' class='beneficiarycls'><td>".$m."</td><td><input type='hidden' value='".$value->beneficiary_id."' name='td_beneficiary_id[]'>".$value->beneficiary_id."</td><td><input type='hidden' value='".$value->CustCode."' name='td_custcode[]'>" . $value->CustCode . "</td><td><input type='hidden' value='".$value->currency_code."' name='td_CurrencyCode[]'>" . $value->currency_code . "</td><td><input type='hidden' value='".$value->CustNRICNO."' name='td_CustNRICNO[]'>" . $value->CustNRICNO . "</td><td><input type='hidden' value='".$value->BeneName."' name='td_BeneName[]'>" . $value->BeneName . "</td><td><input type='hidden' value='".$value->BeneBankName."' name='td_BeneBankName[]'>" . $value->BeneBankName . "</td><td><input type='hidden' value='".$value->BeneBankAccNo."' name='td_BeneBankAccNo[]'>" . $value->BeneBankAccNo . "</td><td><input type='hidden' value='".$value->BeneAddress1."' name='td_BeneAddress1[]'>" . $value->BeneAddress1 . "</td><td><input type='hidden' value='".$value->BeneAddress2."' name='td_BeneAddress2[]'>" . $value->BeneAddress2 . "</td><td><input type='hidden' value='".$value->BeneCountry."' name='td_BeneCountry[]'>" . $value->BeneCountry . "</td><td><input type='hidden' value='".$value->BeneMobileNo."' name='td_BeneMobileNo[]'>" . $value->BeneMobileNo . "</td><td><input type='hidden' value='".$value->BeneBankBranch."' name='td_BeneBankBranch[]'>" . $value->BeneBankBranch . "</td><td><input type='hidden' value='".$value->SwiftCode."' name='td_SwiftCode[]'>" . $value->SwiftCode . "</td><td><input type='hidden' value='".$value->Active."' name='td_beneficiaryactive[]'>" . $value->Active . "</td><td><input type='hidden' value='".$value->purpose."' name='td_purpose[]'>" . $value->purpose . "</td></tr>";
                $m++;
            }
            $benificiery = $constring1;
            return  response()->json(['status' => '200', 'benificiery' => $benificiery, 'beneId' => $beneId]);
        }
            return  response()->json(['status' => '201', 'beneId' => $beneId]);
    }

    public function insert_update(Request $request)
    {
        /*print_r($request->all());die;*/
        $validator = Validator::make($request->all(), [
          'nric_no'         => 'required',
          'cname'           => 'required',  
          'date_of_birth'   => 'required|date',
          'date_of_expiry'  => 'required|date',
        ]);
        if($validator->fails()){
            return response()->json(['status' => '201', 'content'=>$validator->errors()]);
            printArray('test');die;
        }else {
            if($request->cashcustomer_id == '' && $request->type == 'save'){
                $cashcustomer               = new CashCustomer;
                $cashcustomer->CustCode     = $request->cust_code;
                $cashcustomer->PPNo         = $request->nric_no;
                $cashcustomer->Name         = $request->cname;
                $cashcustomer->DOB          = $request->date_of_birth;
                $cashcustomer->Nationality  = $request->nationality;
                $cashcustomer->Address1     = $request->address;
                $cashcustomer->Address2     = $request->address2;
                $cashcustomer->PostalCode     = $request->postal_code;
                $cashcustomer->Address4     = $request->country;
                $cashcustomer->MCType       = $request->mc_type;
                $cashcustomer->PhoneNo      = $request->tele_no;
                $cashcustomer->PendingCustomerRemarks = $request->pending_remarks;
                $cashcustomer->Activebit    = $request->active;
                $cashcustomer->PendingDocuments    = isset($request->pending_documents) ? $request->pending_documents : 0;
                $cashcustomer->IdentifierType =  $request->identifier_type;
                $cashcustomer->IdentifierTypeOther =  $request->identifier_other;
                $cashcustomer->passexpiry          = $request->date_of_expiry;
                $cashcustomer->UnitNo       = $request->unit;
                $cashcustomer->CreateDate   = now();
                $cashcustomer->CreateUser   = Auth::user()->username;
                $cashcustomer->ModifyDate   = now();
                $cashcustomer->ModifyUser   = Auth::user()->username;
                $cashcustomer->save();

                $alllocations = Location::get();
                foreach($alllocations as $location) {
                    $locationcustomerdata = ["Custcode" => $request->cust_code,
                         "CustName"            => isset($request->cname) ? $request->cname : '',
                         "Address1"            => isset($request->address) ? $request->address : '',
                         "Address2"            => isset($request->address2) ? $request->address2 : '',
                         "Address3"            => '',
                         "Phone"               => isset($request->tele_no) ? $request->tele_no : '',
                         "Fax"                 => '',
                         "Balance"             => 0.00,
                         "Name"                => isset($request->cname) ? $request->cname : '',
                         "NRICNo"              => isset($request->nric_no) ? $request->nric_no : '',
                         "Ob"                  => 0.00,
                         "CreditLimit"         => 0.00,
                         "Remarks"             => isset($request->pending_remarks) ? $request->pending_remarks : '',
                         "TTCustomer"          => 0,
                         "RemittanceLicensee"  => 0,
                         "Country"             => isset($request->country) ? $request->country : '',
                         "Location"            => $location->LocationCode,
                         "MoneyChangerLicense" => '',
                         "TradingLicense"      => '',
                         "DOB"                 => isset($request->date_of_birth) ? $request->date_of_birth : '',
                         "DOE"                 => isset($request->date_of_expiry) ? $request->date_of_expiry : '',
                         "MCType"              => isset($request->mc_type) ? $request->mc_type : '',
                         "TTType"              => '',
                         "DailyCreditLimit"    => 0.00,
                         "DealBalance"         => 0.00,
                         "CreatedBy"           => Auth::user()->username,
                         "CreatedDate"         => now(),
                         "ModifiedBy"          =>Auth::user()->username,
                         "ModifiedDate"        =>now(),
                         "WebCustAllowed"      => 0,
                         "WebCustCode"         => '',
                         "Active"              => isset($request->codeactive) ? 1 : 0,
                         "HighRisk"            => isset($request->highrisk) ? 1 : 0,
                        ];
                    LocationCustomer::create($locationcustomerdata);    
                }
                return response()->json(['status' => '200', 'content'=>'Cash Customer Details inserted successfully!']);
            } else if ($request->cashcustomer_id != '' && $request->type == 'update') {
                CashCustomer::where('id', $request->cashcustomer_id)->update([
                    'PPNo' => $request->nric_no,
                    'Name' => $request->cname,
                    'DOB' => $request->date_of_birth,
                    'Nationality' => $request->nationality,
                    'Address1' => $request->address,
                    'Address2' => $request->address2,
                    'PostalCode' => $request->postal_code,
                    'Address4' => $request->country,
                    'MCType' => $request->mc_type,
                    'PhoneNo' => $request->tele_no,
                    'PendingCustomerRemarks' => $request->pending_remarks,
                    'Activebit' => $request->active,
                    'PendingDocuments' => $request->pending_documents,
                    'IdentifierType' => $request->identifier_type,
                    'IdentifierTypeOther' => $request->identifier_other,
                    'passexpiry' => $request->date_of_expiry,
                    'UnitNo' => $request->unit,
                    'Intelligence' => $request->intelligence,
                    'Allow' => $request->allow,
                    'ModifyDate' => now(),
                    'ModifyUser'  => Auth::user()->username,
                ]);
                return response()->json(['status' => '200', 'content'=>'Cash Customer Details Update successfully!']);
            }
        }
    }

    public function getcashcustomerdetails(Request $request)
    {
        $data = CashCustomer::where('PPNo', $request->id)->first();
        if(isset($data)){
            //Get Deals  
            $loccode = Auth::user()->Location;
            $dealinfo = DealTransactionHeader::where('CustomerCode',$data->CustCode)->where('NRICNo',isset($data->PPNo)?$data->PPNo:'' )->where('LocationCode',$loccode)->get();
            if(count($dealinfo) > 0) {
                foreach($dealinfo as $deal) {
                    $dealno = $deal->DealNo;
                    $dealdetails = DealTransactionDetail::where('DealNo',$dealno)->get();
                    if(count($dealdetails) > 0) {
                        $dealstr = '';
                        $dealstatus='';
                        $d = 1;
                        foreach($dealdetails as $detaildeal) {
                            $dealdate = convertDateformat($detaildeal->CreatedDate);
                            if($detaildeal->Status == 'Realised') {
                                $dealstatus = 'disabled';
                            }else{
                                 $dealstatus='';
                            } 
                            $dealstr .= '<tr>
                                         <td>'.$d.'</td>
                                         <td>'.$detaildeal->DealNo.'</td>
                                         <td>'.$dealdate.'</td>
                                         <td>'.$detaildeal->CurrencyCode.'</td>
                                         <td>'.$detaildeal->TranType.'</td>
                                         <td>'.currency_format($detaildeal->FAmount,2).'</td>
                                         <td>'.currency_format($detaildeal->RealisedAmount,2).'</td>
                                         <td>'.currency_format($detaildeal->BalanceAmount,2).'</td>
                                         <td>'.currency_format($detaildeal->Rate).'</td>
                                         <td>'.convertDateformat($detaildeal->ValidTill).'</td>
                                         <td>'.$detaildeal->Status.'</td>
                                         <td><input type="checkbox" id="chosendeal" name="chosendeal[]" value="'.$detaildeal->TranNo.'" checked="checked" '.$dealstatus.' /></td>
                                         </tr>';
                            $d++;
                        }
                        
                    }
                }
                $data['dealentry'] = isset($dealstr) ? $dealstr : '';
            }
            
            //Get Deals
            if($data){
                return response()->json(['status' => '200', 'data' => $data]);
            } else {
                return response()->json(['status' => '201']);
            }
        } else {
                return response()->json(['status' => '201']);
        }
    }
    public function delete(Request $request)
    {
      if($request->id){
        CashCustomer::where('id', $request->id)->delete();
        return response()->json(['status' => '200', 'content'=>'Cash Customer Details Deleted successfully!']);
      } else {
        return response()->json(['status' => '201', 'content'=>'Something went worng!']);
      }
    }
    public function getNricNo(Request $request)
    {
        if($request->nric){
            $data = CashCustomer::where('PPNO','like', '%'.$request->nric.'%')->get();
            $str = '';
            if(count($data)>0){
                $str.='<ul class="list-group" id="nric_autocomplete" style="position: absolute;background: #f8f9fa;width:85%">';
                foreach ($data as $value) {
                    $str.= "<li class='list-group-item' style='cursor:pointer;z-index: 99;' onclick=\"selectedNric('".$value['PPNo']."');\">".$value['PPNo']."</li>";
                }
                $str.='</ul>';
            }
            return response()->json(['status' => '200', 'str'=> $str]);
        } else {
            return response()->json(['status' => '201', 'content'=>'Something went worng!']);
        }
    }
    public function getNameAutoFill(Request $request)
    {
        if($request->name){
            $data = CashCustomer::where('Name','like', '%'.$request->name.'%')->get();
            $str = '';
            if(count($data)>0){
                $str.='<ul class="list-group" id="name_autocomplete" style="position: absolute;background: #f8f9fa;width:85%">';
                foreach ($data as $value) {
                    $str.= "<li class='list-group-item' style='cursor:pointer;z-index: 99;' onclick=\"selectedName('".$value['Name']."');\">".$value['Name']."</li>";
                }
                $str.='</ul>';
            }
            return response()->json(['status' => '200', 'str'=> $str]);
        } else {
            return response()->json(['status' => '201', 'content'=>'Something went worng!']);
        }
    }
    public function getPhnoAutoFill(Request $request)
    {
        if($request->phno){
            $data = CashCustomer::where('PhoneNo','like', '%'.$request->phno.'%')->get();
            $str = '';
            if(count($data)>0){
                $str.='<ul class="list-group" id="phno_autocomplete" style="position: absolute;background: #f8f9fa;width:85%">';
                foreach ($data as $value) {
                    $str.= "<li class='list-group-item' style='cursor:pointer;z-index: 99;' onclick=\"selectedPhno('".$value['PhoneNo']."');\">".$value['PhoneNo']."</li>";
                }
                $str.='</ul>';
            }
            return response()->json(['status' => '200', 'str'=> $str]);
        } else {
            return response()->json(['status' => '201', 'content'=>'Something went worng!']);
        }
    }
}
