<?php

namespace App\Http\Controllers\master;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Exception;
use DB;
use Validator;
use DataTables;
use Auth;

class ProfileController extends Controller
{
   //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
      $allprofiles =  Profile::get();
      return view('master.profile',compact('allprofiles'));
    }

    public function createProfile(Request $request)
    { 
          try
          {
            $data = ["ProfileId" => $request->profileid,
                     "Description" => $request->description,
                     "Active" => isset($request->active) ? '1' : '0',
                     "CreatedBy" => Auth::user()->username
                    ];
            if(isset($request->profileid) && isset($request->description) && empty($request->profile_id)) {              
              Profile::create($data);
              $request->session()->flash('message.level', 'success');
              $request->session()->flash('message.content', 'Profile details added successfully!');
            } else {
              Profile::where('id',$request->profile_id)->update($data);
              $request->session()->flash('message.level', 'success');
              $request->session()->flash('message.content', 'Profile details updated successfully!');
            }
          }
          catch(ValidationException $e)
          {
              DB::rollback();
              $request->session()->flash('message.level', 'danger');
              $request->session()->flash('message.content', $e->getMessage());
          } catch(\Exception $e)
          {
              DB::rollback();
              $request->session()->flash('message.level', 'danger');
              $request->session()->flash('message.content', $e->getMessage());
          }
          return redirect('profile');
       
    }

    public function getprofiledetails(Request $request)
    { 
        $id = $request->id;
        $data = Profile::where('ProfileId', $id)->first();
        if($data){
            return response()->json(['status' => '200', 'details' => $data]);
        }else{
            return response()->json(['status' => '201']);
        }
    }

    public function deleteProfile(Request $request)
    {
        if(!empty($request->profile_id)) {
            $userdetails = User::where('ProfileId',$request->profile_id)->get();
            if(count($userdetails) > 0) {
              return response()->json(['status' => '201']);
            } else {              
              Profile::where('ProfileId',$request->profile_id)->delete();
              return response()->json(['status' => '200']);
            }
        }
    }

    

}
