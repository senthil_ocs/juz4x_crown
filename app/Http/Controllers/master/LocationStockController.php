<?php

namespace App\Http\Controllers\master;

use App\LocationStock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocationStock  $locationStock
     * @return \Illuminate\Http\Response
     */
    public function show(LocationStock $locationStock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocationStock  $locationStock
     * @return \Illuminate\Http\Response
     */
    public function edit(LocationStock $locationStock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocationStock  $locationStock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocationStock $locationStock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocationStock  $locationStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocationStock $locationStock)
    {
        //
    }
}
