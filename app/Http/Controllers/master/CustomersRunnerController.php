<?php

namespace App\Http\Controllers\master;

use App\CustomersRunner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomersRunnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomersRunner  $customersRunner
     * @return \Illuminate\Http\Response
     */
    public function show(CustomersRunner $customersRunner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomersRunner  $customersRunner
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomersRunner $customersRunner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomersRunner  $customersRunner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomersRunner $customersRunner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomersRunner  $customersRunner
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomersRunner $customersRunner)
    {
        //
    }
}
