<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Nationality;
use App\Customer;
use App\Location;
use App\LocationCustomer;
use App\BuySell;
use Hash;
use Exception;
use Redirect;
use Illuminate\Support\Facades\Input;
use App\CustomerContact;
use App\BenificieryDetails;
use DB;
use Auth;
use DataTables;
use Response;
use App\Country;
use Config;

class CustomerController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $mas_customer_type = collect([
            'Local Money Changer',
            'Outside Money Changer',
            'Individual',
            'Others',
            'All'
        ]);
        $company_type = collect([
            'Sole Proprietor',
            'Partnership',
            'Private Limited',
            'Limited'
        ]);
        $identifier_type = Config::get('app.identifire_type');

        $general_type = collect([
            'Local Trading',
            'Overseas Trading'
        ]);
        $employment_detail = collect([
            'Private Company/Organisation',
            'Self-Employed',
            'Government Service',
            'UnEmployed',
            'Home Maker'
        ]);
        $yearly_income = collect([
            'Below 20,000',
            '20000 < 35,000',
            '35,000<55,000',
            '55,000<75,000',
            '75,000<95,000',
            'Above 95,000'
        ]);
        $sourceofincome = collect([
            'From Business',
            'From Employment',
            'From Property/Rental',
            'From Savings',
            'From Others'
        ]);
        $nationality = Nationality::select('Code','Description')->orderBy('Code')->get();
        $country = Country::select('Country')->orderBy('Country', 'ASC')->get();
        return view('master.customer', compact('nationality','mas_customer_type','company_type', 'identifier_type','general_type','employment_detail','yearly_income','sourceofincome','country'));
    }

    public function createcustomer(Request $request)
    {
       // printArray($request->all()); exit;      

        DB::beginTransaction();
        try {
            if($request->code!='') {
                // echo $request->incorporationdate;
                // die;
                $data = ["Custcode" => $request->code,
                                 "CustName" => isset($request->customer_name) ? $request->customer_name : '',
                                 "NatureOfbusiness" => isset($request->businessnature) ? $request->businessnature : '',
                                 "BusinessType" => isset($request->comptype) ? $request->comptype : '',
                                 "Active"   => isset($request->codeactive) ? 1 : 0,
                                 "BuySelltype"   =>  isset($request->buyselltype) ? (string)$request->buyselltype : '',
                                 "TypeOfCustomer" => isset($request->custype) ? implode(',', $request->custype) : '',
                                 "MCType" => isset($request->mascustomertype) ? $request->mascustomertype : '',
                                 "HighRisk" => isset($request->highrisk) ? 1 : 0,
                                 //"NameOfcompany" => isset($request->company_name) ? $request->company_name : '',
                                 "CompanyRegNo" => isset($request->company_regn_no) ? $request->company_regn_no : '',
                                 "IncorporationDate" => isset($request->incorporationdate) ? $request->incorporationdate : '',
                                 "IncorporationPlace" => isset($request->company_place) ? $request->company_place : '',
                                 "MoneyChangerLicense" => isset($request->company_license) ? $request->company_license : '',
                                 "AnnualTurnOver" => isset($request->company_turn_over) ? $request->company_turn_over : '0',
                                 "LicenseValidFrom" => isset($request->company_valid_from) ? $request->company_valid_from : '',
                                 "LicenseValidTo" => isset($request->company_valid_till) ? $request->company_valid_till : '',
                                 "IssuingAuthority" => isset($request->company_issuing_authority) ? $request->company_issuing_authority : '',
                                 "OverSeasBranches" => isset($request->company_overseas) ? 1 : 0,
                                 "CompPostalCode" => isset($request->company_postal) ? $request->company_postal : '',
                                 "CompAddress1" => isset($request->company_address1) ? $request->company_address1 : '',
                                 "CompAddress2" => isset($request->company_address2) ? $request->company_address2 : '',
                                 "Country" => isset($request->company_country) ? $request->company_country : '',
                                 "CompPhone1" => isset($request->company_phone1) ? $request->company_phone1 : '',
                                 "CompPhone2" => isset($request->company_phone2) ? $request->company_phone2 : '',
                                 "CompFax" => isset($request->company_fax) ? $request->company_fax : '',
                                 "CompEmail" => isset($request->company_email) ? $request->company_email : '',
                                 "Remarks" => isset($request->company_remarks) ? $request->company_remarks : '',
                                 "CheckList" => isset($request->company_check_list) ? implode(',', $request->company_check_list) : '',
                                 "Name" => isset($request->company_contact) ? $request->company_contact : '',
                                 "NRICNo" => isset($request->company_nric_no) ? $request->company_nric_no : '',
                                 "TypeOfIdentification" => isset($request->identifiertype) ? $request->identifiertype : '',
                                 "Nationality" => isset($request->nationality) ? $request->nationality : '',
                                 "PostalCode" => isset($request->postalcode) ? $request->postalcode : '',
                                 "Address1" => isset($request->address1) ? $request->address1 : '',
                                 "Address2" => isset($request->address2) ? $request->address2 : '',
                                 "Address3" => isset($request->overseascountry) ? $request->overseascountry : '',
                                 "UnitNo"  =>  isset($request->UnitNo) ? $request->UnitNo : '',
                                 "UnitNoRepresentative"  =>  isset($request->unit_no) ? $request->unit_no : '',
                                 "Phone" => isset($request->phone) ? $request->phone : '',
                                 "Fax" => isset($request->fax) ? $request->fax : '',
                                 "HandPhone" => isset($request->mobile) ? $request->mobile : '',
                                 "Email" => isset($request->emailcomps) ? (string)$request->emailcomps : '',
                                 "DOB" => isset($request->dob) ? $request->dob : '',
                                 "DOE" => isset($request->doe) ? $request->doe : '',
                                 "Position" => isset($request->position) ? $request->position : '',
                                 "EmploymentDetail" => isset($request->employmentdetail) ? $request->employmentdetail : '',
                                 "YearlyIncome" => isset($request->yearlyincome) ? $request->yearlyincome : '',
                                 "SourceOfIncome" => isset($request->sourceofincome) ? $request->sourceofincome : '',
                                 "CompanySourceOfFunds" => isset($request->sourceofincome) ? $request->sourceofincome : '',
                                 "CreatedDate" => now(),
                                 "Agent" => $request->agent,
                                 "Licenced" => $request->licence,
                                ];
                $contactdata = ["CustCode"=>$request->code,
                                "ContactName"=>$request->company_contact,
                                "ContactNricNo"=>$request->company_nric_no,
                                "ContactNationality"=>$request->nationality,
                                "DOB"=>$request->dob,
                                "ContactType"=>'Owners',
                                "ContactPosition"=>$request->position,
                                "ContactSignature"=>"",
                                "Active"=>1,
                                "DOE"=>isset($request->doe) ? $request->doe : '',
                                "CreatedBy"=>Auth::user()->username,
                                "ModifiedBy"=>Auth::user()->username,
                                "ModifiedDate"=>now()
                                ];
                if(!empty($request->optype) && $request->optype=='update') {
                    $cdatadetails = Customer::where('Custcode',$request->code)->first();
                    if($cdatadetails) {
                        $customerid = $cdatadetails->Id;
                        unset($data['Custcode']);    
                        Customer::where('Id',$customerid)->update($data);
                        /*printArray($data);die; */ 
                        $status = '200';
                        $msg = 'Customer Details updated successfully!';
                        // $request->session()->flash('message.level', 'success');
                        // $request->session()->flash('message.content', 'Customer Details updated successfully!');
                    }
                } else {
                    Customer::create($data); 
                    CustomerContact::create($contactdata);                   
                    $status = '200';
                    $msg = 'Customer Details inserted successfully!';
                    // $request->session()->flash('message.level', 'success');
                    // $request->session()->flash('message.content', 'Customer Details inserted successfully!');   
                }   

                DB::commit();      //Insert into location_customer table
                $alllocations = Location::get();
                if(count($alllocations) > 0 && $request->optype!='update') {
                    try {
                        foreach($alllocations as $location) {
                            $locationcustomerdata = ["Custcode" => $request->code,
                                                     "CustName" => isset($request->customer_name) ? $request->customer_name : '',
                                                     "Address1" => isset($request->address1) ? $request->address1 : '',
                                                     "Address2" => isset($request->address2) ? $request->address2 : '',
                                                     "Address3" => isset($request->overseascountry) ? $request->overseascountry : '',
                                                     "Phone" => isset($request->phone) ? $request->phone : '',
                                                     "Fax" => isset($request->fax) ? $request->fax : '',
                                                     "Balance" => 0.00,
                                                     "Name" => isset($request->company_contact) ? $request->company_contact : '',
                                                     "NRICNo" => isset($request->company_nric_no) ? $request->company_nric_no : '',
                                                     "Ob" => 0.00,
                                                     "CreditLimit" => 0.00,
                                                     "Remarks" => isset($request->company_remarks) ? $request->company_remarks : '',
                                                     "TTCustomer" => 0,
                                                     "RemittanceLicensee" => 0,
                                                     "Country" => isset($request->company_country) ? $request->company_country : '',
                                                     "Location" => $location->LocationCode,
                                                     "MoneyChangerLicense" => isset($request->company_license) ? $request->company_license : '',
                                                     "TradingLicense" => '',
                                                     "DOB" => isset($request->dob) ? $request->dob : '',
                                                     "DOE" => isset($request->doe) ? $request->doe : '',
                                                     "MCType" => isset($request->mascustomertype) ? $request->mascustomertype : '',
                                                     "TTType" => '',
                                                     "DailyCreditLimit" => 0.00,
                                                     "DealBalance" => 0.00,
                                                     "CreatedBy" => Auth::user()->username,
                                                     "CreatedDate" => now(),
                                                     "ModifiedBy"=>Auth::user()->username,
                                                     "ModifiedDate"=>now(),
                                                     "WebCustAllowed" => 0,
                                                     "WebCustCode" => '',
                                                     "Active"   => isset($request->codeactive) ? 1 : 0,
                                                     "HighRisk" => isset($request->highrisk) ? 1 : 0,
                                                    ];
                            LocationCustomer::create($locationcustomerdata);    
                        }
                    } catch(ValidationException $e)
                    {
                        DB::rollback();
                        // $request->session()->flash('message.level', 'danger');
                        // $request->session()->flash('message.content', $e->getMessage());
                        $status = '201';
                        $msg = $e->getMessage();
                    } catch(\Exception $e)
                    {
                        DB::rollback();
                        // $request->session()->flash('message.level', 'danger');
                        // $request->session()->flash('message.content', $e->getMessage());
                        $status = '201';
                        $msg = $e->getMessage();
                    }

                }   
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            // echo $e->getMessage();die;
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
            $status = '201';
            $msg = $e->getMessage();
        } catch(\Exception $e)
        {
            DB::rollback();
            // echo $e->getMessage();die;
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
            $status = '201';
            $msg = $e->getMessage();
        }



        //$sessdata = session()->all();
        //printArray($datadd); exit;

        //if($sessdata['message']['level']!="success") {
            //return Redirect::back()->withInput(Input::all());
        //} else {
            // return redirect('customer_master');
            return response()->json(['status' => $status, 'content' => $msg]);
        //}

    }    

    public function addContact(Request $request)
    {
        /*printArray($request->all());die;*/
        DB::beginTransaction();
        try {
          if($request->code!='') {                      
                /*CustomerContact::where('CustCode',$request->code)->delete();
                if(count($request->ctype) > 0)
                {
                    for($c=0;$c<count($request->ctype);$c++) {*/
                        if($request->contactactive=='Yes') {
                            $cactive = 1;
                        } else {
                            $cactive = 0;
                        }
                        $contactdata = ["CustCode"=>$request->code,
                                        "ContactName"=>$request->cusname,
                                        "ContactNricNo"=>$request->nricno,
                                        "ContactNationality"=>$request->contactnationality,
                                        "DOB"=>$request->contactdob,
                                        "ContactType"=>$request->contacttype,
                                        "ContactPosition"=>$request->contactposition,
                                        "ContactSignature"=>"",
                                        "Active"=>$cactive,
                                        "Remarks"=>$request->contactremarks,
                                        "DOE"=>$request->doe,
                                        "CreatedBy"=>Auth::user()->username,
                                        "ModifiedBy"=>Auth::user()->username,
                                        "ModifiedDate"=>now()
                                        ];
                        CustomerContact::create($contactdata);    
                        $status = '200';
                        $msg = 'Contact Details inserted successfully!';         
                    /*}
                }  */
            }  
            DB::commit();     
        } catch(ValidationException $e)
        {
            DB::rollback();
            $status = '201';
            $msg = $e->getMessage();
        } catch(\Exception $e)
        {
            DB::rollback();
            $status = '201';
            $msg = $e->getMessage();
        }

        return response()->json(['status' => $status, 'content' => $msg]);
    }

    public function fetchCustomer(Request $request)
    {
        $datas = Customer::select("Custcode")->where("Custcode","LIKE","%{$request->data}%")->get();
        if($datas){
          return response()->json(['status' => '200', 'data' => $datas]);
        } else {
          return response()->json(['status' => '201',]);
        }
    }

    public function getCustomerDetails(Request $request)
    {
        $loccode = Auth::user()->Location;
        if(!empty($request->code)) {
            $currdata = Customer::where('Custcode',$request->code)->first();
            if($currdata) {
              $currdata = $currdata->toArray();
            }    

            $customerlocationdetails = LocationCustomer::where('Custcode',$request->code)->where('Location',$loccode)->pluck('Balance')->all();
            $customerlocationdetails1 = LocationCustomer::where('Custcode',$request->code)->where('Location',$loccode)->pluck('DealBalance')->all();
            $cusbalance = !empty($customerlocationdetails[0]) ? $customerlocationdetails[0] : '0.00';
            $cusdealbalance = !empty($customerlocationdetails1[0]) ? $customerlocationdetails1[0] : '0.00';

            /*$contactdata = CustomerContact::where('Custcode',$request->code)->get();     
            if(count($contactdata) > 0) {
                $constring = "<input type='hidden' name='contactcount' id='contactcount' value='".count($contactdata)."' />";
                $m = 1;
                foreach ($contactdata as $condata) {
                    if($condata->Active=='1') {
                        $activeflag = 'Yes';
                    } else {
                        $activeflag = 'No';
                    }
                    $constring .= "<tr id='cid".$m."' class='contactcls'><td>".$m."</td><input type='hidden' value='".$condata->ContactType."' name='ctype[]'><td><input type='hidden' value='".$condata->ContactName."' name='nriccustomername[]'>".$condata->ContactName."</td><td><input type='hidden' value='".$condata->ContactNricNo."' name='nricnumber[]'>".$condata->ContactNricNo."</td><td><input type='hidden' value='".$condata->ContactNationality."' name='cnationality[]'>".$condata->ContactNationality."</td><td><input type='hidden' value='".$condata->DOB."' name='cdob[]'>".$condata->DOB."</td><td><input type='hidden' value='".$condata->ContactPosition."' name='cposition[]'>".$condata->ContactPosition."</td><td></td><td><input type='hidden' value='".$condata->Active."' name='cactive[]'>".$activeflag."</td><td><input type='hidden' value='".$condata->Remarks."' name='cremarks[]'>".$condata->Remarks."</td><td><input type='hidden' value='".$condata->DOE."' name='cdoe[]'>".$condata->DOE."</td></tr>";
                    $m++;
                }
                $currdata['cuscontactdata'] = $constring;
            }
            $Benificierydata = BenificieryDetails::where('Custcode',$request->code)->get();
            if(count($Benificierydata) > 0) {
                $constring1 = "<input type='hidden' name='delbeneficiaryclsid' id='delbeneficiaryclsid' value='".count($Benificierydata)."' />";
                $m = 1;
                foreach ($Benificierydata as $key => $value) {
                    $constring1 .="<tr id='bid".$m."' class='beneficiarycls'><td>".$m."</td><td><input type='hidden' value='".$value->beneficiary_id."' name='td_beneficiary_id[]'>".$value->beneficiary_id."</td><td><input type='hidden' value='".$value->CustCode."' name='td_custcode[]'>" . $value->CustCode . "</td><td><input type='hidden' value='".$value->currency_code."' name='td_CurrencyCode[]'>" . $value->currency_code . "</td><td><input type='hidden' value='".$value->CustNRICNO."' name='td_CustNRICNO[]'>" . $value->CustNRICNO . "</td><td><input type='hidden' value='".$value->BeneName."' name='td_BeneName[]'>" . $value->BeneName . "</td><td><input type='hidden' value='".$value->BeneBankName."' name='td_BeneBankName[]'>" . $value->BeneBankName . "</td><td><input type='hidden' value='".$value->BeneBankAccNo."' name='td_BeneBankAccNo[]'>" . $value->BeneBankAccNo . "</td><td><input type='hidden' value='".$value->BeneAddress1."' name='td_BeneAddress1[]'>" . $value->BeneAddress1 . "</td><td><input type='hidden' value='".$value->BeneAddress2."' name='td_BeneAddress2[]'>" . $value->BeneAddress2 . "</td><td><input type='hidden' value='".$value->BeneCountry."' name='td_BeneCountry[]'>" . $value->BeneCountry . "</td><td><input type='hidden' value='".$value->BeneMobileNo."' name='td_BeneMobileNo[]'>" . $value->BeneMobileNo . "</td><td><input type='hidden' value='".$value->BeneBankBranch."' name='td_BeneBankBranch[]'>" . $value->BeneBankBranch . "</td><td><input type='hidden' value='".$value->SwiftCode."' name='td_SwiftCode[]'>" . $value->SwiftCode . "</td><td><input type='hidden' value='".$value->Active."' name='td_beneficiaryactive[]'>" . $value->Active . "</td><td><input type='hidden' value='".$value->purpose."' name='td_purpose[]'>" . $value->purpose . "</td></tr>";
                    $m++;
                }
                $currdata['benificiery'] = $constring1;
            }*/

            $dates = \Carbon\Carbon::parse($currdata['CreatedDate']); 
            $currdata['Createdat'] = $dates->format('d-m-Y');
            $currdata['customerbalance'] = currency_format($cusbalance,2);
            $currdata['cusdealbalance'] = currency_format($cusdealbalance,2);
            return $currdata;       

        }
    }


    public function deleteCustomer(Request $request)
    {
        $analysis_buysell = BuySell::where('CustCode', $request->ccode)->get();
        if(!$analysis_buysell->isEmpty()){
            return response()->json(['status' => '201', 'content'=>'This Customer using Transaction']);
        } else {
            Customer::where('Custcode', $request->ccode)->delete();
            CustomerContact::where('CustCode', $request->ccode)->delete();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Customer Deleted successfully!'); 
            return response()->json(['status' => '200']);
        }
    }

    public function getcontact(Request $request)
    {
        $cuscontactdata = [];
        $contactdata = CustomerContact::where('Custcode',$request->code)->get();     
        if(count($contactdata) > 0) {
            $constring = "<input type='hidden' name='contactcount' id='contactcount' value='".count($contactdata)."' />";
            $m = 1;
            foreach ($contactdata as $condata) {
                if($condata->Active=='1') {
                    $activeflag = 'Yes';
                } else {
                    $activeflag = 'No';
                }
                $constring .= "<tr id='cid".$m."' class='contactcls'><td>".$m."</td><input type='hidden' value='".$condata->ContactType."' name='ctype[]'><td><input type='hidden' value='".$condata->ContactName."' name='nriccustomername[]'>".$condata->ContactName."</td><td><input type='hidden' value='".$condata->ContactNricNo."' name='nricnumber[]'>".$condata->ContactNricNo."</td><td><input type='hidden' value='".$condata->ContactNationality."' name='cnationality[]'>".$condata->ContactNationality."</td><td><input type='hidden' value='".$condata->DOB."' name='cdob[]'>".$condata->DOB."</td><td><input type='hidden' value='".$condata->ContactPosition."' name='cposition[]'>".$condata->ContactPosition."</td><td></td><td><input type='hidden' value='".$condata->Active."' name='cactive[]'>".$activeflag."</td><td><input type='hidden' value='".$condata->Remarks."' name='cremarks[]'>".$condata->Remarks."</td><td><input type='hidden' value='".$condata->DOE."' name='cdoe[]'>".$condata->DOE."</td></tr>";
                $m++;
            }
            $cuscontactdata = $constring;
            return  response()->json(['status' => '200', 'cuscontactdata' => $cuscontactdata]);
        }
            return  response()->json(['status' => '201']);
    }

    public function list(Request $request)
    {
        $nationality = Nationality::select('Code','Description')->orderBy('Code')->get();
        $country = Country::select('Country')->orderBy('Country', 'ASC')->get();
        $general_type = collect([
            'Local Trading',
            'Overseas Trading'
        ]);
        /*return view('master.customer', compact('nationality','mas_customer_type','company_type', 'identifier_type','general_type','employment_detail','yearly_income','sourceofincome','country'));*/
        if ($request->ajax()) {
            $datas = Customer::with('CLocation')->with('Nationalitys');
            if($request->code != '' ){
                $datas = $datas->where('Custcode', $request->code)->orWhere('CustName', 'like', '%'.$request->code.'%')->orWhere('NameOfcompany', 'like', '%'.$request->code.'%'); 
            }
            if($request->type != '' ){
                $datas = $datas->where('BuySelltype', $request->type); 
            }
            if($request->active != ''){
                $active = '0';    
                if($request->active == 'active'){
                    $active = '1';    
                }
                $datas = $datas->where('Active', $active);
            }
            $datas = $datas->orderBy('id','desc')->get();
            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->Custcode;
                })->addColumn('bal', function ($datas) {
                    return currency_format($datas->CLocation[0]->Balance,2);
                })->addColumn('nationalitydesc', function ($datas) {
                    return $datas->Nationalitys->Description;
                })->addColumn('DOB', function ($datas) {
                    if($datas->DOB != ''){
                        return date("d/m/Y", strtotime($datas->DOB) );
                    } else {
                        return '';
                    }
                })->addColumn('Action', function ($datas) {
                    $img1 = env('APP_URL').'/assets/images/contact.png';
                    $img2 = env('APP_URL').'/assets/images/beneficiary.png';
                    if(env('ENABLE_DD_TT') == 1){
                        $hidden = '';
                    } else {
                        $hidden = 'hidden';
                    }
                    return '<div class="row"><div class="col-md-6" style="cursor: pointer;"><a class="text-success mr-2 addContact" data-ref="'.$datas->Custcode.'"><img src="'.$img1.'"></a></div><div class="col-md-6" style="cursor: pointer;" '.$hidden.'><a class="text-success mr-2 addBeneficiary" data-custcode="'.$datas->Custcode.'" data-ref="'.$datas->NRICNo.'"><img src="'.$img2.'"></a></div></div>';
                })->escapeColumns([])
                ->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
        return view('master.customer_list', compact('nationality','country','general_type'));
    }

    public function beneficiary_list(Request $request)
    {
        if ($request->ajax()) {
            $datas = BenificieryDetails::where('CustCode', $request->ccode);
            if($request->code != '' ){
                $datas = $datas->Where('BeneName', 'like', '%'.$request->code.'%')->orWhere('BeneBankAccNo', 'like', '%'.$request->code.'%'); 
            }
            if($request->active != ''){
                $active = 'Inactive';    
                if($request->active == 'active'){
                    $active = 'Active';    
                }
                $datas = $datas->where('Active', $active);
            }
            $datas = $datas->get();
            return Datatables::of($datas)
                ->addIndexColumn()
                ->setRowId(function ($datas) {
                    return $datas->id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->make(true);
        }
    }

    public function customerExport(Request $request)
    {
        if($request->export == 'CSV'){
            $datas = Customer::with('CLocation');
            if($request->data != '' ){
                $datas = $datas->where('Custcode', $request->data)->orWhere('CustName', 'like', '%'.$request->data.'%')->orWhere('NameOfcompany', 'like', '%'.$request->data.'%'); 
            }
            $active = '0';    
            if(isset($request->codeactive) && $request->codeactive != ''){
                if($request->codeactive == 'active'){
                    $active = '1';    
                }
            }
            $datas = $datas->where('Active', $active);
            $datas = $datas->get();
            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=customerlist.csv",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );
            $columns = array('Custcode', 'NameOfcompany', 'TypeOfCustomer', 'DOB', 'Nationality', 'Phone', 'bal', 'Address1','Address2');

            $callback = function() use ($datas, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);
                foreach($datas as $value) {
                    if($value->DOB != ''){
                        $dob = (string)date("d/m/Y", strtotime(str_replace('-','/',$value->DOB)));
                    } else{
                        $dob = '';
                    }
                    fputcsv($file, array($value->Custcode, $value->NameOfcompany,$value->TypeOfCustomer, $dob, $value->Nationality, $value->Phone, currency_format($value->CLocation[0]->Balance,2), $value->Address1,$value->Address2));
                }
                fclose($file);
            };
            return Response::stream($callback, 200, $headers);
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'No data available');
            return Redirect::back();
        }
    }

    public function addCustBeneficiary(Request $request)
    {
        DB::beginTransaction();
        try {
            if($request->addnric_no){
                /*BenificieryDetails::where('CustNRICNO',$request->addnric_no)->delete();
                for($c=0;$c<count($request->td_beneficiary_id);$c++) {*/
                    $maxid = BenificieryDetails::max('id');
                    $beneId= 'B'.str_pad($maxid+1, 6, '0', STR_PAD_LEFT);
                    $data = [
                        "beneficiary_id" => $beneId,
                        "CustCode" => $request->custcode,
                        "CustNRICNO" => $request->addnric_no,
                        "BeneName" => $request->BeneName,
                        "BeneBankName" => $request->BeneBankName,
                        "BeneBankAccNo" => $request->BeneBankAccNo,
                        "BeneAddress1" => $request->BeneAddress1,
                        "BeneAddress2" => $request->BeneAddress2,
                        "BeneCountry" => $request->BeneCountry,
                        "BeneMobileNo" => $request->BeneMobileNo,
                        "BeneBankBranch" => $request->BeneBankBranch,
                        "SwiftCode" => $request->SwiftCode,
                        "Active" => $request->beneficiaryactive,
                        "currency_code" => $request->CurrencyCode,
                    ];
                    BenificieryDetails::create($data);
                        $status = '200';
                        $msg = 'Beneficiary Details inserted successfully!';        
                /*}*/
            } 
            DB::commit();
        }catch(ValidationException $e)
        {
            DB::rollback();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
            $status = '201';
            $msg = $e->getMessage();
        } catch(\Exception $e)
        {
            DB::rollback();
            // $request->session()->flash('message.level', 'danger');
            // $request->session()->flash('message.content', $e->getMessage());
            $status = '201';
            $msg = $e->getMessage();
        }
        return  response()->json(['status' => $status, 'content' => $msg]);
    }

    public function getCustBeneficiary(Request $request)
    {
        $maxid = BenificieryDetails::max('id');
        $beneId= 'B'.str_pad($maxid+1, 6, '0', STR_PAD_LEFT);
        /*$Benificierydata = BenificieryDetails::where('CustCode',$request->custCode)->get();*/
        $Benificierydata = BenificieryDetails::where('CustCode',$request->custCode)->where('CustNRICNO',$request->nric_no)->get();
        /*printArray($Benificierydata);die;*/
        if(count($Benificierydata) > 0) {
            $constring1 = "<input type='hidden' name='delbeneficiaryclsid' id='delbeneficiaryclsid' value='".count($Benificierydata)."' />";
            $m = 1;
            foreach ($Benificierydata as $key => $value) {
                $constring1 .="<tr id='bid".$m."' class='beneficiarycls'><td>".$m."</td><td><input type='hidden' value='".$value->beneficiary_id."' name='td_beneficiary_id[]'>".$value->beneficiary_id."</td><td><input type='hidden' value='".$value->CustCode."' name='td_custcode[]'>" . $value->CustCode . "</td><td><input type='hidden' value='".$value->currency_code."' name='td_CurrencyCode[]'>" . $value->currency_code . "</td><td><input type='hidden' value='".$value->CustNRICNO."' name='td_CustNRICNO[]'>" . $value->CustNRICNO . "</td><td><input type='hidden' value='".$value->BeneName."' name='td_BeneName[]'>" . $value->BeneName . "</td><td><input type='hidden' value='".$value->BeneBankName."' name='td_BeneBankName[]'>" . $value->BeneBankName . "</td><td><input type='hidden' value='".$value->BeneBankAccNo."' name='td_BeneBankAccNo[]'>" . $value->BeneBankAccNo . "</td><td><input type='hidden' value='".$value->BeneAddress1."' name='td_BeneAddress1[]'>" . $value->BeneAddress1 . "</td><td><input type='hidden' value='".$value->BeneAddress2."' name='td_BeneAddress2[]'>" . $value->BeneAddress2 . "</td><td><input type='hidden' value='".$value->BeneCountry."' name='td_BeneCountry[]'>" . $value->BeneCountry . "</td><td><input type='hidden' value='".$value->BeneMobileNo."' name='td_BeneMobileNo[]'>" . $value->BeneMobileNo . "</td><td><input type='hidden' value='".$value->BeneBankBranch."' name='td_BeneBankBranch[]'>" . $value->BeneBankBranch . "</td><td><input type='hidden' value='".$value->SwiftCode."' name='td_SwiftCode[]'>" . $value->SwiftCode . "</td><td><input type='hidden' value='".$value->Active."' name='td_beneficiaryactive[]'>" . $value->Active . "</td><td><input type='hidden' value='".$value->purpose."' name='td_purpose[]'>" . $value->purpose . "</td></tr>";
                $m++;
            }
            $benificiery = $constring1;
            return  response()->json(['status' => '200', 'benificiery' => $benificiery, 'beneId' => $beneId]);
        }
            return  response()->json(['status' => '201', 'beneId' => $beneId]);
    }
}
