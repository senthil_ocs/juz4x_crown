<?php

namespace App\Http\Controllers\model;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Nationality;
use App\SettlementMode;
use App\Customer;
use App\Currency;
use App\CashCustomer;
use App\Location;
use App\CustomerContact;
use App\User;
use Auth;
use App\Profile;
use App\LocationCustomer;
use App\Beneficiary;
use App\BenificieryDetails;
use DB;

class GetmodalDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getdetails(Request $request)
    {
	   $locationCode = Auth::user()->Location;
       $active = '1';         
        if ($request->ajax() && $request->type == 'customer') {
            $data = LocationCustomer::where('Location',$locationCode)->where('Active',$active)->with('customer')->get();
    		// $data = Customer::orderBy('Custcode')->get();
    		return Datatables::of($data)
    			->addIndexColumn()
    			->addColumn('checkbox', function ($data) {
    				return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="customer_id_detail" id="customer_id_detail" value="'.$data->Custcode.'" data-ref="'.$data->CustName.'" data-curbal="'.currency_format($data->Balance,2).'"><span class="checkmark"></span></label>';
  				})
    			->setRowId(function ($data) {
                    return $data->Custcode;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
    	} else if ($request->ajax() && $request->type == 'currency') {
            // $data = Currency::orderBy('CurrencyCode')->get();
             $data = DB::table('currency_master')
            ->leftJoin('location_currency', 'location_currency.CurrencyCode', '=', 'currency_master.CurrencyCode')
            ->where('location_currency.LocationCode', '=', $locationCode)
            ->select('location_currency.CurrencyCode','location_currency.AvgCost','location_currency.SellRate','location_currency.BuyRate','location_currency.Stock','currency_master.CurrencyName')
            ->get()->toArray();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    $trData = $data->CurrencyCode.'|'.$data->CurrencyName.'|'.$data->AvgCost.'|'.$data->Stock;
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="currency_id_detail" id="currency_id_detail" value="'.$trData.'" data-ref="'.$data->CurrencyName.'"><span class="checkmark"></span></label>';
                })
                ->setRowId(function ($data) {
                    return $data->CurrencyCode;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } elseif ($request->ajax() && $request->type == 'cashcustomer') {
            $data = CashCustomer::orderBy('PPNo')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="cashcustomer_id_detail" id="cashcustomer_id_detail" value="'.$data->PPNo.'" data-ref="'.$data->CustCode.'" data-name="'.$data->Name.'"><span class="checkmark"></span></label>';
                })
                ->setRowId(function ($data) {
                    return $data->PPNo;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } elseif ($request->ajax() && $request->type == 'location') {
            $data = Location::orderBy('LocationCode')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="location_id_detail" id="location_id_detail" value="'.$data->LocationCode.'" data-location-name="'.$data->LocationName.'"><span class="checkmark"></span></label>';
                })
                ->setRowId(function ($data) {
                    return $data->LocationCode;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } elseif ($request->ajax() && $request->type == 'customercontacts') {
            $data = CustomerContact::where('CustCode',$request->id)->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    $trData = $data->ContactNricNo.'|'.$data->Remarks.'|'.$data->DOE.'|'.$data->ContactNationality;
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="ccontact_id_detail" id="ccontact_id_detail" value="'.$trData.'"><span class="checkmark"></span></label>';
                })
                ->setRowId(function ($data) {
                    return $data->ContactNricNo;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } elseif ($request->ajax() && $request->type == 'user') {
            $data = User::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="user_id_detail" id="user_id_detail" value="'.$data->username.'"><span class="checkmark"></span></label>';
                })
                ->setRowId(function ($data) {
                    return $data->username;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } elseif ($request->ajax() && $request->type == 'profile') {
            $data = Profile::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="profile_id_detail" id="profile_id_detail" value="'.$data->ProfileId.'""><span class="checkmark"></span></label>';
                })
                ->setRowId(function ($data) {
                    return $data->ProfileId;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } elseif ($request->ajax() && $request->type == 'beneficiary') {
            /*if($request->customer_code == 'CASH')
            {
                $data = BenificieryDetails::where('CustCode', $request->customer_code)->where('CustNRICNO', $request->pno_nric)->get();
            } else {
                $data = BenificieryDetails::where('CustCode', $request->customer_code)->where('CustNRICNO', $request->pno_nric)->get();
            }*/
            $data = BenificieryDetails::where('CustCode', $request->customer_code)->where('CustNRICNO', $request->pno_nric)->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    $trData = $data->beneficiary_id.'|'.$data->BeneName.'|'.$data->CustNRICNO;
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="beneficiary_id_detail" id="beneficiary_id_detail" value="'.$trData.'""><span class="checkmark"></span></label>';
                })
                ->setRowId(function ($data) {
                    return $data->beneficiary_id;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } else if ($request->ajax() && $request->type == 'agent') {
            $data = DB::table('location_customer')
                ->leftJoin('customer_master', 'location_customer.CustCode', '=', 'customer_master.CustCode')
                ->whereNotIn('customer_master.CustCode', [$request->ccode])
                ->where('location_customer.Location', $locationCode)
                ->where('customer_master.Agent', $active)
                ->where('customer_master.Active', $active)
                ->get();
                /*printArray($data);die;*/
                /*$data = LocationCustomer::where('Location',$locationCode)->where('Active',$active)->where('Agent','1')->with('customer')->get();*/
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="agent_id_detail" id="agent_id_detail" value="'.$data->Custcode.'|'.$data->CustName.'|'.$data->MCType.'" data-ref="'.$data->CustName.'"><span class="checkmark"></span></label>';
                })
                ->setRowId(function ($data) {
                    return $data->Custcode;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } else if ($request->ajax() && $request->type == 'ledger') {
            $data = DB::table('ledgerRemark')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) use($request) {
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="ledger_remark" id="ledger_remark_'.$request->name.'" value="'.$data->Name.'"><span class="checkmark"></span></label>';
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } else if ($request->ajax() && $request->type == 'ledgercustomer') {
            $data = LocationCustomer::where('Location',$locationCode)->where('Active',$active)->with('customer')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) use($request) {
                        return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="ledger_customer" id="ledger_customer_'.$request->name.'" value="'.$data->Custcode.'" data-ref="'.$data->CustName.'" data-curbal="'.currency_format($data->Balance,2).'"><span class="checkmark"></span></label>';
                })
                ->setRowId(function ($data) {
                    return $data->Custcode;
                })->setRowClass(function () {
                    return 'tableclicked';
                })->rawColumns(['checkbox'])->make(true);
        } else if ($request->ajax() && $request->type == 'ledgercurrency'){
             $data = DB::table('currency_master')
            ->leftJoin('location_currency', 'location_currency.CurrencyCode', '=', 'currency_master.CurrencyCode')
            ->where('location_currency.LocationCode', $locationCode)
            ->where('currency_master.CurrencyCode','SGD')
            ->select('location_currency.CurrencyCode','location_currency.AvgCost','location_currency.SellRate','location_currency.BuyRate','location_currency.Stock','currency_master.CurrencyName')
            ->get()->toArray(); 
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    $trData = $data->CurrencyCode.'|'.$data->CurrencyName.'|'.$data->AvgCost.'|'.$data->Stock; 
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="currency_id_detail" id="currency_id_detail" value="'.$trData.'" data-ref="'.$data->CurrencyName.'"><span class="checkmark"></span></label>';
            })      
            ->setRowId(function ($data) {
                return $data->CurrencyCode;
            })->setRowClass(function () {
                return 'tableclicked';
            })->rawColumns(['checkbox'])->make(true);
        } else if ($request->ajax() && $request->type == 'orginator'){
             $data = DB::table('orginator')
            ->leftJoin('benificiery_details', 'benificiery_details.id', '=', 'orginator.Beneficiary_id')
            ->where('benificiery_details.CustCode', '=',  $request->customer_code)
            ->where('benificiery_details.CustNRICNO', '=',  $request->pno_nric)
            ->where('benificiery_details.beneficiary_id', '=',  $request->pp_nric)
            ->select('orginator.Name','orginator.NRICNo', 'orginator.DOB', 'orginator.Nationality','orginator.Address', 'orginator.Code', 'orginator.id')
            ->get()->toArray();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    $trData = $data->Name.'|'.$data->NRICNo.'|'.$data->DOB.'|'.$data->Nationality.'|'.$data->Address.'|'.$data->Code.'|'.$data->id; 
                    return '<label class="checkbox checkbox-primary"><input type="checkbox" class="form-check-input" name="orginator_id_detail" id="orginator_id_detail" value="'.$trData.'" data-ref="'.$data->Name.'"><span class="checkmark"></span></label>';
            })      
            ->setRowId(function ($data) {
                return $data->Name;
            })->setRowClass(function () {
                return 'tableclicked';
            })->rawColumns(['checkbox'])->make(true);
        } 
    }
}
