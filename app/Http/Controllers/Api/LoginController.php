<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Location;
use App\LocationCustomer;
use App\CustomerContact;
use App\CashCustomer;
use App\Beneficiary;
use App\Device;
use DB;

class LoginController extends Controller
{
    public function login(Request $request)
    {
    	$nric     = $request->icno;
    	$custcode = $request->customer_code;
    	$type     = $request->type;
    	$mobile   = $request->mobile;

    	if($type=='company' && $mobile==''){
			return response()->json(["success"=>false, "message" =>'Invalid Details']);
		}
		if(isset($nric) && $type!='company'){
			$cust = CashCustomer::where('PPNo',$nric)->first();
			$customerType = 'cash';
		}else if($customerCode!='' && $mobile!=''){
			$customerType = 'company';
			$cust = DB::table('customer_master as cm')
                ->join('location_customer as lc', 'lc.Custcode', '=', 'cm.Custcode')
                ->where('cm.Custcode',$custcode)
                ->where('cm.NRICNo',$nric)
                ->where('lc.Location',$loccode)
                ->where('cm.Active',1)
                ->first();
		}
		if($cust){
			/* START  OTP SECTION */
				$otp = mt_rand(100000, 999999);
		        if(isset($otp)){
					$this->updatemembersOTP($otp,$nric,$customerType);
		        }
		        $otp_status = 1;
			/* 
				if($request->device !=''){
					$hashkey = 'KWyc2RthXGk';
					$otpMsg = $otp." is Your JC Remittance app OTP for Login session".$hashkey;
				}else{
					$otpMsg = $otp." is Your JC Remittance app OTP for Login";
				}
		        $post_data = array(
					'ID' => urlencode('102100002'),
					'Password' => urlencode('12345678a'),
					'Mobile' => urlencode($cust['HandPhone']),
					'Type'=>urlencode('A'),
					'Message'=>urlencode($otpMsg),
					'Sender'=>urlencode('JC Remittance')
				);
				$url = 'https://www.commzgate.net/gateway/SendMsg';
				$authStr ='BasicYXB0YXV0aDpMazlnYWNkazREOE1nOFdN';
				if($cust['CustMobileNO']!='' && $CustNRICNO!='TEST' && $CustNRICNO!='G8557196L' && $CustNRICNO!='S1643382I'){
					SendOTP($url,$post_data,$authStr);
				}
			*/
			/* END  OTP SECTION */
			/*$res = array(
				"CustomerId"	=>	$cust->Id,
				"Custcode"		=>	$cust->Custcode,
				"CustNRICNO"	=>	$cust->NRICNo,
				"CustName"		=>	$cust->CustName,
				"CustAddress1"  =>	$cust->Address1,
				"CustAddress2"	=>	$cust->Address2,
				"CustAddress3"	=>	$cust->Address3,
				"CustCountry"	=>	$cust->Nationality,
				"CustPostalCode"=>	$cust->PostalCode,
				"CustDOB"		=>	date("d-m-Y",strtotime($cust->DOB)),
				"CustMobileNO"  =>  $cust->HandPhone,
				"CompName"		=>	$cust->NameOfcompany,
				"IsCompany"	=>	$cust->IsCompany,
				"CompanyType"	=>	$cust->CompanyType,
				"Nationality"	=>	$cust->Nationality,
				"Location"		=>	$cust->Location,
				"Active"		=>	$cust->Active,
			);*/
			$res = array(
				"CustomerId"	=>	(string)$cust->id,
				"CustId"		=>	(string)$cust->id,
				"CustCode"		=>	(string)$cust->CustCode,
				"CustNRICNO"	=>	(string)$cust->PPNo,
				"CustName"		=>	$cust->Name,
				"CustAddress1"  =>	isset($cust->Address1) ? $cust->Address1 : '',
				"CustAddress2"	=>	isset($cust->Address2) ? $cust->Address2 : '',
				"CustAddress3"	=>	isset($cust->Address3) ? $cust->Address3 : '',
				"CustCountry"	=>	isset($cust->Address4) ? $cust->Address4 : '',
				"CustPostalCode"=>	isset($cust->PostalCode) ? $cust->PostalCode : '',
				"CustDOB"		=>	date("d-m-Y",strtotime($cust->DOB)),
				"CustMobileNO"  =>  $cust->PhoneNo,
				"CompName"		=>	isset($cust->NameOfcompany) ? $cust->NameOfcompany : '',
				/*"IsCompany"	=>	$cust->IsCompany,
				"CompanyType"	=>	$cust->CompanyType,*/
				"Nationality"	=>	$cust->Nationality,
				"Location"		=>	isset($cust->Location) ? $cust->Location : '',
				"Active"		=>	$cust->Activebit ==1 ? 'Active' : 'Inactive',
				"otp"			=>	(string)$otp,
				"otp_status"	=>	$otp_status,
				"CompanyType"   => "",
				"IsCompany"    => "0",
				"terms"        => "1"
			);
			return response()->json(["success"=>true, "details" =>$res]);
		} else {
			return response()->json(["success"=>false, "message" =>'Invalid Customer Details']);
		}
    }

    public function updatemembersOTP($otp,$nric,$type)
    {
    	if($otp){
    		if($type=='cash'){
    			CashCustomer::where('PPNo',$nric)->update([
	    			'Otp'=>$otp
	    		]);
    		}else{
	    		Customer::where('NRICNo',$nric)->update([
	    			'Otp'=>$otp
	    		]);
    		}
    	}
    }

    public function loginVerify(Request $request)
    {
    	$nric     = $request->icno;
    	$otp      = $request->otp;

    	if(isset($nric)){
			$loccode = env('USER_LOCATION');
			$cust = CashCustomer::where('PPNo',$nric)->where('Otp',$otp)->first();
			if($cust){
				$session_id = $this->getRandomNumbers();
				CashCustomer::where('PPNo',$nric)->where('Otp',$otp)->update(['OtpStatus'=>1,'session_id'=>$session_id]);
				//$session_id = $objMainApi->update_members_session($CustNRICNO,$postAry);
				//$beneDetails = Beneficiary::where('CustCode',$cust->CustCode)->where('CustNric',$cust->PPNo)->get();
				$beneDetails = Beneficiary::where('CustNric',$cust->PPNo)->get();

				$ben_details = [];
				if(count($beneDetails) > 0){
					foreach ($beneDetails as $key => $value) {
						$ben_details[] = array(
							"Beneid" 			=>  (string)$value->BeneId,
							"BeneCustCode"		=>	$value->CustCode,
							"BeneCustNRICNO"  	=>	$value->NRICNo,
							"BeneName"			=>	$value->Name,
							"BeneAddress1"		=>	$value->Address1,
							"BeneAddress2"  	=>	$value->Address2,
							"BeneCountry"		=>	$value->Country,
							"BeneBankName"		=>	$value->BankName,
							"BeneBankBranch"	=>	$value->BankBranch,
							"BeneBankAccNo"		=>	$value->BankAcNo,
							"BeneSwiftCode"		=>	$value->SwiftCode,
						);
					}
				}
				if($cust->DOB =='0000-00-00'){
					$custdob = '00-00-0000';
				} else{
					$custdob = date("d-m-Y",strtotime($cust->DOB));
				}
				$res = array(
					"CustomerId"	=>	(string)$cust->id,
					"CustId"		=>	(string)$cust->id,
					"CustCode"		=>	$cust->CustCode,
					"CustNRICNO"	=>	$cust->PPNo,
					"CustName"		=>	$cust->Name,
					"CustAddress1"  =>	isset($cust->Address1) ? $cust->Address1 : '',
					"CustAddress2"	=>	isset($cust->Address2) ? $cust->Address2 : '',
					"CustAddress3"	=>	isset($cust->Address3) ? $cust->Address3 : '',
					"CustCountry"	=>	isset($cust->Address4) ? $cust->Address4 : '',
					"CustPostalCode"=>	isset($cust->PostalCode) ? $cust->PostalCode : '',
					"CustDOB"		=>	date("d-m-Y",strtotime($cust->DOB)),
					"CustMobileNO"  =>  $cust->PhoneNo,
					"CompName"		=>	isset($cust->NameOfcompany) ? $cust->NameOfcompany : '',
					"IsCompany"		=>	isset($cust->IsCompany) ? $cust->IsCompany : '',
					"CompanyType"	=>	isset($cust->CompanyType) ? $cust->CompanyType : '',
					"Nationality"	=>	$cust->Nationality,
					"Location"		=>	isset($cust->Location) ? $cust->Location : '',
					"Active"		=>	$cust->Activebit ==1 ? 'Active' : 'Inactive',
					"Beneficiary Details" => 	$ben_details,
					"session_id"          =>    $session_id,
					"otp"			=>	'',
					"otp_status"	=>	'1',
				);
				return response()->json(["success"=>true, "details" =>$res]);
			} else {
				return response()->json(["success"=>false, "message" =>'Invalid Details']);
			}
			/*$cust = Customer::where('NRICNo',$nric)->where('Otp',$otp)->first();
			if($cust){
				Customer::where('NRICNo',$nric)->where('Otp',$otp)->update(['OtpStatus'=>1]);		
				$beneDetails = Beneficiary::where('CustCode',$cust->Custcode)->where('CustNric',$cust->NRICNo)->get();
				$ben_details = [];
				if($beneDetails){
					foreach ($beneDetails as $key => $value) {
						$ben_details[] = array(
								"Beneid" 			=>  $value->BeneId,
								"BeneCustCode"		=>	$value->CustCode,
								"BeneCustNRICNO"  	=>	$value->NRICNo,
								"BeneName"			=>	$value->Name,
								"BeneAddress1"		=>	$value->Address1,
								"BeneAddress2"  	=>	$value->Address2,
								"BeneCountry"		=>	$value->Country,
								"BeneBankName"		=>	$value->BankName,
								"BeneBankBranch"	=>	$value->BankBranch,
								"BeneBankAccNo"		=>	$value->BankAcNo,
								"BeneSwiftCode"		=>	$value->SwiftCode,
						);
					}
				}
				if($cust->DOB =='0000-00-00'){
					$custdob = '00-00-0000';
				} else{
					$custdob = date("d-m-Y",strtotime($cust->DOB));
				}
	        	$res = array(
						"CustomerId"	=>	$cust->Id,
						"Custcode"		=>	$cust->Custcode,
						"CustNRICNO"	=>	$cust->NRICNo,
						"CustName"		=>	$cust->CustName,
						"CustAddress1"  =>	$cust->Address1,
						"CustAddress2"	=>	$cust->Address2,
						"CustAddress3"	=>	$cust->Address3,
						"CustCountry"	=>	$cust->Nationality,
						"CustPostalCode"=>	$cust->PostalCode,
						"CustDOB"		=>	date("d-m-Y",strtotime($cust->DOB)),
						"CustMobileNO"  =>  $cust->HandPhone,
						"CompName"		=>	$cust->NameOfcompany,
						"IsCompany"	=>	$cust->IsCompany,
						"CompanyType"	=>	$cust->CompanyType,
						"Nationality"	=>	$cust->Nationality,
						"Location"		=>	$cust->Location,
						"Active"		=>	$cust->Active,
						"Beneficiary Details" => 	$ben_details,
					);
				return response()->json(["success"=>true, "details" =>$res]);
			} else {
				return response()->json(["success"=>false, "message" =>'Invalid Details']);	
			}*/
    	} else {
			return response()->json(["success"=>false, "message" =>'Invalid Details']);
		}	
    }
    public function getRandomNumbers($length=10){
		srand(date("s")); 
		$possible_charactors = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
		$string = ""; 
		while(strlen($string)<$length){ 
			$string .=substr($possible_charactors, rand()%((strlen($possible_charactors))),1); 
		} 
		return(uniqid($string)); 
	}

    public function appconfig(Request $request)
    {
    	$devBaseUrl = env('API_URL').'public/api/';
    	$settings = DB::table('company')->first();

		$apiConfig['base_url'] = array(
		    'development'  => $devBaseUrl,
		    'production'   => $devBaseUrl  
		);

		$apiConfig['settings'] = array(
		    'app_mode'        => 'production',
		    'app_store_link'  => '',
		    'play_store_link' => '',
		    'Transaction_limit' => $settings->MassLimit,
		    'Phone_Number'  => $settings->Phone,
		);

		$apiConfig['versions'] = array(
			'api' => array(
				'version' => '1',
				'text'   => 'API version',
			),

			'ios' => array(
				'version'      => '1',
				'text'         => 'one',
				'force_update' => false,
			),
			'android' => array(
				'version'  => '1',
				'text'      => 'one',
				'force_update' => false,
			)   
		);

		$apiConfig['apis'] = array(
		    'login' => array(
		        'development'    => 'login',
		        'production'     => 'login'
		    ),

		    'login_verify'          => array(
		        'development'    => 'login_verify',
		        'production'     => 'login_verify'
		    ),
		    'fetch_rate'            => array(
		        'development'    => 'rate',
		        'production'     => 'rate'
		    ),
		    'currency'              => array(
		        'development'    => 'currency',
		        'production'     => 'currency'
		    ),
		    'add_application'       => array(
		        'development'    => 'add_application',
		        'production'     => 'add_application'
		    ),
		    'application_image'     => array(
		        'development'    => 'application_image',
		        'production'     => 'application_image'
		    ),
		    'purpose'               => array(
		        'development'    => 'purpose',
		        'production'     => 'purpose'
		    ),
		    'get_cutomer'           =>array(
		        'development'    => 'get_cutomer',
		        'production'     => 'get_cutomer'
		    ),
		    'get_transactions'      => array(
		        'development'    => 'get_transactions',
		        'production'     => 'get_transactions'
		    ),
		    'bank' => array(
		        'development'    => 'bank',
		        'production'     => 'bank'
		    ),
		     'bank_accounts' => array(
		        'development'    => 'bank_accounts',
		        'production'     => 'bank_accounts'
		    ),
		    'device'      => array(
		        'development'    => 'device',
		        'production'     => 'device'
		    ),
		    'faq'      => array(
		        'development'    => 'faq',
		        'production'     => 'faq'
		    )
		);
			$qrcode['001'] = array('image_url' => $devBaseUrl.'images/boonlay.jpeg','uen_no'   =>'52867746B778',"Telephone"=>"6266 1817","Mobile"=>"93848701","address_1"=>"221 Boon Lay Shopping Centre","address_2"=>"#01-128 Boon Lay Place",
	"address_3"=>"Singapore 640221","email"=>"General@jcglobal.sg");

		$qrcode['002'] = array('image_url' => $devBaseUrl.'images/yishun1.jpg','uen_no'   => '52867746B',
                        "Telephone"=>"6455 1817","Mobile"=>"90619311","address_1"=>"NorthPoint City #B2-131","address_2"=>"1 NorthPoint Drive","address_3"=>"Singapore 768019","email"=>"Info@jcglobal.sg");
		$apiConfig['qrcode']=$qrcode;
		$apiConfig['paymentMsg']='Thank you we have received your remittance request.Your transaction will be processed after payment is received';

$eng = array("l1"=>"Request Submitted.","l2"=>"Please proceed to make payment.Your transaction will be processed after payment is received.","l3"=>"You may make payment using the following payment methods:");

$ch = array("l1"=>"申请已提交.","l2"=>"您的交易将在付款后处理.","l3"=>"您可以使用以下的付款方式：");
$apiConfig['paymentMsgNew']= array("eng"=>$eng,"ch"=>$ch);


$apiConfig['interval_time'] ="2";
$apiConfig['logout_time'] ="5";

$faq[] = array("p"=>"I undertake to advise JC Global within 30 days of any change in circumstances which causes the information previously provided to JC Global to become incorrect.","p1"=>"如果情况发生任何导致先前提供给JC Global的信息变得不正确的情况，我承诺在30天内向JC Global提供建议.");
$faq[] = array("p"=>"I confirm that the purpose for using the JC Global's remittance services are legitimate, that any moneys or funds concerned, handled or dealt with, are not proceeds from any criminal activity including money laundering, terroist financing and fraud.","p1"=>"我确认使用JC Global汇款服务的目的是合法的，有关，处理或处理的任何金钱或资金并非来自包括洗钱，恐怖分子融资和欺诈在内的任何犯罪活动的收益.");
$faq[] = array("p"=>"I consent to the collection, use, disclosure and processing of my personal data,whether directly or through a third party, for the purposes of complying with the applicable law, regulation or directive.","p1"=>"我同意出于遵守适用法律，法规或指令的目的而直接或通过第三方收集，使用，披露和处理我的个人数据.");
$faq[] = array("p"=>"I hereby declare that I am transacting for my own account and not on behalf of any other person or entity.","p1"=>"我特此声明，我是在为自己的帐户而不是代表任何其他个人或实体进行交易.");

$apiConfig['terms_msg']=$faq;
    	return response()->json(['success' => true, 'data' => $apiConfig, 'message' => 'Configuration']);
    }


    public function faq(Request $request)
    {
    	$faq[] = array("question"=>"How long does it takes for my funds to reach my beneficiary account?","answer"=>"All transactions should be transferred by the next working day. For example, if you remit on Monday, your funds should be transferred to your beneficiary account on Tuesday. If you remit on Friday, your funds should be transferred to your beneficiary account on Monday.However, there may be delays due to public holidays or unforeseen circumstances. For urgent transfers, please contact us for advice on the expected processing time.");

		$faq[] = array("question"=>"Payment options?","answer"=>"You may make your payment using any of the following payment methods:/ 1. PayNow, / 2. Internet bank transfer");

		$faq[] = array("question"=>"How can I know if my transaction has been accepted?","answer"=>"1. Go to Transaction Status. / 2. If the status of the relevant transaction is Processing, your transaction has been accepted and we are currently in the midst of transferring your funds.");


		$faq[] = array("question"=>"How do I add new beneficiary(ies)?","answer"=>"Please head down to any of our branches for assistance.");

		$faq[] = array("question"=>"Are the remittance rates shown in the app live?","answer"=>"The currency rates shown in the app are the identical to the rates offered at the branch.");

		$faq[] = array("question"=>"Other Enquiries","answer"=>"For any other enquiries, please contact us at /Boon Lay Branch /221 Boon Lay Shopping Centre /#01-128 Boon Lay Place /Singapore 640221. /Tel: (+65) 6266 1817 /Email: general@jcglobal.sg //Punggol Branch /Waterway Point, #B2-K11 /83 Punggol Central /Singapore 828761. /Tel: +65 6385 1817 /Email: info@jcglobal.sg //Yishun Branch /Northpoint City,#B2-131 /1 NorthPoint Drive /Singapore 768019. /Tel: +65 6455 1817 /Email: info@jcglobal.sg");

    	return response()->json(['success' => true, 'data' => $faq]);
    }
    public function device(Request $request)
    {
    	$date 		  = date('Y-m-d H:i:s');
    	$mobile_no    = $request->mobile_no;
		$device_type  = $request->device_type;
		$device_token = $request->device_token;
		$version      = $request->version;

		if($mobile_no!='' && $device_type!='' && $device_token!=''){

			$user_id = CashCustomer::where('PhoneNo',$mobile_no)->first();
			//print_r($user_id);exit;
			if($user_id){
				$data = [
					"user_id"     => $user_id->CustCode,
					"mobile_no"   => $mobile_no,
					"device_type" => $device_type,
					"device_token"=> $device_token,
					"version"     => $version,
					"CreatedDate" => $date,
					"ModifiedDate" => $date,
				];
				$exist  = Device::where('user_id', $user_id->CustCode)->first();
				/*if($exist){
					unset($data['user_id']);
					unset($data['CreatedDate']);
					Device::where('user_id',$user_id->CustCode)->update($data);
				} else {
					Device::create($data);
				}*/
				return response()->json(["success"=>true, "message" =>'success']);
			} else {
				return response()->json(["success"=>false, "message" =>'Invalid Details']);
			}
		} else {
			return response()->json(["success"=>false, "message" =>'Invalid Details']);
		}
    }
}
