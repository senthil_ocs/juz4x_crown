<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Nationality;
use App\DDtransaction;
use App\DDtransactionunpost;
use App\Purpose;
use App\Beneficiary;
use App\Currency;
use App\Country;
use App\CustomerType;
use App\Imageupload;
use Carbon\Carbon;
use DB;


class AddapplicationController extends Controller
{
	public function Add(Request $request)
	{
	   	$applicant_id    		 	=  $request->applicant_id;
		$applicant_customer_code 	=  $request->applicant_customer_code;
		$purpose_id    			 	=  $request->purpose_id;
		$beneficiary_id    		 	=  $request->beneficiary_id;
		$beneficiary_customer_code  =  $request->beneficiary_customer_code;
		$currency_code    			=  $request->currency_code;
		$currency_rate    			=  $request->currency_rate;
		$amount    					=  $request->amount;
		$transaction_amount    		=  $request->transaction_amount;
		$additional_amount    		=  $request->additional_amount;
		$total_amount    			=  $request->total_amount;
		$notes    					=  $request->notes;
		$status    					=  $request->status;
		$others_purpose    			=  $request->others_purpose;
		$lat    					=  $request->lat;
		$long    					=  $request->long;


		if($request->all() && $applicant_id != '' && $currency_code != '' && $applicant_customer_code != ''){
			if(!empty($purpose_id)){
				$purpose  = Purpose::where('id',$purpose_id)->first();
			}
			if(!empty($beneficiary_id)){
				$ben = Beneficiary::where('BeneId',$beneficiary_id)->first();
			}
			if(!empty($currency_code)){
				$currency = Currency::where('CurrencyName',$currency_code)->first();
			}
			$cust = DB::table('customer_master')->Where('Custcode', $applicant_customer_code)->orWhere('NRICNo', $applicant_customer_code)->first();
			if(!$cust){
				return response()->json(["success"=>false, "message" =>'Invalid Customer Details']);
			}
			$Tranid = DDtransactionunpost::max('id');
        	$TranNo = (int)$Tranid+1;

			$ddtransaction = new DDtransactionunpost;
			$ddtransaction->TranNo  = str_pad($TranNo, env('DD_PREFIX'), '0', STR_PAD_LEFT);
			$ddtransaction->BeneId  = isset($ben->BeneId) ? $ben->BeneId : '';
			$ddtransaction->CustCode  = $cust->Custcode;
			$ddtransaction->CustName  = $cust->Custcode;
			$ddtransaction->CustPPNo  = $cust->NRICNo;
			$ddtransaction->DOB  = $cust->DOB;
			$ddtransaction->CustNationality  = $cust->Nationality;
			$ddtransaction->Purpose  = isset($purpose->name) ?  $purpose->name : '';
			$ddtransaction->CustTTType  = $cust->TypeOfCustomer;
			$ddtransaction->CurrencyCode  = $currency->CurrencyCode;
			$ddtransaction->ExchRate  = $currency_rate;
			$ddtransaction->FAmount  = $transaction_amount;
			$ddtransaction->LAmount  = $additional_amount;
			$ddtransaction->BenPPNo  = isset($ben->NRICNo) ? $ben->NRICNo : NULL;
			$ddtransaction->BenBank  = isset($ben->BankName) ? $ben->BankName : NULL;
			$ddtransaction->BenAccNo  = isset($ben->BankAcNo) ? $ben->BankAcNo : NULL;
			$ddtransaction->BenBankBranch  = isset($ben->BankBranch) ? $ben->BankBranch : NULL;
			$ddtransaction->TotalAmount = $total_amount;
			$ddtransaction->TranDate = date('Y-m-d H:i:s');
			$ddtransaction->PaymentDate = date('Y-m-d H:i:s');
			$ddtransaction->Other = $others_purpose;
			$ddtransaction->SwiftCode  = isset($ben->SwiftCode) ? $ben->SwiftCode : NULL;
			$ddtransaction->PaidStatus = $status;
			$ddtransaction->CreatedDate = now();
			$ddtransaction->CreatedBy = 'api';
			$ddtransaction->save();

			$lastid = $ddtransaction->id;
			if($lastid){
				$val = DDtransactionunpost::where('id',$lastid)->first();
				$res = array(
					"applicant_id"		=>	$val->TranNo,
					"applicant_customer_code"	=> $val->CustCode,
					"purpose_id"				=> $val->Purpose,
					"beneficiary_id"			=> $val->BeneId,
					"beneficiary_customer_code"	=> $val->BenPPNo,
					"currency_code"				=> $val->CurrencyCode,
					"currency_rate"				=> $val->ExchRate,
					"amount"					=> $val->FAmount,
					"transaction_amount"		=> $val->LAmount,
					"total_amount"				=> $val->TotalAmount,
					"notes" 					=> $val->Remarks,
					"status"					=> $val->PaidStatus,
					"others_purpose"			=> $val->Other
				);
				return response()->json(["success"=>true, "details" =>$res]);
			} else {
				return response()->json(["success"=>false, "message" =>'Invalid Details']);
			}
		} else {
			return response()->json(["success"=>false, "message" =>'Invalid Details']);
		}
	}

	public function application_image(Request $request)
	{
		if($request->id != '' && $request->image != ''){
			$app = DDtransactionunpost::where('id', $request->id)->orWhere('TranNo', $request->id)->first();
			if($app){

				$imageName = time().'.'.$request->image->getClientOriginalExtension();
				$request->image->move(public_path('upload'), $imageName);

				$img 			 = new Imageupload();
				$img->app_id 	 = $request->id;
				$img->image_name = $imageName;
				$img->notes      = $request->notes;
				$img->save();

				$res = Imageupload::where('app_id', $request->id)->get();
				return response()->json(["success"=>true, "details" =>$res->toArray()]);
			} else {
				return response()->json(["success"=>false, "message" =>'Invalid Details']);	
			}
		} else {
			return response()->json(["success"=>false, "message" =>'Invalid Details']);
		}
	}
}
