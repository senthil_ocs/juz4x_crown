<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Currency;
use App\LocationCurrency;
use App\DDtransactionunpost;
use App\Purpose;
use DB;

class CurrencyController extends Controller
{

	public function index(Request $request)
	{   
		$loccode = env('USER_LOCATION');
		if($loccode){
			//$datas = LocationCurrency::where("LocationCode", $loccode)->with('currencies')->get();
			$datas =  DB::table('rate_fix')->get()->toArray();
			//print_r($datas);exit;
			
			if(count($datas) > 0){
				foreach ($datas as $key => $value) {
					$res[] = array(
						"cur_id"		=>	$value->id,
						"cur_code"		=>	$value->currency_code,
						"cus_cur_code"  =>  $value->currency_code,
						"cur_name"		=>	$value->currency_code,
						"cur_status"   	=>	'1',
						"cur_sortorder"	=>	$value->id,
						"rate"			=>  $value->price,
						"flag"          =>  '',
						"additiona_charges"=> array("currency_code"=>$value->currency_code,"min_value"=>"0.9999","min_charges"=>"10.0000","max_value"=>"1.0000","max_charges"=>"10.0000")
					);
				}
				return response()->json(["success"=>true, "details"=>$res]);
			} else {
				return response()->json(["success"=>false, "message" =>'Invalid Details']);
			}
		} else {
			return response()->json(["success"=>false, "message" =>'Invalid Details']);
		}
	}

	public function purpose(Request $request)
	{
		$datas = Purpose::get();
		if(count($datas) > 0){
			foreach ($datas as $key => $value) {
				$res[] = array(
					"pur_id"		=>	$value->id,
					"pur_name"		=>	$value->Name,
					"pur_status"   	=>	($value->Status == 1) ? 'Active' : 'Inactive',
				);
			}
			return response()->json(["success"=>true, "details"=>$res]);
		} else {
			return response()->json(["success"=>false, "message" =>'No Details']);
		}
	}

	public function bank_accounts(Request $request)
	{
		
		$res = array(
			"id"		=>	'2',
			"bank_name"		=>	'DBS',
			"location"   	=>	'001',
			"account_name"  => "Payout",
			"account_type"  =>"Current",
			"sgd_account"   =>"0029003877",
			"zipcode"       =>"",
			"contact_telephone"=>"62661817",
			"contact_mobile"=>"93848701"
		);		
		return response()->json(["success"=>true, "details"=>$res]);
		
	}
	public function get_transactions(Request $request){
		$statusAry      = array('Completed','Processing','Pending','Cancelled');
		if($request->customer_code){
			$tran = DB::table('dd_transaction_unpost')
			/*->leftJoin('beneficiary', 'dd_transaction_unpost.CustCode', '=', 'beneficiary.CustCode')*/
			->where('dd_transaction_unpost.CustCode', $request->customer_code)
			->get();
			$final = array();
			if(count($tran) > 0){
				foreach ($tran as $key => $value) {
					$res['All'][] = array(
						"trans_id"   				=>  $value->TranNo,
						"applicant_id"				=>	$value->id,
						"applicant_customer_code"	=>	$value->CustCode,
						"purpose_id"				=>	$value->Purpose,
						"purpose_name"				=>	$value->Purpose,
						"beneficiary_id"			=>	$value->BeneId,
						"beneficiary_customer_code"	=>	$value->BenPPNo,
						"beneficiary_name"			=>	$value->BenName,
						"currency_code"				=>	$value->CurrencyCode,
						"currency_rate"				=>	$value->ExchRate,
						"amount"					=>	$value->FAmount,
						"transaction_amount"		=>	$value->LAmount,
						"additional_amount"			=>	$value->Comm,
						"total_amount"				=>	$value->TotalAmount,
						"notes" 					=>  $value->Remarks,
						"status"					=>	$value->PaidStatus,
						"received_date"				=>  $value->PaymentDate ? date('d-m-Y', strtotime($value->PaymentDate)) : '',
						"date"					    =>	$value->CreatedDate ? date('d-m-Y', strtotime($value->CreatedDate)) : '',
						/*"location"					=>	$value->location_code,*/
						"others_purpose"			=>	$value->Other,
						"customer_request"          =>'0',
						'location'                  =>'001',
						'pdf'                       =>'',
						'documents'                 =>array()
					);
				}
				
				$statusAry      = array('Completed','Processing','Pending','Cancelled');
				
					foreach ($statusAry as $key => $status) {
						$tran = DB::table('dd_transaction_unpost')
							/*->leftJoin('beneficiary', 'dd_transaction_unpost.CustCode', '=', 'beneficiary.CustCode')*/
							->where('dd_transaction_unpost.CustCode', $request->customer_code)
							->where('dd_transaction_unpost.PaidStatus', $status)
							->get();
							/*printArray($tran);die;*/
							$final = array();
							if(count($tran) > 0){
								foreach ($tran as $key => $value) {
									$final[] = array(
										"trans_id"   				=>  $value->TranNo,
										"applicant_id"				=>	$value->id,
										"applicant_customer_code"	=>	$value->CustCode,
										"purpose_id"				=>	$value->Purpose,
										"purpose_name"				=>	$value->Purpose,
										"beneficiary_id"			=>	$value->BeneId,
										"beneficiary_customer_code"	=>	$value->BenPPNo,
										"beneficiary_name"			=>	$value->BenName,
										"currency_code"				=>	$value->CurrencyCode,
										"currency_rate"				=>	$value->ExchRate,
										"amount"					=>	$value->FAmount,
										"transaction_amount"		=>	$value->LAmount,
										"additional_amount"			=>	$value->Comm,
										"total_amount"				=>	$value->TotalAmount,
										"notes" 					=>  $value->Remarks,
										"status"					=>	$value->PaidStatus,
										"received_date"				=>  $value->PaymentDate ? date('d-m-Y', strtotime($value->PaymentDate)) : '',
										"date"					    =>	$value->CreatedDate ? date('d-m-Y', strtotime($value->CreatedDate)) : '',
										/*"location"					=>	$value->location_code,*/
										"others_purpose"			=>	$value->Other,
									);
								}
							}
						$res[$status] = $final;
					}				
				return response()->json(["success"=>true, "details"=>$res]);
			}else{
				return response()->json(["success"=>false, "message" =>'Details not found']);
			}
		}else{
			return response()->json(["success"=>false, "message" =>'Details not found']);
		}

	}

	public function get_transactionsX(Request $request)
	{
		$statusAry      = array('Completed','Processing','Pending','Cancelled');
		if($request->customer_code){
			$decheck = DDtransactionunpost::where('CustCode', $request->customer_code)->get();
			if(count($decheck) >= 1){
				foreach ($statusAry as $key => $status) {
					$tran = DB::table('dd_transaction_unpost')
						/*->leftJoin('beneficiary', 'dd_transaction_unpost.CustCode', '=', 'beneficiary.CustCode')*/
						->where('dd_transaction_unpost.CustCode', $request->customer_code)
						->where('dd_transaction_unpost.PaidStatus', $status)
						->get();
						/*printArray($tran);die;*/
						$final = array();
						if(count($tran) > 0){
							foreach ($tran as $key => $value) {
								$final[] = array(
									"trans_id"   				=>  $value->TranNo,
									"applicant_id"				=>	$value->id,
									"applicant_customer_code"	=>	$value->CustCode,
									"purpose_id"				=>	$value->Purpose,
									"purpose_name"				=>	$value->Purpose,
									"beneficiary_id"			=>	$value->BeneId,
									"beneficiary_customer_code"	=>	$value->BenPPNo,
									"beneficiary_name"			=>	$value->BenName,
									"currency_code"				=>	$value->CurrencyCode,
									"currency_rate"				=>	$value->ExchRate,
									"amount"					=>	$value->FAmount,
									"transaction_amount"		=>	$value->LAmount,
									"additional_amount"			=>	$value->Comm,
									"total_amount"				=>	$value->TotalAmount,
									"notes" 					=>  $value->Remarks,
									"status"					=>	$value->PaidStatus,
									"received_date"				=>  $value->PaymentDate ? date('d-m-Y', strtotime($value->PaymentDate)) : '',
									"date"					    =>	$value->CreatedDate ? date('d-m-Y', strtotime($value->CreatedDate)) : '',
									/*"location"					=>	$value->location_code,*/
									"others_purpose"			=>	$value->Other,
								);
							}
						}
					$res[$status] = $final;
				}
				return response()->json(["success"=>true, "details"=>$res]);
			} else {
				return response()->json(["success"=>false, "message" =>'Details not found']);
			}
		} else {
			return response()->json(["success"=>false, "message" =>'Details not found']);
		}
	}

}
