<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Beneficiary;
use App\CustomerType;
use App\CashCustomer;
use DB;


class CustomerController extends Controller
{
    public function get_customer(Request $request)
    {
		$nric   = $request->icno;
		$CustId = $request->CustId;
        $type   = $request->type;

		if($nric != ''){
            if($type!='cash'){
                $customer = Customer::where('NRICNo', $nric)->with('Beneficiary')->first();
            }else{
                $customer = CashCustomer::where('PPNo', $nric)->first();
                $beneDetails = Beneficiary::where('CustNric',$customer->PPNo)->get();
            }
			//print_r($customer);exit;
			if($customer){	
				$ben = array();
				if(count($beneDetails ) > 0){
 					foreach ($beneDetails  as $key => $value) {
 						$ben[] = array(
							"Beneid"			=>  $value->BeneId,
							"BeneCustCode"		=>	$value->CustCode,
							"BeneCustNRICNO"  	=>	$value->CustNric,
							"BeneName"			=>	$value->Name,
							"BeneAddress1"		=>	$value->Address1,
							"BeneAddress2"  	=>	$value->Address2,
							"BeneCountry"		=>	$value->Country,
							"BeneBankName"		=>	$value->BankName,
							"BeneBankBranch"	=>	$value->BeneBankBranch,
							"BeneBankAccNo"		=>	$value->BankBranch,
							"BeneSwiftCode"		=>	$value->SwiftCode,
							/*"CurrencyCode"		=>	$value->currency_cod*/
 						);
 					}
				}
				$res = array(
					"CustomerId"   =>  (string)$customer->id,
                    "CustId"        =>  (string)$customer->id,
                    "CustCode"      =>  $customer->CustCode,
                    "CustNRICNO"    =>  $customer->PPNo,
                    "CustName"      =>  $customer->Name,
                    "CustAddress1"  =>  isset($customer->Address1) ? $customer->Address1 : '',
                    "CustAddress2"  =>  isset($customer->Address2) ? $customer->Address2 : '',
                    "CustAddress3"  =>  isset($customer->Address3) ? $customer->Address3 : '',
                    "CustCountry"   =>  isset($customer->Address4) ? $customer->Address4 : '',
                    "CustPostalCode"=>  isset($customer->PostalCode) ? $customer->PostalCode : '',
                    "CustDOB"       =>  date("d-m-Y",strtotime($customer->DOB)),
                    "CustMobileNO"  =>  $customer->PhoneNo,
                    "CompName"      =>  isset($customer->NameOfcompany) ? $customer->NameOfcompany : '',
                    "IsCompany"     =>  isset($customer->IsCompany) ? $customer->IsCompany : '',
                    "CompanyType"   =>  isset($customer->CompanyType) ? $customer->CompanyType : '',
                    "Nationality"   =>  $customer->Nationality,
                    "Location"      =>  isset($customer->Location) ? $customer->Location : '',
                    "Active"        =>  $customer->Activebit ==1 ? 'Active' : 'Inactive',
					"Beneficiary Details" 	=>  $ben
				);
				return response()->json(["success"=>true, "details"=>$res]);
			} else {
				return response()->json(["success"=>false, "message" =>'Invalid Customer Details']);	
			}
		} else {
			return response()->json(["success"=>false, "message" =>'Invalid Customer Details']);
		}
    }

    public function beneficiaryPost(Request $request)
    {
    	if(isset($request->CustCode) && $request->CustCode !=''){
    		$CustCode = $request->CustCode;
    	} else {
    		$errorMsg[] = 'CustCode - Should not empty';
    	}
    	if(isset($request->CustNRICNO) && $request->CustNRICNO !=''){
    		$CustNRICNO = $request->CustNRICNO;
    	} else {
    		$errorMsg[] = 'CustNRICNO - Should not empty';
    	}
    	if(isset($request->BeneName) && $request->BeneName !=''){
    		$BeneName = $request->BeneName;
    	} else {
    		$errorMsg[] = 'BeneName - Should not empty';
    	}
    	if(isset($request->BeneBankName) && $request->BeneBankName !=''){
    		$BeneBankName = $request->BeneBankName;
    	} else {
    		$errorMsg[] = 'BeneBankName - Should not empty';
    	}	
    	if(isset($request->BeneBankAccNo) && $request->BeneBankAccNo !=''){
    		$BeneBankAccNo = $request->BeneBankAccNo;
    	} else {
    		$errorMsg[] = 'BeneBankAccNo - Should not empty';
    	}
    	if(isset($request->BeneBankBranch) && $request->BeneBankBranch !=''){
    		$BeneBankBranch = $request->BeneBankBranch;
    	} else {
    		$errorMsg[] = 'BeneBankBranch - Should not empty';
    	}
    	if(isset($request->BeneMobileNo) && $request->BeneMobileNo !=''){
    		$BeneMobileNo = $request->BeneMobileNo;
    	} else {
    		$errorMsg[] = 'BeneMobileNo - Should not empty';
    	}	

		if(empty($errorMsg)){
        	$data = [
        		'CustCode' => $CustCode,
        		'CustNric' => $CustNRICNO,
        		'NRICNo'   => '',
        		'Name'     => $BeneName,
        		'Address1' => $request->BeneAddress1,
        		'Address2' => $request->BeneAddress2,
        		'Country'  => '',
        		'BankName' => $BeneBankName,
        		'BankBranch' => $BeneBankBranch,
        		'BankAcNo' => $BeneBankAccNo,
        		'SwiftCode' => '',
        	];
            $exit = Beneficiary::where('NRICNo',$CustCode)->orwhere('NRICNo',$CustNRICNO)->first();
            if($exit){
                Beneficiary::where('CustCode',$CustCode)->orwhere('NRICNo',$CustNRICNO)->update($data);
                $message = "updated successfully";
            } else {
                Beneficiary::create($data); 
                $message = "inserted successfully";
            }
            return response()->json(["success"=>true,"message" => $message]);
		} else {
			return response()->json(["success"=>false, "message" =>$errorMsg]);
		}
    }
}
