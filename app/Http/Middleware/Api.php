<?php

namespace App\Http\Middleware;

use Closure;

class Api
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('userid') !='' && $request->header('password') !='' ){
            $header_value = base64_encode(env('AUTH_USER_ID').":".env('AUTH_USER_PWD'));
            $request_header_value = base64_encode($request->header('userid').":".$request->header('password'));
            if($header_value == $request_header_value){
                return $next($request);
            } else {
                return response()->json(["success"=>false, "details" =>"Invalid Header Details"]);
            }
        } else {
            return response()->json(["success"=>false, "details" =>"Invalid Header Details"]);
        }
    }
}
