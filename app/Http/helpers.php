<?php
use Illuminate\Support\Facades\Auth;
use App\LocationCurrency;

function printArray($res){
    echo '<pre>';
    print_r($res);
    echo '</pre>';
}

function getCurrentDateTime() {
    return date('Y-m-d H:i:s');
}

function getComputerName() {
    return shell_exec("hostname");
}

function errorLog($route='',$notes='',$str='')
{
	if(env('APP_ERROR_LOG')){
		$path 	= storage_path('logs/error/');
		$fileName = date('Y-m-d').'.txt';
		$file 	= $path.$fileName;
		$myfile   = fopen($file, "a") or die("Unable to open file!");
		$txt = "\n";
		$txt.= $route."---->".$notes."----> DateTime"."-->".date('Y-m-d H:i:s')."\n";
		$txt.= "======================================================================================\n";
		$txt.= $str;
		fwrite($myfile, "\n". $txt);
		fclose($myfile);
	} 
}

function postLog($route='',$notes='',$str='')
{
	if(env('APP_POST_LOG')){
	  $path 	= storage_path('logs/post_data/');
	  $fileName = date('Y-m-d').'.txt';
	  $file 	= $path.$fileName;
	  $myfile   = fopen($file, "a") or die("Unable to open file!");
	  $txt = "\n";
	  $txt.= $route."---->".$notes."----> DateTime"."-->".date('Y-m-d H:i:s')."\n";
	  $txt.= "======================================================================================\n";
	  $txt.= $str;
	  fwrite($myfile, "\n". $txt);
	  fclose($myfile);
	} 
}

function Api_post_log()
{
	$path  = storage_path('Apipostlog/');
	$fileName = date('Y-m-d').'.txt';
	$file 	= $path.$fileName;
	$myfile   = fopen($file, "a") or die("Unable to open file!");
	
	$txt = "\n";
	$txt.= $route."---->".$notes."----> DateTime"."-->".date('Y-m-d H:i:s')."\n";
	$txt.= "======================================================================================\n";
	$txt.= $str;

	fwrite($myfile, "\n". $txt);
	fclose($myfile);
}

function currency_format($currency, $pre='8')
{
	if($currency == ''){
		$currency = 0;
	}
	if($pre!= ''){
		$data = number_format($currency, $pre);	
	} else {
		$data = number_format($currency, env('AMOUNT_DECIMAL'));
	}
	// $data = str_replace(',', '', $data);
	return $data;
}

 function displayDateformat($date)
 {
	return date_format($date,'Y-m-d');
 }
 function displayDateTimeformat($date)
 {
	return date_format($date,'Y-m-d H:i:s');
 }
 function convertDateTimeformat($date)
 {
	return date('Y-m-d H:i:s',strtotime($date));
 }
 function convertDateformat($date)
 {
	return date('Y-m-d',strtotime($date));
 }
function containsDecimal( $value ) {
    if ( strpos( $value, "." ) !== false ) {
        return true;
    }
    return false;
}

function displayCorrectDateFormat($date)
{
	return date('d/m/Y',strtotime($date));
}

function displayCorrectTimeFormat($date)
{
	return date('H:i:s',strtotime($date));
}

function getLocationCurrencyStock($loccode,$currcode) {
	$locationcurrdetails = LocationCurrency::where('LocationCode',$loccode)->where('CurrencyCode',$currcode)->pluck('Stock')->all();
	return $locationcurrdetails[0];
}

function getLocationCurrencyDealStock($loccode,$currcode) {
	$locationcurrdetails = LocationCurrency::where('LocationCode',$loccode)->where('CurrencyCode',$currcode)->pluck('DealStock')->all();
	return $locationcurrdetails[0];
}

function doGetAddress($strZipCode) 
{
   $graph_url = "https://developers.onemap.sg/commonapi/search?searchVal=".$strZipCode."&returnGeom=Y&getAddrDetails=Y";
   $response = curl_get_file_contents($graph_url);
   $decoded_response = json_decode($response);
   if ($decoded_response->results) {
   		$strResult = array(
   			'block' => $decoded_response->results[0]->BLK_NO,
   			'Road'  => $decoded_response->results[0]->ROAD_NAME,
   			'buliding' => $decoded_response->results[0]->BUILDING,
   			'postalcode' => $decoded_response->results[0]->POSTAL,
   		);
       return response()->json(['strResult'=>$strResult]);
   } else {
       return response()->json(['strResult'=>'Error']);
   }
}
 
function curl_get_file_contents($URL) 
{
   $c = curl_init();
   curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($c, CURLOPT_URL, $URL);
   $contents = curl_exec($c);
   $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
   curl_close($c);
   if ($contents) return $contents;
   else return FALSE;
}

/*function verifyToken($userid='', $password='')
{
	if($userid !='' && $password !='' ){
		$header_value = base64_encode(env('AUTH_USER_ID').":".env('AUTH_USER_PWD'));
		$request_header_value = base64_encode($userid.":".$password);
		if($header_value == $request_header_value){
			return true;
		} else {
			return response()->json(["success"=>false, "details" =>"Invalid Header Details"]);
		}
	} else {
		return response()->json(["success"=>false, "details" =>"Invalid Header Details"]);
	}
} */

function SendOTP($url,$post_data,$authStr)
{
	$client = new \GuzzleHttp\Client();
	$request = $client->post($url,[
	    'headers' => [
		    "authorization:".$authStr,
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
	    ],
	    'form_params' => [$post_data],
	]);
	$response = $request->getBody()->getContents();	
	$data = json_decode($response);
	return $data;
}

function curl_request($url, $data) 
{
    $ch = curl_init($url);
    $headers = [
        'KeyId: ac89c4a6-d382-4f5c-b2dc-b221a602b19d',
        'ORG_ID: ARAMCOP3',
        'Content-Type: application/json'
    ];
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        print "Error: " . curl_error($ch);
        common_log(curl_error($ch));
        exit();
    }
    curl_close($ch);
    return $result;
}



function Test()
{
	require_once 'Crypt/GPG.php';
	$gpg = new Crypt_GPG(array('digest-algo' => 'SHA256', 'cipher-algo' => 'AES256'));
   	printArray($gpg);die;
}
?>