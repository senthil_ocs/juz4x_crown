<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imageupload extends Model
{
    protected $table    = 'application_uploads'; 
    public $timestamps = false;
    protected $fillable = [
    	'app_id', 'image_name', 'notes'
    ];
}
