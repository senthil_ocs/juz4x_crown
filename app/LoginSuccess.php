<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginSuccess extends Model
{
    protected $table    = 'login_details';   
    protected $fillable = [
        'LoginId', 'UserId', 'logintoken','LoginTime','LogoutTime','created_at','updated_at',
    ];    
}
