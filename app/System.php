<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    //
    public $table        = "company";
    protected $fillable  = ['Description','Address1','Address2','Address3','Postalcode'];
}
