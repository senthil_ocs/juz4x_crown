<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orginator extends Model
{
	const CREATED_AT = 'CreatedDate';
	const UPDATED_AT = 'ModifiedDate';
	
    protected $table    = 'orginator'; 
    protected $fillable = ['id','Name','NRICNo', 'DOB', 'Nationality','Address', 'Code', 'Beneficiary_id', 'CreatedBy','CreatedDate','ModifiedBy','ModifiedDate'];
}
