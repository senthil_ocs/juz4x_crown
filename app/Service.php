<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
	
    protected $table    = 'service'; 
    protected $fillable = ['ServiceCode','Description', 'Amount', 'CreateUser','CreateDate','ModifyUser','ModifyDate'];
}
