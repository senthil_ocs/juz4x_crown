<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuySell extends Model
{
    protected $table    = 'buysell'; 
    public $timestamps = false;
    const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';

    protected $fillable = [
        'TranNo', 'TranDate', 'DocType', 'RefNo', 'PPNo', 'Nationality', 'Name','CustName', 'Address1','Address2', 'Address3','Address4', 'Address5','CustCode', 'LocationCode','Remarks', 'TTRefNo','Deleted', 'DOB','MCType', 'TerminalName','CreatedBy', 'CreatedDate','ModifiedBy','ModifiedDate','SettlementMode','IdentifierType','SuspiciousTran','PhoneNo'
    ];

    public function buyselldetails()
    {
    	return $this->hasOne('App\BuySellDetails', 'TranNo', 'TranNo');
    }

    public function buyselldetailss()
    {
        return $this->hasMany('App\BuySellDetails', 'TranNo', 'TranNo');
    }
}
