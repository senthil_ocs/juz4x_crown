<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenificieryDetails extends Model
{
    protected $table    = 'benificiery_details'; 
    protected $fillable = [
        'id','beneficiary_id', 'CustCode', 'CustId', 'CustNRICNO', 'BeneName', 'BeneBankName', 'BeneBankAccNo','BeneAddress1', 'BeneAddress2','BeneCountry', 'BeneMobileNo','BeneBankBranch', 'SwiftCode','	Active','currency_code','purpose'
    ];
}
