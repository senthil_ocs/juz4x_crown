<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationCurrency extends Model
{
    protected $table    = 'location_currency'; 
    protected $fillable = [
        'LocationCode', 'CurrencyCode', 'Varience', 'Stock', 'BuyRate', 'SellRate', 'Units','AvgCost', 'DealStock','CreatedBy','CreatedDate', 'ModifiedBy','ModifiedDate'
    ];

    public function currencies() {
        return $this->hasOne('\App\Currency','CurrencyCode','CurrencyCode');
    }
}
