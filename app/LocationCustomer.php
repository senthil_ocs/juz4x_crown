<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationCustomer extends Model
{
    protected $table    = 'location_customer';
    public $timestamps = false; 
    protected $fillable = [
        'Custcode', 'CustName', 'Address1', 'Address2', 'Address3', 'Address4', 'Address5','Phone', 'Fax','Balance','NRICNo', 'Name','Ob','CreditLimit','Remarks','TTCustomer','RemittanceLicensee','Country','Location','MoneyChangerLicense','TradingLicense','DOB','MCType','TTType','DailyCreditLimit','DealBalance','CreatedBy','CreatedDate','ModifiedBy','ModifiedDate','WebCustAllowed', 'WebCustCode','Active', 'HighRisk'
    ];

    public function customer() {
        return $this->hasOne('\App\Customer','Custcode','Custcode');
    }

    public function CContact() {
        return $this->hasMany('\App\CustomerContact','CustCode','Custcode');
    }
}
