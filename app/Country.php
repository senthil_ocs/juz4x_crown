<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table    = 'country'; 
    protected $fillable = ['Country','A2','A3','Num','Nationality','ShortCut','created_by','updated_by'];
}
