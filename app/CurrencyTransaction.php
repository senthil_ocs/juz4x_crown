<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyTransaction extends Model
{

    protected $table    = 'currency_transaction'; 
    public $timestamps = false;
    protected $fillable = [
        'CurrencyCode', 'Date', 'OpeningStock', 'OpeningAvgCost', 'BuyFamount', 'BuyLamount', 'SellFAmount','SellLAmount', 'OpeningDealStock','CreatedBy','CreatedDate', 'ModifiedBy','ModifiedDate'
    ];
}
