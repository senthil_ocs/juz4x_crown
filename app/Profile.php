<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table    = 'profile'; 
    protected $fillable = [
        'ProfileId', 'Description', 'Active', 'CreatedBy'
    ];

    public function users()
    {
    	return $this->hasMany('App\User');
    }
}
