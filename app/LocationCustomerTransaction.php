<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationCustomerTransaction extends Model
{
    protected $table    = 'location_customer_transaction';
    public $timestamps = false; 
    protected $fillable = [
        'CustCode', 'Date', 'OpeningBalance', 'Dbamt', 'Cramt', 'OpeningDealBalance', 'CreatedBy','CreatedDate', 'ModifiedBy','ModifiedDate','LocationCode'
    ];
}
