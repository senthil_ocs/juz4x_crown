<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Swift extends Model
{
	const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
    protected $table    = 'swift'; 
    protected $fillable = ['Code','Bank','BankBranch','Country','CreateUser','CreateDate','ModifyUser','ModifyDate'];
}

