<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginFailure extends Model
{
    protected $table    = 'login_failure';   
    protected $fillable = [
        'LoginId', 'UserId', 'TrialPassword','TrialDate','created_at','updated_at',
    ];    
}
