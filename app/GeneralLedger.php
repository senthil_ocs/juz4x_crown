<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralLedger extends Model
{
	const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
    protected $table    = 'general_ledger'; 
    protected $fillable = ['date','from_customer_code','to_customer_code','currency','amount','remark_from','	remark_to','CreateUser','CreateDate','ModifyUser','ModifyDate'];
}

