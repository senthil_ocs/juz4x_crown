<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
	const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
    protected $table    = 'nationality'; 
    protected $fillable = ['Code','Description','CreateUser','CreateDate','ModifyUser','ModifyDate'];
}
