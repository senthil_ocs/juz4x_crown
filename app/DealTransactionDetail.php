<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealTransactionDetail extends Model
{
    protected $table    = 'deal_trans_detail';
    public $timestamps = false; 
    protected $fillable = [
        'DealNo', 'TranNo', 'TranType', 'CurrencyCode', 'Rate', 'FAmount', 'LAmount','ValidTill', 'Remarks','Status','RealisedAmount', 'BalanceAmount', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate'
    ];
}
