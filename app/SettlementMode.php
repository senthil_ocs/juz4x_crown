<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettlementMode extends Model
{
	const CREATED_AT = 'CreateDate';
	const UPDATED_AT = 'ModifyDate';
    protected $table    = 'settlement_mode'; 
    protected $fillable = ['SettlementCode','Description','CreateUser','CreateDate','ModifyUser','ModifyDate'];
}
