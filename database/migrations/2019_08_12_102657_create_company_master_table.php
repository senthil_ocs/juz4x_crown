<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_master', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Description',140);
            $table->string('Address1',140);
            $table->string('Address2',50)->nullable();
            $table->string('Address3',50)->nullable();
            $table->string('Postalcode',50)->nullable();
            $table->string('Phone',14)->nullable();
            $table->string('Fax',20)->nullable();
            $table->string('Email',50)->nullable();
            $table->string('url',50)->nullable();
            $table->string('TaxRef',50)->nullable();
            $table->decimal('TaxPerc',8,2);
            $table->decimal('MassLimit',8,2);
            $table->integer('NextDealNo')->nullable();
            $table->integer('NextLogInID')->nullable();
            $table->integer('MaxPasswordAttempt')->nullable();
            $table->integer('PasswordValidity')->nullable();
            $table->string('TTType',2)->nullable();
            $table->string('CheckCustLoc',2)->nullable();
            $table->string('AutoCustomer',2)->nullable();
            $table->string('IsTransferAllowed',2)->nullable();
            $table->decimal('NextTTTranNo',8,2);
            $table->string('CreatedBy',140)->nullable();
            $table->dateTime('CreatedDate');
            $table->string('ModifiedBy',140)->nullable();
            $table->dateTime('ModifiedDate');
            $table->integer('MCStatus')->nullable();
            $table->string('CompanyShortCode',50)->nullable();
            $table->integer('NextBenNricNo')->nullable();
            $table->string('UENNo',50)->nullable();
            $table->integer('NextConsignmentNo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_master');
    }
}
