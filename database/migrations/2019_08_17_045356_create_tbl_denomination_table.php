<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDenominationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('denomination', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('CurrencyCode',10)->nullable();
            $table->decimal('Denomination',10,2)->nullable()->default('0.00');
            $table->string('Description',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denomination');
    }
}
