<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_group', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('GroupCode',10);
            $table->string('Description',50)->nullable();
            $table->string('CreateUser',20)->nullable();
            $table->datetime('CreateDate')->nullable();
            $table->string('ModifyUser',20)->nullable();
            $table->datetime('ModifyDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_group');
    }
}
