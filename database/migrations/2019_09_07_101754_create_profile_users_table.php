<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('UserId',50);
            $table->string('Description',255);
            $table->string('Password',100);
            $table->string('ProfileId',50);
            $table->enum('UserType', ['Cashier', 'Dealer', 'Both']);
            $table->integer('Active')->default('0');
            $table->integer('Administrator')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_users');
    }
}
