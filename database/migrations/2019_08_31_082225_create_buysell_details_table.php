<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuysellDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buysell_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('TranNo');
            $table->integer('Line');
            $table->string('TranType',1);
            $table->string('CurrencyCode',20);
            $table->double('Rate',10,9)->default('0.00');
            $table->double('FAmount',10,9)->default('0.00');
            $table->double('LAmount',10,9)->default('0.00');
            $table->integer('TTRefNo')->default('0');
            $table->string('CreatedBy',50)->nullable();
            $table->dateTime('CreatedDate')->nullable();
            $table->string('ModifiedBy',50)->nullable();
            $table->dateTime('ModifiedDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buysell_details');
    }
}
