<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginFailureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_failure', function (Blueprint $table) {
            $table->bigIncrements('LoginId');
            $table->string('UserId',50);
            $table->string('TrialPassword',50)->nullable();
            $table->string('ComputerName',120)->nullable();
            $table->dateTime('TrialDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('login_failure');
    }
}
