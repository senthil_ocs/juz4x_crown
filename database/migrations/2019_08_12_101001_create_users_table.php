<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('Id');
            $table->string('username', 140)->unique();
            $table->string('password', 140);
            $table->string('Description', 140)->nullable();
            $table->string('ProfileId', 140)->nullable();
            $table->integer('Active')->unsigned()->nullable();
            $table->integer('Admin')->unsigned()->nullable();
            $table->dateTime('ValideTill')->nullable(); 
            $table->string('CreadedBy', 140)->nullable();
            $table->dateTime('CreatedDate')->nullable();    
            $table->string('ModifiedBy', 140)->nullable();
            $table->dateTime('ModifiedDate')->nullable();   
            $table->string('UserType',2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
