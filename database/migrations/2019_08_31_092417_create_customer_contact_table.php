<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_contact', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->string('CustCode',50);
            $table->string('ContactNricNo',100)->nullable();
            $table->string('ContactName',100)->nullable();
            $table->string('ContactNationality',100)->nullable();
            $table->string('DOB',50)->nullable();
            $table->string('CreatedBy',10)->nullable();
            $table->string('ModifiedBy',10)->nullable();
            $table->dateTime('ModifiedDate')->nullable();
            $table->string('ContactType',200)->nullable();
            $table->string('ContactPosition',200)->nullable();
            $table->string('ContactSignature',200)->nullable();
            $table->integer('Active')->default('0');
            $table->string('Remarks',100)->nullable();
            $table->string('DOE',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_contact');
    }
}
