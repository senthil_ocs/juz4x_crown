<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCurrencyMasterTableToAddNullValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('currency_master', function (Blueprint $table) {
            $table->decimal('Varience',8,2)->nullable()->default('0.00')->change();
            $table->decimal('BuyRate',9,2)->nullable()->default('0.00')->change();
            $table->decimal('SellRate',9,2)->nullable()->default('0.00')->change();
            $table->decimal('Units',9,2)->nullable()->default('0.00')->change();
            $table->decimal('Stock',8,2)->nullable()->default('0.00')->change();
            $table->decimal('AvgCost',9,2)->nullable()->default('0.00')->change();
            $table->decimal('DealStock',8,2)->nullable()->default('0.00')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('currency_master', function (Blueprint $table) {
            //
        });
    }
}
