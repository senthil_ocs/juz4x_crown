<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuysellTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buysell', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('TranNo');
            $table->dateTime('TranDate');
            $table->string('DocType',5);
            $table->string('RefNo',50)->nullable();
            $table->string('PPNo',50)->nullable();
            $table->string('Nationality',120)->nullable();
            $table->string('Name',120)->nullable();
            $table->string('CustName',120)->nullable();
            $table->string('Address1',120)->nullable();
            $table->string('Address2',120)->nullable();
            $table->string('Address3',120)->nullable();
            $table->string('Address4',120)->nullable();
            $table->string('Address5',120)->nullable();
            $table->string('CustCode',20)->nullable();
            $table->string('LocationCode',20)->nullable();
            $table->string('Remarks',120)->nullable();
            $table->integer('TTRefNo')->default('0');
            $table->string('Deleted',1)->nullable();
            $table->dateTime('DOB')->nullable();
            $table->string('MCType',30)->nullable();
            $table->string('TerminalName',20)->nullable();
            $table->string('CreatedBy',20)->nullable();
            $table->dateTime('CreatedDate')->nullable();
            $table->string('ModifiedBy',20)->nullable();
            $table->dateTime('ModifiedDate')->nullable();
            $table->string('SettlementMode',100)->nullable();
            $table->string('IdentifierType',20)->nullable();
            $table->integer('SuspiciousTran')->default('0');
            $table->string('PhoneNo',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buysell');
    }
}
