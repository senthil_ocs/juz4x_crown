<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_currency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('LocationCode',20);
            $table->string('CurrencyCode',20);
            $table->decimal('Varience',8,2)->nullable()->default('0.00');
            $table->decimal('Stock',8,2)->nullable()->default('0.00');
            $table->decimal('BuyRate',9,2)->nullable()->default('0.00');
            $table->decimal('SellRate',9,2)->nullable()->default('0.00');
            $table->decimal('Units',9,2)->nullable()->default('0.00');
            $table->decimal('AvgCost',9,2)->nullable()->default('0.00');
            $table->decimal('DealStock',8,2)->nullable()->default('0.00');
            $table->string('CreatedBy',20)->nullable();
            $table->dateTime('CreatedDate')->nullable();
            $table->string('ModifiedBy',20)->nullable();
            $table->dateTime('ModifiedDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_currency');
    }
}
