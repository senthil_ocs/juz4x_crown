<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_master', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('CurrencyCode',20);
            $table->string('CurrencyName',60);
            $table->string('Major',50);
            $table->string('Minor',50);
            $table->string('LocalCurrency',1)->nullable();
            $table->decimal('Varience',8,2)->default('0.00');
            $table->decimal('BuyRate',9,2)->default('0.00');
            $table->decimal('SellRate',9,2)->default('0.00');
            $table->decimal('Units',9,2)->default('0.00');
            $table->decimal('Stock',8,2)->default('0.00');
            $table->decimal('AvgCost',9,2)->default('0.00');
            $table->dateTime('LastPdate')->nullable();
            $table->dateTime('LastSDate')->nullable();
            $table->string('TTCurrency',1)->nullable();
            $table->string('TTCustomerCode',20)->nullable();
            $table->string('TTCustomerUpdate',1)->nullable();
            $table->decimal('DealStock',8,2)->default('0.00');
            $table->string('Denomination',1)->nullable();
            $table->string('CurrencyGroup',20)->nullable();
            $table->string('Region',20)->nullable();
            $table->string('CreatedBy',20)->nullable();
            $table->dateTime('CreatedDate')->nullable();
            $table->string('ModifiedBy',20)->nullable();
            $table->dateTime('ModifiedDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_master');
    }
}
