<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_master', function (Blueprint $table) {
            $table->bigIncrements('Id');
            $table->string('Custcode',20);
            $table->string('CustName',140);
            $table->string('CompanyRegNo',50)->nullable();
            $table->string('IncorporationDate',20)->nullable();
            $table->string('IncorporationPlace',50)->nullable();
            $table->string('NatureOfbusiness',100)->nullable();
            $table->string('CompAddress1',160)->nullable();
            $table->string('CompAddress2',160)->nullable();
            $table->string('CompAddress3',160)->nullable();
            $table->string('Country',50)->nullable();
            $table->string('CompPhone1',30)->nullable();
            $table->string('CompFax',30)->nullable();
            $table->string('CompEmail',200)->nullable();
            $table->string('MoneyChangerLicense',20)->nullable();
            $table->string('RemittanceLicensee',1)->nullable();
            $table->string('Name',140)->nullable();
            $table->string('NRICNo',100)->nullable();
            $table->string('Nationality',50)->nullable();
            $table->string('Address1',160)->nullable();
            $table->string('Address2',160)->nullable();
            $table->string('Address3',120)->nullable();
            $table->string('Address4',120)->nullable();
            $table->string('Address5',120)->nullable();
            $table->string('Phone',30)->nullable();
            $table->string('Fax',30)->nullable();
            $table->string('HandPhone',14)->nullable();
            $table->string('Email',100)->nullable();
            $table->string('DOB',20)->nullable();
            $table->string('Position',250)->nullable();
            $table->string('NameOfcompany',250)->nullable();
            $table->string('SourceOfIncome',250)->nullable();
            $table->double('Ob',8,2)->default('0.00');
            $table->double('CreditLimit',8,2)->default('0.00');
            $table->string('Remarks',500)->nullable();
            $table->string('TTCustomer',1)->nullable();
            $table->string('TradingLicense',20)->nullable();
            $table->string('MCType',30)->nullable();
            $table->string('TTType',30)->nullable();
            $table->double('DailyCreditLimit',8,2)->default('0.00');
            $table->integer('WebCustAllowed')->nullable();
            $table->string('SpecimenSignature',255)->nullable();
            $table->string('AuthorisedSignature',255)->nullable();
            $table->integer('Active')->nullable();
            $table->string('CreatedBy',20)->nullable();
            $table->dateTime('CreatedDate')->nullable();
            $table->string('ModifiedBy',20)->nullable();
            $table->dateTime('ModifiedDate')->nullable();
            $table->string('CompPhone2',20)->nullable();
            $table->string('CompPostalCode',20)->nullable();
            $table->string('PostalCode',20)->nullable();
            $table->longText('EmploymentDetail')->nullable();
            $table->string('YearlyIncome',100)->nullable();
            $table->longText('CheckList',30)->nullable();
            $table->string('BusinessType',30)->nullable();
            $table->string('LicenseValidFrom',20)->nullable();
            $table->string('LicenseValidTo',20)->nullable();
            $table->string('IssuingAuthority',20)->nullable();
            $table->integer('OverSeasBranches')->nullable();
            $table->integer('LocalBranchesCount')->nullable();
            $table->double('AnnualTurnOver',30)->default('0.00');
            $table->string('TypeOfIdentification',20)->nullable();
            $table->string('OverSeasCompAddress',80)->nullable();
            $table->string('OverSeasCountry',50)->nullable();
            $table->string('OverSeasPostalCode',20)->nullable();
            $table->string('OverSeasTelephone',20)->nullable();
            $table->string('BuySelltype',20)->nullable();
            $table->integer('BannedNo')->nullable();
            $table->text('TypeOfCustomer')->nullable();
            $table->text('CompanySourceOfFunds')->nullable();
            $table->text('top3ForeignCurrencies')->nullable();
            $table->string('DealingswithLocalorForeignGovernment',50)->nullable();
            $table->string('EmailAddress',200)->nullable();
            $table->string('Website',200)->nullable();
            $table->string('TotalNoOfEmployees',200)->nullable();
            $table->text('Top3Countries')->nullable();
            $table->string('ReasonForReTT',200)->nullable();
            $table->text('RemitananceVolume')->nullable();
            $table->double('SGDVolume',8,2)->default('0.00');
            $table->integer('PCActive')->nullable();
            $table->string('PendingCustomerRemarks',100)->nullable();
            $table->integer('HighRisk')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_master');
    }
}
