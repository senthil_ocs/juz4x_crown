<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLocationCurrencyTableToChangeDecimalValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('location_currency', function (Blueprint $table) {
            $table->decimal('Varience',18,10)->nullable()->default('0.0000000000')->change();
            $table->decimal('Stock',18,10)->nullable()->default('0.0000000000')->change();
            $table->decimal('BuyRate',18,10)->nullable()->default('0.0000000000')->change();
            $table->decimal('SellRate',18,10)->nullable()->default('0.0000000000')->change();
            $table->decimal('Units',18,10)->nullable()->default('0.0000000000')->change();
            $table->decimal('AvgCost',18,10)->nullable()->default('0.0000000000')->change();
            $table->decimal('DealStock',18,10)->nullable()->default('0.0000000000')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location_currency', function (Blueprint $table) {
            //
        });
    }
}
